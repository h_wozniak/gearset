/**
*   @author         Sebastian Łasisz
*   @description    This class is used to retrieve information about Contact for given Id.
**/

@RestResource(urlMapping = '/contact-data/*')
global without sharing class IntegrationContactData {


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HTTP METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */


    @HttpGet
    global static void getContactData() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Id personId = RestContext.request.params.get('person_id');

        if (personId == null) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(605);
            return ;
        }

        try {
            res.responseBody = Blob.valueOf(prepareResponse(personId));
            res.statusCode = 200;
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;    
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- PREPARING DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String prepareResponse(Id contactId) {
        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name, ADO__c, Active_To__c, Active_From__c, iPressoId__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
        ];        

        Contact candidate = [
        	SELECT Id, FirstName, Middle_Name__c, LastName, Family_Name__c, Father_Name__c, Mother_Name__c, Gender__c, Pesel__c, Birthdate, Place_of_Birth__c, Country_of_Birth__c, Country_of_Origin__c,
	        	   OtherCountry, OtherPostalCode, OtherCity, OtherStreet, MailingCountry, MailingPostalCode, MailingCity, MailingStreet, Nationality__c, School_Certificate_Town__c, School_Certificate_Country__c,
	        	   Phone, MobilePhone, Additional_Phones__c, OtherPhone, Email, Additional_Emails__c, A_Level__c, A_Level_Id__c, A_Level_Issue_Date__c, Regional_Examination_Commission__c, Year_of_Graduation__c,
	               Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, School__r.BillingCity,
	               Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c, Consent_Graduates__c, School__c, School_Name__c, School__r.Name, School_Type__c, School__r.School_Type__c, School_Town__c, 
                   (SELECT University_from_Finished_University__c, University_from_Finished_University__r.Name, ZPI_University_Name__c, University_from_Finished_University__r.BillingCity, 
                           ZPI_University_City__c, University_from_Finished_University__r.School_Type__c, Finished_Course__c, Graduation_Year__c, Status__c, Defense_Date__c, Diploma_Number__c, Country_of_Diploma_Issue__c,
                           Diploma_Issue_Date__c, Gained_Title__c 
                    FROM FinishedSchools__r),
                   (SELECT Employed_Till__c, Employed_From__c, Company_Name__c, Position__c, City_of_Employment__c, Country_of_Employment__c, Postal_Code__c, Street_of_Employment__c 
                    FROM WorkExperience__r)
	        FROM Contact
	        WHERE Id = :contactId
        ];

        Contact_JSON contactJSON = new Contact_JSON();
        contactJSON.first_name = candidate.FirstName;
        contactJSON.middle_name = candidate.Middle_Name__c;
        contactJSON.last_name = candidate.LastName;
        contactJSON.family_name = candidate.Family_Name__c;
        contactJSON.father_name = candidate.Father_Name__c;
        contactJSON.mother_name = candidate.Mother_Name__c;
        contactJSON.gender = candidate.Gender__c;
        contactJSON.personal_id = candidate.Pesel__c;
        contactJSON.birthdate = dateToString(candidate.Birthdate);
        contactJSON.place_of_birth = candidate.Place_of_Birth__c;
        contactJSON.country_of_birth = candidate.Country_of_Birth__c;
        contactJSON.country_of_origin = candidate.Country_of_Origin__c;
        contactJSON.nationality = prepareNationalities(candidate.Nationality__c);
        contactJSON.addresses = addressesToList(candidate);
        contactJSON.phones = phonesToList(candidate.Phone, candidate.MobilePhone);
        contactJSON.primary_email = candidate.Email;
        contactJSON.additional_emails = emailsToList(candidate.Email, candidate.Additional_Emails__c, false);
        contactJSON.consents = prepareConsentsForContact(candidate, catalogConsentsToValidate);
        contactJSON.secondary_school = prepareSecondarySchool(candidate);
        contactJSON.university = prepareUniversities(candidate);
        contactJSON.career = prepareCareer(candidate);

        return JSON.serialize(contactJSON);
    }

    public static List<Address_JSON> addressesToList(Contact contactToSendToOtherSystems) {
        List<Address_JSON> contactAddresses = new List<Address_JSON>();

        Address_JSON homeAddressFromCandidate = new Address_JSON();
        homeAddressFromCandidate.type = 'Home';
        homeAddressFromCandidate.country = contactToSendToOtherSystems.OtherCountry;
        homeAddressFromCandidate.postal_code = contactToSendToOtherSystems.OtherPostalCode;
        homeAddressFromCandidate.city = contactToSendToOtherSystems.OtherCity;
        homeAddressFromCandidate.street = contactToSendToOtherSystems.OtherStreet;
        contactAddresses.add(homeAddressFromCandidate);

        Address_JSON mailingAddressFromCandidate = new Address_JSON();
        mailingAddressFromCandidate.type = 'Mailing';
        mailingAddressFromCandidate.country = contactToSendToOtherSystems.MailingCountry;
        mailingAddressFromCandidate.postal_code = contactToSendToOtherSystems.MailingPostalCode;
        mailingAddressFromCandidate.city = contactToSendToOtherSystems.MailingCity;
        mailingAddressFromCandidate.street = contactToSendToOtherSystems.MailingStreet;
        contactAddresses.add(mailingAddressFromCandidate); 

        return contactAddresses;      
    }

    public static List<University_JSON> prepareUniversities(Contact candidate) {
        List<University_JSON> universities = new List<University_JSON>();
        for (Qualification__c uniOnContact : candidate.FinishedSchools__r) {
            University_JSON university = new University_JSON();
            university.name = uniOnContact.University_from_Finished_University__c != null ? uniOnContact.University_from_Finished_University__r.Name : uniOnContact.ZPI_University_Name__c;
            university.city = uniOnContact.University_from_Finished_University__c != null ? uniOnContact.University_from_Finished_University__r.BillingCity : uniOnContact.ZPI_University_City__c;
            university.type = uniOnContact.University_from_Finished_University__c != null ? uniOnContact.University_from_Finished_University__r.School_Type__c : null;
            university.major = uniOnContact.Finished_Course__c;
            university.graduation_year = uniOnContact.Graduation_Year__c;

            Diploma_JSON diploma = new Diploma_JSON();
            diploma.status = uniOnContact.Status__c;
            diploma.examination_date = dateToString(uniOnContact.Defense_Date__c);
            diploma.diploma_number = uniOnContact.Diploma_Number__c;
            diploma.issue_country = uniOnContact.Country_of_Diploma_Issue__c;
            diploma.issue_date = dateToString(uniOnContact.Diploma_Issue_Date__c);
            diploma.title = uniOnContact.Gained_Title__c;

            university.diploma = diploma;
            universities.add(university);
        }

        return universities;
    }

    public static SecondarySchool_JSON prepareSecondarySchool(Contact contactToSendToOtherSystems) {
        SecondarySchool_JSON secondary_school = new SecondarySchool_JSON();
        secondary_school.name = contactToSendToOtherSystems.School__c == null ? contactToSendToOtherSystems.School_Name__c :contactToSendToOtherSystems.School__r.Name;
        secondary_school.type = contactToSendToOtherSystems.School__c == null ? contactToSendToOtherSystems.School_Type__c : contactToSendToOtherSystems.School__r.School_Type__c;
        secondary_school.city = contactToSendToOtherSystems.School__c == null ? contactToSendToOtherSystems.School_Town__c : contactToSendToOtherSystems.School__r.BillingCity;
        secondary_school.a_level_status = contactToSendToOtherSystems.A_Level__c;
        secondary_school.a_level_id = contactToSendToOtherSystems.A_Level_Id__c;
        secondary_school.a_level_issue_date = dateToString(contactToSendToOtherSystems.A_Level_Issue_Date__c);
        secondary_school.examination_committee_name = contactToSendToOtherSystems.Regional_Examination_Commission__c;
        secondary_school.certificate_city = contactToSendToOtherSystems.School_Certificate_Town__c;
        secondary_school.certificate_country = contactToSendToOtherSystems.School_Certificate_Country__c;
        secondary_school.graduation_year = contactToSendToOtherSystems.Year_of_Graduation__c;

        return secondary_school;
    }

    public static List<Career_JSON> prepareCareer(Contact candidate) {
        List<Career_JSON> careers = new List<Career_JSON>();

        if (candidate != null) {
            for (Qualification__c workExperience : candidate.WorkExperience__r) {
                Career_JSON career = new Career_JSON();
                career.date_end = dateToString(workExperience.Employed_Till__c);
                career.date_start = dateToString(workExperience.Employed_From__c);
                career.company_name = workExperience.Company_Name__c;
                career.position = workExperience.Position__c;

                AddressCareer_JSON address = new AddressCareer_JSON();
                address.city = workExperience.City_of_Employment__c;
                address.country = workExperience.Country_of_Employment__c;
                address.postal_code = workExperience.Postal_Code__c;
                address.street = workExperience.Street_of_Employment__c;
                career.address = address;

                careers.add(career);
            }
        }

        return careers;
    }

    public static List<Phone_JSON> phonesToList(String phoneNumber, String mobilePhone) {
        List<Phone_JSON> phones = new List<Phone_JSON>();

        if (phoneNumber != null && phoneNumber != '') {
            Phone_JSON phone = new Phone_JSON();
            phone.type = 'Default';
            phone.phone_number = phoneNumber;        
            phones.add(phone);
        }

        if (mobilePhone != null && mobilePhone != '') {
            Phone_JSON cellPhone = new Phone_JSON();
            cellPhone.type = 'Mobile';
            cellPhone.phone_number = mobilePhone;        
            phones.add(cellPhone);
        }

        return phones;        
    }

    public static List<String> emailsToList(String email, String additionalEmails, Boolean addPrimaryEmail) {
        List<String> emails = new List<String>();
        if (additionalEmails != null && additionalEmails != '' ) {
            emails = additionalEmails.split('; ');
        }
        if (addPrimaryEmail) emails.add(email);

        return emails;
    }

    /* Method used to retrieve valid consents for iPresso */
    public static List<Consent_JSON> prepareConsentsForContact(Contact contactWithConsent, List<Catalog__c> catalogConsentsToValidate) {
        List<String> consentNames = new List<String>{ RecordVals.MARKETING_CONSENT_1, RecordVals.MARKETING_CONSENT_2, 
                                                      RecordVals.MARKETING_CONSENT_4, RecordVals.MARKETING_CONSENT_5};

        List<Consent_JSON> consentsForContact = new List<Consent_JSON>();

        for (Catalog__c catalogConsent : catalogConsentsToValidate) {
        	for (String consentName : consentNames) {
	        	if (catalogConsent.Name.contains(consentName)) {
		            if (contactWithConsent != null && CatalogManager.getApiADONameFromConsentName(catalogConsent.Name) != null 
		                && contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)) != null) {                
		                Set<String> contactAcceptedADO = CommonUtility.getOptionsFromMultiSelect((String)contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)));
		                Set<String> catalogSelectedADO = CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c);
		                
		                consentsForContact.add(new Consent_JSON(catalogConsent.Name, contactAcceptedADO.containsAll(catalogSelectedADO))); 
		            }     
		            else {
		                consentsForContact.add(new Consent_JSON(catalogConsent.Name, false)); 
		            }  
		        }
		    }
        }

        return consentsForContact;
    }

    public static List<String> prepareNationalities(String nationalitiesToPrepare) {
        List<String> nationalities = CommonUtility.getOptionsFromMultiSelectToList(nationalitiesToPrepare);
        Map<String, String> nationalityTransaltionMap = DictionaryTranslator.getTranslatedPicklistValues(CommonUtility.LANGUAGE_CODE_PL, Contact.Nationality__c);

        List<String> newNationalities = new List<String>();

        for (String nationality : nationalities) {
            newNationalities.add(nationalityTransaltionMap.get(nationality));
        }

        return newNationalities;
    }

    public static String dateToString(Date dateToConvert) {
        if (dateToConvert != null) {
            return dateToConvert.year() + '-' + dateToConvert.month() + '-' + dateToConvert.day();
        }

        return null;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class Contact_JSON {
    	public String first_name;
    	public String middle_name;
    	public String last_name;
    	public String family_name;
        public String father_name;
        public String mother_name;
        public String gender;
        public String personal_id;
        public String birthdate;
        public String place_of_birth;
        public String country_of_birth;
        public String country_of_origin;
        public List<String> nationality;
    	public List<Address_JSON> addresses;
    	public List<Phone_JSON> phones;
    	public String primary_email;
    	public List<String> additional_emails;
    	public List<Consent_JSON> consents;
        public List<University_JSON> university;
        public SecondarySchool_JSON secondary_school;
        public List<Career_JSON> career;
    }

    public class University_JSON {
        public String name;
        public String city;
        public String type;
        public String major;
        public String graduation_year;
        public Diploma_JSON diploma;
    }

    public class Diploma_JSON {
        public String status;
        public String examination_date;
        public String diploma_number;
        public String issue_country;
        public String issue_date;
        public String title;
    }

    public class SecondarySchool_JSON {
        public String name;
        public String type;
        public String city;
        public String a_level_status;
        public String a_level_id;
        public String a_level_issue_date;
        public String examination_committee_name;
        public String certificate_city;
        public String certificate_country;
        public String graduation_year;
    }

    public class Career_JSON {
        public String company_name;
        public String position;
        public String date_start;
        public String date_end;
        public AddressCareer_JSON address;
    }

    public class AddressCareer_JSON {
        public String street;
        public String city;
        public String postal_code;
        public String country;
    }

    public class Address_JSON {
        public String type;
        public String country;
        public String postal_code;
        public String city;
        public String street;
    }

    public class Phone_JSON {
        public String type;
        public String phone_number;
    }
    
    public class Consent_JSON {
        public String type;
        public Boolean value;

        public Consent_JSON(String type, Boolean value) {
            this.type = type;
            this.value = value;
        }
    }

}