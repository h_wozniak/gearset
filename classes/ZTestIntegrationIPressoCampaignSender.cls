@isTest
private class  ZTestIntegrationIPressoCampaignSender {


	/* ------------------------------------------------------------------------------------------------ */
	/* -------------------------------------------- TAGS ---------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------ */

	@isTest static void test_sendIPressoContactList() {
		CustomSettingDefault.initDidacticsCampaign();
		Id campaignId = ZDataTestUtilityMethods.IPCS_prepareDataTags();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.IPCS_MassEnrollmentSendCalloutMock());
		Test.startTest();
		Boolean result = IntegrationIPressoCreateSegments.sendIPressoContactList(campaignId);
		Test.stopTest();

		List<Marketing_Campaign__c> iPressoMembersPostUpdate = [
				SELECT Id, Synchronization_Status__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
		];

		for (Marketing_Campaign__c iPressoMemberPostUpdate : iPressoMembersPostUpdate) {
			System.assertEquals(iPressoMemberPostUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_INPROGRESS);
		}

		System.assertEquals(result, true);
	}

	@isTest static void test_prepareIPressoContactListForTags() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_TAGS), true);
		Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

		Test.startTest();
		String result = IntegrationIPressoCreateSegments.prepareIPressoContactListForTags(new List<Marketing_Campaign__c> { campaignMember }, iPressoCampaign);
		Test.stopTest();

		Marketing_Campaign__c iPressoContact = [
				SELECT Id, iPresso_Crireria_Type_Value__c, iPressoId__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
		];

		System.assertEquals(result, '[{"tags":["' + iPressoCampaign.iPresso_Crireria_Type_Value__c + '"],"ipresso_contact_id":' +  iPressoContact.iPressoId__c + '}]');
	}

	@isTest static void test_updateSyncStatus_inProgress() {
		CustomSettingDefault.initDidacticsCampaign();
		Id campaignId = ZDataTestUtilityMethods.IPCS_prepareDataTags();
		Test.startTest();

		List<Marketing_Campaign__c> iPressoMembersPreUpdate = [
				SELECT Id, Synchronization_Status__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
		];

		IntegrationIPressoCreateSegments.updateSyncStatus_inProgress(iPressoMembersPreUpdate);
		Test.stopTest();

		List<Marketing_Campaign__c> iPressoMembersPostUpdate = [
				SELECT Id, Synchronization_Status__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
		];

		for (Marketing_Campaign__c iPressoMemberPreUpdate : iPressoMembersPreUpdate) {
			for (Marketing_Campaign__c iPressoMemberPostUpdate : iPressoMembersPostUpdate) {
				if (iPressoMemberPreUpdate.Id == iPressoMemberPostUpdate.Id) {
					System.assertNotEquals(iPressoMemberPreUpdate.Synchronization_Status__c, iPressoMemberPostUpdate.Synchronization_Status__c);
					System.assertEquals(iPressoMemberPostUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_INPROGRESS);
				}
			}
		}
	}


	/* ------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------ SEGMENTS -------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------ */

	@isTest static void test_prepareIPressoContactListForSegments() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS, iPresso_Crireria_Segment_Lifetime__c = 50), true);
		Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

		Test.startTest();
		String result = IntegrationIPressoCreateSegments.prepareIPressoContactListForSegment(new List<Marketing_Campaign__c> { campaignMember }, iPressoCampaign, null);
		Test.stopTest();

		Marketing_Campaign__c iPressoContact = [
				SELECT Id, iPresso_Crireria_Type_Value__c, iPressoId__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
		];

		System.assertEquals(result, '{"segment_live_time":"' + iPressoCampaign.iPresso_Crireria_Segment_Lifetime__c + '","segment":"' + iPressoCampaign.iPresso_Crireria_Type_Value__c + '","ipresso_contacts_id":[]}');
	}

	@isTest static void test_prepareIPressoContactListForSegments_2() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS, iPresso_Crireria_Segment_Lifetime__c = 50), true);
		Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id, iPressoId__c = '1234'), true);

		Marketing_Campaign__c iPressoContact = [
				SELECT Id, iPresso_Crireria_Type_Value__c, iPressoId__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
		];

		iPressoContact.iPressoId__c = '1234';
		update iPressoContact;

		Test.startTest();
		String result = IntegrationIPressoCreateSegments.prepareIPressoContactListForSegment(new List<Marketing_Campaign__c> { campaignMember }, iPressoCampaign, null);
		Test.stopTest();

		iPressoContact = [
				SELECT Id, iPresso_Crireria_Type_Value__c, iPressoId__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
		];

		System.assertEquals(result, '{"segment_live_time":"' + iPressoCampaign.iPresso_Crireria_Segment_Lifetime__c + '","segment":"' + iPressoCampaign.iPresso_Crireria_Type_Value__c + '","ipresso_contacts_id":["' + iPressoContact.iPressoId__c + '"]}');
	}

	@isTest static void test_sendIPressoContactListSegment() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS, iPresso_Crireria_Segment_Lifetime__c = 50), true);
		Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.IPCS_SendSegmentsMock());
		Test.startTest();
		Boolean result = IntegrationIPressoCreateSegments.sendIPressoContactList(iPressoCampaign.Id);
		Test.stopTest();

		List<Marketing_Campaign__c> iPressoMembersPostUpdate = [
				SELECT Id, Synchronization_Status__c
				FROM Marketing_Campaign__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
		];

		for (Marketing_Campaign__c iPressoMemberPostUpdate : iPressoMembersPostUpdate) {
			System.assertEquals(iPressoMemberPostUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_INPROGRESS);
		}

		System.assertEquals(result, true);
	}
}