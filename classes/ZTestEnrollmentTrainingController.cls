@isTest
private class ZTestEnrollmentTrainingController {       
    //----------------------------------------------------------------------------------
    //EnrollmentTrainingController
    //----------------------------------------------------------------------------------
    private static Map<Integer, sObject> prepareData1() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        Account university = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(University_from_Training__c = university.Id), true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createCandidateContact(new Contact(
            Foreigner__c = false,
            FirstName = 'Jan',
            LastName = 'Kowalski',
            Pesel__c = '89050503473',
            Email = 'email@gmail.com',
            MobilePhone = '123456789',
            MailingStreet = 'Lelewela',
            MailingCity = 'Wrocław',
            MailingPostalCode = '27-200',
            Position__c = 'Inne',
            Department_Business_Area__c = 'IT department'
        ), true));
        retMap.put(3, ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true));

        
        return retMap;
    }
    
    private static Map<Integer, sObject> prepareData2() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(
            Type__c = CommonUtility.OFFER_TYPE_CLOSED
        ), true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Offer_Value__c = 1000,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Company__c = ZDataTestUtility.createCompany(new Account(
                NIP__c = '3672620147'
            ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        
        return retMap;
    }

private static Map<Integer, sObject> prepareData3() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(
            Type__c = CommonUtility.OFFER_TYPE_CLOSED
        ), true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Offer_Value__c = 1000,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Company__c = ZDataTestUtility.createCompany(new Account(
                NIP__c = '3672620147'
            ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        retMap.put(4, ZDataTestUtility.createCompany(new Account(NIP__c = '2619579142'), true));
        retMap.put(5, ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'contactNameTest',
            AccountId = retMap.get(4).Id
        ), true));
        retMap.put(6, ZDataTestUtility.createCandidateContact(new Contact(
            AccountId = retMap.get(4).Id,
            Foreigner__c = false,
            FirstName = 'Jan',
            LastName = 'Kowalski',
            Pesel__c = '89050503473',
            Email = 'email@gmail.com',
            MobilePhone = '123456789',
            MailingStreet = 'Lelewela',
            MailingCity = 'Wrocław',
            MailingPostalCode = '27-200',
            Position__c = 'Inne',
            Department_Business_Area__c = 'IT department'
        ), true));
        
        return retMap;
    }
    
    private static Map<Integer, sObject> prepareData4() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createOpenTrainingEnrollment(null, true));
        retMap.put(1, ZDataTestUtility.createADHOCDiscount(null, true));
        
        return retMap;
    }   

    @isTest static void testTrainingEnrollmentSaved() {
        Map<Integer, sObject> dataMap = prepareData1();
        ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3, Accepted_by_default__c = true), true);
        

        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('scheduleId', dataMap.get(1).Id);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        //accept consents
        for (EnrollmentTrainingController.Consent_Wrapper cw : cntrl.ew.consentList) {
            cw.checked = true;
        }

        Test.startTest();
        cntrl.saveEnrollment();
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [SELECT Id FROM Enrollment__c];
        System.assertNotEquals(0, enrollmentList.size());

        List<Marketing_Campaign__c> mcList = [SELECT Id FROM Marketing_Campaign__c];
        System.assertNotEquals(0, mcList.size());
    }

    @isTest static void testTrainingEnrollmentEdit() {
        Map<Integer, sObject> dataMap = prepareData2();
        

        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        System.assertEquals('com', cntrl.selectedEnrollmentType);


        Contact c = ZDataTestUtility.createCandidateContact(null, true);
        ApexPages.currentPage().getParameters().put('contactId', c.Id);
        cntrl.addParticipant();
        cntrl.addParticipant();


        System.assertEquals(2, cntrl.ew.prList.size());

        ApexPages.currentPage().getParameters().put('index', '0');
        cntrl.removePerson();

        Test.startTest();
        cntrl.saveEnrollment();
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [SELECT Id FROM Enrollment__c];
        System.assertEquals(2, enrollmentList.size());
    }

    @isTest static void testTrainingOther() {
        Map<Integer, sObject> dataMap = prepareData2();
        

        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        cntrl.fillCompanyInfo();

        System.assertEquals('3672620147', cntrl.ew.mainEnrollment.NIP_for_Invoice__c);

        cntrl.enrollmentTypeChanged();

        System.assertEquals(0, cntrl.ew.prList.size());

        Test.startTest();
        cntrl.saveEnrollment();
        Test.stopTest();
    }

    @isTest static void testSetContactPerson() {
        Map<Integer, sObject> dataMap = prepareData3();
        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(6).Id);
        ApexPages.currentPage().getParameters().put('name', String.valueOf(dataMap.get(6).get('FirstName')));
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        Test.startTest();
        cntrl.setContactPerson();
        System.assertEquals(String.valueOf(dataMap.get(6).get('FirstName')), cntrl.contactPersonName);
        Test.stopTest();
    }

    @isTest static void testNextPerson() {
        Map<Integer, sObject> dataMap = prepareData3();
        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(6).Id);
        ApexPages.currentPage().getParameters().put('name', String.valueOf(dataMap.get(6).get('FirstName')));
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        Test.startTest();
        cntrl.nextPerson();
        System.assertEquals(true, cntrl.nextParticipant);
        Test.stopTest();
    }

    @isTest static void testCancel() {
        Map<Integer, sObject> dataMap = prepareData3();
        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(6).Id);
        ApexPages.currentPage().getParameters().put('name', String.valueOf(dataMap.get(6).get('FirstName')));
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        Test.startTest();
        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();
        Test.stopTest();

        System.assertEquals(null, cntrl.cancel());
    }

    @isTest static void testFillContactInfo() {
        Map<Integer, sObject> dataMap = prepareData3();
        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(6).Id);
        ApexPages.currentPage().getParameters().put('name', String.valueOf(dataMap.get(6).get('FirstName')));
        ApexPages.currentPage().getParameters().put('enrollmentId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        Test.startTest();
        cntrl.fillContactInfo();
        Test.stopTest();

        System.assertEquals(cntrl.ew.mainEnrollment.First_Name_for_Invoice__c, null);
    }

    @isTest static void testFillContactInfoWithInvoice() {
        Map<Integer, sObject> dataMap = prepareData1();
        PageReference pageRef = Page.EnrollmentTraining;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('scheduleId', dataMap.get(1).Id);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(2).Id);

        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        cntrl.ew.mainEnrollment.The_Invoice_after_Payment__c = true;

        Test.startTest();
        cntrl.fillContactInfo();
        Test.stopTest();

        System.assertEquals(cntrl.ew.mainEnrollment.First_Name_for_Invoice__c, ((Contact) dataMap.get(2)).FirstName);
    }

    @isTest static void testCreateUpdateMC3IfNeeded() {
        Map<Integer, sObject> dataMap = prepareData1();
        PageReference pageRef = Page.EnrollmentTraining;
        ApexPages.currentPage().getParameters().put('scheduleId', dataMap.get(1).Id);
        ApexPages.currentPage().getParameters().put('candidateId', dataMap.get(2).Id);

        Catalog__c marketingConsent3 = ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_3
        ), true);

        Test.startTest();
        EnrollmentTrainingController cntrl = new EnrollmentTrainingController();

        //accept consents
        for (EnrollmentTrainingController.Consent_Wrapper cw : cntrl.ew.consentList) {
            cw.checked = true;
        }
        cntrl.saveEnrollment();
        Test.stopTest();
    }
    
    @isTest static void testEnrollmentDiscountSaved() {
        Map<Integer, sObject> dataMap = prepareData4();
        

        PageReference pageRef = Page.EnrollmentDiscount;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getparameters().put('enrollment', dataMap.get(0).Id);

        EnrollmentDiscountController cntrl = new EnrollmentDiscountController();
        cntrl.targetParentDiscountId = dataMap.get(1).Id;
        cntrl.fillFieldsWithDiscountValues();
        
        Test.startTest();
        cntrl.saveDiscount();
        Test.stopTest();

        List<Discount__c> discountList = [SELECT Id FROM Discount__c];
        System.assertNotEquals(0, discountList.size());

    }
    
    @isTest static void testEnrollmentPagination() {
        PageReference pageRef = Page.EnrollmentDiscount;
        Test.setCurrentPageReference(pageRef);

        //fake Id
        ApexPages.currentPage().getParameters().put('enrollment', 'a057E000000yEh9');
        
        Id discountId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR);
        List<Discount__c> availableDiscounts = [SELECT Id, Name, RecordType.Name, Discount_Kind__c, Discount_Value__c, Valid_From__c, Valid_To__c 
                                                FROM Discount__c 
                                                WHERE (NOT RecordTypeId = : discountId)
                                                AND Active__c = true
                                                AND (Valid_From__c <= TODAY OR Valid_From__c = null)
                                                AND (Valid_To__c >= TODAY OR Valid_To__c = null)
                                                ORDER BY RecordType.Name];
        
        EnrollmentDiscountController cntrl = new EnrollmentDiscountController();
        List<Discount__c> paginatedList;
        Integer lastGroup;
        System.debug(Integer.valueOf(availableDiscounts.size()/cntrl.groupSize));
        //cntrl.groupNumber = Integer.valueOf(availableDiscounts.size()/cntrl.groupSize);  
        cntrl.firstGroup();
        Test.startTest();
        paginatedList = cntrl.getPaginatedAvailableDiscounts();
        
        System.assertEquals(math.min(availableDiscounts.size(),cntrl.groupSize),paginatedList.size());

        cntrl.previousGroup();

        System.assertEquals(1,cntrl.groupNumber);

        cntrl.lastGroup();
        lastGroup = cntrl.groupNumber;
        cntrl.nextGroup();
        System.assertEquals(lastGroup, cntrl.groupNumber);
        Test.stopTest();
    }

}