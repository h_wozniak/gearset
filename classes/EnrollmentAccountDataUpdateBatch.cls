/**
* @author       Wojciech Słodziak
* @description  Batch class to update Enrollment's University_Contact_Data__c after it changes on Account
**/

global class EnrollmentAccountDataUpdateBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global EnrollmentAccountDataUpdateBatch(Set<Id> accountIds) {
		query = 'SELECT Id, University_Contact_Data__c, Training_Offer__r.Training_Offer_from_Schedule__r.University_from_Training__r.Contact_Data_for_Trainings__c ' +
				'FROM Enrollment__c WHERE Training_Offer__r.Training_Offer_from_Schedule__r.University_from_Training__c IN ' + 
				CommonUtility.listToSOQLString(new List<Id>(accountIds)) + ' AND Training_Offer__c != null';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Enrollment__c> enrollmentList) {
        for (Enrollment__c enr : enrollmentList) {
            enr.University_Contact_Data__c = enr.Training_Offer__r.Training_Offer_from_Schedule__r.University_from_Training__r.Contact_Data_for_Trainings__c;
        }

        try {
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}