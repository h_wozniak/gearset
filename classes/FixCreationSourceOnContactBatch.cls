/**
* @author       Sebastian Łasisz
* @description  Batch class for updating Creation Source on multiple contacts in Database.
**/

/* ------------------------------------------ example --------------------------------------------
Database.executeBatch(
    new FixCreationSourceOnContactBatch(
    	CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD
    ),
    100
);
------------------------------------------- end of example ------------------------------------- */

global class FixCreationSourceOnContactBatch implements Database.Batchable<sObject> {
    String creationSourceType;

    global FixCreationSourceOnContactBatch(String creationSourceType) {
    	this.creationSourceType = creationSourceType;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Creation_Source__c, CreatedDate, Source__c
            FROM Contact
            WHERE Creation_Source__c = null
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) {
    	if (creationSourceType == CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD) {
    		Set<Id> contactId = new Set<Id>();
	        for (Contact obj : scope) {
	        	contactId.add(obj.Id);
	        }

	        List<Marketing_Campaign__c> returnedLeads = [
	        	SELECT Id, CreatedDate, Contact_From_Returning_Leads__c
	        	FROM Marketing_Campaign__c
	        	WHERE Contact_From_Returning_Leads__c IN :contactId
	        	AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
	        ];
    		
    		List<Contact> contactsToUpdate = new List<Contact>();
    		for (Contact obj : scope) {
    			for (Marketing_Campaign__c returnedLead : returnedLeads) {
    				if (obj.Id == returnedLead.Contact_From_Returning_Leads__c
    					&& (returnedLead.CreatedDate.getTime() - obj.CreatedDate.getTime() <= 60000)) {
    					obj.Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD;
    					contactsToUpdate.add(obj);
    				}
    			}
    		}

	        Database.update(contactsToUpdate, false);
    	}
    	else if (creationSourceType == CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI) {
    		Set<Id> contactId = new Set<Id>();
	        for (Contact obj : scope) {
	        	contactId.add(obj.Id);
	        }

	        List<Enrollment__c> studyEnrollments = [
	        	SELECT Id, CreatedDate, Candidate_Student__c
	        	FROM Enrollment__c
	        	WHERE Candidate_Student__c IN :contactId
	        	AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
	        	AND Enrollment_Source__c = :CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI
	        ];
    		
    		List<Contact> contactsToUpdate = new List<Contact>();
    		for (Contact obj : scope) {
    			for (Enrollment__c studyEnrollment : studyEnrollments) {
    				if (obj.Id == studyEnrollment.Candidate_Student__c && (studyEnrollment.CreatedDate.getTime() - obj.CreatedDate.getTime() <= 60000)) {
    					obj.Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI;
    					contactsToUpdate.add(obj);
    				}
    			}
    		}

	        Database.update(contactsToUpdate, false);
    	}
    	else if (creationSourceType == CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO) {
    		List<Contact> contactsToUpdate = new List<Contact>();
	        for (Contact obj : scope) {
	        	if (obj.Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO) {
	        		obj.Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO;
	        		contactsToUpdate.add(obj);
	        	}
	        }

	        Database.update(contactsToUpdate, false);
    	}
    	else {
    		List<Contact> contactsToUpdate = new List<Contact>();
	        for (Contact obj : scope) {
	        	if (obj.Creation_Source__c == null) {
	        		obj.Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_CRM;
	        		contactsToUpdate.add(obj);
	        	}
	        }

	        Database.update(contactsToUpdate, false);
    	}
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}