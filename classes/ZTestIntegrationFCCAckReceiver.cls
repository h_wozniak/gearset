@isTest
global class ZTestIntegrationFCCAckReceiver {
    
    @isTest static void test_receiveFCCInformation() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        List<IntegrationFCCAckReceiver.FCCAckReceiver_JSON> receivers = new List<IntegrationFCCAckReceiver.FCCAckReceiver_JSON>();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        IntegrationFCCAckReceiver.RecordIds_JSON recordId = new IntegrationFCCAckReceiver.RecordIds_JSON();
        recordId.crm_contact_id = candidate.Id;
        recordId.ext_contact_id = '5';

        Marketing_Campaign__c externalCampaign = ZDataTestUtility.createExtCampaign(null, true);
        Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaign(new Marketing_Campaign__c(External_campaign__c = externalCampaign.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
        	new Marketing_Campaign__c(Campaign_Member__c = campaign.Id, Campaign_Contact__c = candidate.Id
        ), true);

        IntegrationFCCAckReceiver.FCCAckReceiver_JSON receiver = new IntegrationFCCAckReceiver.FCCAckReceiver_JSON();
        receiver.contacts = new List<IntegrationFCCAckReceiver.RecordIds_JSON>();
        receiver.contacts.add(recordId);
        receiver.campaign_id = campaign.Id;
        receiver.ext_campaign_id = externalCampaign.External_Id__c;

        receivers.add(receiver);

        req.requestBody = Blob.valueOf(JSON.serialize(receivers));

        Test.startTest();
        IntegrationFCCAckReceiver.receiveFCCInformation();
        Test.stopTest();

        
        System.assertEquals(200, res.statusCode);
    }

}