@isTest
private class ZTestGenerateReferenceNumber {
    
    @isTest static void testGenerateReferenceNumber() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id,
            Reference_Number__c = 1
        ), true);
        
        Decimal referenceNumber = documentEnrollment.Reference_Number__c;
        
        Test.startTest();
        String result = DocGen_GenerateDocumentReferenceNumber.executeMethod(documentEnrollment.Id, null, null);
        Test.stopTest();
        
        System.assertNotEquals(result, '');
    }
    
    @isTest static void testGenerateReferenceNumberFromCustomSettings() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I
        ), true);
        
        Test.startTest();
        String result = DocGen_GenerateDocumentReferenceNumber.executeMethod(studyEnrollment.Id, null, null);
        Test.stopTest();
        
        CustomSettingDefault.initDocumentReferenceNumber();
        Decimal referenceNumber = CustomSettingManager.getCurrentDocumentReferenceNumber();
        
        System.assertNotEquals(result, '');
    }
}