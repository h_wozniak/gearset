/**
*   @author         Radosław Urbański
*   @description    This class is used check that Contact exist in CRM for given parameters.
**/

@RestResource(urlMapping = '/check-crm-id/*')
global without sharing class IntegrationContactCheck {

	/* ------------------------------------------------------------------------------------------------ */
	/* ---------------------------------------- HTTP METHODS ------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------ */

	@HttpPOST
	global static void checkContact() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String jsonBody = req.requestBody.toString();

		IntegrationManager.ContactRequest_JSON parsedJson;

		try {
			parsedJson = parse(jsonBody);
		} catch (Exception ex) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
		}

		if (parsedJson != null) {
			try {
				res.statusCode = 400;
				if (String.isEmpty(parsedJson.first_name)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'first_name');
				} else if (String.isEmpty(parsedJson.last_name)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'last_name');
				} else if (String.isEmpty(parsedJson.email)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'email');
				} else {
					List<Contact> cntList = [SELECT Id 
						FROM Contact 
						WHERE FirstName = :parsedJson.first_name AND LastName = :parsedJson.last_name AND (Email = :parsedJson.email OR Additional_Emails__c LIKE :parsedJson.email) LIMIT 1];
					if (cntList.size() > 0) {
						res.statusCode = 200;
						IntegrationManager.ContactResponse_JSON resp = new IntegrationManager.ContactResponse_JSON(cntList.get(0).Id);
						res.responseBody = Blob.valueOf(JSON.serialize(resp));
					} else {
						res.responseBody = IntegrationError.getErrorJSONBlobByCode(1200);
					}
				}
			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
			}
		}
	}

	private static IntegrationManager.ContactRequest_JSON parse(String jsonBody) {
		return (IntegrationManager.ContactRequest_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.ContactRequest_JSON.class);
	}

}