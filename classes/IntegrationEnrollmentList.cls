/**
*   @author         Sebastian Łasisz
*   @description    This class is used to retrieve all enrollments for given Candidate.
**/

@RestResource(urlMapping = '/enrollments/*')
global without sharing class IntegrationEnrollmentList {


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HTTP METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */


    @HttpGet
    global static void getEnrollmentsForContact() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String personId = RestContext.request.params.get('person_id');

        if (personId == null || personId == '') {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(605);
            return ;
        }

        List<Enrollment__c> studyEnrollments = [
        	SELECT Id, Course_or_Specialty_Offer__r.Name, Course_or_Specialty_Offer__r.Trade_Name__c, Status__c, CreatedDate, Enrollment_Id_ZPI__c
        	FROM Enrollment__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        	AND Candidate_Student__c = :personId
        ];

        try {
	        List<Enrollment_JSON> enrollmentJSON = new List<Enrollment_JSON>();
	        for (Enrollment__c studyEnrollment : studyEnrollments) {
	        	enrollmentJSON.add(
	        		new Enrollment_JSON(
	        			studyEnrollment.Id, 
                        studyEnrollment.Enrollment_Id_ZPI__c, 
                        String.valueOf(studyEnrollment.CreatedDate), 
                        studyEnrollment.Status__c, 
                        studyEnrollment.Course_or_Specialty_Offer__r.Trade_Name__c, 
                        studyEnrollment.Course_or_Specialty_Offer__r.Name
	        		)
	        	);
	        }

            res.responseBody = Blob.valueOf(JSON.serialize(enrollmentJSON));
            res.statusCode = 200;
        }
        catch (Exception ex) {
        	System.debug(ex);
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;    
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    global class Enrollment_JSON {
    	public String enrollment_id;
        public String enrollment_id_zpi;
    	public String enrollment_create_date;
    	public String enrollment_status;
    	public String product_name;
        public String product_code;

    	public Enrollment_JSON(String enrollment_id, String enrollment_id_zpi, String enrollment_create_date, String enrollment_status, String product_name, String product_code) {
    		this.enrollment_id = enrollment_id;
            this.enrollment_id_zpi = enrollment_id_zpi;
    		this.enrollment_create_date = enrollment_create_date;
    		this.enrollment_status = enrollment_status;
    		this.product_name = product_name;
            this.product_code = product_code;
    	}
    }

}