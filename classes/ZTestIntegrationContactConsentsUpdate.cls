@isTest
global class ZTestIntegrationContactConsentsUpdate {

    @isTest static void test_integration_noContact() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        String body = '[{"value":false,"consent_name":"Komunikacja bezpośrednia Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Gdańsk","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Toruń","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Wrocław","consent_date":"2017-08-31 06:33:36"},{"value":false,"consent_name":"Komunikacja kanałami elektronicznymi WSB Poznań","consent_date":"2017-09-25 12:08:31"},{"value":false,"consent_name":"Śledzenie losów absolwentów Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Wrocław","consent_date":"2017-08-31 08:33:36"}]';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('person_id', null);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsUpdate.receiveConsents();

        System.assertEquals(400, res.statusCode);
    }

    @isTest static void test_integration_malformedJSON() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        String body = '["value":false,"consent_name":"Komunikacja bezpośrednia Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Gdańsk","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Toruń","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Wrocław","consent_date":"2017-08-31 06:33:36"},{"value":false,"consent_name":"Komunikacja kanałami elektronicznymi WSB Poznań","consent_date":"2017-09-25 12:08:31"},{"value":false,"consent_name":"Śledzenie losów absolwentów Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Wrocław","consent_date":"2017-08-31 08:33:36"}]';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsUpdate.receiveConsents();

        System.assertEquals(400, res.statusCode);
    }

    @isTest static void test_integration() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        String body = '[{"value":false,"consent_name":"Komunikacja bezpośrednia Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Gdańsk","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Toruń","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Wrocław","consent_date":"2017-08-31 06:33:36"},{"value":false,"consent_name":"Komunikacja kanałami elektronicznymi WSB Poznań","consent_date":"2017-09-25 12:08:31"},{"value":false,"consent_name":"Śledzenie losów absolwentów Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Wrocław","consent_date":"2017-08-31 08:33:36"}]';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsUpdate.receiveConsents();

        System.assertEquals(200, res.statusCode);
    }

    @isTest static void test_integration_withDefinedCatalog() {
    	ZDataTestUtility.prepareCatalogData(true, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        String body = '[{"value":false,"consent_name":"Komunikacja bezpośrednia Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Gdańsk","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Toruń","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Wrocław","consent_date":"2017-08-31 06:33:36"},{"value":false,"consent_name":"Komunikacja kanałami elektronicznymi WSB Poznań","consent_date":"2017-09-25 12:08:31"},{"value":false,"consent_name":"Śledzenie losów absolwentów Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Wrocław","consent_date":"2017-08-31 08:33:36"}]';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsUpdate.receiveConsents();

        System.assertEquals(200, res.statusCode);
    }

    @isTest static void test_integration_withConsents() {
    	ZDataTestUtility.prepareCatalogData(true, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        String body = '[{"value":false,"consent_name":"Komunikacja bezpośrednia","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Komunikacja bezpośrednia WSB Wrocław","consent_date":"2017-08-31 08:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Gdańsk","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi","consent_date":"2017-08-31 06:33:36"},{"value":true,"consent_name":"Komunikacja kanałami elektronicznymi WSB Wrocław","consent_date":"2017-08-31 06:33:36"},{"value":false,"consent_name":"Komunikacja kanałami elektronicznymi WSB Poznań","consent_date":"2017-09-25 12:08:31"},{"value":false,"consent_name":"Śledzenie losów absolwentów","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Śledzenie losów absolwentów","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing Globalna","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Gdańsk","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Poznań","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing WSB Toruń","consent_date":"2017-08-31 08:33:36"},{"value":false,"consent_name":"Marketing","consent_date":"2017-08-31 08:33:36"}]';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsUpdate.receiveConsents();

        System.assertEquals(200, res.statusCode);
    }
}