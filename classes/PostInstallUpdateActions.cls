public without sharing class PostInstallUpdateActions implements Database.Batchable<SObject>, InstallHandler {
    
    List<Integer> emailNumber = new List<Integer>{ null, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    public void onInstall(InstallContext ic) {
        Database.executeBatch(this);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, Email from Contact]);
    }

    public void execute(Database.BatchableContext bc, Sobject[] records) {

        try {
            for (Integer i = 0; i < records.size(); i++) {
                Contact contactToUpdate = (Contact) records[i];
                Integer rand = Integer.valueOf(Math.floor(Math.random() * 10));

                String emailPart = String.valueOf(emailNumber[rand]);

                if (emailPart == null) {
                    emailpart = '';
                }

                contactToUpdate.Email = 'juna' +  emailpart + '@teb-akademia.pl';
            }

            CommonUtility.skipContactUpdateInOtherSystems = true;
            update records;
            CommonUtility.skipContactUpdateInOtherSystems = false;
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public void finish(Database.BatchableContext bc) { }
}