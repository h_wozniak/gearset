/**
*   @author         Grzegorz Długosz, Sebastian Łasisz
*   @description    schedulable class that runs every 1 hour and imports history of interaction ( calls ) from FCC.
**/

/**
*   To initiate the ScheduleIntegrationFCCInteraction execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once every 1hour ---
*   System.schedule('ScheduleIntegrationFCCInteraction', '0 0 * * * ? *', new ScheduleIntegrationFCCInteraction());
**/

global class ScheduleIntegrationFCCInteraction  implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new IntegrationFCCInteractionBatch(), 20);
    }
}