/**
* @author       Sebastian Łasisz
* @description
**/
public with sharing class MarketingNewButtonOverride {

    public List<SelectOption> recordTypeList {get; set;}
    public String selectedRecordType {get; set;}
    public Map<Integer, WrappedRecordTypes> availableRecordTypesTable {get; set;}

    public MarketingNewButtonOverride(ApexPages.StandardController controller) {
        recordTypeList = getRecordTypes();
    }

    public List<SelectOption> getRecordTypes() {
        List<SelectOption> result = new List<SelectOption>();
        List<Id> recordTypesIdList = new List<Id>();

        recordTypesIdList.add(CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN));
        recordTypesIdList.add(CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES));
        recordTypesIdList.add(CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_IPRESSO_SALES));
        recordTypesIdList.add(CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS));
        recordTypesIdList.add(CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_INFORMATOR_ACTIVITY));

        Set<Id> availableForUserRTIds = CommonUtility.getAvailableRecordTypeIdsForSObject(Schema.SObjectType.Marketing_Campaign__c);
        List<RecordType> possibleRecordTypes = [
            SELECT Id, toLabel(Name), Description 
            FROM RecordType 
            WHERE Id in :recordTypesIdList 
            ORDER BY Id
        ];
        
        availableRecordTypesTable = new Map<Integer, WrappedRecordTypes>();
        Integer index = 0;
        for(RecordType rt : possibleRecordTypes) {
            if (availableForUserRTIds.contains(rt.Id)) {
                result.add(new SelectOption(rt.Id, rt.Name));
                WrappedRecordTypes wrt = new WrappedRecordTypes();
                wrt.name = rt.Name;
                wrt.description = rt.description;
                availableRecordTypesTable.put(index, wrt);
                index++;
            }
        }   

        return result;
    }

    public PageReference continueStep() {
        String prefix = CommonUtility.getPrefix('Marketing_Campaign__c');
        String url = '/' + prefix + '/e?RecordType=' + selectedRecordType + '&retURL=/' + prefix + '/o&nooverride=1';
        
        return new PageReference(url);
    }

    public class WrappedRecordTypes {
        public String name { get; set; }
        public String description { get; set; }
    }

}