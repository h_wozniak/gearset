/**
 * Created by martyna.stanczuk on 2018-07-13.
 */

public with sharing class LEX_MassResignation {
    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }

    @AuraEnabled
    public static Boolean resignParticipants(List<Id> recordIds) {

        List<Enrollment__c> participantsToUpdate = new List<Enrollment__c>();
        for (Id participantId : recordIds) {
            Enrollment__c participant = new Enrollment__c(Id=participantId);
                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
                participantsToUpdate.add(participant);
        }
        try {
            if (!participantsToUpdate.isEmpty()) {
                update participantsToUpdate;
            }
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }
}