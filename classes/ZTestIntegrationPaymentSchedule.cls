@isTest
global class ZTestIntegrationPaymentSchedule {


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ TEST DATA ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */


    private static Map<Integer, sObject> prepareDataForTest(Decimal instAmount, String installments) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        CommonUtility.preventEditingDiscounts = true;

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        //config PriceBook
        configPriceBook(retMap.get(1).Id, instAmount);
        PriceBookManager.activatePriceBooks(new List<Id> { retMap.get(1).Id });

        //create enrollment
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Status__c = CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = installments
        ), true));

        ZDataTestUtility.createPaymentOnEnrollment(new Payment__c(Enrollment_from_Schedule_Installment__c = retMap.get(2).Id, Installment_Year_Calendar__c = '1'), true);

        retMap.put(3, ZDataTestUtility.createManualDiscount(new Discount__c(
                Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_ENTRY,
                Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
                Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
                Enrollment__c = retMap.get(2).Id,
                Discount_Value__c = 50,
                Applied__c = true
        ), true));

        retMap.put(4, ZDataTestUtility.createManualDiscount(new Discount__c(
                Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
                Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY,
                Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE,
                Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_DISCRETIONARY,
                Discount_Value__c = 20,
                Enrollment__c = retMap.get(2).Id,
                Applied__c = true
        ), true));

        retMap.put(5, ZDataTestUtility.createEducationalAgreement(new Enrollment__c(
                Source_Enrollment__c = retMap.get(2).Id
        ), true));

        Enrollment__c studyEnr = (Enrollment__c) retMap.get(2);
        studyEnr.Educational_Agreement__c = retMap.get(5).Id;
        update studyEnr;

        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- TEST METHODS - SENDING -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_buildPaymentSchedulesJSON() {
        Test.startTest();
        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');
        Test.stopTest();

        Enrollment__c studyEnr = [
                SELECT Id, (SELECT Id, PriceBook_Value__c, Installment_Year_Calendar__c, Installment_Semester__c, Installment_Variant__c, Type__c, Payment_Deadline__c,
                        Schedule_Model__c, Description__c
                FROM ScheduleInstallments__r)
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assertNotEquals(studyEnr.ScheduleInstallments__r.size(), 0);

        String generatedJSON = IntegrationPaymentScheduleHelper.buildPaymentSchedulesJSON(new Set<Id> { dataMap.get(2).Id }, false);


        System.assertNotEquals(null, generatedJSON);

        // enr id
        System.assert(generatedJSON.contains(dataMap.get(5).Id));

        // operations
        System.assert(generatedJSON.contains('"operations"'));

        // installments
        System.assert(generatedJSON.contains('"installments"'));

        // discount definitions
        System.assert(generatedJSON.contains(dataMap.get(3).Id));
        System.assert(generatedJSON.contains(dataMap.get(4).Id));
    }

    @isTest static void test_sendPaymentSchedulesAsync() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock());

        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        Test.startTest();
        IntegrationPaymentScheduleSender.sendPaymentSchedulesAsync(new Set<Id> { dataMap.get(2).Id }, false);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, studyEnr.Synchronization_Status__c);
    }

    @isTest static void test_sendPaymentSchedulesSync_btn() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock());

        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        Test.startTest();
        String jsonResult = StudyWebservices.transferPaymentsToKS(new List<Id> { dataMap.get(2).Id });

        WebserviceUtilities.WS_Response result = (WebserviceUtilities.WS_Response) JSON.deserialize(jsonResult, WebserviceUtilities.WS_Response.class);
        System.assert(result.result);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, studyEnr.Synchronization_Status__c);
    }

    @isTest static void test_sendPaymentSchedulesSync_btn_OneInstallmentPerYear() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock());

        Map<Integer, sObject> dataMap = prepareDataForTest(100, '1');

        Test.startTest();
        String jsonResult = StudyWebservices.transferPaymentsToKS(new List<Id> { dataMap.get(2).Id });

        WebserviceUtilities.WS_Response result = (WebserviceUtilities.WS_Response) JSON.deserialize(jsonResult, WebserviceUtilities.WS_Response.class);
        System.assert(result.result);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, studyEnr.Synchronization_Status__c);
    }

    @isTest static void test_sendPaymentSchedulesSync_btn_fail() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock_Fail());

        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        Test.startTest();
        String jsonResult = StudyWebservices.transferPaymentsToKS(new List<Id> { dataMap.get(2).Id });

        WebserviceUtilities.WS_Response result = (WebserviceUtilities.WS_Response) JSON.deserialize(jsonResult, WebserviceUtilities.WS_Response.class);
        System.assert(result.result);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, studyEnr.Synchronization_Status__c);
    }

    @isTest static void test_sendPaymentSchedulesSync_function_fail() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock_Fail());

        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        Test.startTest();
        Boolean result = IntegrationPaymentScheduleSender.sendPaymentScheduleFinal(new Set<Id> { dataMap.get(2).Id }, false);

        System.assert(!result);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_FAILED, studyEnr.Synchronization_Status__c);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- TEST METHODS - RECEIVING ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_receivePaymentScheduleAcks_validJSON_success() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(5).Id;
        respBodyJSON.success = true;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(respBodyJSON));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentScheduleAckReceiver.receivePaymentScheduleAcks();

        System.assertEquals(200, RestContext.response.statusCode);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c, Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_FINISHED, studyEnr.Synchronization_Status__c);
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS, studyEnr.Status__c);
    }

    @isTest static void test_receivePaymentScheduleAcks_validJSON_failure() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(5).Id;
        respBodyJSON.success = false;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(respBodyJSON));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentScheduleAckReceiver.receivePaymentScheduleAcks();

        System.assertEquals(200, RestContext.response.statusCode);

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_FAILED, studyEnr.Synchronization_Status__c);
    }

    @isTest static void test_receivePaymentScheduleAcks_invalidJSON() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100, '2');

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(5).Id;
        respBodyJSON.success = true;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize('stringPartToDamageJSON' + respBodyJSON + 'stringPartToDamageJSON'));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentScheduleAckReceiver.receivePaymentScheduleAcks();

        System.assertEquals(400, RestContext.response.statusCode);

        System.assert(RestContext.response.responseBody.toString().contains(IntegrationError.errorCodeToMsgMap.get(601)));

        Enrollment__c studyEnr = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];
        Test.stopTest();

        System.assertEquals(CommonUtility.SYNCSTATUS_NEW, studyEnr.Synchronization_Status__c);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class IntegrationPaymentSchedulesMock implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

            String createPaymentEndPoint = esb.Create_Payment_Schedules_End_Point__c;
            String esbIp = esb.Service_Url__c;

            System.assertEquals(esbIp + createPaymentEndPoint, req.getEndpoint());
            System.assertEquals(IntegrationManager.HTTP_METHOD_POST, req.getMethod());

            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setBody('{"error_code":202,"invalid_records":null}');
            res.setStatusCode(200);

            return res;
        }
    }

    global class IntegrationPaymentSchedulesMock_Fail implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

            String createPaymentEndPoint = esb.Create_Payment_Schedules_End_Point__c;
            String esbIp = esb.Service_Url__c;

            System.assertEquals(esbIp + createPaymentEndPoint, req.getEndpoint());
            System.assertEquals(IntegrationManager.HTTP_METHOD_POST, req.getMethod());

            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setStatusCode(500);

            return res;
        }
    }

}