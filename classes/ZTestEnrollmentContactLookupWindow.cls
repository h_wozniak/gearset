@isTest
private class ZTestEnrollmentContactLookupWindow {
    private static Map<Integer, sObject> prepareData3() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(
                Type__c = CommonUtility.OFFER_TYPE_CLOSED
        ), true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
                Trade_Name__c = 'Obsługa komputera',
                Training_Offer_from_Schedule__c = retMap.get(0).Id,
                Active__c = true,
                Bank_Account_No__c = '123',
                Number_of_Seats__c = 5,
                Offer_Value__c = 1000,
                Valid_From__c = System.today(),
                Valid_To__c = System.today(),
                Start_Time__c = '10:50',
                Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
                Training_Place__c = 'Hotel WSB',
                City__c = 'Wrocław',
                Street__c = 'Warszawska 10',
                Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
                Training_Offer__c = retMap.get(1).Id,
                Company__c = ZDataTestUtility.createCompany(new Account(
                        NIP__c = '3672620147'
                ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
                Enrollment_Training_Participant__c = retMap.get(2).Id,
                Training_Offer_for_Participant__c = retMap.get(1).Id,
                Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        retMap.put(4, ZDataTestUtility.createCompany(new Account(NIP__c = '2619579142'), true));
        retMap.put(5, ZDataTestUtility.createCandidateContact(new Contact(
                FirstName = 'contactNameTest',
                AccountId = retMap.get(4).Id
        ), true));
        retMap.put(6, ZDataTestUtility.createCandidateContact(new Contact(
                AccountId = retMap.get(4).Id,
                Foreigner__c = false,
                FirstName = 'Jan',
                LastName = 'Kowalski',
                Pesel__c = '89050503473',
                Email = 'email@gmail.com',
                MobilePhone = '123456789',
                MailingStreet = 'Lelewela',
                MailingCity = 'Wrocław',
                MailingPostalCode = '27-200',
                Position__c = 'Inne',
                Department_Business_Area__c = 'IT department'
        ), true));

        return retMap;
    }

    private static Map<Integer, sObject> prepareData4() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCompany(new Account(NIP__c = '2619579142'), true));
        retMap.put(1, ZDataTestUtility.createCandidateContact(new Contact(
                FirstName = 'contactNameTest',
                AccountId = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createCandidateContact(new Contact(
                AccountId = ZDataTestUtility.createCompany(new Account(
                        ParentId = retMap.get(0).Id,
                        NIP__c = '3672620147'
                ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createCandidateContact(new Contact(
                AccountId = retMap.get(0).Id,
                Foreigner__c = false,
                FirstName = 'Jan',
                LastName = 'Kowalski',
                Pesel__c = '89050503473',
                Email = 'email@gmail.com',
                MobilePhone = '123456789',
                MailingStreet = 'Lelewela',
                MailingCity = 'Wrocław',
                MailingPostalCode = '27-200',
                Position__c = 'Inne',
                Department_Business_Area__c = 'IT department'
        ), true));

        return retMap;
    }

    @isTest static void testEnrollmentContactLookupWindow() {
        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        //prep data
        Account parentCompany = ZDataTestUtility.createCompany(new Account(NIP__c = '2619579142'), true);
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
                FirstName = 'contactNameTest',
                AccountId = parentCompany.Id
        ), true);
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
                AccountId = ZDataTestUtility.createCompany(new Account(
                        ParentId = parentCompany.Id,
                        NIP__c = '3672620147'
                ), true).Id
        ), true);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', parentCompany.Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        Test.stopTest();

        System.assertEquals(2, cntrl.contactList.size());

        cntrl.searchTxt = 'wrongName';
        cntrl.searchLookup();

        System.assertEquals(0, cntrl.contactList.size());

        cntrl.searchTxt = 'NameTe';
        cntrl.searchLookup();

        System.assertEquals(1, cntrl.contactList.size());


        cntrl.changeNewMode();

        cntrl.newRecord();

        cntrl.newContact.FirstName = 'firstName';
        cntrl.newContact.LastName = 'lastName';
        cntrl.newContact.Phone = '999888777';

        cntrl.insertContact();

        List<Contact> contactList = [SELECT Id FROM Contact];

        System.assertEquals(3, contactList.size()); //needed more fields to be filled
    }

    @isTest static void testEnrollmentContactLookupWindowCheckContact() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        cntrl.idMatch = dataMap.get(3).Id;
        cntrl.CheckContact();
        System.assertEquals(true, cntrl.isComplete);
        Test.stopTest();
    }

    @isTest static void testEnrollmentContactLookupCheckContact() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        cntrl.idMatch = dataMap.get(2).Id;
        cntrl.CheckContact();
        System.assertEquals(false, cntrl.isComplete);
        System.assertEquals(true, cntrl.editMode);
        Test.stopTest();
    }

    @isTest static void testEnrollmentContactLookupCancelEdit() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        cntrl.cancelEdit();
        System.assertEquals(false, cntrl.editMode);
        Test.stopTest();
    }

    @isTest static void testEnrollmentContactLookupUpdateContact() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        Test.stopTest();

        cntrl.editContact = (Contact)dataMap.get(2);
        cntrl.updateContact();
        System.assertEquals(false, cntrl.isComplete);

        cntrl.editContact = (Contact)dataMap.get(3);
        cntrl.newContact = (Contact)dataMap.get(3);
        cntrl.updateContact();
        System.assertEquals(dataMap.get(3).Id, cntrl.idMatch);
        System.assertEquals(true, cntrl.isComplete);
    }

    @isTest static void testEnrollmentContactLookupEditContact() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'participant');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        cntrl.editContact = (Contact)dataMap.get(3);
        cntrl.updateContact();
        System.assertEquals(dataMap.get(3).Id, cntrl.idMatch);
        System.assertEquals(true, cntrl.isComplete);
        Test.stopTest();
    }

    @isTest static void testEnrollmentContactLookupInsertContact() {
        Map<Integer, sObject> dataMap = prepareData3();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'contactPerson');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(4).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        Test.stopTest();
        cntrl.newContact = (Contact)dataMap.get(6);
        cntrl.editContact = (Contact)dataMap.get(6);

        cntrl.insertContact();
        System.assertEquals(false, cntrl.isComplete);
    }

    @isTest static void testEnrollmentContactLookupWindow3() {
        Map<Integer, sObject> dataMap = prepareData4();

        PageReference pageRef = Page.EnrollmentContactLookupWindow;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('personType', 'contactPerson');
        ApexPages.CurrentPage().getParameters().put('openTraining', 'true');
        ApexPages.CurrentPage().getParameters().put('company', dataMap.get(0).Id);

        Test.startTest();
        EnrollmentContactLookupWindow cntrl = new EnrollmentContactLookupWindow();
        Test.stopTest();

        cntrl.idMatch = dataMap.get(3).Id;
        cntrl.CheckContact();
        System.assertEquals(true, cntrl.isComplete);

        cntrl.idMatch = dataMap.get(2).Id;
        cntrl.CheckContact();
        System.assertEquals(false, cntrl.isComplete);
        System.assertEquals(true, cntrl.editMode);

        cntrl.cancelEdit();
        System.assertEquals(false, cntrl.editMode);

        cntrl.idMatch = dataMap.get(3).Id;
        cntrl.updateContact();
        System.assertEquals(dataMap.get(3).Id, cntrl.idMatch);
        System.assertEquals(false, cntrl.isComplete);

        cntrl.editContact = (Contact)dataMap.get(3);
        cntrl.updateContact();
        System.assertEquals(dataMap.get(3).Id, cntrl.idMatch);
        System.assertEquals(true, cntrl.isComplete);

        cntrl.idMatch = dataMap.get(1).Id;
        cntrl.CheckContact();
        System.assertEquals(true, cntrl.isComplete);
    }

}