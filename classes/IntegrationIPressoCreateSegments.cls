/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send Marketing Campaign to iPresso
**/

global without sharing class IntegrationIPressoCreateSegments {

    public static Boolean sendIPressoContactList(Id iPressoCampaignId) {
        Marketing_Campaign__c iPressoCampaign = [
                SELECT Id, iPresso_Criteria_Type__c, iPresso_Crireria_Type_Value__c, iPresso_Crireria_Segment_Lifetime__c
                FROM Marketing_Campaign__c
                WHERE Id = :iPressoCampaignId
        ];

        IntegrationIpressoSendCampaignBatch activityBatch = new IntegrationIpressoSendCampaignBatch(iPressoCampaign.Id, iPressoCampaign.iPresso_Criteria_Type__c);
        Database.executebatch(activityBatch, 100);

        return true;
    }

    public static String prepareIPressoContactListForSegment(List<Marketing_Campaign__c> iPressoContacts, Marketing_Campaign__c iPressoCampaign, Decimal segmentId) {
        Set<String> iPressoContactIds = new Set<String>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
            if (iPressoContact.iPressoId__c != null) {
                iPressoContactIds.add(iPressoContact.iPressoId__c);
            }
        }

        List<String> ipressoIds = new List<String>();
        ipressoIds.addAll(iPressoContactIds);

        if (segmentId == null) {
            IPressoSegments iPressoSegmentsToSend = new IPressoSegments(iPressoCampaign.iPresso_Crireria_Type_Value__c, ipressoIds, String.valueOf(iPressoCampaign.iPresso_Crireria_Segment_Lifetime__c));
            return JSON.serialize(iPressoSegmentsToSend);
        }
        else {
            UpdateIPressoSegments iPressoSegmentsToSend = new UpdateIPressoSegments(segmentId, ipressoIds);
            return JSON.serialize(iPressoSegmentsToSend);
        }
    }

    public static String prepareIPressoContactListForTags(List<Marketing_Campaign__c> iPressoContacts, Marketing_Campaign__c iPressoCampaign) {
        List<IPressoTags> iPressoTagsToSend = new List<IPressoTags>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
            if (iPressoCampaign.iPresso_Crireria_Type_Value__c != null) {
                iPressoTagsToSend.add(new IPressoTags(iPressoContact.iPressoId__c, iPressoCampaign.iPresso_Crireria_Type_Value__c.split('; ')));
            }
            else {
                iPressoTagsToSend.add(new IPressoTags(iPressoContact.iPressoId__c, null));
            }
        }

        return JSON.serialize(iPressoTagsToSend);
    }

    public static void updateSyncStatus_inProgress(List<Marketing_Campaign__c> iPressoMembers) {
        for (Marketing_Campaign__c iPressoMember : iPressoMembers) {
            iPressoMember.Synchronization_Status__c = CommonUtility.SYNCSTATUS_INPROGRESS;
        }

        try {
            update iPressoMembers;
        }
        catch (Exception e){
            ErrorLogger.log(e);
        }
    }

    public static void updateSyncStatus_onFinish(List<Marketing_Campaign__c> iPressoMembers, Boolean success) {
        for (Marketing_Campaign__c iPressoMember : iPressoMembers) {
            if (success) {
                iPressoMember.Synchronization_Status__c = CommonUtility.SYNCSTATUS_FINISHED;
            }
            else {
                iPressoMember.Synchronization_Status__c = CommonUtility.SYNCSTATUS_FAILED;
            }
        }

        try {
            update iPressoMembers;
        }
        catch (Exception e){
            ErrorLogger.log(e);
        }
    }

    public static void updateContacts_onFinish(Set<Id> contactIds, String tagValues) {
        List<String> tagList = tagValues.split('; ');

        List<Contact> contacts = [
                SELECT Id, Tags__c
                FROM Contact
                WHERE Id IN :contactIds
        ];

        for (Contact c : contacts) {
            if (c.Tags__c == null) c.Tags__c = '';
            List<String> tagsOnContact = c.Tags__c.split('; ');
            Set<String> setOfTags = new Set<String>();

            if (!tagsOnContact.isEmpty()) {
                setOfTags.addAll(tagsOnContact);
            }

            if (!tagList.isEmpty()) {
                setOfTags.addAll(tagList);
            }

            setOfTags.remove(null);
            setOfTags.remove('');

            String properTags;
            for (String tag : setOfTags) {
                properTags = properTags == null ? tag : properTags + '; ' + tag;
            }

            c.Tags__c = properTags;
        }

        try {
            update contacts;
        }
        catch(Exception e) {
            ErrorLogger.log(e);
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class IPressoTags {
        public String ipresso_contact_id;
        public List<String> tags;

        public IPressoTags(String ipresso_contact_id, List<String> tags) {
            this.ipresso_contact_id = ipresso_contact_id;
            this.tags = tags;
        }
    }

    public class IPressoSegments {
        public String segment;
        public List<String> ipresso_contacts_id;
        public String segment_live_time;

        public IPressoSegments(String segment, List<String> ipresso_contacts_id, String segment_live_time) {
            this.segment = segment;
            this.ipresso_contacts_id = ipresso_contacts_id;
            this.segment_live_time = segment_live_time;
        }
    }

    public class UpdateIPressoSegments {
        public String segment_id;
        public List<String> ipresso_contacts_id;

        public UpdateIPressoSegments(Decimal segment_id, List<String> ipresso_contacts_id) {
            this.segment_id = String.valueOf(segment_id);
            this.ipresso_contacts_id = ipresso_contacts_id;
        }
    }

    public class SegmentResponse_JSON {
        public Integer code;
        public Data_JSON data;
        public String message;
    }

    public class Data_JSON {
        public Integer id;
        public Integer contacts_in_segment;
    }
}