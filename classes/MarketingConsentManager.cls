/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to Marketing_Campaign__c object related to Consents
**/

public without sharing class MarketingConsentManager {

    private static Map<Id, User> userMap;

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- TRIGGER LOGIC ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz */
    // Update contact on enrollment before sharing settings are recalcualted
    public static void updateContactWithoutSharing(Contact contactToUpdate) {
        try {
            update contactToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // Update contact on enrollment before sharing settings are recalcualted
    public static void updateContactWithoutSharingList(List<Contact> contactToUpdate) {
        try {
            update contactToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // Validate whether Candidate can accept consent with given ado
    public static void verifyIfCandidateCanAcceptConsent(List<Contact> contactsToVerifyConsents) {
        for (Contact contact : contactsToVerifyConsents) {
            if ((contact.Consent_Direct_Communications__c && String.isBlank(contact.Consent_Direct_Communications_ADO__c))
                || (contact.Consent_Electronic_Communication__c && String.isBlank(contact.Consent_Electronic_Communication_ADO__c))
                || (contact.Consent_Graduates__c && String.isBlank(contact.Consent_Graduates_ADO__c))
                || (contact.Consent_Marketing__c && String.isBlank(contact.Consent_Marketing_ADO__c))
                || (contact.Consent_PUCA__c && String.isBlank(contact.Consent_PUCA_ADO__c))
            ) {
                contact.addError(Label.msg_error_AcceptedConsentNeedsADO);
            }
        }
    }

    /* Sebastian Łasisz */
    // Validate whether Candidate can deny 'marketing consent 3' (PUCA)
    public static void verifyIfCandidateCanDenyMC3(List<Contact> contactToVerifyMC3) {
        Set<Id> contactsIds = new Set<Id>();
        for (Contact contact : contactToVerifyMC3) {
            contactsIds.add(contact.Id);
        }

        List<Enrollment__c> studyEnrOnContacts = [
            SELECT Id, Status__c, University_Name__c, Candidate_Student__c
            FROM Enrollment__c
            WHERE Candidate_Student__c IN :contactsIds
            AND Status__c != :CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
            AND Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION
        ];

        for (Contact contact : contactToVerifyMC3) {
            for (Enrollment__c studyEnrOnContact : studyEnrOnContacts) {
                if (contact.Id == studyEnrOnContact.Candidate_Student__c) {
                    if (contact.Consent_PUCA_ADO__c == null || !contact.Consent_PUCA_ADO__c.contains(CommonUtility.getMarketingEntityByUniversityName(studyEnrOnContact.University_Name__c))) {
                            contact.addError(Label.msg_error_cantDenyMC3);
                        }
                    }
            }
        }
    }


    /* Sebastian Łasisz */
    // Method used to add missing ado for consent definition
    public static Contact updateConsentOnContact(String entity, Contact contactToAddConsent, Schema.SObjectField sObjField) {
        String value = (String)contactToAddConsent.get(sObjField);

        if (value != null) {
            if (!value.contains(entity)) {
                if (value.length() == 0) {
                    contactToAddConsent.put(sObjField, entity);
                }
                else {
                    contactToAddConsent.put(sObjField, value + ';' + entity);
                }
            }
            else {
                contactToAddConsent.put(sObjField, value);
            }
        }
        else {
            contactToAddConsent.put(sObjField, entity);
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to add missing ado for consent definition (Person Account)
    public static Account updateConsentOnAccount(String entity, Account accountToAddConsent, Schema.SObjectField sObjField) {
        String value = (String)accountToAddConsent.get(sObjField);

        if (value != null) {
            if (!value.contains(entity)) {
                if (value.length() == 0) {
                    accountToAddConsent.put(sObjField, entity);
                }
                else {
                    accountToAddConsent.put(sObjField, value + ';' + entity);
                }
            }
            else {
                accountToAddConsent.put(sObjField, value);
            }
        }
        else {
            accountToAddConsent.put(sObjField, entity);
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // Method used to add missing ado for consent definition
    public static Contact updateConsentOnContact(String entity, Contact contactToAddConsent, String fieldName) {
        String value = (String)contactToAddConsent.get(fieldName);
        
        if (value != null) {
            if (!value.contains(entity)) {
                if (value.length() == 0) {
                    contactToAddConsent.put(fieldName, entity);
                }
                else {
                    contactToAddConsent.put(fieldName, value + ';' + entity);
                }
            }
            else {
                contactToAddConsent.put(fieldName, value);
            }
        }
        else {
            contactToAddConsent.put(fieldName, entity);
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to add missing ado for consent definition (Person Account)
    public static Account updateConsentOnAccount(String entity, Account accountToAddConsent, String fieldName) {
        String value = (String)accountToAddConsent.get(fieldName);
        
        if (value != null) {
            if (!value.contains(entity)) {
                if (value.length() == 0) {
                    accountToAddConsent.put(fieldName, entity);
                }
                else {
                    accountToAddConsent.put(fieldName, value + ';' + entity);
                }
            }
            else {
                accountToAddConsent.put(fieldName, value);
            }
        }
        else {
            accountToAddConsent.put(fieldName, entity);
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // Method used to add missing ado for consent definition
    public static Contact updateConsentOnContact(Set<String> entities, Contact contactToAddConsent, String fieldName) {        
        for (String entity : entities) {
            if (fieldName != null) {
                String value = (String)contactToAddConsent.get(fieldName);
                if (value != null) {
                    if (!value.contains(entity)) {
                        if (value.length() == 0) {
                            contactToAddConsent.put(fieldName, entity);
                        }
                        else {
                            contactToAddConsent.put(fieldName, value + ';' + entity);
                        }
                    }
                    else {
                        contactToAddConsent.put(fieldName, value);
                    }
                }
                else {
                    contactToAddConsent.put(fieldName, entity);
                }
            }
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to add missing ado for consent definition (Person Account)
    public static Account updateConsentOnAccount(Set<String> entities, Account accountToAddConsent, String fieldName) {        
        for (String entity : entities) {
            if (fieldName != null) {
                String value = (String)accountToAddConsent.get(fieldName);
                if (value != null) {
                    if (!value.contains(entity)) {
                        if (value.length() == 0) {
                            accountToAddConsent.put(fieldName, entity);
                        }
                        else {
                            accountToAddConsent.put(fieldName, value + ';' + entity);
                        }
                    }
                    else {
                        accountToAddConsent.put(fieldName, value);
                    }
                }
                else {
                    accountToAddConsent.put(fieldName, entity);
                }
            }
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // Method used to remove given ado for consent definition
    public static Contact removeConsentOnContact(String entity, Contact contactToAddConsent, Schema.SObjectField sObjField) {
        String value = (String)contactToAddConsent.get(sObjField);

        if (value != null && value.contains(entity)) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            valuesAsSet.remove(entity);
            contactToAddConsent.put(sObjField, CommonUtility.getMultiSelectFromSet(valuesAsSet));            
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to remove given ado for consent definition (Person Account)
    public static Account removeConsentOnAccount(String entity, Account accountToAddConsent, Schema.SObjectField sObjField) {
        String value = (String)accountToAddConsent.get(sObjField);

        if (value != null && value.contains(entity)) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            valuesAsSet.remove(entity);
            accountToAddConsent.put(sObjField, CommonUtility.getMultiSelectFromSet(valuesAsSet));            
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // Method used to remove given ado for consent definition
    public static Contact removeConsentOnContact(String entity, Contact contactToAddConsent, String field) {
        String value = (String)contactToAddConsent.get(field);

        if (value != null && value.contains(entity)) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            valuesAsSet.remove(entity);
            contactToAddConsent.put(field, CommonUtility.getMultiSelectFromSet(valuesAsSet));            
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to remove given ado for consent definition (Person Account)
    public static Account removeConsentOnAccount(String entity, Account accountToAddConsent, String field) {
        String value = (String)accountToAddConsent.get(field);

        if (value != null && value.contains(entity)) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            valuesAsSet.remove(entity);
            accountToAddConsent.put(field, CommonUtility.getMultiSelectFromSet(valuesAsSet));            
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // Method used to remove given ado for consent definition
    public static Contact removeConsentOnContact(Set<String> entity, Contact contactToAddConsent, String field) {
        String value = (String)contactToAddConsent.get(field);

        if (value != null) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            if (valuesAsSet.containsAll(entity)) {
                valuesAsSet.removeAll(entity);
                contactToAddConsent.put(field, CommonUtility.getMultiSelectFromSet(valuesAsSet));           
            } 
        }

        return contactToAddConsent;
    }


    /* Sebastian Łasisz, Radosław Urbański */
    // Method used to remove given ado for consent definition (Person Account)
    public static Account removeConsentOnAccount(Set<String> entity, Account accountToAddConsent, String field) {
        String value = (String)accountToAddConsent.get(field);

        if (value != null) {
            Set<String> valuesAsSet = CommonUtility.getOptionsFromMultiSelect(value);
            if (valuesAsSet.containsAll(entity)) {
                valuesAsSet.removeAll(entity);
                accountToAddConsent.put(field, CommonUtility.getMultiSelectFromSet(valuesAsSet));           
            } 
        }

        return accountToAddConsent;
    }


    /* Sebastian Łasisz */
    // On consent changes determine checkbox value
    public static void clearEntitiesOnCheckboxReset(List<Contact> contactsToValidateCheckBoxConsents) {
        Map<Contact, Set<String>> refusedConsents = new Map<Contact, Set<String>>();
        List<Contact> contactsToReavulate = new List<Contact>();

        for (Contact contactToValidateCheckBoxConsents : contactsToValidateCheckBoxConsents) {
            /* Check whether ADO should be cleared */
            if (!contactToValidateCheckBoxConsents.Consent_Direct_Communications__c) {
                contactToValidateCheckBoxConsents.Consent_Direct_Communications_ADO__c = null;
            }
            if (!contactToValidateCheckBoxConsents.Consent_Electronic_Communication__c) {
                contactToValidateCheckBoxConsents.Consent_Electronic_Communication_ADO__c = null;
            }
            if (!contactToValidateCheckBoxConsents.Consent_Graduates__c) {
                contactToValidateCheckBoxConsents.Consent_Graduates_ADO__c = null;
            }
            if (!contactToValidateCheckBoxConsents.Consent_Marketing__c) {
                contactToValidateCheckBoxConsents.Consent_Marketing_ADO__c = null;
            }
            if (!contactToValidateCheckBoxConsents.Consent_PUCA__c) {
                /* Clear all other consents from cleared ADO */
                refusedConsents.put(contactToValidateCheckBoxConsents, CommonUtility.getOptionsFromMultiSelect(contactToValidateCheckBoxConsents.Consent_PUCA_ADO__c));
                contactsToReavulate.add(contactToValidateCheckBoxConsents);
                contactToValidateCheckBoxConsents.Consent_PUCA_ADO__c = null;
            }
        }

        MarketingConsentManager.setAllConsentsToNoWhenMarketingIsSetNo(refusedConsents);
        MarketingConsentManager.disableCheckboxValueBasedOnAdoValues(contactsToReavulate);
    }

    /* Sebastian Łasisz */
    // On consent changes determine checkbox value
    public static void disableCheckboxValueBasedOnAdoValues(List<Contact> contactsToValidateCheckBoxConsents) {
        for (Contact contactToValidateCheckBoxConsents : contactsToValidateCheckBoxConsents) {
            /* Check whether ADO should activate checkbox */
            if (contactToValidateCheckBoxConsents.Consent_Direct_Communications_ADO__c == null) {
                contactToValidateCheckBoxConsents.Consent_Direct_Communications__c = false;
            }
            if (contactToValidateCheckBoxConsents.Consent_Electronic_Communication_ADO__c == null) {
                contactToValidateCheckBoxConsents.Consent_Electronic_Communication__c = false;
            }
            if (contactToValidateCheckBoxConsents.Consent_Graduates_ADO__c == null) {
                contactToValidateCheckBoxConsents.Consent_Graduates__c = false;
            }
            if (contactToValidateCheckBoxConsents.Consent_Marketing_ADO__c == null) {
                contactToValidateCheckBoxConsents.Consent_Marketing__c = false;
            }
            if (contactToValidateCheckBoxConsents.Consent_PUCA_ADO__c == null) {
                contactToValidateCheckBoxConsents.Consent_PUCA__c = false;
            }
        }
    }

    /* Sebastian Łasisz */
    // On consent changes determine checkbox value
    public static void enableCheckboxValueBasedOnAdoValues(List<Contact> contactsToValidateCheckBoxConsents) {
        for (Contact contactToValidateCheckBoxConsents : contactsToValidateCheckBoxConsents) {
            /* Check whether ADO should activate checkbox */
            if (contactToValidateCheckBoxConsents.Consent_Direct_Communications_ADO__c != null) {
                contactToValidateCheckBoxConsents.Consent_Direct_Communications__c = true;
            }
            if (contactToValidateCheckBoxConsents.Consent_Electronic_Communication_ADO__c != null) {
                contactToValidateCheckBoxConsents.Consent_Electronic_Communication__c = true;
            }
            if (contactToValidateCheckBoxConsents.Consent_Graduates_ADO__c != null) {
                contactToValidateCheckBoxConsents.Consent_Graduates__c = true;
            }
            if (contactToValidateCheckBoxConsents.Consent_Marketing_ADO__c != null) {
                contactToValidateCheckBoxConsents.Consent_Marketing__c = true;
            }
            if (contactToValidateCheckBoxConsents.Consent_PUCA_ADO__c != null) {
                contactToValidateCheckBoxConsents.Consent_PUCA__c = true;
            }
        }
    }

    /* Sebastian Łasisz */
    // set all consents on target when 'marketing consent 3' is set to 'no'
    public static void setAllConsentsToNoWhenMarketingIsSetNo(Map<Contact, Set<String>> refusedConsents) {
        for (Contact contactWithAcceptedConsents : refusedConsents.keySet()) {
            Set<String> marketing3ConsentAdoValues = refusedConsents.get(contactWithAcceptedConsents);

            if (!marketing3ConsentAdoValues.isEmpty()) {
                if (contactWithAcceptedConsents.Consent_Direct_Communications__c) {
                    contactWithAcceptedConsents.Consent_Direct_Communications_ADO__c = retrieveActualAdoForConsent(contactWithAcceptedConsents, Contact.Consent_Direct_Communications_ADO__c, marketing3ConsentAdoValues);
                }
                if (contactWithAcceptedConsents.Consent_Electronic_Communication__c) {
                    contactWithAcceptedConsents.Consent_Electronic_Communication_ADO__c = retrieveActualAdoForConsent(contactWithAcceptedConsents, Contact.Consent_Electronic_Communication_ADO__c, marketing3ConsentAdoValues);
                }
                if (contactWithAcceptedConsents.Consent_Graduates__c) {
                    contactWithAcceptedConsents.Consent_Graduates_ADO__c = retrieveActualAdoForConsent(contactWithAcceptedConsents, Contact.Consent_Graduates_ADO__c, marketing3ConsentAdoValues);
                }
                if (contactWithAcceptedConsents.Consent_Marketing__c) {
                    contactWithAcceptedConsents.Consent_Marketing_ADO__c = retrieveActualAdoForConsent(contactWithAcceptedConsents, Contact.Consent_Marketing_ADO__c, marketing3ConsentAdoValues);
                }                
            }
        }
    }

    /* Sebastian Łasisz */
    // Helper method for setAllConsentsToNoWhenMarketingIsSetNo to retrieve 
    public static String retrieveActualAdoForConsent(Contact contactWithConsents, Schema.SObjectField sObjField, Set<String> pucaADO) {
        String adoValues = (String)contactWithConsents.get(sObjField);
        Set<String> adoValuesAsSet = CommonUtility.getOptionsFromMultiSelect(adoValues);
        for (String ado : pucaADO) {
            if (adoValuesAsSet.contains(ado)) {
                adoValuesAsSet.remove(ado);
            }
        }

        return CommonUtility.getMultiSelectFromSet(adoValuesAsSet);
    }

    /* Sebastian Łasisz */
    // set 'marketing consent 3' (PUCA) on true if any other consent is accepted
    public static void setMarketingCampaign3OnTrue(List<Contact> contactsWithAcceptedConsents) {
        List<HistoryLog> historyLogs = new List<HistoryLog>();

        for (Contact contactWithAcceptedConsents : contactsWithAcceptedConsents) {
            Set<String> marketing3ConsentAdoValues = retrieveActiveAdoForConsents(contactWithAcceptedConsents);
            if (!marketing3ConsentAdoValues.isEmpty()) {
                /* On PUCA selection add another instance to history log */
                if (!contactWithAcceptedConsents.Consent_PUCA__c) {
                    HistoryLog historyLog = new HistoryLog();
                    historyLog.contactId = contactWithAcceptedConsents.Id;
                    historyLog.contact = contactWithAcceptedConsents;

                    historyLog.consentHistoryLogs = new List<ConsentHistoryLog>();                    
                    historyLog.consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_PUCA__c'), true, marketing3ConsentAdoValues, null));
                    
                    historyLogs.add(historyLog);
                }
                contactWithAcceptedConsents.Consent_PUCA__c = true;
                contactWithAcceptedConsents.Consent_PUCA_ADO__c = CommonUtility.getMultiSelectFromSet(marketing3ConsentAdoValues);
            }
        }

        MarketingConsentManager.createChangeHistoryLog(historyLogs);
    }

    /* Sebastian Łasisz */
    // Helper method for setMarketingCampaign3OnTrue to retrieve all appropiate ADO for MC3
    public static Set<String> retrieveActiveAdoForConsents(Contact contactWithConsents) {
        Set<String> activeADO = new Set<String>();
        activeADO.addAll(CommonUtility.getOptionsFromMultiSelect(contactWithConsents.Consent_PUCA_ADO__c));
        activeADO.addAll(CommonUtility.getOptionsFromMultiSelect(contactWithConsents.Consent_Direct_Communications_ADO__c));
        activeADO.addAll(CommonUtility.getOptionsFromMultiSelect(contactWithConsents.Consent_Electronic_Communication_ADO__c));
        activeADO.addAll(CommonUtility.getOptionsFromMultiSelect(contactWithConsents.Consent_Graduates_ADO__c));
        activeADO.addAll(CommonUtility.getOptionsFromMultiSelect(contactWithConsents.Consent_Marketing_ADO__c));

        return activeADO;
    }

    /* Sebastian Łasisz */
    // change Contact RT to Anonymous if last PUCA is set to NO
    public static void changeContactRTOnMC3Denial(List<Contact> contactsWithDeniedPUCA) {
        for (Contact contactToChangeRT : contactsWithDeniedPUCA) {
            contactToChangeRT.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS);
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ HISTORY LOG ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static HistoryLog prepareHistoryLog(Contact cNew, Contact cOld) {
        HistoryLog historyLog = new HistoryLog();
        historyLog.contactId = cNew.Id;
        historyLog.contact = cNew;
        historyLog.consentHistoryLogs = new List<ConsentHistoryLog>();

        /* Prepare log based on consent change */
        historyLog.consentHistoryLogs.addAll(prepareConsentHistoryLogOnConsentChange(cNew, cOld));
        /* Prepare log based on consent ado change */
        historyLog.consentHistoryLogs.addAll(prepareConsentHistoryLogOnAdoChange(cNew, cOld));

        return historyLog;
    }

    public static List<ConsentHistoryLog> prepareConsentHistoryLogOnAdoChange(Contact cNew, Contact cOld) {    
        List<ConsentHistoryLog> consentHistoryLogs = new List<ConsentHistoryLog>();
        if (cNew.Consent_Marketing_ADO__c != cOld.Consent_Marketing_ADO__c) {
            Set<String> removedValues = prepareConsentHistoryLogOnAdoRemoval(cOld.Consent_Marketing_ADO__c, cNew.Consent_Marketing_ADO__c);
            Set<String> addedValues = prepareConsentHistoryLogOnAdoInsertion(cOld.Consent_Marketing_ADO__c, cNew.Consent_Marketing_ADO__c);
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Marketing__c'), cNew.Consent_Marketing__c, addedValues, removedValues));
        }     
        if (cNew.Consent_Electronic_Communication_ADO__c != cOld.Consent_Electronic_Communication_ADO__c) {
            Set<String> removedValues = prepareConsentHistoryLogOnAdoRemoval(cOld.Consent_Electronic_Communication_ADO__c, cNew.Consent_Electronic_Communication_ADO__c);
            Set<String> addedValues = prepareConsentHistoryLogOnAdoInsertion(cOld.Consent_Electronic_Communication_ADO__c, cNew.Consent_Electronic_Communication_ADO__c);
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Electronic_Communication__c'), cNew.Consent_Electronic_Communication__c, addedValues, removedValues));
        }     
        if (cNew.Consent_PUCA_ADO__c != cOld.Consent_PUCA_ADO__c) {
            Set<String> removedValues = prepareConsentHistoryLogOnAdoRemoval(cOld.Consent_PUCA_ADO__c, cNew.Consent_PUCA_ADO__c);
            Set<String> addedValues = prepareConsentHistoryLogOnAdoInsertion(cOld.Consent_PUCA_ADO__c, cNew.Consent_PUCA_ADO__c);
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_PUCA__c'), cNew.Consent_PUCA__c, addedValues, removedValues));            
        }     
        if (cNew.Consent_Direct_Communications_ADO__c != cOld.Consent_Direct_Communications_ADO__c) {
            Set<String> removedValues = prepareConsentHistoryLogOnAdoRemoval(cOld.Consent_Direct_Communications_ADO__c, cNew.Consent_Direct_Communications_ADO__c);
            Set<String> addedValues = prepareConsentHistoryLogOnAdoInsertion(cOld.Consent_Direct_Communications_ADO__c, cNew.Consent_Direct_Communications_ADO__c);
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Direct_Communications__c'), cNew.Consent_Direct_Communications__c, addedValues, removedValues));            
        }     
        if (cNew.Consent_Graduates_ADO__c != cOld.Consent_Graduates_ADO__c) {
            Set<String> removedValues = prepareConsentHistoryLogOnAdoRemoval(cOld.Consent_Graduates_ADO__c, cNew.Consent_Graduates_ADO__c);
            Set<String> addedValues = prepareConsentHistoryLogOnAdoInsertion(cOld.Consent_Graduates_ADO__c, cNew.Consent_Graduates_ADO__c);
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Graduates__c'), cNew.Consent_Graduates__c, addedValues, removedValues));            
        }

        return consentHistoryLogs;
    }

    /* Sebastian Łasisz */
    /* Helper method for prepareHistoryLog to retrieve changes in consents after removing ado */
    public static Set<String> prepareConsentHistoryLogOnAdoRemoval(String cOldValues, String cNewValues) {
        Set<String> cOldValuesSet = CommonUtility.getOptionsFromMultiSelect(cOldValues);
        Set<String> cNewValuesSet = CommonUtility.getOptionsFromMultiSelect(cNewValues);

        for (String value : cNewValuesSet) {
            if (cOldValuesSet.contains(value)) {
                cOldValuesSet.remove(value);
            }
        }

        return cOldValuesSet;
    }

    /* Sebastian Łasisz */
    /* Helper method for prepareHistoryLog to retrieve changes in consents after adding new ado */
    public static Set<String> prepareConsentHistoryLogOnAdoInsertion(String cOldValues, String cNewValues) {
        Set<String> cOldValuesSet = CommonUtility.getOptionsFromMultiSelect(cOldValues);
        Set<String> cNewValuesSet = CommonUtility.getOptionsFromMultiSelect(cNewValues);    

        Set<String> newValuesSet = new Set<String>();
        for (String value : cNewValuesSet) {
            if (!cOldValuesSet.contains((value))) {
                newValuesSet.add((value));
            }
        }

        return newValuesSet;
    }

    /* Sebastian Łasisz */
    /* Helper method for prepareHistoryLog to determine logs for changed consent state */
    public static List<ConsentHistoryLog> prepareConsentHistoryLogOnConsentChange(Contact cNew, Contact cOld) {
        List<ConsentHistoryLog> consentHistoryLogs = new List<ConsentHistoryLog>();
        if (cNew.Consent_PUCA__c != cOld.Consent_PUCA__c && !cNew.Consent_PUCA__c) {
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_PUCA__c'), cNew.Consent_PUCA__c, null, null));
        }
        if (cNew.Consent_Direct_Communications__c != cOld.Consent_Direct_Communications__c && !cNew.Consent_Direct_Communications__c) {
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Direct_Communications__c'), cNew.Consent_Direct_Communications__c, null, null));
        }
        if (cNew.Consent_Marketing__c != cOld.Consent_Marketing__c && !cNew.Consent_Marketing__c) {
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Marketing__c'), cNew.Consent_Marketing__c, null, null));
        }
        if (cNew.Consent_Graduates__c != cOld.Consent_Graduates__c && !cNew.Consent_Graduates__c) {
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Graduates__c'), cNew.Consent_Graduates__c, null, null));
        }
        if (cNew.Consent_Electronic_Communication__c != cOld.Consent_Electronic_Communication__c && !cNew.Consent_Electronic_Communication__c) {
            consentHistoryLogs.add(new ConsentHistoryLog(CatalogManager.getCatalogByAPIName('Consent_Electronic_Communication__c'), cNew.Consent_Electronic_Communication__c, null, null));
        }

        return consentHistoryLogs;
    }

    /* Sebastian Łasisz */
    /* Main method for creating history log for contact based on recent changes */
    public static void createChangeHistoryLog(List<HistoryLog> historyLogToParse) {
        Set<Id> userIds = new Set<Id>();
        for (HistoryLog historyLog : historyLogToParse) {
            userIds.add(historyLog.contact.LastModifiedById);
            userIds.add(historyLog.contact.CreatedById);
        }

        if (userMap == null) {
            userMap = new Map<Id, User>([
                SELECT Id, Name
                FROM User
            ]);
        }

        Id currentUser = UserInfo.getUserId();

        Map<Boolean, String> convenstValueMap = new Map<Boolean, String> {
            true => 'TAK',
            false => 'NIE'
        };

        for (HistoryLog historyLog : historyLogToParse) {
            List<String> changeHistoryLog = new List<String>();
            if (historyLog.contact.Change_History__c != null && historyLog.contact.Change_History__c.length() > 0) {
                changeHistoryLog = historyLog.contact.Change_History__c.split('\r\n');
            }

            List<String> newChangeHistoryLog = new List<String>();

            /* Prepare date for consent history log */
            String dateForHistoryLog = String.valueOf(System.now());
            if (historyLog.contact.TECH_Acquired_Consent_Date_Returned_Lead__c != null) {
                dateForHistoryLog = String.valueOf(historyLog.contact.TECH_Acquired_Consent_Date_Returned_Lead__c);
            }

            for (ConsentHistoryLog consentHistoryLog : historyLog.consentHistoryLogs) {
                String entity = consentHistoryLog.consentName.substring(consentHistoryLog.consentName.lastIndexOf(' ') + 1);
                
                /* Accepted ADO */
                if (consentHistoryLog.adoAccepted != null && !consentHistoryLog.adoAccepted.isEmpty()) {
                    String newlogRow = (
                        '- ' + dateForHistoryLog + ' ' +
                        historyLog.contact.System_Updating_Contact__c + ' ' + 
                        (userMap.get(currentUser).Name) + ' ' +  
                        consentHistoryLog.consentName.replace('WSB ' + entity, '') + ' ' + 
                        'TAK' + ' ' + 
                        setToString(consentHistoryLog.adoAccepted)
                    ); 

                    newChangeHistoryLog.add(newlogRow);      
                }     
                /* Declined ADO */  
                if (consentHistoryLog.adoDeclined != null && !consentHistoryLog.adoDeclined.isEmpty()) {
                    String newlogRow = (
                        '- ' + dateForHistoryLog + ' ' +
                        historyLog.contact.System_Updating_Contact__c + ' ' + 
                        (userMap.get(currentUser).Name) + ' ' +  
                        consentHistoryLog.consentName.replace('WSB ' + entity, '') + ' ' + 
                        'NIE' + ' ' + 
                        setToString(consentHistoryLog.adoDeclined)
                    );      

                    newChangeHistoryLog.add(newlogRow); 
                }    
                /* Change of Consent */
                if (consentHistoryLog.adoAccepted == null && consentHistoryLog.adoDeclined == null) {
                    String newlogRow = (
                        '- ' + dateForHistoryLog + ' ' +
                        historyLog.contact.System_Updating_Contact__c + ' ' + 
                        (userMap.get(currentUser).Name) + ' ' +  
                        consentHistoryLog.consentName.replace('WSB ' + entity, '') + ' ' +
                        convenstValueMap.get(consentHistoryLog.changedValue)
                    );      

                    newChangeHistoryLog.add(newlogRow); 
                }    
            }

            /* Split list of values and add new logs at the beggining */
            newChangeHistoryLog.addAll(changeHistoryLog);
            String properChangeHistoryLog = '';

            /* Fill change history log, if size of log is longer than field length, remove oldest records */
            Integer finalHistoryLogSize = 0;
            for (String changeHistoryLogVar : newChangeHistoryLog) {
                String newLogRecord = changeHistoryLogVar + '\r\n';
                if ((finalHistoryLogSize + newLogRecord.length()) <= 13000) {
                    properChangeHistoryLog += newLogRecord;
                    finalHistoryLogSize += newLogRecord.length();
                }
            }
            
            historyLog.contact.Change_History__c = properChangeHistoryLog;
        }
    }

    /* Sebastian Łasisz */
    /* Helper method for createChangeHistoryLog to transfer set to string. In the process transfer full ado names to initials */
    public static String setToString(Set<String> setOfAdos) {
        String adoString = '[';
        for (String ado : setOfAdos) {
            adoString += CommonUtility.getEntityByMarketingEntity(ado) + ', ';
        }

        adoString = adoString.removeEnd(', ');
        adoString += ']';

        return adoString;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- HELPER HISTORY LOG CLASSES ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class HistoryLog {
        Id contactId;
        Contact contact;
        List<ConsentHistoryLog> consentHistoryLogs;
    }

    public class ConsentHistoryLog {
        String consentName;
        Boolean changedValue;
        Set<String> adoAccepted;
        Set<String> adoDeclined;

        public ConsentHistoryLog(String consentName, Boolean changedValue, Set<String> adoAccepted, Set<String> adoDeclined) {
            this.consentName = consentName;
            this.changedValue = changedValue;
            this.adoAccepted = adoAccepted;
            this.adoDeclined = adoDeclined;
        }
    }

}