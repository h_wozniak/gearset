public with sharing class ContactPageController {

	public String contactId { get; set; }
	public String script {get; set; }
	public Contact contact { get; set; }
	public String value { get; set; }


	public ContactPageController(ApexPages.StandardController stdController) {
		ApexPages.StandardController controller = stdController;
		controller.addFields(new List<String> {'Email', 'Additional_Emails__c'});
    	contact = (Contact)controller.getrecord();
	    PageReference page = ApexPages.currentPage();
	    contactId = page.getParameters().get('id');
		//contactId = '0038E00000TG9cQ';
		script = '<script>'
			+ 'function doSomething() {'
				+ 'sforce.connection.sessionId = "' + UserInfo.getSessionId() + '";'
				+ 'j$ = jQuery.noConflict();'
     			+ 'var jsonContactObj = \'' + null + '\';'
     			+ 'jQuery.modal(\'<div style="height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);"><div style="font-weight: bold; text-align: center;">' + Label.modal_Loading + '...</div><div style="padding: 15px 20px;"><div style=" height: 31px; width: 31px; background: url(/resource/AjaxLoaderGif) no-repeat" ></div></div></div>\');'
     			+ 'var jsonIPressoContactObj = \'' + null + '\';'
     			+ 'var result = sforce.apex.execute("WebserviceUtilities", "updateContactAndRepinIPressoContact", {contactJSON: jsonContactObj, iPressoJSON : jsonIPressoContactObj});'
     			+ 'window.location.replace("/" + result);'
			+ '}'
		+ '</script>';
	}

	//public PageReference save() {
	//	System.debug('Email : ' + contact.Email + ', Addtional Emails: ' + contact.Additional_Emails__c);
	//	return null;
	//}

	public PageReference sayHello() {
		//ErrorLogger.msg('Email : ' + contact.Email + ', Addtional Emails: ' + contact.Additional_Emails__c);
		//ErrorLogger.msg('Value: ' + value);
		//ErrorLogger.msg('Say something nice');
		return null;
	}
}