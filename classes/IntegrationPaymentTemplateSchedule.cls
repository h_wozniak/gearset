/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send payment templates to experia
**/

/**
*   To initiate the IntegrationPaymentTemplateSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('IntegrationPaymentTemplateSchedule', '0 0 8 * * ? *', new IntegrationPaymentTemplateSchedule());
**/


global class IntegrationPaymentTemplateSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
    	List<Offer__c> paymentTemplatesToSendToExperia = [
    		SELECT Id
    		FROM Offer__c
    		WHERE PriceBook_Date_To_Send_To_Experia__c = :System.Today()
    		AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG)
    		OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
    		OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK))
    	];

    	Set<Id> paymentTemplatesIdsToSendToExperia = new Set<Id>();
    	for (Offer__c paymentTemplate : paymentTemplatesToSendToExperia) {
    		paymentTemplatesIdsToSendToExperia.add(paymentTemplate.Id);
    	}

        IntegrationPaymentTemplateHelper.updateSyncStatus_inProgress(true, paymentTemplatesIdsToSendToExperia);
        IntegrationPaymentTemplateSender.sendPaymentTemplatesASync(paymentTemplatesIdsToSendToExperia);    	
    }
}