/**
 * Created by martyna.stanczuk on 2018-06-27.
 **/

public with sharing class LEX_Rewind_Resignation_Controller {

    private static final Set<String> forbiddenStatuses = new Set<String> {
            CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
            CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS,
            CommonUtility.ENROLLMENT_STATUS_RESIGNATION
    };

    @AuraEnabled
    public static String hasEditAccess(String recordId) {
        return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
    }

    @AuraEnabled
    public static String checkRecord(String recordId) {

        String canBeRewind = 'FALSE';

        Enrollment__c enrollment = [SELECT Last_Active_Status__c ,Status__c FROM Enrollment__c WHERE Id = :recordId];

        if (!forbiddenStatuses.contains(enrollment.Last_Active_Status__c) && enrollment.Status__c==CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
            canBeRewind = 'TRUE';
        }

        return canBeRewind;
    }

    @AuraEnabled
    public static String updateFieldValues(String objId, String sObjectName, String[] fieldNames, String[] fieldTypes, String[] fieldValues) {
        return LEXServiceUtilities.updateFieldValues(objId, sObjectName, fieldNames, fieldTypes, fieldValues) ? 'SUCCESS' : 'FAILURE';
    }

    @AuraEnabled
    public static String getLastActiveStatus(String recordId){
        List<Enrollment__c> enrollments = [SELECT Last_Active_Status__c FROM Enrollment__c WHERE Id = :recordId];
        Enrollment__c enrollment = enrollments[0];
        return enrollment.Last_Active_Status__c;
    }

    @AuraEnabled
    public static void setEditAccess(String recordId, Boolean canBeEdited) {
        Enrollment__c enrollment = [SELECT TECH_Allow_Resignation_Field_Change__c FROM Enrollment__c WHERE Id = :recordId];
        if(enrollment.TECH_Allow_Resignation_Field_Change__c != canBeEdited){
            enrollment.TECH_Allow_Resignation_Field_Change__c = canBeEdited;
            update enrollment;
        }
    }
}