@isTest
private class ZTestLEX_NewButtonOverrideController {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    @isTest static void testDiscountNewButtonOverride() {
        Boolean errorOccured = false;

        try {
            System.assertEquals(0, LEX_NewButtonOverrideController.getRecordTypes(null).size());
        }
        catch (Exception e) {
            errorOccured = true;
        }
        System.assert(errorOccured);
    }

    @isTest static void testDiscountNewButtonOverride_2() {
        System.assertNotEquals(0, LEX_NewButtonOverrideController.getRecordTypes('Discount__c').size());
    }

    @isTest static void testgetObjectLabel() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            System.assertEquals('Discount', LEX_NewButtonOverrideController.getObjectLabel('Discount__c'));
        }
    }

}