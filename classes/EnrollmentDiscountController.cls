public with sharing class EnrollmentDiscountController {

    public List<Discount__c> availableDiscounts {get; set;}

    public Integer groupNumber = 1;
    public String previousClass { get; set; }
    public String nextClass { get; set; }
    public Integer minIndex { get; set; }
    public Integer maxIndex { get; set; }
    public Id idToDel { get; set; }
    public Discount__c newDiscount { get; set; }
    public Id targetParentDiscountId {get; set; }
    public Boolean showDiscountDetails { get; set; }
    public Id enrollmentId { get; set; }
    public Boolean isAdHoc { get; set; }
    public final Integer groupSize = 5;
    
    public EnrollmentDiscountController() {
        showDiscountDetails = false;
        enrollmentId = Id.valueOf(Apexpages.currentPage().getParameters().get('enrollment'));
    }

    public List<Discount__c> getPaginatedAvailableDiscounts() {
        Id discountId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR);

        availableDiscounts = [SELECT Id, Name, toLabel(RecordType.Name), Discount_Kind__c, Discount_Value__c, Valid_From__c, Valid_To__c 
                              FROM Discount__c 
                              WHERE (NOT RecordTypeId = : discountId)
                              AND Active__c = true
                              AND (Valid_From__c <= TODAY OR Valid_From__c = null)
                              AND (Valid_To__c >= TODAY OR Valid_To__c = null)
                              AND (RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_AD_HOC) 
                                  OR RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT))
                              ORDER BY RecordType.Name];
                              
        return sublist(this.availableDiscounts, groupSize * (groupNumber - 1), groupSize * groupNumber);
    }

    private List<sObject> sublist(List<sObject> recordList, Integer fromIndex, Integer toIndex) {
        List<sObject> resultList = new List<sObject>();
        for (Integer i = fromIndex; i < toIndex; i++) {
            if (i < recordList.size()) {
                resultList.add(recordList[i]);
            }
        }
        verifyClassesAndIndexes();
        return resultList;
    }

    private void verifyClassesAndIndexes() {
        if (groupNumber*groupSize >= availableDiscounts.size()) {
            nextClass = 'inactive';
        } else {
            nextClass = '';
        }
        if (groupNumber <= 1) {

            previousClass = 'inactive';
        } else {
            previousClass = '';
        }
        minIndex = (groupNumber - 1)*groupSize + 1;
        maxIndex = (groupNumber*groupSize) > availableDiscounts.size()? availableDiscounts.size() : (groupNumber*groupSize);
    }

    public void nextGroup() {
        if (groupNumber*groupSize < availableDiscounts.size()) {
            groupNumber++;
        }
    }

    public void previousGroup() {
        if (groupNumber > 1) {
            groupNumber--;
        }
    }

    public void lastGroup() {
        groupNumber = Integer.valueOf(Decimal.valueOf(Double.valueOf(availableDiscounts.size())/groupSize).round(System.RoundingMode.CEILING));
    }

    public void firstGroup() {
        groupNumber = 1;
    }

    public void fillFieldsWithDiscountValues() {
        showDiscountDetails = true;
        newDiscount = new Discount__c();
        newDiscount.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR);
        newDiscount.Enrollment__c = enrollmentId;
        newDiscount.Discount_from_Discount_Connector__c = targetParentDiscountId;
        Discount__c targetParentDiscount = [SELECT Id, Name, Discount_Value__c, RecordTypeId, Discount_Kind__c 
                                            FROM Discount__c 
                                            WHERE Id = :targetParentDiscountId LIMIT 1];
        
        newDiscount.Discount_Kind__c = targetParentDiscount.Discount_Kind__c;
        newDiscount.Discount_Value__c = targetParentDiscount.Discount_Value__c;
        isAdHoc = targetParentDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_AD_HOC);
    }

    public PageReference saveDiscount() {
        if (newDiscount.Discount_Kind__c == null || newDiscount.Discount_Value__c == null) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_DiscountKindAndValueReq));
            return null;
        }

        try {
            upsert newDiscount;
            return new PageReference('/' + newDiscount.Enrollment__c);
        } catch(Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }

    public PageReference back() {
        return new PageReference('/' + enrollmentId);
    }
}