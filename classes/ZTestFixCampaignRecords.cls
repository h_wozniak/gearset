@isTest
public class ZTestFixCampaignRecords {

    @isTest static void testBatch() {
        CustomSettingDefault.initDidacticsCampaign();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);
        Id universityStudyOffer = courseOffer.University_Study_Offer_from_Course__c;

        Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaign(
                new Marketing_Campaign__c(
                        Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_WELCOMECALL,
                        Active__c = true,
                        Date_Start__c = System.today(),
                        Date_End__c = System.today()
                ), true);

        Marketing_Campaign__c campaignOffer = ZDataTestUtility.createCampaignOffer(new Marketing_Campaign__c(Campaign_from_Campaign_Offer__c = campaign.Id, Campaign_Offer__c = universityStudyOffer), true);

        List<Marketing_Campaign__c> classifiersOnCampaign = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
                AND Campaign_from_Classifier__c = :campaign.Id
                AND Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW
        ];

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        System.assertEquals(studyEnr.Status__c, 'Confirmed');

        Database.executeBatch(new FixCampaignRecords(), 200);
        Test.stopTest();
    }
}