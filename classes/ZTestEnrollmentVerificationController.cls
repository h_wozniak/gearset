@isTest
private class ZTestEnrollmentVerificationController {

    @isTest static void test_VerifyEnr() {
        Enrollment__c enr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_CRM,
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
        ), true);
        
        Enrollment__c studyEnr = [SELECT Id, VerificationString__c FROM Enrollment__c WHERE Id = :enr.Id];

        System.debug(studyEnr);

        EnrollmentVerificationController cntrl = new EnrollmentVerificationController();

        Test.startTest();
        EnrollmentVerificationController.verifyEnrollment(studyEnr.VerificationString__c);
        Test.stopTest();

        Enrollment__c studyEnr2 = [SELECT Id, Status__c, VerificationDate__c FROM Enrollment__c WHERE Id = :enr.Id];

        System.assertNotEquals(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED, studyEnr2.Status__c);
        System.assertEquals(System.today(), studyEnr2.VerificationDate__c.date());

        EnrollmentVerificationController.VerificationResponse resp = EnrollmentVerificationController.verifyEnrollment(studyEnr.VerificationString__c);

        System.assertEquals(resp.alreadyVerified, true);
    }

    @isTest static void test_VerifyEnr_fail() {
        Test.startTest();
        EnrollmentVerificationController.VerificationResponse resp = EnrollmentVerificationController.verifyEnrollment('wrongVerificationCode');
        Test.stopTest();

        System.assertEquals(resp.result, false);
        System.assertEquals(resp.alreadyVerified, false);
    }

}