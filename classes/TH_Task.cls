public without sharing class TH_Task extends TriggerHandler.DelegateBase {
	List<Task> tasksToSendActivityAsAttendece;
	List<Task> tasksToSendContactActivity;
	List<Task> tasksToSendMarketingPhoneContactActivity;

    public override void prepareBefore() {
    	tasksToSendActivityAsAttendece = new List<Task>();
    	tasksToSendContactActivity = new List<Task>();
    	tasksToSendMarketingPhoneContactActivity = new List<TasK>();
    }

    //public override void prepareAfter() {}

    //public override void prepareAfter() {}

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Task aNew = (Task)obj;

           	/* Sebastian Łasisz */
           	// Prepare list of tasks that is going to send to iPresso as Activity (Attandence at the event)
            if (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_ATTENDACE_AT_EVENT_ACTIVITY)) {
            	tasksToSendActivityAsAttendece.add(aNew);
            }

           	/* Sebastian Łasisz */
           	// Prepare list of tasks that is going to send to iPresso as Activity (Marketing Phone Contact)
            if (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_MARKETING_PHONE_CONTACT_ACTIVITY)) {
            	tasksToSendMarketingPhoneContactActivity.add(aNew);
            }

           	/* Sebastian Łasisz */
           	// Prepare list of tasks that is going to send to iPresso as Activity (Contact)
            if (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CONTACT_ACTIVITY)) {
            	tasksToSendContactActivity.add(aNew);
            }
        }
    }

    //public override void afterInsert(Map<Id, sObject> o) {}

    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    //public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    //public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {

       	/* Sebastian Łasisz */
       	// Prepare list of tasks that is going to send to iPresso as Activity (Attandence at the event)
        if (tasksToSendActivityAsAttendece != null && !tasksToSendActivityAsAttendece.isEmpty()) {
        	ActivityManager.attendaceAtTheEventActivity(tasksToSendActivityAsAttendece);
        }

       	/* Sebastian Łasisz */
       	// Prepare list of tasks that is going to send to iPresso as Activity (Marketing Phone Contact)
        if (tasksToSendContactActivity != null && !tasksToSendContactActivity.isEmpty()) {
        	ActivityManager.contactActivity(tasksToSendContactActivity);
        }

       	/* Sebastian Łasisz */
       	// Prepare list of tasks that is going to send to iPresso as Activity (Contact)
        if (tasksToSendMarketingPhoneContactActivity != null && !tasksToSendMarketingPhoneContactActivity.isEmpty()) {
        	ActivityManager.marketingPhoneContactActivity(tasksToSendMarketingPhoneContactActivity);
        }
    }
}