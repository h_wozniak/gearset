/**
* @author       Wojciech Słodziak
* @description  Batch class to update Schedule's University_Contact_Data__c after it changes on Account
**/

global class ScheduleAccountDataUpdateBatch implements Database.Batchable<sObject> {
    
    String query;

    global ScheduleAccountDataUpdateBatch(Set<Id> accountIds) {
        query = 'SELECT Id, University_Contact_Data__c, Training_Offer_from_Schedule__r.University_from_Training__r.Contact_Data_for_Trainings__c FROM Offer__c ' +
                'WHERE (RecordTypeId = \'' + CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) + '\' OR RecordTypeId = \'' + CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE) + '\') ' + 
                'AND Training_Offer_from_Schedule__r.University_from_Training__c IN ' + CommonUtility.listToSOQLString(new List<Id>(accountIds));
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Offer__c> scheduleList) {
        for (Offer__c sch : scheduleList) {
            sch.University_Contact_Data__c = sch.Training_Offer_from_Schedule__r.University_from_Training__r.Contact_Data_for_Trainings__c;
        }

        try {
            update scheduleList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
    
    global void finish(Database.BatchableContext BC) {

    }

}