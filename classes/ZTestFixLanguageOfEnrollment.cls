@isTest
public class ZTestFixLanguageOfEnrollment {

	@isTest static void testBatch() {
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
		
		Test.startTest();
		Database.executeBatch(new FixLanguageOfEnrollment(), 200);
		Test.stopTest();
	}
}