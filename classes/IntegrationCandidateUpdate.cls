/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Candidate Integration
**/

@RestResource(urlMapping = '/contact_update/*')
global without sharing class IntegrationCandidateUpdate {
    
    @HttpPut
    global static void receiveContactInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();
        
        IntegrationManager.Contact_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            /* BY ALL THAT IS GOOD AND HOLY DO NOT UNCOMMENT THIS LINE UNLESS COMPLETELY SURE EXPERIA WONT SEND FAKE CONTACTS */
            /* IF YOU DONT EXPECT THOUSANDS OF EMAIL PER DAY */
            //ErrorLogger.log(e);
            System.debug(e.getMessage());
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }
        
        String universityForSharing;
        if (parsedJson != null) {
            try {
                Contact contactToCheckId = [SELECT Id, University_for_Sharing__c FROM Contact WHERE Id = :parsedJson.updating_system_contact_id];
                universityForSharing = contactToCheckId.University_for_Sharing__c;
            }
            catch (Exception e) {
                /* BY ALL THAT IS GOOD AND HOLY DO NOT UNCOMMENT THIS LINE UNLESS COMPLETELY SURE EXPERIA WONT SEND FAKE CONTACTS */
                /* IF YOU DONT EXPECT THOUSANDS OF EMAIL PER DAY */
                //ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(704);
                return ;
            }

            try {
                ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
                Boolean ignoreEmailUpdates = esb.Ignore_Email_Updates_From_Experia__c;

                Contact contactToUpdate = [
                    SELECT Id, Consent_Marketing_ADO__c, Consent_Graduates_ADO__c, Consent_Electronic_Communication_ADO__c,
                    Consent_Direct_Communications_ADO__c
                    FROM Contact
                    WHERE Id = :parsedJson.updating_system_contact_id
                ];

                contactToUpdate.FirstName = parsedJson.first_name;
                contactToUpdate.Middle_Name__c = parsedJson.second_name;
                contactToUpdate.LastName = parsedJson.last_name;
                contactToUpdate.Family_Name__c = parsedJson.family_name;
                contactToUpdate.System_Updating_Contact__c = parsedJson.updating_system;

                if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_EXPERIA) {
                    CommonUtility.preventSendingDataToExperia = true;
                } else if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_MA) {
                    CommonUtility.preventSendingDataToIPresso = true;
                } else if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_ZPI) {
                    CommonUtility.preventSendingDataToZPI = true;
                }

                if (!ignoreEmailUpdates) {
                    if (parsedJson.primary_email != null) {
                        contactToUpdate.Email = parsedJson.primary_email;
                    }
                    if (parsedJson.additional_emails != null && parsedJson.additional_emails.size() > 0) {
                        contactToUpdate.Additional_Emails__c = listToString(parsedJson.additional_emails);
                    }         
                }
                contactToUpdate = (Contact)updatePhones(contactToUpdate, parsedJson.phones);
                contactToUpdate = (Contact)updateAddresses(contactToUpdate, parsedJson.addresses);

                if (parsedJson.marketing_automation != null) {
                    contactToUpdate = updateMAInformation(contactToUpdate, parsedJson.marketing_automation, universityForSharing);
                }

                contactToUpdate = updateConsents(contactToUpdate, parsedJson.consents, parsedJson.updating_system);

                CommonUtility.skipUpdatingUpdateSystemContactField = true;
                CommonUtility.preventTriggeringEmailValidation = true;

                update contactToUpdate;
            }
            catch(Exception e) {
                /* BY ALL THAT IS GOOD AND HOLY DO NOT UNCOMMENT THIS LINE UNLESS COMPLETELY SURE EXPERIA WONT SEND FAKE CONTACTS */
                /* IF YOU DONT EXPECT THOUSANDS OF EMAIL PER DAY */
                //ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(601, e.getMessage());
                return ;
            }
        }
    }

    public static Contact updateMAInformation(Contact contactToUpdate, IntegrationManager.MarketingAutomation_JSON marketingAutomation, String department) {
        contactToUpdate.Year_of_Graduation__c = marketingAutomation.a_level_exam_year;
        contactToUpdate.School_Type__c = marketingAutomation.school_type;

        if (marketingAutomation.school_name != null) {
            List<Account> schools = [SELECT Id, BillingCity, RecordTypeId FROM Account WHERE Name = :marketingAutomation.school_name];

            if (!schools.isEmpty() && schools.get(0).RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)) {
                contactToUpdate.School__c = schools.get(0).Id;
                contactToUpdate.School_Town__c = schools.get(0).BillingCity;
            }
            else {
                contactToUpdate.School_Name__c = marketingAutomation.school_name;
            }
        }
        contactToUpdate.Position__c = marketingAutomation.position;
        String areasOfInteresets = '';
        for (String areaOfInterest : marketingAutomation.area_of_interest) {
            areasOfInteresets += areaOfInterest + ';';
        }

        areasOfInteresets = areasOfInteresets.left(areasOfInteresets.length() - 1);
        contactToUpdate.Areas_of_Interest__c = areasOfInteresets;

        List<String> departments = new List<String>();
        if (department != null) {
            departments = department.split(', ');
        }

        Set<String> setOfDepartments = new Set<String>();
        setOfDepartments.addAll(departments);
        setOfDepartments.addAll(marketingAutomation.department);

        String departmentsToUpdate = '';
        for (String departmentToAdd : setOfDepartments) {
            departmentsToUpdate += departmentToAdd + ', ';
        }

        departmentsToUpdate = departmentsToUpdate.left(departmentsToUpdate.length() - 2);

        contactToUpdate.University_for_Sharing__c = departmentsToUpdate;

        return contactToUpdate;
    }
    
    global static String listToString(List<String> listToConvert) {
        String result = '';
        if (listToConvert != null) {
            for (Integer i = 0; i < listToConvert.size(); i++) {
                if (i == (listToConvert.size() - 1)) {
                    result += listToConvert.get(i);
                }
                else {
                    result += listToConvert.get(i) + '; ';
                }
            }
        }
        
        return result;
    }
    
    public static sObject updatePhones(sObject contactToUpdate, List<IntegrationManager.Phone_JSON> phones) {
        String apiSuffix = '';

        if(contactToUpdate instanceof Contact) {
            apiSuffix = '__c';
        } else if(contactToUpdate instanceof Account){
            apiSuffix = '__pc';
        } else {
            //throw ex
        }

        String additional_phones = '';
        for (IntegrationManager.Phone_JSON phone : phones) {
            if (phone.type == 'Mobile') {
                contactToUpdate.put('MobilePhone', phone.phoneNumber);
            }
            else if (phone.type =='Default') {
                contactToUpdate.put('Phone', phone.phoneNumber);
            }
            else {
                additional_phones += phone.phoneNumber + '; ';
            }
        }
        contactToUpdate.put('Additional_Phones'+apiSuffix, additional_phones.left(additional_phones.length() - 2));
        return contactToUpdate;
    }
    
    public static sObject updateAddresses(sObject contactToUpdate, List<IntegrationManager.Address_JSON> addresses) {
        String apiSuffix = '';

        if(contactToUpdate instanceof Contact) {
            apiSuffix = '__c';
        } else if(contactToUpdate instanceof Account){
            apiSuffix = '__pc';
        } else {
            //throw ex
        }

        Boolean mailingExists = false;  

        Map<String, String> addressesTranslatonMap = new Map<String, String>{
            'dolnośląskie' => 'Lower Silesia',
            'kujawsko-pomorskie' => 'Kuyavian-Pomeranian',
            'lubelskie' => 'Lublin',
            'lubuskie' => 'Lubusz',
            'łódzkie' => 'Łódź',
            'małopolskie' => 'Lesser Poland',
            'mazowieckie' => 'Masovian',
            'opolskie' => 'Opole',
            'podkarpackie' => 'Subcarpathian',
            'podlaskie' => 'Podlaskie',
            'pomorskie' => 'Pomeranian',
            'śląskie' => 'Silesia',
            'świętokrzyskie' => 'Świętokrzyskie',
            'warmińsko-mazurskie' => 'Warmian-Masurian',
            'wielkopolskie' => 'Greater Poland',
            'zachodniopomorskie' => 'West Pomeranian'
        };     

        for (IntegrationManager.Address_JSON address : addresses) {
            if (address.type == 'Mailing') {
                contactToUpdate.put('MailingPostalCode', address.postal_code);
                contactToUpdate.put('MailingCity',address.city);
                contactToUpdate.put('MailingStreet', address.street);
                String properState = addressesTranslatonMap.get(address.state);
                contactToUpdate.put('MailingState', properState != null ? properState : address.state);

                String properCountry = DictionaryTranslator.getKeyForTranslatedValue(address.country, CommonUtility.LANGUAGE_CODE_PL, Contact.Country_of_Origin__c);
                if (properCountry != null) {
                    contactToUpdate.put('MailingCountry', properCountry);
                }
                else if (address.country != null && address.country != '') {
                    contactToUpdate.put('MailingCountry', address.country);
                }
                else if ((address.country == null || address.country == '') && properState != null) {
                    contactToUpdate.put('MailingCountry', CommonUtility.CONTACT_COUNTRY_POLAND);
                }

                mailingExists = true;
            }
            else if (address.type == 'Other') {
                contactToUpdate.put('OtherPostalCode', address.postal_code);
                contactToUpdate.put('OtherCity', address.city);
                contactToUpdate.put('OtherStreet', address.street);
                String properState = addressesTranslatonMap.get(address.state);
                contactToUpdate.put('OtherState', properState != null ? properState : address.state);
                
                String properCountry = DictionaryTranslator.getKeyForTranslatedValue(address.country, CommonUtility.LANGUAGE_CODE_PL, Contact.Country_of_Origin__c);
                if (properCountry != null) {
                    contactToUpdate.put('OtherCountry', properCountry);
                }
                else if (address.country != null && address.country != '') {
                    contactToUpdate.put('OtherCountry', address.country);
                }
                else if ((address.country == null || address.country == '') && properState != null) {
                    contactToUpdate.put('OtherCountry', CommonUtility.CONTACT_COUNTRY_POLAND);
                }
            }
        }
        
        contactToUpdate.put('The_Same_Mailling_Address'+apiSuffix, !mailingExists);
        
        return contactToUpdate;
    }
    
    public static Contact updateConsents(Contact contactToUpdate, List<IntegrationManager.MarketingConsents_JSON> consents, String updatingSystem) { 
        if (consents != null) {
            for (IntegrationManager.MarketingConsents_JSON consent : consents) {  
                if (consent.type != RecordVals.MARKETING_CONSENT_ENR_2) {
                    String consentName = consent.type + ' ' + consent.department;
                    if (consent.value == true) {
                        contactToUpdate = MarketingConsentManager.updateConsentOnContact(consent.department, contactToUpdate, CatalogManager.getApiADONameFromConsentName(consentName));
                    }
                    else {
                        contactToUpdate = MarketingConsentManager.removeConsentOnContact(consent.department, contactToUpdate, CatalogManager.getApiADONameFromConsentName(consentName));
                    }
                }
            }
        }

        return contactToUpdate;
    }
    
    private static IntegrationManager.Contact_JSON parse(String jsonBody) {
        return (IntegrationManager.Contact_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.Contact_JSON.class);
    }
}