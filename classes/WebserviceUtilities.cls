/**
*   @author         Mateusz Pruszyński, Wojciech Słodziak
*   @description    Utility class, that is used to store methods to be called from js buttons
**/

global with sharing class WebserviceUtilities {


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- COMMON -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // Method that check whether user has edit access on record
    webservice static Boolean hasEditAccess(Id recordId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId() AND RecordId = :recordId
        ];

        return ura.HasEditAccess;
    }


    /* Sebastian Łasisz */
    // Method that check whether user has create permission on Price Books based on permission
    webservice static Boolean hasCreateAccessToPriceBooks() {
        List<PermissionSetAssignment> psa = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE PermissionSet.Name = :CommonUtility.PERMISSION_SET_PRICEBOOK
                AND AssigneeId = :UserInfo.getUserId()
        ];

        return psa.size() > 0;
    }


    /* Wojciech Słodziak */
    // Method that check whether user has edit access on record List
    webservice static Boolean hasEditAccessList(List<Id> recordIdList) {
        Boolean hasAccess = true;

        List<UserRecordAccess> uraList = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId() AND RecordId IN :recordIdList
        ];

        for (UserRecordAccess ura : uraList) {
            if (!ura.HasEditAccess) {
                hasAccess = false;
            }
        }

        return hasAccess;
    }


    /* Wojciech Słodziak */
    /* this method returns passed value in the correct format, based on user locale */
    global static String getNumberByUserLocale(Decimal valueToConvert) {
        return valueToConvert.format();
    }

    /* Sebastian Łasisz */
    // method used to retrieve account name from Id
    webservice static String getAccountNameFromId(String accountId) {
        Account acc = [SELECT Name FROM Account WHERE Id = :accountId];
        return acc.Name;
    }

    /* Sebastian Łasisz */
    // method used to upate contact and ipresso contact on email change
    webservice static String updateContactAndRepinIPressoContact(String contactJSON, String iPressoJSON, String iPressoContactsJSON) {
        contactJSON = contactJSON.replaceAll('\r', '__r').replaceAll('\n', '__n');

        Contact contactToUpdate = (Contact)System.JSON.deserialize(contactJSON, Contact.class);
        if (contactToUpdate.Change_History__c != null) {
            contactToUpdate.Change_History__c = contactToUpdate.Change_History__c.replaceAll('__r', '\r').replaceAll('__n', '\n');
        }
        Marketing_Campaign__c iPressoContact = (Marketing_Campaign__c)System.JSON.deserialize(iPressoJSON, Marketing_Campaign__c.class);
        List<Marketing_Campaign__c> iPressoContacts = (List<Marketing_Campaign__c>)System.JSON.deserialize(iPressoContactsJSON, List<Marketing_Campaign__c>.class);

        CommonUtility.preventChangingEmailTwice = true;
        return iPressoManager.repinIPressoContact(contactToUpdate, iPressoContact, iPressoContacts);
    }

    /* Wojciech Słodziak */
    webservice static String getPrefixWebservice(String sObjectApiName) {
        return CommonUtility.getPrefix(sObjectApiName);
    }

    /* Wojciech Słodziak */
    webservice static Id getRecordTypeId(string objectName, string recTypeDevName) {
        return CommonUtility.getRecordTypeId(objectName, recTypeDevName);
    }

    /* Wojciech Słodziak */
    /* method that returns list of field labels in user locale */
    public static String[] getLabelForField(String objectName, String[] fields) {
        for (integer i=0; i<fields.size(); i++) {
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType leadSchema = schemaMap.get(objectName);
            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

            fields[i] = fieldMap.get(fields[i]).getDescribe().getLabel();
        }
        return fields;
    }

    /* Wojciech Słodziak */
    // webservice to get picklist as HTML list (for JS Buttons)
    webservice static String getPicklistAsHTML(String objName, String fieldName, String idHTMLTag) {
        Schema.sObjectType sObj = Schema.getGlobalDescribe().get(objName);
        Schema.sObjectField sObjField = sObj.getDescribe().fields.getMap().get(fieldName);
        Schema.DescribeFieldResult fieldResult = sObjField.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        String html = '<select id="' + idHTMLTag + '">';
        for(Schema.PicklistEntry f : ple)
        {
            html += '<option value="' + f.getValue() + '">' + f.getLabel() + '</option>';
        }
        return html + '</select>';
    }

    /* Wojciech Słodziak */
    // webservice to update field values for specific object
    webservice static Boolean updateFieldValues(String objId, String sObjectName, String[] fieldNames, String[] fieldTypes, String[] fieldValues) {
        Schema.SObjectType objSchema = Schema.getGlobalDescribe().get(sObjectName);
        sObject obj = objSchema.newSObject(objId);
        for (Integer i = 0; i < fieldNames.size(); i++) {
            if (fieldTypes[i].toLowerCase() == 'string') {
                obj.put(fieldNames[i], fieldValues[i]);
            } else if (fieldTypes[i].toLowerCase() == 'boolean') {
                obj.put(fieldNames[i], Boolean.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'integer') {
                obj.put(fieldNames[i], Integer.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'decimal') {
                obj.put(fieldNames[i], Decimal.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'date') {
                String todaysDate = fieldValues[i];
                String[] dateComponents = todaysDate.split('\\.');
                Date today = Date.newInstance(Integer.valueOf(dateComponents[2]), Integer.valueOf(dateComponents[1]), Integer.valueOf(dateComponents[0]));
                obj.put(fieldNames[i], today);
            }
        }
        try {
            update obj;
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ SPECIFIC -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    /* Update Consents on Contact as accepted */
    webservice static Boolean markConsentsAsAccepted(String[] idArray, String dateString, String storageLocation) {
        List<Marketing_Campaign__c> consentsToAccept = [SELECT Id, Consent__c FROM Marketing_Campaign__c WHERE Id in :idArray];

        Date consentDate;
        if (dateString != null && dateString != '') {
            try {
                List<String> dateParts = dateString.split('\\.');
                consentDate = Date.newInstance(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[0]));
            } catch(Exception ex) {
                //wrong date input
                return false;
            }
        } else {
            consentDate = System.today();
        }

        for (Marketing_Campaign__c consent : consentsToAccept) {
            consent.Consent__c = CommonUtility.MARKETING_CONSENT_YES;
            consent.Consent_Source__c = CommonUtility.MARKETING_CONSENT_SRC_MANUAL;
            consent.Consent_Storage_Location__c = storageLocation;
            consent.Consent_Date__c = consentDate;
        }

        try {
            update consentsToAccept;
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }

    /* Wojciech Słodziak */
    /* Check if contact has all required data for enrollment */
    webservice static Boolean verifyContactDataCompletion(Id contactId) {
        Contact c = [
                SELECT Id, FirstName, LastName, Pesel__c, Phone, MobilePhone, Email, MailingStreet, MailingCity, MailingPostalCode
                FROM Contact
                WHERE Id = :contactId
        ];

        return c.FirstName != null && c.LastName != null
                && c.Pesel__c != null
                && (c.Phone != null || c.MobilePhone != null || c.Email != null)
                && c.MailingStreet != null && c.MailingCity != null && c.MailingPostalCode != null;
    }

    /* Wojciech Słodziak */
    // webservice to get Additional Charges options from Custom Settings to be selected for Enrollment
    // method checks which are already added and rejects them from result list 
    webservice static String getAdditionalChargesAsPicklistHTML(Id studyEnrId, String idHTMLTag) {
        Map<String, AdditionalCharges__c> customSettingMap = AdditionalCharges__c.getAll().clone();

        List<Payment__c> exisitingCharges = [
                SELECT Id, Name, Description__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :studyEnrId
                AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        for (Payment__c charge : exisitingCharges) {
            customSettingMap.remove(charge.Description__c);
        }

        if (customSettingMap.isEmpty()) {
            return null;
        }

        String html = '<select id="' + idHTMLTag + '">';
        for (AdditionalCharges__c charge : customSettingMap.values())
        {
            html += '<option value="' + charge.Name + '">' + charge.Name + ' (' + charge.Price__c.format() + ' '
                    + CommonUtility.getCurrencySymbolFromIso(CommonUtility.CURRENCY_CODE_PLN) + ')' + '</option>';
        }

        return html + '</select>';
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- RESPONSE MODEL ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class WS_Response {
        public Boolean result;
        public String data;
        public String message;

        public WS_Response(Boolean result, String message, String data) {
            this.result = result;
            this.message = message;
            this.data = data;
        }
    }

}