/**
*   @author         Wojciech Słodziak
*   @description    Utility class used for initial default configuration of custom settings
**/

public without sharing class CustomSettingDefault {

    // Wojciech Słodziak
    /* method that inserts default values into TrainingVerification__c in case there are none */
    public static void initTrainingVerification() {
        List<TrainingVerification__c> customSettingList = new List<TrainingVerification__c>();
        if (TrainingVerification__c.getInstance('ClosedOfferPaymentVerification') == null) {
            customSettingList.add(new TrainingVerification__c(
                Name = 'ClosedOfferPaymentVerification',
                Priority__c = CommonUtility.TASK_PRIO_HIGH,
                Reminder_Difference__c = 14,
                Status__c = CommonUtility.TASK_STATUS_NOTSTARTED,
                Subject__c = 'Weryfikacja wpływów płatności klientów'
            ));
        }
        if (TrainingVerification__c.getInstance('GroupVerification') == null) {
            customSettingList.add(new TrainingVerification__c(
                Name = 'GroupVerification',
                Priority__c = CommonUtility.TASK_PRIO_HIGH,
                Reminder_Difference__c = 7,
                Status__c = CommonUtility.TASK_STATUS_NOTSTARTED,
                Subject__c = 'Weryfikacja kompletności grupy'
            ));
        }
        if (TrainingVerification__c.getInstance('ListVerification') == null) {
            customSettingList.add(new TrainingVerification__c(
                Name = 'ListVerification',
                Priority__c = CommonUtility.TASK_PRIO_HIGH,
                Reminder_Difference__c = 1,
                Status__c = CommonUtility.TASK_STATUS_NOTSTARTED,
                Subject__c = 'Weryfikacja listy obecności'
            ));
        }
        if (TrainingVerification__c.getInstance('PaymentVerification') == null) {
            customSettingList.add(new TrainingVerification__c(
                Name = 'PaymentVerification',
                Priority__c = CommonUtility.TASK_PRIO_HIGH,
                Reminder_Difference__c = 10,
                Status__c = CommonUtility.TASK_STATUS_NOTSTARTED,
                Subject__c = 'Weryfikacja wpływów płatności klientów'
            ));
        }
        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }      
    }


    public static void initStudyAbbreviations() {
        initStudyKindAbbreviations();
        initStudyModeAbbreviations();
    }


    /* method that inserts default values into StudyKind_Abbreviation__c in case there are none */
    private static void initStudyKindAbbreviations() {
        List<StudyKind_Abbreviation__c> customSettingList = new List<StudyKind_Abbreviation__c>();

        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_BACHELOR) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_BACHELOR,
                Abbreviation__c = 'LIC'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_ENGINEERING) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_ENGINEERING,
                Abbreviation__c = 'INZ'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_MASTER) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_MASTER,
                Abbreviation__c = 'MAG'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_PGCOURSES) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_PGCOURSES,
                Abbreviation__c = 'SP'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_COM_MASTER) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_COM_MASTER,
                Abbreviation__c = 'UMAG'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_COM_PG) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_COM_PG,
                Abbreviation__c = 'USP'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_COM_ENG) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_COM_ENG,
                Abbreviation__c = 'UINZ'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_COM_MBA) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_COM_MBA,
                Abbreviation__c = 'MBA'
            ));
        }
        if (StudyKind_Abbreviation__c.getInstance(CommonUtility.OFFER_KIND_COM_EMBA) == null) {
            customSettingList.add(new StudyKind_Abbreviation__c(
                Name = CommonUtility.OFFER_KIND_COM_EMBA,
                Abbreviation__c = 'eMBA'
            ));
        }

        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* method that inserts default values into StudyMode_Abbreviation__c in case there are none */
    private static void initStudyModeAbbreviations() {
        List<StudyMode_Abbreviation__c> customSettingList = new List<StudyMode_Abbreviation__c>();

        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_PART_TIME) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_PART_TIME,
                Abbreviation__c = 'NST'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_FULL_TIME) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_FULL_TIME,
                Abbreviation__c = 'ST'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_WEEKEND) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_WEEKEND,
                Abbreviation__c = 'WEEK'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_BLENDED) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_BLENDED,
                Abbreviation__c = 'BL'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_ONLINE) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_ONLINE,
                Abbreviation__c = 'ONL'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_EVENING) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_EVENING,
                Abbreviation__c = 'WIE'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_OPEN) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_OPEN,
                Abbreviation__c = 'OTW'
            ));
        }
        if (StudyMode_Abbreviation__c.getInstance(CommonUtility.OFFER_MODE_CLOSED) == null) {
            customSettingList.add(new StudyMode_Abbreviation__c(
                Name = CommonUtility.OFFER_MODE_CLOSED,
                Abbreviation__c = 'ZAM'
            ));
        }

        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static DocumentReferenceNumber__c initDocumentReferenceNumber() {
        DocumentReferenceNumber__c documentReferecenNumber = DocumentReferenceNumber__c.getOrgDefaults();
        documentReferecenNumber.ReferenceNumber__c = 1;
        upsert documentReferecenNumber;

        return documentReferecenNumber;
    }

    public static void initEsbConfig() {
        ESB_Config__c esbConfig = ESB_Config__c.getOrgDefaults();
        
        esbConfig.Create_Candidate_End_Point__c = 'api/v0/candidate';
        esbConfig.Create_Didactics_End_Point__c = 'didactics/create';
        esbConfig.Create_Payment_Templates_End_Point__c = 'notimplemeneted';
        esbConfig.Import_Scoring_From_iPresso_End_Point__c = 'notimplemented';
        esbConfig.Sync_Consents_With_iPresso__c = 'notimplemented';
        esbConfig.Google_Calendar_Event__c = 'notimplemented';
        esbConfig.Service_Url__c = '0.0.0.0:8080';
        esbConfig.Enrollment_Link__c = 'http://www.google.com';
        esbConfig.Retrieve_Arrears_End_Point__c = 'notimplemented';

        try {
            upsert esbConfig;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static void initDidacticsCampaign() {
        List<Campaign_Details__c> customSettingList = new List<Campaign_Details__c>();

        if (Campaign_Details__c.getInstance(CommonUtility.OFFER_MODE_PART_TIME) == null) {
            customSettingList.add(new Campaign_Details__c(
                Name = 'Szczegóły kampanii',
                API_Name__c = 'Campaign_Details__c',
                RecordType__c = 'Campaign_Details'
            ));
        }

        if (Campaign_Details__c.getInstance(CommonUtility.OFFER_MODE_PART_TIME) == null) {
            customSettingList.add(new Campaign_Details__c(
                Name = 'Szczegóły kampanii 2',
                API_Name__c = 'Campaign_from_Campaign_Details_2__c',
                RecordType__c = 'Campaign_Details_2'
            ));
        }

        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static void initMembershipNationality() {
        List<Membership_Countries_EU_EFTA__c> customSettingList = new List<Membership_Countries_EU_EFTA__c>();

        if (Membership_Countries_EU_EFTA__c.getInstance(CommonUtility.CONTACT_CITIZENSHIP_POLISH) == null) {
            customSettingList.add(new Membership_Countries_EU_EFTA__c(
                Name = CommonUtility.CONTACT_CITIZENSHIP_POLISH
            ));
        }

        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static void initAdditionalCharges() {
        List<AdditionalCharges__c> customSettingList = new List<AdditionalCharges__c>();

        if (AdditionalCharges__c.getInstance(RecordVals.CS_AC_NAME_STUDENTCARD) == null) {
            customSettingList.add(new AdditionalCharges__c(
                Name = RecordVals.CS_AC_NAME_STUDENTCARD,
                Price__c = 17
            ));
        }

        try {
            if (!customSettingList.isEmpty()) {
                upsert customSettingList;
            }
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static void initIPressoLastScoringRetrievalDate() {
        iPresso_Last_Scoring_Retrival_Date__c iPressoScoring = new iPresso_Last_Scoring_Retrival_Date__c();
        iPressoScoring.Date_From__c = System.now();

        try {
            upsert iPressoScoring;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }
}