@isTest
private class ZTestTuitionSystemUnenrolled {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    @isTest static void test_generateTuitionSystem_enrollment_fixed() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
                    Number_of_Semesters__c = 5
            ), true);

            Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED,
                    Course_or_Specialty_Offer__c = courseOffer.Id
            ), true);

            Test.startTest();
            DocGen_GenerateTuitionSystemUnenrolled tuitionSystem = new DocGen_GenerateTuitionSystemUnenrolled();

            String result = DocGen_GenerateTuitionSystemUnenrolled.executeMethod(studyEnrollment.Id, null, null);
            Test.stopTest();

            System.assertEquals(result, '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>');
        }
    }
    
    @isTest static void test_generateTuitionSystem_enrollment_graded() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                    Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
                    Number_of_Semesters__c = 5
            ), true);

            Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                    Course_or_Specialty_Offer__c = courseOffer.Id
            ), true);

            Test.startTest();
            String result = DocGen_GenerateTuitionSystemUnenrolled.executeMethod(studyEnrollment.Id, null, null);
            Test.stopTest();

            System.assertEquals(result, '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>');
        }
    }

    @isTest static void test_generateTuitionSystem_documentEenrollment_fixed() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
                    Number_of_Semesters__c = 5
            ), true);

            Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED,
                    Course_or_Specialty_Offer__c = courseOffer.Id
            ), true);

            Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                    Enrollment_from_Documents__c = studyEnrollment.Id
            ), true);

            Test.startTest();
            DocGen_GenerateTuitionSystemUnenrolled tuitionSystem = new DocGen_GenerateTuitionSystemUnenrolled();

            String result = DocGen_GenerateTuitionSystemUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            Test.stopTest();

            System.assertEquals(result, '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>');
        }
    }
    
    @isTest static void test_generateTuitionSystem_documentEnrollment_graded() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                    Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
                    Number_of_Semesters__c = 5
            ), true);

            Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                    Course_or_Specialty_Offer__c = courseOffer.Id
            ), true);

            Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                    Enrollment_from_Documents__c = studyEnrollment.Id
            ), true);

            Test.startTest();
            String result = DocGen_GenerateTuitionSystemUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            Test.stopTest();

            System.assertEquals(result, '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>');
        }
    }
}