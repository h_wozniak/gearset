/**
 * Created by Sebastian.Lasisz on 12.04.2018.
 */

global with sharing class UnenrolledDocReminderBatch  implements Database.Batchable<sObject> {
    Date dayOfEnrollment;

    global UnenrolledDocReminderBatch(Integer daysAfterEnrollment) {
        dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Study_Start_Date__c, Language_of_Enrollment__c, University_Name__c, Unenrolled__c, WasIs_Unenrolled__c, Degree__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND (Unenrolled__c = true
                OR WasIs_Unenrolled__c = true)
                AND (Degree__c = :CommonUtility.OFFER_DEGREE_II
                OR Degree__c = :CommonUtility.OFFER_DEGREE_II_PG)
                AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
                OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrIds = new Set<Id>();
        Map<Id, Date> dateAssigment = EmailManager_UnenrolledDocListReminder.generateUnenrolledAgreementTill(enrList);

        for (Enrollment__c enr : enrList) {
            System.debug(dateAssigment.get(enr.Id));
            System.debug(System.today());
            if (dateAssigment.get(enr.Id) == System.today()) {
                enrIds.add(enr.Id);
            }
        }

        EmailManager_UnenrolledDocListReminder.sendDocumentListReminderUnenrolled(enrIds);
    }

    global void finish(Database.BatchableContext BC) {}

}