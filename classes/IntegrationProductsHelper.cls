/**
*   @author         Sebastian Łasisz
*   @description    Class for creating catalog of products. 
**/

public with sharing class IntegrationProductsHelper {

    public static String prepareProductsToSend() {
        /* PREPARE QUERIES */
        String result = ('Query start ' + System.now());
        List<Offer__c> courseOffers = prepareCourseList();
        List<Offer__c> specialtiesOffers = prepareSpecialtiesList();        
        List<Offer__c> specializationsOffers = prepareSpecializationList();
        List<Offer__c> languagesOnOffer = prepareLanguageList();
        List<Offer__c> priceBooks = preparePriceBookList();
        List<Catalog__c> preambles = preparePreambles();

        /* PREPARE HELPER MAPS */
        result += ('Mapping start ' + System.now());
        Map<Id, List<Language_JSON>> offerToLanguageMap = prepareOfferLanguageMap(languagesOnOffer);
        Map<Id, List<Offer__c>> offerToPriceBookMap = preparePriceBookMap(priceBooks);
        Map<Id, List<SpecializationOffer_JSON>> offerToSpecializationMap = prepareSpecjalizationMap(specializationsOffers);
        Map<Id, List<SpecialtyOffer_JSON>> offerToSpecialtyMap = prepareSpecialtyMap(specialtiesOffers, offerToLanguageMap, offerToPriceBookMap, offerToSpecializationMap);
        result += ('Mapping stop ' + System.now());

        /* PREPARE JSON */
        result += ('Prepare JSON start ' + System.now());
        List<CourseOffer_JSON> offerJSONList = prepareCourseJSON(courseOffers, offerToLanguageMap, offerToPriceBookMap, offerToSpecializationMap, offerToSpecialtyMap, preambles);
        result += ('Prepare JSON stop ' + System.now());

        /* SERIALIZE JSON */
        result += ('Serialize start ' + System.now());
        String jsonSerialized = JSON.serialize(offerJSONList);
        result += ('Serialize stop ' + System.now());

        System.debug('result ' + result);
        return jsonSerialized;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HELPER METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static Map<Id, List<Language_JSON>> prepareOfferLanguageMap(List<Offer__c> languagesOnOffer) {
        Map<Id, List<Language_JSON>> offerToLanguageMap = new Map<Id, List<Language_JSON>>();

        for (Offer__c language : languagesOnOffer) {
            List<Language_JSON> languageList = offerToLanguageMap.get(language.Course_Offer_from_Language__c);

            Language_JSON languageJSON = new Language_JSON();
            languageJSON.language = language.Language__c;
            languageJSON.mandatory = language.Mandatory__c;

            if (languageList == null) {
                offerToLanguageMap.put(language.Course_Offer_from_Language__c, new List<Language_JSON> { languageJSON });
            }
            else {
                offerToLanguageMap.get(language.Course_Offer_from_Language__c).add(languageJSON);
            }
        }

        return offerToLanguageMap;
    }

    public static Map<Id, List<Offer__c>> preparePriceBookMap(List<Offer__c> priceBooks) {
        Map<Id, List<Offer__c>> offerToPriceBookMap = new Map<Id, List<Offer__c>>();

        for (Offer__c priceBook : priceBooks) {
            List<Offer__c> priceBookList = offerToPriceBookMap.get(priceBook.Offer_from_Price_Book__c);
            if (priceBookList == null) {
                offerToPriceBookMap.put(priceBook.Offer_from_Price_Book__c, new List<Offer__c> { priceBook});
            }
            else {
                offerToPriceBookMap.get(priceBook.Offer_from_Price_Book__c).add(priceBook);
            }
        }

        return offerToPriceBookMap;
    }

    public static Map<Id, List<SpecializationOffer_JSON>> prepareSpecjalizationMap(List<Offer__c> specializationsOffers) {
        Map<Id, List<SpecializationOffer_JSON>> offerToSpecializationMap = new Map<Id, List<SpecializationOffer_JSON>>();

        for (Offer__c specialization : specializationsOffers) {
            SpecializationOffer_JSON specializationOnOffer = prepareSpecializationOnoffer(specialization);

            List<SpecializationOffer_JSON> specializationList = offerToSpecializationMap.get(specialization.Offer_from_Specialization__c);
            if (specializationList == null) {
                offerToSpecializationMap.put(specialization.Offer_from_Specialization__c, new List<SpecializationOffer_JSON> { specializationOnOffer});
            }
            else {
                offerToSpecializationMap.get(specialization.Offer_from_Specialization__c).add(specializationOnOffer);
            }
        }

        return offerToSpecializationMap;
    }

    public static Map<Id, List<SpecialtyOffer_JSON>> prepareSpecialtyMap(List<Offer__c> specialtiesOffers, Map<Id, List<Language_JSON>> offerToLanguageMap, Map<Id, List<Offer__c>> offerToPriceBookMap, Map<Id, List<SpecializationOffer_JSON>> offerToSpecializationMap) {

        Map<Id, List<SpecialtyOffer_JSON>> offerToSpecialtyMap = new Map<Id, List<SpecialtyOffer_JSON>>();

        for (Offer__c specialty : specialtiesOffers) {
            List<SpecializationOffer_JSON> specializations = offerToSpecializationMap.get(specialty.Id) != null ? 
                                                             offerToSpecializationMap.get(specialty.Id) : 
                                                             new List<SpecializationOffer_JSON>();

            List<Language_JSON> languages = offerToLanguageMap.get(specialty.Course_Offer_from_Specialty__c) != null ? 
                                     offerToLanguageMap.get(specialty.Course_Offer_from_Specialty__c) : 
                                     new List<Language_JSON>();

            List<Offer__c> priceBooksOnOffer  = offerToPriceBookMap.get(specialty.Id) != null ? offerToPriceBookMap.get(specialty.Id) : new List<Offer__c>();
            SpecialtyOffer_JSON specialtyOnOffer = prepareSpecialtyOnOnffer(specialty, specializations, languages, priceBooksOnOffer);
            
            List<SpecialtyOffer_JSON> specialtyList = offerToSpecialtyMap.get(specialty.Course_Offer_from_Specialty__c);
            if (specialtyList == null) {
                offerToSpecialtyMap.put(specialty.Course_Offer_from_Specialty__c, new List<SpecialtyOffer_JSON> { specialtyOnOffer});
            }
            else {
                offerToSpecialtyMap.get(specialty.Course_Offer_from_Specialty__c).add(specialtyOnOffer);
            }
        }

        return offerToSpecialtyMap;
    }

    public static List<CourseOffer_JSON> prepareCourseJSON(List<Offer__c> courseOffers, Map<Id, List<Language_JSON>> offerToLanguageMap, Map<Id, List<Offer__c>> offerToPriceBookMap, Map<Id, List<SpecializationOffer_JSON>> offerToSpecializationMap, Map<Id, List<SpecialtyOffer_JSON>> offerToSpecialtyMap, List<Catalog__c> preambles) {

        List<CourseOffer_JSON> offerJSONList = new List<CourseOffer_JSON>();

        for (Offer__c courseOffer : courseOffers) {
            List<SpecializationOffer_JSON> specializations = offerToSpecializationMap.get(courseOffer.Id) != null ? 
                                                             offerToSpecializationMap.get(courseOffer.Id) : 
                                                             new List<SpecializationOffer_JSON>();

            List<SpecialtyOffer_JSON> specialties = offerToSpecialtyMap.get(courseOffer.Id) != null ? offerToSpecialtyMap.get(courseOffer.Id) : new List<SpecialtyOffer_JSON>();
            List<Language_JSON> languages = offerToLanguageMap.get(courseOffer.Id) != null ? offerToLanguageMap.get(courseOffer.Id) : new List<Language_JSON>();
            List<Offer__c> priceBooksOnOffer  = offerToPriceBookMap.get(courseOffer.Id) != null ? offerToPriceBookMap.get(courseOffer.Id) : new List<Offer__c>();

            offerJSONList.add(IntegrationProductsHelper.buildProductsJSON(courseOffer, specializations, specialties, preambles, languages, priceBooksOnOffer));
        }

        return offerJSONList;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- QUERY METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static List<Offer__c> prepareCourseList() {        
        return [SELECT Id, Name, Trade_Name__c, University_Name__c, Configuration_Status_Course_Offer__c, Degree__c, Kind__c, Mode__c,
                   Leading_Language__c, Number_of_Semesters__c, Active_from__c, Active_to__c, Number_of_Languages__c,
                   University_Study_Offer_from_Course__r.Study_Start_Date__c, Guided_group__c, Enrollment_Link_Id__c,
                   Higher_Semester_Enrollment_Avaliable__c, Offer_Number__c
                FROM Offer__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
                AND Active_to__c >= :Date.today()
                AND Dont_show_in_ZPI__c = false
        ];
    }

    public static List<Offer__c> prepareSpecialtiesList() {
        return [SELECT Id, Name, Specialty_Trade_Name__c, Configuration_Status_Specialty__c, Course_Offer_from_Specialty__c, Leading_Language__c, 
                   Field__c, Number_of_Semesters__c, Active_from__c, Active_to__c, Guided_group__c, Enrollment_Link_Id__c
                FROM Offer__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
                AND Active_to__c >= :Date.today()
                AND Dont_show_in_ZPI__c = false
        ];
    }

    public static List<Offer__c> prepareSpecializationList() {
        return [SELECT Id, Name, Short_Name__c, Offer_from_Specialization__c, Leading_Language__c, Field__c, Specialization__r.Field__c,
                   Specialization__r.Name, Specialization__r.Short_Name__c, Specialization__r.Leading_Language__c, Specialization__r.Trade_Name__c
                FROM Offer__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_SPECIALIZATION)
                AND Dont_show_in_ZPI__c = false
        ];
    }

    public static List<Offer__c> prepareLanguageList() {
        return [SELECT Language__c, Course_Offer_from_Language__c, Mandatory__c
                FROM Offer__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE)
        ];
    }

    public static List<Offer__c> preparePriceBookList() {
        return [SELECT Installment_Variants__c, Price_Book_Type__c, Graded_Tuition__c, Onetime_Price__c, Offer_from_Price_Book__c
                FROM Offer__c 
                WHERE Currently_in_use__c = true 
                AND Price_Book_Type__c != :CommonUtility.OFFER_PBTYPE_SPECIAL
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG))
        ];
    }

    public static List<Catalog__c> preparePreambles() {
        return [SELECT Id, Name
                FROM Catalog__c
                WHERE Type__c = :CommonUtility.CATALOG_TYPE_PREAMBLE
        ];
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- JSON METHODS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static CourseOffer_JSON buildProductsJSON(Offer__c courseOffer, List<SpecializationOffer_JSON> specializationsOffers, List<SpecialtyOffer_JSON> specialtiesOffers, List<Catalog__c> preambles, List<Language_JSON> languages, List<Offer__c> pricebooks) {
        CourseOffer_JSON courseOfferToParse = new CourseOffer_JSON();
        courseOfferToParse.product_name = courseOffer.Name;
        courseOfferToParse.product_tradename = courseOffer.Trade_Name__c;
        courseOfferToParse.university_name = courseOffer.University_Name__c;
        courseOfferToParse.guided_group = courseOffer.Guided_group__c;
        if (courseOffer.Guided_group__c) {
            courseOfferToParse.url_id = courseOffer.Enrollment_Link_Id__c;
        }
        courseOfferToParse.enrollment_enable = courseOffer.Configuration_Status_Course_Offer__c && courseOffer.Active_to__c >= Date.today() && courseOffer.Active_from__c <= Date.today();
        courseOfferToParse.degree = IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(courseOffer.Degree__c);
        courseOfferToParse.kind = IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(courseOffer.Kind__c);
        courseOfferToParse.mode = IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(courseOffer.Mode__c);
        courseOfferToParse.leading_language = courseOffer.Leading_Language__c;
        courseOfferToParse.number_of_languages = Integer.valueOf(courseOffer.Number_of_Languages__c);
        courseOfferToParse.semesters = Integer.valueOf(courseOffer.Number_of_Semesters__c);
        courseOfferToParse = prepareTuitionSystemAndInstallmentsOnOffer(courseOfferToParse, pricebooks);
        courseOfferToParse.languages = languages;
        courseOfferToParse.specialities = specialtiesOffers;
        courseOfferToParse.specializations = specializationsOffers;
        courseOfferToParse.preamble = preparePreambleForCourseOffer(courseOffer.University_Name__c, preambles);
        courseOfferToParse.offer_number = Integer.valueOf(courseOffer.Offer_Number__c);
        if (courseOffer.University_Study_Offer_from_Course__r.Study_Start_Date__c != null) {
            courseOfferToParse.study_start_date = String.valueOf(courseOffer.University_Study_Offer_from_Course__r.Study_Start_Date__c.year()) + '-' + 
                                                  (courseOffer.University_Study_Offer_from_Course__r.Study_Start_Date__c.month() >= 10 
                                                  ? String.valueOf(courseOffer.University_Study_Offer_from_Course__r.Study_Start_Date__c.month())
                                                  : '0' + String.valueOf(courseOffer.University_Study_Offer_from_Course__r.Study_Start_Date__c.month()));
        }

        return courseOfferToParse;
    }

    public static String preparePreambleForCourseOffer(String universityName, List<Catalog__c> preambles) {
        String preambleName = '';
        String preambleId = '';

        if (universityName == RecordVals.WSB_NAME_WRO || universityName == RecordVals.WSB_NAME_OPO) {
            preambleName = RecordVals.WSB_NAME_WRO.split(' ')[1];
        }
        else if (universityName == RecordVals.WSB_NAME_GDA || universityName == RecordVals.WSB_NAME_GDY) {
            preambleName = RecordVals.WSB_NAME_GDA.split(' ')[1];
        }
        else if (universityName == RecordVals.WSB_NAME_TOR || universityName == RecordVals.WSB_NAME_BYD) {
            preambleName = RecordVals.WSB_NAME_TOR.split(' ')[1];
        }
        else {
            preambleName = RecordVals.WSB_NAME_POZ.split(' ')[1];
        }

        for (Catalog__c preamble : preambles) {
            if (preamble.Name.contains(preambleName)) {
                preambleId = preamble.Id;
            }
        }

        return preambleId;
    }

    public static SpecialtyOffer_JSON prepareSpecialtyOnOnffer(Offer__c specialty, List<SpecializationOffer_JSON> specializationsOffers, List<Language_JSON> languageList, List<Offer__c> pricebooks) {
        SpecialtyOffer_JSON specialtyOnOffer = new SpecialtyOffer_JSON();
        specialtyOnOffer.name = specialty.Name;
        specialtyOnOffer.tradename = specialty.Specialty_Trade_Name__c;
        specialtyOnOffer.enable = specialty.Configuration_Status_Specialty__c && specialty.Active_to__c >= Date.today() && specialty.Active_from__c <= Date.today();
        specialtyOnOffer.leading_language = specialty.Leading_Language__c;
        specialtyOnOffer.guided_group = specialty.Guided_group__c;
        if (specialty.Guided_group__c) {
            specialtyOnOffer.url_id = specialty.Enrollment_Link_Id__c;
        }
        specialtyOnOffer.languages = languageList;
        specialtyOnOffer.field = specialty.Field__c;
        specialtyOnOffer = prepareTuitionSystemAndInstallmentsOnSpecialtyOffer(specialtyOnOffer, pricebooks);
        specialtyOnOffer.specializations = specializationsOffers;

        return specialtyOnOffer;
    }

    public static SpecializationOffer_JSON prepareSpecializationOnoffer(Offer__c specialization) {
        SpecializationOffer_JSON specializationOnOffer = new SpecializationOffer_JSON();
        specializationOnOffer.name = specialization.Name;
        specializationOnOffer.shortname = specialization.Specialization__r.Short_Name__c;
        specializationOnOffer.leading_language = specialization.Specialization__r.Leading_Language__c;
        specializationOnOffer.field = specialization.Specialization__r.Field__c;
        specializationOnOffer.trade_name = specialization.Specialization__r.Trade_Name__c;

        return specializationOnOffer;
    }

    public static SpecialtyOffer_JSON prepareTuitionSystemAndInstallmentsOnSpecialtyOffer(SpecialtyOffer_JSON courseOfferToParse, List<Offer__c> priceBooks) {
        List<String> tuitionSystemDefault = new List<String>();
        List<String> tuitionSystemForeigner = new List<String>();
        String installmentsDefault = '';
        String installmentsForeigner = '';

        if (priceBooks != null) {
            for (Offer__c priceBook : priceBooks) {
                if (priceBook.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD) {
                    if (priceBook.Onetime_Price__c != null) tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME));
                    tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED));
                    if (priceBook.Graded_Tuition__c == true) tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED));
                    installmentsDefault = priceBook.Installment_Variants__c;
                }
                else if (priceBook.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_FOREIGNERS) {
                    if (priceBook.Onetime_Price__c != null) tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME));
                    tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED));
                    if (priceBook.Graded_Tuition__c == true) tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED));
                    installmentsForeigner = priceBook.Installment_Variants__c;
                }
            }
        }

        courseOfferToParse.tuition_system_default = tuitionSystemDefault;
        courseOfferToParse.tuition_system_foreigner = tuitionSystemForeigner;

        List<Integer> installmentsDefaultList = new List<Integer>();
        if (installmentsDefault != null && installmentsDefault != '') {
            for (String installment : installmentsDefault.split(';')) {
                installmentsDefaultList.add(Integer.valueOf(installment));
            }
        }
        courseOfferToParse.installments = installmentsDefaultList;

        List<Integer> installmentsForeignerList = new List<Integer>();
        if (installmentsForeigner != null && installmentsForeigner != '') {
            for (String installment : installmentsForeigner.split(';')) {
                installmentsForeignerList.add(Integer.valueOf(installment));
            }
        }
        courseOfferToParse.installments_foreigner = installmentsForeignerList;

        return courseOfferToParse;
    }

    public static CourseOffer_JSON prepareTuitionSystemAndInstallmentsOnOffer(CourseOffer_JSON courseOfferToParse, List<Offer__c> priceBooks) {
        List<String> tuitionSystemDefault = new List<String>();
        List<String> tuitionSystemForeigner = new List<String>();
        String installmentsDefault = '';
        String installmentsForeigner = '';

        if (priceBooks != null) {
            for (Offer__c priceBook : priceBooks) {
                if (priceBook.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD) {
                    if (priceBook.Onetime_Price__c != null) tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME));
                    tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED));
                    if (priceBook.Graded_Tuition__c) tuitionSystemDefault.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED));
                    installmentsDefault = priceBook.Installment_Variants__c;
                }
                else if (priceBook.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_FOREIGNERS) {
                    if (priceBook.Onetime_Price__c != null) tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME));
                    tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED));
                    if (priceBook.Graded_Tuition__c) tuitionSystemForeigner.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED));
                    installmentsForeigner = priceBook.Installment_Variants__c;
                }
            }
        }

        courseOfferToParse.tuition_system_default = tuitionSystemDefault;
        courseOfferToParse.tuition_system_foreigner = tuitionSystemForeigner;

        List<Integer> installmentsDefaultList = new List<Integer>();
        if (installmentsDefault != null && installmentsDefault != '') {
            for (String installment : installmentsDefault.split(';')) {
                installmentsDefaultList.add(Integer.valueOf(installment));
            }
        }
        courseOfferToParse.installments = installmentsDefaultList;

        List<Integer> installmentsForeignerList = new List<Integer>();
        if (installmentsForeigner != null && installmentsForeigner != '') {
            for (String installment : installmentsForeigner.split(';')) {
                installmentsForeignerList.add(Integer.valueOf(installment));
            }
        }
        courseOfferToParse.installments_foreigner = installmentsForeignerList;

        return courseOfferToParse;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class CourseOffer_JSON {
        public String product_name;
        public String product_tradename;
        public Boolean enrollment_only_on_speciality;
        public Boolean guided_group;
        public String url_id;
        public String university_name;
        public Boolean enrollment_enable;
        public Integer number_of_languages;
        public String degree;
        public String kind;
        public String mode;
        public Integer semesters;
        public String leading_language;
        public List<String> tuition_system_default;
        public List<String> tuition_system_foreigner;
        public List<Integer> installments;
        public List<Integer> installments_foreigner;
        public List<Language_JSON> languages;
        public List<SpecialtyOffer_JSON> specialities;
        public List<SpecializationOffer_JSON> specializations;
        public String preamble;
        public String study_start_date;
        public Integer offer_number;
    }

    public class Language_JSON {
        public String language;
        public Boolean mandatory;
    }

    public class SpecialtyOffer_JSON {
        public String name;
        public String tradename;
        public Boolean enable;
        public String leading_language;
        public Boolean guided_group;
        public String url_id;
        public String field;
        public List<String> tuition_system_default;
        public List<String> tuition_system_foreigner;
        public List<Integer> installments;
        public List<Integer> installments_foreigner;
        public List<Language_JSON> languages;
        public List<SpecializationOffer_JSON> specializations;
    }

    public class SpecializationOffer_JSON {
        public String name;
        public String shortname;
        public String leading_language;
        public String field;
        public String trade_name;
    }
}