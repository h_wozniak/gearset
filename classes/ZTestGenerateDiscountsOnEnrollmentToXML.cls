@isTest
private class ZTestGenerateDiscountsOnEnrollmentToXML {
    
    @isTest static void testGenerateXMLWithoutDiscounts() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        
        enxoodocgen__Document_Template__c agreementTemplate = new enxoodocgen__Document_Template__c();
        agreementTemplate.Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateXMLWithDiscount() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        ZDataTestUtility.createManualDiscount(new Discount__c(
            Enrollment__c = studyEnrollment.Id, 
            Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), true);
        
        enxoodocgen__Document_Template__c agreementTemplate = new enxoodocgen__Document_Template__c();
        agreementTemplate.Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateXMLWithDiscounts() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        ZDataTestUtility.createManualDiscounts(new Discount__c(
            Enrollment__c = studyEnrollment.Id, 
            Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), 5, true);
        
        enxoodocgen__Document_Template__c agreementTemplate = new enxoodocgen__Document_Template__c();
        agreementTemplate.Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateXMLWithoutEnrollment() {
        Test.startTest();
        try {
            DocGen_GenerateDiscountsOnEnrollment.executeMethod(null, null, null);
        }
        catch(Exception e) {
            System.assert(e.getMessage().contains('List has no rows for assignment to SObject'));
        }
        Test.stopTest();
    }
    
    @isTest static void testGenerateXMLWithoutDiscountsPG() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        
        enxoodocgen__Document_Template__c agreementPGTemplate = new enxoodocgen__Document_Template__c();
        agreementPGTemplate.Name = '[Umowa wolny słuchacz]' + RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementPGTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementPGTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateXMLWithDiscountPG() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        ZDataTestUtility.createManualDiscount(new Discount__c(
            Enrollment__c = studyEnrollment.Id, 
            Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), true);
        
        enxoodocgen__Document_Template__c agreementPGTemplate = new enxoodocgen__Document_Template__c();
        agreementPGTemplate.Name = '[MBA]' + RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementPGTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementPGTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }

    @isTest static void testGenerateXMLWithDiscountPG_2() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH), true);

        Catalog__c doc = ZDataTestUtility.createDocument(new Catalog__c(Name = '[RU]Name'), true);
        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH, Document__c = doc.Id), true);
        ZDataTestUtility.createManualDiscount(new Discount__c(
                Enrollment__c = studyEnrollment.Id,
                Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), true);

        enxoodocgen__Document_Template__c agreementPGTemplate = new enxoodocgen__Document_Template__c();
        agreementPGTemplate.Name = '[Wyższe]' + RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementPGTemplate;

        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementPGTemplate.Id, null);
        Test.stopTest();

        System.assertNotEquals('', result);
    }

    @isTest static void testGenerateXMLWithDiscountPG_3() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN), true);

        Catalog__c doc = ZDataTestUtility.createDocument(new Catalog__c(Name = '[EN]Name'), true);
        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN, Document__c = doc.Id), true);
        ZDataTestUtility.createManualDiscount(new Discount__c(
                Enrollment__c = studyEnrollment.Id,
                Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), true);

        enxoodocgen__Document_Template__c agreementPGTemplate = new enxoodocgen__Document_Template__c();
        agreementPGTemplate.Name = '[PG]' + RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementPGTemplate;

        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementPGTemplate.Id, null);
        Test.stopTest();

        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateXMLWithDiscountsPG() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);
        ZDataTestUtility.createManualDiscounts(new Discount__c(
            Enrollment__c = studyEnrollment.Id, 
            Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
        ), 5, true);
        
        enxoodocgen__Document_Template__c agreementPGTemplate = new enxoodocgen__Document_Template__c();
        agreementPGTemplate.Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT;
        insert agreementPGTemplate;
        
        Test.startTest();
        String result = DocGen_GenerateDiscountsOnEnrollment.executeMethod(studyEnrollment.Id, agreementPGTemplate.Id, null);
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
}