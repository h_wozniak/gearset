@isTest
private class ZTestUnenrolledManager {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- UNENROLLMENT PROCESS INIT ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest public static void testUnenrolledEnabled() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Test.startTest();

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Document__r.Name
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollments.size() > 0);
        
        Test.stopTest();
        studyEnr.Unenrolled__c = true;
        update studyEnr;

        List<Enrollment__c> documentEnrollmentsAfterEnrUpdate = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollmentsAfterEnrUpdate.size() > 0);
        

        List<Enrollment__c> documentsAfterUpdate = [
            SELECT Id, Document__c, For_delivery_in_Stage__c, Delivery_Deadline__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__c = :studyEnr.Id
        ];

        for (Enrollment__c doc : documentsAfterUpdate) {
            if (doc.Document__c == CatalogManager.getDocumentAgreementUnenrolledId()) {
                System.assertEquals(CommonUtility.ENROLLMENT_FDI_STAGE_COD, doc.For_delivery_in_Stage__c);
            }
        }
    }

    @isTest public static void testUnenrolledEnabled_agrExists() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Test.startTest();

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);        

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Document__r.Name
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        Test.stopTest();

        System.assert(documentEnrollments.size() > 0);
        studyEnr.Unenrolled__c = true;
        update studyEnr;

        List<Enrollment__c> documentEnrollmentsAfterEnrUpdate = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollmentsAfterEnrUpdate.size() > 0);
        

        List<Enrollment__c> documentsAfterUpdate = [
            SELECT Id, Document__c, For_delivery_in_Stage__c, Delivery_Deadline__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__c = :studyEnr.Id
        ];

        for (Enrollment__c doc : documentsAfterUpdate) {
            if (doc.Document__c == CatalogManager.getDocumentAgreementUnenrolledId()) {
                System.assertEquals(CommonUtility.ENROLLMENT_FDI_STAGE_COD, doc.For_delivery_in_Stage__c);
            }
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------- UNENROLLMENT PROCESS FINALIZATION --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest public static void testUnenrolled_Finalization() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Test.startTest();

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Document__r.Name
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollments.size() > 0);

        Test.stopTest();

        studyEnr.Unenrolled__c = true;
        studyEnr.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS;
        update studyEnr;
        studyEnr.Unenrolled__c = false;
        update studyEnr;

        List<Enrollment__c> documentEnrollmentsAfterEnrUpdate = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollmentsAfterEnrUpdate.size() > 0);
        

        List<Enrollment__c> documentsAfterUpdate = [
            SELECT Id, Document__c, For_delivery_in_Stage__c, Delivery_Deadline__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__c = :studyEnr.Id
        ];

        for (Enrollment__c doc : documentsAfterUpdate) {
            if (doc.Document__c == CatalogManager.getDocumentAgreementId()) {
                System.assertEquals(CommonUtility.ENROLLMENT_FDI_STAGE_COD, doc.For_delivery_in_Stage__c);
            }
        }
    }

    @isTest public static void testUnenrolled_Finalization_AgreExists() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Document__r.Name, Reference_Number__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        Decimal currentRefNumForAgr;
        for (Enrollment__c doc : documentEnrollments) {
            if (doc.Document__c == CatalogManager.getDocumentAgreementId()) {
                currentRefNumForAgr = doc.Reference_Number__c;
            }
        }
        
        Test.stopTest();

        System.assert(documentEnrollments.size() > 0);
        
        //studyEnr.Unenrolled__c = true;
        studyEnr.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS;
        update studyEnr;
        //studyEnr.Unenrolled__c = false;
        //update studyEnr;

        List<Enrollment__c> documentEnrollmentsAfterEnrUpdate = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollmentsAfterEnrUpdate.size() > 0);
        

        List<Enrollment__c> documentsAfterUpdate = [
            SELECT Id, Document__c, For_delivery_in_Stage__c, Delivery_Deadline__c, Reference_Number__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__c = :studyEnr.Id
        ];

        for (Enrollment__c doc : documentsAfterUpdate) {
            if (doc.Document__c == CatalogManager.getDocumentAgreementId()) {
                System.assertEquals(CommonUtility.ENROLLMENT_FDI_STAGE_COD, doc.For_delivery_in_Stage__c);
                System.assertEquals(currentRefNumForAgr, doc.Reference_Number__c);
            }
        }
    }

    @isTest public static void testUnenrolledDisabled() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Test.startTest();

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id, 
            Installments_per_Year__c = String.valueOf(10),
            Unenrolled__c = true
        ), true);

        List<Enrollment__c> documentEnrollments = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        System.assert(documentEnrollments.size() > 0);
        
        studyEnr.Unenrolled__c = false;

        Test.stopTest();        
        update studyEnr;

        Enrollment__c studyEnrollmentAfterUpdate = [SELECT Id, Unenrolled__c FROM Enrollment__c WHERE Id = :studyEnr.Id];
        System.assert(!studyEnrollmentAfterUpdate.Unenrolled__c);

        List<Enrollment__c> documentEnrollmentsAfterUpdate = [
            SELECT Id, Document__c, For_delivery_in_Stage__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__c = :studyEnrollmentAfterUpdate.Id
        ];

        for (Enrollment__c documentEnrollment : documentEnrollmentsAfterUpdate) {
            if (documentEnrollment.Document__c == CatalogManager.getDocumentAgreementUnenrolledId()) {
                System.assertEquals(CommonUtility.ENROLLMENT_FDI_STAGE_COD, documentEnrollment.For_delivery_in_Stage__c);
            }
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ DOCUMENT MODIFICATION ACTIONS ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest public static void test_updateStudyEnrollmentStatusOnUnerolledAgreementChange() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            Installments_per_Year__c = String.valueOf(10),
            Unenrolled__c = false
        ), true);

        Test.startTest();
        
        studyEnr.Unenrolled__c = true;
        update studyEnr;

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Acceptance_Date__c, Document_Accepted__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Document__c = :CatalogManager.getDocumentAgreementUnenrolledId()
        ];

        System.assertEquals(1, documentEnrollments.size());

        Enrollment__c unenrAgrDoc = documentEnrollments[0];
        unenrAgrDoc.Acceptance_Date__c = System.today();
        unenrAgrDoc.Document_Accepted__c = true;
        update unenrAgrDoc;

        Enrollment__c studyEnrAfter1 = [SELECT Unenrolled_Status__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT, studyEnrAfter1.Unenrolled_Status__c);

        unenrAgrDoc.Acceptance_Date__c = null;
        unenrAgrDoc.Document_Accepted__c = false;
        update unenrAgrDoc;

        Enrollment__c studyEnrAfter2 = [SELECT Unenrolled_Status__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(CommonUtility.ENROLLMENT_UNENR_STATUS_COD, studyEnrAfter2.Unenrolled_Status__c);
        Test.stopTest();
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- VALIDATIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest public static void testUnenrolledOnWrongDegree() {
        Test.startTest();

        Boolean error = false;
        try{
            Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(
                new Enrollment__c(
                    Unenrolled__c = true,
                    Degree__c = CommonUtility.OFFER_DEGREE_II, 
                    University_Name__c = RecordVals.WSB_NAME_TOR
            ), true);
        }
        catch(Exception e) {
            error = true;
        }
        System.assert(error);

        Test.stopTest();
    }

    @isTest public static void test_checkIfEnrollmentIsInProperStatusBeforeUnenrolledAgreement() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            Installments_per_Year__c = String.valueOf(10),
            Unenrolled__c = false
        ), true);
        
        studyEnr.Unenrolled__c = true;
        update studyEnr;

        Test.stopTest();

        List<Enrollment__c> documentEnrollments = [
            SELECT Id, Acceptance_Date__c, Document_Accepted__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Document__c = :CatalogManager.getDocumentAgreementUnenrolledId()
        ];

        System.assertEquals(1, documentEnrollments.size());
        System.assertEquals(documentEnrollments.get(0).Document_Accepted__c, false);

        Enrollment__c doc = documentEnrollments.get(0);
        doc.Document_Accepted__c = true;
        doc.Acceptance_Date__c = System.today();

        Boolean errorOccured = false;

        try {
            CommonUtility.skipDocumentValidations = false;
            update doc;
        }
        catch (Exception e) {
            errorOccured = true;
        }

        System.assertEquals(errorOccured, false);
    }

}