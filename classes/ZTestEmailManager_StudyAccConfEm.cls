@isTest
private class ZTestEmailManager_StudyAccConfEm {

    @isTest static void test_sendRequiredDocumentListReminder_I() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);

        Test.startTest();
        enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT;
        update enrollment;
        Test.stopTest();


        List<Task> taskList = [SELECT Id FROM Task WHERE Subject = :Label.title_StudyAcceptanceConfirmationSent];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_II() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);

        Test.startTest();
        enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT;
        update enrollment;
        Test.stopTest();
        

        List<Task> taskList = [SELECT Id FROM Task WHERE Subject = :Label.title_StudyAcceptanceConfirmationSent];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_PG() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_PG,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);

        Test.startTest();
        enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT;
        enrollment.Do_not_set_up_Student_Card__c = true;
        update enrollment;
        Test.stopTest();


        List<Task> taskList = [SELECT Id FROM Task WHERE Subject = :Label.title_StudyAcceptanceConfirmationSent];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_MBA() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_MBA,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);
        
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);

        Test.startTest();
        enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT;
        enrollment.Do_not_set_up_Student_Card__c = true;
        update enrollment;
        Test.stopTest();


        List<Task> taskList = [SELECT Id FROM Task WHERE Subject = :Label.title_StudyAcceptanceConfirmationSent];
        System.assert(!taskList.isEmpty());
    }

}