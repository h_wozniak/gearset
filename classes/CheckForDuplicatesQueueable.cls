/**
*   @author         Sebastian Łasisz
*   @description    This class is used to finalize deduplication process
**/

public with sharing class CheckForDuplicatesQueueable implements Queueable, Database.AllowsCallouts {
	List<Contact> contactsToCheckDuplicates;

	public CheckForDuplicatesQueueable(List<Contact> contactsToCheckDuplicates) {
		this.contactsToCheckDuplicates = contactsToCheckDuplicates;
	}

	public void execute(QueueableContext qc) {
		//CommonUtility.runMerginAsQueueable = true;
		if (contactsToCheckDuplicates != null && !contactsToCheckDuplicates.isEmpty()) {
			Set<Id> contactIds = new Set<Id>();
			for (Contact contact : contactsToCheckDuplicates) {
				contactIds.add(contact.Id);
			}

			List<Contact> contacts = [
					SELECT Id, RecordTypeId, FirstName, LastName, Pesel__c, Phone, MobilePhone, Email, Additional_Emails__c, Additional_Phones__c, Name,
							Is_Potential_Duplicate__c, Potential_Duplicate__c, Foreigner__c, Confirmed_Contact__c, Account.RecordTypeId, AccountId
					FROM Contact
					WHERE Id IN :contactIds
			];

			DuplicateManager.checkForDuplicates(contacts);
		}
	}
}