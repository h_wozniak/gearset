@isTest
private class ZTestGenerateStudyKind {
    
    @isTest static void testEnrollmentModeFullTime() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);
        
        Test.startTest();
        String result = DocGen_GenerateStudyKind.executeMethod(documentEnrollment.Id, null, null);
        Test.stopTest();
    }
    
    @isTest static void testEnrollmentModeWeekend() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
            Kind__c = CommonUtility.OFFER_KIND_ENGINEERING,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);
        
        Test.startTest();
        String result = DocGen_GenerateStudyKind.executeMethod(documentEnrollment.Id, null, null);
        Test.stopTest();
    }
    
}