/**
*   @author         Sebastian Łasisz
*   @description    Class used to inform user whether generation of document finished.
**/

public class DocumentGenerationStatusController {
	public Integer numberOfDocuments;
    public Integer enrDocSize = 0;
    public Boolean generationStatus = false;
    public Boolean showedPopup { get; set; }
    public String enrollmentType { get; set; }

    public Id enrollmentId;
    public List<Enrollment__c> enrollmentDocuments;
    public Enrollment__c enrollment;
    public List<String> documentsOnEnrollment = new List<String> { 
    	RecordVals.CATALOG_DOCUMENT_AGREEMENT, 
    	RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED,
    	RecordVals.CATALOG_DOCUMENT_ACCEPTANCE,
    	RecordVals.CATALOG_DOCUMENT_OATH,
    	RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
    };

    public DocumentGenerationStatusController (ApexPages.StandardController controller) {
    	enrollmentId = controller.getRecord().Id;

    	enrollment = [
    		SELECT Id, RecordTypeId, Current_File__c, LastModifiedDate
    		FROM Enrollment__c
    		WHERE Id = :enrollmentId
    	];

    	if (enrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)) {
	    	enrollmentDocuments = [
	    		SELECT Id, Current_File__c 
	    		FROM Enrollment__c 
	    		WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT) 
	    		AND Document__r.Name IN :documentsOnEnrollment
	    		AND Enrollment_from_Documents__c = :enrollmentId
	    		AND Current_File__c = :CommonUtility.DURING_GENERATION
	    	];

	    	numberOfDocuments = enrollmentDocuments.size();
	    	enrollmentType = CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY;
	    }
	    else {
	    	enrollmentType = CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT;
	    }
    }
			
    public PageReference checkDocumentGenerationStudy() {
    	List<Enrollment__c> enrDocs = [
    		SELECT Id, Current_File__c 
    		FROM Enrollment__c 
    		WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT) 
    		AND Document__r.Name IN :documentsOnEnrollment
    		AND Enrollment_from_Documents__c = :enrollmentId
    	];

    	for (Enrollment__c enrDoc : enrDocs) {
    		for (Enrollment__c enrollmentDocument : enrollmentDocuments) {
    			if (enrDoc.Id == enrollmentDocument.Id
    				&& enrDoc.Current_File__c != enrollmentDocument.Current_File__c) {
    				enrDocSize++;
    			}
    		}
    	}

    	if (enrDocSize == numberOfDocuments && numberOfDocuments > 0) {
    		generationStatus = true;
    	}

    	enrollmentDocuments = enrDocs;

        return null;
    }

    public PageReference checkDocumentGenerationDocument() {
    	Enrollment__c enrDoc = [
    		SELECT Id, RecordTypeId, Current_File__c, LastModifiedDate
    		FROM Enrollment__c
    		WHERE Id = :enrollmentId
    	];

    	if (enrDoc.Current_File__c != enrollment.Current_File__c
    		|| enrDoc.LastModifiedDate != enrollment.LastModifiedDate) {
    		generationStatus = true;
    	}

    	return null;

    }

    public Boolean getGenerationStatus() {
    	return generationStatus;
    }

    public PageReference disablePopup() {
    	showedPopup = true;
    	return null;
    }
}