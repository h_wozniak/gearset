/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to remove synced iPresso campaigns
**/

/**
*   To initiate the IntegrationIpressoDeleteTagsSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('IntegrationIpressoDeleteTagsSchedule', '0 0 8 * * ? *', new IntegrationIpressoDeleteTagsSchedule());
**/


global class IntegrationIpressoDeleteTagsSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
    	Map<Id, Marketing_Campaign__c> campaignsToDelete = new Map<Id, Marketing_Campaign__c>([
    		SELECT Id
    		FROM Marketing_Campaign__c
    		WHERE ((iPresso_Criteria_Type__c = :CommonUtility.MARKETING_CRITERIA_TYPE_TAGS AND TECH_Synchronized_All_Contacts__c = true)
    		OR (iPresso_Criteria_Type__c = :CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS AND Segment_Expiration_Date__c != null AND Segment_Expiration_Date__c <= :System.now()))
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_IPRESSO_SALES)
    	]);

        IntegrationIpressoDeleteTagsBatch campaignsBatch = new IntegrationIpressoDeleteTagsBatch(campaignsToDelete.keySet());
        Database.executebatch(campaignsBatch, 200);
    }
}