/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to Marketing_Campaign__c object related to Classifiers
**/

public without sharing class ClassifierManager {

    /**
     * Add Classifiers to Marketing Campaign
     * @author Sebastian Łasisz
     * @param  marketingSalesCampaignsToAddClassifiers List of Marketing Campaign to add list of classifiers.
     */
    public static void createClassifiersForMarketingCampaign(List<Marketing_Campaign__c> marketingCampaignsToAddClassifiers) {
        List<Marketing_Campaign__c> classifiersToAdd = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c marketingCampaigns : marketingCampaignsToAddClassifiers) {
            if (marketingCampaigns.Campaign_Kind__c == CommonUtility.MARKETING_CAMPAIGN_WELCOMECALL) {
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW, marketingCampaigns.Id, false, false));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_LOD, marketingCampaigns.Id, true, false));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_RESIGNATION, marketingCampaigns.Id, true, true));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_COMPLETIONSTARTED, marketingCampaigns.Id, true, false));
            }
            else if (marketingCampaigns.Campaign_Kind__c == CommonUtility.MARKETING_CAMPAIGN_LACKOFDOCUMENTS) {
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW, marketingCampaigns.Id, false, false));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_RESIGNATION, marketingCampaigns.Id, true, true)); 
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_COMPLETIONSTARTED, marketingCampaigns.Id, true, false));               
            }
            else if (marketingCampaigns.Campaign_Kind__c == CommonUtility.MARKETING_CAMPAIGN_CONFIRMATIONOFENROLLING) {
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW, marketingCampaigns.Id, false, false));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_FINISHEDSUCCESS, marketingCampaigns.Id, true, false));
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_FINISHEDFAILED, marketingCampaigns.Id, true, true));
            }
            else {
                classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW, marketingCampaigns.Id, false, false));                
            }
        }

        try {
            insert classifiersToAdd;
        }
        catch (Exception e) {
            if (!e.getMessage().contains(Label.msg_error_cantEditCampaign)) {
                ErrorLogger.log(e);
            }
        }
    }

    /**
     * Add Classifiers to Marketing Sales Campaign
     * @author Sebastian Łasisz
     * @param  marketingSalesCampaignsToAddClassifiers List of Marketing Campaign to add list of classifiers.
     */
    public static void createClassifiersForMarketingSalesCampaign(List<Marketing_Campaign__c> marketingSalesCampaignsToAddClassifiers) {
        List<Marketing_Campaign__c> classifiersToAdd = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c marketingCampaigns : marketingSalesCampaignsToAddClassifiers) {
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_NEW, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_LEAD, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_CONSENT_REJECT, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_WRONG_CONTACT, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ZPI_DECLARATION, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DUPLICATIOM, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ALEVEL_REDO, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_OTHER_DEFENSE_DATE, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_NOTINTERESTED, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_MAIL_VOICE, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_RECONTACT, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DB_REMOVAL, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_CLOSED_NO_CONTACT, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_NO_CONSENT_REGISTER, marketingCampaigns.Id, true, true));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ZPI_BY_CONSULTANT, marketingCampaigns.Id, true, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ZPI_LINK, marketingCampaigns.Id, false, false));
            classifiersToAdd.add(createClassifier(CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_MAIL, marketingCampaigns.Id, false, false));
        }

        try {
            insert classifiersToAdd;
        }
        catch (Exception e) {
            if (!e.getMessage().contains(Label.msg_error_cantEditCampaign)) {
                ErrorLogger.log(e);
            }
        }        
    }

    /**
     * Helper Method for External Classifier Creation
     * @author Sebastian Łasisz
     * @param  name             Name of classifier.
     * @param  campaignId       Id of Marketing Campaign that's going to be linked with newly created Classifier.
     * @param  classifierClosed Bools value whether classifier is marked as closed.
     * @param  classifierFailed Bools value whether classifier is marked as failed.
     * @return                  Newly created Classifier that's not inserted to database.
     */
    public static Marketing_Campaign__c createClassifier(String name, Id campaignId, Boolean classifierClosed, Boolean classifierFailed) {
        Marketing_Campaign__c classifier = new Marketing_Campaign__c();
        classifier.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER);
        classifier.Classifier_Name__c = name;
        classifier.Campaign_from_Classifier__c = campaignId;
        classifier.Classifier_Closing__c = classifierClosed;
        classifier.Classifier_Failed__c = classifierFailed;

        return classifier;
    }

    /**
     * Helper Method for External Classifier Creation
     * @author Sebastian Łasisz
     * @param  name             Name of classifier.
     * @param  campaignId       Id of Marketing Campaign that's going to be linked with newly created Classifier.
     * @param  classifierClosed Bools value whether classifier is marked as closed.
     * @param  classifierFailed Bools value whether classifier is marked as failed.
     * @param  classifierId     External id of classifier.
     * @return                  Newly created Classifier that's not inserted to database.
     */
    public static Marketing_Campaign__c createExternalClassifier(String name, Id campaignId, Boolean classifierClosed, Boolean classifierFailed, Integer classifierId) {
        Marketing_Campaign__c classifier = new Marketing_Campaign__c();
        classifier.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL_CLASSIFIER);
        classifier.Classifier_Name__c = name;
        classifier.External_Campaign_from_Classifier__c = campaignId;
        classifier.Classifier_Closing__c = classifierClosed;
        classifier.Classifier_Failed__c = classifierFailed;
        classifier.Classifier_System__c = true;
        classifier.Classifier_ID__c  = String.valueOf(classifierId);

        return classifier;
    }

    /**
     * Attach system classifiers to Campaign that is connected to FCC
     * @author Sebastian Łasisz
     * @param  marketingCampaignsToAttachSystemClassifiers Set of Marketing Campaign Ids that's used to create and map exteral system classifiers to main campaign.
     */
    public static void createSystemClassifiers(Set<Id> marketingCampaignsToAttachSystemClassifiers) {
        List<Marketing_Campaign__c> marketingCampaigns = [
            SELECT Id, Name, External_campaign__c,
            (SELECT Id, Name, Classifier_Name__c FROM Marketing2__r)
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)
            AND Id IN :marketingCampaignsToAttachSystemClassifiers
        ];

        Set<Id> externalCampaignIds = new Set<Id>();
        for (Marketing_Campaign__c marketingCampaignsToAttachSystemClassifier : marketingCampaigns) {
            externalCampaignIds.add(marketingCampaignsToAttachSystemClassifier.External_campaign__c);
        }

        List<Marketing_Campaign__c> externalCampaignsWithClassifiers = [
            SELECT Id, Name,
            (SELECT Id, Classifier_Name__c, Classifier_ID__c FROM External_Classifier__r)
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL)
            AND Id IN :externalCampaignIds
        ];

        List<String> namesOfClassifiersToAdd = new List<String> { 
            'vmd (system)', 'odebrany (system)', 'nieodebrany (system)', 'błędny (system)', 'zajęty (system)', 'zamknięcie (system)', 
            'abandon (system)', 'zamknięcie (system, predictive)', 'nieobsłużony (system)', 'recall (system, edycja klasyfikatora)', 
            'recall (system, usunięcie klasyfikatora)', 'zamknięcie (system, edycja klasyfikatora)', 
            'zamknięcie (system, usunięcie klasyfikatora)', 'pominięty (system)', 'transfer (system)', 
            'transfer na zewnątrz (system)', 'konsultacja (system)', 'konsultacja na zewnątrz (system)', 
            'zamknięcie (system, limit połączeń)', 'zamknięcie (system, czarna lista)', 'zamknięcie (system, hlr)', 
            'pds odmowa (system)', 'przekierowanie (system)', 'recall ticket (system)', 'oczekujące nieodebrane (system)'
        };

        List<Marketing_Campaign__c> classifiersToAdd = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c marketingCampaignsToAttachSystemClassifier : marketingCampaigns) {
            for (String classifierToAdd : namesOfClassifiersToAdd) {
                Boolean exists = false;
                for (Marketing_Campaign__c classifier : marketingCampaignsToAttachSystemClassifier.Marketing2__r) {
                    if (classifier.Classifier_Name__c == classifierToAdd) {
                        exists = true;
                    }
                }
                if (!exists) {
                    classifiersToAdd.add(createClassifier(classifierToAdd, marketingCampaignsToAttachSystemClassifier.Id, false, false));
                }
            }
        }


        for (Marketing_Campaign__c externalCampaignsWithClassifier : externalCampaignsWithClassifiers) {
            for (Marketing_Campaign__c externalClassifier : externalCampaignsWithClassifier.External_Classifier__r) {
                for (Marketing_Campaign__c classifierToAdd : classifiersToAdd) {
                    if (classifierToAdd.Classifier_Name__c == externalClassifier.Classifier_Name__c) {
                        classifierToAdd.Classifier_from_External_Classifier__c = externalClassifier.Id;
                    }
                }
            }
        }

        try {
            insert classifiersToAdd;
        }
        catch (Exception e) {
            if (!e.getMessage().contains(Label.msg_error_cantEditCampaign)) {
                ErrorLogger.log(e);
            }
        }
    }

    /**
     * Attach system classifiers to External Campaign
     * @author Sebastian Łasisz
     * @param  marketingCampaigns List of Marketing Campaigns that's going to have system external classifiers attached.
     */
    public static void createSystemExternalClassifiers(List<Marketing_Campaign__c> marketingCampaigns) {
        List<String> namesOfClassifiersToAdd = new List<String> { 
            'vmd (system)', 'odebrany (system)', 'nieodebrany (system)', 'błędny (system)', 'zajęty (system)', 'zamknięcie (system)', 
            'abandon (system)', 'zamknięcie (system, predictive)', 'nieobsłużony (system)', 'recall (system, edycja klasyfikatora)', 
            'recall (system, usunięcie klasyfikatora)', 'zamknięcie (system, edycja klasyfikatora)', 
            'zamknięcie (system, usunięcie klasyfikatora)', 'pominięty (system)', 'transfer (system)', 
            'transfer na zewnątrz (system)', 'konsultacja (system)', 'konsultacja na zewnątrz (system)', 
            'zamknięcie (system, limit połączeń)', 'zamknięcie (system, czarna lista)', 'zamknięcie (system, hlr)', 
            'pds odmowa (system)', 'przekierowanie (system)', 'recall ticket (system)', 'oczekujące nieodebrane (system)'
        };

        List<Marketing_Campaign__c> classifiersToAdd = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c marketingCampaignsToAttachSystemClassifier : marketingCampaigns) {
            Integer counter = 0;
            for (String classifierToAdd : namesOfClassifiersToAdd) {
                Integer properCounter = counter;
                if (properCounter == 0) properCounter = -25;
                if (properCounter == 24) properCounter = 25;
                if (properCounter == 23) properCounter = 24;
                classifiersToAdd.add(createExternalClassifier(classifierToAdd, marketingCampaignsToAttachSystemClassifier.Id, false, false, properCounter));
                counter++;
            }
        }

        try {
            insert classifiersToAdd;
        }
        catch (Exception e) {
            if (!e.getMessage().contains(Label.msg_error_cantEditCampaign)) {
                ErrorLogger.log(e);
            }
        }
    }   
}