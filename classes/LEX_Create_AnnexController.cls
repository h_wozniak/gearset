public with sharing class LEX_Create_AnnexController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String wasIsUnenrolled(String recordId) {
		Enrollment__c enr = [SELECT Id, Unenrolled__c, WasIs_Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];
		return (!enr.Unenrolled__c && enr.WasIs_Unenrolled__c) ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String allowedStatus(String recordId) {
		Set<String> allowedStatuses = new Set<String> {
			CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
			CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
		};

		Enrollment__c enr = [SELECT Id, Status__c, Unenrolled__c, Unenrolled_Status__c FROM Enrollment__c WHERE Id = :recordId];
		if (enr.Unenrolled__c) {
            if (enr.Status__c == 'Resignation') return 'FALSE';
			return (allowedStatuses.contains(enr.Unenrolled_Status__c) ? 'TRUE' : 'FALSE');
		}
		else {
			return (allowedStatuses.contains(enr.Status__c) ? 'TRUE' : 'FALSE');
		}
	}

	@AuraEnabled
	public static String createAnnex(String recordId) {
		return LEXStudyUtilities.createAnnex(recordId);
	}

	@AuraEnabled
	public static Void finalizeAnnex(String previousEnrId, String newEnrId) {
		System.debug('queries '+Limits.getQueries());
		LEXStudyUtilities.finalizeAnnex(previousEnrId, newEnrId);
	}
}