public with sharing class LEX_Regenerate_DocumentController {

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}


	@AuraEnabled
	public static Decimal getCustomSettings(String recordId) {
		return LEXStudyUtilities.getCustomSettings(recordId);
	}

	@AuraEnabled
	public static String regenerateDocument(String recordId, Decimal refNumb, Boolean password) {
		Boolean response = LEXStudyUtilities.generatePreviouslyFailedDocument(recordId, refNumb, password);
		return response ? 'TRUE' : 'FALSE'; 
	}
}