@isTest
global class ZTestIntegrationInteractionReceiver {

    @isTest static void test_receiveInteractionInformations() {
    	Contact newContact = ZDataTestUtility.createCandidateContact(new Contact(ID_Unique__c = '0038E00000HvFLW'), true);

    	newContact.ID_Unique__c = '0038E00000HvFLW';
    	update newContact;

    	Contact contactToCreateTask = [
            SELECT Id, ID_Unique__c
            FROM Contact
            WHERE ID_Unique__c = :'0038E00000HvFLW'
        ];

        System.assert(contactToCreateTask != null);

        String body = '[{"crm_contact_id":"0038E00000HvFLW","external_contact_id":"' + newContact.ID_Unique__c + '","interaction_subject":"Test call","interaction_date":"2016-10-10","interaction_type":"Call","interaction_classifier":"string","interaction_description":"Some call description","agent_id":"1234","call_details":{"call_type":"Incomming","call_answered":true,"phone_number":"123465789"}}]';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationInteractionReceiver.receiveInteractionInformations();
        }
        catch (Exception e) {
        	System.assertEquals(e.getMessage(), '');
        }
        Test.stopTest();
    }
}