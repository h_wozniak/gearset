/**
*   @author         Wojciech Słodziak
*   @description    Utility class, that is used to store methods related to PriceBooks
**/

global without sharing class PriceBookManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- HELPER VALUE SETS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static final List<Schema.SObjectField> gradedPriceFields = new List<Schema.SObjectField> {
        Offer__c.Graded_Price_1_Year__c,
        Offer__c.Graded_Price_2_Year__c,
        Offer__c.Graded_Price_3_Year__c,
        Offer__c.Graded_Price_4_Year__c,
        Offer__c.Graded_Price_5_Year__c
    };

    public static Map<Id, Integer> rtToNumberMap = new Map<Id, Integer> {
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK) => 1,
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG) => 2,
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA) => 3
    };

    public static final Set<Id> priceBookRecordTypeIds = new Set<Id> {
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
    };



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- HELPER METHODS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Id getPBRecordTypeIdByDegree(String degree) {
        Map<String, Id> degreeToRTMap = new Map<String, Id> {
            CommonUtility.OFFER_DEGREE_I => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            CommonUtility.OFFER_DEGREE_II => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            CommonUtility.OFFER_DEGREE_U => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            CommonUtility.OFFER_DEGREE_II_PG => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            CommonUtility.OFFER_DEGREE_PG => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG),
            CommonUtility.OFFER_DEGREE_MBA => CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
        };

        return degreeToRTMap.get(degree);
    }

    /* Wojciech Słodziak */
    /* helper method to put Installment Configs to map with Installment Variant as key */
    public static Map<Integer, Offer__c> getInstallmentConfigsMapByInstallmentVariant(List<Offer__c> installmentConfigList) {
        Map<Integer, Offer__c> variantToInstConfigMap = new Map<Integer, Offer__c>();

        for (Offer__c installmentConfig : installmentConfigList) {
            variantToInstConfigMap.put(Integer.valueOf(installmentConfig.Installment_Variant__c), installmentConfig);
        }

        return variantToInstConfigMap;
    }

    /* Wojciech Słodziak */
    public static List<SelectOption> getTutitionSystemList(Offer__c priceBook) {
        Map<String, String> pvMap = CommonUtility.getPicklistLabelMap(Schema.Enrollment__c.Tuition_System__c);

        List<SelectOption> options = new List<SelectOption>();

        if (priceBook.Installment_Variants__c != null) {
            options.add(new SelectOption(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED, pvMap.get(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED)));
        }
        if (priceBook.Graded_Tuition__c) {
            options.add(new SelectOption(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED, pvMap.get(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED)));
        }
        if (priceBook.Onetime_Price__c != null) {
            options.add(new SelectOption(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME, pvMap.get(CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME)));
        }

        return options;
    }

    /* Wojciech Słodziak */
    public static List<SelectOption> getTuitionModePaymentList(Offer__c priceBook) {
        List<String> installmentVariatnts = CommonUtility.getOptionsFromMultiSelectToList(priceBook.Installment_Variants__c);

        List<SelectOption> options = new List<SelectOption>();
        for (String variant : installmentVariatnts) {
            options.add(new SelectOption(variant, variant));
        }

        return options;
    }

    /* Wojciech Słodziak */
    public static List<Offer__c> getInstallmentConfigList(Id priceBookId) {
        return [
            SELECT Id, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c,
            Graded_Price_5_Year__c 
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c = :priceBookId 
            ORDER BY Installment_Variant__c ASC
        ];
    }


   
    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- CONFIG VALIDATION METHODS ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    /* method checks if Price Books are configured correctly and if yes activates them */
    webservice static Boolean activatePriceBooks(List<String> priceBookIdList) {
        List<Offer__c> priceBookList = [
            SELECT Id, Active__c, RecordTypeId, Number_of_Semesters_formula__c, Graded_tuition__c
            FROM Offer__c 
            WHERE Id IN :priceBookIdList
        ];

        List<Offer__c> priceBookInstallmentConfigsAll = [
            SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, 
            Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, 
            Graded_Price_5_Year__c 
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c IN :priceBookIdList
        ];

        List<Offer__c> instListToUpdate = new List<Offer__c>();

        for (Offer__c priceBook : priceBookList) {
            List<Offer__c> priceBookInstallmentConfigsCurrent = new List<Offer__c>();
            for (Offer__c priceBookInstallmentCfg : priceBookInstallmentConfigsAll) {
                if (priceBook.Id == priceBookInstallmentCfg.Price_Book_from_Installment_Config__c) {
                    priceBookInstallmentConfigsCurrent.add(priceBookInstallmentCfg);
                    instListToUpdate.add(priceBookInstallmentCfg);
                }
            }

            Boolean result;
            if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)) {
                result = PriceBookManager.checkIfCanActivateI_II(priceBook, priceBookInstallmentConfigsCurrent);
            } else 
            if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG)
                || priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)) {
                result = PriceBookManager.checkIfCanActivatePG_MBA(priceBook, priceBookInstallmentConfigsCurrent);
            }

            // if Price Book cannot be activated stop method and return false
            if (!result) {
                return false;
            } else {
                priceBook.Active__c = true;
            }
        }

        try {
            update priceBookList;
            update instListToUpdate;
        } catch(Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }

        return true;
    }


    /* Wojciech Słodziak */
    /* method usable for I & II Degree Study Price Books */
    private static Boolean checkIfCanActivateI_II(Offer__c priceBook, List<Offer__c> priceBookInstallmentConfigs) {
        Integer yearsCount = Integer.valueOf(Math.ceil(Double.valueOf(priceBook.Number_of_Semesters_formula__c) / 2));
        Boolean oddSemesters = Math.mod(Integer.valueOf(priceBook.Number_of_Semesters_formula__c), 2) == 1;

        for (Offer__c installmentConfig : priceBookInstallmentConfigs) {
            if (installmentConfig.Fixed_Price__c == null) {
                return false;
            }

            if (priceBook.Graded_tuition__c) {
                for (Integer year = 0; year < yearsCount; year++) {
                    if (
                        !(
                            year == yearsCount - 1 
                            && (
                                installmentConfig.Installment_Variant__c == 12 
                                || oddSemesters && installmentConfig.Installment_Variant__c == 1
                            )
                        )
                    ) { //applicable cells
                        if (installmentConfig.get(gradedPriceFields.get(year)) == null) {
                            return false;
                        }
                    } else {
                        // clear value of Price Book cells that are not applicable - in case of wrong PriceBook import 
                        installmentConfig.put(gradedPriceFields.get(year), null);
                    }
                }
            }
        }

        return true;
    }

    /* Wojciech Słodziak */
    /* method usable for PG & MBA Degree Study Price Books */
    private static Boolean checkIfCanActivatePG_MBA(Offer__c priceBook, List<Offer__c> priceBookInstallmentConfigs) {
        Integer yearsCount = Integer.valueOf(Math.ceil(Double.valueOf(priceBook.Number_of_Semesters_formula__c) / 2));
        Boolean oddSemesters = Math.mod(Integer.valueOf(priceBook.Number_of_Semesters_formula__c), 2) == 1;

        for (Offer__c installmentConfig : priceBookInstallmentConfigs) {
            if (installmentConfig.Fixed_Price__c == null) {
                return false;
            }
        }

        return true;
    }






    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- PRICE BOOK DESIGNATION ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    /* method usable for retrieving proper Price Book for Course / Specialty */
    public static Offer__c getProperPriceBookForOffer(Id offerId, Boolean forForeigner) {
        Offer__c resultPriceBook;

        resultPriceBook = PriceBookManager.getProperPriceBookForOfferHelper(offerId, forForeigner);


        //get Price Book from Course if there is no Price Books on Specialty
        if (resultPriceBook == null) {
            List<Offer__c> specWithCourse = [
                SELECT Id, Course_Offer_from_Specialty__c 
                FROM Offer__c 
                WHERE Id = :offerId AND Course_Offer_from_Specialty__c != null
            ];

            if (!specWithCourse.isEmpty()) {
                Id courseId = specWithCourse[0].Course_Offer_from_Specialty__c;
                
                resultPriceBook = PriceBookManager.getProperPriceBookForOfferHelper(courseId, forForeigner);
            }
        }

        return resultPriceBook;
    }

    /* Wojciech Słodziak */
    /* helper method usable for retrieving proper Price Book for Course / Specialty not considering other offers in Offer Tree */
    private static Offer__c getProperPriceBookForOfferHelper(Id offerId, Boolean forForeigner) {
        Offer__c resultPriceBook;

        List<Offer__c> offerPriceBookList = [
            SELECT Id, Name, Price_Book_Type__c, Installment_Variants__c, Graded_tuition__c, Entry_fee__c, Onetime_Price__c, Number_of_Semesters_formula__c, Enrollment_fee__c, 
                   Qualifying_Fee__c
            FROM Offer__c
            WHERE (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD 
            OR Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_FOREIGNERS) 
            AND Currently_in_use__c = true AND Offer_from_Price_Book__c = :offerId
        ];

        for (Offer__c pb : offerPriceBookList) {
            if (forForeigner && pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_FOREIGNERS) {
                resultPriceBook = pb;
            } else {
                if (resultPriceBook == null && pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD) {
                    resultPriceBook = pb;
                }
            }
        }

        return resultPriceBook;
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------- PRICE BOOK DESIGNATION (BY TRIGGERS) ------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    /* discover and automatically connect Price Book that is valid for Enrollment */
    public static void connectProperPriceBook(Set<Id> enrIds) {
        List<Enrollment__c> enrollmentsToUpdate = [
            SELECT Id, Price_Book_from_Enrollment__c, Candidate_Student__r.Foreigners_PriceBook__c, Course_or_Specialty_Offer__c
            FROM Enrollment__c 
            WHERE Id IN :enrIds
        ];

        Map<Id, List<Enrollment__c>> courseOrSpecToEnrMap = new Map<Id, List<Enrollment__c>>();

        Set<Id> offerIds = new Set<Id>();
        for (Enrollment__c enr : enrollmentsToUpdate) {
            enr.Price_Book_from_Enrollment__c = null;
            offerIds.add(enr.Course_or_Specialty_Offer__c);

            List<Enrollment__c> currentAddedEnrollmentList = courseOrSpecToEnrMap.get(enr.Course_or_Specialty_Offer__c);
            if (currentAddedEnrollmentList != null) {
                currentAddedEnrollmentList.add(enr);
            } else {
                courseOrSpecToEnrMap.put(enr.Course_or_Specialty_Offer__c, new List<Enrollment__c>{ enr });
            }
        }

        Map<Id, Id> offerToPBMap = new Map<Id, Id>();
        Map<Id, Id> offerToForeignerPBMap = new Map<Id, Id>();

        PriceBookManager.putPriceBooksFromOffers(offerIds, offerToPBMap, offerToForeignerPBMap);

        PriceBookManager.matchPriceBookToEnrollment(courseOrSpecToEnrMap, offerIds, offerToPBMap, offerToForeignerPBMap);

        PriceBookManager.putPriceBooksFromCoursesForSpecs(offerIds, offerToPBMap, offerToForeignerPBMap);

        PriceBookManager.matchPriceBookToEnrollment(courseOrSpecToEnrMap, offerIds, offerToPBMap, offerToForeignerPBMap);

        try {
            update enrollmentsToUpdate;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Wojciech Słodziak */
    // helper method for connectProperPriceBook
    private static void putPriceBooksFromOffers(Set<Id> offerIds, Map<Id, Id> offerToPBMap, Map<Id, Id> offerToForeignerPBMap) {
        List<Offer__c> offerList = [
            SELECT Id, 
                (SELECT Id, Price_Book_Type__c FROM PriceBooks__r 
                 WHERE (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD OR Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_FOREIGNERS) 
                 AND Currently_in_use__c = true) 
            FROM Offer__c WHERE Id IN :offerIds
        ];

        Set<Id> specialtiesToDiscoverPBIds = new Set<Id>();
        for (Offer__c offer : offerList) {
            for (Offer__c pb : offer.PriceBooks__r) {
                if (pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD) {
                    offerToPBMap.put(offer.Id, pb.Id);
                } else if (pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_FOREIGNERS) {
                    offerToForeignerPBMap.put(offer.Id, pb.Id);
                }
            }
        }
    }

    /* Wojciech Słodziak */
    // helper method for connectProperPriceBook
    private static void putPriceBooksFromCoursesForSpecs(Set<Id> offerIds, Map<Id, Id> offerToPBMap, Map<Id, Id> offerToForeignerPBMap) {
        List<Offer__c> specialtiesToDiscoverPBFromCourse = [
            SELECT Id, Course_Offer_from_Specialty__c 
            FROM Offer__c 
            WHERE Id IN :offerIds 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
        ];

        Set<Id> courseIds = new Set<Id>();
        for (Offer__c spec : specialtiesToDiscoverPBFromCourse) {
            courseIds.add(spec.Course_Offer_from_Specialty__c);
        }

        if (!courseIds.isEmpty()) {        
            //courses with Price Books for specialties
            List<Offer__c> coursesWithPriceBooks = [
                SELECT Id, 
                    (SELECT Id, Price_Book_Type__c FROM PriceBooks__r 
                     WHERE (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD OR Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_FOREIGNERS) 
                     AND Currently_in_use__c = true) 
                FROM Offer__c WHERE Id IN :courseIds
            ];

            for (Offer__c spec : specialtiesToDiscoverPBFromCourse) {
                for (Offer__c course : coursesWithPriceBooks) {
                    if (course.Id == spec.Course_Offer_from_Specialty__c) {
                        for (Offer__c pb : course.PriceBooks__r) {
                            if (pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD && !offerToPBMap.containsKey(spec.Id)) {
                                offerToPBMap.put(spec.Id, pb.Id);
                            } else if (pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_FOREIGNERS && !offerToForeignerPBMap.containsKey(spec.Id)) {
                                offerToForeignerPBMap.put(spec.Id, pb.Id);
                            }
                        }
                    }
                }
            }
        }
    }

    /* Wojciech Słodziak */
    // helper method for connectProperPriceBook
    private static void matchPriceBookToEnrollment(Map<Id, List<Enrollment__c>> courseOrSpecToEnrMap, Set<Id> offerIds, Map<Id, Id> offerToPBMap, 
                                                   Map<Id, Id> offerToForeignerPBMap) {
        for (Id offerId : offerIds) {
            List<Enrollment__c> enrList = courseOrSpecToEnrMap.get(offerId);
            for (Enrollment__c enr : enrList) {
                if (enr.Candidate_Student__r.Foreigners_PriceBook__c && enr.Price_Book_from_Enrollment__c == null) {
                    enr.Price_Book_from_Enrollment__c = offerToForeignerPBMap.get(offerId);
                }

                if (enr.Price_Book_from_Enrollment__c == null) {
                    enr.Price_Book_from_Enrollment__c = offerToPBMap.get(offerId);
                }
            }
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- VARIOUS METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // evaluate which PriceBook is current for this Offer (only for Standard and Foreigner without Residence Card types)
    public static void evaluateWhichOfferPriceBookIsCurrent(Set<Id> offerToReevaluateCurrentPriceBook) {
        Set<String> priceBookTypes = new Set<String> { CommonUtility.OFFER_PBTYPE_STANDARD, CommonUtility.OFFER_PBTYPE_FOREIGNERS };

        List<Offer__c> priceBookList = [
            SELECT Id, Price_Book_Type__c, PriceBook_From__c, PriceBook_KickIn__c, Offer_from_Price_Book__c, Currently_in_use__c, Active__c 
            FROM Offer__c 
            WHERE Offer_from_Price_Book__c IN :offerToReevaluateCurrentPriceBook 
            AND Price_Book_Type__c IN : priceBookTypes 
            ORDER BY PriceBook_From__c ASC
        ];

        Map<Id, Offer__c> priceBooksToUpdate = new Map<Id, Offer__c>();

        for (String pbType : priceBookTypes) { // one of each may be in use simultaneously
            for (Id offerId : offerToReevaluateCurrentPriceBook) {
                Offer__c priceBookLatest;

                for (Offer__c priceBook : priceBookList) {
                    if (offerId == priceBook.Offer_from_Price_Book__c && priceBook.Price_Book_Type__c == pbType) {
                        if (priceBook.Currently_in_use__c) {
                            priceBook.Currently_in_use__c = false;
                            priceBooksToUpdate.put(priceBook.Id, priceBook);
                        }

                        if (priceBook.Active__c 
                            && (
                                priceBook.PriceBook_From__c <= System.now() 
                                || ( // Salesforce workflow may run a bit before PriceBook_From__c date
                                    priceBook.PriceBook_KickIn__c 
                                    && priceBook.PriceBook_From__c <= System.now().addMinutes(15)
                                )
                            )
                        ) {
                            priceBookLatest = priceBook;
                        }

                        if (priceBook.PriceBook_KickIn__c == true) {
                            priceBook.PriceBook_KickIn__c = false;
                            priceBooksToUpdate.put(priceBook.Id, priceBook);
                        }
                    }
                }

                if (priceBookLatest != null) {
                    priceBookLatest.Currently_in_use__c = true;
                    priceBooksToUpdate.put(priceBookLatest.Id, priceBookLatest);
                }
            }
        }

        try {
            update priceBooksToUpdate.values();
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // set Current PriceBook From date on Offer (only Standard types)
    public static void setCurrentPriceBookFromDateOnOffer(Set<Id> offerToSetPriceBookFromDate) {
        List<Offer__c> priceBookList = [
            SELECT Id, PriceBook_From__c, Currently_in_use__c, Offer_from_Price_Book__c 
            FROM Offer__c 
            WHERE Offer_from_Price_Book__c IN :offerToSetPriceBookFromDate 
            AND Currently_in_use__c = true AND Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD
        ];

        List<Offer__c> offersToUpdate = new List<Offer__c>();
        for (Id offerId : offerToSetPriceBookFromDate) {
            Offer__c offer = new Offer__c(
                Id = offerId,
                Current_Price_Book_in_use_from__c = null
            );

            for (Offer__c priceBook : priceBookList) {
                if (offerId == priceBook.Offer_from_Price_Book__c) {
                    offer.Current_Price_Book_in_use_from__c = priceBook.PriceBook_From__c;
                }
            }
            offersToUpdate.add(offer);
        }

        try {
            update offersToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // Check if Special type Price Book is in Use (if Active and Date from)
    public static void checkIfSpecialTypePriceBookIsInUse(List<Offer__c> specialPriceBookToCheckIfInUse) {
        for (Offer__c priceBook : specialPriceBookToCheckIfInUse) {
            if (priceBook.Active__c && (priceBook.PriceBook_From__c <= System.now() || priceBook.PriceBook_KickIn__c 
                && priceBook.PriceBook_From__c <= System.now().addMinutes(15))
            ) {
                priceBook.Currently_in_use__c = true;
            } else {
                priceBook.Currently_in_use__c = false;
            }
            priceBook.PriceBook_KickIn__c = false;
        }
    }

    /* Wojciech Słodziak */
    // create Installment Configs for Price Book after insert, stop this logic if those installments are created by Import
    public static void createInstallmentConfigForPriceBook(List<Offer__c> priceBooksToCreateInstallmentConfigs) {
        List<Offer__c> installmentConfigList = new List<Offer__c>();
        for (Offer__c priceBook : priceBooksToCreateInstallmentConfigs) {
            Set<String> multiselectValues = CommonUtility.getOptionsFromMultiSelect(priceBook.Installment_Variants__c);
            for (String variant : multiselectValues) {
                Offer__c installmentConfig = new Offer__c();
                installmentConfig.RecordTypeId = CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_PB_INSTALLMENT_CFG);
                installmentConfig.Installment_Variant__c = Integer.valueOf(variant);
                installmentConfig.Price_Book_from_Installment_Config__c = priceBook.Id;
                installmentConfigList.add(installmentConfig);
            }
        }

        try {
            insert installmentConfigList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // Delete Price Book Installment Configs on delete of Price Book
    public static void deletePriceBookInstallmentConfigsOnPriceBookDeletion(Set<Id> priceBookIdsToDeleteInstallmentConfigs) {
        List<Offer__c> installmentConfigList = [
            SELECT Id 
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c IN :priceBookIdsToDeleteInstallmentConfigs
        ];

        try {
            delete installmentConfigList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // update Installment Configs for Price Book after Installment Variants update
    public static void updateInstallmentConfigForPriceBookAfterInstallmentVariantUpdate(List<Offer__c> priceBooksToUpdateInstallmentConfigs) {
        Set<Id> pbIdList = new Set<Id>();
        for (Offer__c pb : priceBooksToUpdateInstallmentConfigs) {
            pbIdList.add(pb.Id);
        }

        List<Offer__c> installmentConfigsToDelete = new List<Offer__c>();
        List<Offer__c> installmentConfigsToInsert = new List<Offer__c>();

        List<Offer__c> installmentConfigList = [
            SELECT Id, Installment_Variant__c, Price_Book_from_Installment_Config__c 
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c IN :pbIdList
        ];

        for (Offer__c pb : priceBooksToUpdateInstallmentConfigs) {
            Set<String> multiselectValues = CommonUtility.getOptionsFromMultiSelect(pb.Installment_Variants__c);
            for (Offer__c installmentConfig : installmentConfigList) {
                if (installmentConfig.Price_Book_from_Installment_Config__c == pb.Id) {
                    if (!multiselectValues.contains(String.valueOf(installmentConfig.Installment_Variant__c))) {
                        installmentConfigsToDelete.add(installmentConfig);
                    }
                }
            }

            for (String variant : multiSelectValues) {
                Boolean variantExists = false;
                for (Offer__c installmentConfig : installmentConfigList) {
                    if (installmentConfig.Price_Book_from_Installment_Config__c == pb.Id && installmentConfig.Installment_Variant__c == Decimal.valueOf(variant)) {
                        variantExists = true;
                    }
                }

                if (!variantExists) {
                    Offer__c installmentConfig = new Offer__c();
                    installmentConfig.RecordTypeId = CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_PB_INSTALLMENT_CFG);
                    installmentConfig.Installment_Variant__c = Decimal.valueOf(variant);
                    installmentConfig.Price_Book_from_Installment_Config__c = pb.Id;
                    installmentConfigsToInsert.add(installmentConfig);
                }
            }
        }

        try {
            delete installmentConfigsToDelete;
            insert installmentConfigsToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // Method for clearing installment config values on Graded_Tuition__c cbx change
    public static void clearPriceBookTuitionInstallments(Set<Id> pbIds) {
        List<Offer__c> pbList = [
            SELECT Id,
                (SELECT Id, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id IN :pbIds
        ];

        List<Offer__c> instCfgToUpdate = new List<Offer__c>();

        for (Offer__c pb : pbList) {
            for (Offer__c pbInstCfg : pb.InstallmentConfigs__r) {
                pbInstCfg.Graded_Price_1_Year__c = null;
                pbInstCfg.Graded_Price_2_Year__c = null;
                pbInstCfg.Graded_Price_3_Year__c = null;
                pbInstCfg.Graded_Price_4_Year__c = null;
                pbInstCfg.Graded_Price_5_Year__c = null;

                instCfgToUpdate.add(pbInstCfg);
            }
        }

        try {
            update instCfgToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- VALIDATIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    /* Wojciech Słodziak */
    // Check if there isn't PriceBook with the same PriceBook_from__c date and the same type   
    public static void checkIfPriceBookExistsWithSameDateAndType(List<Offer__c> priceBooksToCheckDuplication) {
        Set<Id> priceBookToCheckIdList = new Set<Id>();
        Set<Id> offerIdList = new Set<Id>();
        for (Offer__c priceBook : priceBooksToCheckDuplication) {
            priceBookToCheckIdList.add(priceBook.Id);
            offerIdList.add(priceBook.Offer_from_Price_Book__c);
        }
        List<Offer__c> priceBookList = [
            SELECT Id, Name, PriceBook_From__c, Price_Book_Type__c, Offer_from_Price_Book__c 
            FROM Offer__c 
            WHERE Offer_from_Price_Book__c IN :offerIdList AND (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD 
            OR Price_Book_Type__c = : CommonUtility.OFFER_PBTYPE_FOREIGNERS) AND Id NOT IN :priceBookToCheckIdList
        ];

        priceBookList.addAll(priceBooksToCheckDuplication);

        for (Offer__c pbToCheck : priceBooksToCheckDuplication) {
            for (Offer__c otherPB : priceBookList) {
                if (pbToCheck !== otherPB 
                    && (pbToCheck.Id == null || pbToCheck.Id != otherPB.Id) 
                    && pbToCheck.Offer_from_Price_Book__c == otherPB.Offer_from_Price_Book__c
                    && pbToCheck.Price_Book_Type__c == otherPB.Price_Book_Type__c 
                    && pbToCheck.PriceBook_From__c == otherPB.PriceBook_From__c) {
                    if(!CommonUtility.isLightningEnabled()) {
                        pbToCheck.addError(' ' + Label.msg_error_PriceBookAlreadyExists 
                            + ' (' 
                            + (otherPB.Id != null? '<a target="_blank" href="/' + otherPB.Id + '">' + otherPB.Name + '</a>' + ' - ' : '') 
                            + pbToCheck.PriceBook_From__c + ')', false);
                    } else {
                        pbToCheck.addError(Label.msg_error_PriceBookAlreadyExists  + ' ('+ otherPB.Name + ' - ' + pbToCheck.PriceBook_From__c + ')', false);
                    }
                }
            }
        }
    }

    /* Wojciech Słodziak */
    // Disable editing of Price Book fields if it is used for any Enrollment
    public static void disableEditingPriceBookFieldsIfUsed(List<Offer__c> priceBooksToCheckIfCanEdit) {
        Set<Id> priceBookIds = new Set<Id>();
        for (Offer__c priceBook : priceBooksToCheckIfCanEdit) {
            priceBookIds.add(priceBook.Id);
        }

        List<Enrollment__c> enrollmentList = [
            SELECT Id, Price_Book_from_Enrollment__c 
            FROM Enrollment__c 
            WHERE Price_Book_from_Enrollment__c IN :priceBookIds
        ];

        for (Offer__c priceBook : priceBooksToCheckIfCanEdit) {
            Boolean hasEnrollments = false;
            for (Enrollment__c enr : enrollmentList) {
                if (priceBook.Id == enr.Price_Book_from_Enrollment__c) {
                    hasEnrollments = true;
                }
            }

            if (hasEnrollments) {
                priceBook.addError(' ' + Label.msg_error_PriceBookHasEnrollments);
            }
        }
    }

}