/**
* @author       Wojciech Słodziak
* @description  Controller for ContactStageBar used on Contact page layouts
**/

public with sharing class ContactStageBarController {

    Contact contact { get; set; }

    public List<StatusWrapper> currentStatusesList { get; set; }
    public Double tdWidth { get; set; }

    public ContactStageBarController(ApexPages.StandardController controller) {
        contact = [SELECT Statuses__c FROM Contact WHERE Id = :controller.getRecord().Id];

        currentStatusesList = getStatusesList(contact, CommonUtility.getPicklistLabelMap(Schema.Contact.Statuses__c));

        tdWidth = 100.0/currentStatusesList.size();
    }

    private List<StatusWrapper> getStatusesList(Contact contact, Map<String, String> fieldLabelMap) {
        List<StatusWrapper>  statusesList = new List<StatusWrapper> ();

        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_INTERESTED, fieldLabelMap, 'icons/interested.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_CANDIDATE, fieldLabelMap, 'icons/candidate.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_ACCEPTED, fieldLabelMap, 'icons/accepted.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_STUDENT, fieldLabelMap, 'icons/student.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_GRADUATE, fieldLabelMap, 'icons/graduateC.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_PARTICIPANT, fieldLabelMap, 'icons/ebook.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_FOREIGNER, fieldLabelMap, 'icons/foreignerchiny.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_DUPLICATE, fieldLabelMap, 'icons/duplicate2.png', true));

        for (StatusWrapper status : statusesList) {
            if (contact.Statuses__c != null && contact.Statuses__c.contains(status.value)) {
                status.isActive = true;
            }
        }

        return statusesList;
    }


    private class StatusWrapper {
        public String label { get; set; }
        public String value { get; set; }
        public String iconUrl { get; set; }
        public Boolean warning { get; set; }
        public Boolean isActive { get; set; }

        public StatusWrapper(String value, Map<String, String> fieldLabelMap, String iconUrl) {
            this.value = value;
            label = fieldLabelMap.get(value);
            this.iconUrl = iconUrl;
            isActive = false;
        }

        public StatusWrapper(String value, Map<String, String> fieldLabelMap, String iconUrl, Boolean warning) {
            this(value, fieldLabelMap, iconUrl);
            this.warning = warning;
        }
    }

}