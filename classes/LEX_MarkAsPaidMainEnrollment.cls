/**
 * Created by Sebastian on 23.01.2018.
 */

public with sharing class LEX_MarkAsPaidMainEnrollment {

    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }

    @AuraEnabled
    public static Boolean updateRecord(Id studyEnrId) {
        try {
            Enrollment__c mainTrainingEnrollment = new Enrollment__c(
                    Id = studyEnrId,
                    Status__c = CommonUtility.ENROLLMENT_STATUS_PAID
            );

            update mainTrainingEnrollment;
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

}