@isTest
private class ZTestScheduleSelectionController {
    //----------------------------------------------------------------------------------
    //ScheduleSelectionController
    //----------------------------------------------------------------------------------

    private static Map<Integer, sObject> prepareData1() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createCandidateContact(null, true));
        retMap.put(3, ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true));

        
        return retMap;
    }
    
    
    @isTest static void testScheduleSelectionController1() {
        Map<Integer, sObject> dataMap = prepareData1();
        

        PageReference pageRef = Page.ScheduleSelection;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('enrollmentRT', CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING));

        Test.startTest();
        ScheduleSelectionController cntrl = new ScheduleSelectionController();

        System.assertNotEquals('', cntrl.allUniversitiesJSON);

        List<Offer__c> schedules = ScheduleSelectionController.getSchedulesForTraining(dataMap.get(0).Id);
        Test.stopTest();

        System.assertNotEquals(0, schedules.size());
        cntrl.back();
    }
    
    @isTest static void testScheduleSelectionController2() {
        Map<Integer, sObject> dataMap = prepareData1();

        PageReference pageRef = Page.ScheduleSelection;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('enrollmentRT', CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING));
        ApexPages.CurrentPage().getParameters().put('retURL', 'testRetUrl');

        Test.startTest();
        ScheduleSelectionController cntrl = new ScheduleSelectionController();
        Test.stopTest();

        System.assertNotEquals('', cntrl.allUniversitiesJSON);

        cntrl.back();
    }

}