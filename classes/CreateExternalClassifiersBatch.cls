/**
*   @author         Sebastian Łasisz
*   @description    batch class that creates external classifiers for imported external campaigns
**/

global class CreateExternalClassifiersBatch implements Database.Batchable<sObject> {
	Set<Id> marketingCampaignsToAttachSystemClassifiers;

	global CreateExternalClassifiersBatch(Set<Id> campaignIds) {
		marketingCampaignsToAttachSystemClassifiers = campaignIds;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        	SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL)
            AND Id IN :marketingCampaignsToAttachSystemClassifiers
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Marketing_Campaign__c> marketingCampaigns) { 
        ClassifierManager.createSystemExternalClassifiers(marketingCampaigns);	
	}

	global void finish(Database.BatchableContext BC) { }
}