/**
*   @author         Wojciech Słodziak
*   @description    Rest Resource class for receiving acknowledgements for Payment Templates transfers to Experia.
**/

@RestResource(urlMapping = '/payment_template_ack/*')
global without sharing class IntegrationPaymentTemplateAckReceiver {


    @HttpPost
    global static void receivePaymentTemplateAcks() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        IntegrationManager.AckResponse_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            res.statusCode = 200;
            IntegrationPaymentTemplateHelper.updateSyncStatus_onFinish(parsedJson.success, new Set<Id> { parsedJson.record_id });

            if (!parsedJson.success) {
                ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationPaymentTemplateAckReceiver.class.getName() + '\n' + jsonBody);
            }
        }
    }
    
    private static IntegrationManager.AckResponse_JSON parse(String jsonBody) {
        return (IntegrationManager.AckResponse_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.AckResponse_JSON.class);
    }

}