public with sharing class LEX_AddTrainingScheduleController {
	
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String setCodeHelperNameAndNameForSchedules(Id trainingOffersId) {
		Offer__c training = [
				SELECT Id, Name,
				(SELECT Id, Name, Code_Helper_Num__c
				FROM Schedules__r
				WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
				OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
				ORDER BY Code_Helper_Num__c DESC NULLS LAST
				)
				FROM Offer__c
				WHERE Id = :trainingOffersId
		];

        Decimal maxCode = 0;

        for (Offer__c schedule : training.Schedules__r) {
            if (schedule.Code_Helper_Num__c != null && schedule.Code_Helper_Num__c > maxCode) {
                maxCode = schedule.Code_Helper_Num__c;
            }
        }

        return 'T' + training.Name + '-' + String.valueOf(++maxCode).leftPad(3).replaceAll(' ', '0');
	}

	@AuraEnabled
	public static String getTargetRecordType(String recordId) {
		Offer__c offer = [SELECT Type__c FROM Offer__c WHERE Id = :recordId];
		if(offer.Type__c == CommonUtility.OFFER_TYPE_OPEN) {
			return CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE);
		} else {
			return CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE);
		}
	}
}