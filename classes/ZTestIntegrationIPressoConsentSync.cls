@isTest
global class ZTestIntegrationIPressoConsentSync {

	@isTest static void test_prepareConsentData() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.IPCS_prepareData();

        Test.startTest();
        Catalog__c consent = (Catalog__c)dataMap.get(0);
        String result = IntegrationIPressoConsentSync.prepareConsentData(consent.Id, consent.Name);
        Test.stopTest();

        System.assertNotEquals('', result);
	}
    
    @isTest static void test_syncConsentsWithIPresso_resourceUnavailable() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.IPCS_prepareData();
        
        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.ConsentsSendFailedCalloutMock());
        
        Test.startTest();
        Catalog__c consent = (Catalog__c)dataMap.get(0);
        Boolean result = IntegrationIPressoConsentSync.syncConsentsWithIPresso(consent.Id, consent.Name);
        Test.stopTest();
        
        System.assert(!result);
    }
    
    @isTest static void test_syncConsentsWithIPresso() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.IPCS_prepareData();
        
        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.ConsentsSendCalloutMock());
        
        Test.startTest();
        Catalog__c consent = (Catalog__c)dataMap.get(0);
        Boolean result = IntegrationIPressoConsentSync.syncConsentsWithIPresso(consent.Id, consent.Name);
        Test.stopTest();
        
        System.assert(result);
    }
    
    @isTest static void test_syncConsentsWithIPressoAsync() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.IPCS_prepareData();
        
        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.ConsentsSendCalloutMock());
        
        Test.startTest();
        Boolean errorOccured = false;
        try {
        Catalog__c consent = (Catalog__c)dataMap.get(0);
        	IntegrationIPressoConsentSync.syncConsentsWithIPressoAsync(consent.Id, consent.Name);
        }
        catch (Exception e) {
        	errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
}