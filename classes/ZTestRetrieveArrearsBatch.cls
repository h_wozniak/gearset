@isTest
global class ZTestRetrieveArrearsBatch {

    @isTest static void test_RetrieveArrearsBatch() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        qEduAgr.University_Name__c = CommonUtility.MARKETING_ENTITY_GDANSK;
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(0, qEduAgr.Portal_Login__c));

        Test.startTest();
        Database.executeBatch(new RetrieveArrearsBatch(), 200);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, 0);
    }

    @isTest static void test_RetrieveArrearsBatch_2() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        qEduAgr.University_Name__c = CommonUtility.MARKETING_ENTITY_GDANSK;
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(200, qEduAgr.Portal_Login__c));

        Test.startTest();
        Database.executeBatch(new RetrieveArrearsBatch(), 200);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, - 200);
    }

    @isTest static void test_RetrieveArrearsBatch_3() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        qEduAgr.University_Name__c = CommonUtility.MARKETING_ENTITY_GDANSK;
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(200, '12345'));

        Test.startTest();
        Database.executeBatch(new RetrieveArrearsBatch(), 200);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, null);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class ArrearsRetrieveCalloutMock implements HttpCalloutMock {
        Integer balance;
        String portalLogin;

        ArrearsRetrieveCalloutMock(Integer balance, String portalLogin) {
            this.balance = balance;
            this.portalLogin = portalLogin;
        }

        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);

            List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

            IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
            response.portal_login = portalLogin;
            response.to_pay_total = balance;
            response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
            responseJSON.add(response);

            String preparedJson = JSON.serializePretty(responseJSON);
            res.setBody(preparedJson);

            return res;
        }
    }
}