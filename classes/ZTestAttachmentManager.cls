@isTest
private class ZTestAttachmentManager {
    
    @isTest static void testMultipleAttachmentsInsertedAtOnce() {        
        CustomSettingDefault.initDocumentReferenceNumber();
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(null, true);
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        List<Attachment> attachmentList = new List<Attachment>();
        
        Integer attachmentListSize = 5;
        
        for (Integer i = 0; i < attachmentListSize; i++ ) {
            Attachment newAttachment = new Attachment();    
            newAttachment.Name = 'Unit Test Attachment';
            newAttachment.Body = bodyBlob;
            newAttachment.ParentId = documentEnrollment.id;
            attachmentList.add(newAttachment);
        }
        
        Test.startTest();
        insert attachmentList;
        Test.stopTest();
        
        List<Attachment> updatedAttachmentList = [SELECT Id FROM Attachment];
        
        System.assertEquals(updatedAttachmentList.size(), attachmentListSize);

        List<Enrollment__c> newDocumentEnrollment = [
            SELECT Id, Current_File__c, Current_File_Name__c 
            FROM Enrollment__c 
            WHERE Id = :documentEnrollment.Id
        ];

        System.assertEquals(newDocumentEnrollment[0].Current_File_Name__c, attachmentList[attachmentListSize-1].Name);
    }
    
    @isTest static void testSetAttachmentName() {   
        CustomSettingDefault.initDocumentReferenceNumber();
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(null, true);
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        List<Attachment> attachmentList = new List<Attachment>();
        
        Integer attachmentListSize = 5;
        
        for (Integer i = 0; i < attachmentListSize; i++ ) {
            Attachment newAttachment = new Attachment();    
            newAttachment.Name = 'Unit Test Attachment';
            newAttachment.Body = bodyBlob;
            newAttachment.ParentId = documentEnrollment.id;
            attachmentList.add(newAttachment);
        }
        
        Test.startTest();
        insert attachmentList;
        Test.stopTest();

        List<Enrollment__c> newDocumentEnrollment = [
            SELECT Id, Current_File__c, Current_File_Name__c 
            FROM Enrollment__c 
            WHERE Id = :documentEnrollment.Id
        ];
        
        System.assertNotEquals(documentEnrollment.Current_File__c, newDocumentEnrollment[0].Current_File__c);
        
        System.assertNotEquals(documentEnrollment.Current_File_Name__c, newDocumentEnrollment[0].Current_File_Name__c);
        
        System.assertEquals(newDocumentEnrollment[0].Current_File_Name__c, attachmentList[attachmentListSize-1].Name);
    }
    
    @isTest static void testRemovingAttachmentsFromEnrollment() {   
        CustomSettingDefault.initDocumentReferenceNumber();
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(null, true);
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        List<Attachment> attachmentList = new List<Attachment>();
        
        Integer attachmentListSize = 5;
        
        for (Integer i = 0; i < attachmentListSize; i++) {
            Attachment newAttachment = new Attachment();    
            newAttachment.Name = 'Unit Test Attachment';
            newAttachment.Body = bodyBlob;
            newAttachment.ParentId = documentEnrollment.id;
            attachmentList.add(newAttachment);
        }
        
        Test.startTest();
        insert attachmentList;
        Test.stopTest();
        
        List<Enrollment__c> newDocumentEnrollment = [
            SELECT Id, Current_File__c, Current_File_Name__c 
            FROM Enrollment__c 
            WHERE Id = :documentEnrollment.Id
        ];
        
        List<Attachment> attachmentsOnDocument = [
            SELECT Id, Name
            FROM Attachment
            WHERE parentId = :documentEnrollment.Id
            ORDER BY CreatedDate DESC
        ];
        
        String currentFileName = newDocumentEnrollment[0].Current_File_Name__c;
        
        try {
            delete attachmentsOnDocument[0];
        }
        catch (Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage() != ''? true : false;
            System.assert(expectedExceptionThrown);
        }
        
        newDocumentEnrollment = [
            SELECT Id, Current_File__c, Current_File_Name__c 
            FROM Enrollment__c 
            WHERE Id = :documentEnrollment.Id
        ];
        
        String newFileName = newDocumentEnrollment[0].Current_File_Name__c;

        System.assertEquals(newDocumentEnrollment[0].Current_File_Name__c, attachmentList[attachmentListSize-1].Name);
    }


    @isTest static void testCheckSendReqDocList() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT
        ), true);

        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_OATH
        ), true);

        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
            Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH
        ), true);

        Enrollment__c studyDocEnrAgr = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentAgreementId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);
        
        Enrollment__c studyDocEnrAgrOath = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentOathId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);
        
        Enrollment__c studyQuestionnare = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentPersonalQuestionnareId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);



        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            EmailTemplate defaultEmailTemplate = new EmailTemplate();
            defaultEmailTemplate.isActive = true;
            defaultEmailTemplate.Name = 'x';
            defaultEmailTemplate.DeveloperName = EmailManager_DocumentListEmail.DocListEmTemplates.X23ac1_StudyEnrDocList.name();
            defaultEmailTemplate.TemplateType = 'text';
            defaultEmailTemplate.FolderId = UserInfo.getUserId();
            defaultEmailTemplate.Subject = 'x';

            insert defaultEmailTemplate;
        }

        //test with one attachment (generated document)
        Attachment att1 = new Attachment(
            Name = 'test-att1',
            Body = Blob.valueOf('body'),
            ParentId = studyDocEnrAgr.Id
        );
        insert att1;

        Enrollment__c studyEnrAfter1 = [SELECT RequiredDocListEmailedCount__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(0, studyEnrAfter1.RequiredDocListEmailedCount__c);

        //test with two attachments (generated documents)
        Attachment att2 = new Attachment(
            Name = 'test-att2',
            Body = Blob.valueOf('body'),
            ParentId = studyDocEnrAgrOath.Id
        );
        insert att2;

        //test with three attachments (generated documents)
        Attachment att3 = new Attachment(
            Name = 'test-att3',
            Body = Blob.valueOf('body'),
            ParentId = studyQuestionnare.Id
        );
        insert att3;

        Test.stopTest();

        Enrollment__c studyEnrAfter2 = [SELECT RequiredDocListEmailedCount__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(1, studyEnrAfter2.RequiredDocListEmailedCount__c);
    }
}