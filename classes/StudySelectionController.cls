/**
* @author       Wojciech Słodziak
* @description  This class is a controller for StudySelection.page, which allows study (course or specialty) finding and selection
**/

global with sharing class StudySelectionController {


    public String allUniversitiesJSON { get; set; }
    public String degreeListJSON { get; set; }

    public Id candidateId { get; set; }
    public Id enrollmentId { get; set; }

    public String currencySymbol { get; set; }

    public String retURL { get; set; }
    public String retURLtoBack { get; set; }

    public Boolean isLightning {get; set; }
    public StudySelectionController() {
        isLightning = (UserInfo.getUiTheme().contains('Theme4'));
        candidateId = Apexpages.currentPage().getParameters().get('candidateId');
        enrollmentId = Apexpages.currentPage().getParameters().get('enrollmentId'); //only for url passing
        retURLtoBack = Apexpages.currentPage().getParameters().get('retURL');

        List<Account> universityList = getUniversities();
        allUniversitiesJSON = JSON.serialize(universityList);

        List<PicklistValue_Wrapper> options = getDegrees();
        degreeListJSON = JSON.serialize(options);

        currencySymbol = CommonUtility.getCurrencySymbolFromIso(CommonUtility.CURRENCY_CODE_PLN).unescapeHtml4();
        retURL = 'apex/StudySelection' + (candidateId != null? '?candidateId=' + candidateId : '');
    }

    private List<Account> getUniversities() {
        return [
            SELECT Id, Name 
            FROM Account 
            WHERE RecordTypeId = : CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
        ];
    }


    private static List<PicklistValue_Wrapper> getDegrees() {
        List<PicklistValue_Wrapper> options = new List<PicklistValue_Wrapper>();
        
        Schema.DescribeFieldResult fieldResult = Offer__c.Degree__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for(Schema.PicklistEntry f : ple)
        {
            PicklistValue_Wrapper pvw = new PicklistValue_Wrapper();
            pvw.value = f.getValue();
            pvw.label = f.getLabel();
            options.add(pvw);
        }
        
        return options;
    }

    @RemoteAction @ReadOnly
    global static List<Offer__c> getCoursesForUniversity(Id universityId, String degree) {
        String soql = 'SELECT Id, Name, Trade_Name__c, University_Study_Offer_from_Course__r.University__c, University_Study_Offer_from_Course__r.University__r.Name, ' 
            + 'Number_of_Semesters__c, Active_from__c, Active_to__c, Enrollment_only_for_Specialties__c, Number_of_Specialties__c, toLabel(Mode__c), toLabel(Kind__c), '
            + '(SELECT Id FROM Specializations__r), (SELECT Id FROM Specialties__r), Degree__c, '
            + 'toLabel(RecordType.Name) FROM Offer__c WHERE RecordTypeId = \'' + CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
            + '\' AND Configuration_Status_Course_Offer__c = true AND Active_from__c <= TODAY';

        if (universityId != null) {
            soql += ' AND University_Study_Offer_from_Course__r.University__c = \'' + universityId + '\'';
        }
        if (degree != null) {
            soql += ' AND University_Study_Offer_from_Course__r.Degree__c = \'' + degree + '\'';
        }

        soql += ' ORDER BY Name';

        try {
            List<Offer__c> courseOffersForUniversity =  Database.query(soql);

            Set<Id> courseOffersIds = new Set<Id>();
            for (Offer__c courseOfferForUniversity : courseOffersForUniversity) {
                courseOffersIds.add(courseOfferForUniversity.Id);
            }

            List<Offer__c> specialtiesWithSpecializations = [
                SELECT Id, Course_Offer_from_Specialty__c,
                (SELECT Id FROM Specializations__r)
                FROM Offer__c
                WHERE Course_Offer_from_Specialty__c IN :courseOffersIds
            ];

            List<Offer__c> courses = new List<Offer__c>();
            for (Offer__c courseOfferForUniversity : courseOffersForUniversity) {
                if (courseOfferForUniversity.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
                    if (!courseOfferForUniversity.Enrollment_only_for_Specialties__c) {
                        if (courseOfferForUniversity.Specializations__r.size() > 0) {
                            courses.add(courseOfferForUniversity);
                        }
                        else {
                            // check for specializations in specialties
                            for (Offer__c specialty : specialtiesWithSpecializations) {
                                if (specialty.Course_Offer_from_Specialty__c == courseOfferForUniversity.Id) {
                                    if (specialty.Specializations__r.size() > 0) {
                                        courses.add(courseOfferForUniversity);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        courses.add(courseOfferForUniversity);
                    }
                }
                else {
                    courses.add(courseOfferForUniversity);
                }
            }

            return courses;

        } catch (Exception ex) {
            return null;
        }
    }

    @RemoteAction @ReadOnly
    global static List<Offer__c> getSpecialtiesForCourse(Id courseId) {
        List<Offer__c> specialites = [
            SELECT Name, Specialty_Trade_Name__c, Active_from__c, Active_to__c, toLabel(RecordType.Name), Degree__c, 
                   Enrollment_only_for_Specialties__c,
                   (SELECT Id FROM Specializations__r)
            FROM Offer__c 
            WHERE RecordtypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER) 
            AND Course_Offer_from_Specialty__c = :courseId AND Configuration_Status_Specialty__c = true AND Active_from__c <= TODAY
            ORDER BY Name
        ];

        List<Offer__c> properlyConfiguredSpecialties = new List<Offer__c>();
        for (Offer__c specialty : specialites) {
            if (specialty.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) { 
                if (specialty.Specializations__r.size() > 0) {
                    properlyConfiguredSpecialties.add(specialty);
                }
            }
            else {
                properlyConfiguredSpecialties.add(specialty);
            }
        }

        return properlyConfiguredSpecialties;
    }


    public PageReference back() {
        if (retURLtoBack != null) {
            return new PageReference(retURLtoBack);
        }
        return null;
    }

    global class PicklistValue_Wrapper {
        String value;
        String label;
    }
}