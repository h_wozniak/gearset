/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send Candidate's Consents to Experia when requested.
**/

@RestResource(urlMapping = '/consents/*')
global without sharing class IntegrationContactConsentsResource {
    
    @HttpGet
    global static void sendConsents() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String contactId = req.params.get('person_id');

        try {
        	Contact candidate = [
        		SELECT Id, Consent_Direct_Communications__c, Consent_Electronic_Communication__c, Consent_Graduates__c, Consent_Marketing__c, Change_History__c,
        		Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, CreatedDate
        		FROM Contact
        		WHERE Id = :contactId
        	];

            res.responseBody = Blob.valueOf(IntegrationContactConsentsResource.prepareCandidateConsents(candidate));
            res.statusCode = 200;
        }
        catch (Exception ex) {
            //ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(604));
            res.statusCode = 400;    
        }
    }

    global static String prepareCandidateConsents(Contact candidate) {
    	List<String> changeHistory = candidate.Change_History__c != null ? candidate.Change_History__c.split('\r\n') : new List<String>();

        List<Consent_JSON> consents = new List<Consent_JSON>();
        consents.addAll(prepareConsents(
        	candidate.Consent_Direct_Communications__c, 
        	CommonUtility.getOptionsFromMultiSelect(candidate.Consent_Direct_Communications_ADO__c), 
        	RecordVals.MARKETING_CONSENT_4,
        	changeHistory,
        	candidate.CreatedDate)
        );        

        consents.addAll(prepareConsents(
        	candidate.Consent_Electronic_Communication__c, 
        	CommonUtility.getOptionsFromMultiSelect(candidate.Consent_Electronic_Communication_ADO__c), 
        	RecordVals.MARKETING_CONSENT_2,
        	changeHistory,
        	candidate.CreatedDate)
        );     

        consents.addAll(prepareConsents(
        	candidate.Consent_Graduates__c, 
        	CommonUtility.getOptionsFromMultiSelect(candidate.Consent_Graduates_ADO__c), 
        	RecordVals.MARKETING_CONSENT_5,
        	changeHistory,
        	candidate.CreatedDate)
        );     

        consents.addAll(prepareConsents(
        	candidate.Consent_Marketing__c, 
        	CommonUtility.getOptionsFromMultiSelect(candidate.Consent_Marketing_ADO__c), 
        	RecordVals.MARKETING_CONSENT_1,
        	changeHistory,
        	candidate.CreatedDate)
        );      

        return JSON.serialize(consents);
    }

    public static List<Consent_JSON> prepareConsents(Boolean consentValue, Set<String> adoNames, String consentName, List<String> changeHistory, DateTime creationDate) {
    	Set<String> predefeniedAdoSet = new Set<String> { CommonUtility.MARKETING_ENTITY_GDANSK, CommonUtility.MARKETING_ENTITY_POZNAN, 
    													  CommonUtility.MARKETING_ENTITY_TORUN, CommonUtility.MARKETING_ENTITY_WROCLAW };

		List<Consent_JSON> consetJSONForDefiniedConsent = new List<Consent_JSON>();    													  
    	if (consentValue && adoNames != null) {
    		for (String ado : adoNames) {
    			consetJSONForDefiniedConsent.add(new Consent_JSON(consentName + ' ' + ado, dateFromChangeHistory(changeHistory, consentName, ado, consentValue, creationDate), ado, true));
    			predefeniedAdoSet.remove(ado);
    		}
    	}

    	//if (predefeniedAdoSet.size() == 4) {
    	//	consetJSONForDefiniedConsent.add(new Consent_JSON(consentName + ' Globalna', dateFromChangeHistory(changeHistory, consentName, null, false, creationDate), false));
    	//}

    	//if (predefeniedAdoSet.size() == 0) {
    	//	consetJSONForDefiniedConsent.add(new Consent_JSON(consentName + ' Globalna', dateFromChangeHistory(changeHistory, consentName, null, true, creationDate), true));
    	//}

		for (String ado : predefeniedAdoSet) {
			consetJSONForDefiniedConsent.add(new Consent_JSON(consentName + ' ' + ado, dateFromChangeHistory(changeHistory, consentName, ado, false, creationDate), ado, false));
		}  

    	return consetJSONForDefiniedConsent;
    }

    public static String dateFromChangeHistory(List<String> changeHistory, String consentName, String ado, Boolean value, DateTime creationDate) {
    	for (String log : changeHistory) {
    		if (ado != null && log.contains(consentName) && log.contains(CommonUtility.getEntityByMarketingEntity(ado)) 
    			&& (value && log.contains('TAK') || !value && log.contains('NIE'))) {
    			List<String> logValues = log.split(' ');
    			return logValues[1] + ' ' + logValues[2];
    		}
    	}

    	return String.valueOf(creationDate);
    }

    public class Consent_JSON {
    	public String consent_name;
    	public String consent_date;
        public String department;
    	public Boolean value;

    	public Consent_JSON(String consent_name, String consent_date, String department, Boolean value) {
    		this.consent_name = consent_name;
    		this.consent_date = consent_date;
            this.department = department;
    		this.value = value;
    	}
    }
}