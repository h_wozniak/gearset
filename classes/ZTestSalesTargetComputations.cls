@isTest
private class ZTestSalesTargetComputations {
    

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- STUDY BATCHES ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_StudySalesTargetBatch() {
        Test.startTest();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        // user
        Profile p = ZDataTestUtility.getProfile(CommonUtility.PROFILE_SALES_DE);
        retMap.put(0, ZDataTestUtility.createUser(p, 'alias', 'TestSalesDE', 'TestSalesDE@test.com'));


        // offers
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = retMap.get(1).Id
        ), true));

        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = retMap.get(1).Id
        ), true));

        retMap.put(3, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(2).Id,
            Active__c = true
        ), true));

        ZDataTestUtility.createEnrollmentStudies(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(2).Id,
            Educational_Advisor__c = retMap.get(0).Id,
            Source_Studies__c = CommonUtility.ENROLLMENT_SOURCE_ST_EA,
            Enrollment_Date__c = Datetime.newInstance(2016, 3, 25),
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), 1, true);
        Test.stopTest();

        ZDataTestUtility.createEnrollmentStudies(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(2).Id,
            Source_Studies__c = CommonUtility.ENROLLMENT_SOURCE_ST_CC,
            Enrollment_Date__c = Datetime.newInstance(2016, 3, 26),
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), 1, true);

        // targets
        retMap.put(4, ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            University_Offer_Study__c = retMap.get(1).Id,
            Target_Realization__c = 1000
        ), true));

        retMap.put(5, ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = retMap.get(4).Id,
            Applies_from__c = Date.newInstance(2016, 3, 23),
            Applies_to__c = Date.newInstance(2016, 3, 25)
        ), true));

        retMap.put(6, ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = retMap.get(4).Id,
            Applies_from__c = Date.newInstance(2016, 3, 26),
            Applies_to__c = Date.newInstance(2016, 3, 26)
        ), true));

        retMap.put(7, ZDataTestUtility.createEduAdvTarget(new Target__c(
            Target_from_EduAdv_Target__c = retMap.get(4).Id,
            Educational_Advisor__c = retMap.get(0).Id
        ), true));

        retMap.put(8, ZDataTestUtility.createTeamTarget(new Target__c(
            Target_from_Team_Target__c = retMap.get(4).Id,
            Team__c = CommonUtility.TARGET_TEAM_CC
        ), true));

        // run batches
        SalesTargetComputations.executeStudiesBatch(true);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TRAININGS BATCHES ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareData_TrainingTargets_Open() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversity(new Account(
            Name = RecordVals.WSB_NAME_WRO
        ), true));


        retMap.put(1, ZDataTestUtility.createTrainingOffer(new Offer__c(
            Type__c = CommonUtility.OFFER_TYPE_OPEN,
            University_from_Training__c = retMap.get(0).Id
        ), true));

        retMap.put(2, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(1).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1500,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));

        retMap.put(3, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(2).Id
        ), true));

        retMap.put(4, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(3).Id,
            Training_Offer_for_Participant__c = retMap.get(2).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED,
            Enrollment_Value__c = ((Offer__c)retMap.get(2)).Price_per_Person__c
        ), true));

        return retMap;
    }   

    @isTest static void test_TrainingSalesTargetBatch_Open() {
        Map<Integer, sObject> dataMap = prepareData_TrainingTargets_Open();

        Test.startTest();

        Target__c targetOpen = ZDataTestUtility.createTrainingEnrollmentTarget(new Target__c(
            University_WSB__C = dataMap.get(0).Id,
            Target_Realization_curr__c = 1000000,
            Trainings_Type__c = CommonUtility.TARGET_TRAININGSTYPE_OPEN,
            Applies_from__c = Date.today(),
            Applies_to__c = Date.today()
        ), true);

        Target__c targetBoth = ZDataTestUtility.createTrainingEnrollmentTarget(new Target__c(
            University_WSB__C = dataMap.get(0).Id,
            Target_Realization_curr__c = 1000000,
            Trainings_Type__c = CommonUtility.TARGET_TRAININGSTYPE_OPENCLOSED,
            Applies_from__c = Date.today(),
            Applies_to__c = Date.today()
        ), true);

        // run batches
        SalesTargetComputations.executeTrainingsBatch(true);

        Test.stopTest();


        Target__c targetOpenAfter = [SELECT Realization_Amount_curr__c, Realization_Amount_Batch_Helper_curr__c FROM Target__c WHERE Id = :targetOpen.Id];
        Target__c targetBothAfter = [SELECT Realization_Amount_curr__c FROM Target__c WHERE Id = :targetBoth.Id];

        System.assertEquals(0, targetOpenAfter.Realization_Amount_Batch_Helper_curr__c);
        System.assertEquals(1500, targetOpenAfter.Realization_Amount_curr__c);
        System.assertEquals(1500, targetBothAfter.Realization_Amount_curr__c);
    }


   
    private static Map<Integer, sObject> prepareData_TrainingTargets_Closed() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
       
        retMap.put(0, ZDataTestUtility.createUniversity(new Account(
            Name = RecordVals.WSB_NAME_WRO
        ), true));

        retMap.put(5, ZDataTestUtility.createTrainingOffer(new Offer__c(
            Type__c = CommonUtility.OFFER_TYPE_CLOSED,
            University_from_Training__c = retMap.get(0).Id
        ), true));
        retMap.put(6, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(5).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Offer_Value__c = 2000,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(7, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(6).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED,
            Enrollment_Value__c = ((Offer__c)retMap.get(6)).Offer_Value__c,
            Company__c = ZDataTestUtility.createCompany(new Account(
                NIP__c = '3672620147'
            ), true).Id
        ), true));
        retMap.put(8, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(7).Id,
            Training_Offer_for_Participant__c = retMap.get(6).Id
        ), true));

        return retMap;
    }

    @isTest static void test_TrainingSalesTargetBatch_Closed() {
        Map<Integer, sObject> dataMap = prepareData_TrainingTargets_Closed();

        Test.startTest();

        // targets
        Target__c targetClosed = ZDataTestUtility.createTrainingEnrollmentTarget(new Target__c(
            University_WSB__C = dataMap.get(0).Id,
            Target_Realization_curr__c = 1000000,
            Trainings_Type__c = CommonUtility.TARGET_TRAININGSTYPE_CLOSED,
            Applies_from__c = Date.today(),
            Applies_to__c = Date.today()
        ), true);

        Target__c targetBoth = ZDataTestUtility.createTrainingEnrollmentTarget(new Target__c(
            University_WSB__C = dataMap.get(0).Id,
            Target_Realization_curr__c = 1000000,
            Trainings_Type__c = CommonUtility.TARGET_TRAININGSTYPE_OPENCLOSED,
            Applies_from__c = Date.today(),
            Applies_to__c = Date.today()
        ), true);

        // run batches
        SalesTargetComputations.executeTrainingsBatch(true);

        Test.stopTest();


        Target__c targetClosedAfter = [SELECT Realization_Amount_curr__c, Realization_Amount_Batch_Helper_curr__c FROM Target__c WHERE Id = :targetClosed.Id];
        Target__c targetBothAfter = [SELECT Realization_Amount_curr__c FROM Target__c WHERE Id = :targetBoth.Id];

        System.assertEquals(2000, targetClosedAfter.Realization_Amount_curr__c);
        System.assertEquals(0, targetClosedAfter.Realization_Amount_Batch_Helper_curr__c);
        System.assertEquals(2000, targetBothAfter.Realization_Amount_curr__c);
    }

}