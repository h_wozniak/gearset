/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Study Enrollment documents list
*	Błąd #1729 - CRM PRODUKCJA - R-00362588 - brak automatycznej wysyłki dokumentów rekrutacyjnych do kandydata
**/

/**
*   To initiate the ResendNotSentDocumentEmail execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('ResendNotSentDocumentEmail', '0 0 8 * * ? *', new ResendNotSentDocumentEmail());
**/


global class ResendNotSentDocumentEmail implements Schedulable {

    global void execute(SchedulableContext sc) {
        ResendNotSentDocumentEmailBatch sendDocumentEmailBatch = new ResendNotSentDocumentEmailBatch();
        Database.executebatch(sendDocumentEmailBatch, 1);
    }
}