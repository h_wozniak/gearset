/**
* @description  This class is a Trigger Handler for Trainings - rewriting field values (for emails/preview)
**/

public without sharing class TH_OfferRewrites extends TriggerHandler.DelegateBase {
    

    List<Offer__c> schedulesWithUpdatedValidToDate;
    List<Offer__c> schedulesToCopyFieldFromTO;
    List<Offer__c> trainingOffersToSetUniverName;
    List<Offer__c> tOffersToUpdateScheduleTradeName;
    Set<Id> schedulesToUpdateEnrollmentForEmailFields;
    Set<Id> trainingOffersToUpdateEnrollmentForEmailfields;
    List<Offer__c> schedulesToCopyUniversityContactData;
    List<Offer__c> trainingsToCopyFieldFrom;


    public override void prepareBefore() {
        schedulesToCopyFieldFromTO = new List<Offer__c>();
        trainingOffersToSetUniverName = new List<Offer__c>();
        schedulesToCopyUniversityContactData = new List<Offer__c>();
    }

    public override void prepareAfter() {
        schedulesWithUpdatedValidToDate = new List<Offer__c>();
        tOffersToUpdateScheduleTradeName = new List<Offer__c>();
        schedulesToUpdateEnrollmentForEmailFields = new Set<Id>();
        trainingOffersToUpdateEnrollmentForEmailfields = new Set<Id>();
        trainingsToCopyFieldFrom = new List<Offer__c>();
    }

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Offer__c newOffer = (Offer__c)obj;

            /* Wojciech Słodziak */
            // copy Trade Name from Univer Lookup on Training Offer Insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)) {
                trainingOffersToSetUniverName.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields from Training Offer on Schedule insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                schedulesToCopyFieldFromTO.add(newOffer);
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c newOffer;
        for(Id key : newOffers.keySet()) {
            newOffer = newOffers.get(key);
        }
    }

    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c oldOffer;
        Offer__c newOffer;
        for(Id key : newOffers.keySet()) {
            oldOffer = oldOffers.get(key);
            newOffer = newOffers.get(key);

            /* Wojciech Słodziak */
            // if Valid_To__c changed update this value on hidden field on Enrollment 
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                && newOffer.Valid_To__c != oldOffer.Valid_To__c) {
                schedulesWithUpdatedValidToDate.add(newOffer);
            }

            /* Wojciech Słodziak */
            // if Trade name changes on Training Offer - update Training Schedule's Trade Names
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                && newOffer.Trade_Name__c != oldOffer.Trade_Name__c) {
                tOffersToUpdateScheduleTradeName.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy for email fields from Schedule on edit
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
                ) 
                && (
                    newOffer.Valid_From__c != oldOffer.Valid_From__c 
                    || newOffer.Valid_To__c != oldOffer.Valid_To__c 
                    || newOffer.Payment_Deadline__c != oldOffer.Payment_Deadline__c 
                    || newOffer.University_City_in_Vocative__c != oldOffer.University_City_in_Vocative__c 
                    || newOffer.Street__c != oldOffer.Street__c || newOffer.City__c != oldOffer.City__c 
                    || newOffer.Bank_Account_No__c != oldOffer.Bank_Account_No__c
                )
            ) {
                schedulesToUpdateEnrollmentForEmailFields.add(newOffer.Id);
            }

            /* Wojciech Słodziak */
            // copy for email fields from Schedule trainingOffer Administrator change
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                && newOffer.Training_Administrator__c != oldOffer.Training_Administrator__c) {
                trainingOffersToUpdateEnrollmentForEmailfields.add(newOffer.Id);
            }

            /* Sebastian Łasisz */
            // update of field data on value change
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                && newOffer.Field__c != oldOffer.Field__c) {
                trainingsToCopyFieldFrom.add(newOffer);
            }


        }
    }

    //public override void beforeDelete(Map<Id, sObject> old) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {

        /* Wojciech Słodziak */
        // copy Name from Univer Lookup on Training Offer Insert
        if (trainingOffersToSetUniverName != null && !trainingOffersToSetUniverName.isEmpty()) {
            Set<Id> universitiesId = new Set<Id>();
            for (Offer__c to : trainingOffersToSetUniverName) {
                universitiesId.add(to.University_from_Training__c);
            }

            List<Account> universities = [
                SELECT Name 
                FROM Account 
                WHERE Id IN :universitiesId
            ];

            for (Offer__c to : trainingOffersToSetUniverName) {
                for (Account univer : universities) {
                    if (to.University_from_Training__c == univer.Id) {
                        to.University_Name__c = univer.Name;
                    }
                }
            }
        }

        /* Wojciech Słodziak */
        // copy Trade Name from Training Offer on schedule insert
        // copy Field from Training Offer on schedule Insert
        if (schedulesToCopyFieldFromTO != null && !schedulesToCopyFieldFromTO.isEmpty()) {
            Set<Id> trainingOfferIds = new Set<Id>();
            for (Offer__c sch : schedulesToCopyFieldFromTO) {
                trainingOfferIds.add(sch.Training_Offer_from_Schedule__c);
            }
            List<Offer__c> trainingOfferList = [SELECT Id, Trade_Name__c, University_from_Training__r.Name, Field__c, University_from_Training__r.BillingCity,
                                                University_from_Training__r.Contact_Data_for_Trainings__c 
                                                FROM Offer__c 
                                                WHERE Id IN :trainingOfferIds 
                                                AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)];

            for (Offer__c sch : schedulesToCopyFieldFromTO) {
                for (Offer__c to : trainingOfferList) {
                    if (sch.Training_Offer_from_Schedule__c == to.Id) {
                        sch.Trade_Name__c = to.Trade_Name__c;
                        sch.University_Name__c = to.University_from_Training__r.Name;
                        sch.Field__c = to.Field__c;
                        if (to.University_from_Training__r.BillingCity != null) {
                            sch.University_City_in_Vocative__c = VocativeRule.generateNameInVocative(to.University_from_Training__r.BillingCity);
                        }
                        sch.University_Contact_Data__c = to.University_from_Training__r.Contact_Data_for_Trainings__c;
                    }
                }
            }
        }

        /* Wojciech Słodziak */
        // if Valid_To__c changed update this value on hidden field on Enrollment 
        if (schedulesWithUpdatedValidToDate != null && !schedulesWithUpdatedValidToDate.isEmpty()) {
            Set<Id> scheduleIds = new Set<Id>();
            for (Offer__c schedule : schedulesWithUpdatedValidToDate) {
                scheduleIds.add(schedule.Id);
            }

            List<Enrollment__c> mainEnrollments = [
                SELECT Id, Training_Offer__c, Schedule_End_Time_Copy__c 
                FROM Enrollment__c 
                WHERE Training_Offer__c IN :scheduleIds 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
            ];

            for (Offer__c sch : schedulesWithUpdatedValidToDate) {
                for (Enrollment__c enr : mainEnrollments) {
                    if (sch.Id == enr.Training_Offer__c) {
                        enr.Schedule_End_Time_Copy__c = sch.Valid_To__c;
                    }
                }
            }

            try {
                update mainEnrollments;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // update Trade Name on schedule if it changes on Training Offer
        if (tOffersToUpdateScheduleTradeName != null && !tOffersToUpdateScheduleTradeName.isEmpty()) {
            Set<Id> trainingOfferIds = new Set<Id>();
            for (Offer__c to : tOffersToUpdateScheduleTradeName) {
                trainingOfferIds.add(to.Id);
            }

            List<Offer__c> scheduleList = [
                SELECT Id, Trade_Name__c, Training_Offer_from_Schedule__c 
                FROM Offer__c 
                WHERE Training_Offer_from_Schedule__c IN :trainingOfferIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Offer__c to : tOffersToUpdateScheduleTradeName) {
                for (Offer__c sch : scheduleList) {
                    if (to.Id == sch.Training_Offer_from_Schedule__c) {
                        sch.Trade_Name__c = to.Trade_Name__c;
                    }
                }
            }

            try {
                update scheduleList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // copy for email fields from Schedule on edit
        if (schedulesToUpdateEnrollmentForEmailFields != null && !schedulesToUpdateEnrollmentForEmailFields.isEmpty()) {
            EnrollmentManager_Trainings.copyForEmailFields(schedulesToUpdateEnrollmentForEmailFields);
        }

        /* Wojciech Słodziak */
        // copy for email fields from Schedule on TrainingOffer Administrator change
        if (trainingOffersToUpdateEnrollmentForEmailfields != null && !trainingOffersToUpdateEnrollmentForEmailfields.isEmpty()) {
            List<Offer__c> affectedSchedules = [
                SELECT Id 
                FROM Offer__c 
                WHERE Training_Offer_from_Schedule__c IN :trainingOffersToUpdateEnrollmentForEmailfields 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            Set<Id> affectedSchedulesIds = new Set<Id>();

            for (Offer__c sch : affectedSchedules) {
                affectedSchedulesIds.add(sch.Id);
            }

            EnrollmentManager_Trainings.copyForEmailFields(affectedSchedulesIds);
        }

        /* Sebastian Łasisz */
        // update Field data on Schedule from Offer
        if (trainingsToCopyFieldFrom != null && !trainingsToCopyFieldFrom.isEmpty()) {
            Set<Id> trainingIds = new Set<Id>();
            for (Offer__c trainingOffer : trainingsToCopyFieldFrom) {
                trainingIds.add(trainingOffer.Id);
            }

            List<Offer__c> scheduleOffers = [
                SELECT Id, Field__c, Training_Offer_from_Schedule__c 
                FROM Offer__c 
                WHERE Training_Offer_from_Schedule__c IN :trainingIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Offer__c trainingOffer : trainingsToCopyFieldFrom) {
                for (Offer__c schedule : scheduleOffers) {
                    if (schedule.Training_Offer_from_Schedule__c == trainingOffer.Id) {
                        schedule.Field__c = trainingOffer.Field__c;
                    }
                }
            }
            
            try {
                update scheduleOffers;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

    }

}