/**
* @author       Grzegorz Długosz
* @description  Test class for IntegrationFCCInteractionReceiver (CRM Sends request to ESB [FCC] and tries to get call history then save it).
**/

@isTest
private class  ZTestIntegrationFCCInteractionReceiver {

    private static void prepareCampaigns() {
        List<Marketing_Campaign__c> extCampaignsList = new List<Marketing_Campaign__c>();

        Marketing_Campaign__c extCampaign1 = new Marketing_Campaign__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
            Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
            Campaign_Name__c = 'extCampaignName_1',
            External_Id__c = '941'
            );

        Marketing_Campaign__c extCampaign2 = new Marketing_Campaign__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
            Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
            Campaign_Name__c = 'extCampaignName_2',
            External_Id__c = '942'
            );

        Marketing_Campaign__c extCampaign3 = new Marketing_Campaign__c(
            RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
            Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
            Campaign_Name__c = 'extCampaignName_3',
            External_Id__c = '943'
            );

        extCampaignsList.add(extCampaign1);
        extCampaignsList.add(extCampaign2);
        extCampaignsList.add(extCampaign3);

        insert extCampaignsList;
    }

    private static List<String> prepareContacts() {

        List<Contact> testContacts = new List<Contact>();
        List<String> contactsIds = new List<String>();

        Contact cnt1 = new Contact(
            LastName = 'testContact1', 
            Phone = '123456789', 
            Email = 'testContact1@gmail.com', 
            The_Same_Mailling_Address__c = false,
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED, 
            RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
            External_Contact_Id__c = 'testExternalId1'
            );

        Contact cnt2 = new Contact(
            LastName = 'testContact2', 
            Phone = '987654321', 
            Email = 'testContact2@gmail.com', 
            The_Same_Mailling_Address__c = false,
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED, 
            RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
            External_Contact_Id__c = 'testExternalId2'
            );

        testContacts.add(cnt1);
        testContacts.add(cnt2);

        insert testContacts;

        for (Contact cnt : testContacts) {
            contactsIds.add(cnt.Id);
        }

        return contactsIds;
    }

    private static void prepareCustomSettings() {
        FCC_Last_Interaction_Retrival_Date__c fccLastInteraction = new FCC_Last_Interaction_Retrival_Date__c();
        fccLastInteraction.Date_From__c = DateTime.valueOf('2014-10-10 15:21:00');

        insert fccLastInteraction;
    }

    @isTest static void successfulInvocation_200() {

        Test.setMock(HttpCalloutMock.class, new IntegrationFCCInteractionWebserviceMock());
        List<String> cntsIds = prepareContacts();
        prepareCustomSettings();
        prepareCampaigns();

        Test.startTest();
            IntegrationFCCInteractionReceiver.sendInteractionRequestSync();
        Test.stopTest();
        
        List<Task> listOfTasks = [
            SELECT WhoId, Status, RecordTypeId, Agent_Id__c, Call_Type__c, ActivityDate, Phone_Number__c
            FROM Task 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL)
        ];

        List<Task> listOfTasksForContact1 = [
            SELECT WhoId, Status, RecordTypeId, Agent_Id__c, Call_Type__c, ActivityDate, Phone_Number__c
            FROM Task 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL)
            AND WhoId = :cntsIds.get(0)
        ];

        List<Task> listOfTasksForContact2 = [
            SELECT WhoId, Status, RecordTypeId, Agent_Id__c, Call_Type__c, ActivityDate, Phone_Number__c
            FROM Task 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL)
            AND WhoId = :cntsIds.get(1)
        ];

        //system.assertEquals(2, listOfTasksForContact1.size());
        //system.assertEquals(1, listOfTasksForContact2.size());
        //system.assertEquals(3, listOfTasks.size());

    }

    @isTest static void unsuccessfulInvocation_Error() {

        Test.setMock(HttpCalloutMock.class, new IntegrationFCCInteractionWsMockError());
        prepareContacts();
        prepareCustomSettings();
        prepareCampaigns();

        Test.startTest();
            IntegrationFCCInteractionReceiver.sendInteractionRequestAsync();
        Test.stopTest();
        
        List<Task> listOfTasks = [
            SELECT WhoId, Status, RecordTypeId, Agent_Id__c, Call_Type__c, ActivityDate, Phone_Number__c
            FROM Task 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL)
        ];

        system.assertEquals(0, listOfTasks.size());
    }

    @isTest static void successfulInvocation_200_EmptyList() {

        Test.setMock(HttpCalloutMock.class, new IntegrationFCCInteractionWebserviceMockEmpty());
        prepareContacts();
        prepareCustomSettings();
        prepareCampaigns();

        Test.startTest();
            IntegrationFCCInteractionReceiver.sendInteractionRequestSync();
        Test.stopTest();
        
        List<Task> listOfTasks = [
            SELECT WhoId, Status, RecordTypeId, Agent_Id__c, Call_Type__c, ActivityDate, Phone_Number__c
            FROM Task 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL)
        ];

        system.assertEquals(0, listOfTasks.size());
    }

    public class IntegrationFCCInteractionWebserviceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');

            List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> requestInteractionsJson = new List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>();

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson1 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-10', 
                'testAgentId1', 
                'nameOfTestAgent1', 
                'Outcoming', 
                'ANSWER', 
                '123456789',
                '123456789');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson2 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId2', 
                '2015-10-11', 
                'testAgentId2', 
                'nameOfTestAgent2', 
                'Incoming', 
                'NO ANSWER', 
                '987654321',
                '987654321');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson3 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-12', 
                'testAgentId1', 
                'nameOfTestAgent1', 
                'Incoming', 
                'BUSY', 
                '123456789',
                '12345679');

            requestInteractionsJson.add(requestInteractionJson1);
            requestInteractionsJson.add(requestInteractionJson2);
            requestInteractionsJson.add(requestInteractionJson3);

            String preparedJson = JSON.serializePretty(requestInteractionsJson);

            res.setBody(preparedJson);

            return res;
        }

    }

    public class IntegrationFCCInteractionWsMockError implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(400);
            res.setStatus('Bad Request');

            List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> requestInteractionsJson = new List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>();

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson1 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-10', 
                'testAgentId1', 
                'nameOfTestAgent1', 
                'Outcoming', 
                'ANSWER', 
                '123456789',
                '123456789');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson2 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId2', 
                '2015-10-11', 
                'testAgentId2', 
                'nameOfTestAgent2', 
                'Incoming', 
                'NO ANSWER', 
                '987654321',
                '12345679');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson3 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-12', 
                'testAgentId1', 
                'nameOfTestAgent1', 
                'Incoming', 
                'BUSY', 
                '123456789',
                '12356789');

            requestInteractionsJson.add(requestInteractionJson1);
            requestInteractionsJson.add(requestInteractionJson2);
            requestInteractionsJson.add(requestInteractionJson3);

            String preparedJson = JSON.serializePretty(requestInteractionsJson);

            res.setBody(preparedJson);

            return res;
        }

    }

    public class IntegrationFCCInteractionWebserviceMockEmpty implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');

            List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> requestInteractionsJson = new List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>();

            String preparedJson = JSON.serializePretty(requestInteractionsJson);

            res.setBody(preparedJson);

            return res;
        }

    }

}