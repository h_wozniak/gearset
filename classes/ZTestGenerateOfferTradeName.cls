@isTest
private class ZTestGenerateOfferTradeName {

    @isTest static void testGeneratingOfferTradeNameFromCourseOffer() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);

        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        Test.stopTest();

        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateOfferTradeName.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, 'Test1');
    }

    @isTest static void testGeneratingOfferTradeNameFromSpecialtyOffer_emptyTN() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id, Initial_Specialty_Declaration__c = specialtyOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        Test.stopTest();
        
        String result = DocGen_GenerateOfferTradeName.executeMethod(studyEnrollment.Id, null, null);
        System.assertEquals(result, null);
    }

    @isTest static void testGeneratingOfferTradeNameFromSpecialtyOffer_TN() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id, Trade_Name__c = 'Test1'), true);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id, Initial_Specialty_Declaration__c = specialtyOffer.ID), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        Test.stopTest();
        
        String result = DocGen_GenerateOfferTradeName.executeMethod(studyEnrollment.Id, null, null);
        System.assertEquals(result, null);
    }
    
    @isTest static void testGeneratingOfferTradeNameFromSpecialtyOffer() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id, Specialty_Trade_Name__c = 'Test2'), true);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        Test.stopTest();
        
        String result = DocGen_GenerateOfferTradeName.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, 'Test1, {}: Test2');
    }
    
    @isTest static void testGeneratingOfferTradeNameFromSpecialtyOfferAndSpecialization() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id, Specialty_Trade_Name__c = 'Test2'), true);
        Offer__c specializationOffer = ZDataTestUtility.createSpecializationOffer(new Offer__c(Trade_Name__c = 'Test3'), true);

        Test.startTest();

        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id, Specialization__c = specializationOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateOfferTradeName.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, 'Test1, {}: Test2, {}: Test3');
        Test.stopTest();
    }
    
    @isTest static void testGeneratingOfferTradeNameFromSpecializationOffer() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id, Trade_Name__c = 'Test1'), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id, Trade_Name__c = 'Test2'), true);
        Offer__c specializationOffer = ZDataTestUtility.createSpecializationOffer(null, true);
                
        Test.startTest();

        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id, specialization__c = specializationOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        String result = DocGen_GenerateOfferTradeName.executeMethod(documentEnrollment.Id, null, null);
        System.assertNotEquals(result, '');
        Test.stopTest();
    }
}