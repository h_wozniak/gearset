@isTest
private class ZTestIntegrationContactUpdateOther {
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(
            Phone = '123456789',
            Firstname = 'Bob',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Pesel__c = '05261508023',
            Country_of_Origin__c = 'Poland',
            The_Same_Mailling_Address__c = true,
            OtherCountry = 'Poland',
            OtherState = 'Lower Silesia',
            OtherCity = 'Wrocław',
            OtherStreet = 'Krzyki'
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = retMap.get(0).Id), true));

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;
        
        return retMap;
    }

	@isTest static void test_sendUpdatedContactToOtherSystems() {
		Map<Integer, sObject> retMap = prepareData();

		Test.startTest();
        Map<String, String> newMap = new Map<String, String>();
        newMap.put(retMap.get(0).Id, 'Experia;ZPI,MA,FCC');

		IntegrationContactUpdateOther.sendUpdatedContactToOtherSystems(newMap);
		Test.stopTest();
	}
}