/**
* @description  This class is a Trigger Handler for Trainings - field edit disabling
**/

public without sharing class TH_OfferDisables2 extends TriggerHandler.DelegateBase {


    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c oldOffer;
        Offer__c newOffer;
        for (Id key : newOffers.keySet()) {
            oldOffer = oldOffers.get(key);
            newOffer = newOffers.get(key);



            /* ------------------------------------------------------------------------------------------------ */
            /* ----------------------------------- UNIVERSITY STUDY OFFER ------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of Degree__c on University Study Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_UNIVERSITY_OFFER_STUDY) 
                && newOffer.Degree__c != oldOffer.Degree__c && oldOffer.Degree__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Degree__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of University on University Study Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_UNIVERSITY_OFFER_STUDY) 
                && newOffer.University__c != oldOffer.University__c && oldOffer.University__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'University__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Study Start Date on University Study Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_UNIVERSITY_OFFER_STUDY) 
                && newOffer.Study_Start_Date__c != oldOffer.Study_Start_Date__c && oldOffer.Study_Start_Date__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Study_Start_Date__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ---------------------------------------- COURSE OFFER ------------------------------------------ */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of University_Study_Offer_from_Course__c on Course
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.University_Study_Offer_from_Course__c != oldOffer.University_Study_Offer_from_Course__c 
                && oldOffer.University_Study_Offer_from_Course__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'University_Study_Offer_from_Course__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Kind on Course
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Kind__c != oldOffer.Kind__c && oldOffer.Kind__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Kind__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Mode on Course
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Mode__c != oldOffer.Mode__c && oldOffer.Mode__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Mode__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Number of Semesters on Course
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Number_of_Semesters__c != oldOffer.Number_of_Semesters__c && oldOffer.Number_of_Semesters__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Number_of_Semesters__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* --------------------------------------- SPECIALTY OFFER ---------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of Course_Offer_from_Specialty__c on Specialty
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER) 
                && newOffer.Course_Offer_from_Specialty__c != oldOffer.Course_Offer_from_Specialty__c && oldOffer.Course_Offer_from_Specialty__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Course_Offer_from_Specialty__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ---------------------------------- COURSE & SPECIALTY OFFER ------------------------------------ */
            /* ------------------------------------------------------------------------------------------------ */
         
            /* Wojciech Słodziak */
            // Disable editing of Short Name on Course and Specialty
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
                ) 
                && newOffer.Short_Name__c != oldOffer.Short_Name__c && oldOffer.Short_Name__c != null
            ) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Short_Name__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* --------------------------------------- DOCUMENT OFFER ----------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Sebastian Łasisz */
            // Disable editing Document lookup on Offer Document
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_REQUIRED_DOCUMENT) 
                && newOffer.Document__c != oldOffer.Document__c) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Document__c'));    
            }
  
            /* Wojciech Słodziak */
            // Disable editing Offer_from_Document__c lookup on Offer Document
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_REQUIRED_DOCUMENT) 
                && newOffer.Offer_from_Document__c != oldOffer.Offer_from_Document__c) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Offer_from_Document__c'));    
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* --------------------------------------- LANGUAGE OFFER ----------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Sebastian Łasisz */
            // Disable editing Language picklist on Language Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE) 
                && newOffer.Language__c != oldOffer.Language__c) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Language__c'));    
            }
  
            /* Wojciech Słodziak */
            // Disable editing Course_Offer_from_Language__c lookup on Language Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE) 
                && newOffer.Course_Offer_from_Language__c != oldOffer.Course_Offer_from_Language__c) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Course_Offer_from_Language__c'));    
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ------------------------------------ SPECIALIZATION OFFER -------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of Offer_from_Specialization__c on Specialization
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_SPECIALIZATION) 
                && newOffer.Offer_from_Specialization__c != oldOffer.Offer_from_Specialization__c) { 
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Offer_from_Specialization__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Specialization__c on Specialization
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_SPECIALIZATION) 
                && newOffer.Specialization__c != oldOffer.Specialization__c) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Specialization__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ---------------------------------------- PRICE BOOKS ------------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of Price_Book_from_Installment_Config__c on PB Installment Config
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_PB_INSTALLMENT_CFG) 
                && newOffer.Price_Book_from_Installment_Config__c != oldOffer.Price_Book_from_Installment_Config__c 
                && oldOffer.Price_Book_from_Installment_Config__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Price_Book_from_Installment_Config__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Price_Book_Type__c on Price Book
            if (PriceBookManager.priceBookRecordTypeIds.contains(newOffer.RecordTypeId) 
                && newOffer.Price_Book_Type__c != oldOffer.Price_Book_Type__c && oldOffer.Price_Book_Type__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Price_Book_Type__c'));
            }

            /* Wojciech Słodziak */
            // Disable editing of Offer_from_Price_Book__c on Price Book
            if (PriceBookManager.priceBookRecordTypeIds.contains(newOffer.RecordTypeId) 
                && newOffer.Offer_from_Price_Book__c != oldOffer.Offer_from_Price_Book__c 
                && oldOffer.Offer_from_Price_Book__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Offer_from_Price_Book__c'));
            }

        }
    }

}