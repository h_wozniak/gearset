/**
* @author       Wojciech Słodziak
* @description  Class for accessing resources required in EmailTemplates
**/

public without sharing class EmailTemplateComponentController {

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- CONFIG --------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* WSB LOGOS PORTALS */
    public final static String BYD_LOGO_API = 'WSB_Byd_LOGO';
    public final static String CHO_LOGO_API = 'WSB_Cho_LOGO';
    public final static String GDAGDY_LOGO_API = 'WSB_GdaGdy_LOGO';
    public final static String OPO_LOGO_API = 'WSB_Opo_LOGO';
    public final static String POZ_LOGO_API = 'WSB_Poz_LOGO';
    public final static String SZC_LOGO_API = 'WSB_SZC_LOGO';
    public final static String TOR_LOGO_API = 'WSB_Tor_LOGO';
    public final static String WRO_LOGO_API = 'WSB_Wro_LOGO';



    /* COMMUNITY PORTALS */
    public final static String FB_LOGO_API = 'FB_LOGO';
    public final static String IN_LOGO_API = 'IN_LOGO';
    public final static String YT_LOGO_API = 'YT_LOGO';
    public final static String TW_LOGO_API = 'TW_LOGO';
    public final static String PRZEWODNIK_LOGO_API = 'PRZEWODNIK_LOGO';



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- INIT ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public String universityNameParam { get; set; }
    public Id enrollmentId { get; set; }
    public Id accountId { get; set; }

    public Datetime eventDateTime { get; set; }
    public String BillingCity { get; set; }
    public String BillingStreet { get; set; }
    public String BillingPostalCode { get; set; }
    public String Name { get; set; }

    public String mba_TitleName { get; set; }
    public String mba_Street { get; set; }
    public String mba_PostAndCity { get; set; }
    public String mba_phone { get; set; }
    public String mba_email { get; set; }


    public EmailTemplateComponentController() {}



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- COMPANY LOGOS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String mapUniversityNameToImgLogoApi(String universityName) {
        Map<String, String> resultMap = new Map<String, String> {
                RecordVals.WSB_NAME_BYD => BYD_LOGO_API,
                RecordVals.WSB_NAME_CHO => CHO_LOGO_API,
                RecordVals.WSB_NAME_GDA => GDAGDY_LOGO_API,
                RecordVals.WSB_NAME_GDY => GDAGDY_LOGO_API,
                RecordVals.WSB_NAME_OPO => OPO_LOGO_API,
                RecordVals.WSB_NAME_POZ => POZ_LOGO_API,
                RecordVals.WSB_NAME_TOR => TOR_LOGO_API,
                RecordVals.WSB_NAME_SZC => SZC_LOGO_API,
                RecordVals.WSB_NAME_WRO => WRO_LOGO_API
        };

        return resultMap.get(universityName);
    }

    public String getCompanyLogoURL() {
        String imgApiName = EmailTemplateComponentController.mapUniversityNameToImgLogoApi(universityNameParam);
        return EmailTemplateComponentController.getImgURLByApiName(imgApiName);
    }

    public Boolean getDegreeMBA() {
        if (enrollmentId != null) {
            Enrollment__c studyEnr = [
                    SELECT Id, Degree__c
                    FROM Enrollment__c
                    WHERE Id = :enrollmentId
            ];

            List<Account> accList = [
                    SELECT Id, BillingStreet, BillingCity, BillingPostalCode, NIP__c, Parent.NIP__c, MBA_office_address__c, PG_office_address__c,
                            Bachelors_office_address__c, Master_office_address__c
                    FROM Account
                    WHERE Name = :universityNameParam
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
            ];

            if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA && accList.get(0).MBA_office_address__c != null) {
                List<String> parsedAddress = accList.get(0).MBA_office_address__c.split(', ');
                mba_TitleName = parsedAddress.get(0);
                mba_Street = parsedAddress.get(1);
                mba_PostAndCity = parsedAddress.get(2);
                mba_phone = parsedAddress.get(3);

                if (parsedAddress.size() <= 4) {
                    mba_email = '';
                }
                else {
                    mba_email = parsedAddress.get(4);
                }

                return true;
            }
            else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG && accList.get(0).PG_office_address__c != null) {
                List<String> parsedAddress = accList.get(0).PG_office_address__c.split(', ');
                mba_TitleName = parsedAddress.get(0);
                mba_Street = parsedAddress.get(1);
                mba_PostAndCity = parsedAddress.get(2);
                mba_phone = parsedAddress.get(3);

                if (parsedAddress.size() <= 4) {
                    mba_email = '';
                }
                else {
                    mba_email = parsedAddress.get(4);
                }

                return true;
            }
            else if ((studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I) && accList.get(0).Bachelors_office_address__c != null) {
                List<String> parsedAddress = accList.get(0).Bachelors_office_address__c.split(', ');
                mba_TitleName = parsedAddress.get(0);
                mba_Street = parsedAddress.get(1);
                mba_PostAndCity = parsedAddress.get(2);
                mba_phone = parsedAddress.get(3);

                if (parsedAddress.size() <= 4) {
                    mba_email = '';
                }
                else {
                    mba_email = parsedAddress.get(4);
                }

                return true;
            }
            else if ((studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II) && accList.get(0).Master_office_address__c != null) {
                List<String> parsedAddress = accList.get(0).Master_office_address__c.split(', ');
                mba_TitleName = parsedAddress.get(0);
                mba_Street = parsedAddress.get(1);
                mba_PostAndCity = parsedAddress.get(2);
                mba_phone = parsedAddress.get(3);

                if (parsedAddress.size() <= 4) {
                    mba_email = '';
                }
                else {
                    mba_email = parsedAddress.get(4);
                }

                return true;
            }

            mba_email = 'asdasd';
        }

        return false;
    }

    public String getMBAOfficeName() {
        if (enrollmentId != null) {
            Enrollment__c studyEnr = [
                    SELECT Id, Degree__c
                    FROM Enrollment__c
                    WHERE Id = :enrollmentId
            ];

            List<Account> accList = [
                    SELECT Id, BillingStreet, BillingCity, BillingPostalCode, NIP__c, Parent.NIP__c, MBA_office_address__c
                    FROM Account
                    WHERE Name = :universityNameParam
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
            ];

            if (accList.get(0).MBA_office_address__c != null) {
                List<String> parsedAddress = accList.get(0).MBA_office_address__c.split(', ');
                return parsedAddress.get(0).replace('Biuro', 'Biura');
            }
        }

        return 'Biura Rekrutacji';
    }

    public Account getPersonContact() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, Visit_Contact_Person__c, Visit_Contact_Person__r.FirstName, Visit_Contact_Person__r.LastName, Visit_Contact_Person__r.Phone, Visit_Contact_Person__r.Email
                    FROM Account
                    WHERE Id = :accountId
            ];

            return highSchool;
        }

        return null;
    }

    public String getCityNameInLocative() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, WSB_Area__c
                    FROM Account
                    WHERE Id = :accountId
            ];

            Account university = [
                    SELECT Id, Name, University_City_in_Locative_with_Prefix__c
                    FROM Account
                    WHERE Name = :highSchool.WSB_Area__c
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                    LIMIT 1
            ];

            return university.University_City_in_Locative_with_Prefix__c;
        }

        return null;
    }

    public String getEventDate() {
        if (eventDateTime != null) {
            Integer integerDay = eventDateTime.date().day();
            String day = (integerDay < 10 ? '0' + integerDay : String.valueOf(integerDay));

            Integer integerMonth = eventDateTime.date().month();
            String month = (integerMonth < 10 ? '0' + integerMonth : String.valueOf(integerMonth));

            return day + '.' + month + '.' + eventDateTime.date().year();
        }

        return null;
    }

    public String getEventTime() {
        if (eventDateTime != null) {
            Integer integerHour = eventDateTime.time().hour();
            String hour = (integerHour < 10 ? '0' + integerHour : String.valueOf(integerHour));

            Integer integerMinute = eventDateTime.time().minute();
            String minute = (integerMinute < 10 ? '0' + integerMinute : String.valueOf(integerMinute));

            return hour + ':' + minute;
        }

        return null;
    }

    public String getHighSchoolCity() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, BillingCity
                    FROM Account
                    WHERE Id = :accountId
            ];

            return highSchool.BillingCity;

        }

        return null;
    }

    public String getHighSchoolStreet() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, BillingStreet
                    FROM Account
                    WHERE Id = :accountId
            ];

            return highSchool.BillingStreet;

        }

        return null;
    }

    public String getHighSchoolPostalCode() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, BillingPostalCode
                    FROM Account
                    WHERE Id = :accountId
            ];

            return highSchool.BillingPostalCode;

        }

        return null;
    }

    public String getHighSchoolName() {
        if (accountId != null) {
            Account highSchool = [
                    SELECT Id, Name
                    FROM Account
                    WHERE Id = :accountId
            ];

            return highSchool.Name;

        }

        return null;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ COMMUNITY PORTAL LOGOS ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public String getFBLogoURL() {
        return EmailTemplateComponentController.getImgURLByApiName(FB_LOGO_API);
    }

    public String getINLogoURL() {
        return EmailTemplateComponentController.getImgURLByApiName(IN_LOGO_API);
    }

    public String getYTLogoURL() {
        return EmailTemplateComponentController.getImgURLByApiName(YT_LOGO_API);
    }

    public String getTWLogoURL() {
        return EmailTemplateComponentController.getImgURLByApiName(TW_LOGO_API);
    }

    public String getPrzewodnikLogoURL() {
        return EmailTemplateComponentController.getImgURLByApiName(PRZEWODNIK_LOGO_API);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ UNIVERSITY ACCOUNT DATA ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public Account getUniversityAccountData() {
        List<Account> accList = [
                SELECT Id, BillingStreet, BillingCity, BillingPostalCode, NIP__c, Parent.NIP__c
                FROM Account
                WHERE Name = :universityNameParam
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
        ];

        if (!accList.isEmpty()) {
            return accList[0];
        }

        return null;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HELPER METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String getImgURLByApiName(String apiName) {
        String sfBaseUrl = Url.getSalesforceBaseUrl().toExternalForm();
        String imgServerPart = '/servlet/servlet.ImageServer?id=';
        String orgIdPart = '&oid=' + UserInfo.getOrganizationId();

        List<Document> doc = [
                SELECT Id
                FROM Document
                WHERE DeveloperName = :apiName
        ];

        if (!doc.isEmpty()) {
            return sfBaseUrl + imgServerPart + doc[0].Id + orgIdPart;
        }

        return null;
    }

}