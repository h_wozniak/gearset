public without sharing class DocumentListEmailController {

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- INIT ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public String enrollmentId { get; set; }
    public String discountsAvailableUntil { get; set; }

    public static Map<String, Map<String, String>> labelMap = new Map<String, Map<String, String>>();
    public static Map<String, Map<Integer, String>> calendarMonthMap = new Map<String, Map<Integer, String>>();


    public DocumentListEmailController() {}



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- METHODS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public void prepareMonthMap() {
        calendarMonthMap.put(CommonUtility.LANGUAGE_CODE_PL, new Map<Integer, String> {
            1 => 'stycznia',
            2 => 'lutego',
            3 => 'marca',
            4 => 'kwietnia',
            5 => 'maja',
            6 => 'czerwca',
            7 => 'lipca',
            8 => 'sierpnia',
            9 => 'września',
            10 => 'października',
            11 => 'listopada',
            12 => 'grudnia'
        });

        calendarMonthMap.put(CommonUtility.LANGUAGE_CODE_EN, new Map<Integer, String> {
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        });

        calendarMonthMap.put(CommonUtility.LANGUAGE_CODE_RU, new Map<Integer, String> {
            1 => 'январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'ноябрь',
            12 => 'Декабрь'
        });
    }

    //public static void prepareLabelMap() {
    //    labelMap.put(CommonUtility.LANGUAGE_CODE_PL, new Map<String, String> {
    //        'msg_decreaseTuition' => 'obniża Twoje czesne o',
    //        'msg_payInstead' => 'sprawia, że płacisz',
    //        'msg_onFirstYear' => 'na I roku studiów',
    //        'msg_instead' => 'zamiast',
    //        'msg_entitlesToADiscount' => 'uprawnia Cię do zniżki'
    //    });

    //    labelMap.put(CommonUtility.LANGUAGE_CODE_EN, new Map<String, String> {
    //        'msg_decreaseTuition' => 'lowers your tuition fees by',
    //        'msg_payInstead' => 'makes you pay',
    //        'msg_onFirstYear' => 'the first year of study',
    //        'msg_instead' => 'instead',
    //        'msg_entitlesToADiscount' => 'entitles you to a discount'
    //    });

    //    labelMap.put(CommonUtility.LANGUAGE_CODE_RU, new Map<String, String> {
    //        'msg_decreaseTuition' => 'уменьшает ваши платы за обучение',
    //        'msg_payInstead' => 'Это заставляет вас платить',
    //        'msg_onFirstYear' => 'первый год обучения',
    //        'msg_instead' => 'вместо',
    //        'msg_entitlesToADiscount' => 'дает право на получение скидки'
    //    });        
    //}

    public List<Document_Wrapper> getDocumentList() {
        if (enrollmentId == null) {
            return null;
        }

        List<Document_Wrapper> docWrapperList = new List<Document_Wrapper>();

        List<Enrollment__c> docList = [
            SELECT Id, Document__r.Trade_Name__c, Enrollment_from_Documents__c, For_delivery_in_Stage__c, Document_Accepted__c, Document_Delivered__c,
                   Document__r.Russian_Trade_Name__c, Document__r.English_Trade_Name__c, Language_of_Main_Enrollment__c
            FROM Enrollment__c
            WHERE Enrollment_from_Documents__c = :enrollmentId
            AND (For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_COD OR For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_AD)
            ORDER BY Current_File_Name__c DESC NULLS LAST
        ];

        for (Enrollment__c doc : docList) {
            if (!doc.Document_Delivered__c && !doc.Document_Accepted__c) {
                Document_Wrapper dw = new Document_Wrapper();
                if (doc.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_POLISH) {
                    dw.docName = doc.Document__r.Trade_Name__c;
                }
                else if (doc.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                    dw.docName = doc.Document__r.English_Trade_Name__c;
                }
                else if (doc.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN) {
                    dw.docName = doc.Document__r.Russian_Trade_Name__c;
                }
                dw.delivered = doc.Document_Accepted__c;
                docWrapperList.add(dw);
            }
        }

        return docWrapperList;
    }

    public List<Discount_Wrapper> getDiscountList() {
        if (enrollmentId == null) {
            return null;
        }

        Enrollment__c studyEnr = [
            SELECT Id, Degree__c, Price_Book_from_Enrollment__r.Entry_fee__c, Confirmation_Date__c, Unenrolled__c, Language_of_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        List<Discount_Wrapper> discountWrapperList = new List<Discount_Wrapper>();

        List<Discount__c> discountList;

        if (!studyEnr.Unenrolled__c) {
            discountList = [
                SELECT Id, Name, Trade_Name__c, Discount_Kind__c, Discount_Value__c, Applies_to__c, Discount_Type_Studies__c, English_Trade_Name__c, 
                       Russian_Trade_Name__c,
                       Discount_from_Discount_Connector__r.Discount_from_Offer_DC__r.Valid_To__c, Delivery_Documents_Date__c, Discount_from_Discount_Connector__c
                FROM Discount__c
                WHERE Enrollment__c = :enrollmentId
				AND Discount_Value__c != 0
                AND Applied__c = true
                AND Applies_to__c != :CommonUtility.DISCOUNT_APPLIESTO_QUALIFYING
            ];
        }
        else {
            discountList = [
                SELECT Id, Name, Trade_Name__c, Discount_Kind__c, Discount_Value__c, Applies_to__c, Discount_Type_Studies__c, English_Trade_Name__c, 
                       Russian_Trade_Name__c,
                       Discount_from_Discount_Connector__r.Discount_from_Offer_DC__r.Valid_To__c, Delivery_Documents_Date__c, Discount_from_Discount_Connector__c
                FROM Discount__c
                WHERE Enrollment__c = :enrollmentId
                AND Applied__c = true
                AND Discount_Type_Studies__c = :CommonUtility.DISCOUNT_TYPEST_UNENR
                AND Applies_to__c != :CommonUtility.DISCOUNT_APPLIESTO_QUALIFYING
            ];
        }


        String languageCode = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        prepareMonthMap();
        //prepareLabelMap();
        //String decreasedTuitionLabel = labelMap.get(languageCode).get('msg_decreaseTuition');
        //String payInstead = labelMap.get(languageCode).get('msg_payInstead');
        //String firstYear = labelMap.get(languageCode).get('msg_onFirstYear');
        //String instead = labelMap.get(languageCode).get('msg_instead');
        //String entitlesDiscount = labelMap.get(languageCode).get('msg_entitlesToADiscount');

        String decreasedTuitionLabel = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_decreaseTuition');
        String payInstead = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_payInstead');
        String firstYear = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_onFirstYear');
        String instead = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_instead');
        String entitlesDiscount = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_entitlesToADiscount');

        for (Discount__c disc : discountList) {
            Discount_Wrapper dw = new Discount_Wrapper();
            String discountTradeName;

            if (studyEnr.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_POLISH) {
                discountTradeName = disc.Trade_Name__c;
            }
            else if (studyEnr.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                discountTradeName = disc.English_Trade_Name__c;
            }
            else if (studyEnr.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN) {
                discountTradeName = disc.Russian_Trade_Name__c;
            }

            Boolean canAddDiscount = true;
            if (disc.Discount_Type_Studies__c != CommonUtility.DISCOUNT_TYPEST_CODE) {
                if ((disc.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT || disc.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE)
                    && disc.Applies_to__c == CommonUtility.DISCOUNT_APPLIESTO_TUITION) {                    

                    if (disc.Discount_from_Discount_Connector__c == null) {
                        discountsAvailableUntil = dateToString(disc.Delivery_Documents_Date__c, languageCode);
                    }
                    else {
                        discountsAvailableUntil = dateToString(disc.Discount_from_Discount_Connector__r.Discount_from_Offer_DC__r.Valid_To__c, languageCode);
                    }
                }
                if (disc.Applies_to__c == CommonUtility.DISCOUNT_APPLIESTO_TUITION) {
                    if (studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_II  && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_II_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_MBA) {
                        if (disc.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT) {
                            dw.msg = discountTradeName + ' ' + decreasedTuitionLabel + ' ' + Integer.valueOf(disc.Discount_Value__c) + ' zł ' + firstYear;
                        }
                        else {
                            dw.msg = discountTradeName + ' ' + decreasedTuitionLabel + ' ' + Integer.valueOf(disc.Discount_Value__c) + '% ' + firstYear;                        
                        }
                    }
                    else {
                        if (disc.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT) {
                            dw.msg = discountTradeName + ' ' + decreasedTuitionLabel + ' ' + Integer.valueOf(disc.Discount_Value__c) + ' zł';
                        }
                        else {
                            dw.msg = discountTradeName + ' ' + decreasedTuitionLabel + ' ' + Integer.valueOf(disc.Discount_Value__c) + '%';                        
                        }                        
                    }
                }
                else if (disc.Applies_to__c == CommonUtility.DISCOUNT_APPLIESTO_ENTRY) {
                    if (studyEnr.Price_Book_from_Enrollment__r.Entry_fee__c != null) {
                        if (disc.Discount_Kind__c != CommonUtility.DISCOUNT_KIND_AMOUNT) {
                            dw.msg = discountTradeName + ' ' + payInstead + ' ' + Integer.valueOf(studyEnr.Price_Book_from_Enrollment__r.Entry_fee__c * (1 - (disc.Discount_Value__c / 100))) + ' zł ' + instead + ' ' + studyEnr.Price_Book_from_Enrollment__r.Entry_fee__c + ' zł';
                        }
                        else {
                            dw.msg = discountTradeName + ' ' + payInstead + ' ' + Integer.valueOf(studyEnr.Price_Book_from_Enrollment__r.Entry_fee__c - disc.Discount_Value__c) + ' zł ' + instead + ' ' + studyEnr.Price_Book_from_Enrollment__r.Entry_fee__c + ' zł';
                        }
                    }
                    else {
                        canAddDiscount = false;
                    }
                }
            }
            else {
                if (disc.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT) {
                    dw.msg = discountTradeName + ' ' + entitlesDiscount + ' ' + Integer.valueOf(disc.Discount_Value__c) + ' zł';
                }
                else {
                    dw.msg = discountTradeName + ' ' + entitlesDiscount + ' ' + Integer.valueOf(disc.Discount_Value__c) + '%';                    
                }
            }
            if (canAddDiscount) {
                discountWrapperList.add(dw);
            }
        }

        if (discountsAvailableUntil == null || discountsAvailableUntil == '') {
            if (studyEnr.Confirmation_Date__c != null) {
                discountsAvailableUntil = dateToString(Date.newInstance(studyEnr.Confirmation_Date__c.year(), studyEnr.Confirmation_Date__c.month(), studyEnr.Confirmation_Date__c.day()).addDays(7), languageCode);
            }
            else {
                discountsAvailableUntil = dateToString(Date.today().addDays(7), languageCode);                
            }
        } 

        return discountWrapperList;
    }

    public String getlanguageOfEnrollment() {
        if (enrollmentId == null) {
            return null;
        }

        List<Enrollment__c> studyEnrList = [
            SELECT Id, Language_of_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        if (!studyEnrList.isEmpty()) {
            return studyEnrList[0].Language_of_Enrollment__c;
        }

        return null;
    }

    public String dateToString(Date validTo, String languageCode) {
        Integer month = Integer.valueOf(validTo.month());
        String monthAsString = calendarMonthMap.get(languageCode).get(month);

        return validTo.day() + ' ' + monthAsString + ' ' + validTo.year();
    }

    public Date getDocumentDeliveryDeadline() {
        if (enrollmentId == null) {
            return null;
        }

        List<Enrollment__c> studyEnrList = [
            SELECT Id, Delivery_Deadline__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        if (!studyEnrList.isEmpty()) {
            return studyEnrList[0].Delivery_Deadline__c;
        }

        return null;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- MODEL CLASSES ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class Document_Wrapper {
        public String docName { get; set; }
        public Boolean delivered { get; set; }
    }

    public class Discount_Wrapper {
        public String msg { get; set; }
    }

}