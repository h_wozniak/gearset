@isTest
private class ZTestPaymentRewrites {

	private static Map<Integer, sObject> prepareDataForTestRewrites() {
		Map<Integer, sObject> retMap = new Map<Integer, sObject>();

		retMap.put(0, ZDataTestUtility.createEnrollmentStudy(null, true));

		return retMap;
	}


	@isTest static void testRewritesFields() {
        Test.startTest();
        Map<Integer, sObject> dataMap = prepareDataForTestRewrites();

        List<Payment__c> paymnetsOnEnrollment = ZDataTestUtility.createPaymentsOnEnrollment(
            new Payment__c(Enrollment_from_Schedule_Installment__c = dataMap.get(0).Id), 
            5, true);

        String universityName = ((Enrollment__c)dataMap.get(0)).University_Name__c;


    	for (Payment__c paymentOnEnrollment : paymnetsOnEnrollment) {
    		System.assertEquals(universityName, paymentOnEnrollment.University_Name__c);
    	}

        Test.stopTest();
	}

}