/**
* @author       Wojciech Słodziak
* @description  Test class for batches
**/

@isTest
private class ZTestBatchClasses {
    


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- ScheduleAccountDataUpdateBatch.cls TEST ----------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_ScheduleAccountDataUpdateBatch() {
        String testData1 = 'testData1';
        String testData2 = 'testData2';

        Account university = ZDataTestUtility.createUniversity(new Account(
            Contact_Data_for_Trainings__c = testData1
        ), true);

        Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(
            University_from_Training__c = university.Id
        ), true);

        Offer__c schedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Training_Offer_from_Schedule__c = trainingOffer.Id
        ), true);


        Offer__c scheduleAfterInsert = [SELECT Id, University_Contact_Data__c FROM Offer__c WHERE Id = :schedule.Id];
        System.assertEquals(testData1, scheduleAfterInsert.University_Contact_Data__c);

        Test.startTest();

        university.Contact_Data_for_Trainings__c = testData2;
        update university;

        Test.stopTest();

        Offer__c scheduleAfterUpdate = [SELECT Id, University_Contact_Data__c FROM Offer__c WHERE Id = :schedule.Id];
        System.assertEquals(testData2, scheduleAfterUpdate.University_Contact_Data__c);
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- EnrollmentAccountDataUpdateBatch.cls TEST --------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_EnrollmentAccountDataUpdateBatch() {

        Account university = ZDataTestUtility.createUniversity(new Account(
            BillingCity = 'Wrocław'
        ), true);

        Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(
            University_from_Training__c = university.Id
        ), true);

        Offer__c schedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Training_Offer_from_Schedule__c = trainingOffer.Id
        ), true);


        Offer__c scheduleAfterInsert = [SELECT Id, University_City_in_Vocative__c FROM Offer__c WHERE Id = :schedule.Id];
        System.assertEquals('Wrocławiu', scheduleAfterInsert.University_City_in_Vocative__c);

        Test.startTest();

        university.BillingCity = 'Bydgoszcz';
        update university;

        Test.stopTest();

        Offer__c scheduleAfterUpdate = [SELECT Id, University_City_in_Vocative__c FROM Offer__c WHERE Id = :schedule.Id];
        System.assertEquals('Bydgoszczy', scheduleAfterUpdate.University_City_in_Vocative__c);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- ScheduleCityInVocativeUpdateBatch.cls TEST -------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_ScheduleCityInVocativeUpdateBatch() {
        String testData1 = 'testData1';
        String testData2 = 'testData2';

        Account university = ZDataTestUtility.createUniversity(new Account(
            Contact_Data_for_Trainings__c = testData1
        ), true);

        Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(
            University_from_Training__c = university.Id
        ), true);

        Offer__c schedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Training_Offer_from_Schedule__c = trainingOffer.Id
        ), true);

        Enrollment__c trainingEnr = ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = schedule.Id
        ), true);


        Enrollment__c enrAfterInsert = [SELECT Id, University_Contact_Data__c FROM Enrollment__c WHERE Id = :trainingEnr.Id];
        System.assertEquals(testData1, enrAfterInsert.University_Contact_Data__c);

        Test.startTest();

        university.Contact_Data_for_Trainings__c = testData2;
        update university;

        Test.stopTest();

        Enrollment__c enrAfterUpdate = [SELECT Id, University_Contact_Data__c FROM Enrollment__c WHERE Id = :trainingEnr.Id];
        System.assertEquals(testData2, enrAfterUpdate.University_Contact_Data__c);
    }

}