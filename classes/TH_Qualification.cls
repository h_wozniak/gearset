/**
* @description  This class is a Trigger Handler for Didactics - complex logic
**/

public without sharing class TH_Qualification extends TriggerHandler.DelegateBase {

    List<Qualification__c> didacticsToRewriteUniversityForSharing;
    Set<Id> finishedUniversitiesToUpdateGraduateOnContact;
    Set<Id> contactsIdToDetermineGraduationStatus;

    public override void prepareBefore() {
        didacticsToRewriteUniversityForSharing = new List<Qualification__c>();
    }

    public override void prepareAfter() {
        contactsIdToDetermineGraduationStatus = new Set<Id>();
        finishedUniversitiesToUpdateGraduateOnContact = new Set<Id>();
    }


    public override void beforeInsert(List<sObject> o ) {
        List<Qualification__c> newQualificationList = (List<Qualification__c>)o;
        for(Qualification__c newQualification : newQualificationList) {

            /* Wojciech SŁodziak */
            // rewrite University for sharing from Contact on record insert
            if (newQualification.RecordTypeId == CommonUtility.getRecordTypeId('Qualification__c', CommonUtility.QUALIFICATION_RT_FINISHED_UNIV)) {
                didacticsToRewriteUniversityForSharing.add(newQualification);
            }

        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        for (Id key : o.keySet()) {
            Qualification__c newQualification = (Qualification__c)o.get(key);

            /* Sebastian Łasisz */
            // update Graduation Status on Contact after Finished University with WSB University is created
            if (newQualification.RecordTypeId == CommonUtility.getRecordTypeId('Qualification__c', CommonUtility.QUALIFICATION_RT_FINISHED_UNIV)
                && newQualification.Status__c == CommonUtility.CONTACT_DIPLOMA_AQUIRED) {
                finishedUniversitiesToUpdateGraduateOnContact.add(newQualification.Id);
            }
        }
    }

    public override void afterDelete(Map<Id, sObject> old) { 
        Map<Id, Qualification__c> oldQualifications = (Map<Id, Qualification__c>)old;
        for(Id key : oldQualifications.keySet()) {
            Qualification__c oldQualification = oldQualifications.get(key);

            /* Sebastian Łasisz */
            // Determine candidates graduation status after removing Finished University
            if (oldQualification.RecordTypeId == CommonUtility.getRecordTypeId('Qualification__c', CommonUtility.QUALIFICATION_RT_FINISHED_UNIV)) {
                contactsIdToDetermineGraduationStatus.add(oldQualification.Candidate_from_Finished_University__c);
            }
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
         for (Id key : old.keySet()) {
            Qualification__c oldQualification = (Qualification__c)old.get(key);
            Qualification__c newQualification = (Qualification__c)o.get(key);

        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
         for (Id key : old.keySet()) {
            Qualification__c oldQualification = (Qualification__c)old.get(key);
            Qualification__c newQualification = (Qualification__c)o.get(key);

            /* Sebastian Łasisz */
            // update Graduation Status on Contact after Finished University with WSB University is update
            if (newQualification.RecordTypeId == CommonUtility.getRecordTypeId('Qualification__c', CommonUtility.QUALIFICATION_RT_FINISHED_UNIV)
                && newQualification.Status__c == CommonUtility.CONTACT_DIPLOMA_AQUIRED) {
                finishedUniversitiesToUpdateGraduateOnContact.add(newQualification.Id);
            }
        }
    }

    public override void finish() {
        /* Wojciech SŁodziak */
        // rewrite University for sharing from Contact on record insert
        if (didacticsToRewriteUniversityForSharing != null && !didacticsToRewriteUniversityForSharing.isEmpty()) {
            QualificationManager.rewriteUniversityForSharingFromContact(didacticsToRewriteUniversityForSharing);
        }

        /* Sebastian Łasisz */
        // update Graduation Status on Contact after Finished University with WSB University is created
        if (finishedUniversitiesToUpdateGraduateOnContact != null && !finishedUniversitiesToUpdateGraduateOnContact.isEmpty()) {
            ContactManager.updateGraduateStatusOnContact(finishedUniversitiesToUpdateGraduateOnContact);
        }

        /* Sebastian Łasisz */
        // Determine candidates graduation status after removing Finished University
        if (contactsIdToDetermineGraduationStatus != null && !contactsIdToDetermineGraduationStatus.isEmpty()) {
            ContactManager.determineGraduationStatus(contactsIdToDetermineGraduationStatus);
        }
    }

}