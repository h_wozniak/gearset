@isTest
private class ZTestCommonEmailUtilitiesController {
    
    @isTest static void test_getAcademicYearRepresentation() {
        Offer__c univOffStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Study_Start_Date__c = Date.newInstance(2016, 3, 1)
        ), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = univOffStudy.Id
        ), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        CommonEmailUtilitiesController ceuc = new CommonEmailUtilitiesController();
        ceuc.enrollmentId = studyEnr.Id;

        Test.startTest();
        String yearRepresentation = ceuc.getAcademicYearRepresentation();
        Test.stopTest();

        System.assertEquals('2015/16', yearRepresentation);
    }

    @isTest static void test_getMBAYearRange() {
        Offer__c univOffStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Study_Start_Date__c = Date.newInstance(2016, 3, 1)
        ), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = univOffStudy.Id,
            Number_of_Semesters__c = 4
        ), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        CommonEmailUtilitiesController ceuc = new CommonEmailUtilitiesController();
        ceuc.enrollmentId = studyEnr.Id;

        Test.startTest();
        String yearRange = ceuc.getMBAYearRange();
        Test.stopTest();

        System.assertEquals('2016-2018', yearRange);
    }
    
}