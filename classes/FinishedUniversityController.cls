public with sharing class FinishedUniversityController {
    
    public Qualification__c finishedUniversity { get; set; }
    public String candidateId { get; set; }
    public String idMatch { get; set; }
    public Boolean isComplete { get; set; }
    
    public FinishedUniversityController() {
        candidateId = Apexpages.currentPage().getParameters().get('candidateId');
        
        finishedUniversity = new Qualification__c();
        finishedUniversity.Candidate_from_Finished_University__c = candidateId;
        finishedUniversity.RecordTypeId = CommonUtility.getRecordTypeId('Qualification__c', CommonUtility.QUALIFICATION_RT_FINISHED_UNIV);

        isComplete = false;
        
        if (candidateId != null) {
            idMatch = candidateId;
        }
    }
    
    public void save() {
        try {
            insert finishedUniversity;
            isComplete = true;
        }
        catch (Exception e) {
            isComplete = false;
            ApexPages.addMessages(e);
        }
    }

    public void cancel() {
        isComplete = true;
    }
    
    public List<Schema.FieldSetMember> getFinishedUniversitiesFields() {
        return SObjectType.Qualification__c.FieldSets.Finished_University.getFields();
    }
}