/**
*   @author         Grzegorz Długosz, editor: Sebastian Łasisz
*   @description    This class is used to send new campaign members (contacts) to FCC
**/

global without sharing class IntegrationFCCContactSender {

    @future(callout=true)
    public static void sendNewFCCRecordsAsync(Set<Id> campId) {
        IntegrationFCCContactSender.sendNewFCCRecordsFinal(campId);
    }

    public static Boolean sendNewFCCRecordsSync(Set<Id> campId) {
        return IntegrationFCCContactSender.sendNewFCCRecordsFinal(campId);
    }

    // final method for sending callout
    private static Boolean sendNewFCCRecordsFinal(Set<Id> campId) {
        HttpRequest httpReq = new HttpRequest();
        
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createFCCAddRecordsEndPoint = esb.FCC_Add_Records_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createFCCAddRecordsEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationFCCContactSenderHelper.prepareJSON(campId);
        httpReq.setBody(jsonToSend);

        
        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = (httpRes.getStatusCode() == 200);
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationFCCContactSenderHelper.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + campId);
        }

        return success;
    }
}