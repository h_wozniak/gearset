@isTest
private class ZTestDocumentGeneratorManager {

	private static Map<Integer, sObject> prepareDataForTest(Date studyStartDate, Boolean createOldTemplate, String numberAndSeries, Boolean foreigner) {
		Map<Integer, sObject> retMap = new Map<Integer, sObject>();
		ZDataTestUtility.createDocument(new Catalog__c(
				Name = RecordVals.CATALOG_DOCUMENT_ACCEPTANCE,
				Active__c = true
		), true);

		retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = studyStartDate), true));
		retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));

		Test.startTest();
		if (numberAndSeries == null) {
			retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
		}
		else {
			Contact candidate;
			if (foreigner) {
				candidate = ZDataTestUtility.createCandidateContact(new Contact(Number_and_Series__c = numberAndSeries, Pesel__c = '26072618152', Nationality__c = 'German'), true);
			}
			else {
				candidate = ZDataTestUtility.createCandidateContact(new Contact(Number_and_Series__c = numberAndSeries, Pesel__c = '26072618152', Nationality__c = 'Polish'), true);
			}
			retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = candidate.Id), true));
		}
		Test.stopTest();

		if (!createOldTemplate) {
			retMap.put(3, [
					SELECT Id
					FROM Enrollment__c
					WHERE Enrollment_from_Documents__c = :retMap.get(2).Id
					AND Document__c = :CatalogManager.getDocumentAcceptanceId()
			]);
		}
		else {
			retMap.put(3, [
					SELECT Id
					FROM Enrollment__c
					WHERE Enrollment_from_Documents__c = :retMap.get(2).Id
					AND Document__c = :CatalogManager.getDocumentAgreementId()
			]);
		}

		enxoodocgen__Document_Template__c  template = new enxoodocgen__Document_Template__c ();
		template.Name = '[PL][Test][' + CommonUtility.OFFER_DEGREE_MBA + '][' + CommonUtility.WSB_ENTITY_WRO + ']';
		insert template;
		retMap.put(4, template);

		enxoodocgen__Document_Template__c  template_en = new enxoodocgen__Document_Template__c ();
		template_en.Name = '[EN][Test][' + CommonUtility.OFFER_DEGREE_MBA + '][' + CommonUtility.WSB_ENTITY_WRO + ']';
		insert template_en;
		retMap.put(5, template_en);

		if (createOldTemplate) {
			enxoodocgen__Document_Template__c  template_old = new enxoodocgen__Document_Template__c ();
			template_old.Name = '[2014/2015][EN][Test][' + CommonUtility.OFFER_DEGREE_MBA + '][' + CommonUtility.WSB_ENTITY_WRO + ']';
			insert template_old;
			retMap.put(6, template_old);
		}

		return retMap;
	}

	@isTest static void test_generateDocument() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocument(CommonUtility.ENROLLMENT_LANGUAGE_POLISH, 'Test', CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocument_en() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocument(CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH, 'Test', CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocument_incorrectTemplateName() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocument(null, 'Test', CommonUtility.OFFER_DEGREE_U, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocumentSync() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocumentSync(CommonUtility.ENROLLMENT_LANGUAGE_POLISH, 'Test', CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocumentSync_en() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocumentSync(CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH, 'Test', CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocumentSync_incorrectTemplateName() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.generateDocumentSync(CommonUtility.ENROLLMENT_LANGUAGE_POLISH, 'Test', CommonUtility.OFFER_DEGREE_U, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, CommonUtility.GENERATION_FAILED);
	}

	@isTest static void test_updateStudyEnrGenerationStatus() {
		Map<Integer, sObject> dataMap = prepareDataForTest(System.today(), false, null, true);

		DocumentGeneratorManager.updateStudyEnrGenerationStatus(dataMap.get(3).Id);

		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertEquals(docEnr.Current_File__c, CommonUtility.GENERATION_FAILED);
	}

	@isTest static void test_generateDocument_oldOffer() {
		Map<Integer, sObject> dataMap = prepareDataForTest(Date.newInstance(2010, 1, 1), true, null, true);

		Boolean errorOccured = false;
		try {
			DocumentGeneratorManager.generateDocument(CommonUtility.ENROLLMENT_LANGUAGE_POLISH, RecordVals.CATALOG_DOCUMENT_AGREEMENT, CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);
		}
		catch (Exception e) {
			errorOccured = true;
		}

		System.assert(!errorOccured);
		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertNotEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_generateDocument_oldOffer2() {
		Map<Integer, sObject> dataMap = prepareDataForTest(Date.newInstance(2015, 1, 1), true, null, true);

		Boolean errorOccured = false;
		try {
			DocumentGeneratorManager.generateDocument(CommonUtility.ENROLLMENT_LANGUAGE_POLISH, RecordVals.CATALOG_DOCUMENT_AGREEMENT, CommonUtility.OFFER_DEGREE_MBA, CommonUtility.WSB_ENTITY_WRO, 100, dataMap.get(3).Id, true, false);
		}
		catch (Exception e) {
			errorOccured = true;
		}

		System.assert(!errorOccured);
		Enrollment__c docEnr = [SELECT Id, Current_File__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

		System.assertNotEquals(docEnr.Current_File__c, null);
	}

	@isTest static void test_retrievePasswordForDocument_null() {
		Map<Integer, sObject> dataMap = prepareDataForTest(Date.newInstance(2015, 1, 1), true, null, true);

		String result = DocumentGeneratorManager.retrievePasswordForDocument(dataMap.get(3).Id);
		System.assertEquals(result, null);
	}

	@isTest static void test_retrievePasswordForDocument_foreigner() {
		Map<Integer, sObject> dataMap = prepareDataForTest(Date.newInstance(2015, 1, 1), true, 'AKB915253', true);

		Contact candidate = [
				SELECT Id, Foreigner__c
				FROM Contact
				WHERE Pesel__c = '26072618152'
		];

		System.assert(candidate.Foreigner__c);

		String result = DocumentGeneratorManager.retrievePasswordForDocument(dataMap.get(3).Id);
		System.assertEquals(result, '915253');
	}

	@isTest static void test_retrievePasswordForDocument_pl() {
		Map<Integer, sObject> dataMap = prepareDataForTest(Date.newInstance(2015, 1, 1), true, 'AKB915253', false);

		Contact candidate = [
				SELECT Id, Foreigner__c
				FROM Contact
				WHERE Pesel__c = '26072618152'
		];

		System.assert(!candidate.Foreigner__c);

		String result = DocumentGeneratorManager.retrievePasswordForDocument(dataMap.get(3).Id);
		System.assertEquals(result, '915253');
	}
}