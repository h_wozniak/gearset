/**
* @author       Wojciech Słodziak
* @description  Class for storing methods related to Payment__c
**/

public without sharing class PaymentManager {
    
    /* Wojciech Słodziak */
    // method for checking if Entry Fee Payment Confirmation Document is required after Entry Inst insert, delete or Value_after_Discount__c change 
    public static void updatePaymentConfDocFDIBasedOnEntryInst(Set<Id> studyEnrIds) {
        List<Enrollment__c> studyEnrList = [
            SELECT Id, 
                (SELECT Id, For_delivery_in_Stage__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentEntryPaymentConfId()),
                (SELECT Id, Value_after_Discount__c
                FROM ScheduleInstallments__r
                WHERE Type__c = :CommonUtility.PAYMENT_TYPE_ENTRY_FEE)
            FROM Enrollment__c
            WHERE Id IN :studyEnrIds
        ];

        List<Enrollment__c> docListToUpdate = new List<Enrollment__c>();

        for (Enrollment__c studyEnr : studyEnrList) {
            Enrollment__c paymentConfDoc;
            if (!studyEnr.Documents__r.isEmpty()) {
                paymentConfDoc = studyEnr.Documents__r[0];
            }

            if (paymentConfDoc != null) {
                Payment__c entryInst;
                if (!studyEnr.ScheduleInstallments__r.isEmpty()) {
                    entryInst = studyEnr.ScheduleInstallments__r[0];
                }

                if (entryInst != null && entryInst.Value_after_Discount__c != 0) {
                    paymentConfDoc.For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD;
                } else {
                    paymentConfDoc.For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD_CON;
                }

                docListToUpdate.add(paymentConfDoc);
            }
        }

        try {
            CommonUtility.skipDocumentValidations = true;
            update docListToUpdate;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // helper method for creating map with payment schedule
    public static Map<Decimal, List<String>> generatePaymentInstallmentsMap(Id enrollmentId) {
        Map<Decimal, List<Payment__c>> paymentMap = new Map<Decimal, List<Payment__c>>();

        Enrollment__c studyEnr = [
            SELECT Id, Number_of_Semesters__c, Starting_Semester__c, Degree__c, Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c
            FROM Enrollment__c
            WHERE Id =: enrollmentId
        ];

        Boolean ifOdd = math.mod(Integer.valueOf(studyEnr.Starting_Semester__c), 2) != 0;
        Boolean first = true;

        Integer realNumberOfSemesters = studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c != null ? 
                                        Integer.valueOf(studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c) :
                                        Integer.valueOf(studyEnr.Number_of_Semesters__c);

        List<Payment__c> payments = [
            SELECT Installment_Semester__c, FORMAT(PriceBook_Value__c), Discount_Value__c, Value_after_Discount__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :enrollmentId
            AND Type__c = :CommonUtility.DISCOUNT_APPLIESTO_TUITION
            AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
        ];

        for (Payment__c payment : payments) {
            if (payment.Installment_Semester__c != null) {
                if (!paymentMap.containsKey(payment.Installment_Semester__c)) {
                    paymentMap.put(payment.Installment_Semester__c, new List<Payment__c>());
                }
                paymentMap.get(payment.Installment_Semester__c).add(payment);
            }
        }

        Map<Decimal, List<String>> paymentInstallments = new Map<Decimal, List<String>>();

        Integer semesterCounter = Integer.valueOf(studyEnr.Starting_Semester__c);
        // Generate Rows Filled with 0's when Starting Semester doesnt start at 1
        /* Błąd #1938 - Incosistence in TEBA decision. DO NOT DELETE UNLESS SURE */

        //Integer semesterCounter = 1;
        //while(semesterCounter <= Integer.valueOf(studyEnr.Starting_Semester__c)) {
        //    if ((math.mod(semesterCounter, 2) != 0)) {
        //        paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
        //    }
        //    else {
        //        paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
        //    }
        //    semesterCounter++;
        //}

        //semesterCounter--;
        for (Decimal paymentInstallment : paymentMap.keySet()) {
            // Generate Rows Filled with 0's when Semester doesnt have Payments
            while(semesterCounter < paymentInstallment) {
                if ((math.mod(semesterCounter, 2) != 0) || (semesterCounter == Integer.valueOf(studyEnr.Starting_Semester__c))) {
                    paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
                }
                else {
                    paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
                }
                semesterCounter++;
            }

            List<Payment__c> paymentsList = paymentMap.get(paymentInstallment);

            Decimal sumPayment = 0;
            Decimal sumDiscount = 0;
            Decimal onePayment = 0;
            Decimal installmentsCounter = 0;
            Integer currentSemester = 0;

            for (Payment__c payment : paymentsList) {
                installmentsCounter++;
                sumDiscount += payment.Discount_Value__c;
                sumPayment += payment.PriceBook_Value__c;
                onePayment  = payment.PriceBook_Value__c - payment.Discount_Value__c;
                currentSemester++;
            }
            sumPayment = sumPayment - sumDiscount;

            if ((math.mod(semesterCounter, 2) != 0) || (paymentInstallments.get(paymentInstallment-1) == null) 
                || (first && ifOdd)) {
                paymentInstallments.put(paymentInstallment, new List<String> { String.valueOf(sumDiscount.format()) + ' zł', String.valueOf(sumPayment.format()) + ' zł', String.valueOf(onePayment.format()) + ' zł', String.valueOf(installmentsCounter), String.valueOf(currentSemester) });
            }
            else if (!(first && ifOdd)) {
                paymentInstallments.put(paymentInstallment, new List<String> { String.valueOf(sumDiscount.format()) + ' zł', String.valueOf(sumPayment.format()) + ' zł', String.valueOf(onePayment.format()) + ' zł', String.valueOf(currentSemester) });
                paymentInstallments.get(paymentInstallment-1)[3] = String.valueOf((Decimal.valueOf(paymentInstallments.get(paymentInstallment-1)[3]) + installmentsCounter));
            }
            if (first && semesterCounter > 1) {
                first = false;
            }
            first = false;
            semesterCounter++;
        }

        // Generate Rows Filled with 0's when Number of Semesters is lower than semester count for current degree
        while (semesterCounter <= realNumberOfSemesters) {
            if ((math.mod(semesterCounter, 2) != 0)) {
                paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
            }
            else {
                paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
            }
            semesterCounter++;
        }

        return paymentInstallments;
    }

    public static Map<Decimal, List<String>> generatePaymentsMapWithoutFormatting(Id enrollmentId) {
        Map<Decimal, List<Payment__c>> paymentMap = new Map<Decimal, List<Payment__c>>();

        Enrollment__c studyEnr = [
            SELECT Id, Number_of_Semesters__c, Starting_Semester__c, Degree__c, Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        Integer realNumberOfSemesters = studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c != null ? 
                                        Integer.valueOf(studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c) :
                                        Integer.valueOf(studyEnr.Number_of_Semesters__c);

        List<Payment__c> payments = [
            SELECT Installment_Semester__c, FORMAT(PriceBook_Value__c), Discount_Value__c, Value_after_Discount__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :enrollmentId
            AND Type__c = :CommonUtility.DISCOUNT_APPLIESTO_TUITION
            AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
        ];

        for (Payment__c payment : payments) {
            if (payment.Installment_Semester__c != null) {
                if (!paymentMap.containsKey(payment.Installment_Semester__c)) {
                    paymentMap.put(payment.Installment_Semester__c, new List<Payment__c>());
                }
                paymentMap.get(payment.Installment_Semester__c).add(payment);
            }
        }

        Map<Decimal, List<String>> paymentInstallments = new Map<Decimal, List<String>>();
        
        Integer semesterCounter = Integer.valueOf(studyEnr.Starting_Semester__c);

        /* Błąd #1938 - Incosistence in TEBA decision. DO NOT DELETE UNLESS SURE */
        //Integer semesterCounter = 1;
        //while(semesterCounter <= Integer.valueOf(studyEnr.Starting_Semester__c)) {
        //    if ((math.mod(semesterCounter, 2) != 0)) {
        //        paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
        //    }
        //    else {
        //        paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
        //    }
        //    semesterCounter++;
        //}

        //semesterCounter--;
        for (Decimal paymentInstallment : paymentMap.keySet()) {
            while (semesterCounter < paymentInstallment) {
                if ((math.mod(semesterCounter, 2) != 0) || (semesterCounter == Integer.valueOf(studyEnr.Starting_Semester__c))) {
                    paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
                }
                else {
                    paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
                }
                semesterCounter++;
            }

            List<Payment__c> paymentsList = paymentMap.get(paymentInstallment);

            Decimal sumPayment = 0;
            Decimal sumDiscount = 0;
            Decimal onePayment = 0;
            Decimal installmentsCounter = 0;
            Integer currentSemester = 0;

            for (Payment__c payment : paymentsList) {
                installmentsCounter++;
                sumDiscount += payment.Discount_Value__c;
                sumPayment += payment.PriceBook_Value__c;
                onePayment  = payment.PriceBook_Value__c - payment.Discount_Value__c;
                currentSemester++;
            }
            sumPayment = sumPayment - sumDiscount;
            if ((math.mod(semesterCounter, 2) != 0) || (paymentInstallments.get(paymentInstallment-1) == null)) {
                paymentInstallments.put(paymentInstallment, new List<String> { String.valueOf(sumDiscount) + ' zł', String.valueOf(sumPayment) + ' zł', String.valueOf(onePayment) + ' zł', String.valueOf(installmentsCounter), String.valueOf(currentSemester) });
            }
            else {
                paymentInstallments.put(paymentInstallment, new List<String> { String.valueOf(sumDiscount) + ' zł', String.valueOf(sumPayment) + ' zł', String.valueOf(onePayment) + ' zł', String.valueOf(currentSemester) });
                paymentInstallments.get(paymentInstallment-1)[3] = String.valueOf((Decimal.valueOf(paymentInstallments.get(paymentInstallment-1)[3]) + installmentsCounter));
            }
            semesterCounter++;
        }

        // Generate Rows Filled with 0's when Number of Semesters is lower than semester count for current degree
        while (semesterCounter <= realNumberOfSemesters) {
            if ((math.mod(semesterCounter, 2) != 0)) {
                paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0', '0' });
            }
            else {
                paymentInstallments.put(semesterCounter, new List<String> { '0 zł', '0 zł', '0 zł', '0' });
            }
            semesterCounter++;
        }

        return paymentInstallments;
    }

    /* Sebastian Łasisz */
    // helper method for creating map with payment schedule for SP degree
    public static Map<Decimal, List<String>> generatePaymentInstallmentsMapSP(Id enrollmentId) {
        Enrollment__c studyEnr = [
            SELECT Id, Tuition_System__c, Installments_per_year__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        Map<Decimal, List<String>> paymentInstallmentsMap = generatePaymentsMapWithoutFormatting(enrollmentId);
        Map<Decimal, List<String>> newPaymentInstallmentsMap = new Map<Decimal, List<String>>();

        for (Decimal paymentInstallment : paymentInstallmentsMap.keySet()) {
            Decimal academicYear =  math.mod(Integer.valueOf(paymentInstallment), 2) == 0 ? (paymentInstallment / 2) : (paymentInstallment + 1) / 2;
            if (newPaymentInstallmentsMap.get(academicYear) != null) {
                List<String> paymentsInstallmentsValues = newPaymentInstallmentsMap.get(academicYear);
                List<String> paymentInstallmentsValues = paymentInstallmentsMap.get(paymentInstallment);

                paymentInstallmentsValues[0] = 
                    String.valueOf(
                        (Integer.valueOf(removeSpaceFromString(
                            paymentInstallmentsMap.get(paymentInstallment)[0].left(
                                paymentInstallmentsMap.get(paymentInstallment)[0].length() - 3
                            )).replaceAll(' ', '')
                        ) + 
                        Integer.valueOf(removeSpaceFromString(
                            newPaymentInstallmentsMap.get(academicYear)[0].left(
                                newPaymentInstallmentsMap.get(academicYear)[0].length() - 3
                            ).replaceAll(' ', '')))).format()
                        ) + ' zł';

                paymentInstallmentsValues[1] = 
                    String.valueOf(
                        (Integer.valueOf(removeSpaceFromString(
                            paymentInstallmentsMap.get(paymentInstallment)[1].left(paymentInstallmentsMap.get(paymentInstallment)[1].length() - 3)).replaceAll(' ', ''))
                  + Integer.valueOf(removeSpaceFromString(
                        newPaymentInstallmentsMap.get(academicYear)[1].left(newPaymentInstallmentsMap.get(academicYear)[1].length() - 3))
                        )).format()) + ' zł';

                if (studyEnr.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME || studyEnr.Installments_per_year__c == '1') {
                    paymentInstallmentsValues[2] = (
                        String.valueOf(((Integer.valueOf(paymentInstallmentsMap.get(paymentInstallment)[2].left(paymentInstallmentsMap.get(paymentInstallment)[2].length() - 3).replaceAll(' ', '')) 
                      + Integer.valueOf(removeSpaceFromString(newPaymentInstallmentsMap.get(academicYear)[2].left(newPaymentInstallmentsMap.get(academicYear)[2].length() - 3)).replaceAll(' ', '')))).format()) + ' zł');
                }
                else {
                    paymentInstallmentsValues[2] = (
                        String.valueOf(((Integer.valueOf(paymentInstallmentsMap.get(paymentInstallment)[2].left(paymentInstallmentsMap.get(paymentInstallment)[2].length() - 3).replaceAll(' ', '')) 
                      + Integer.valueOf(removeSpaceFromString(newPaymentInstallmentsMap.get(academicYear)[2].left(newPaymentInstallmentsMap.get(academicYear)[2].length() - 3)).replaceAll(' ', ''))) / 2).format()) + ' zł');
                }

                if (paymentInstallmentsMap.get(paymentInstallment).size() > 3) {
                    paymentInstallmentsValues[3] = String.valueOf(newPaymentInstallmentsMap.get(academicYear)[3]);
                }
                else {
                    paymentInstallmentsValues.add(newPaymentInstallmentsMap.get(academicYear)[3]);
                }

                newPaymentInstallmentsMap.put(academicYear, paymentInstallmentsValues);
            }
            else {
                List<String> paymentInstallmentsValues = paymentInstallmentsMap.get(paymentInstallment);
                for (Integer i = 0; i < paymentInstallmentsValues.size(); i ++) {

                    String newPayment = paymentInstallmentsValues.get(i).replace('.00', '');
                    Boolean formatValue = false;
                    if (newPayment.length() > 3) {
                        newPayment = newPayment.left(newPayment.length() - 3);
                        formatValue = true;
                    }

                    if (formatValue) {
                        paymentInstallmentsValues[i] = String.valueOf(Integer.valueOf(newPayment).format()) + ' zł';
                    }
                    else {
                        paymentInstallmentsValues[i] = newPayment;
                    }
                }
                newPaymentInstallmentsMap.put(academicYear, paymentInstallmentsValues);
            }

        }

        return newPaymentInstallmentsMap;
    }

    /* Sebastian Łasisz */
    // helper method for creating map with payment schedule for unenrolled
    public static List<String> generatePaymentInstallmentsMapUnenrolled(Id enrollmentId) {
        Map<Decimal, List<Payment__c>> paymentMap = new Map<Decimal, List<Payment__c>>(); 

        List<Payment__c> payments = [
            SELECT Installment_Semester__c, FORMAT(PriceBook_Value__c), Discount_Value__c, Value_after_Discount__c, Unenrolled_Installment__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :enrollmentId
            AND Type__c = :CommonUtility.DISCOUNT_APPLIESTO_TUITION
        ]; 

        for (Payment__c payment : payments) {
            if (payment.Installment_Semester__c != null) {
                if (!paymentMap.containsKey(payment.Installment_Semester__c)) {
                    paymentMap.put(payment.Installment_Semester__c, new List<Payment__c>());
                }
                paymentMap.get(payment.Installment_Semester__c).add(payment);
            }
        } 

        Map<Decimal, List<String>> paymentInstallments = new Map<Decimal, List<String>>();

        Decimal firstPaymentWithoutDiscount = 0;
        Decimal discountsOnPayments = 0;
        Decimal discountsOnFirstAndSecondRate = 0;
        Decimal firstPaymentAfterDiscount = 0; 

        for (Decimal paymentInstallment : paymentMap.keySet()) {
            List<Payment__c> paymentsList = paymentMap.get(paymentInstallment);
            Integer paymentInstallmentCounter = 1; 

            for (Payment__c payment : paymentsList) {
                if (paymentInstallmentCounter == 1 && firstPaymentWithoutDiscount == 0) {
                    firstPaymentWithoutDiscount = payment.PriceBook_Value__c;
                    firstPaymentAfterDiscount = payment.PriceBook_Value__c - payment.Discount_Value__c;
                }

                if (paymentInstallmentCounter == 1 || paymentInstallmentCounter == 2) {
                    discountsOnFirstAndSecondRate += payment.Discount_Value__c;
                }

                discountsOnPayments += payment.Discount_Value__c;
                paymentInstallmentCounter++;
            }
        }

        List<Discount__c> discountConnList = [
            SELECT Id, Enrollment__c, Discount_Value__c, Discount_Kind__c, Applies_to__c, Applied_through__c, Is_Unenrolled_Discount__c,
                   Discount_Type_Studies__c
            FROM Discount__c
            WHERE Enrollment__c = :enrollmentId 
            AND Applied__c = true
            AND Discount_Value__c != null 
            AND Discount_Kind__c != :CommonUtility.DISCOUNT_KIND_PERCENTAGE 
            AND Applies_to__c = :CommonUtility.DISCOUNT_APPLIESTO_TUITION
        ];

        Decimal unenrolledDiscountsValue = 0;
        Decimal otherDiscountsValue = 0;
        for (Payment__c payment : payments) {
            if (payment.Unenrolled_Installment__c) {
                unenrolledDiscountsValue += payment.Discount_Value__c;
            }

            otherDiscountsValue += payment.Discount_Value__c;
        }

//        for (Discount__c discount : discountConnList) {
//            if (discount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_UNENR) {
//                unenrolledDiscountsValue += discount.Discount_Value__c;
//            }
//            otherDiscountsValue += discount.Discount_Value__c;
//        }

        return new List<String> {
            String.valueOf(firstPaymentWithoutDiscount.format()),
            String.valueOf(otherDiscountsValue.format()),
            String.valueOf(unenrolledDiscountsValue.format()),
            String.valueOf(firstPaymentAfterDiscount.format())
        };
    }

    public static String removeSpaceFromString(String numberWithSpace) {
        numberWithSpace  = numberWithSpace.replace('.00', '');        
        String[] splitString = numberWithSpace.split('');
        String result = '';
        for (String numericValue : splitString) {
            if (numericValue.isNumeric()) {
                result += numericValue;
            }
        }

        return result;
    }

    /* Wojciech Słodziak */
    // method for adding or removing Student Card Charge to Payment Schedule
    public static void addOrRemoveStudentCardPayment(Set<Id> studyEnrIds) {
        AdditionalCharges__c studentCardChargeCS = AdditionalCharges__c.getInstance(RecordVals.CS_AC_NAME_STUDENTCARD);

        if (studentCardChargeCS != null) {
            List<Payment__c> chargesToInsert = new List<Payment__c>();
            List<Payment__c> chargesToDelete = new List<Payment__c>();

            List<Enrollment__c> studyEnrListWithSCC = [
                SELECT Id, Do_not_set_up_Student_Card__c, OwnerId,
                    (SELECT Id
                    FROM ScheduleInstallments__r
                    WHERE Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE AND Description__c = :studentCardChargeCS.Name)
                FROM Enrollment__c
                WHERE Id IN :studyEnrIds
            ];

            Map<Id, Enrollment__c> studyEnrListWithEntryInst = new Map<Id, Enrollment__c>([
                SELECT Id,
                    (SELECT Payment_Deadline__c, Installment_Year_Calendar__c, OwnerId, Installment_Semester__c
                    FROM ScheduleInstallments__r
                    WHERE Type__c = :CommonUtility.PAYMENT_TYPE_TUITION_FEE
                    ORDER BY Payment_Deadline__c ASC
                    LIMIT 1)
                FROM Enrollment__c
                WHERE Id IN :studyEnrIds
            ]);

            for (Enrollment__c studyEnr : studyEnrListWithSCC) {
                if (!studyEnr.Do_not_set_up_Student_Card__c && studyEnr.ScheduleInstallments__r.isEmpty()) {
                    // add
                    Payment__c studentCardCharge = new Payment__c();
                    studentCardCharge.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
                    studentCardCharge.OwnerId = studyEnr.OwnerId;
                    studentCardCharge.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
                    studentCardCharge.Description__c = studentCardChargeCS.Name;
                    studentCardCharge.PriceBook_Value__c = studentCardChargeCS.Price__c;
                    studentCardCharge.Type__c = CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE;
                    
                    Enrollment__c enrWithEntryInst = studyEnrListWithEntryInst.get(studyEnr.Id);

                    if (!enrWithEntryInst.ScheduleInstallments__r.isEmpty()) {
                        studentCardCharge.Payment_Deadline__c = enrWithEntryInst.ScheduleInstallments__r[0].Payment_Deadline__c;
                        studentCardCharge.Installment_Year_Calendar__c = enrWithEntryInst.ScheduleInstallments__r[0].Installment_Year_Calendar__c;
                        studentCardCharge.Installment_Semester__c = enrWithEntryInst.ScheduleInstallments__r[0].Installment_Semester__c;
                        studentCardCharge.OwnerId = enrWithEntryInst.ScheduleInstallments__r[0].OwnerId;
                    }

                    chargesToInsert.add(studentCardCharge);
                }
                if (studyEnr.Do_not_set_up_Student_Card__c && !studyEnr.ScheduleInstallments__r.isEmpty()) {
                    // delete
                    chargesToDelete.add(studyEnr.ScheduleInstallments__r[0]);
                }
            }

            try {
                CommonUtility.skipSecondVerificationOfStudentCard = true;
                if (!chargesToInsert.isEmpty()) {
                    insert chargesToInsert;
                }
                if (!chargesToDelete.isEmpty()) {
                    delete chargesToDelete;
                }
                CommonUtility.skipSecondVerificationOfStudentCard = false;
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        } else {
            ErrorLogger.msg(CommonUtility.CS_UNDEFINED_ERROR + ' ' + RecordVals.CS_AC_NAME_STUDENTCARD);
        }
    }

    /* Wojciech Słodziak */
    // method for checking wether Do_not_set_up_Student_Card__c should be checked or
    public static void verifyDoNotSetUpStudentCardCbx(Set<Id> studyEnrIds) {
        List<Enrollment__c> studyEnrList = [
            SELECT Id, Do_not_set_up_Student_Card__c,
                (SELECT Id
                FROM ScheduleInstallments__r
                WHERE Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE AND Description__c = :RecordVals.CS_AC_NAME_STUDENTCARD)
            FROM Enrollment__c
            WHERE Id IN :studyEnrIds
        ];

        for (Enrollment__c studyEnr : studyEnrList) {
            if (studyEnr.ScheduleInstallments__r.isEmpty() && !studyEnr.Do_not_set_up_Student_Card__c) {
                // check
                studyEnr.Do_not_set_up_Student_Card__c = true;
            }
            if (!studyEnr.ScheduleInstallments__r.isEmpty() && studyEnr.Do_not_set_up_Student_Card__c) {
                // uncheck
                studyEnr.Do_not_set_up_Student_Card__c = false;
            }
        }

        try {
            CommonUtility.skipSecondVerificationOfStudentCard = true;
            update studyEnrList;
            CommonUtility.skipSecondVerificationOfStudentCard = false;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // method for updating deadline for payment schedule
    public static void updatePaymentsWithExpiredDeadline(Set<Id> studyEnrIds) {
        List<Enrollment__c> studyEnrList = [
            SELECT Id,
                (SELECT Id, Payment_Deadline__c
                FROM ScheduleInstallments__r)
            FROM Enrollment__c
            WHERE Id IN :studyEnrIds
        ];

        Date acceptablePaymentDeadline = Date.today().addDays(14);
        List<Payment__c> paymentsToUpdate = new List<Payment__c>();
        for (Enrollment__c studyEnr : studyEnrList) {
            for (Payment__c payment : studyEnr.ScheduleInstallments__r) {
                if (payment.Payment_Deadline__c == null || payment.Payment_Deadline__c < acceptablePaymentDeadline) {
                    payment.Payment_Deadline__c = acceptablePaymentDeadline;
                    paymentsToUpdate.add(payment);
                }
            }
        }

        update paymentsToUpdate;
    }

    /* Sebastian Łasisz */
    // method for checking if payment can be edited
    public static void checkWhetherCanEditPayment(List<Payment__c> paymentsOnEnrollment) {
        Set<Id> studyEnrIds = new Set<Id>();
        for (Payment__c payment : paymentsOnEnrollment) {
            studyEnrIds.add(payment.Enrollment_from_Schedule_Installment__c);
        }

        Map<Id, Enrollment__c> studyEnrMap = new Map<Id, Enrollment__c>([
            SELECT Id, Status__c
            FROM Enrollment__c
            WHERE Id IN :studyEnrIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        ]);

        for (Payment__c payment : paymentsOnEnrollment) {
            Enrollment__c studyEnrFromPayment = studyEnrMap.get(payment.Enrollment_from_Schedule_Installment__c);

            if (!CommonUtility.checkIfCanEditUneditableEnrollment()
                && (studyEnrFromPayment.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                || studyEnrFromPayment.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                || studyEnrFromPayment.Status__c == CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS)) {
                payment.addError(Label.msg_errorCantEditPayment);
            }
        }
    }

}