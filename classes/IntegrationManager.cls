/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store globally used variables for Integration
**/

public without sharing class IntegrationManager {
    


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- FINAL VARIABLES ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public static final String HTTP_METHOD_POST = 'POST';
    public static final String HTTP_METHOD_PUT = 'PUT';
    public static final String HTTP_METHOD_GET = 'GET';
    public static final String HEADER_CONTENT_TYPE = 'Content-Type';
    public static final String CONTENT_TYPE_JSON = 'application/json;charset="UTF-8"';



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- MODEL CLASSES ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */


    // used in communication CRM -> ESB for reading ESB responses, only sent if response is different than 2xx
    public class ESBResponse_JSON {
        public Integer error_code;
        public String message;
    }

    // used in updating didactics 
    public class Didactics_JSON {
        public String didactics_status;
        public String portal_login;
        public String bank_acc_nbr;
        public String student_nbr;
        public String product_name;
        public Integer current_semester;
        public String current_semester_chg_date;
        public String trade_name;
        public String degree;
        public String kind;
        public String mode;
        public Integer number_of_semesters;
        public FinishedUniversity_JSON finished_university;
    }

    public class FinishedUniversity_JSON {
        public String obtained_title;
        public String diploma_number;
        public String country_of_diploma_issue;
        public String diploma_issue_date;
        public String defense_date;
        public String status;
    }

    // used in communication ESB -> CRM for reading ACKs and Experia forwarded responses
    public class AckResponse_JSON {
        public String record_id;
        public Boolean success;
        public Integer status_code;
        public String message;
        public String additional_data;
    }

    // used in communication ESB -> CRM for reading ACKs
    public class Contact_JSON {
        public String updating_system;
        public String updating_system_contact_id;
        public String first_name;
        public String second_name;
        public String last_name;
        public String family_name;
        public List<Address_JSON> addresses;
        public List<Phone_JSON> phones;
        public String primary_email;
        public List<String> additional_emails;
        public List<MarketingConsents_JSON> consents;
        public MarketingAutomation_JSON marketing_automation;
    }

    public class MarketingAutomation_JSON {
        public String position;
        public List<String> department;
        public List<String> area_of_interest;
        public String a_level_exam_year;
        public String school_type;
        public String school_name;
    }

    public class Address_JSON {
        public String type;
        public String country;
        public String postal_code;
        public String city;
        public String street;
        public String state;
    }

    public class Phone_JSON {
        public String type;
        public String phoneNumber;
    }

    public class Email_JSON {
        public String type;
        public String email;
    }

    public class MarketingConsents_JSON {
        public String type;
        public String department;
        public Boolean value;
        public Date consent_date;
        public Date refusal_date;
        public String source;
        public String storage_location;
    }

    public class Prospect_JSON {
        public String updating_system;
        public String updating_system_contact_id;
        public String first_name;
        public String last_name;
        public String status;
        public String email;
        public String campaign_id;
        public List<Campaign_Details_JSON> campaign_details;
        public List<Phone_JSON> phones;
        public List<MarketingConsents_JSON> consents;
    }

    public class Campaign_Details_JSON {
        public String detail_name;
        public String detail_value;
    }

    // used in communication CRM -> ESB to check if given email exists in CRM, only sent if response is equal to 2xx
    public class ESBEmailResponse_JSON {
        public boolean email_exists;

        public ESBEmailResponse_JSON(boolean email_exists) {
            this.email_exists = email_exists;
        }
    }

    // used in communication ESB -> CRM for reading ZPI requests
    public class ESBEmailRequest_JSON {
        public String email;
    }

    // used in communication ESB -> CRM for reading ZPI requests to check if contact exist in CRM
    public class ContactRequest_JSON {
        public String first_name;
        public String last_name;
        public String email;
    }

    // used in communication CRM -> ESB to check if given contact exist in CRM, only sent if response is equal to 2xx
    public class ContactResponse_JSON {
        public String person_id;

        public ContactResponse_JSON(String person_id) {
            this.person_id = person_id;
        }
    }
}