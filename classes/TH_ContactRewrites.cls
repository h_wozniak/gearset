public without sharing class TH_ContactRewrites extends TriggerHandler.DelegateBase {
    
    // public override void prepareBefore() {}
    
    // public override void prepareAfter() {}
    
    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Contact cNew = (Contact)obj;
            
            /* Sebastian Łasisz */
            // change number and series value to uppercase
            if (cNew.Number_and_Series__c != null) {
                cNew.Number_and_Series__c = cNew.Number_and_Series__c.toUpperCase();
            }

            /* Sebastian Łasisz */
            // Update mailing address with other address when same mailing address checkbox is selected
            if (cNew.The_Same_Mailling_Address__c) {
                cNew.MailingStreet = cNew.OtherStreet;
                cNew.MailingCity = cNew.OtherCity;
                cNew.MailingState = cNew.OtherState;
                cNew.MailingPostalCode = cNew.OtherPostalCode;
                cNew.MailingCountry = cnew.OtherCountry;
            }
        }
    }
    
    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
         for (Id key : old.keySet()) {
            Contact cOld = (Contact)old.get(key);
            Contact cNew = (Contact)o.get(key);

            /* Sebastian Łasisz */
            // change number and series value to uppercase
            if (cNew.Number_and_Series__c != null) {
                cNew.Number_and_Series__c = cNew.Number_and_Series__c.toUpperCase();
            }

            /* Sebastian Łasisz */
            // Update mailing address with other address when same mailing address checkbox is selected
            if (cNew.The_Same_Mailling_Address__c) {
                cNew.MailingStreet = cNew.OtherStreet;
                cNew.MailingCity = cNew.OtherCity;
                cNew.MailingState = cNew.OtherState;
                cNew.MailingPostalCode = cNew.OtherPostalCode;
                cNew.MailingCountry = cnew.OtherCountry;
            }

            /* Sebastian Łasisz */
            // Set proper status based on Lead Scoring
            if (cNew.Lead_Scoring__c != cOld.Lead_Scoring__c && cNew.Lead_Scoring__c != null) {
                if (Integer.valueOf(cNew.Lead_Scoring__c) >= 0 && Integer.valueOf(cNew.Lead_Scoring__c) <= 99) {
                    cNew.Status__c = CommonUtility.CONTACT_STATUS_PROSPECT;
                }
                else if (Integer.valueOf(cNew.Lead_Scoring__c) >= 100 && Integer.valueOf(cNew.Lead_Scoring__c) <= 199) {
                    cNew.Status__c = CommonUtility.CONTACT_STATUS_MQL;
                }
                else if (Integer.valueOf(cNew.Lead_Scoring__c) >= 200 && Integer.valueOf(cNew.Lead_Scoring__c) <= 299) {
                    cNew.Status__c = CommonUtility.CONTACT_STATUS_SAL;
                }
                else if (Integer.valueOf(cNew.Lead_Scoring__c) >= 300 && Integer.valueOf(cNew.Lead_Scoring__c) <= 399) {
                    cNew.Status__c = CommonUtility.CONTACT_STATUS_SQL;
                }
                else if (Integer.valueOf(cNew.Lead_Scoring__c) >= 400) {
                    cNew.Status__c = null;                  
                }
            }

            if (cNew.Pesel__c != cOld.Pesel__c && cNew.Pesel__c != null
                && cNew.Foreigner__c
                && !cNew.Pesel__c.contains('00000')) {

                Integer twoDecimalMonth = Integer.valueOf(cNew.Pesel__c.substring(2, 4));
                Integer twoDecimalYear = Integer.valueOf(cNew.Pesel__c.substring(0, 2));
                Integer fullDecimalYear;
                Integer fullDecimalMonth;
                if (twoDecimalMonth <= 12) {
                    fullDecimalYear = twoDecimalYear + 1900;
                    fullDecimalMonth = twoDecimalMonth;
                }
                else if (twoDecimalMonth <= 32) {
                    fullDecimalYear = twoDecimalYear + 2000;
                    fullDecimalMonth = twoDecimalMonth - 20;
                }
                else if (twoDecimalMonth <= 52) {
                    fullDecimalYear = twoDecimalYear + 2100;
                    fullDecimalMonth = twoDecimalMonth - 40;
                }
                else if (twoDecimalMonth <= 72) {
                    fullDecimalYear = twoDecimalYear + 2200;
                    fullDecimalMonth = twoDecimalMonth - 60;
                }
                else if (twoDecimalMonth <= 92) {
                    fullDecimalYear = twoDecimalYear + 1800;
                    fullDecimalMonth = twoDecimalMonth - 80;
                }


                cNew.Birthdate = Date.newInstance(fullDecimalYear, fullDecimalMonth, Integer.valueOf(cNew.Pesel__c.substring(4, 6)));
                cNew.Gender__c = math.mod(Integer.valueOf(cNew.Pesel__c.substring(9, 10)), 2) == 0 ? CommonUtility.CONTACT_GENDER_FEMALE : CommonUtility.CONTACT_GENDER_MALE;
            }
         }
    }
}