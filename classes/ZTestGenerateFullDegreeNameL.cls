@isTest
private class ZTestGenerateFullDegreeNameL {
    
    //@isTest static void testGenerateFullNameWithEnrollmentWithIDegree() {
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Degree__c = CommonUtility.OFFER_DEGREE_I), true);

    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_I);
    //}
    
    //@isTest static void testGenerateFullNameWithEnrollmentWithIIDegree() {
    //    Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);
    //    Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        
    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_II);
    //}
    
    //@isTest static void testGenerateFullNameWithEnrollmentWithIIPGDegree() {
    //    Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
    //    Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);

    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_II_PG);
    //}
    
    //@isTest static void testGenerateFullNameWithEnrollmentWithUDegree() {
    //    Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_U), true);
    //    Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);

    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_U);
    //}
    
    //@isTest static void testGenerateFullNameWithEnrollmentWithMBADegree() {
    //    Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_MBA), true);
    //    Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        
    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, '');
    //}
    
    //@isTest static void testGenerateFullNameWithoutEnrollment() {
    //    Test.startTest();

    //    try {
    //        String result = DocGen_GenerateFullDegreeName.executeMethod(null, null, null);
    //    }
    //    catch (Exception e) {
    //        System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
    //    }
        
    //    Test.stopTest();
    //}
    
    //@isTest static void testGenerateFullNameWithoutDegree() {
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(studyEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_I);
    //}
    
    //@isTest static void testGenerateFullNameFromDocument() {
    //    Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Degree__c = CommonUtility.OFFER_DEGREE_I), true);
    //    Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

    //    Test.startTest();
    //    String result = DocGen_GenerateFullDegreeName.executeMethod(documentEnrollment.Id, null, null);
    //    Test.stopTest();
        
    //    System.assertEquals(result, DocumentGeneratorHelper.OFFER_DEGREE_FULL_I);
        
    //}

}