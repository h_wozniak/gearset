@isTest
private class ZTestPaymentManager {
    
    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        CustomSettingDefault.initAdditionalCharges();
        ZDataTestUtility.prepareCatalogData(true, true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = true,
            Entry_fee__c = 500
        ), true));

        //config PriceBook
        configPriceBook_I_II_U(retMap.get(1).Id, instAmount);
        
        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(0).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2'
        ), true));
        Test.stopTest();

        return retMap;
    }

    private static void configPriceBook_I_II_U(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void test_addOrRemoveStudentCardPayment() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        List<Payment__c> additionalChargeList = [
            SELECT Id, Payment_Deadline__c, Installment_Year_Calendar__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
            AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        System.assert(!additionalChargeList.isEmpty());

        //Test.startTest();
        Enrollment__c studyEnr1 = new Enrollment__c(
            Id = dataMap.get(2).Id,
            Do_not_set_up_Student_Card__c = true
        );
        update studyEnr1;

        List<Payment__c> additionalChargeList_cbxOn = [
            SELECT Id, Payment_Deadline__c, Installment_Year_Calendar__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :studyEnr1.Id
            AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        System.assert(additionalChargeList_cbxOn.isEmpty());

        Enrollment__c studyEnr2 = new Enrollment__c(
            Id = dataMap.get(2).Id,
            Do_not_set_up_Student_Card__c = false
        );
        update studyEnr2;
        //Test.stopTest();

        List<Payment__c> additionalChargeList_cbxOff = [
            SELECT Id, Payment_Deadline__c, Installment_Year_Calendar__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :studyEnr2.Id
            AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        System.assert(!additionalChargeList_cbxOff.isEmpty());
    }
    
    @isTest static void test_verifyDoNotSetUpStudentCardCbx() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        List<Payment__c> additionalChargeList = [
            SELECT Id, Payment_Deadline__c, Installment_Year_Calendar__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
            AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        System.assert(!additionalChargeList.isEmpty());

        //Test.startTest();
        delete additionalChargeList;

        Enrollment__c studyEnr1 = [SELECT Do_not_set_up_Student_Card__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assert(studyEnr1.Do_not_set_up_Student_Card__c);

        AdditionalCharges__c studentCardChargeCS = AdditionalCharges__c.getInstance(RecordVals.CS_AC_NAME_STUDENTCARD);
        Payment__c studentCardCharge = new Payment__c();
        studentCardCharge.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        studentCardCharge.Enrollment_from_Schedule_Installment__c = studyEnr1.Id;
        studentCardCharge.Description__c = studentCardChargeCS.Name;
        studentCardCharge.PriceBook_Value__c = studentCardChargeCS.Price__c;
        studentCardCharge.Type__c = CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE;
        insert studentCardCharge;


        Enrollment__c studyEnr2 = [SELECT Do_not_set_up_Student_Card__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assert(!studyEnr2.Do_not_set_up_Student_Card__c);
        //Test.stopTest();
    }
    
    @isTest static void test_generatePaymentInstallmentsMap() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        //Test.startTest();
        PaymentManager.generatePaymentInstallmentsMap(dataMap.get(2).Id);
        //Test.stopTest();
    }
    
    @isTest static void test_generatePaymentsMapWithoutFormatting() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        //Test.startTest();
        PaymentManager.generatePaymentsMapWithoutFormatting(dataMap.get(2).Id);
        //Test.stopTest();
    }
    
    @isTest static void test_generatePaymentInstallmentsMapSP() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        //Test.startTest();
        PaymentManager.generatePaymentInstallmentsMapSP(dataMap.get(2).Id);
        //Test.stopTest();
    }
    
    @isTest static void test_generatePaymentInstallmentsMapUnenrolled() {
        Map<Integer, sObject> dataMap = prepareDataForTest(100);

        //Test.startTest();
        PaymentManager.generatePaymentInstallmentsMapUnenrolled(dataMap.get(2).Id);
        //Test.stopTest();
    }
    
}