@isTest
global class ZTestIntegrationArrearsRetrieval {

    @isTest static void test_parseAckResponse() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(0, qEduAgr.Portal_Login__c));

        Test.startTest();
        List<Enrollment__c> eduAgreements = new List<Enrollment__c>();
        eduAgreements.add(qEduAgr);
        IntegrationArrearsRetrieval.retrieveArrearsForLogins(eduAgreements);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, 0);
    }

    @isTest static void test_parseAckResponse_2() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(200, qEduAgr.Portal_Login__c));

        Test.startTest();
        List<Enrollment__c> eduAgreements = new List<Enrollment__c>();
        eduAgreements.add(qEduAgr);
        IntegrationArrearsRetrieval.retrieveArrearsForLogins(eduAgreements);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, -200);
    }

    @isTest static void test_parseAckResponse_3() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock(200, '12345'));

        Test.startTest();
        List<Enrollment__c> eduAgreements = new List<Enrollment__c>();
        eduAgreements.add(qEduAgr);
        IntegrationArrearsRetrieval.retrieveArrearsForLogins(eduAgreements);
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, null);
    }

    @isTest static void test_preparePortalLogins() {
        Boolean errorOccured;

        try {
            Set<String> portalLogins = IntegrationArrearsRetrieval.preparePortalLogins(null);
            System.assert(portalLogins.isEmpty());
        }
        catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);
    }

    @isTest static void test_preparePortalLogins_2() {
        Set<String> portalLogins = IntegrationArrearsRetrieval.preparePortalLogins(new List<Enrollment__c>());
        System.assert(portalLogins.isEmpty());
    }

    @isTest static void test_preparePortalLogins_3() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);

        Set<String> portalLogins = IntegrationArrearsRetrieval.preparePortalLogins(eduAgrList);
        System.assertEquals(portalLogins.size(), 1);
    }

    @isTest static void test_preparePortalLogins_4() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        update qEduAgr;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);

        Set<String> portalLogins = IntegrationArrearsRetrieval.preparePortalLogins(eduAgrList);
        System.assertEquals(portalLogins.size(), 0);
    }

    @isTest static void test_preparePortalLogins_5() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Enrollment__c eduAgr2 = new Enrollment__c();
        eduAgr2.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        eduAgr2.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        eduAgr2.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        insert eduAgr2;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);
        eduAgrList.add(eduAgr2);

        Set<String> portalLogins = IntegrationArrearsRetrieval.preparePortalLogins(eduAgrList);
        System.assertEquals(portalLogins.size(), 1);
    }

    @isTest static void test_retrieveArrearsForLogins() {
        List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

        IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
        response.portal_login = '1234';
        response.to_pay_total = 0;
        response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
        responseJSON.add(response);

        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Enrollment__c eduAgr2 = new Enrollment__c();
        eduAgr2.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        eduAgr2.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        eduAgr2.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        insert eduAgr2;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);
        eduAgrList.add(eduAgr2);

        List<Enrollment__c> eduAgrToUpdate = IntegrationArrearsRetrieval.parseAckResponse(responseJSON, eduAgrList);
        System.assertEquals(eduAgrToUpdate.size(), 1);
        System.assertEquals(eduAgrToUpdate.get(0).Balance__c, 0);
    }

    @isTest static void test_retrieveArrearsForLogins_2() {
        List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Enrollment__c eduAgr2 = new Enrollment__c();
        eduAgr2.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        eduAgr2.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        eduAgr2.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        insert eduAgr2;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);
        eduAgrList.add(eduAgr2);

        List<Enrollment__c> eduAgrToUpdate = IntegrationArrearsRetrieval.parseAckResponse(responseJSON, eduAgrList);
        System.assertEquals(eduAgrToUpdate.size(), 0);
    }

    @isTest static void test_retrieveArrearsForLogins_3() {
        List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

        IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
        response.portal_login = '1234';
        response.to_pay_total = 0;
        response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
        responseJSON.add(response);

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();

        List<Enrollment__c> eduAgrToUpdate = IntegrationArrearsRetrieval.parseAckResponse(responseJSON, eduAgrList);
        System.assertEquals(eduAgrToUpdate.size(), 0);
    }

    @isTest static void test_retrieveArrearsForLogins_4() {
        List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

        IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
        response.portal_login = '12345';
        response.to_pay_total = 0;
        response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
        responseJSON.add(response);

        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Enrollment__c eduAgr2 = new Enrollment__c();
        eduAgr2.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        eduAgr2.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        eduAgr2.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        insert eduAgr2;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);
        eduAgrList.add(eduAgr2);

        List<Enrollment__c> eduAgrToUpdate = IntegrationArrearsRetrieval.parseAckResponse(responseJSON, eduAgrList);
        System.assertEquals(eduAgrToUpdate.size(), 0);
    }

    @isTest static void test_retrieveArrearsForLogins_5() {
        List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

        IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
        response.portal_login = '1234';
        response.to_pay_total = 500;
        response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
        responseJSON.add(response);

        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        qEduAgr.Portal_Login__c = '1234';
        update qEduAgr;

        Enrollment__c eduAgr2 = new Enrollment__c();
        eduAgr2.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        eduAgr2.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        eduAgr2.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        insert eduAgr2;

        List<Enrollment__c> eduAgrList = new List<Enrollment__c>();
        eduAgrList.add(qEduAgr);
        eduAgrList.add(eduAgr2);

        List<Enrollment__c> eduAgrToUpdate = IntegrationArrearsRetrieval.parseAckResponse(responseJSON, eduAgrList);
        System.assertEquals(eduAgrToUpdate.size(), 1);
        System.assertEquals(eduAgrToUpdate.get(0).Balance__c, -500);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class ArrearsRetrieveCalloutMock implements HttpCalloutMock {
        Integer balance;
        String portalLogin;

        ArrearsRetrieveCalloutMock(Integer balance, String portalLogin) {
            this.balance = balance;
            this.portalLogin = portalLogin;
        }

        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);

            List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

            IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
            response.portal_login = portalLogin;
            response.to_pay_total = balance;
            response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
            responseJSON.add(response);

            String preparedJson = JSON.serializePretty(responseJSON);
            res.setBody(preparedJson);

            return res;
        }
    }

}