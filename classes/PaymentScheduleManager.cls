/**
* @author       Wojciech Słodziak
* @editor       Sebastian Łasisz
* @description  Class for Payment Schedule management 
**/

public without sharing class PaymentScheduleManager {



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------- PAYMENT SCHEDULE GENERATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method called from trigger to generate Payment Schedule based on Offer and selected Price Book
    public static void generatePaymentSchedules(Set<Id> enrIds) {
        List<Payment__c> installmentsToDelete = PaymentScheduleManager.getInstallmentsToDelete(enrIds);

        Set<Id> installmentsToDeleteIds = new Set<Id>();
        for (Payment__c payment : installmentsToDelete) {
            installmentsToDeleteIds.add(payment.Id);
        }

        List<Payment__c> installmentsToInsert = new List<Payment__c>();

        List<Enrollment__c> studyEnrList = [
                SELECT Id, OwnerId, University_Name__c, Degree__c, Price_Book_from_Enrollment__c, Course_or_Specialty_Universal__c,
                        Installments_per_Year__c, Tuition_System__c, Starting_Semester__c, Unenrolled_Status__c,
                        Course_or_Specialty_Universal__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
                        Course_or_Specialty_Universal__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
                        Course_or_Specialty_Universal__r.Kind__c, Unenrolled__c, WasIs_Unenrolled__c, Do_not_set_up_Student_Card__c,
                        Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c, Number_of_Semesters__c,
                (SELECT Id, Type__c FROM ScheduleInstallments__r WHERE Type__c = :CommonUtility.PAYMENT_TYPE_ENTRY_FEE AND Id NOT IN :installmentsToDeleteIds)
                FROM Enrollment__c
                WHERE Id IN :enrIds AND Price_Book_from_Enrollment__c != null
        ];

        Set<Id> pbIds = new Set<Id>();
        for (Enrollment__c studyEnr : studyEnrList) {
            pbIds.add(studyEnr.Price_Book_from_Enrollment__c);
        }

        Map<Id, Offer__c> pbMap = new Map<Id, Offer__c>([
                SELECT Id, Number_of_Semesters_formula__c, Entry_fee__c, Onetime_Price__c, RecordTypeId, Schedule_Model__c, Schedule_Model_2__c, Enrollment_fee__c, Qualifying_fee__c,
                (SELECT Id, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c,
                        Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c WHERE Id IN :pbIds
        ]);

        for (Enrollment__c studyEnr : studyEnrList) {
            Date studyStartDate = (studyEnr.Course_or_Specialty_Universal__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null?
                    studyEnr.Course_or_Specialty_Universal__r.University_Study_Offer_from_Course__r.Study_Start_Date__c :
                    studyEnr.Course_or_Specialty_Universal__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c);

            Offer__c priceBook = pbMap.get(studyEnr.Price_Book_from_Enrollment__c);

            installmentsToInsert.addAll(PaymentScheduleManager.generatePaymentInstallments(studyEnr, priceBook, studyStartDate));
        }

        try {
            CommonUtility.skipStudyEnrValueCalculation = true;
            delete installmentsToDelete;
            CommonUtility.skipStudyEnrValueCalculation = false;

            insert installmentsToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        // apply discounts for freshly generated Payment Schedule if they exist
        DiscountApplicationManager_Studies.applyDiscountsToPaymentSchedule(enrIds);
    }

    /* Wojciech Słodziak */
    // installments that are already synced shouldn't be deleted
    private static List<Payment__c> getInstallmentsToDelete(Set<Id> enrIds) {
        return [
                SELECT Id
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c IN :enrIds
                AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
                AND Is_added_manually__c = false
        ];
    }

    /* Wojciech Słodziak */
    // dispatching method for generating Payment Installments
    private static List<Payment__c> generatePaymentInstallments(Enrollment__c studyEnr, Offer__c priceBook, Date studyStartDate) {
        List<Payment__c> paymentList;

        Integer realNumberOfSemesters = studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c != null ?
                Integer.valueOf(studyEnr.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c) :
                Integer.valueOf(studyEnr.Number_of_Semesters__c);

        if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)) {
            paymentList = PaymentScheduleManager.generatePaymentInstallmentsFor_I_II_U(studyEnr, priceBook, studyStartDate, realNumberOfSemesters);
        } else
                if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG)
                        || priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)) {
                    paymentList = PaymentScheduleManager.generatePaymentInstallmentsFor_PG_MBA(studyEnr, priceBook, studyStartDate, Integer.valueOf(studyEnr.Number_of_Semesters__c));
                }

        return paymentList;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------- I, II && U Degree GENERATION ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method usable for I & II Degree Study Price Books
    private static List<Payment__c> generatePaymentInstallmentsFor_I_II_U(Enrollment__c studyEnr, Offer__c priceBook, Date studyStartDate, Integer realNumberOfSemesters) {
        List<Payment__c> paymentInstList = new List<Payment__c>();

        String installmentPostfix = String.valueOf(studyStartDate.month()).leftPad(2, '0') + '_' + studyStartDate.year();

        Integer semesterCount = Integer.valueOf(priceBook.Number_of_Semesters_formula__c);
        Integer yearCount = Integer.valueOf(Math.ceil(Double.valueOf(semesterCount) / 2));
        Boolean oddSemesters = Math.mod(semesterCount, 2) == 1;
        Integer instPerYear = (studyEnr.Installments_per_Year__c != null) ? Integer.valueOf(studyEnr.Installments_per_Year__c) : 1;
        Boolean isWinterEnrollment = PaymentScheduleManager.isWinterEnrollment(studyStartDate);
        String offerKind = studyEnr.Course_or_Specialty_Universal__r.Kind__c;

        Map<Integer, Offer__c> variantToInstConfigMap = PriceBookManager.getInstallmentConfigsMapByInstallmentVariant(priceBook.InstallmentConfigs__r);

        Map<Integer, PaymentScheduleDeadlineDesignator> psddMap = new Map<Integer, PaymentScheduleDeadlineDesignator>();

        // student card charge
        Payment__c studentCardCharge;
        if (!studyEnr.Do_not_set_up_Student_Card__c) {
            studentCardCharge = PaymentScheduleManager.buildStudentCardCharge(studyEnr);

            if (studentCardCharge != null) {
                paymentInstList.add(studentCardCharge);
            }
        }

        // entry installment
        Payment__c entryInst;

        if (priceBook.Entry_fee__c != null && priceBook.Entry_fee__c > 0 && studyEnr.ScheduleInstallments__r.isEmpty()) {
            entryInst = PaymentScheduleManager.buildEntryFeeInst(studyEnr, priceBook);

            paymentInstList.add(entryInst);
        }

        // enrollment fee installment
        Payment__c enrollmentFeeInst;
        if (priceBook.Enrollment_fee__c != null && priceBook.Enrollment_fee__c > 0) {
            enrollmentFeeInst = PaymentScheduleManager.buildEnrollmentFeeInst(studyEnr, priceBook);

            paymentInstList.add(enrollmentFeeInst);
        }

        // one-time study installment
        if (studyEnr.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME) {
            Payment__c oneTimeInst = PaymentScheduleManager.buildOnetimeInst(studyEnr, priceBook, studyStartDate);

            PaymentScheduleDeadlineDesignator psdd = new PaymentScheduleDeadlineDesignator(studyEnr.University_Name__c, studyEnr.Degree__c, isWinterEnrollment,
                    studyStartDate, 1, yearCount, semesterCount,
                    '1', offerKind, realNumberOfSemesters);
            oneTimeInst.Payment_Deadline__c = psdd.designateDateForInstallment(1, 0);

            if (isWinterEnrollment) {
                if (oneTimeInst.Payment_Deadline__c != null) {
                    if (oneTimeInst.Payment_Deadline__c.month() < 9) {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year() - 1);
                    } else {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year());
                    }
                }
            }
            else {
                if (oneTimeInst.Payment_Deadline__c != null) {
                    if (oneTimeInst.Payment_Deadline__c.month() < 9) {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year() - 1);
                    } else {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year());
                    }
                }
            }

            paymentInstList.add(oneTimeInst);
        }
        else {

            // tuition installments
            Integer serialNo = 1;
            Integer currentYear = 0;
            Integer passedInstallments = 0;

            for (Integer year = 0; year < yearCount; year++) {
                Boolean isLastYear = year == (yearCount - 1);

                Integer installmentsThisYear = (isLastYear && instPerYear == 12 && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_MBA)? 10 : instPerYear;
                Integer actualInstallmentsThisYear = (oddSemesters && isLastYear && installmentsThisYear > 1)? installmentsThisYear / 2 : installmentsThisYear;

                for (Integer installmentNum = 1; installmentNum <= actualInstallmentsThisYear; installmentNum++) {
                    Integer installmentSemester = (year * 2) + (installmentNum <= Math.ceil(Decimal.valueOf(installmentsThisYear) / 2)? 1 : 2);

                    // for starting different than first generate only Installments starting from selected Semester
                    if (installmentSemester >= Integer.valueOf(studyEnr.Starting_Semester__c)) {
                        passedInstallments++;

                        Integer instVariantThisYear;
                        if (oddSemesters && isLastYear && instPerYear == 1) {
                            instVariantThisYear = 2;
                        } else if (isLastYear && instPerYear == 12 && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_MBA) {
                            instVariantThisYear = 10;
                        } else {
                            instVariantThisYear = instPerYear;
                        }

                        Decimal valueForThisInstallment;
                        if (variantToInstConfigMap.containsKey(instVariantThisYear)) {
                            if (studyEnr.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED) {
                                valueForThisInstallment = variantToInstConfigMap.get(instVariantThisYear).Fixed_Price__c;
                            } else {
                                valueForThisInstallment = (Decimal) variantToInstConfigMap.get(instVariantThisYear).get(PriceBookManager.gradedPriceFields[year]);
                            }
                        }
                        /* Additional validation in case price book is empty */
                        else if (variantToInstConfigMap.containsKey(instPerYear)) {
                            /* If there is one tuition payment per year and there's not price book defined for 2 rates per year, take half of the first years rate */
                            if (instVariantThisYear == 2 && oddSemesters && isLastYear && instPerYear == 1) {
                                if (studyEnr.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED) {
                                    valueForThisInstallment = variantToInstConfigMap.get(instPerYear).Fixed_Price__c;
                                } else {
                                    valueForThisInstallment = (Decimal) variantToInstConfigMap.get(instPerYear).get(PriceBookManager.gradedPriceFields[year-1]);
                                }

                                valueForThisInstallment *= 0.5;
                                valueForThisInstallment = valueForThisInstallment.setScale(0, RoundingMode.HALF_DOWN);
                            }
                        }

                        Payment__c tuitionInst = new Payment__c();
                        // if Candidate is Unenrolled mark first 2 Tuition Payment Installments
                        if (!studyEnr.Unenrolled__c || serialNo <= UnenrolledManager.numberOfUnenrolledSemesters) {
                            tuitionInst.Unenrolled_Installment__c = studyEnr.Unenrolled__c;
                        }

                        tuitionInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
                        tuitionInst.OwnerId = studyEnr.OwnerId;
                        tuitionInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
                        tuitionInst.Description__c = installmentNum + 'z' + actualInstallmentsThisYear + '-' + installmentPostfix;
                        tuitionInst.PriceBook_Value__c = valueForThisInstallment;
                        tuitionInst.Type__c = CommonUtility.PAYMENT_TYPE_TUITION_FEE;
                        tuitionInst.Schedule_Model__c = (Math.mod(installmentSemester, 2) == 1 ? priceBook.Schedule_Model__c : priceBook.Schedule_Model_2__c);
                        tuitionInst.Installment_Year__c = year + 1;
                        tuitionInst.Installment_Semester__c = installmentSemester;
                        tuitionInst.Installments_this_Year__c = actualInstallmentsThisYear;
                        tuitionInst.Installment_Variant__c = String.valueOf(instVariantThisYear);
                        tuitionInst.Installment_Serial_Number__c = serialNo++;
                        tuitionInst.Installment_Number_in_Year__c = installmentNum;

                        PaymentScheduleDeadlineDesignator psdd = psddMap.get(instVariantThisYear);
                        if (psdd == null) {
                            psdd = new PaymentScheduleDeadlineDesignator(studyEnr.University_Name__c, studyEnr.Degree__c, isWinterEnrollment,
                                    studyStartDate, instVariantThisYear, yearCount, semesterCount,
                                    '1', offerKind, realNumberOfSemesters);
                            psddMap.put(instVariantThisYear, psdd);
                        }

                        if (psdd.designateDateForInstallment(installmentNum, year) != null) {
                            tuitionInst.Payment_Deadline__c = psdd.designateDateForInstallment(installmentNum, year);
                        }
                        else {
                            currentYear++;
                            passedInstallments = 1;
                            tuitionInst.Payment_Deadline__c = psdd.designateDateForInstallment(installmentNum, year);
                        }

                        if (tuitionInst.Payment_Deadline__c != null) {
                            if (isWinterEnrollment) {
                                if (tuitionInst.Payment_Deadline__c.month() < 9) {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year() - 1);
                                } else {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year());
                                }
                            }
                            else {
                                if (tuitionInst.Payment_Deadline__c.month() < 9) {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year() - 1);
                                } else {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year());
                                }
                            }
                        }

                        paymentInstList.add(tuitionInst);

                        if (passedInstallments == instPerYear) {
                            currentYear++;
                            passedInstallments = 0;
                        }
                    }
                }
            }
        }

        Integer firstTuitionInstOffset = 0;
        if (studentCardCharge != null) firstTuitionInstOffset++;
        if (entryInst != null) firstTuitionInstOffset++;
        if (enrollmentFeeInst != null) firstTuitionInstOffset++;

        // apply info from first Tuition Installment to Student Card Charge Installment
        if (studentCardCharge != null && paymentInstList.size() > firstTuitionInstOffset) {
            studentCardCharge.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            studentCardCharge.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            studentCardCharge.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // apply info from first Tuition Installment to Entry Installment
        if (entryInst != null && paymentInstList.size() > firstTuitionInstOffset) {
            entryInst.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            entryInst.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            entryInst.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // apply info from first Tuition Installment to Enrollment fee Installment
        if (enrollmentFeeInst != null && paymentInstList.size() > firstTuitionInstOffset) {
            enrollmentFeeInst.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            enrollmentFeeInst.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            enrollmentFeeInst.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // if candidate was unenrolled that means he had 2 payments synced with KS and they shouldn't be generatred again
        // it must be deleted at this point for payment deadlines to be generated correctly
        if (!studyEnr.Unenrolled__c && studyEnr.WasIs_Unenrolled__c && studyEnr.Unenrolled_Status__c == CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS) {
            Integer j = 0;
            while (j < paymentInstList.size()) {
                if (paymentInstList[j].Installment_Serial_Number__c == 1 || paymentInstList[j].Installment_Serial_Number__c == 2) {
                    paymentInstList.remove(j);
                } else {
                    j++;
                }
            }
        }

        return paymentInstList;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- PG & MBA GENERATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method usable for PG & MBA Degree Study Price Books
    private static List<Payment__c> generatePaymentInstallmentsFor_PG_MBA(Enrollment__c studyEnr, Offer__c priceBook, Date studyStartDate, Integer realNumberOfSemesters) {
        List<Payment__c> paymentInstList = new List<Payment__c>();

        String installmentPostfix = String.valueOf(studyStartDate.month()).leftPad(2, '0') + '_' + studyStartDate.year();

        Integer semesterCount = Integer.valueOf(priceBook.Number_of_Semesters_formula__c);
        Integer yearCount = Integer.valueOf(Math.ceil(Double.valueOf(semesterCount) / 2));
        Boolean oddSemesters = Math.mod(semesterCount, 2) == 1;
        Integer instPerYear = (studyEnr.Installments_per_Year__c != null) ? Integer.valueOf(studyEnr.Installments_per_Year__c) : 1;
        Boolean isWinterEnrollment = PaymentScheduleManager.isWinterEnrollment(studyStartDate);
        String offerKind = studyEnr.Course_or_Specialty_Universal__r.Kind__c;

        Map<Integer, Offer__c> variantToInstConfigMap = PriceBookManager.getInstallmentConfigsMapByInstallmentVariant(priceBook.InstallmentConfigs__r);

        Map<Integer, PaymentScheduleDeadlineDesignator> psddMap = new Map<Integer, PaymentScheduleDeadlineDesignator>();

        // student card charge
        Payment__c studentCardCharge;
        if (!studyEnr.Do_not_set_up_Student_Card__c) {
            studentCardCharge = PaymentScheduleManager.buildStudentCardCharge(studyEnr);

            if (studentCardCharge != null) {
                paymentInstList.add(studentCardCharge);
            }
        }

        // entry installment
        Payment__c entryInst;
        if (priceBook.Entry_fee__c != null && priceBook.Entry_fee__c > 0 && studyEnr.ScheduleInstallments__r.isEmpty()) {
            entryInst = PaymentScheduleManager.buildEntryFeeInst(studyEnr, priceBook);

            paymentInstList.add(entryInst);
        }

        // Qualifying Fee charge
        Payment__c qualifyingInst;
        if (priceBook.Qualifying_fee__c != null && priceBook.Qualifying_fee__c > 0) {
            qualifyingInst = PaymentScheduleManager.buildQualifyingFee(studyEnr, priceBook);

            paymentInstList.add(qualifyingInst);
        }

        // enrollment fee installment
        Payment__c enrollmentFeeInst;
        if (priceBook.Enrollment_fee__c != null && priceBook.Enrollment_fee__c > 0) {
            enrollmentFeeInst = PaymentScheduleManager.buildEnrollmentFeeInst(studyEnr, priceBook);

            paymentInstList.add(enrollmentFeeInst);
        }

        // one-time study installment
        if (studyEnr.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME) {
            Payment__c oneTimeInst = PaymentScheduleManager.buildOnetimeInst(studyEnr, priceBook, studyStartDate);

            PaymentScheduleDeadlineDesignator psdd = new PaymentScheduleDeadlineDesignator(studyEnr.University_Name__c, studyEnr.Degree__c, isWinterEnrollment,
                    studyStartDate, 1, yearCount, semesterCount,
                    '1', offerKind, realNumberOfSemesters);
            oneTimeInst.Payment_Deadline__c = psdd.designateDateForInstallment(1, 0);

            if (isWinterEnrollment) {
                if (oneTimeInst.Payment_Deadline__c != null) {
                    if (oneTimeInst.Payment_Deadline__c.month() < 9) {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year() - 1);
                    } else {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year());
                    }
                }
            }
            else {
                if (oneTimeInst.Payment_Deadline__c != null) {
                    if (oneTimeInst.Payment_Deadline__c.month() < 9) {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year() - 1);
                    } else {
                        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(oneTimeInst.Payment_Deadline__c.year());
                    }
                }
            }

            paymentInstList.add(oneTimeInst);
        }
        else {

            // tuition installments
            Integer serialNo = 1;
            Integer currentYear = 0;
            Integer passedInstallments = 0;

            for (Integer year = 0; year < yearCount; year++) {
                Boolean isLastYear = year == (yearCount - 1);

                Decimal installmentsThisYear = (isLastYear && (instPerYear == 12 || instPerYear == 11) && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_MBA)? 10 : instPerYear;

                Integer actualInstallmentsThisYear = Integer.valueOf((oddSemesters && isLastYear && installmentsThisYear > 1)?
                        Math.ceil(installmentsThisYear / 2) : installmentsThisYear);

                for (Integer installmentNum = 1; installmentNum <= actualInstallmentsThisYear; installmentNum++) {

                    Integer installmentSemester = (year * 2) + (installmentNum <= Math.ceil(installmentsThisYear / 2)? 1 : 2);


                    if (installmentSemester >= Integer.valueOf(studyEnr.Starting_Semester__c)) {
                        passedInstallments++;

                        Integer instVariantThisYear;
                        // exceptions
                        if (oddSemesters && isLastYear && instPerYear == 1) {
                            instVariantThisYear = 2;
                        } else if (isLastYear && (instPerYear == 12 || instPerYear == 11) && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_PG && studyEnr.Degree__c != CommonUtility.OFFER_DEGREE_MBA) {
                            instVariantThisYear = 10;
                        } else {
                            instVariantThisYear = instPerYear;
                        }

                        Decimal valueForThisInstallment;
                        if (variantToInstConfigMap.containsKey(instVariantThisYear)) {
                            valueForThisInstallment = variantToInstConfigMap.get(instVariantThisYear).Fixed_Price__c;
                        }
                        /* Additional validation in case price book is empty */
                        else if (variantToInstConfigMap.containsKey(instPerYear)) {
                            /* If there is one tuition payment per year and there's not price book defined for 2 rates per year, take half of the first years rate */
                            if (instVariantThisYear == 2 && oddSemesters && isLastYear && instPerYear == 1) {
                                valueForThisInstallment = variantToInstConfigMap.get(instPerYear).Fixed_Price__c;

                                valueForThisInstallment *= 0.5;
                                valueForThisInstallment = valueForThisInstallment.setScale(0, RoundingMode.HALF_DOWN);
                            }
                        }
                        else {
                            return new List<Payment__c>(); // in case Price Book configuration is not adequate
                        }

                        Payment__c tuitionInst = new Payment__c();
                        tuitionInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
                        tuitionInst.OwnerId = studyEnr.OwnerId;
                        tuitionInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
                        tuitionInst.Description__c = installmentNum + 'z' + actualInstallmentsThisYear + '-' + installmentPostfix;
                        tuitionInst.PriceBook_Value__c = valueForThisInstallment;
                        tuitionInst.Type__c = CommonUtility.PAYMENT_TYPE_TUITION_FEE;
                        tuitionInst.Schedule_Model__c = (Math.mod(installmentSemester, 2) == 1? priceBook.Schedule_Model__c : priceBook.Schedule_Model_2__c);
                        tuitionInst.Installment_Year__c = year + 1;
                        tuitionInst.Installment_Semester__c = installmentSemester;
                        tuitionInst.Installments_this_Year__c = actualInstallmentsThisYear;
                        tuitionInst.Installment_Variant__c = String.valueOf(instVariantThisYear);
                        tuitionInst.Installment_Serial_Number__c = serialNo++;
                        tuitionInst.Installment_Number_in_Year__c = installmentNum;

                        //if (isWinterEnrollment && Math.mod(Integer.valueOf(tuitionInst.Installment_Semester__c), 2) == 1) {
                        //    tuitionInst.Installment_Year_Calendar__c = String.valueOf(studyStartDate.year() + currentYear - 1);
                        //} else {
                        //    tuitionInst.Installment_Year_Calendar__c = String.valueOf(studyStartDate.year() + currentYear);
                        //}

                        PaymentScheduleDeadlineDesignator psdd = psddMap.get(instVariantThisYear);
                        if (psdd == null) {
                            psdd = new PaymentScheduleDeadlineDesignator(studyEnr.University_Name__c, studyEnr.Degree__c, isWinterEnrollment,
                                    studyStartDate, instVariantThisYear, yearCount, semesterCount,
                                    '1', offerKind, realNumberOfSemesters);
                            psddMap.put(instVariantThisYear, psdd);
                        }

                        if (psdd.designateDateForInstallment(installmentNum, year) != null) {
                            tuitionInst.Payment_Deadline__c = psdd.designateDateForInstallment(installmentNum, year);
                        }
                        else {
                            currentYear++;
                            passedInstallments = 1;
                            tuitionInst.Payment_Deadline__c = psdd.designateDateForInstallment(installmentNum, year);
                        }

                        if (isWinterEnrollment) {
                            if (tuitionInst.Payment_Deadline__c != null) {
                                if (tuitionInst.Payment_Deadline__c.month() < 9) {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year() - 1);
                                } else {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year());
                                }
                            }
                        }
                        else {
                            if (tuitionInst.Payment_Deadline__c != null) {
                                if (tuitionInst.Payment_Deadline__c.month() < 9) {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year() - 1);
                                } else {
                                    tuitionInst.Installment_Year_Calendar__c = String.valueOf(tuitionInst.Payment_Deadline__c.year());
                                }
                            }
                        }

                        paymentInstList.add(tuitionInst);

                        if (passedInstallments == instPerYear) {
                            currentYear++;
                            passedInstallments = 0;
                        }
                    }
                }
            }
        }

        Integer firstTuitionInstOffset = 0;
        if (studentCardCharge != null) firstTuitionInstOffset++;
        if (entryInst != null) firstTuitionInstOffset++;
        if (enrollmentFeeInst != null) firstTuitionInstOffset++;
        if (qualifyingInst != null) firstTuitionInstOffset++;

        // apply info from first Tuition Installment to Student Card Charge Installment
        if (studentCardCharge != null && paymentInstList.size() > firstTuitionInstOffset) {
            studentCardCharge.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            studentCardCharge.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            studentCardCharge.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // apply info from first Tuition Installment to Entry Installment
        if (entryInst != null && paymentInstList.size() > firstTuitionInstOffset) {
            entryInst.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            entryInst.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            entryInst.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // apply info from first Tuition Installment to Enrollment fee Installment
        if (enrollmentFeeInst != null && paymentInstList.size() > firstTuitionInstOffset) {
            enrollmentFeeInst.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            enrollmentFeeInst.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            enrollmentFeeInst.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        // apply info from first Tuition Installment to Enrollment fee Installment
        if (qualifyingInst != null && paymentInstList.size() > firstTuitionInstOffset) {
            qualifyingInst.Installment_Year_Calendar__c = paymentInstList[firstTuitionInstOffset].Installment_Year_Calendar__c;
            qualifyingInst.Installment_Semester__c = paymentInstList[firstTuitionInstOffset].Installment_Semester__c;
            qualifyingInst.Payment_Deadline__c = paymentInstList[firstTuitionInstOffset].Payment_Deadline__c;
        }

        return paymentInstList;
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- HELPER METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method for building Student Card charge - based on Custom Settings
    private static Payment__c buildStudentCardCharge(Enrollment__c studyEnr) {
        AdditionalCharges__c studentCardChargeCS = AdditionalCharges__c.getInstance(RecordVals.CS_AC_NAME_STUDENTCARD);

        if (studentCardChargeCS != null) {
            Payment__c studentCardCharge = new Payment__c();
            studentCardCharge.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
            studentCardCharge.OwnerId = studyEnr.OwnerId;
            studentCardCharge.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
            studentCardCharge.Description__c = studentCardChargeCS.Name;
            studentCardCharge.PriceBook_Value__c = studentCardChargeCS.Price__c;
            studentCardCharge.Type__c = CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE;
            return studentCardCharge;
        } else {
            ErrorLogger.msg(CommonUtility.CS_UNDEFINED_ERROR + ' ' + RecordVals.CS_AC_NAME_STUDENTCARD);
            return null;
        }
    }

    /* Wojciech Słodziak */
    // method for building Entry fee Installment
    private static Payment__c buildEntryFeeInst(Enrollment__c studyEnr, Offer__c priceBook) {
        Payment__c entryInst = new Payment__c();
        entryInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        entryInst.OwnerId = studyEnr.OwnerId;
        entryInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
        entryInst.Description__c = Label.title_EntryFee;
        entryInst.PriceBook_Value__c = priceBook.Entry_fee__c;
        entryInst.Type__c = CommonUtility.PAYMENT_TYPE_ENTRY_FEE;
        entryInst.Unenrolled_Installment__c = studyEnr.Unenrolled__c;

        return entryInst;
    }

    /* Sebastian Łasisz */
    // method for building Qualifying fee installment
    private static Payment__c buildQualifyingFee(Enrollment__c studyEnr, Offer__c priceBook) {
        Payment__c entryInst = new Payment__c();
        entryInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        entryInst.OwnerId = studyEnr.OwnerId;
        entryInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
        entryInst.Description__c = Label.title_qualificationProcedure;
        entryInst.PriceBook_Value__c = priceBook.Qualifying_fee__c;
        entryInst.Type__c = CommonUtility.PAYMENT_TYPE_QUALIFYING_FEE;

        return entryInst;
    }

    /* Wojciech Słodziak */
    // method for building Enrollment fee Installment
    private static Payment__c buildEnrollmentFeeInst(Enrollment__c studyEnr, Offer__c priceBook) {
        Payment__c entryInst = new Payment__c();
        entryInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        entryInst.OwnerId = studyEnr.OwnerId;
        entryInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
        entryInst.Description__c = Label.title_EnrollmentFee;
        entryInst.PriceBook_Value__c = priceBook.Enrollment_fee__c;
        entryInst.Type__c = CommonUtility.PAYMENT_TYPE_ENROLLMENT_FEE;

        return entryInst;
    }

    /* Wojciech Słodziak */
    // method for building One-time Installment
    private static Payment__c buildOnetimeInst(Enrollment__c studyEnr, Offer__c priceBook, Date studyStartDate) {
        Payment__c oneTimeInst = new Payment__c();
        oneTimeInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        oneTimeInst.OwnerId = studyEnr.OwnerId;
        oneTimeInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
        oneTimeInst.Description__c = Label.title_OneTimeInst;
        oneTimeInst.PriceBook_Value__c = priceBook.Onetime_Price__c;
        oneTimeInst.Schedule_Model__c = priceBook.Schedule_Model__c;
        oneTimeInst.Type__c = CommonUtility.PAYMENT_TYPE_TUITION_FEE;
        oneTimeInst.Installment_Year__c = 1;
        oneTimeInst.Installment_Semester__c = 1;
        oneTimeInst.Installments_this_Year__c = 1;
        oneTimeInst.Installment_Variant__c = String.valueOf(1);
        oneTimeInst.Installment_Serial_Number__c = 1;
        oneTimeInst.Installment_Number_in_Year__c = 1;
        oneTimeInst.Installment_Year_Calendar__c = String.valueOf(studyStartDate.year());

        return oneTimeInst;
    }

    /* Wojciech Słodziak */
    // helper method to check if date is in first 6 months of year
    public static Boolean isWinterEnrollment(Date dateToCheck) {
        return dateToCheck.month() >= 2 && dateToCheck.month() <= 7;
    }
}