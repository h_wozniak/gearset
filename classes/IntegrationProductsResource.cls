/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send Products to Experia when queried.
**/

@RestResource(urlMapping = '/products/*')
global without sharing class IntegrationProductsResource {
    
    @HttpGet
    global static void sendProducts() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        try {
            res.responseBody = Blob.valueOf(IntegrationProductsHelper.prepareProductsToSend());
            res.statusCode = 200;
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;    
        }
    }
}