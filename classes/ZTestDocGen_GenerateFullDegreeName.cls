@isTest
private class ZTestDocGen_GenerateFullDegreeName {
    private static Map<Integer, sObject> prepareDataForTest_EN(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

    private static Map<Integer, sObject> prepareDataForTest_PL(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

    private static Map<Integer, sObject> prepareDataForTest_RU(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

	@isTest static void test_generateFullDegreeName_I() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_I);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_I.toLowerCase());
	}

	@isTest static void test_generateFullDegreeName_II() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_II.toLowerCase());		
	}

	@isTest static void test_generateFullDegreeName_II_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_II_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_II_PG.toLowerCase());		
	}

	@isTest static void test_generateFullDegreeName_U() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_U);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_U.toLowerCase());		
	}

	@isTest static void test_generateFullDegreeName_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_PG.toLowerCase());		
	}

	@isTest static void test_generateFullDegreeName_MBA() {
		Map<Integer, sObject> dataMap = prepareDataForTest_EN(CommonUtility.OFFER_DEGREE_MBA);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_MBA);		
	}



	@isTest static void PL_test_generateFullDegreeName_I() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_I);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_I.toLowerCase());
	}

	@isTest static void PL_test_generateFullDegreeName_II() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_II.toLowerCase());		
	}

	@isTest static void PL_test_generateFullDegreeName_II_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_II_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_II_PG.toLowerCase());		
	}

	@isTest static void PL_test_generateFullDegreeName_U() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_U);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_U.toLowerCase());		
	}

	@isTest static void PL_test_generateFullDegreeName_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_PG.toLowerCase());		
	}

	@isTest static void PL_test_generateFullDegreeName_MBA() {
		Map<Integer, sObject> dataMap = prepareDataForTest_PL(CommonUtility.OFFER_DEGREE_MBA);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_MBA);		
	}



	@isTest static void RU_test_generateFullDegreeName_I() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_I);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_I.toLowerCase());
	}

	@isTest static void RU_test_generateFullDegreeName_II() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_II.toLowerCase());		
	}

	@isTest static void RU_test_generateFullDegreeName_II_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_II_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_II_PG.toLowerCase());		
	}

	@isTest static void RU_test_generateFullDegreeName_U() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_U);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_U.toLowerCase());		
	}

	@isTest static void RU_test_generateFullDegreeName_PG() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_PG);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_PG.toLowerCase());		
	}

	@isTest static void RU_test_generateFullDegreeName_MBA() {
		Map<Integer, sObject> dataMap = prepareDataForTest_RU(CommonUtility.OFFER_DEGREE_MBA);

		Test.startTest();
		String result = DocGen_GenerateFullDegreeName.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_MBA);		
	}
}