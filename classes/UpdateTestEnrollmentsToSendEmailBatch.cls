/**
* @author       Sebastian Łasisz
* @description  Batch class for sending given email template
**/

global class UpdateTestEnrollmentsToSendEmailBatch implements Database.Batchable<sObject> {
    Integer templateNumber;
    String external_id;

    global UpdateTestEnrollmentsToSendEmailBatch(Integer templateNumber, String external_id) {
        this.templateNumber = templateNumber;
        this.external_id = external_id;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id
                FROM Enrollment__c
                WHERE Candidate_Student__r.External_Contact_Id__c = :external_id
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject enr : scope) {
        	Enrollment__c studyEnr = (Enrollment__c)enr;

        	if (templateNumber == 21) {
        		studyEnr.VerificationLinkEmailedCount__c = 0;
        		update studyEnr;

        		EmailManager.sendVerificationLink(new Set<Id> { studyEnr.Id });
        	}
        	else if (templateNumber == 22) {
        		studyEnr.VerificationLinkEmailedCount__c = 1;
        		update studyEnr;

        		EmailManager.sendVerificationLink(new Set<Id> { studyEnr.Id });
        	}
        	else if (templateNumber == 23) {
        		studyEnr.RequiredDocListEmailedCount__c = 0;
        		update studyEnr;

        		EmailManager_DocumentListEmail.sendRequiredDocumentListFinal(new Set<Id> { studyEnr.Id });
        	}
        	else if (templateNumber == 25) {
        		EmailManager_UnenrolledDocumentListEmail.sendRequiredDocumentListFinal(new Set<Id> { studyEnr.Id });
        	}
        	else if (templateNumber == 26) {
        		studyEnr.RequiredDocListEmailedCount__c = 1;
        		update studyEnr;

        		EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(new Set<Id> { studyEnr.Id });	
        	}
        	else if (templateNumber == 27) {
        		studyEnr.RequiredDocListEmailedCount__c = 2;
        		update studyEnr;

        		EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(new Set<Id> { studyEnr.Id });	
        	}
        	else if (templateNumber == 28) {
        		EmailManager_StudyAcceptanceConfEmail.sendStudyAcceptanceConfirmationEmail(new Set<Id> { studyEnr.Id });
        	}
        	else if (templateNumber == 29) {
        		EmailManager_StudyExtranetEmail.sendExtranetInformationnEmail(new Set<Id> { studyEnr.Id });
        	}
        }
    }
    
    global void finish(Database.BatchableContext BC) {}    
}