/**
* @author       Mateusz Pruszyński, editor Wojciech Słodziak
**/

public with sharing class EnrollmentContactLookupWindow {

    public final String TYPE_PARTICIPANT = 'participant';
    public final String TYPE_CONTACT_PERSON = 'contactPerson';


    public String searchTxt {get; set;}
    public List<Contact> contactList {get; set;}
    public Id rt {get; set;}
    public Boolean newMode {get; set;}
    public Boolean editMode {get; set;}
    public Contact newContact {get; set;}
    public Contact editContact {get; set;}
    public String nameMatch {get; set;}
    public String idMatch {get; set;}
    public String personType {get; set;}
    public Boolean openTraining {get; set;}
    public Boolean isComplete {get; set;}
    public String companyId {get; set;}
    public List<Account> parent {get; set;}
    public List<Account> child {get; set;}
    private String condition {get; set;}
    private String query {get; set;}

    public EnrollmentContactLookupWindow() {
        newMode = false;
        editMode = false;
        isComplete = false;
        newContact = new Contact();
        personType = Apexpages.currentPage().getParameters().get('personType');
        companyId = Apexpages.currentPage().getParameters().get('company');
        openTraining = Boolean.valueOf(Apexpages.currentPage().getParameters().get('openTraining'));

        if (personType != null) {
            if (personType == TYPE_PARTICIPANT) {
                rt = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE);
                condition = 'RecordTypeId != \'' + CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS) + '\'';
            }
            else if (personType == TYPE_CONTACT_PERSON) {
                rt = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CONTACTPERSON);
                condition = '(RecordTypeId = \'' + rt + '\' OR Contact_Person__c = true) AND RecordTypeId != \''
                        + CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS) + '\'';
            }

            if (String.isNotBlank(companyId)) {
                condition += ' AND (AccountId = \'' + companyId + '\'';
                parent = [SELECT Id, ParentId FROM Account WHERE Id = :companyId LIMIT 1];
                child = [SELECT Id FROM Account WHERE ParentId = :companyId];
                if (!parent.isEmpty() && parent[0].ParentId != null) {
                    condition += ' OR AccountId = \'' + parent[0].ParentId + '\'';
                }
                for(Account acc: child) {
                    condition += ' OR AccountId = \'' + acc.Id + '\'';
                }
                condition += ')';
            }

            query = 'SELECT Id, Name, Account.Name, RecordType.Name, Email, Phone, AccountId, Account.Parent.Name FROM Contact WHERE ' + condition;

            try {
                contactList = Database.Query(query + ' LIMIT 100');
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
        }
    }

    public void searchLookup() {
        String currentQuery;
        if (!String.isBlank(searchTxt)) {
            searchTxt = String.escapeSingleQuotes(searchTxt.remove('*'));
            List<String> splitQueries = searchTxt.split(' ');
            if (splitQueries.isEmpty()) {
                splitQueries.add(searchTxt);
            }

            String whereString = '';
            for (Integer i = 0; i < splitQueries.size(); i++) {
                whereString += ' AND (FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
            }

            currentQuery = query + whereString;
        } else {
            currentQuery = query;
        }

        try {
            contactList = Database.Query(currentQuery + ' LIMIT 100');
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    public void checkContact() {
        editContact = [SELECT Id, Foreigner__c, FirstName, LastName, Email, Phone, MobilePhone, MailingStreet, MailingCity,
                MailingPostalCode, Position__c, Department_Business_Area__c
        FROM Contact
        WHERE Id = :idMatch];

        //check data completion
        if (contactDataIsComplete(editContact)) {
            isComplete = true;
        } else {
            showDataCompletionError();
            isComplete = false;
            editMode = true;
        }
    }

    public void changeNewMode() {
        newMode = !newMode;
    }

    public void cancelEdit() {
        editMode = false;
    }

    public void updateContact() {
        //check data completion
        if (contactDataIsComplete(editContact)) {
            try {
                update editContact;
                nameMatch = editContact.FirstName + ' ' + editContact.LastName;
                idMatch = editContact.Id;
                isComplete = true;
            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        } else {
            showDataCompletionError();
            isComplete = false;
            editMode = true;
        }
    }

    private Boolean contactDataIsComplete(Contact c) {
        if (!openTraining || personType == TYPE_CONTACT_PERSON) {
            //closed training or contact person
            if (c.FirstName != null && c.LastName != null && (c.Phone != null || c.MobilePhone != null || c.Email != null)) {
                return true;
            }
        } else {
            //open training individual
            if (companyId == null && c.FirstName != null && c.LastName != null
                    && (c.Phone != null || c.MobilePhone != null || c.Email != null)
                    && c.MailingStreet != null && c.MailingCity != null && c.MailingPostalCode != null) {
                return true;
            }
            //open training with company
            if (companyId != null && c.FirstName != null && c.LastName != null
                    && (c.Phone != null || c.MobilePhone != null || c.Email != null)) {
                return true;
            }
        }
        return false;
    }

    public void newRecord() {
        newContact.RecordTypeId = rt;
        if (String.isNotBlank(companyId)) { newContact.AccountId = companyId; }
        changeNewMode();
    }

    public void insertContact() {
        //check data completion
        if (contactDataIsComplete(newContact)) {
            try {
                CommonUtility.preventChangingEmailTwice = true;
                if (newContact.AccountId != null) {
                    insert newContact;
                    nameMatch = newContact.FirstName + ' ' + newContact.LastName;
                    idMatch = newContact.Id;
                }
                nameMatch = newContact.FirstName + ' ' + newContact.LastName;
                isComplete = true;

            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        } else {
            showDataCompletionError();
            isComplete = false;
        }
    }

    private void showDataCompletionError() {
        ApexPages.Message msg;
        if (!openTraining || personType == TYPE_CONTACT_PERSON) {
            msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_ReqFieldsEnrollmentClosedOrCP);
        } else {
            if (companyId == null) {
                msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_ReqFieldsEnrollmentOpenInd);
            } else {
                msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_ReqFieldsEnrollmentOpenCom);
            }
        }
        ApexPages.addMessage(msg);
    }

}