/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to iPresso Activity Business Logic
**/

public without sharing class ActivityManager {

	public static Map<String, String> effectOfTalkingWithCCMap = new Map<String, String> {
		'Closure - no contact'=> 'Zamknięcie - Brak kontaktu',
		'Consent Withdrawn'=> 'Ponowny kontakt',
		'Contact again'=> 'Ponowny kontakt',
		'Correctional school'=> 'Matura poprawkowa',
		'Defense at another time'=> 'Obrona w innym terminie',
		'Duplicate in the base'=> 'Dubel w bazie',
		'Email'=> 'Mail',
		'Lead to the Sales Department'=> 'Lead do działu sprzedaży',
		'No permission to register'=> 'Brak zgody na rejestrację',
		'Request to remove from the database'=> 'Prośba o usunięcie z bazy',
		'Saved'=> 'Zapisany',
		'The Declaration is Lost'=> 'Deklaracja ZPI',
		'Uninterested'=> 'Niezainteresowany',
		'Voicemail'=> 'Poczta głosowa',
		'Written in zpi'=> 'Zapisany w ZPI',
		'Wrong Contact'=> 'Błędny kontakt',
		'ZPI by the consultant' => 'ZPI przez konsultanta',
		'ZPI Link'=> 'Link do ZPI'
	};

	public static Map<string, String> effectOfContactMap = new Map<String, String> {
		'Interested' => 'zainteresowany',
		'Neutral' => 'neutralny',
		'Disinterest' => 'brak zainteresowania'
	};

	public static Map<String, String> categoryOFContactMap = new Map<String, String> {
		'WSB Enrollment Office' => 'Biuro rekrutacji',
		'Educational Advisor' => 'doradca edukacyjny',
		'MBA Office' => 'Biuro Programu MBA'
	};

	public static Map<String, String> contactChannelMap = new Map<String, String> {
		'Personal Visit' => 'wizyta osobista',
		'Phone' => 'telefon',
		'Metting in the Company' => 'spotkanie w firmie'
	};

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- LACK OF DOCUMENTS ACTIVITY --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

	/* Helper method for ActivityLackOfDocumentsBatch for sending iPresso Activity  */
    public static void lackOfDocumentsActivity(List<Enrollment__c> studyEnrollments) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();

		Set<Id> contactIds = new Set<Id>();
		for (Enrollment__c studyEnrollment : studyEnrollments) {
			contactIds.add(studyEnrollment.Candidate_Student__c);
		}

		Map<Id, Contact> contacts = new Map<Id, Contact>([
			SELECT Id,
			(SELECT Id, iPressoId__c FROM iPressoContacts__r WHERE iPressoId__c != null)
			FROM Contact
			WHERE Id IN :contactIds
		]);

		for (Enrollment__c studyEnrollment : studyEnrollments) {
			Contact contactWithData = contacts.get(studyEnrollment.Candidate_Student__c);
			for (Marketing_Campaign__c contact : contactWithData.iPressoContacts__r) {
				Boolean ifFullDocumentation = true;
				if (studyEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_CONFIRMED || studyEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_COD) {
					ifFullDocumentation = false;
				}

				iPressoActivities.add(prepareActivityForLackOfDocuments(String.valueOf(ifFullDocumentation), contact));
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListSync(JSON.serialize(iPressoActivities));
		}
    }

	/* Helper method for resignationOnEnrollmentActivity to prepare iPresso Activity on Enrollment Resignation */
	public static IntegrationIPressoSendActivities.IPressoActivityContact_JSON prepareActivityForLackOfDocuments(String ifFullDocumentation, Marketing_Campaign__c contact) {
		IntegrationIPressoSendActivities.IPressoParameters_JSON fullDocumentation = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
			CommonUtility.ACTIVITY_FULL_DOCUMENTATION, 
			ifFullDocumentation
		);

		List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { fullDocumentation };

		IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
			CommonUtility.ACTIVITY_COLLECTION_OF_DOCUMENTS, 
			System.now(), 
			parameters
		);

		return new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(contact.iPressoId__c, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity });
	}


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- INFORMATOR -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static void informatorActivity(Id iPressoCampaignId) {	
    	Marketing_Campaign__c iPressoCampaign = [
    		SELECT Id, Informator_Name__c, Informator_City__c
    		FROM Marketing_Campaign__c
    		WHERE Id = :iPressoCampaignId
    	];

    	List<Marketing_Campaign__c> iPressoMembers = [
    		SELECT Id, Synchronization_Status__c, Campaign_Contact__c
    		FROM Marketing_Campaign__c
    		WHERE Campaign_Member__c = :iPressoCampaignId
    		AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
    	];

    	Set<Id> contactIds = new Set<Id>();
    	for (Marketing_Campaign__c iPressoMember : iPressoMembers) {
    		contactIds.add(iPressoMember.Campaign_Contact__c);
    	}

    	List<Marketing_Campaign__c> iPressoContacts = [
    		SELECT Id, iPressoId__c
    		FROM Marketing_Campaign__c
    		WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
    		AND Contact_from_iPresso__c IN :contactIds
    		AND iPressoId__c != null
    	];

        Map<String, String> translatedPickListValues = DictionaryTranslator.getTranslatedPicklistValues('PL', Marketing_Campaign__c.Informator_Name__c);

		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();
    	for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
			IntegrationIPressoSendActivities.IPressoParameters_JSON nameOfInformator = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
				CommonUtility.ACTIVITY_INFORMATOR_NAME, 
				translatedPickListValues.get(iPressoCampaign.Informator_Name__c)
			);

			IntegrationIPressoSendActivities.IPressoParameters_JSON cityOfInformator = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
				CommonUtility.ACTIVITY_INFORMATOR_CITY, 
				String.valueOf(iPressoCampaign.Informator_City__c)
			);

			List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { nameOfInformator, cityOfInformator };

			IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
				CommonUtility.ACTIVITY_INFORMATOR, 
				System.now(), 
				parameters
			);

			iPressoActivities.add(new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(iPressoContact.iPressoId__c, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity }));

    	}

    	if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAndUpdateRecordsAtFuture(iPressoCampaignId, JSON.serialize(iPressoActivities));
		}
    }

    public static void updateSyncStatus_onFinish(Id iPressoCampaignId, Boolean success) {
    	List<Marketing_Campaign__c> iPressoMembers = [
    		SELECT Id, Synchronization_Status__c, Campaign_Contact__c
    		FROM Marketing_Campaign__c
    		WHERE Campaign_Member__c = :iPressoCampaignId
    		AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
    	];

        for (Marketing_Campaign__c iPressoMember : iPressoMembers) {
            iPressoMember.Synchronization_Status__c = success ? CommonUtility.SYNCSTATUS_FINISHED : CommonUtility.SYNCSTATUS_FAILED;
        }

        try {
            update iPressoMembers;
        }
        catch (Exception e){
            ErrorLogger.log(e);
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- ATTENDANCE AT THE EVENT ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Method used to send Activity to iPresso containing information about attendace at given event (created manually from TASK) */
    public static void attendaceAtTheEventActivity(List<Task> attendaceAtEventActivities) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();
		Map<Id, Contact> contactMap = prepareMapOfIpressoContacts(attendaceAtEventActivities);

    	for (Task attendaceAtEvent : attendaceAtEventActivities) {
    		Contact contactWithData = contactMap.get(attendaceAtEvent.WhoId);
    		if (contactWithData != null) {
	    		for (Marketing_Campaign__c iPressoContact : contactWithData.iPressoContacts__r) {
					IntegrationIPressoSendActivities.IPressoParameters_JSON eventName = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
						CommonUtility.ACTIVITY_EVENT_NAME, 
						attendaceAtEvent.Event_Name__c
					);

					IntegrationIPressoSendActivities.IPressoParameters_JSON eventDate = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
						CommonUtility.ACTIVITY_DATE_TIME, 
						String.valueOf(attendaceAtEvent.ActivityDate)
					);

					List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { eventName, eventDate };

					IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
						CommonUtility.ACTIVITY_ATTENDACE_AT_EVENT, 
						attendaceAtEvent.ActivityDate, 
						attendaceAtEvent.Time_of_Event__c , 
						parameters
					);

					iPressoActivities.add(new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(
						iPressoContact.iPressoId__c, 
						new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity })
					);
				}
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAtFuture(JSON.serialize(iPressoActivities));
		}
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- CONTACT ACTIVITY ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Method used to send Activity to iPresso containing information about changes related to contact (created manually from TASK) */
    public static void contactActivity(List<Task> contactAcitivities) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();
		Map<Id, Contact> contactMap = prepareMapOfIpressoContacts(contactAcitivities);

    	for (Task contactActivity : contactAcitivities) {
    		Contact contactWithData = contactMap.get(contactActivity.WhoId);
    		if (contactWithData != null) {
	    		for (Marketing_Campaign__c iPressoContact : contactWithData.iPressoContacts__r) {
					IntegrationIPressoSendActivities.IPressoParameters_JSON effectOfContact = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
						CommonUtility.ACTIVITY_CONTACT_EFFECT, 
						effectOfContactMap.get(contactActivity.Effect_of_Contact__c)
					);

					IntegrationIPressoSendActivities.IPressoParameters_JSON categoryOfContact = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
						CommonUtility.ACTIVITY_CONTACT_CATEGORY, 
						categoryOFContactMap.get(contactActivity.Category_of_Contact__c)
					);

					IntegrationIPressoSendActivities.IPressoParameters_JSON contactChannel = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
						CommonUtility.ACTIVITY_CONTACT_CHANNEL, 
						contactChannelMap.get(contactActivity.Contact_Channel__c)
					);

					List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { 
						effectOfContact, categoryOfContact, contactChannel 
					};

					IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
						CommonUtility.ACTIVITY_CONTACT, 
						contactActivity.ActivityDate,  
						contactActivity.Time_of_Event__c, 
						parameters
					);

					iPressoActivities.add(new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(iPressoContact.iPressoId__c, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity }));
				}
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAtFuture(JSON.serialize(iPressoActivities));
		}
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- MARKETING PHONE CONTACT ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Method used to send Activity to iPresso containing information about contacting candidate by CC (created manually from TASK) */
    public static void marketingPhoneContactActivity(List<TasK> marketingPhoneContactActivities) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();
		Map<Id, Contact> contactMap = prepareMapOfIpressoContacts(marketingPhoneContactActivities);

    	for (Task marketingPhoneContact : marketingPhoneContactActivities) {
    		Contact contactWithData = contactMap.get(marketingPhoneContact.WhoId);
    		if (contactWithData != null) {
	    		for (Marketing_Campaign__c iPressoContact : contactWithData.iPressoContacts__r) {

					IntegrationIPressoSendActivities.IPressoParameters_JSON effectOfCCTalk = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
					 	CommonUtility.ACTIVITY_EFFECT_OF_CC_TALK, 
					 	effectOfTalkingWithCCMap.get(marketingPhoneContact.Effect_of_Talking_with_CC__c)
					 );

					List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { effectOfCCTalk };

					IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
						CommonUtility.ACTIVITY_MARKETING_PHONE, 
						marketingPhoneContact.ActivityDate, 
						marketingPhoneContact.Time_of_Event__c, 
						parameters
					);

					iPressoActivities.add(new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(iPressoContact.iPressoId__c, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity }));
				}
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAtFuture(JSON.serialize(iPressoActivities));
		}
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- RESIGNATION ON ENROLLMENT ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

	public static void resignationOnEnrollmentActivity(List<Enrollment__c> studyEnrollments) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();

		Set<Id> contactIds = new Set<Id>();
		for (Enrollment__c studyEnrollment : studyEnrollments) {
			contactIds.add(studyEnrollment.Candidate_Student__c);
		}

		Map<Id, Contact> contacts = new Map<Id, Contact>([
			SELECT Id,
			(SELECT Id, iPressoId__c FROM iPressoContacts__r WHERE iPressoId__c != null)
			FROM Contact
			WHERE Id IN :contactIds
		]);

		for (Enrollment__c studyEnrollment : studyEnrollments) {
			Contact contactWithData = contacts.get(studyEnrollment.Candidate_Student__c);
			for (Marketing_Campaign__c contact : contactWithData.iPressoContacts__r) {
				iPressoActivities.add(prepareActivityForResignation(studyEnrollment, contact));
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAtFuture(JSON.serialize(iPressoActivities));
		}
	}

	/* Helper method for resignationOnEnrollmentActivity to prepare iPresso Activity on Enrollment Resignation */
	public static IntegrationIPressoSendActivities.IPressoActivityContact_JSON prepareActivityForResignation(Enrollment__c studyEnrollment, Marketing_Campaign__c contact) {
		IntegrationIPressoSendActivities.IPressoParameters_JSON resignationReason = new IntegrationIPressoSendActivities.IPressoParameters_JSON(CommonUtility.ACTIVITY_RESIGNATION_REASON, studyEnrollment.Resignation_Reason_List__c);

		List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { resignationReason };

		IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
			CommonUtility.ACTIVITY_RESINGATION, 
			studyEnrollment.Resignation_Date__c, 
			studyEnrollment.LastModifiedDate, 
			parameters
		);

		return new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(contact.iPressoId__c, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity });
	}


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------- STATUS CHANGE ON CONTACT -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

	/* 
		Method used to send activity to iPresso when Minor Status (Prospect, MQL, SAL, SQL) is changed or Main Status is changed (Candidate, Interested).
		Parameter activityName is responsible for determining which Status is supposed to be taken in consideration.
	*/
	public static void changeStatusOnContactActivity(List<ChangeStatusActivity> changeStatusActivities, String activityName, String previousStatusName, String newStatusName) {
		List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();

		Set<Id> contactIds = new Set<Id>();
		for (ChangeStatusActivity changeStatusActivity : changeStatusActivities) {
			contactIds.add(changeStatusActivity.contactId);
		}

		Map<Id, Contact> contacts = new Map<Id, Contact>([
			SELECT Id,
			(SELECT Id, iPressoId__c FROM iPressoContacts__r WHERE iPressoId__c != null)
			FROM Contact
			WHERE Id IN :contactIds
		]);

        Map<String, String> statusesMap = new Map<String, String> {
            'Interested' => 'zainteresowany',
            'Candidate' => 'kandydat',
            'Accepted' => 'przyjęty',
            'Student' => 'Student',
            'Graduate' => 'absolwent',
            'Foreigner' => 'Obcokrajowiec',
            'Duplicate' => 'Duplikat'
        };

		for (ChangeStatusActivity changeStatusActivity : changeStatusActivities) {
			Contact contactWithData = contacts.get(changeStatusActivity.contactId);
			for (Marketing_Campaign__c contact : contactWithData.iPressoContacts__r) {
				iPressoActivities.add(prepareActivityForChangeStatus(contact.iPressoId__c, changeStatusActivity, activityName, translateStatusToPL(previousStatusName, statusesMap), translateStatusToPL(newStatusName, statusesMap)));
			}
		}

		if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
			IntegrationIPressoSendActivities.sendActivitiesListAtFuture(JSON.serialize(iPressoActivities));			
		}
	}

	/* Helper method for changeStatusOnContactActivity to create iPresso Activity Contact Object */
	public static IntegrationIPressoSendActivities.IPressoActivityContact_JSON prepareActivityForChangeStatus(String iPressoContactId, ChangeStatusActivity changeStatusActivity, String activityName, String previousStatusName, String newStatusName) {

		IntegrationIPressoSendActivities.IPressoParameters_JSON previousStatus = new IntegrationIPressoSendActivities.IPressoParameters_JSON(
			previousStatusName, 
			changeStatusActivity.oldStatus
		);

		IntegrationIPressoSendActivities.IPressoParameters_JSON newStatus = new IntegrationIPressoSendActivities.IPressoParameters_JSON(newStatusName, changeStatusActivity.newStatus);
		List<IntegrationIPressoSendActivities.IPressoParameters_JSON> parameters = new List<IntegrationIPressoSendActivities.IPressoParameters_JSON> { previousStatus, newStatus };

		IntegrationIPressoSendActivities.IPressoActivity_JSON activity = new IntegrationIPressoSendActivities.IPressoActivity_JSON(
			activityName, 
			changeStatusActivity.changeOfStatusDate, 
			parameters
		);

		return new IntegrationIPressoSendActivities.IPressoActivityContact_JSON(iPressoContactId, new List<IntegrationIPressoSendActivities.IPressoActivity_JSON> { activity });
	}

    public static String translateStatusToPL(String text, Map<String, String> statusesMap) {
        for (String value : statusesMap.keySet()) {
            text = text.replaceAll(value, statusesMap.get(value));
        }

        return text;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Method used to retrieve map of contacts for given list of tasks */
    public static Map<Id, Contact> prepareMapOfIpressoContacts(List<Task> listOfTasksOnContacts) {
		Set<Id> contactId = new Set<Id>();
		
		for (Task task : listOfTasksOnContacts) {
			contactId.add(task.WhoId);
		}

		Map<Id, Contact> contacts = new Map<Id, Contact>([
			SELECT Id,
			(SELECT Id, iPressoId__c FROM iPressoContacts__r)
			FROM Contact
			WHERE Id IN :contactId
		]);

		return contacts;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- MODEL CLASSES ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class ChangeStatusActivity {
    	public Id contactId;
    	public String oldStatus;
    	public String newStatus;
    	public DateTime changeOfStatusDate;

    	public ChangeStatusActivity(Id contactId, String oldStatus, String newStatus, DateTime changeOfStatusDate) {
    		this.contactId = contactId;
    		this.oldStatus = oldStatus;
    		this.newStatus = newStatus;
    		this.changeOfStatusDate = changeOfStatusDate;
    	}
    }
}