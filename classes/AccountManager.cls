/**
* @author       Sebastian Łasisz123456789
* @description  Class for storing methods related to Account object
**/
public without sharing class AccountManager {

    /**
     * @author Wojciech Słodziak
     *
     * check duplicate Accounts by NIP
     * @param accsToCheckDuplicates List of accounts to look for duplicates in NIP.
     */
    public static void checkDuplicateNIP(List<Account> accsToCheckDuplicates) {
        Set<String> nipList = new Set<String>();
        for (Account a : accsToCheckDuplicates) {
            nipList.add(a.NIP__c);
        }
        List<Account> potentialDuplicates = [SELECT Id, Name, NIP__c FROM Account WHERE NIP__c IN :nipList];

        for (Account a: accsToCheckDuplicates) {
            for (Account dup : potentialDuplicates) {
                if (a.NIP__c == dup.NIP__c && a.NIP__c != CommonUtility.NULL_NIP) {
                    if(!CommonUtility.isLightningEnabled()) {
                        a.NIP__c.addError(' ' + Label.msg_error_DupFound + ': <a target="_blank" href="/' + dup.Id + '">' + dup.Name + '</a>', false);
                    } else {
                        a.NIP__c.addError(' ' + Label.msg_error_DupFound + dup.Id + ' - ' + dup.Name, false);
                    }
                }
            }
        }
    }
    /**
     * @author Sebastian Lasisz
     *
     * Rewrite University for sharing from Contact to Account.
     * @param accountIds Set of account Ids to recalculate University for Sharing which is responsible for write access on Account/Contact.
     */
    public static void updateUniversityForSharing(Set<Id> accountIds) {
        List<Account> accountsToUpdate = [
                SELECT Id, TECH_University_for_sharing__c, (SELECT Id, University_for_sharing__c FROM Contacts)
                FROM Account
                WHERE Id IN :accountIds
        ];

        for (Account account : accountsToUpdate) {
            Set<String> universityForSharingFromAcc = new Set<String>();
            if (account.TECH_University_for_sharing__c != null) {
                universityForSharingFromAcc.addAll(account.TECH_University_for_sharing__c.split(', '));
            }

            for (Contact contact : account.Contacts) {
                if (contact.University_for_sharing__c != null) {
                    universityForSharingFromAcc.addAll(contact.University_for_sharing__c.split(', '));
                }
            }

            if (!universityForSharingFromAcc.isEmpty()) {
                String finalUniversityForSharing = '';
                for (String university : universityForSharingFromAcc) {
                    finalUniversityForSharing += university + ', ';
                }

                account.TECH_University_for_sharing__c = finalUniversityForSharing.left(finalUniversityForSharing.length() - 2);
            }
        }

        try {
            update accountsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /**
     * @author Sebastian Lasisz
     *
     * Calculate status based on incoming scoring.     *
     * @param scoring   Integer value needed to set proper status on Contact / Person Account.
     *
     * @return  Contact / Person Account Status based on scoring (calculated based on iPresso).
     */
    public static String recalculateStatusBasedOnScoring(Integer scoring) {
        if (scoring >= 0 && scoring <= 99) {
            return CommonUtility.CONTACT_STATUS_PROSPECT;
        } else if (scoring >= 100 && scoring <= 199) {
            return CommonUtility.CONTACT_STATUS_MQL;
        } else if (scoring >= 200 && scoring <= 299) {
            return CommonUtility.CONTACT_STATUS_SAL;
        } else if (scoring >= 300 && scoring <= 399) {
            return CommonUtility.CONTACT_STATUS_SQL;
        }

        return null;
    }

    /**
     * @author  Sebastian Lasisz
     *
     * Calculate birthdate based on pesel number.
     * @param pesel Pesel number of Contact/Person Account.
     *
     * @return  Birthdate of Contact/Person Account.
     */
    public static Date calculateBirthdate(String pesel) {
        Integer twoDecimalMonth = Integer.valueOf(pesel.substring(2, 4));
        Integer twoDecimalYear = Integer.valueOf(pesel.substring(0, 2));
        Integer fullDecimalYear;
        Integer fullDecimalMonth;
        if (twoDecimalMonth <= 12) {
            fullDecimalYear = twoDecimalYear + 1900;
            fullDecimalMonth = twoDecimalMonth;
        } else if (twoDecimalMonth <= 32) {
            fullDecimalYear = twoDecimalYear + 2000;
            fullDecimalMonth = twoDecimalMonth - 20;
        } else if (twoDecimalMonth <= 52) {
            fullDecimalYear = twoDecimalYear + 2100;
            fullDecimalMonth = twoDecimalMonth - 40;
        } else if (twoDecimalMonth <= 72) {
            fullDecimalYear = twoDecimalYear + 2200;
            fullDecimalMonth = twoDecimalMonth - 60;
        } else if (twoDecimalMonth <= 92) {
            fullDecimalYear = twoDecimalYear + 1800;
            fullDecimalMonth = twoDecimalMonth - 80;
        }

        return Date.newInstance(fullDecimalYear, fullDecimalMonth, Integer.valueOf(pesel.substring(4, 6)));
    }
}