/**
* @author       Wojciech Słodziak
* @description  Class to send emails from triggers (NOTE: it is set to without sharing)
**/

public without sharing class EmailManager {

    private final static List<EmailTemplate> emailTemplates = [SELECT Id, DeveloperName FROM EmailTemplate];
    private static Map<String, EmailTemplate> devNameToETMap;

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private enum WorfklowEmailNames {
        X22_StudyConf,
        X22_MBA_StudyConf,
        X22_SP_StudyConf,

        X21_StudyConf,
        X21_MBA_StudyConf,
        X21_SP_StudyConf
    }

    private static String designateWorkflowNameForEnrollment_21(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U) {
            workflow = WorfklowEmailNames.X21_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            workflow = WorfklowEmailNames.X21_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            workflow = WorfklowEmailNames.X21_SP_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            workflow = WorfklowEmailNames.X21_MBA_StudyConf;
        }

        return workflow != null? (language + '_' + workflow.name()) : null;
    }

    private static String designateWorkflowNameForEnrollment_22(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U) {
            workflow = WorfklowEmailNames.X22_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            workflow = WorfklowEmailNames.X22_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            workflow = WorfklowEmailNames.X22_SP_StudyConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            workflow = WorfklowEmailNames.X22_MBA_StudyConf;
        }

        return workflow != null? (language + '_' + workflow.name()) : null;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- TEMPLATES --------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    public static final String EMAIL_TEMPLATE_SEATS_LIMIT_EXCEEDED = 'Osi_gni_ty_limit_miejsc_vf';
    public static final String EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT = 'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci';
    public static final String EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_11 = 'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci_do_11_dni_przed';
    public static final String EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_10_3 = 'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci_10_3_dni_przed';
    public static final String EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_2_1 = 'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci_2_1_dni_przed';


    public static final String EMAIL_TEMPLATE_LIMIT_EXCEEDED_COURSE = 'X24_c_CourseSeatLimitExceeded';
    public static final String EMAIL_TEMPLATE_LIMIT_EXCEEDED_SPEC = 'X24_s_SpecSeatLimitExceeded';


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- TRAININGS --------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static void sendOverlimitEmailToTrainingAdmin(Set<Id> scheduleIdList) {
        List<EmailTemplate> et = [SELECT Id, Body, Subject FROM EmailTemplate WHERE IsActive = true AND DeveloperName = :EmailManager.EMAIL_TEMPLATE_SEATS_LIMIT_EXCEEDED];
        List<OrgWideEmailAddress> owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :CommonUtility.INTEGRATION_USERNAME];

        List<Offer__c> schedules = [SELECT Id, Training_Offer_from_Schedule__r.Training_Administrator__c FROM Offer__c WHERE Id IN: scheduleIdList];
        if (!et.isEmpty()) {
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(Offer__c schedule : schedules) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                mail.setTargetObjectId(schedule.Training_Offer_from_Schedule__r.Training_Administrator__c);
                mail.saveAsActivity = false;
                mail.setWhatId(schedule.Id);
                mail.setTemplateId(et[0].Id);
                if (!owa.isEmpty()) {
                    mail.setOrgWideEmailAddressId(owa[0].Id);
                }
                mails.add(mail);                     
            }
            try {
                if (!mails.isEmpty()) {
                    Messaging.sendEmail(mails);
                }
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        } else {
            ErrorLogger.configMsg(CommonUtility.ET_UNDEFINED_ERROR + ' ' + EmailManager.EMAIL_TEMPLATE_SEATS_LIMIT_EXCEEDED);
        }
    }

    public static void sendPaymentDataToParticipants(Set<Id> enrollmentIdList) {
        Map<Id, Messaging.SingleEmailMessage> mails = new Map<Id, Messaging.SingleEmailMessage>();
        List<EmailTemplate> et11 = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_11];
        List<EmailTemplate> et10_3 = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_10_3];
        List<EmailTemplate> et2_1 = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_2_1];
        List<OrgWideEmailAddress> owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :CommonUtility.INTEGRATION_USERNAME];

        if (!et11.isEmpty() && !et10_3.isEmpty() && !et2_1.isEmpty()) {
            List<Enrollment__c> enrollmentList = [
                SELECT Id, Participant__c, Participant__r.Name, Enrollment_Training_Participant__c, Training_Offer_for_Participant__r.Valid_From__c 
                FROM Enrollment__c 
                WHERE Training_Offer_for_Participant__r.RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                AND Enrollment_Training_Participant__r.Reporting_Person__c = null AND Company_from_Participant_Result__c = null 
                AND Participant__r.Email != null AND Id IN :enrollmentIdList AND Training_Offer_for_Participant__r.Valid_From__c > TODAY
            ];

            if (enrollmentList.isEmpty()) {
                return;
            }

            for(Enrollment__c enrollment : enrollmentList) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
                mail.setTargetObjectId(enrollment.Participant__c);
                mail.setSaveAsActivity(false);
                mail.setWhatId(enrollment.Enrollment_Training_Participant__c);

                Integer dayDiff = System.today().daysBetween(enrollment.Training_Offer_for_Participant__r.Valid_From__c);
                if (dayDiff >= 11) {
                    mail.setTemplateId(et11[0].Id);
                } else if (dayDiff >= 3) {
                    mail.setTemplateId(et10_3[0].Id);
                } else if (dayDiff >= 1) {
                    mail.setTemplateId(et2_1[0].Id);
                }


                if (!owa.isEmpty()) {
                    mail.setOrgWideEmailAddressId(owa[0].Id);
                }
                mails.put(enrollment.Id, mail);        
            }

            try {
                if (!mails.isEmpty()) {
                    Messaging.sendEmail(mails.values());
                }

                List<Task> TasksToInsert = new List<Task>();
                for(Enrollment__c enrollment : enrollmentList) {
                    TasksToInsert.add(createEmailTask(
                        Label.title_EventSentPaymentDataTo + ' ' + enrollment.Participant__r.Name,
                        enrollment.Id,
                        enrollment.Participant__c,
                        mails.get(enrollment.Id).getPlainTextBody().replaceAll('\n', '</br>'),
                        null
                    ));
                }
                insert TasksToInsert;

            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        } else {
            ErrorLogger.configMsg(CommonUtility.ET_UNDEFINED_ERROR + ' ' + EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_11 + ' OR ' 
                                  + EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_10_3 + ' OR ' 
                                  + EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_2_1);
        }
    }

    
    @Future /* @Future required to wait for Enrollment Value calculations in trigger */
    public static void sendPaymentDataToReportingPerson(Set<Id> enrollmentIdList) {
        Map<Id, Messaging.SingleEmailMessage> mails = new Map<Id, Messaging.SingleEmailMessage>();
        List<EmailTemplate> et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT];
        List<OrgWideEmailAddress> owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :CommonUtility.INTEGRATION_USERNAME];

        List<Enrollment__c> enrollmentList = [SELECT Id, Reporting_Person__c FROM Enrollment__c WHERE Id IN :enrollmentIdList AND Reporting_Person__r.Email != null];

        if (!et.isEmpty()) {
            for(Enrollment__c enrollment : enrollmentList) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(enrollment.Reporting_Person__c);
                mail.setSaveAsActivity(false);
                mail.setWhatId(enrollment.Id);
                mail.setTemplateId(et[0].Id);
                if (!owa.isEmpty()) {
                    mail.setOrgWideEmailAddressId(owa[0].Id);
                }
                mails.put(enrollment.Id, mail);        
            }
            try {
                if (!mails.isEmpty()) {
                    Messaging.sendEmail(mails.values());
                }

                List<Task> TasksToInsert = new List<Task>();

                for(Enrollment__c enrollment : enrollmentList) {
                    TasksToInsert.add(createEmailTask(
                        Label.title_EventSentPaymentDataToRepPerson,
                        enrollment.Id,
                        enrollment.Reporting_Person__c,
                        mails.get(enrollment.Id).getPlainTextBody().replaceAll('\n', '</br>'),
                        null
                    ));
                }
                insert TasksToInsert;


            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        } else {
            ErrorLogger.configMsg(CommonUtility.ET_UNDEFINED_ERROR + ' ' + EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT);
        }
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- STUDIES ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static void sendVerificationLink(Set<Id> enrollmentIdList) {
        sendVerificationLinkFinal(false, enrollmentIdList);
    }

    // sending verification link by email
    public static Boolean sendVerificationLinkFinalFromButton(Boolean skipVerificationCount, Set<Id> enrollmentIdList) {
        Boolean result = true;

        List<Enrollment__c> enrollmentList = [
            SELECT Id, Candidate_Student__c, VerificationLinkEmailedCount__c, Degree__c, VerificationString__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
        ];

        if (devNameToETMap == null) {
            devNameToETMap = new Map<String, EmailTemplate>();
            for (EmailTemplate et : emailTemplates) {
                devNameToETMap.put(et.DeveloperName, et);
            }
        }

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName;
            String workflowNameForCopy;

            String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
            if (enrollment.VerificationLinkEmailedCount__c == 0 || skipVerificationCount) {
                workflowTriggerName = designateWorkflowNameForEnrollment_21(enrollment);
                workflowNameForCopy = language + '_' + (language == 'PL' ? 'X21_D_StudyConf' : 'X21_StudyConf');
            } else {
                workflowTriggerName = designateWorkflowNameForEnrollment_22(enrollment);
                workflowNameForCopy = language + '_' + 'X22_StudyConf';
            }

            recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
            
            enrollment.VerificationLinkEmailedCount__c++;
            enrollment.VerificationLinkEmailedDate__c = System.today();
            
            tasksToInsert.add(createEmailTask(
                Label.title_EventSentVerificationMail + (enrollment.VerificationLinkEmailedCount__c > 1? ' (' 
                    + enrollment.VerificationLinkEmailedCount__c + ')' : ''),
                enrollment.Id,
                enrollment.Candidate_Student__c,
                prepareFakeEmailBodyWithoutVerificationLink(devNameToETMap.get(workflowNameForCopy).Id, enrollment.Id, enrollment.VerificationString__c),
                null
            ));
        }
        
        try {
            insert tasksToInsert;
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            result = false;
        }
        
        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }

        return result;
    }

    // sending verification link by email
    public static void sendVerificationLinkFinal(Boolean skipVerificationCount, Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Candidate_Student__c, VerificationLinkEmailedCount__c, Degree__c, VerificationString__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
        ];

        if (devNameToETMap == null) {
            devNameToETMap = new Map<String, EmailTemplate>();
            for (EmailTemplate et : emailTemplates) {
                devNameToETMap.put(et.DeveloperName, et);
            }
        }

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName;
            String workflowNameForCopy;

            String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
            if (enrollment.VerificationLinkEmailedCount__c == 0 || skipVerificationCount) {
                workflowTriggerName = designateWorkflowNameForEnrollment_21(enrollment);
                workflowNameForCopy = language + '_' + (language == 'PL' ? 'X21_D_StudyConf' : 'X21_StudyConf');
            } else {
                workflowTriggerName = designateWorkflowNameForEnrollment_22(enrollment);
                workflowNameForCopy = language + '_' + 'X22_StudyConf';
            }

            recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
            
            enrollment.VerificationLinkEmailedCount__c++;
            enrollment.VerificationLinkEmailedDate__c = System.today();
            
            tasksToInsert.add(createEmailTask(
                Label.title_EventSentVerificationMail + (enrollment.VerificationLinkEmailedCount__c > 1? ' (' 
                    + enrollment.VerificationLinkEmailedCount__c + ')' : ''),
                enrollment.Id,
                enrollment.Candidate_Student__c,
                prepareFakeEmailBodyWithoutVerificationLink(devNameToETMap.get(workflowNameForCopy).Id, enrollment.Id, enrollment.VerificationString__c),
                null
            ));
        }
        
        try {
            insert tasksToInsert;
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
        
        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }
    }


    // email sending must be transfered to Integration user context, because of problems with Verification Site User nad Visualforce Templates
    public static void sendOverlimitEmailToCourseAdmin(Set<Id> offerIdList) {
        if (CommonUtility.isVerificationSiteUser()) {
            TransferJobToDefaultUserEmService.tranfserJob(TransferJobToDefaultUserEmService.Transferable.COURSE_OVERLIMIT_EMAIL, offerIdList);
        } else {
            EmailManager.sendOverlimitEmailToCourseAdminFinal(offerIdList);
        }
    }


    public static void sendOverlimitEmailToCourseAdminFinal(Set<Id> offerIdList) {
        List<EmailTemplate> courseET = [
            SELECT Id, Body, Subject 
            FROM EmailTemplate 
            WHERE IsActive = true AND DeveloperName = :EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_COURSE
        ];

        List<EmailTemplate> specET = [
            SELECT Id, Body, Subject 
            FROM EmailTemplate 
            WHERE IsActive = true AND DeveloperName = :EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_SPEC
        ];

        List<OrgWideEmailAddress> owa = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :CommonUtility.INTEGRATION_USERNAME];

        List<Offer__c> offerList = [SELECT Id, Course_Administrator__c, RecordTypeId FROM Offer__c WHERE Id IN: offerIdList AND Course_Administrator__c != null];

        if (!courseET.isEmpty() && !specET.isEmpty()) {
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

            for(Offer__c offer : offerList) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                mail.setTargetObjectId(offer.Course_Administrator__c);
                mail.saveAsActivity = false;
                mail.setWhatId(offer.Id);
                mail.setTemplateId(offer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)? courseET[0].Id : specET[0].Id);
                if (!owa.isEmpty()) {
                    mail.setOrgWideEmailAddressId(owa[0].Id);
                }
                mails.add(mail);
            }

            try {
                if (!mails.isEmpty()) {
                    Messaging.sendEmail(mails);
                }
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        } else {
            ErrorLogger.configMsg(CommonUtility.ET_UNDEFINED_ERROR + ' ' + EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_COURSE + ' OR ' 
                                  + EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_SPEC);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- OTHER EMAILS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static void sendNotificationToUser(Id userId, String notificationMessage) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setTargetObjectId(userId);
        mail.saveAsActivity = false;
        mail.setSubject(Label.title_EmailNotification);
        mail.setPlainTextBody(notificationMessage);

        try {
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { mail });
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Task createEmailTask(String subject, Id whatId, Id whoId, String description, List<String> attachments) {
        Task e = new Task();
        e.Subject = subject;
        e.WhatId = whatId;
        e.ActivityDate = System.today();
        e.Description = description;
        e.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_EMAIL);
        e.WhoId = whoId;
        e.Status = 'Completed';
        // work-around for orgs without defined integration user for testing purposes
        if (!Test.isRunningTest()) {
//            e.OwnerId = CommonUtility.getIntegrationUser().Id;
        }

        List<String> firstPartOfList = new List<String>();
        List<String> secondPartOfList = new List<String>();
        Integer elements = 0;
        if (attachments != null && !attachments.isEmpty()) {
            for (String attachment : attachments) {
                if (elements <= 3) firstPartOfList.add(attachment);
                else secondPartOfList.add(attachment);

                elements++;
            }

            if (firstPartOfList != null && !firstPartOfList.isEmpty()) {
                String firstPartListString;
                for (String value : firstPartOfList) {
                    if (firstPartListString == null) {
                        firstPartListString = value;
                    }
                    firstPartListString += ';' + value;
                }

                e.Pinned_Attachments__c = firstPartListString.length() <= 255 ? firstPartListString : firstPartListString.substring(0, 255);
            }

            if (secondPartOfList != null && !secondPartOfList.isEmpty()) {
                String secondPartListString;
                for (String value : secondPartOfList) {
                    if (secondPartListString == null) {
                        secondPartListString = value;
                    }
                    secondPartListString += ';' + value;
                }

                e.Pinned_Attachments2__c = secondPartListString.length() <= 255 ? secondPartListString : secondPartListString.substring(0, 255);
            }
        }

        return e;
    }

    public static Task createEmailTaskWithTermsOfUse(String subject, Id whatId, Id whoId, String description, List<String> attachments, List<String> termsOfUseAttachments) {
        Task e = new Task();
        e.Subject = subject;
        e.WhatId = whatId;
        e.ActivityDate = System.today();
        e.Description = description;
        e.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_EMAIL);
        e.WhoId = whoId;
        e.Status = 'Completed';
        e.OwnerId = CommonUtility.getIntegrationUser().Id;

        List<String> firstPartOfList = new List<String>();
        List<String> secondPartOfList = new List<String>();
        Integer elements = 0;

        if (attachments != null && !attachments.isEmpty()) {
            for (String attachment : attachments) {
                if (elements <= 3) firstPartOfList.add(attachment);
                else secondPartOfList.add(attachment);

                elements++;
            }

            if (firstPartOfList != null && !firstPartOfList.isEmpty()) {
                String firstPartListString;
                for (String value : firstPartOfList) {
                    if (firstPartListString == null) {
                        firstPartListString = value;
                    }
                    firstPartListString += ';' + value;
                }

                e.Pinned_Attachments__c = firstPartListString.length() <= 255 ? firstPartListString : firstPartListString.substring(0, 255);
            }

            if (secondPartOfList != null && !secondPartOfList.isEmpty()) {
                String secondPartListString;
                for (String value : secondPartOfList) {
                    if (secondPartListString == null) {
                        secondPartListString = value;
                    }
                    secondPartListString += ';' + value;
                }

                e.Pinned_Attachments2__c = secondPartListString.length() <= 255 ? secondPartListString : secondPartListString.substring(0, 255);
            }
        }

        if (termsOfUseAttachments != null && !termsOfUseAttachments.isEmpty()) {
            String termsOfUseAttachmentsString;
            for (String value : termsOfUseAttachments) {
                if (termsOfUseAttachmentsString == null) {
                    termsOfUseAttachmentsString = value;
                }
                termsOfUseAttachmentsString += ';' + value;
            }

            e.Pinned_Terms_of_Use__c = termsOfUseAttachmentsString.length() <= 255 ? termsOfUseAttachmentsString : termsOfUseAttachmentsString.substring(0, 255);
        }

        return e;
    }

    public static String prepareFakeEmailBody(String templateId, Id whatId) {
        try {
            if (templateId != null && templateId != '' && whatId != null) {
                Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[]{'invalid@emailaddr.es'};
                mail.setToAddresses(toAddresses);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false);
                mail.setSenderDisplayName('MMPT');
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.setTemplateId(templateId);
                mail.setWhatId(whatId);
                
                Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                Database.rollback(sp);

                return mail.getHTMLBody();
            }
        }
        catch (Exception e) { }
        
        return '';
    }

    public static String prepareFakeEmailBodyWithoutVerificationLink(String templateId, Id whatId, String verificationString) {
        try {
            if (templateId != null && templateId != '' && whatId != null) {
                Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[]{'invalid@emailaddr.es'};
                mail.setToAddresses(toAddresses);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false);
                mail.setSenderDisplayName('MMPT');
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.setTemplateId(templateId);
                mail.setWhatId(whatId);

                Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                Database.rollback(sp);

                String result = mail.getHTMLBody();
                if (verificationString != null && verificationString != '') {
                    result = mail.getHTMLBody().replace(verificationString, '');
                }

                return result;
            }
        }
        catch (Exception e) { }
        
        return '';
    }

}