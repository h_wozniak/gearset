global class DocGen_GenerateAgreementDocument implements enxoodocgen.DocGen_StringTokenInterface {

    global static String executeMethod(String objectId, String templateId, String methodName) {
        if (methodName == 'generatePaymentInstallmentsTable') {
            return DocGen_GenerateAgreementDocument.generateDynamicPaymentInstallmentsTable(objectId);          
        }
        if (methodName == 'generatePaymentInstallmentsTableSP') {
            return DocGen_GenerateAgreementDocument.generatePaymentInstallmentsTableSP(objectId);
        }
        if (methodName == 'generatePaymentInstallmentsUnenrolled') {
            return DocGen_GenerateAgreementDocument.generatePaymentInstallmentsUnenrolled(objectId);
        }
        if (methodName == 'generateAnexInformation') {
            return DocGen_GenerateAgreementDocument.generateAnexInformation(objectId);
        }
        return '';
    }

    global static String generateAnexInformation(String objectId) {
        Enrollment__c enr = [
            SELECT Id, Enrollment_from_Documents__r.Previous_Enrollment__r.Annexing__c, Enrollment_from_Documents__r.Previous_Enrollment__c, 
                   Language_of_Main_Enrollment__c
            FROM Enrollment__c 
            WHERE Id = :objectId 
            LIMIT 1
        ];
        
        if (enr.Enrollment_from_Documents__r.Previous_Enrollment__c != null && enr.Enrollment_from_Documents__r.Previous_Enrollment__r.Annexing__c) {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);
            String annexing = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Annexing');

            return '<w:p><w:pPr><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + annexing + '</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p/>';
        }
        else {
            return '';
        }
    }

    global Static String generatePaymentInstallmentsUnenrolled(String objectId) {
        Enrollment__c enr = [SELECT Id, Enrollment_from_Documents__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId LIMIT 1];
        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

        List<String> payments = PaymentManager.generatePaymentInstallmentsMapUnenrolled(enr.Enrollment_from_Documents__c); 
        String result = '<w:tbl><w:tblPr><w:tblW w:w="10773" w:type="dxa"/><w:tblInd w:w="60" w:type="dxa"/><w:tblBorders><w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0600" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="5173"/><w:gridCol w:w="5600"/></w:tblGrid><w:tr><w:trPr><w:jc w:val="center"/><w:cnfStyle w:val="100000000000" w:firstRow="1" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="2694" w:type="dxa"/><w:tcBorders><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="BF"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS') + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="2694" w:type="dxa"/><w:tcBorders><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="BF"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Unr_1') + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3260" w:type="dxa"/><w:tcBorders><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="BF"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="100000000000" w:firstRow="1" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Unr_2') + '</w:t></w:r><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:br/><w:t xml:space="preserve"></w:t></w:r><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> §</w:t></w:r><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> 4 </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2034" w:type="dxa"/><w:tcBorders><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="BF"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="100000000000" w:firstRow="1" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Unr_3') + ' </w:t></w:r><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:br/><w:t>' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Unr_4') + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2502" w:type="dxa"/><w:tcBorders><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="BF"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="100000000000" w:firstRow="1" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_Unr_5') + '</w:t></w:r></w:p></w:tc></w:tr><w:tr><w:trPr><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="2694" w:type="dxa"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>2</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="2694" w:type="dxa"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + payments.get(0) + ' zł' + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3260" w:type="dxa"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="000000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + payments.get(1) +  ' zł ' + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2034" w:type="dxa"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="000000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="pl-PL"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + payments.get(2) + ' zł' + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2502" w:type="dxa"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:cnfStyle w:val="000000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + payments.get(3) + ' zł' + '</w:t></w:r></w:p></w:tc></w:tr></w:tbl>';

        return result;
    }

    global static String generateDynamicPaymentInstallmentsTable(String objectId) {
        Enrollment__c enr = [SELECT Id, Enrollment_from_Documents__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId LIMIT 1];
        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

        Map<Decimal, List<String>> paymentInstallmentMap = PaymentManager.generatePaymentInstallmentsMap(enr.Enrollment_from_Documents__c);

        List<String> header = new List<String> { 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_YEAR_OF_STUDY'), 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS'), 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_SEMESTER'), 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_NUMBER_OF_INSTALLMENTS_SEM'), 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_PROMOTION_VALUE'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_TUITION_TO_PAY'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS_VALUE')
        };

        String result = '';

        result += '<w:tbl><w:tblPr><w:tblW w:w="10773" w:type="dxa"/><w:tblInd w:w="60" w:type="dxa"/><w:tblBorders><w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0600" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="5173"/><w:gridCol w:w="5600"/></w:tblGrid>';

        result += createColoredTableHeaderRow(header);

        for (Decimal paymentInstallment : paymentInstallmentMap.keySet()) {
            List<String> paymentsList = paymentInstallmentMap.get(paymentInstallment);

            if (paymentsList.size() == 5) {
                Decimal academicYear =  math.mod(Integer.valueOf(paymentInstallment), 2) == 0 ? (paymentInstallment / 2) : (paymentInstallment + 1) / 2 ;
                result += createTableRow(academicYear, paymentInstallment, paymentsList);
            }
            else {
                result += createTableMergedRow(paymentInstallment, paymentsList);
            }
        }
        result += '</w:tbl>';
        
        return result;
    }

    global static String generatePaymentInstallmentsTableSP(String objectId) {
        Enrollment__c enr = [SELECT Id, Enrollment_from_Documents__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId LIMIT 1];
        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

        Map<Decimal, List<String>> paymentInstallmentMap = PaymentManager.generatePaymentInstallmentsMapSP(enr.Enrollment_from_Documents__c);

        List<String> header = new List<String> { 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_YEAR_OF_STUDY'), 
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_PROMOTION_VALUE'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_TUITION_TO_PAY'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS_VALUE')
        };

        String result = '';

        result += '<w:tbl><w:tblPr><w:tblW w:w="10773" w:type="dxa"/><w:tblInd w:w="60" w:type="dxa"/><w:tblBorders><w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0600" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="5173"/><w:gridCol w:w="5600"/></w:tblGrid>';

        result += createColoredTableHeaderRow(header);

        for (Decimal paymentInstallment : paymentInstallmentMap.keySet()) {
            List<String> paymentsList = paymentInstallmentMap.get(paymentInstallment);

            result += createTableListRow(paymentInstallment, paymentsList);
        }
        result += '</w:tbl>';
        
        return result;

    }
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- ROW MEHTODS ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global static String createTableHeaderRow(List<String> headerValues) {
        String result = '<w:tr><w:jc w:val="center"/>';
        for (String headerValue : headerValues) {
            result += '<w:tc><w:tcPr><w:tcW w:w="1768" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + headerValue + '</w:t></w:r></w:p></w:tc>';
        }

        result += '</w:tr>';

        return result;
    }

    global static String createColoredTableHeaderRow(List<String> headerValues) {
        String result = '<w:tr><w:jc w:val="center"/>';
        for (String headerValue : headerValues) {
            result += createColumnWithBackgroudnColorString(2034, headerValue);
        }

        result += '</w:tr>';

        return result;
    }

    global static String createTableListRow(Decimal paymentInstallment, List<String> headerValues) {
        String result = '<w:tr><w:jc w:val="center"/>' + createColumnWithBackroungNotHeader(2034, String.valueOf(paymentInstallment)) + createColumnWithBackroungNotHeader(2034, String.valueOf(headerValues.get(3)));

        for (Integer i = 0; i < 3; i++) {
            result += '<w:tc><w:tcPr><w:tcW w:w="1768" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + String.valueOf(headerValues.get(i)) + '</w:t></w:r></w:p></w:tc>';
        }

        result += '</w:tr>';

        return result;
    }

    global static String createTableMergedRow(Decimal semester, List<String> paymentInstallmentsValues) {
        System.debug('merged row: ' + paymentInstallmentsValues);
        
        String result = '<w:tr><w:jc w:val="center"/>';
        result += createMergedColumns(1535) + createMergedColumns(1535) + createColumn(1768, semester) + createColumn(1768, paymentInstallmentsValues.get(3));

        for (Integer payment = 0; payment < 3; payment++) {
            result += createColumn(1768, paymentInstallmentsValues.get(payment));
        }

        result += '</w:tr>';

        return result;
    }

    global static String createTableRow(Decimal academicYear, Decimal semester, List<String> values) {
        String result = '<w:tr><w:jc w:val="center"/>';

        result += createMergableColumn(1768, academicYear) + createMergableColumn(1768, values.get(3)) + createColumn(1768, semester) + createColumn(1768, values.get(4));

        for (Integer payment = 0; payment < 3; payment++) {
            result += createColumn(1768, values.get(payment));
        }

        result += '</w:tr>';

        return result;
    }
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- COLUMN METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    global static String createColumn(Integer width, Decimal value) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + String.valueOf(width) + '" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createColumn(Integer width, String value) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + String.valueOf(width) + '" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }
    global static String createMergedColumns(Integer width) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + String.valueOf(width) + '" w:type="dxa"/><w:vMerge/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p></w:tc>';
    }
    
    global static String createMergableColumn(Integer width, Decimal academicYear) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:vMerge w:val="restart"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + academicYear + '</w:t></w:r></w:p></w:tc>';
    }
    
    global static String createMergableColumn(Integer width, String academicYear) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:vMerge w:val="restart"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + academicYear + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createColumnWithBackgroudnColor(Integer width, Decimal value) {
        return '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="' + width + '" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createColumnWithBackgroudnColorString(Integer width, String value) {
        return '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="' + width + '" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createColumnWithBackroungNotHeader(Integer width, String value) {
        return '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="' + width + '" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="yellow"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }
}