public with sharing class LEX_Send_Email_with_DocumentsController {

	private static final Set<String> forbiddenStatuses = new Set<String> {
		CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
		CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
		CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS,
		CommonUtility.ENROLLMENT_STATUS_RESIGNATION
	};

	@AuraEnabled
	public static String checkStatus(String recordId) {
		Enrollment__c enr = [SELECT Status__c FROM Enrollment__c WHERE Id = :recordId];
		return (!forbiddenStatuses.contains(enr.Status__c)) ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String sendEmailWithDocuments(String recordId) {
		return LEXStudyUtilities.sendEmailWithDocuments(recordId) ? 'SUCCESS' : 'FAILURE';
	}
}