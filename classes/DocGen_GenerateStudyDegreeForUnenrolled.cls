global class DocGen_GenerateStudyDegreeForUnenrolled  implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateStudyDegreeForUnenrolled() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateStudyDegreeForUnenrolled.generateStudyDegreeForUnenrolled(objectId);  
    }

    global static String generateStudyDegreeForUnenrolled(String objectId) {
        Enrollment__c enrollment = [SELECT Degree__c, Enrollment_from_Documents__r.Degree__c, Unenrolled__c, Enrollment_from_Documents__r.Unenrolled__c FROM Enrollment__c WHERE Id =: objectId];
        
        String degree = '';

        if (enrollment.Degree__c != null) {
            degree = enrollment.Degree__c;
        }
        else {
            degree = enrollment.Enrollment_from_Documents__r.Degree__c;
        }
        
        if (degree == CommonUtility.OFFER_DEGREE_II) {
            return '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia SP/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>studia II stopnia SP</w:t></w:r>';
        }
        else {
            return '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia</w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>/ studia II stopnia SP</w:t></w:r>';
        }
    }
}