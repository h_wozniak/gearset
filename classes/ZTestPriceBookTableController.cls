@isTest
private class ZTestPriceBookTableController {
    private static Map<Integer, sObject> prepareDataForTest_AmountDiscounts(Boolean pbActive, Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();


        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = pbActive
        ), true));

        //config PriceBook
        configPriceBook(retMap.get(1).Id, instAmount);



        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void testPriceBookTableController() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        PageReference pageRef = Page.PriceBookTable;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        Test.startTest();
        PriceBookTableController cntrl = new PriceBookTableController(stdController);
        Test.stopTest();

        System.assertNotEquals(cntrl.pbId, null);
    }

    @isTest static void testchangeMode() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        PageReference pageRef = Page.PriceBookTable;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        Test.startTest();
        PriceBookTableController cntrl = new PriceBookTableController(stdController);
        Test.stopTest();

        cntrl.changeMode();
        System.assertEquals(cntrl.detailMode, false);
        System.assertEquals(cntrl.configAllowed, true);
    }

    @isTest static void testDoublechangeMode() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        PageReference pageRef = Page.PriceBookTable;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        Test.startTest();
        PriceBookTableController cntrl = new PriceBookTableController(stdController);
        Test.stopTest();

        cntrl.changeMode();
        cntrl.changeMode();

        System.assertEquals(cntrl.detailMode, true);
    }

    @isTest static void test_cancelEdit() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        PageReference pageRef = Page.PriceBookTable;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        Test.startTest();
        PriceBookTableController cntrl = new PriceBookTableController(stdController);
        Test.stopTest();

        cntrl.cancelEdit();

        System.assertEquals(cntrl.detailMode, true);
    }

    @isTest static void test_saveTable() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        PageReference pageRef = Page.PriceBookTable;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        Test.startTest();
        PriceBookTableController cntrl = new PriceBookTableController(stdController);
        Test.stopTest();

        cntrl.saveTable();    	
    }

}