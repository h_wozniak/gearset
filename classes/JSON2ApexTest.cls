@isTest
private class JSON2ApexTest {
        
    static testMethod void testParse() {
        String json = '{'+
        '  \"trainingCode\": \"WSB_Toruń_Training_Biznesplan_2015-08-21\",'+
        '  \"type\":\"Private\",'+
        '  \"attendees\":['+
        '{'+
        '   \"firstName\": \"Carlene\",'+
        '   \"lastName\": \"West\",'+
        '   \"phone\": \"955406315\",'+
        '   \"email\": \"rodgerswest@poshome.com\",'+
        '   \"position\": \"Manager\",'+
        '   \"department\": \"IT\",'+
        '   \"pesel\": \"123456789\",'+
        '\"mailingStreet\": \"Flory 3/2\",'+
        '\"mailingCity\": \"Warsaw\",'+
        '\"mailingState\": \"mazowieckie\",'+
        '\"mailingPostalCode\": \"23-400\",'+
        '\"mailingCountry\": \"Poland\"'+
        '   }'+
        '],'+
        '  \"personalDataProcessingAgreement\": true,'+
        '  \"portalRegulationsAgreement\": false,'+
        '  \"commercialInfoAgreement\": false,'+
        '  \"source\": \"WWW\",'+
        '  \"sameAsContact\": false,'+
        '  \"invoiceAfterPayment\": true,'+
        '  \"invoiceDeliveryChannel\": \"email\",'+
        '  \"invoiceProforma\": true,'+
        '  \"invoiceDescription\": \"Faktura numer 123\",'+
        '  \"invoiceFirstName\": \"Jan\",'+
        '  \"invoiceLastName\": \"Kowalski\",'+
        '  \"invoiceCity\": \"Warsaw\",'+
        '  \"invoicePostalCode\": \"32-234\",'+
        '  \"invoiceStreet\": \"Flory 3/2\",'+
        '  \"invoiceCountry\": \"Poland\"'+
        '}';
        JSON2Apex obj = JSON2Apex.parse(json);
        System.assert(obj != null);
        System.debug(obj);

    }

}