@isTest
private class ZTestStudySelectionController {
    
    @isTest static void test_getCoursesForUniversity_universityEmpty_degreeEmpty() {
        Test.startTest();
        List<Offer__c> coursesForUniversity = StudySelectionController.getCoursesForUniversity(null, null);
        Test.stopTest();
        
        System.assertEquals(coursesForUniversity.size(), 0);
    }
    
    @isTest static void test_getCoursesForUniversity_universityEmpty_degreeNotEmpty() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_I), true);
        List<Offer__c> courseOffer = ZDataTestUtility.createCourseOffers(
            new Offer__c(
                University_Study_Offer_from_Course__c = univesityOffer.Id,
                Trade_Name__c = 'Test Name',
                Short_Name__c = 'TN',
                Limit_min__c = 0,
                Active_from__c = Date.today(),
                Active_to__c = Date.today(),
                Current_Price_Book_in_use_from__c = Date.today()
        ), 3, true);
        
        Test.startTest();
        List<Offer__c> coursesForUniversity = StudySelectionController.getCoursesForUniversity(null, CommonUtility.OFFER_DEGREE_I);
        Test.stopTest();
        
        //System.assertEquals(coursesForUniversity.size(), 3);
    }
    
    @isTest static void test_getCoursesForUniversity_universityNotEmpty_degreeEmpty() {
        Account university = ZDataTestUtility.createUniversity(null, true);
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        List<Offer__c> courseOffer = ZDataTestUtility.createCourseOffers(
            new Offer__c(
                University_Study_Offer_from_Course__c = univesityOffer.Id,
                Trade_Name__c = 'Test Name',
                Short_Name__c = 'TN',
                Limit_min__c = 0,
                Active_from__c = Date.today(),
                Active_to__c = Date.today(),
                Current_Price_Book_in_use_from__c = Date.today()
        ), 3, true);
        
        List<Offer__c> courseOfferrOnAnotherOffer = ZDataTestUtility.createCourseOffers(null, 3, true);
        
        Test.startTest();
        List<Offer__c> coursesForUniversity = StudySelectionController.getCoursesForUniversity(university.Id, null);
        Test.stopTest();
        
        //System.assertEquals(coursesForUniversity.size(), 3);
    }
    
    @isTest static void test_getCoursesForUniversity_universityNotEmpty_degreeNotEmpty() {
        Account university = ZDataTestUtility.createUniversity(null, true);
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        List<Offer__c> courseOffer = ZDataTestUtility.createCourseOffers(
            new Offer__c(
                University_Study_Offer_from_Course__c = univesityOffer.Id,
                Trade_Name__c = 'Test Name',
                Short_Name__c = 'TN',
                Limit_min__c = 0,
                Active_from__c = Date.today(),
                Active_to__c = Date.today(),
                Current_Price_Book_in_use_from__c = Date.today()
        ), 3, true);
        
        List<Offer__c> courseOfferrOnAnotherOffer = ZDataTestUtility.createCourseOffers(null, 3, true);
        
        Test.startTest();
        List<Offer__c> coursesForUniversity = StudySelectionController.getCoursesForUniversity(university.Id, CommonUtility.OFFER_DEGREE_II);
        Test.stopTest();
        
        //System.assertEquals(coursesForUniversity.size(), 3);
    }
    
    @isTest static void test_getSpecialtiesForCourse_WithSpecialties() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(
            new Offer__c(
                University_Study_Offer_from_Course__c = univesityOffer.Id,
                Trade_Name__c = 'Test Name',
                Short_Name__c = 'TN',
                Limit_min__c = 0,
                Active_from__c = Date.today(),
                Active_to__c = Date.today(),
                Current_Price_Book_in_use_from__c = Date.today()
        ), true);
        
        List<Offer__c> specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffers(
            new Offer__c(
                Course_Offer_from_Specialty__c = courseOffer.Id, 
                Trade_Name__c = 'Test Name',
                Short_Name__c = 'TN',
                Limit_min__c = 0,
                Active_from__c = Date.today(),
                Active_to__c = Date.today(),
                Current_Price_Book_in_use_from__c = Date.today()
        ), 3, true);
        
        Test.startTest();
        List<Offer__c> specialitesForCourse = StudySelectionController.getSpecialtiesForCourse(courseOffer.Id);
        Test.stopTest();
        
        //System.assertEquals(specialitesForCourse.size(), 3);
    }
    
    @isTest static void test_getSpecialtiesForCourse_WithoutSpecialties() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = univesityOffer.Id), true);
        
        Test.startTest();
        List<Offer__c> specialitesForCourse = StudySelectionController.getSpecialtiesForCourse(courseOffer.Id);
        Test.stopTest();
        
        System.assertEquals(specialitesForCourse.size(), 0);
    }
    
    @isTest static void test_back_withRetUrl() {
        PageReference pageRef = Page.StudySelection;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('retURL', '/');
        StudySelectionController studyContrl = new StudySelectionController();
        
        Test.startTest();
        PageReference  backRef = studyContrl.back();
        Test.stopTest();
        
        System.assertNotEquals(backRef, null);
    }
    
    @isTest static void test_back_withoutRetUrl() {
        PageReference pageRef = Page.StudySelection;
        Test.setCurrentPageReference(pageRef);
        StudySelectionController studyContrl = new StudySelectionController();
        
        Test.startTest();
        PageReference backRef = studyContrl.back();
        Test.stopTest();
        
        System.assertEquals(backRef, null);
    }
}