@isTest
private class ZTestEnrollmentStageBarController {

    @isTest static void testEnrollmentStageBarController_freshEnrollment() {
        PageReference pageRef = Page.EnrollmentStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnrollment);

        Test.startTest();
        EnrollmentStageBarController cntrl = new EnrollmentStageBarController(stdController);
        Test.stopTest();

        System.assert(cntrl.enrollmentId != null);
        System.assertEquals(cntrl.backgroundColor, 'border-bottom: 2px solid #FFFFFF');
    }

    @isTest static void testEnrollmentStageBarController_inProgress() {
        PageReference pageRef = Page.EnrollmentStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = CommonUtility.SYNCSTATUS_INPROGRESS), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnrollment);

        Test.startTest();
        EnrollmentStageBarController cntrl = new EnrollmentStageBarController(stdController);
        Test.stopTest();

        System.assert(cntrl.enrollmentId != null);
        System.assertEquals(cntrl.backgroundColor, 'border-bottom: 2px solid #D7F26D');
    }

    @isTest static void testEnrollmentStageBarController_finished() {
        PageReference pageRef = Page.EnrollmentStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = CommonUtility.SYNCSTATUS_FINISHED), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnrollment);

        Test.startTest();
        EnrollmentStageBarController cntrl = new EnrollmentStageBarController(stdController);
        Test.stopTest();

        System.assert(cntrl.enrollmentId != null);
        System.assertEquals(cntrl.backgroundColor, 'border-bottom: 2px solid #52A849');
    }

    @isTest static void testEnrollmentStageBarController_null() {
        PageReference pageRef = Page.EnrollmentStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = null), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnrollment);

        Test.startTest();
        EnrollmentStageBarController cntrl = new EnrollmentStageBarController(stdController);
        Test.stopTest();

        System.assert(cntrl.enrollmentId != null);
        System.assertEquals(cntrl.backgroundColor, 'border-bottom: 2px solid #D84141');
    }

}