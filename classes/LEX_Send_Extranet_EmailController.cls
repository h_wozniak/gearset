public with sharing class LEX_Send_Extranet_EmailController {

	private static final Set<String> forbiddenStatuses = new Set<String> {
		CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
		CommonUtility.ENROLLMENT_STATUS_CONFIRMED,
		CommonUtility.ENROLLMENT_STATUS_COD,
		CommonUtility.ENROLLMENT_STATUS_DC,
		CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
		CommonUtility.ENROLLMENT_STATUS_RESIGNATION
	};

	@AuraEnabled
	public static String checkStatus(String recordId) {
		Enrollment__c enr = [SELECT Status__c, Unenrolled__c, Unenrolled_Status__c FROM Enrollment__c WHERE Id = :recordId];
		if (enr.Unenrolled__c) {
			return (!forbiddenStatuses.contains(enr.Unenrolled_Status__c)) ? 'TRUE' : 'FALSE';
		}
		else {
			return (!forbiddenStatuses.contains(enr.Status__c)) ? 'TRUE' : 'FALSE';
		}
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String sendExtranetEmail(String recordId) {
		return LEXStudyUtilities.sendExtranetEmail(recordId);
	}
}