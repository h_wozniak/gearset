/**
*   @author         Radosław Urbański
*   @description    This class is used to test functionality of IntegrationRequiredDocuments which should inform external systems about documents for enrollment. 
**/

@isTest
private class ZTestIntegrationRequiredDocuments {
	
	@isTest static void test_postRequiredDocuments_documentExists() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		Enrollment__c enrollmentDocument = [SELECT Id FROM Enrollment__c WHERE Enrollment_from_Documents__c = :enrollment.Id AND Document__r.Name = :RecordVals.CATALOG_DOCUMENT_AGREEMENT LIMIT 1];
		Attachment att1 = new Attachment(Name = 'Test attachment name', Body = Blob.valueOf('Test attachment body'), ParentId = enrollmentDocument.Id);
		Attachment att2 = new Attachment(Name = (CommonUtility.UPLOADED_DOCUMENT_PREFIX + 'Test attachment name'), Body = Blob.valueOf('Test attachment body'), ParentId = enrollmentDocument.Id);
		List<Attachment> attList = new List<Attachment>{att1, att2};
		insert attList;

		RestRequest req = new RestRequest();
        req.httpMethod = 'GET';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationRequiredDocuments.postRequiredDocuments();
		Test.stopTest();

		// then
		System.assertEquals(200, res.statusCode);
		System.assertNotEquals(null, res.responseBody);
		System.assertEquals(true, res.responseBody.toString().contains('"document_title":"' + RecordVals.CATALOG_DOCUMENT_AGREEMENT +'"'));
		System.assertEquals(true, res.responseBody.toString().contains('"generated_file_name":"Test attachment name"'));
		System.assertEquals(true, res.responseBody.toString().contains('"uploaded_file_name":"' + CommonUtility.UPLOADED_DOCUMENT_PREFIX + 'Test attachment name"'));
	}

	@isTest static void test_postRequiredDocument_requestNullParameter() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

		RestRequest req = new RestRequest();
        req.httpMethod = 'GET';
        req.params.put('enrollment_id', null);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationRequiredDocuments.postRequiredDocuments();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id'), res.responseBody);
	}

	@isTest static void test_postRequiredDocument_requestWrongId() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

		RestRequest req = new RestRequest();
        req.httpMethod = 'GET';
        req.params.put('enrollment_id', 'wrongId');
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationRequiredDocuments.postRequiredDocuments();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604), res.responseBody);
	}
}