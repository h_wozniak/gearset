@isTest
private class ZTestTuitionSystem {

    @isTest static void test_generateTuitionSystem_enrollment() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED,
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Test.startTest();
        DocGen_GenerateStudyTuitionSystem tuitionSystem = new DocGen_GenerateStudyTuitionSystem();
        
        String result = DocGen_GenerateStudyTuitionSystem.executeMethod(studyEnrollment.Id, null, null);
        Test.stopTest();
    }
    
    @isTest static void test_generateTuitionSystem_documentEnrollment() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED,
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);
        
        Test.startTest();
        String result = DocGen_GenerateStudyTuitionSystem.executeMethod(documentEnrollment.Id, null, null);
        Test.stopTest();
        
    }
}