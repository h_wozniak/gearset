@isTest
private class ZTestIntegrationConsentDictionaryHelper {
    
    private static void prepareData() {
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_2,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_3,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_4,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_5,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
    }
    
    @isTest static void test_getDictionaryConsents_All() {
        prepareData();
        
        List<Catalog__c> listOfConsents = [
            SELECT Name, Content_ENG__c, Content_PL__c, Content_DE__c, Content_UA__c, Type__c
            FROM Catalog__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
        ];
        
        System.assertEquals(listOfConsents.size(), 5);
        
        Test.startTest();
        String result = IntegrationConsentDictionaryHelper.getDictionaryConsents();
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void test_getDictionaryConsents_1() {
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_5,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
        
        List<Catalog__c> listOfConsents = [
            SELECT Name, Content_ENG__c, Content_PL__c, Content_DE__c, Content_UA__c, Type__c, Active_From__c
            FROM Catalog__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
        ];
        
        System.assertEquals(listOfConsents.size(), 1);
        
        Test.startTest();
        String result = IntegrationConsentDictionaryHelper.getDictionaryConsents();
        Test.stopTest();

        String day = listOfConsents.get(0).Active_From__c.day() < 10 ? '0' + String.valueOf(listOfConsents.get(0).Active_From__c.day()) : String.valueOf(listOfConsents.get(0).Active_From__c.day());
        String month = listOfConsents.get(0).Active_From__c.month() < 10 ? '0' + String.valueOf(listOfConsents.get(0).Active_From__c.month()) : String.valueOf(listOfConsents.get(0).Active_From__c.month());        
        String currentDate = listOfConsents.get(0).Active_From__c.year() + '-' + month + '-' + day;
        System.assertNotEquals('', result);
    }
}