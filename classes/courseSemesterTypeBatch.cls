/**
 * Created by martyna.stanczuk on 2018-06-19.
 */

global with sharing class courseSemesterTypeBatch   implements Database.Batchable<sObject> {

    global courseSemesterTypeBatch() {
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        SELECT Id, Semester_Type__c, Offer_Number__c, University_Study_Offer_from_Course__c
        FROM Offer__c
        WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Offer__c> courseOfferList) {
        List<Offer__c> courseOffersToUpdate = new List<Offer__c>();

        Set<Id> studyOfferIds = new Set<Id>();
        for(Offer__c offer :courseOfferList){
            studyOfferIds.add(offer.University_Study_Offer_from_Course__c);
        }

        Map<Id, Offer__c> studyOffersWithDates = new Map<Id, Offer__c>([SELECT Id, Study_Start_Date__c FROM Offer__c WHERE Id IN :studyOfferIds]);

        for (Offer__c courseOffer : courseOfferList) {
            Date dateToCheck = studyOffersWithDates.get(courseOffer.University_Study_Offer_from_Course__c).Study_Start_Date__c;
            courseOffer.Semester_Type__c = generatePeriodOfRecrutation(dateToCheck);
            courseOffer.Offer_Number__c = 10;
            if(courseOffer.Semester_Type__c == CommonUtility.CATALOG_PERIOD_WINTER){
                courseOffer.Offer_Number__c = courseOffer.Offer_Number__c + (dateToCheck.year() - 2018)*2 - 1;
            }
            else {
                courseOffer.Offer_Number__c = courseOffer.Offer_Number__c + (dateToCheck.year() - 2018)*2;
            }

            courseOffersToUpdate.add(courseOffer);
        }

        update courseOffersToUpdate;
    }

    global void finish(Database.BatchableContext BC) {

    }

    static String generatePeriodOfRecrutation(Date dateToCheck) {
        Boolean isWinterEnrollment = dateToCheck.month() >= 2 && dateToCheck.month() <= 7;
        return isWinterEnrollment ? CommonUtility.CATALOG_PERIOD_WINTER : CommonUtility.CATALOG_PERIOD_SUMMER;
    }

}