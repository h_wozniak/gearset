@isTest
private class ZTestPaymentScheduleAndDiscounts {


    private static Map<Integer, sObject> prepareDataForTest_AmountDiscounts(Boolean pbActive, Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();


        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = pbActive
        ), true));

        //config PriceBook
        configPriceBook(retMap.get(1).Id, instAmount);
        
        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(0).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2'
        ), true));



        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------- TEST DELETION OF Payment_DC ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_removePaymentDiscConnFromScheduleInstallments_onDiscountChange() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        //Test.startTest();

        Discount__c manualDiscount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Discount_Value__c = 70,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            Enrollment__c = dataMap.get(2).Id,
            Applied__c = true
        ), true);

        List<Discount__c> paymentDiscountConnList = [
            SELECT Id
            FROM Discount__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR)
        ];

        Test.stopTest();
        System.assert(!paymentDiscountConnList.isEmpty());

        Set<Id> pdConnIds = new Set<Id>();
        for (Discount__c pdConn : paymentDiscountConnList) {
            pdConnIds.add(pdConn.Id);
        }
        manualDiscount.Applied__c = false;
        update manualDiscount;

        List<Discount__c> paymentDiscountConnListAfter = [
            SELECT Id
            FROM Discount__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR)
            AND Id IN :pdConnIds
        ];

        System.assert(paymentDiscountConnListAfter.isEmpty());
    }

    @isTest static void test_removePaymentDiscConnFromScheduleInstallments_onEnrollmentParamsChange() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);
        
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        Test.stopTest();

        //Test.startTest();

        Discount__c manualDiscount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Discount_Value__c = 70,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            Enrollment__c = dataMap.get(2).Id,
            Applied__c = true
        ), true);

        List<Discount__c> paymentDiscountConnList = [
            SELECT Id
            FROM Discount__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR)
        ];
        System.assert(!paymentDiscountConnList.isEmpty());

        Set<Id> pdConnIds = new Set<Id>();
        for (Discount__c pdConn : paymentDiscountConnList) {
            pdConnIds.add(pdConn.Id);
        }
        Enrollment__c studyEnr = new Enrollment__c(
            Id = dataMap.get(2).Id,
            Installments_per_Year__c = '12'
        );

        update studyEnr;

        List<Discount__c> paymentDiscountConnListAfter = [
            SELECT Id
            FROM Discount__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR)
            AND Id IN :pdConnIds
        ];

        System.assert(paymentDiscountConnListAfter.isEmpty());
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- DEFAULT DISCOUNT CASES  ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testDiscountApplication() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);

        Enrollment__c studyEnrBefore = [
            SELECT Id, Value_after_Discount__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        Decimal discountAmount = 300;

        //Test.startTest();
        Discount__c manualDiscount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Discount_Value__c = discountAmount,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            Enrollment__c = dataMap.get(2).Id,
            Applied__c = true
        ), true);
        Test.stopTest();


        Enrollment__c studyEnr = [
            SELECT Id, Value_after_Discount__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(valBefore - discountAmount, studyEnr.Value_after_Discount__c);
    }

    @isTest static void testDoubleDiscountApplication() {
        Map<Integer, sObject> dataMap = prepareDataForTest_AmountDiscounts(true, 100);
        Test.stopTest();

        Enrollment__c studyEnrBefore = [
            SELECT Id, Value_after_Discount__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        Decimal discountAmount = 100;

        //Test.startTest();
        Discount__c manualDiscountAmount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Discount_Value__c = 10,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            Enrollment__c = dataMap.get(2).Id,
            Applied__c = true
        ), true);
        Discount__c manualDiscountPercentage = ZDataTestUtility.createManualDiscount(new Discount__c(
            Discount_Value__c = discountAmount,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON,
            Enrollment__c = dataMap.get(2).Id,
            Applied__c = true
        ), true);


        Enrollment__c studyEnr = [
            SELECT Id, Value_after_Discount__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(valBefore - discountAmount > studyEnr.Value_after_Discount__c);
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- TIME DISCOUNT CASES  --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    private static Map<Integer, sObject> prepareDataForTest_TimeDiscounts() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        User testUser = new User();
        testUser.UserName = 'testUser1@company12345612345xx.com';
        testUser.Email = 'testuser1@company.com';
        testUser.LastName = 'user';
        testUser.FirstName = 'test';
        testUser.Alias = 'alias';
        testUser.EmailEncodingKey = 'UTF-8';
        testUser.LanguageLocaleKey = 'pl';
        testUser.WSB_Department__c = RecordVals.WSB_NAME_WRO;
        testUser.LocaleSidKey = 'pl';
        testUser.TimeZoneSidKey = 'Europe/Berlin';
        testUser.ProfileId = ZDataTestUtility.getProfile(CommonUtility.PROFILE_SALES_BR).Id;
        testUser.UserRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id;

        insert testUser;

        System.runAs(testUser) {

            Account univ = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true);

            Offer__c univOffStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                University__c = univ.Id
            ), true);
            
            retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = univOffStudy.Id
                ), true));
            retMap.put(1, ZDataTestUtility.createOfferTimeDiscount(new Discount__c(
                Offer_from_Offer_DC__c = ((Offer__c) retMap.get(0)).University_Study_Offer_from_Course__c
            ), true));

            //create enrollment
            Test.startTest();
            retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
            ), true));
            Test.stopTest();
        }


        return retMap;
    }

    @isTest static void testTimeDiscountApplication() {
        Map<Integer, sObject> dataMap = prepareDataForTest_TimeDiscounts();

        List<Discount__c> automaticDiscounts = [
            SELECT Id
            FROM Discount__c
            WHERE Enrollment__c = :dataMap.get(2).Id
        ];

        System.assert(!automaticDiscounts.isEmpty());
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- COMPANY DISCOUNT CASES  ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_CompanyDiscounts() {
        User u = [SELECT Id, WSB_Department__c FROM User WHERE Id = :UserInfo.getUserId()];
        u.WSB_Department__c = RecordVals.WSB_NAME_WRO;
        update u;

        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Account univ = ZDataTestUtility.createUniversity(new Account(
            Name = RecordVals.WSB_NAME_WRO
        ), true);

        Offer__c univOffStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            University__c = univ.Id
        ), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = univOffStudy.Id
        ), true));

        retMap.put(1, ZDataTestUtility.createCompany(null, true));

        retMap.put(2, ZDataTestUtility.createCandidateContact(
            new Contact(AccountId = retMap.get(1).Id), 
            true
        ));

        retMap.put(3, ZDataTestUtility.createCompanyDiscount(new Discount__c(
            Company_from_Company_Discount__c = retMap.get(1).Id
        ), true));

        retMap.put(4, ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(
            Discount_from_Company_DC__c = retMap.get(3).Id,
            Company_from_Company_Discount__c = retMap.get(1).Id
        ), true));

        //create enrollment
        Test.startTest();
        retMap.put(5, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(0).Id,
            Candidate_Student__c = retMap.get(2).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2'
        ), true));
        Test.stopTest();

        return retMap;
    }

    @isTest static void testCompanyDiscountApplication() {
        Map<Integer, sObject> dataMap = prepareDataForTest_CompanyDiscounts();

        List<Discount__c> automaticDiscounts = [
            SELECT Id
            FROM Discount__c
            WHERE Enrollment__c = :dataMap.get(5).Id
        ];

        System.assert(!automaticDiscounts.isEmpty());
        System.assertEquals(automaticDiscounts.size(), 1);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ CODE DISCOUNT CASES  -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    private static User createUser(String department, Id userRoleId) {        
        User user = new User();
        user.UserName = 'user1@company12345612345xx.com';
        user.Email = 'user1@company.com';
        user.LastName = 'user';
        user.FirstName = 'test';
        user.Alias = 'alias';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'pl';
        user.WSB_Department__c = department;
        user.LocaleSidKey = 'pl';
        user.TimeZoneSidKey = 'Europe/Berlin';
        user.ProfileId = ZDataTestUtility.getProfile(CommonUtility.PROFILE_SALES_BR).Id;
        user.UserRoleId = userRoleId;

        insert user;

        return user;
    }

    private static Map<Integer, sObject> prepareDataForTest_CodeDiscounts(String promoCode, String enrPromoCode) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(), true));

        retMap.put(1, ZDataTestUtility.createCodeDiscount(null, true));

        retMap.put(2, ZDataTestUtility.createPromoCode(new Discount__c(
            Discount_from_Promo_Code__c = retMap.get(1).Id,
            Promo_Code__c = promoCode
        ), true));

        //create enrollment
        Test.startTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(0).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2',
            Discount_Code__c = enrPromoCode
        ), true));

        return retMap;
    }

    @isTest static void testCodeDiscountApplication_OnEnrInsert() {
        User u = createUser(RecordVals.WSB_NAME_WRO, ([SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id));

        System.runAs(u) {
            Map<Integer, sObject> dataMap = prepareDataForTest_CodeDiscounts('X34D17', 'X34D17');

            List<Discount__c> codeDiscounts = [
                SELECT Id, Discount_from_Discount_Connector__r.Number_of_uses__c, Name
                FROM Discount__c
                WHERE Enrollment__c = :dataMap.get(3).Id
            ];
            Test.stopTest();

            System.assert(!codeDiscounts.isEmpty());

            if (!codeDiscounts.isEmpty()) {
               System.assertEquals(codeDiscounts[0].Discount_from_Discount_Connector__r.Number_of_uses__c, 1);
               System.assert(codeDiscounts[0].Name.contains('X34D17'));
            }
        }
    }

    @isTest static void testCodeDiscountApplication_OnEnrEditWithWrongCode() {
        User u = createUser(RecordVals.WSB_NAME_WRO, ([SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id));

        System.runAs(u) {
            Map<Integer, sObject> dataMap = prepareDataForTest_CodeDiscounts('X34D17', null);

            Enrollment__c enr = new Enrollment__c(
                Id = dataMap.get(3).Id,
                Discount_Code__c = 'KWJ923'
            );

            Test.stopTest();
            //Test.startTest();
            try {
                update enr;
                System.assert(false);
            } catch (Exception e) {
                System.assert(e.getMessage().contains(Label.msg_error_PromoCodeNotFound));
            }
            //Test.stopTest();
        }
    }

    @isTest static void testCodeDiscountApplication_Overlimit() {
        User u = createUser(RecordVals.WSB_NAME_WRO, ([SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id));

        System.runAs(u) {
            Map<Integer, sObject> dataMap = prepareDataForTest_CodeDiscounts('X34D17', 'X34D17');

            Enrollment__c enr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = dataMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2',
                Discount_Code__c = 'X34D17'
            ), false);

            //Test.startTest();
            Test.stopTest();
            try {
                insert enr;
                System.assert(false);
            } catch (Exception e) {
                System.assert(e.getMessage().contains(Label.msg_error_PromoCodeUsesOverLimit));
            }
            //Test.stopTest();
        }
    }

    private static Map<Integer, sObject> prepareDataForTest_CodeDiscountsBasedOnSpec(String promoCode, String enrPromoCode, Boolean withEnr) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(), true));

        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));

        retMap.put(2, ZDataTestUtility.createCodeDiscount(null, true));

        retMap.put(3, ZDataTestUtility.createCodeDiscountConn(new Discount__c(
            Discount_from_Offer_DC__c = retMap.get(2).Id,
            Offer_from_Offer_DC__c = ((Offer__c) retMap.get(0)).University_Study_Offer_from_Course__c
        ), true));

        retMap.put(4, ZDataTestUtility.createPromoCode(new Discount__c(
            Discount_from_Promo_Code__c = retMap.get(2).Id,
            Promo_Code__c = promoCode
        ), true));

        if (withEnr) {
            //create enrollment
            Test.startTest();
            retMap.put(5, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(1).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2',
                Discount_Code__c = enrPromoCode
            ), true));
        }


        return retMap;
    }

    @isTest static void testCodeDiscountApplication_OnEnrInsertBasedOnSpec_CorrectCode() {
        User u = createUser(RecordVals.WSB_NAME_WRO, ([SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id));

        System.runAs(u) {
            Map<Integer, sObject> dataMap = prepareDataForTest_CodeDiscountsBasedOnSpec('X34D17', 'X34D17', true);

            List<Discount__c> codeDiscounts = [
                SELECT Id, Discount_from_Discount_Connector__r.Number_of_uses__c, Name
                FROM Discount__c
                WHERE Enrollment__c = :dataMap.get(5).Id
            ];

            Test.stopTest();
            System.assert(!codeDiscounts.isEmpty());

            if (!codeDiscounts.isEmpty()) {
               System.assertEquals(codeDiscounts[0].Discount_from_Discount_Connector__r.Number_of_uses__c, 1);
               System.assert(codeDiscounts[0].Name.contains('X34D17'));
            }
        }
    }

    @isTest static void testCodeDiscountApplication_OnEnrInsertBasedOnSpec_IncorrectCode() {
        User u = createUser(RecordVals.WSB_NAME_WRO, ([SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id));

        System.runAs(u) {
            Map<Integer, sObject> dataMap = prepareDataForTest_CodeDiscountsBasedOnSpec('X34D17', null, false);

            Test.startTest();

            try {
                ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                    Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                    Installments_per_Year__c = '2',
                    Discount_Code__c = 'X34D17'
                ), true);
                System.assert(false);
            } catch (Exception e) {
                System.assert(e.getMessage().contains(Label.msg_error_PromoCodeNotFound));
            }

            Test.stopTest();
        }
    }

}