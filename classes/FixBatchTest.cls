@isTest
private class FixBatchTest {
    static testMethod void  testBatchExecution() {
        Enrollment__c enr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_COD), true);
        enr.Status__c = 'Confirmed';
        enr.Unenrolled_Status__c = null;
        enr.Annex_or_Related_Enr__c = null;
        enr.Enrollment_Value__c  = 0;
        enr.Discount_Value__c = 0;
        //enr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY);
        
        update enr;
        
        Test.startTest();
            Database.executeBatch(new FixBatch(), 1);
        Test.stopTest();
        
        
    }
}