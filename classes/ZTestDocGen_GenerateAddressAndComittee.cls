@isTest
private class ZTestDocGen_GenerateAddressAndComittee {

    private static Map<Integer, sObject> prepareDataForTest(String phone, String mobile, String mailingCity, String otherCity) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
		Contact c = ZDataTestUtility.createCandidateContact(
			new Contact(Phone = phone, MobilePhone = mobile, 
				MailingCity = mailingCity, MailingStreet = 'Ruska', MailingState = 'Lower Silesia', MailingPostalCode = '00-520', MailingCountry = 'Poland',
				OtherCity = otherCity, OtherStreet = 'Ruska', OtherState = 'Lower Silesia', OtherPostalCode = '00-520', OtherCountry = 'Poland'
		), true);
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id, Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, c);

        return retMap;
	}

	@isTest static void test_generatePhoneNumber_Phone() {
		String phone = '123456789';
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'questionnarePhone');
		Test.stopTest();

		System.assertEquals(phone, result);
	}

	@isTest static void test_generatePhoneNumber_Mobile() {
		String phone = null;
		String mobile = '123456789';
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Contact c = [SELECT Phone, MobilePhone FROM Contact WHERE Id = :dataMap.get(4).Id];
		c.Phone = phone;
		c.MobilePhone = mobile;
		update c;

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'questionnarePhone');
		Test.stopTest();

		System.assertEquals(mobile, result);
	}

	@isTest static void test_generatePhoneNumber_None() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Contact c = [SELECT Phone, MobilePhone FROM Contact WHERE Id = :dataMap.get(4).Id];
		c.Phone = phone;
		c.MobilePhone = mobile;
		update c;

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'questionnarePhone');
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_getContact_properId() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		Enrollment__c result = DocGen_GenerateAddressAndComittee.getContact(dataMap.get(3).Id);
		Test.stopTest();

		System.assertNotEquals(result, null);
	}

	@isTest static void test_getContact_emptyId() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');
		Boolean errorOccured = false;

		Test.startTest();
		try {
			Enrollment__c result = DocGen_GenerateAddressAndComittee.getContact(null);
		}
		catch (Exception e) {
			errorOccured = true;
		}
		Test.stopTest();

		System.assert(errorOccured);
	}

	@isTest static void test_compareAddresses_same() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		Boolean result = DocGen_GenerateAddressAndComittee.compareAddresses((Enrollment__c)dataMap.get(3));
		Test.stopTest();

		System.assertEquals(result, true);
	}

	@isTest static void test_generateMailingCity_same() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingCity');
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_generateMailingCity_different() {
		String phone = null;
		String mobile = null;
		String mailingCity = 'Wrocław';
		String otherCity = 'Warszawa';
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, mailingCity, otherCity);

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null,  'mailingCity');
		Test.stopTest();

		System.assertEquals(result, 'Wrocław');		
	}

	@isTest static void test_generateMailingStreet_same() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingStreet');
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_generateMailingStreet_different() {
		String phone = null;
		String mobile = null;
		String mailingCity = 'Wrocław';
		String otherCity = 'Warszawa';
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, mailingCity, otherCity);

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingStreet');
		Test.stopTest();

		System.assertEquals(result, 'Ruska');			
	}

	@isTest static void test_generateMailingState_same() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingState');
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_generateMailingState_different() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Warszawa');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingState');
		Test.stopTest();

		System.assertEquals(result, 'dolnośląskie');		
	}

	@isTest static void test_generateMailingPostalCode_same() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Wrocław');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingPostalCode');
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_generateMailingPostalCode_different() {
		String phone = null;
		String mobile = null;
		Map<Integer, sObject> dataMap = prepareDataForTest(phone, mobile, 'Wrocław', 'Warszawa');

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, 'mailingPostalCode');
		Test.stopTest();

		System.assertEquals(result, '00-520');		
	}

	@isTest static void test_generateCommittee_OnCourse() {
        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(Selection_Committee__c = 'Commision on Offer', University_Study_Offer_from_Course__c = dataMap.get(0).Id), true));
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = dataMap.get(1).Id), true));
        dataMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = dataMap.get(2).Id), true));

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, 'Commision on Offer');	
	}

	@isTest static void test_generateCommittee_OnSpecialty() {
        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = dataMap.get(0).Id), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__C(Selection_Committee__c = 'Commision on Specialty', Course_Offer_from_Specialty__c = dataMap.get(1).Id), true));
        dataMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = dataMap.get(2).Id), true));
        dataMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = dataMap.get(3).Id), true));

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(4).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, 'Commision on Specialty');	
	}

	@isTest static void test_generateCommittee_OnCourseFromSpecialty() {
        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(Selection_Committee__c = 'Commision on Specialty',University_Study_Offer_from_Course__c = dataMap.get(0).Id), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__C( Course_Offer_from_Specialty__c = dataMap.get(1).Id), true));
        dataMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = dataMap.get(2).Id), true));
        dataMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = dataMap.get(3).Id), true));

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(4).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, 'Commision on Specialty');	
	}

	@isTest static void test_generateCommittee_OnUniFromCourse() {
        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Selection_Committee__c = 'Commision on Uni Offer', Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = dataMap.get(0).Id), true));
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = dataMap.get(1).Id), true));
        dataMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = dataMap.get(2).Id), true));

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(3).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, 'Commision on Uni Offer');	
	}

	@isTest static void test_generateCommittee_OnUniFromSpecialty() {
        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Selection_Committee__c = 'Commision on Specialty from Uni', Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = dataMap.get(0).Id), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__C(Course_Offer_from_Specialty__c = dataMap.get(1).Id), true));
        dataMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = dataMap.get(2).Id), true));
        dataMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = dataMap.get(3).Id), true));

		Test.startTest();
		String result = DocGen_GenerateAddressAndComittee.executeMethod(dataMap.get(4).Id, null, null);
		Test.stopTest();

		System.assertEquals(result, 'Commision on Specialty from Uni');	
	}
}