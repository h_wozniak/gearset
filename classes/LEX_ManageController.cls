public with sharing class LEX_ManageController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String checkCompany(String recordId) {
		Enrollment__c enr = [SELECT Id, Company__c FROM Enrollment__c WHERE Id = :recordId];
		return enr.Company__c != null ? 'TRUE' : 'FALSE';
	}
}