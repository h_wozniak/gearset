/**
* @author        Wojciech Słodziak
* @description  This class is an extension for PriceBookTable.page
**/
public with sharing class PriceBookTableController {
    
    public Boolean detailMode { get; set; }
    public Id pbId { get; set; }
    public Offer__c priceBook { get; set; }
    public Offer__c priceBookForView { get; set; }
    public Integer priceBookRT { get; set; }
    public List<Offer__c> installmentConfigList { get; set; }
    public Boolean configAllowed { get; set; }
    public Boolean canEdit { get; set; }

    /* required only for I & II */
    public Boolean hasGradedPrices { get; set; }

    /* for view */
    public Boolean displaySyncStatus { get; set; }



    public PriceBookTableController(ApexPages.StandardController controller) {
        pbId = controller.getRecord().Id;

        //load required data
        loadPriceBookData(pbId);

        priceBookRT = PriceBookManager.rtToNumberMap.get(priceBook.RecordTypeId);

        // check edit access
        canEdit = WebserviceUtilities.hasEditAccess(priceBook.Id);
        configAllowed = isConfigAllowed();

        // prepare for view
        hasGradedPrices = priceBook.Number_of_Semesters_formula__c > 2 && priceBook.Graded_tuition__c;
        if (priceBook.Display_PB_Deactivation_Warning__c) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, Label.msg_info_PriceBookWasDeactivated));
        }
        displaySyncStatus = (priceBook.Price_Book_Type__c != CommonUtility.OFFER_PBTYPE_SPECIAL);
        detailMode = true;
    }

    private void loadPriceBookData(Id pbId) {
        priceBook = getPriceBook(pbId);
        priceBookForView = getPriceBookForView(pbId);
        installmentConfigList = PriceBookManager.getInstallmentConfigList(pbId);
    }


    private Offer__c getPriceBook(Id pbId) {
        return [
            SELECT Number_of_Semesters_formula__c, PriceBook_From__c, Active__c, RecordTypeId, Graded_tuition__c, Display_PB_Deactivation_Warning__c, Price_Book_Type__c
            FROM Offer__c
            WHERE Id = :pbId
        ];
    }

    private Offer__c getPriceBookForView(Id pbId) {
        return [
            SELECT toLabel(Synchronization_Status__c)
            FROM Offer__c
            WHERE Id = :pbId
        ];
    }

    // checks wheter Price Books is used in any Enrollment
    private Boolean isConfigAllowed() {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Price_Book_from_Enrollment__c 
            FROM Enrollment__c 
            WHERE Price_Book_from_Enrollment__c = :priceBook.Id LIMIT 1
        ];

        return enrollmentList.isEmpty();
    }


    public void changeMode() {
        detailMode = !detailMode;
        if(detailMode) {
            Boolean success = saveTable();
            if (!success) {
                detailMode = false;
            }
        } else {
            priceBook.Display_PB_Deactivation_Warning__c = false;
            try {
                update priceBook;
            } catch(Exception ex) {
                ErrorLogger.log(ex);
            }
        }
    }


    public void cancelEdit() {
        // clear fields
        loadPriceBookData(pbId);

        detailMode = true;
    }


    // if was Active__c, check if is configured properly after save - if not - deactivate and inform user
    public Boolean saveTable() {
        try {
            update priceBook;
            update installmentConfigList;

            if (priceBook.Active__c) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, Label.msg_info_PriceBookConfigurationNotComplete));

                priceBook.Active__c = false;
                priceBook.Synchronization_Status__c = CommonUtility.SYNCSTATUS_MODIFIED;
                try {
                    update priceBook;
                } catch(Exception ex) {
                    ErrorLogger.log(ex);
                }
            }
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            return false;
        }

        return true;
    }

}