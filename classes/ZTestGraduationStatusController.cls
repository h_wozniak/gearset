@isTest
private class ZTestGraduationStatusController {

    @isTest static void test_GraduationStatusController_notGraduation_Comparison() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assertEquals(cntrl.graduateErrorMsg, '');
        System.assertEquals(cntrl.backgroundColor, '');

        System.assertEquals(cntrl.generateAgreementMsg, '');
        System.assertEquals(cntrl.backgroundAgreementColor, '');
    }

    @isTest static void test_GraduationStatusController_notGraduation_Checked() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(Graduate__c = true), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id, Graduation_Checked__c = false), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assert(cntrl.graduateErrorMsg.contains(Label.msg_differenceInGraduationStatus));
        System.assertEquals(cntrl.backgroundColor, '#f4f433');
        System.assertEquals(cntrl.fontColor, '#000000');

        System.assertEquals(cntrl.generateAgreementMsg, '');
        System.assertEquals(cntrl.backgroundAgreementColor, '');
    }

    @isTest static void test_GraduationStatusController_Graduation_Checked() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(Graduate__c = true), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id, Graduation_Checked__c = true), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assert(cntrl.graduateErrorMsg.contains(Label.msg_differenceInGraduationStatusAccepted));
        System.assertEquals(cntrl.backgroundColor, '#52A849');
        System.assertEquals(cntrl.fontColor, '#FFFFFF');

        System.assertEquals(cntrl.generateAgreementMsg, '');
        System.assertEquals(cntrl.backgroundAgreementColor, '');
    }

    @isTest static void test_GraduationStatusController_VerifyAgreement() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(Graduate__c = true), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id, Graduation_Checked__c = true, Verify_Agreement__c = true), true);
        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assert(cntrl.graduateErrorMsg.contains(Label.msg_differenceInGraduationStatusAccepted));
        System.assertEquals(cntrl.backgroundColor, '#52A849');
        System.assertEquals(cntrl.fontColor, '#FFFFFF');

        System.assert(cntrl.generateAgreementMsg.contains(Label.msg_verifyAgreement));
        System.assertEquals(cntrl.backgroundAgreementColor, '#f4f433');
    }

    @isTest static void test_Changed_Fields_on_Documents() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(Graduate__c = true), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(
        	new Enrollment__c(
        		Candidate_Student__c = c.Id, 
        		Graduation_Checked__c = true, 
        		Verify_Agreement__c = true,
        		Changed_Fields_on_Documents__c = 'Initial_Specialty_Declaration__c Email'
        ), true);

        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assert(cntrl.graduateErrorMsg.contains(Label.msg_differenceInGraduationStatusAccepted));
        System.assertEquals(cntrl.backgroundColor, '#52A849');
        System.assertEquals(cntrl.fontColor, '#FFFFFF');

        System.assert(cntrl.generateAgreementMsg.contains(Label.msg_verifyAgreement));
        System.assertEquals(cntrl.backgroundAgreementColor, '#f4f433');

        System.assertEquals(cntrl.generateAllDocumentsMsg, '');
        System.assertEquals(cntrl.backgroudnAllDocumentsColor, '');
    }

    @isTest static void test_Changed_Fields_on_Documents_VerifyDocuments() {
        PageReference pageRef = Page.GraduationStatus;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(Graduate__c = true), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(
        	new Enrollment__c(
        		Candidate_Student__c = c.Id, 
        		Graduation_Checked__c = true, 
        		Verify_Agreement__c = true,
        		Changed_Fields_on_Documents__c = 'Initial_Specialty_Declaration__c Email',
        		Verify_All_Documents__c = true
        ), true);

        ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

        Test.startTest();
        GraduationStatusController cntrl = new GraduationStatusController(stdController);
        Test.stopTest();

        System.assert(cntrl.graduateErrorMsg.contains(Label.msg_differenceInGraduationStatusAccepted));
        System.assertEquals(cntrl.backgroundColor, '#52A849');
        System.assertEquals(cntrl.fontColor, '#FFFFFF');

        System.assert(cntrl.generateAgreementMsg.contains(Label.msg_verifyAgreement));
        System.assertEquals(cntrl.backgroundAgreementColor, '#f4f433');

        System.assert(cntrl.generateAllDocumentsMsg.contains(Label.msg_error_validateAllDocuments1));
        System.assertEquals(cntrl.backgroudnAllDocumentsColor, '#f4f433');
    }
}