/**
* @author       Wojciech Słodziak
* @description  Controller for EnrollmentStageBar used on Enrollment page layouts for studies
**/

public with sharing class EnrollmentStageBarController {

    public Id enrollmentId { get; set; }
    public Enrollment__c enrollment { get; set; }

    public Map<String, String> statusMap { get; set; }
    public List<StageWrapper> statusList { get; set; }
    public Map<String, String> unenrStatusMap { get; set; }
    public List<StageWrapper> unenrStatusList { get; set; }

    public Double tdWidth { get; set; }
    public Double unenrTdWidth { get; set; }
    
    public String enrollmentSychStatus { get; set; }
    public String backgroundColor { get; set;}


    public EnrollmentStageBarController (ApexPages.StandardController controller) {
        enrollmentId = controller.getRecord().Id;
        enrollment = getEnrollment(enrollmentId);

        statusMap = CommonUtility.getPicklistLabelMap(Enrollment__c.Status__c);
        unenrStatusMap = CommonUtility.getPicklistLabelMap(Enrollment__c.Unenrolled_Status__c);

        statusList = getList();
        unenrStatusList = getUnenrList();
        
        if (enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_NEW || enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_MODIFIED) {
            backgroundColor = 'border-bottom: 2px solid #FFFFFF';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_INPROGRESS) {
            backgroundColor = 'border-bottom: 2px solid #D7F26D';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_FINISHED) {
            backgroundColor = 'border-bottom: 2px solid #52A849';
        }
        else {
            backgroundColor = 'border-bottom: 2px solid #D84141';
        }
        
        enrollment = [SELECT Id, ToLabel(Synchronization_Status__c), Status__c, Unenrolled__c, Unenrolled_Status__c, WasIs_Unenrolled__c FROM Enrollment__c WHERE Id = :enrollmentId];
        enrollmentSychStatus = enrollment.Synchronization_Status__c;
    }


    private static Enrollment__c getEnrollment(Id enrId) {
        return [
            SELECT Id, Status__c, Unenrolled__c, Unenrolled_Status__c, WasIs_Unenrolled__c, Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :enrId
        ];
    }

    private List<StageWrapper> getList() {
        List<StageWrapper>  retList = new List<StageWrapper> ();
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED), 'icons/stage1.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_CONFIRMED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_CONFIRMED), 'icons/like.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_COD, statusMap.get(CommonUtility.ENROLLMENT_STATUS_COD), 'icons/file.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_DC, statusMap.get(CommonUtility.ENROLLMENT_STATUS_DC), 'icons/tasks.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_ACCEPTED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_ACCEPTED), 'icons/potwierdzony.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, statusMap.get(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT), 'icons/handshake.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS, statusMap.get(CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS), 'icons/book.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS, statusMap.get(CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS), 'icons/coin.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_RESIGNATION, statusMap.get(CommonUtility.ENROLLMENT_STATUS_RESIGNATION), 'icons/stage5.png'));

        tdWidth = 100.0/retList.size();
        return retList;
    }

    private List<StageWrapper> getUnenrList() {
        List<StageWrapper>  retList = new List<StageWrapper> ();
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_COD, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_COD), 'icons/file.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT), 'icons/handshake.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS), 'icons/book.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS), 'icons/coin.png'));

        unenrTdWidth = 100.0/retList.size();
        return retList;
    }


    private class StageWrapper {
        public String value { get; set; }
        public String label { get; set; }
        public String url { get; set; }

        public StageWrapper(String value, String label, String url) {
            this.value = value;
            this.label = label;
            this.url = url;
        }
    }

}