/**
*   @author         Sebastian Łasisz
*   @description    This class is used to retrieve url to access control panel
**/

global without sharing class IntegrationIPressoControlPanelLink {

    global static AckReceiver_JSON retrievePanelLink() {
        HttpRequest httpReq = new HttpRequest();
    
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createDidactics = esb.IPresso_Control_Panel__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_GET);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = httpRes.getStatusCode() == 200;
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationEnrollmentSender.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n');
        }

        IntegrationIPressoControlPanelLink.AckReceiver_JSON parsedJson;

        try {
            parsedJson = parse(httpRes.getBody());
        } catch(Exception ex) {
        	ErrorLogger.log(ex);
        }

        return parsedJson;
    }
    
    private static IntegrationIPressoControlPanelLink.AckReceiver_JSON parse(String jsonBody) {
        return (IntegrationIPressoControlPanelLink.AckReceiver_JSON) System.JSON.deserialize(jsonBody, IntegrationIPressoControlPanelLink.AckReceiver_JSON.class);
    }

    global class AckReceiver_JSON {
    	public String url;
    	public String error_code;
    	public String message;
    }
}