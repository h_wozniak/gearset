/**
* @description  This class is a Trigger Handler for Studies - rewriting field values (for emails/preview)
**/

public without sharing class TH_OfferRewrites2 extends TriggerHandler.DelegateBase {
    
    List<Offer__c> studyOffersToCopyFields;
    List<Offer__c> specsToCopyFields;
    List<Offer__c> coursesToCopyFields;
    List<Offer__c> priceBooksToCopyFields;
    List<Offer__c> priceBooksInstToCopyFields;
    List<Offer__c> specializationsToCopyFields;
    List<Offer__c> reqDocumentsToCopyFields;
    List<Offer__c> foreignLanguageToCopyFields;

    List<Offer__c> courseListToUpdateSpecialtyTN;
    List<Offer__c> coursesToCopyAccStatusToSpec;
    List<Offer__c> offersToCopyAccStatusToEnr;
    List<Offer__c> coursesToUpdateCourseAdminOnSpec;


    public override void prepareBefore() {
        studyOffersToCopyFields = new List<Offer__c>();
        specsToCopyFields = new List<Offer__c>();
        coursesToCopyFields = new List<Offer__c>();
        priceBooksToCopyFields = new List<Offer__c>();
        priceBooksInstToCopyFields = new List<Offer__c>();
        priceBooksInstToCopyFields = new List<Offer__c>();
        reqDocumentsToCopyFields = new List<Offer__c>();
        foreignLanguageToCopyFields = new List<Offer__c>();
        specializationsToCopyFields = new List<Offer__c>();
    }

    public override void prepareAfter() {
        courseListToUpdateSpecialtyTN = new List<Offer__c>();
        coursesToCopyAccStatusToSpec = new List<Offer__c>();
        offersToCopyAccStatusToEnr = new List<Offer__c>();
        coursesToUpdateCourseAdminOnSpec = new List<Offer__c>();
    }

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Offer__c newOffer = (Offer__c)obj;

            /* Wojciech Słodziak */
            // copy multiple fields on Study Offer insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_UNIVERSITY_OFFER_STUDY)) {
                studyOffersToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Course insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)) {
                coursesToCopyFields.add(newOffer);
            }

            /* Sebastian Łasisz */
            // if guided group is selected ignore discounts
            if ((newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
                || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER))
                && newOffer.Guided_Group__c == true) {
                newOffer.Ignore_Discounts__c = true;
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Specialty insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)) {
                specsToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Price Book insert
            if (PriceBookManager.priceBookRecordTypeIds.contains(newOffer.RecordTypeId)) {
                priceBooksToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Price Book Installment Config insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_PB_INSTALLMENT_CFG)) {
                priceBooksInstToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Specialization insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_SPECIALIZATION)) {
                specializationsToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Required Document insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_REQUIRED_DOCUMENT)) {
                reqDocumentsToCopyFields.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy multiple fields on Foreign Language insert
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE)) {
                foreignLanguageToCopyFields.add(newOffer);
            }

        }
    }

    //public override void afterInsert(Map<Id, sObject> o) {}

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        for(Id key : newOffers.keySet()) {
            Offer__c oldOffer = oldOffers.get(key);
            Offer__c newOffer = newOffers.get(key);


            /* Sebastian Łasisz */
            // if guided group is selected ignore discounts
            if ((newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
                || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER))
                && newOffer.Guided_Group__c != oldOffer.Guided_Group__c
                && newOffer.Guided_Group__c == true) {
                newOffer.Ignore_Discounts__c = true;
            }
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        for(Id key : newOffers.keySet()) {
            Offer__c oldOffer = oldOffers.get(key);
            Offer__c newOffer = newOffers.get(key);

            /* Wojciech Słodziak */
            // If Trade name changes on Training Offer - update Training Schedule's Trade Names
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Trade_Name__c != oldOffer.Trade_Name__c) {
                courseListToUpdateSpecialtyTN.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy Acceptance Status from Course on change
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Acceptation_Status__c != oldOffer.Acceptation_Status__c) {
                coursesToCopyAccStatusToSpec.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy Acceptance Status from Course / Specialty to Enrollment
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
                ) 
                && newOffer.Acceptation_Status__c != oldOffer.Acceptation_Status__c
            ) {
                offersToCopyAccStatusToEnr.add(newOffer);
            }

            /* Wojciech Słodziak */
            // copy Course ADministrator from Course to Specialties on update
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) 
                && newOffer.Course_Administrator__c != oldOffer.Course_Administrator__c) {
                coursesToUpdateCourseAdminOnSpec.add(newOffer);
            }

        }
    }

    //public override void beforeDelete(Map<Id, sObject> old) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {

        /* Wojciech Słodziak */
        // copy multiple fields on Study Offer insert
        if (studyOffersToCopyFields != null && !studyOffersToCopyFields.isEmpty()) {
            List<Id> universityIds = new List<Id>();
            for (Offer__c uof : studyOffersToCopyFields) {
                universityIds.add(uof.University__c);
            }

            Map<Id, Account> universityMap = new Map<Id, Account>([
                SELECT Id, Name 
                FROM Account 
                WHERE Id IN :universityIds
            ]);

            for (Offer__c uof : studyOffersToCopyFields) {
                uof.University_Name__c = universityMap.get(uof.University__c).Name;
            }
        }


        /* Wojciech Słodziak */
        // copy multiple fields on Course insert
        if (coursesToCopyFields != null && !coursesToCopyFields.isEmpty()) {
            List<Id> univerOfferStudyIds = new List<Id>();
            for (Offer__c course : coursesToCopyFields) {
                univerOfferStudyIds.add(course.University_Study_Offer_from_Course__c);
            }

            Map<Id, Offer__c> univerOfferStudyList = new Map<Id, Offer__c>([
                SELECT Id, University__r.Name, Degree__c 
                FROM Offer__c 
                WHERE Id IN :univerOfferStudyIds
            ]);

            for (Offer__c course : coursesToCopyFields) {
                Offer__c univerOfferStudy = univerOfferStudyList.get(course.University_Study_Offer_from_Course__c);

                course.University_Name__c = univerOfferStudy.University__r.Name;
                course.Degree__c = univerOfferStudy.Degree__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Specialty insert
        if (specsToCopyFields != null && !specsToCopyFields.isEmpty()) {
            Set<Id> courseIds = new Set<Id>();
            for (Offer__c spec : specsToCopyFields) {
                courseIds.add(spec.Course_Offer_from_Specialty__c);
            }

            Map<Id, Offer__c> courseList = new Map<Id, Offer__c>([
                SELECT Id, Acceptation_Status__c, University_Name__c, Trade_name__c, Degree__c, Course_Administrator__c, Kind__c, Mode__c, Field__c
                FROM Offer__c 
                WHERE Id IN :courseIds
            ]);

            for (Offer__c spec : specsToCopyFields) {
                Offer__c course = courseList.get(spec.Course_Offer_from_Specialty__c);

                spec.University_Name__c = course.University_Name__c;
                spec.Acceptation_Status__c = course.Acceptation_Status__c;
                spec.Trade_name__c = course.Trade_name__c;
                spec.Degree__c = course.Degree__c;
                spec.Course_Administrator__c = course.Course_Administrator__c;
                spec.Kind__c = course.Kind__c;
                spec.Mode__c = course.Mode__c;
                spec.Field__c = course.Field__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Price Book insert
        if (priceBooksToCopyFields != null && !priceBooksToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Offer__c priceBook : priceBooksToCopyFields) {
                offerIds.add(priceBook.Offer_from_Price_Book__c);
            }

            Map<Id, Offer__c> offerList = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            for (Offer__c priceBook : priceBooksToCopyFields) {
                Offer__c offer = offerList.get(priceBook.Offer_from_Price_Book__c);

                priceBook.University_Name__c = offer.University_Name__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Price Book Installment Config insert
        if (priceBooksInstToCopyFields != null && !priceBooksInstToCopyFields.isEmpty()) {
            Set<Id> priceBookIds = new Set<Id>();
            for (Offer__c priceBookInst : priceBooksInstToCopyFields) {
                priceBookIds.add(priceBookInst.Price_Book_from_Installment_Config__c);
            }

            Map<Id, Offer__c> priceBookList = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :priceBookIds
            ]);

            for (Offer__c priceBookInst : priceBooksInstToCopyFields) {
                Offer__c priceBook = priceBookList.get(priceBookInst.Price_Book_from_Installment_Config__c);

                priceBookInst.University_Name__c = priceBook.University_Name__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Specialization insert
        if (specializationsToCopyFields != null && !specializationsToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Offer__c specialization : specializationsToCopyFields) {
                offerIds.add(specialization.Offer_from_Specialization__c);
            }

            Map<Id, Offer__c> offerList = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            for (Offer__c specialization : specializationsToCopyFields) {
                Offer__c offer = offerList.get(specialization.Offer_from_Specialization__c);

                specialization.University_Name__c = offer.University_Name__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Required Document insert
        if (reqDocumentsToCopyFields != null && !reqDocumentsToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Offer__c reqDoc : reqDocumentsToCopyFields) {
                offerIds.add(reqDoc.Offer_from_Document__c);
            }

            Map<Id, Offer__c> offerList = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            for (Offer__c reqDoc : reqDocumentsToCopyFields) {
                Offer__c offer = offerList.get(reqDoc.Offer_from_Document__c);

                reqDoc.University_Name__c = offer.University_Name__c;
            }
        }

        /* Wojciech Słodziak */
        // copy multiple fields on Foreign Language insert
        if (foreignLanguageToCopyFields != null && !foreignLanguageToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Offer__c language : foreignLanguageToCopyFields) {
                offerIds.add(language.Course_Offer_from_Language__c);
            }

            Map<Id, Offer__c> offerList = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            for (Offer__c language : foreignLanguageToCopyFields) {
                Offer__c offer = offerList.get(language.Course_Offer_from_Language__c);

                language.University_Name__c = offer.University_Name__c;
            }
        }

        /* Wojciech Słodziak */
        // copy Acceptance Status from Course to Specialty on change
        if (coursesToCopyAccStatusToSpec != null && !coursesToCopyAccStatusToSpec.isEmpty()) {
            Set<Id> courseIds = new Set<Id>();
            for (Offer__c course : coursesToCopyAccStatusToSpec) {
                courseIds.add(course.Id);
            }

            List<Offer__c> specList = [
                SELECT Id, Acceptation_Status__c, Course_Offer_from_Specialty__c 
                FROM Offer__c 
                WHERE Course_Offer_from_Specialty__c IN :courseIds
            ];

            for (Offer__c course : coursesToCopyAccStatusToSpec) {
                for (Offer__c spec : specList) {
                    if (spec.Course_Offer_from_Specialty__c == course.Id) {
                        spec.Acceptation_Status__c = course.Acceptation_Status__c;
                    }
                }
            }

            try {
                update specList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // copy Acceptance Status from Course to Specialty on change
        if (offersToCopyAccStatusToEnr != null && !offersToCopyAccStatusToEnr.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Offer__c offer : offersToCopyAccStatusToEnr) {
                offerIds.add(offer.Id);
            }

            List<Enrollment__c> enrList = [
                SELECT Id, Acceptation_Status__c, Course_or_Specialty_Offer__c, Course_or_Specialty_from_EduAgr__c 
                FROM Enrollment__c 
                WHERE Course_or_Specialty_Offer__c IN :offerIds OR Course_or_Specialty_from_EduAgr__c IN :offerIds
            ];

            for (Offer__c offer : offersToCopyAccStatusToEnr) {
                for (Enrollment__c enr : enrList) {
                    if (enr.Course_or_Specialty_Offer__c == offer.Id || enr.Course_or_Specialty_from_EduAgr__c == offer.Id) {
                        enr.Acceptation_Status__c = offer.Acceptation_Status__c;
                    }
                }
            }

            try {
                update enrList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // update Trade Name on Specialty after Course edit
        if (courseListToUpdateSpecialtyTN != null && !courseListToUpdateSpecialtyTN.isEmpty()) {
            List<Id> courseOfferIds = new List<Id>();
            for (Offer__c courseOffer : courseListToUpdateSpecialtyTN) {
                courseOfferIds.add(courseOffer.Id);
            }

            List<Offer__c> specialtyList = [
                SELECT Id, Trade_Name__c, Course_Offer_from_Specialty__c 
                FROM Offer__c 
                WHERE Course_Offer_from_Specialty__c IN :courseOfferIds
            ];

            for (Offer__c courseOffer : courseListToUpdateSpecialtyTN) {
                for (Offer__c spec : specialtyList) {
                    if (courseOffer.Id == spec.Course_Offer_from_Specialty__c) {
                        spec.Trade_Name__c = courseOffer.Trade_Name__c;
                    }
                }
            }

            try {
                update specialtyList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // update Course Administrator on Specialty after Course edit
        if (coursesToUpdateCourseAdminOnSpec != null && !coursesToUpdateCourseAdminOnSpec.isEmpty()) {
            List<Id> courseOfferIds = new List<Id>();
            for (Offer__c courseOffer : coursesToUpdateCourseAdminOnSpec) {
                courseOfferIds.add(courseOffer.Id);
            }

            List<Offer__c> specialtyList = [
                SELECT Id, Course_Offer_from_Specialty__c, Course_Administrator__c 
                FROM Offer__c 
                WHERE Course_Offer_from_Specialty__c IN :courseOfferIds
            ];

            for (Offer__c courseOffer : coursesToUpdateCourseAdminOnSpec) {
                for (Offer__c spec : specialtyList) {
                    if (courseOffer.Id == spec.Course_Offer_from_Specialty__c) {
                        spec.Course_Administrator__c = courseOffer.Course_Administrator__c;
                    }
                }
            }
            
            try {
                update specialtyList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

    }

}