/**
*   @author         Sebastian Łasisz
*   @description    This class is used to create documents with attachments on study enrollment
**/

public without sharing class EnrollmentAnnexFinalizationQueueable implements Queueable, Database.AllowsCallouts {
	Enrollment__c newEnr;
	Enrollment__c previousEnr;
	List<Enrollment__c> languages;

	public EnrollmentAnnexFinalizationQueueable(Enrollment__c newEnr, Enrollment__c previousEnr, List<Enrollment__c> languages) {
		this.newEnr = newEnr;
		this.previousEnr = previousEnr;
		this.languages = languages;
	}

	public void execute(QueueableContext qc) {
		EnrollmentAnnexingManager.createAnnex_FinalizationHelper(newEnr, previousEnr, languages);
	} 
}