/**
* @author       Wojciech Słodziak
* @description  Class receive job transfers from other transactions. This is a workaround to change context user for some tasks.
**/

global class TransferJobToDefaultUserEmService implements Messaging.InboundEmailHandler {

    /* list of handled methods */
    public enum Transferable {
        COURSE_OVERLIMIT_EMAIL,
        DOC_LIST_EMAIL,
        UNENROLLED_DOC_LIST_EMAIL,
        DOCUMENTS
    }


    
    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- JOB RECEIVE ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        String plainText = email.plainTextBody;

        TransferJobDescription tjd;
        try {
            tjd = (TransferJobDescription) JSON.deserialize(plainText, TransferJobDescription.class);
        } catch (JSONException ex) {
            ErrorLogger.log(ex);
            result.success = false;
        }


        if (tjd != null && tjd.jobName != null && tjd.jobRecordId != null) {
            if (tjd.jobName == Transferable.COURSE_OVERLIMIT_EMAIL.name()) {
                EmailManager.sendOverlimitEmailToCourseAdminFinal(new Set<Id> { tjd.jobRecordId });
            } else 
            if (tjd.jobName == Transferable.DOC_LIST_EMAIL.name()) {
                EmailManager_DocumentListEmail.sendRequiredDocumentListFinal(new Set<Id> { tjd.jobRecordId });
            } else 
            if (tjd.jobName == Transferable.UNENROLLED_DOC_LIST_EMAIL.name()) {
                EmailManager_UnenrolledDocumentListEmail.sendRequiredDocumentListFinal(new Set<Id> { tjd.jobRecordId });
            } else 
            if (tjd.jobName == Transferable.DOCUMENTS.name()) {
                GenerateMissingAttachmentsOnEnrollment.generateAttachmentForDocuments(new Set<Id> { tjd.jobRecordId });
            }
            
            result.success = true;
        }

        return result;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ JOB TRANSFER INVOKATION METHOD ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method to send email to Job Transfer Email Service to change running user context (workaround)
    // templateForEmails may be null
    // works strictly with TransferJobEmailWorkflow workflow
    public static void tranfserJob(Transferable transferableJob, Set<Id> recordIds) {
        List<SObject> sObjToUpdate = new List<SObject>();

        Schema.SObjectType sObjType = new List<Id>(recordIds)[0].getSObjectType();

        for (Id recordId : recordIds) {
            TransferJobToDefaultUserEmService.TransferJobDescription tjd = new TransferJobToDefaultUserEmService.TransferJobDescription();
            tjd.jobName = transferableJob.name();
            tjd.jobRecordId = recordId;

            SObject sObj = Schema.getGlobalDescribe().get(sObjType.getDescribe().getName()).newSObject();

            sObj.Id = recordId;
            sObj.put('TransferJobHelperJSON__c', JSON.serialize(tjd));

            sObjToUpdate.add(sObj);
        }
		
        try {
            Database.update(sObjToUpdate, false); // triggers workflow
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        for (SObject sObj : sObjToUpdate) {
            sObj.put('TransferJobHelperJSON__c', null);
        }
        
        try {
            Database.update(sObjToUpdate, false); // resets value to null
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- JOB TRANSFER MODEL ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global class TransferJobDescription {
        public String jobName;
        public Id jobRecordId;
    }
}