/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Integration Interaction
**/

@RestResource(urlMapping = '/contact-data-crm/interaction-add/*')
global without sharing class IntegrationInteractionReceiver {

 	@HttpPost
    global static void receiveInteractionInformations() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();
        
        List<IntegrationInteractionHelper.Interaction_JSON> parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(601));
            return ;
        }

        Set<Id> externalContactIds = new Set<Id>();
        for (IntegrationInteractionHelper.Interaction_JSON newTaskJSON : parsedJson) {
        	externalContactIds.add(newTaskJSON.external_contact_id);
        }

        List<Contact> contactsToCreateTask;
        try {
	        contactsToCreateTask = [
	        	SELECT Id, ID_Unique__c
	        	FROM Contact
	        	WHERE ID_Unique__c IN :externalContactIds
	        ];
	    }
	    catch (Exception e) {
	    	ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(903));
            return ;
	    }

	    List<Task> tasksToInsert = new List<Task>();
	    for (IntegrationInteractionHelper.Interaction_JSON newTaskJSON : parsedJson) {
	    	for (Contact contactToCreateTask : contactsToCreateTask) {
	    		if (contactToCreateTask.ID_Unique__c == newTaskJSON.external_contact_id) {
			    	if (newTaskJSON.interaction_type == 'Call') {
					    Task newTask = new Task();
					    newTask.WhoId = contactToCreateTask.Id;
					    newTask.Status = 'Completed';
					    newTask.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL);
					    newTask.Agent_Id__c = newTaskJSON.agent_id;
					    newTask.Call_Answered__c = newTaskJSON.call_details.call_answered;
					    newTask.Call_Type__c = newTaskJSON.call_details.call_type;
					    newTask.Interaction_Classifier__c = newTaskJSON.interaction_classifier;		    
					    newTask.Subject = newTaskJSON.interaction_subject;
					    newTask.ActivityDate = CommonUtility.stringToDate(newTaskJSON.interaction_date);
					    newTask.Description = newTaskJSON.interaction_description;
					    newTask.Phone_Number__c = newTaskJSON.call_details.phone_number;

					    tasksToInsert.add(newTask);
					}
				}
			}
		}

		try {
			insert tasksToInsert;
		}
		catch (Exception e) {
			ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(400, e.getMessage()));
            return ;
		}
    }
    
    private static List<IntegrationInteractionHelper.Interaction_JSON> parse(String jsonBody) {
        return (List<IntegrationInteractionHelper.Interaction_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationInteractionHelper.Interaction_JSON>.class);
    }
}