/**
*   @author         Grzegorz Długosz
*   @description    This class is a helper for IntegrationExtCampaignCreation.
**/

public without sharing class IntegrationExtCampaignCreationHelper {
	
    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- INVOKATION METHOD -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static void createExtCampaign(List<ExtCampaignRequest_JSON> requestJSON) {
        IntegrationExtCampaignCreationHelper.validateJSON(requestJSON);

        List<Marketing_Campaign__c> listOfCampaigns = new List<Marketing_Campaign__c>();

        for (ExtCampaignRequest_JSON externalCampaign : requestJSON) {

            Marketing_Campaign__c marketingRecord = new Marketing_Campaign__c(
                External_Id__c = externalCampaign.external_campaign_id,
                Campaign_Name__c = externalCampaign.campaign_name,
                Campaign_Communication_Form__c = externalCampaign.campaign_communication_form,
                RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL)
            );

            listOfCampaigns.add(marketingRecord);
        }

        upsert listOfCampaigns External_Id__c;

    }


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- VALIDATION METHODS -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static void validateJSON(List<ExtCampaignRequest_JSON> requestJSON) {
        
        //check if all required fields are filled
        for (ExtCampaignRequest_JSON externalCampaign : requestJSON) {

            if (externalCampaign.external_campaign_id == null) {
                throw new InvalidFieldValueException('external_campaign_id');
            }
            if (externalCampaign.campaign_name == null) {
                throw new InvalidFieldValueException('campaign_name');
            }

            if (externalCampaign.campaign_communication_form == null
                || CommonUtility.getProperPicklistLabel(Marketing_Campaign__c.Campaign_Communication_Form__c, externalCampaign.campaign_communication_form) == null) {
                throw new InvalidFieldValueException('campaign_communication_form');
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ REQUEST MODEL DEFINITION ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class ExtCampaignRequest_JSON {
        public String external_campaign_id;
        public String campaign_name;
        public String campaign_communication_form;
        public String fields;

        public ExtCampaignRequest_JSON(String external_campaign_id, String campaign_name, String campaign_communication_form, String fields){
            this.external_campaign_id = external_campaign_id;
            this.campaign_name = campaign_name;
            this.campaign_communication_form = campaign_communication_form;
            this.fields = fields;
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- EXCEPTION CLASSES ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class InvalidFieldValueException extends Exception {}
}