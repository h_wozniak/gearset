/**
*   @author         Sebastian Łasisz
*   @description    This class is used to test if functionality used to retrieve enrollment list
**/
@isTest
private class ZTestIntegrationEnrollmentList {

    @isTest static void getEnrollmentsForContact_null() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/enrollments/';
        req.addParameter('person_id', null);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        IntegrationEnrollmentList.getEnrollmentsForContact();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(400, res.statusCode);
    }

    @isTest static void getEnrollmentsForContact() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        ZDataTestUtility.prepareCatalogData(true, false);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(MobilePhone = '1234', Consent_Direct_Communications_ADO__c = CommonUtility.MARKETING_ENTITY_GDANSK), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/enrollments/';
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        IntegrationEnrollmentList.getEnrollmentsForContact();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(200, res.statusCode);
    }
}