@isTest
private class ZTestMethodGetDate {
    
    @isTest static void testGenerateDate() {
        Test.startTest();
        String result = DocGen_MethodGetDate.executeMethod(null, null, null);
        Date dateToConvert = Date.today();

    	String day = dateToConvert.day() < 10 ? '0' + String.valueOf(dateToConvert.day()) : String.valueOf(dateToConvert.day());
    	String month = dateToConvert.month() < 10 ? '0' + String.valueOf(dateToConvert.month()) : String.valueOf(dateToConvert.month());

        System.assert(result == day + '.' + month + '.' + dateToConvert.year());
        Test.stopTest();
    }
}