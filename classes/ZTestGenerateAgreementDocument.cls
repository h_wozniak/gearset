@isTest
private class ZTestGenerateAgreementDocument {
    
    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];
        
        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }
        
        update pb.InstallmentConfigs__r;
    }
    
    @isTest static void testGenerateStaticPaymentInstallmentsTable() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);

        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();
        

        Enrollment__c docEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        List<Payment__c> instList = [
            SELECT Id, PriceBook_Value__c
            FROM Payment__c 
            WHERE Enrollment_from_Schedule_Installment__c = :studyEnrollment.Id
        ];
        
        List<Payment__c> payments = [
            SELECT Installment_Semester__c, PriceBook_Value__c, Discount_Value__c, Value_after_Discount__c
            FROM Payment__c
            WHERE Enrollment_from_Schedule_Installment__c = :studyEnrollment.Id
        ];
        
        System.debug(payments);
        
        System.assert(!instList.isEmpty());
        String result = DocGen_GenerateAgreementDocument.executeMethod(docEnrollment.Id, null, 'generatePaymentInstallmentsTable');
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGenerateStaticPaymentInstallmentsTable_WithoutMethodName() {
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();
        
        String result = DocGen_GenerateAgreementDocument.executeMethod(studyEnrollment.Id, null, null);
        
        System.assertEquals('', result);
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutPriceBook() {
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c( 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        
        Test.startTest();
        try {
            String result = DocGen_GenerateAgreementDocument.executeMethod(studyEnrollment.Id, null, null);
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }
        Test.stopTest();        
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutEnrollment() {
        Test.startTest();
        try {
            String result = DocGen_GenerateAgreementDocument.executeMethod(null, null, null);
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }
        Test.stopTest();        
    }

    @isTest static void testGeneratePaymentInstallmentsTableSP() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);
        
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();

        Enrollment__c docEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateAgreementDocument.executeMethod(docEnrollment.Id, null, 'generatePaymentInstallmentsTableSP');
        
        System.assertNotEquals('', result);
    }

    @isTest static void test_generateAnexInformation_proper() {
        Enrollment__c annexedEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Annexing__c = true,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        
        Test.startTest();

        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Previous_Enrollment__c = annexedEnrollment.Id,
                Installments_per_Year__c = '2'),
            true);

        Enrollment__c docEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateAgreementDocument.executeMethod(docEnrollment.Id, null, 'generateAnexInformation');
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }

    @isTest static void test_generateAnexInformation() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);
        
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);

        Enrollment__c docEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateAgreementDocument.executeMethod(docEnrollment.Id, null, 'generateAnexInformation');
        Test.stopTest();
        
        System.assertEquals('', result);
    }

    @isTest static void test_generatePaymentInstallmentsUnenrolled() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);
        
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);

        Enrollment__c docEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        Test.stopTest();
        
        String result = DocGen_GenerateAgreementDocument.executeMethod(docEnrollment.Id, null, 'generatePaymentInstallmentsUnenrolled');
        
        System.assertNotEquals('', result);
    }

    @isTest static void test_createTableListRow_withIncompleteList() {
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5' };

        Test.startTest();
        try {
            String result = DocGen_GenerateAgreementDocument.createTableListRow(semester, paymentsInstallmentsValue);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('List index out of bounds'));
        }
        Test.stopTest();
    }

    @isTest static void test_createTableListRow_withCompleteList() {
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5', '2', '1', '4' };

        Test.startTest();
        String result = DocGen_GenerateAgreementDocument.createTableListRow(semester, paymentsInstallmentsValue);
        Test.stopTest();
    }

    @isTest static void test_createTableMergedRow_withIncompleteList() {
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5' };

        Test.startTest();
        try {
            String result = DocGen_GenerateAgreementDocument.createTableMergedRow(semester, paymentsInstallmentsValue);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('List index out of bounds'));
        }
        Test.stopTest();
    }

    @isTest static void test_createTableMergedRow_withCompleteList() {
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5', '2', '1', '4' };

        Test.startTest();
        String result = DocGen_GenerateAgreementDocument.createTableMergedRow(semester, paymentsInstallmentsValue);
        Test.stopTest();
    }

    @isTest static void test_createTableRow_withIncompleteList() {
        Decimal academicYear = 2;
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5' };

        Test.startTest();
        try {
            String result = DocGen_GenerateAgreementDocument.createTableRow(academicYear, semester, paymentsInstallmentsValue);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('List index out of bounds'));
        }
        Test.stopTest();
    }

    @isTest static void test_createTableRow_withCompleteList() {
        Decimal academicYear = 2;
        Decimal semester = 5;
        List<String> paymentsInstallmentsValue = new List<String> { '5', '2', '1', '4', '5' };

        Test.startTest();
        String result = DocGen_GenerateAgreementDocument.createTableRow(academicYear, semester, paymentsInstallmentsValue);
        Test.stopTest();
    }
}