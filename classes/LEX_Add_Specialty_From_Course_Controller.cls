public with sharing class LEX_Add_Specialty_From_Course_Controller {
	
	@AuraEnabled
	public static Boolean hasEditAccess(String offerId) {
		return LEXServiceUtilities.hasEditAccess(offerId);
	}

	@AuraEnabled
	public static String getSpecialtyRTId() {
		return CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER);
	}
	
	@AuraEnabled
	public static Offer__c getObjectParams(String offerId) {
		return [SELECT Certificate__c, Limit_min__c, Limit_max__c, Active_from__c, Active_to__c, Leading_Language__c 
				FROM Offer__c 
				WHERE Id = :offerId 
				LIMIT 1];
	}
}