/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send activities to iPresso
**/

global without sharing class IntegrationIPressoSendActivities {
    
    @future(callout=true)
    public static void sendActivitiesListAndUpdateRecordsAtFuture(Id iPressoCampaignId, String activitiesToSend) {
        Boolean result = IntegrationIPressoSendActivities.sendActivitiesListSync(activitiesToSend);
        ActivityManager.updateSyncStatus_onFinish(iPressoCampaignId, result);
    }
    
    @future(callout=true)
    public static void sendActivitiesListAtFuture(String activitiesToSend) {
        IntegrationIPressoSendActivities.sendActivitiesListSync(activitiesToSend);
    }

    public static Boolean sendActivitiesListSync(String activitiesToSend) {
        if (activitiesToSend != null && activitiesToSend != '[]') {
            System.debug('activitiesToSend ' + activitiesToSend);
            HttpRequest httpReq = new HttpRequest();
        
            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
            
            String createDidactics = esb.Send_Activities_To_iPresso__c;
            String esbIp = esb.Service_Url__c;

            httpReq.setEndpoint(esbIp + createDidactics);
            httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
            httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
            httpReq.setBody(activitiesToSend);

            Http http = new Http();
            HTTPResponse httpRes = http.send(httpReq);

            Boolean success = httpRes.getStatusCode() == 201;
            if (!success) {
                ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoSendActivities.class.getName() 
                    + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + activitiesToSend);
            }

            return success;
        }

        return true;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- MODEL CLASSES ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class IPressoActivityContact_JSON {
    	public String ipresso_contact_id;
    	public List<IPressoActivity_JSON> activities;

    	public IPressoActivityContact_JSON(String ipresso_contact_id, List<IPressoActivity_JSON> activities) {
    		this.ipresso_contact_id = ipresso_contact_id;
    		this.activities = activities;
    	}
    }

    public class IPressoActivity_JSON {
    	public String activity_key;
    	public String activity_date;
    	public List<IPressoParameters_JSON> parameters;

    	public IPressoActivity_JSON(String activity_key, Datetime activity_date, List<IPressoParameters_JSON> parameters) {
    		this.activity_key = activity_key;
    		this.activity_date = activity_date.format('yyyy-MM-dd kk:mm:ss');
    		this.parameters = parameters;
    	}

        public IPressoActivity_JSON(String activity_key, Date activity_date, DateTime activity_time, List<IPressoParameters_JSON> parameters) {
            this.activity_key = activity_key;
            this.activity_date = datetime.newInstance(activity_date.year(), activity_date.month(),activity_date.day()).format('yyyy-MM-dd') + ' ' + activity_time.format('kk:mm:ss');
            this.parameters = parameters;
        }

        public IPressoActivity_JSON(String activity_key, Date activity_date, String activity_time, List<IPressoParameters_JSON> parameters) {
            this.activity_key = activity_key;
            List<String> activityTime = activity_time.split(':');
            String properActivityTime = DateTime.newInstance(activity_date.year(), activity_date.month(),activity_date.day(), Integer.valueOf(activityTime.get(0)), Integer.valueOf(activityTime.get(1)), 0).format('yyyy-MM-dd kk:mm:ss');
            this.activity_date = properActivityTime;
            this.parameters = parameters;
        }
    }

    public class IPressoParameters_JSON {
    	public String parameter_key;
    	public String parameter_value;

    	public IPressoParameters_JSON(String parameter_key, String parameter_value) {
    		this.parameter_key = parameter_key;
    		this.parameter_value = parameter_value;
    	}
    }

}