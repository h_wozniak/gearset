@isTest
private class ZTestOfferNewButtonOverride {

    @isTest static void testOfferNewButtonOverride() {
        PageReference pageRef = Page.OfferNewButtonOverride;
        Test.setCurrentPageReference(pageRef);
        
        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.standardController(new Offer__c());
        OfferNewButtonOverride cntrl = new OfferNewButtonOverride(stdController);
        Test.stopTest();

        System.assert(!cntrl.recordTypeList.isEmpty());

        System.assertNotEquals(null, cntrl.continueStep());
    }

}