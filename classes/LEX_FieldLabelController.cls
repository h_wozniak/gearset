public with sharing class LEX_FieldLabelController {

    @AuraEnabled
    public static FieldMetadata getFieldMetadata(String sobjectName, String fieldName) {
        return new FieldMetadata(sobjectName, fieldName);
    }

    public class FieldMetadata {
        @AuraEnabled
        public String label { get; set; }
        public Schema.DescribeFieldResult fieldDescribe { get; set; }
        @AuraEnabled public String inlineHelpText { get; set; }

        public FieldMetadata(String sobjectName, String fieldName) {
            fieldDescribe = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap().get(fieldName).getDescribe();
            label = fieldDescribe.getLabel();
            inlineHelpText = fieldDescribe.getInlineHelpText();
        }
    }
}