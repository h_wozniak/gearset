/**
* @description  This class is a Trigger Handler for Study Discounts - field edit disabling
**/

public without sharing class TH_DiscountDisables2 extends TriggerHandler.DelegateBase {

    List<Discount__c> discountsToCheckIfCanEdit;
    List<Discount__c> discountsToCheckIfCanDelete;
    List<Discount__c> discountsToCheckifCanDeleteOnActiveDisc;

    public override void prepareBefore() {
        discountsToCheckIfCanEdit = new List<Discount__c>();
        discountsToCheckIfCanDelete = new List<Discount__c>();
        discountsToCheckifCanDeleteOnActiveDisc = new List<Discount__c>();
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        for (Id key : oldDiscounts.keySet()) {
            Discount__c oldDiscount = oldDiscounts.get(key);
            Discount__c newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // on Discount edit check if it is allowed - depends on Enrollment stage
            if ((
                    newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                )
                && (
                	oldDiscount.Applied__c != newDiscount.Applied__c
                	|| oldDiscount.Discount_Kind__c != newDiscount.Discount_Kind__c
                	|| oldDiscount.Discount_Value__c != newDiscount.Discount_Value__c
                	|| oldDiscount.Applies_to__c != newDiscount.Applies_to__c
                	|| oldDiscount.Applied_through__c != newDiscount.Applied_through__c
            	)
            ) {
                discountsToCheckIfCanEdit.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Disable editing Account 
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_ACCOUNT_COMPANY_DC)
                && (newDiscount.Discount_from_Company_DC__c != oldDiscount.Discount_from_Company_DC__c
                || newDiscount.Company_from_Company_Discount__c != oldDiscount.Company_from_Company_Discount__c
                || newDiscount.Name != oldDiscount.Name)) {
                newDiscount.addError(Label.msg_error_cantEditDiscount);
            }

            /* Sebastian Łasisz */
            // Disable editing offer connectors
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)
                || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC))
                && newDiscount.Offer_from_Offer_DC__c != oldDiscount.Offer_from_Offer_DC__c) {
                newDiscount.addError(Label.msg_error_cantEditConnectorActive);
            }
        }
    }

    public override void beforeInsert(List<sObject> o) {
        List<Discount__c> newDiscountList = (List<Discount__c>)o;
        for(Discount__c newDiscount : newDiscountList) {

            /* Sebastian Łasisz */
            // on Discount edit check if can apply Entry Discount
            if (!CommonUtility.preventEditingDiscounts
                && newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                && newDiscount.Applied__c == true
                && newDiscount.Applies_to__c == CommonUtility.DISCOUNT_APPLIESTO_ENTRY) {
                discountsToCheckIfCanEdit.add(newDiscount);
            }
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        for (Id key : oldDiscounts.keySet()) {
            Discount__c oldDiscount = oldDiscounts.get(key);

            /* Sebastian Łasisz */
            // Disable removing offer connectors with attached enrollments
            if (oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)
                || oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC)) {
                discountsToCheckIfCanDelete.add(oldDiscount);
            }

            /* Sebastian Łasisz */
            // Disable removing offer connectors with active discounts
            if (oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)
                || oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC)) {
                discountsToCheckifCanDeleteOnActiveDisc.add(oldDiscount);
            }            
        }

    }


    public override void finish() {

        /* Wojciech Słodziak */
        // on Discount edit check if it is allowed - depends on Enrollment stage
        if (discountsToCheckIfCanEdit != null && !discountsToCheckIfCanEdit.isEmpty()) {
            DiscountManager_Studies.checkIfCanEditDiscountOnEnrollment(discountsToCheckIfCanEdit);
        }

        /* Sebastian Łasisz */
        // Disable removing offer connectors with active discounts
        if (discountsToCheckIfCanDelete != null && !discountsToCheckIfCanDelete.isEmpty()) {
            DiscountManager_Studies.checkIfCanDeleteDiscountOfferActiveDiscount(discountsToCheckIfCanDelete);
        }

        /* Sebastian Łasisz */
        // Disable removing offer connectors with attached enrollments
        if (discountsToCheckIfCanDelete != null && !discountsToCheckIfCanDelete.isEmpty()) {
            DiscountManager_Studies.checkIfCanDeleteDiscountOffer(discountsToCheckIfCanDelete);
        }
	}
}