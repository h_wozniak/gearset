public with sharing class LEX_Add_Price_Book_Controller {
	
	@AuraEnabled
	public static Boolean hasEditAccess(String offerId) {
		return LEXServiceUtilities.hasEditAccess(offerId);
	}

	@AuraEnabled
	public static Boolean hasCreateAccessToPriceBooks() {
		return LEXServiceUtilities.hasCreateAccessToPriceBooks();
	}

	@AuraEnabled
	public static String getPriceBookRTId(String offerId) {
		String result;
		Offer__c offer = [SELECT Degree__c FROM Offer__c WHERE Id = :offerId LIMIT 1];
		if (offer.Degree__c == 'PG') {
			result = CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG);
		} else if (offer.Degree__c == 'MBA') {
			result = CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA);
		} else {
			result = CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK);
		}
		return result;
	}
}