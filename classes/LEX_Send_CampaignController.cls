public with sharing class LEX_Send_CampaignController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String sendContactsToIPresso(String recordId) {
		return LEXStudyUtilities.sendContactsToIPresso(recordId) ? 'SUCCESS' : 'FAILURE';
	}
}