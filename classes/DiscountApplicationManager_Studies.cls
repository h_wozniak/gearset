/**
* @author       Wojciech Słodziak, Sebastian Łasisz
* @description  Class for computing applied Discounts into Paymetn Schedule Installments
**/

public without sharing class DiscountApplicationManager_Studies {


    public static Map<String, String> discountToPaymentTypeMatcher = new Map<String, String> {
            CommonUtility.DISCOUNT_APPLIESTO_ENTRY => CommonUtility.PAYMENT_TYPE_ENTRY_FEE,
            CommonUtility.DISCOUNT_APPLIESTO_TUITION => CommonUtility.PAYMENT_TYPE_TUITION_FEE,
            CommonUtility.DISCOUNT_APPLIESTO_QUALIFYING => CommonUtility.PAYMENT_TYPE_QUALIFYING_FEE,
            CommonUtility.DISCOUNT_APPLIESTO_ENROLLMENT => CommonUtility.PAYMENT_TYPE_ENROLLMENT_FEE
    };

    public static Set<String> timeDiscountTypeForDenial = new Set<String> {
            CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE,
            CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE,
            CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            CommonUtility.DISCOUNT_TYPEST_VIS,
            CommonUtility.DISCOUNT_TYPEST_EMPLOYEEDISCOUNT
    };

    public static Set<String> companyDiscountTypeForDenial = new Set<String> {
            CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE,
            CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE,
            CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            CommonUtility.DISCOUNT_TYPEST_VIS,
            CommonUtility.DISCOUNT_TYPEST_EMPLOYEEDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_CODE
    };

    public static Set<String> codeDiscountTypeForDenial = new Set<String> {
            CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE,
            CommonUtility.DISCOUNT_TYPEST_CHANCELLOR,
            CommonUtility.DISCOUNT_TYPEST_VIS,
            CommonUtility.DISCOUNT_TYPEST_EMPLOYEEDISCOUNT,
            CommonUtility.DISCOUNT_TYPEST_CODE
    };

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- DISCOUNT CALCULATION ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */


    /* Wojciech Słodziak */
    // Edtitor: Sebastian Łasisz
    // method called from triggers to apply discount value to Payment Schedule
    public static void applyDiscountsToPaymentSchedule(Set<Id> enrIds) {
        Map<Id, Payment__c> paymentInstallmentsToUpdate = new Map<Id, Payment__c>();
        List<Discount__c> paymentDiscountConnList = new List<Discount__c>();

        Map<Id, Enrollment__c> enrWithInstallmentsMap = new Map<Id, Enrollment__c>([
                SELECT Id, Number_of_Semesters__c, Installments_per_Year__c, Tuition_System__c, Unenrolled__c, Starting_Semester__c,
                (SELECT Id, Enrollment_from_Schedule_Installment__c, PriceBook_Value__c, Discount_Value__c, Type__c, Installment_Year__c,
                        Installments_this_Year__c, Installment_Variant__c, Installment_Semester__c, Unenrolled_Installment__c, Description__c
                FROM ScheduleInstallments__r
                ORDER BY Installment_Serial_Number__c ASC NULLS FIRST)
                FROM Enrollment__c
                WHERE Id IN :enrIds
        ]);

        List<Discount__c> discountConnList = [
                SELECT Id, Enrollment__c, Discount_Value__c, Discount_Kind__c, Applies_to__c, Applied_through__c,
                        Discount_Type_Studies__c, Name
                FROM Discount__c
                WHERE Enrollment__c IN :enrIds AND Applied__c = true
                AND Discount_Value__c != null AND Discount_Kind__c != null AND Applies_to__c != null
                ORDER BY Discount_Kind__c DESC, Discount_Value__c ASC
        ];

        Map<Id, Decimal> unenrolledDiscountAmountApplied = new Map<Id, Decimal>();

        // reset Inst Discounts and add to paymentInstallmentsToUpdate if modified
        for (Enrollment__c enrWithInstallments : enrWithInstallmentsMap.values()) {
            for (Payment__c paymentInst : enrWithInstallments.ScheduleInstallments__r) {
                if (paymentInst.Discount_Value__c != 0) {
                    paymentInst.Discount_Value__c = 0;
                    paymentInstallmentsToUpdate.put(paymentInst.Id, paymentInst);
                }
            }
        }

        // aplly Discounts
        for (Discount__c discount : discountConnList) {
            Enrollment__c enrWithIntallments = enrWithInstallmentsMap.get(discount.Enrollment__c);
            List<Discount__c> paymentToThisDiscConnList = new List<Discount__c>();
            List<Payment__c> paymentsApplicableForDiscount = DiscountApplicationManager_Studies.getApplicablePaymentInstallments(discount, enrWithIntallments.ScheduleInstallments__r, enrWithIntallments);

            // create Map of Payments for Discount
            Set<Decimal> numberOfSemestersForEnrollment = new Set<Decimal>();
            Decimal numberOfSemestersOverAll = enrWithIntallments.Number_of_Semesters__c - (Integer.valueOf(enrWithIntallments.Starting_Semester__c) - 1);
            Decimal numberOfPaymentsOnEnrollment = enrWithIntallments.Installments_per_Year__c != null ? Decimal.valueOf(enrWithIntallments.Installments_per_Year__c) : 0;
            Map<Decimal, List<Payment__c>> schedulePaymentsBasedOnSemester = new Map<Decimal, List<Payment__c>>();
            for (Payment__c payment : paymentsApplicableForDiscount) {
                Decimal academicYear;
                if (payment.Installment_Semester__c != null) {
                    academicYear =  math.mod(Integer.valueOf(payment.Installment_Semester__c), 2) == 0 ? (payment.Installment_Semester__c / 2) : (payment.Installment_Semester__c + 1) / 2 ;
                }
                else {
                    academicYear = 1;
                }

                // create Set of Academic Year of Payments for Discount
                if (!numberOfSemestersForEnrollment.contains(payment.Installment_Semester__c)) {
                    numberOfSemestersForEnrollment.add(payment.Installment_Semester__c);
                }

                // create Map of Payments for Discount per semester
                if (!schedulePaymentsBasedOnSemester.containsKey(payment.Installment_Semester__c)) {
                    schedulePaymentsBasedOnSemester.put(payment.Installment_Semester__c, new List<Payment__c>());
                }
                schedulePaymentsBasedOnSemester.get(payment.Installment_Semester__c).add(payment);
            }

            DiscountApplicationManager_Studies.applyDiscountToListOfInstallments(numberOfSemestersForEnrollment, schedulePaymentsBasedOnSemester, discount, paymentToThisDiscConnList, paymentsApplicableForDiscount, paymentInstallmentsToUpdate, unenrolledDiscountAmountApplied, numberOfSemestersOverAll, numberOfPaymentsOnEnrollment, enrWithIntallments.Tuition_System__c);

            paymentDiscountConnList.addAll(paymentToThisDiscConnList);
        }

        //apply total rounding after all Discounts are applied
        for (Enrollment__c enrWithInstallments : enrWithInstallmentsMap.values()) {
            DiscountApplicationManager_Studies.applyTotalRoundingAndCreateRoundingRecords(paymentDiscountConnList, enrWithInstallments.ScheduleInstallments__r);
        }

        try {

            DiscountManager_Studies.removePaymentDiscConnFromScheduleInstallments(paymentInstallmentsToUpdate.keySet());

            update paymentInstallmentsToUpdate.values();

            insert paymentDiscountConnList;

        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Wojciech Słodziak */
    // Edtitor: Sebastian Łasisz
    // helper method for applyDiscountsToPaymentSchedule, universal method for applying Amount/Percentage Discounts to Payment Installments
    private static void applyDiscountToListOfInstallments(Set<Decimal> numberOfSemestersForEnrollment, Map<Decimal, List<Payment__c>> schedulePaymentsBasedOnSemester,
            Discount__c discount, List<Discount__c> paymentDiscountConnList, List<Payment__c> applicableInstList, Map<Id, Payment__c> modifiedInstMap,
            Map<Id, Decimal> unenrolledDiscountAmountApplied, Decimal numberOfSemestersOverAll, Decimal numberOfPaymentsOnEnrollment, String tuitionSystem) {

        if (applicableInstList.isEmpty()) {
            return;
        }

        Integer numberOfYearsForPayments = numberOfSemestersForEnrollment.size();

        Boolean isAmountDiscount = discount.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT;

        // calculate Installment's applicable value Sum, for Percent Discounts calculated from Discount Val after previous Discounts
        Decimal instApplicableValSum = 0;
        for (Payment__c paymentInst : applicableInstList) {
            instApplicableValSum += (isAmountDiscount? paymentInst.PriceBook_Value__c : paymentInst.PriceBook_Value__c - paymentInst.Discount_Value__c);
        }

        Decimal discountAmount;

        if (isAmountDiscount) {
            discountAmount = discount.Discount_Value__c;
        } else {
            if (discount.Applies_to__c != CommonUtility.PAYMENT_TYPE_ENTRY_FEE
                    && discount.Applies_to__c != CommonUtility.PAYMENT_TYPE_QUALIFYING_FEE
                    && discount.Applies_to__c != CommonUtility.PAYMENT_TYPE_ENROLLMENT_FEE
                    && discount.Applied_through__c == CommonUtility.DISCOUNT_APPLIEDTHR_1STSEM && !applicableInstList.isEmpty()
                    && Integer.valueOf(applicableInstList[0].Installment_Variant__c) == 1) {
                discount.Discount_Value__c *= 0.5;
            }

            discountAmount = (instApplicableValSum * discount.Discount_Value__c / 100).setScale(2, RoundingMode.HALF_UP);
        }

        Decimal discountOverflow = 0;

        for (Payment__c paymentInst : applicableInstList) {
            Decimal discountableValueLeft = paymentInst.PriceBook_Value__c - paymentInst.Discount_Value__c;

            Decimal instValToSumRatio = 0;
            if (instApplicableValSum != 0) {
                instValToSumRatio = (isAmountDiscount? paymentInst.PriceBook_Value__c : paymentInst.PriceBook_Value__c - paymentInst.Discount_Value__c)
                        / instApplicableValSum;
            }

            Integer numberOfPaymentsForInstallment = schedulePaymentsBasedOnSemester.get(paymentInst.Installment_Semester__c).size();

            Decimal instDiscount;
            Decimal instDiscountRounded;

            if (!isAmountDiscount) {
                instDiscount = discountAmount * instValToSumRatio;
                instDiscountRounded = instDiscount.setScale(2, RoundingMode.HALF_UP);
            }
            else {
                if (paymentInst.Type__c == CommonUtility.PAYMENT_TYPE_TUITION_FEE && discount.Applied_through__c == CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE && (tuitionSystem != CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME)) {
                    if ((paymentInst.Installment_Semester__c == 1 && numberOfPaymentsOnEnrollment == 1 && numberOfSemestersOverAll > 1)
                            || (paymentInst.Installment_Semester__c == 3 && numberOfPaymentsOnEnrollment == 1 && numberOfSemestersOverAll > 1 && numberOfSemestersOverAll != 3)
                            || (paymentInst.Installment_Semester__c == 5 && numberOfPaymentsOnEnrollment == 1 && numberOfSemestersOverAll > 1 && numberOfSemestersOverAll != 5)) {
                        instDiscount = (((discountAmount / (numberOfSemestersOverAll)) / numberOfPaymentsForInstallment));
                        instDiscountRounded = instDiscount.setScale(0, RoundingMode.CEILING) * 2;
                    }
                    else if (numberOfPaymentsForInstallment == 1) {
                        instDiscount = ((discountAmount / (numberOfSemestersOverAll)) / numberOfPaymentsForInstallment);
                        instDiscountRounded = instDiscount.setScale(2, RoundingMode.HALF_UP);
                    }
                    else {
                        instDiscount = ((discountAmount / numberOfYearsForPayments) / numberOfPaymentsForInstallment);
                        instDiscountRounded = instDiscount.setScale(2, RoundingMode.HALF_UP);
                    }
                }
                else {
                    instDiscount = discountAmount * instValToSumRatio;
                    instDiscountRounded = instDiscount.setScale(2, RoundingMode.HALF_UP);
                }
            }

            Decimal appliedDiscount;
            if (discountableValueLeft >= instDiscountRounded) {
                appliedDiscount = instDiscountRounded;
                paymentInst.Discount_Value__c += appliedDiscount;
            } else {
                appliedDiscount = discountableValueLeft;
                paymentInst.Discount_Value__c += appliedDiscount;
                discountOverflow += (instDiscountRounded - appliedDiscount);
            }

            Discount__c paymentDiscountConn = new Discount__c();
            paymentDiscountConn.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR);
            paymentDiscountConn.Payment_from_Payment_DC__c = paymentInst.Id;
            paymentDiscountConn.Discount_from_Payment_DC__c = discount.Id;
            paymentDiscountConn.Discounted_Value__c = appliedDiscount;

            paymentDiscountConnList.add(paymentDiscountConn);
            modifiedInstMap.put(paymentInst.Id, paymentInst);
        }

        //DiscountApplicationManager_Studies.applyOverflowInReverseOrder(paymentDiscountConnList, applicableInstList, discountOverflow);
    }

    /* Wojciech Słodziak */
    // helper method for applyDiscountsToPaymentSchedule, returns list of Installments that are covered by Discount 
    private static List<Payment__c> getApplicablePaymentInstallments(Discount__c discount, List<Payment__c> paymentInstList, Enrollment__c enr) {
        List<Payment__c> applicableInstList = new List<Payment__c>();
        for (Payment__c paymentInst : paymentInstList) {
            if (
                    DiscountApplicationManager_Studies.discountToPaymentTypeMatcher.get(discount.Applies_to__c) == paymentInst.Type__c
                            && (
                            (
                                    (paymentInst.Type__c == CommonUtility.PAYMENT_TYPE_ENTRY_FEE
                                            || paymentInst.Type__c == CommonUtility.PAYMENT_TYPE_QUALIFYING_FEE
                                            || paymentInst.Type__c == CommonUtility.PAYMENT_TYPE_ENROLLMENT_FEE)
                                            || discount.Applied_through__c == CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE
                                            || (discount.Applied_through__c == CommonUtility.DISCOUNT_APPLIEDTHR_1STSEM
                                            && paymentInst.Installment_Semester__c == Integer.valueOf(enr.Starting_Semester__c))
                                            || (discount.Applied_through__c == CommonUtility.DISCOUNT_APPLIEDTHR_1STY
                                            && (paymentInst.Installment_Semester__c == Integer.valueOf(enr.Starting_Semester__c)
                                            || paymentInst.Installment_Semester__c == (Integer.valueOf(enr.Starting_Semester__c) + 1) ))

                            )
                    )
                    ) {
                applicableInstList.add(paymentInst);
            }
        }

        return applicableInstList;
    }

    /* Wojciech Słodziak */
    // helper method for applyDiscountsToPaymentSchedule, method applies Discount overflow to Payment Installments in reverse order
    private static void applyOverflowInReverseOrder(List<Discount__c> paymentDiscountConnList, List<Payment__c> applicableInstList, Decimal discountOverflow) {

        if (discountOverflow != 0) {
            for (Integer index = applicableInstList.size() - 1; index >= 0 && discountOverflow != 0; index--) {
                Payment__c paymentInst = applicableInstList[index];
                Discount__c paymentDiscountConn = paymentDiscountConnList[index];

                Decimal appliedDifference;

                if (discountOverflow > 0) {
                    Decimal discountableValueLeft = paymentInst.PriceBook_Value__c - paymentInst.Discount_Value__c;

                    if (discountableValueLeft >= discountOverflow) {
                        appliedDifference = discountOverflow;
                        paymentInst.Discount_Value__c += discountOverflow;
                        discountOverflow = 0;
                    } else {
                        appliedDifference = discountableValueLeft;
                        paymentInst.Discount_Value__c = appliedDifference;
                        discountOverflow -= appliedDifference;
                    }

                }

                paymentDiscountConn.Discounted_Value__c += appliedDifference;
            }
        }
    }

    /* Wojciech Słodziak */
    // helper method for applyDiscountsToPaymentSchedule, 
    // method applies rounding to Total Discount on Payment Installment and creates Discount Connector on Installment which informs about applied rounding
    private static void applyTotalRoundingAndCreateRoundingRecords(List<Discount__c> discountConnList, List<Payment__c> paymentInstList) {
        List<Discount__c> roundingRecordList = new List<Discount__c>();

        for (Payment__c paymentInst : paymentInstList) {
            Decimal discountAfterRounding = paymentInst.Discount_Value__c.setScale(0, RoundingMode.CEILING);
            Decimal roundingApplied = discountAfterRounding - paymentInst.Discount_Value__c;
            paymentInst.Discount_Value__c = discountAfterRounding;

            if (roundingApplied > 0) {
                Discount__c roundingRecord = new Discount__c();
                roundingRecord.Name = Label.title_Rounding;
                roundingRecord.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PAYMENT_DISCOUNT_CONNECTOR);
                roundingRecord.Payment_from_Payment_DC__c = paymentInst.Id;
                roundingRecord.Discounted_Value__c = roundingApplied;

                roundingRecordList.add(roundingRecord);
            }
        }

        discountConnList.addAll(roundingRecordList);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ DISCOUNT MANIPULATION ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * Method called from trigger that's executed when button to add company discounts is run.
     * @author Sebastian Lasisz
     *
     * @param newDiscounts List of company discounts to determine which related discounts should be disapplied.
     */
    public static void dissaplyOtherDiscounts(List<Discount__c> newDiscounts) {
        Set<Id> enrollmentIds = new Set<Id>();
        Set<Id> discountIds = new Set<Id>();
        for (Discount__c newDiscount : newDiscounts) {
            enrollmentIds.add(newDiscount.Enrollment__c);
            discountIds.add(newDiscount.Id);
        }

        discountIds.remove(null);

        List<Discount__c> appliedDiscountsOnEnrollment = [
                SELECT Id, Discount_Type_Studies__c, Enrollment__c, Applies_to__c
                FROM Discount__c
                WHERE Enrollment__c IN :enrollmentIds
                AND Applied__c = true
                AND Id NOT IN :discountIds
        ];

        List<Discount__c> discountsToUpdate = new List<Discount__c>();
        for (Discount__c appliedDiscount : appliedDiscountsOnEnrollment) {
            for (Discount__c newDisc : newDiscounts) {
                if (appliedDiscount.Enrollment__c == newDisc.Enrollment__c && appliedDiscount.Applies_to__c == newDisc.Applies_to__c) {
                    appliedDiscount.Applied__c = false;
                    discountsToUpdate.add(appliedDiscount);
                }
            }
        }

        try {
            update discountsToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // when newly created discount is added disapply non matching discounts
    public static void manageAppliedDiscounts(List<Discount__c> newDiscounts) {
        Set<Id> enrollmentIds = new Set<Id>();
        Set<Id> discountIds = new Set<Id>();
        for (Discount__c newDiscount : newDiscounts) {
            enrollmentIds.add(newDiscount.Enrollment__c);
            discountIds.add(newDiscount.Id);
        }

        discountIds.remove(null);

        List<Discount__c> appliedDiscountsOnEnrollment = [
                SELECT Id, Discount_Type_Studies__c, Enrollment__c, Applies_to__c
                FROM Discount__c
                WHERE Enrollment__c IN :enrollmentIds
                AND Applied__c = true
                AND Id NOT IN :discountIds
        ];

        List<Discount__c> discountsToUpdate = new List<Discount__c>();
        for (Discount__c appliedDiscount : appliedDiscountsOnEnrollment) {
            for (Discount__c newDisc : newDiscounts) {
                if (appliedDiscount.Enrollment__c == newDisc.Enrollment__c && appliedDiscount.Applies_to__c == newDisc.Applies_to__c) {
                    if (
                            ((appliedDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT
                                    || appliedDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE)
                                    && timeDiscountTypeForDenial.contains(newDisc.Discount_Type_Studies__c)
                            )
                                    || (appliedDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT
                                    && companyDiscountTypeForDenial.contains(newDisc.Discount_Type_Studies__c)
                            )
                                    || (appliedDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_CODE
                                    && codeDiscountTypeForDenial.contains(newDisc.Discount_Type_Studies__c)
                            )
                            ) {
                        appliedDiscount.Applied__c = false;
                        discountsToUpdate.add(appliedDiscount);
                    }
                }
            }
        }

        try {
            update discountsToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

}