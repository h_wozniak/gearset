@isTest
private class ZTestEmailManager_UnenrolledDocList {

    private static Map<Integer, sObject> prepareDataForTest_Statistics(Boolean withSpecialties) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));

        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = retMap.get(0).Id
        ), true));

        if (withSpecialties) {
            retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                    Course_Offer_from_Specialty__c = retMap.get(1).Id
            ), true));
        }

        return retMap;
    }

    private static void prepareTemplates() {
        EmailTemplate defaultEmailTemplate = new EmailTemplate();
        defaultEmailTemplate.isActive = true;
        defaultEmailTemplate.Name = '[PL][24] Przypomnienie o konieczności dostarczenia dokumentów dla WS';
        defaultEmailTemplate.DeveloperName = 'PL_' + EmailManager_UnenrolledDocListReminder.emailTemplateName;
        defaultEmailTemplate.TemplateType = 'text';
        defaultEmailTemplate.FolderId = UserInfo.getUserId();
        defaultEmailTemplate.Subject = 'x';

        insert defaultEmailTemplate;
    }

   /* @isTest static void test_SendRequiredDocumentList() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        ZDataTestUtility.prepareCatalogData(true, true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        Test.startTest();

        Date unenrolledDate = System.today().addDays(4).addMonths(-1);
        String month = (unenrolledDate.month() < 10) ? ('0' + unenrolledDate.month()) : String.valueOf(unenrolledDate.month());
        String day = (unenrolledDate.day() < 10) ? ('0' + unenrolledDate.day()) : String.valueOf(unenrolledDate.day());
		Date testDate = System.today();
        String buildDate = day + '.' + month;
        ZDataTestUtility.createOfferDateAssigment(new Catalog__c(Unenrolled_Agreement_Till__c = buildDate), true);

        studyEnr.Degree__c = CommonUtility.OFFER_DEGREE_II;
        studyEnr.Unenrolled__c = true;
        studyEnr.Study_Start_Date__c = System.today();
        
        update studyEnr;
		
        List<Enrollment__c> enrList = [
                SELECT Id, Study_Start_Date__c, Language_of_Enrollment__c, University_Name__c, Unenrolled__c, WasIs_Unenrolled__c, Degree__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND (Unenrolled__c = true
                OR WasIs_Unenrolled__c = true)
                AND (Degree__c = :CommonUtility.OFFER_DEGREE_II
                OR Degree__c = :CommonUtility.OFFER_DEGREE_II_PG)
                AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
                OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
        ];
		System.debug('studyEnr.Study_Start_Date__c ' + enrList.get(0).Study_Start_Date__c);
        Map<Id, Date> generatedMap = EmailManager_UnenrolledDocListReminder.generateUnenrolledAgreementTill(enrList);
        
        System.assertEquals(generatedMap.get(enrList.get(0).Id), System.today());

        System.assertEquals(enrList.size(), 1);
        System.assertEquals(enrList.get(0).Unenrolled__c, true);

        UnenrolledDocReminderBatch unenrolledDatesBatch = new UnenrolledDocReminderBatch(0);
        Database.executebatch(unenrolledDatesBatch, 100);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_SendRequiredDocumentList_2() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        ZDataTestUtility.prepareCatalogData(true, true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        Test.startTest();

        Date unenrolledDate = System.today().addDays(5);

        String month = (unenrolledDate.month() < 10) ? ('0' + unenrolledDate.month()) : String.valueOf(unenrolledDate.month());
        String day = (unenrolledDate.day() < 10) ? ('0' + unenrolledDate.day()) : String.valueOf(unenrolledDate.day());

        String buildDate = day + '.' + month;

        if(System.today().month() >=8 && System.today().month()<=3) {
            ZDataTestUtility.createOfferDateAssigment(new Catalog__c(Unenrolled_Agreement_Till__c = buildDate, Period_of_Recrutation__c = CommonUtility.CATALOG_PERIOD_SUMMER), true);
        }
        else {
            ZDataTestUtility.createOfferDateAssigment(new Catalog__c(Unenrolled_Agreement_Till__c = buildDate), true);
        }

        studyEnr.Degree__c = CommonUtility.OFFER_DEGREE_II;
        studyEnr.Unenrolled__c = true;
        studyEnr.Study_Start_Date__c = System.today();
        update studyEnr;

        List<Enrollment__c> enrList = [
                SELECT Id, Study_Start_Date__c, Language_of_Enrollment__c, University_Name__c, Unenrolled__c, WasIs_Unenrolled__c, Degree__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND (Unenrolled__c = true
                OR WasIs_Unenrolled__c = true)
                AND (Degree__c = :CommonUtility.OFFER_DEGREE_II
                OR Degree__c = :CommonUtility.OFFER_DEGREE_II_PG)
                AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
                OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
        ];

        Map<Id, Date> generatedMap = EmailManager_UnenrolledDocListReminder.generateUnenrolledAgreementTill(enrList);
        System.assertNotEquals(generatedMap.get(enrList.get(0).Id), System.today());
        System.assertEquals(generatedMap.get(enrList.get(0).Id), System.today().addDays(1));

        System.assertEquals(enrList.size(), 1);
        System.assertEquals(enrList.get(0).Unenrolled__c, true);

        UnenrolledDocReminderBatch unenrolledDatesBatch = new UnenrolledDocReminderBatch(0);
        Database.executebatch(unenrolledDatesBatch, 100);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(taskList.isEmpty());
    }*/

    @isTest static void test_SendRequiredDocumentList_3() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        ZDataTestUtility.prepareCatalogData(true, true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        Test.startTest();

        Date unenrolledDate = System.today().addDays(4);

        String month = (unenrolledDate.month() < 10) ? ('0' + unenrolledDate.month()) : String.valueOf(unenrolledDate.month());
        String day = (unenrolledDate.day() < 10) ? ('0' + unenrolledDate.day()) : String.valueOf(unenrolledDate.day());

        String buildDate = day + '.' + month;

        ZDataTestUtility.createOfferDateAssigment(new Catalog__c(Unenrolled_Agreement_Till__c = buildDate, Entity__c = 'WSB Bydgoszcz'), true);

        studyEnr.Degree__c = CommonUtility.OFFER_DEGREE_II;
        studyEnr.Unenrolled__c = true;
        studyEnr.Study_Start_Date__c = System.today();
        update studyEnr;

        List<Enrollment__c> enrList = [
                SELECT Id, Study_Start_Date__c, Language_of_Enrollment__c, University_Name__c, Unenrolled__c, WasIs_Unenrolled__c, Degree__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND (Unenrolled__c = true
                OR WasIs_Unenrolled__c = true)
                AND (Degree__c = :CommonUtility.OFFER_DEGREE_II
                OR Degree__c = :CommonUtility.OFFER_DEGREE_II_PG)
                AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
                OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
        ];

        Map<Id, Date> generatedMap = EmailManager_UnenrolledDocListReminder.generateUnenrolledAgreementTill(enrList);
        System.assertEquals(generatedMap.get(enrList.get(0).Id), null);

        System.assertEquals(enrList.size(), 1);
        System.assertEquals(enrList.get(0).Unenrolled__c, true);

        UnenrolledDocReminderBatch unenrolledDatesBatch = new UnenrolledDocReminderBatch(0);
        Database.executebatch(unenrolledDatesBatch, 100);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(taskList.isEmpty());
    }

    @isTest static void test_getAdditionalDocAttMap_offer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.size(), 1);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_course() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        System.assertEquals(enrollmentList.size(), 1);

        List<Offer__c> additionalDocumentsOfferList = EmailManager_UnenrolledDocumentListEmail.prepareOfferIdSetFromEnrollment(enrollmentList);
        System.assertEquals(additionalDocumentsOfferList.size(), 1);

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_courseOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDoc2 = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment2 = new Attachment();
        newAttachment2.Name = 'Unit Test Attachment';
        newAttachment2.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment2.ParentId = additionalDoc2.Additional_Document_from_Offer__c;

        insert newAttachment2;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        List<Offer__c> additionalDocumentsOfferList = EmailManager_UnenrolledDocumentListEmail.prepareOfferIdSetFromEnrollment(enrollmentList);
        System.assertEquals(additionalDocumentsOfferList.size(), 2);

        System.debug('additionalDocumentsOfferList ' + additionalDocumentsOfferList);

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.debug('attachmentMap ' + attachmentMap);
        System.debug(additionalOfferDocument.Id);
        System.debug(additionalDoc2.Id);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 2);

    }

    @isTest static void test_getAdditionalDocAttMap_specialty() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);

    }

    @isTest static void test_getAdditionalDocAttMap_specialtyOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_specialtyAndOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c addDoc2 = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), true);

        Test.startTest();

        Attachment newAttachment2 = new Attachment();
        newAttachment2.Name = 'Unit Test Attachment';
        newAttachment2.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment2.ParentId = addDoc2.Additional_Document_from_Offer__c;

        insert newAttachment2;
        
        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);
        
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 2);
    }

    @isTest static void test_getAdditionalDocAttMap_specialtyCourseOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        List<Offer__c> additionalDocuments = new List<Offer__c>();

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), false));

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), false));

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), false));

        insert additionalDocuments;

        List<Attachment> atts = new List<Attachment>();
        for (Offer__c off : additionalDocuments) {
            Attachment newAttachment = new Attachment();
            newAttachment.Name = 'Unit Test Attachment';
            newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
            newAttachment.ParentId = off.Additional_Document_from_Offer__c;

            atts.add(newAttachment);
        }

        insert atts;

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_UnenrolledDocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 3);
        Test.stopTest();
    }

}