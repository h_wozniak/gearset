@isTest
private class ZTestLEX_SendEmaiToHighSchoolController {

    @isTest static void test_sendEmail() {
        System.assertEquals(LEX_SendEmaiToHighSchoolController.sendWorkshopProposal(null), true);
    }

    @isTest static void test_sendEmail2() {
        Account highSchool = ZDataTestUtility.createHighSchool(null, true);
        System.assertEquals(LEX_SendEmaiToHighSchoolController.sendWorkshopProposal(highSchool.Id), true);
    }
}