@isTest
private class ZTestDocDeliveryMoreThanSevenDays {
    
    @isTest static void testDocDeliveryLessThanSevenDays() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789', A_Level__c = CommonUtility.CONTACT_ALEVEL_NOTPASSED), true));
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(1).Id), true));
        
        Test.startTest();
        
        retMap.put(5, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Documents_Collected__c = false,
            VerificationDate__c = Date.today().addDays(-14),
            Delivery_Deadline__c = Date.today().addDays(-6),
            Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED,
            RequiredDocListEmailedCount__c = 1,
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_CRM,
            Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH),
        true));

        Enrollment__c studyEnr = [
            SELECT Id, Status__c, RequiredDocListEmailedCount__c, Documents_Delivered__c, Remaining_Days_For_Document_Delivery__c, Dont_send_automatic_emails__c, Delivery_Deadline__c
            FROM Enrollment__c
            WHERE Id = :retMap.get(5).Id
        ];

        System.assertEquals(studyEnr.Status__c, CommonUtility.ENROLLMENT_STATUS_CONFIRMED);
        System.assertEquals(studyEnr.RequiredDocListEmailedCount__c, 1);
        System.assertEquals(studyEnr.Documents_Delivered__c, false);
        System.assert(studyEnr.Remaining_Days_For_Document_Delivery__c > 7);
        System.assertEquals(studyEnr.Dont_send_automatic_emails__c, false);
        System.assertEquals(studyEnr.Delivery_Deadline__c, Date.today().addDays(7));

        List<Enrollment__c> studyEnrs = [
            SELECT Id
            FROM Enrollment__c 
            WHERE Documents_Delivered__c = false
            AND Remaining_Days_For_Document_Delivery__c > 7
            AND Delivery_Deadline__c = :Date.today().addDays(7)
            AND Unenrolled__c = false
            AND RequiredDocListEmailedCount__c = 1
            AND RequiredDocListEmailedCount__c < 3
            AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
            AND Dont_send_automatic_emails__c = false
        ];

        System.assertEquals(studyEnrs.size(), 1);

        Database.executeBatch(new DocDeliveryMoreThanSevenDaysRemindBatch(7, 1));
        Test.stopTest();

        List<Task> tasksList = [SELECT Id FROM Task];
        System.assert(tasksList.size() > 0);
    }
}