public class CustomLookUpController {
    public class LookupObject{
        @AuraEnabled
        public String id;
        @AuraEnabled
        public String name;

        LookupObject(Id id, Object name){
            this.id = String.valueOf(id);
            this.name = String.valueOf(name);
        }
    }

    @AuraEnabled
    public static List < LookupObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);

        String searchKey = '%' + searchKeyWord + '%';

        List < LookupObject > returnList = new List < LookupObject > ();

        if(!objectName.equalsIgnoreCase('CASE')){
            String sQuery =  'SELECT id, Name FROM ' + ObjectName + ' WHERE Name LIKE: searchKey ORDER BY createdDate DESC limit 5';
            List < sObject > lstOfRecords = Database.query(sQuery);
            for (sObject obj: lstOfRecords) {
                returnList.add(new LookupObject(obj.Id, obj.get('Name')));
                System.debug(String.valueOf(obj.get('Name')));
            }
        }else{
            String sQuery =  'SELECT id, CaseNumber FROM ' + ObjectName + ' WHERE CaseNumber LIKE :searchKey ORDER BY createdDate DESC limit 5';
            List < sObject > lstOfRecords = Database.query(sQuery);
            for (sObject obj: lstOfRecords) {
                returnList.add(new LookupObject(obj.Id, obj.get('CaseNumber')));
                System.debug(String.valueOf(obj.get('CaseNumber')));
            }
        }

        return returnList;
    }

    @AuraEnabled
    public static LookupObject receiveLookupObject(String recordId, String objectName){
        if(!objectName.equalsIgnoreCase('CASE')){
            String query = 'SELECT Id, Name FROM ' + objectName + ' WHERE Id=:recordId';
            sObject record = Database.query(query);
            return new LookupObject(record.Id, record.get('Name'));
        }else{
            String query = 'SELECT Id, CaseNumber FROM ' + objectName + ' WHERE Id=:recordId';
            sObject record = Database.query(query);
            return new LookupObject(record.Id, record.get('CaseNumber'));
        }
    }
}