/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Consents for iPresso
**/


global without sharing class IntegrationIPressoConsentSync {

    @future(callout=true)
    public static void syncConsentsWithIPressoAsync(Id consentId, String consentName) {
        IntegrationIPressoConsentSync.syncConsentsWithIPresso(consentId, consentName);
    }

    public static Boolean syncConsentsWithIPresso(Id consentId, String consentName) {       
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        String jsonToSend = prepareConsentData(consentId, consentName);
        String endPoint = esb.Sync_Consents_With_iPresso__c;

                  
        HttpRequest httpReq = new HttpRequest();
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + endPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        httpReq.setBody(jsonToSend);

        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = httpRes.getStatusCode() == 200;
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoConsentSync.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody());
        }

        ACKReceiver_JSON result = parse(httpRes.getBody());

        Catalog__c consentToUpdate = [
            SELECT Id, IPressoId__c
            FROM Catalog__c
            WHERE Id = :result.crm_consent_id
        ];

        consentToUpdate.IPressoId__c = result.ipresso_consent_id;

        try {
            update consentToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

        return success;
    }
    
    global static String prepareConsentData(Id consentId, String consentName) {
        Catalog__c consent = [
            SELECT Id, IPressoId__c, Name, Content__c
            FROM Catalog__c
            WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            OR RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENT_LAGNAUGE))
            AND Id = :consentId
        ];

        MarketingConsent_JSON consentJSON = new MarketingConsent_JSON(consent.IPressoId__c, consent.Id, consentName, consent.Content__c);

        return JSON.serialize(consentJSON);
    }
    
    private static ACKReceiver_JSON parse(String jsonBody) {
        return (ACKReceiver_JSON) System.JSON.deserialize(jsonBody, ACKReceiver_JSON.class);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class MarketingConsent_JSON {
        public String ipresso_consent_id { get; set; }
        public String crm_consent_id { get; set; }
        public String consent_name { get; set; }
        public String consent_content { get; set; }

        public MarketingConsent_JSON(String consentId, String crmId, String consentName, String consentContent) {
            ipresso_consent_id = consentId;
            crm_consent_id = crmId;
            consent_name = consentName;
            consent_content = consentContent;
        }
    }

    @TestVisible
    public class ACKReceiver_JSON {
        public String status_code;
        public String ipresso_consent_id;
        public String consent_name;
        public String crm_consent_id;
    }
}