public class ZDataTestUtilityMethods {

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- ZTestLEX_AddManualDiscount ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Map<Integer, sObject> LEX_AMD_prepareDataForTest(Boolean pbActive, Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5
        ), true));

        retMap.put(1, ZDataTestUtility.createCandidateContact(null, true));

        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = pbActive
        ), true));

        configPriceBook(retMap.get(2).Id, 10.0);

        Test.startTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(
                new Enrollment__c(
                        Candidate_Student__c = retMap.get(1).Id,
                        Course_or_Specialty_Offer__c = retMap.get(0).Id,
                        Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                        VerificationDate__c = System.Today(),
                        Installments_per_Year__c = '2'),
                true));

        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id, (
                        SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                        FROM InstallmentConfigs__r
                )
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- ZTestLEX_AddManualDiscount ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static User getUserWithProfileAnalysis() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = 'TEBA Contact Center'];

        User usr = new User(
                Alias = 'tebaAn',
                Email = 'tebaAn@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingAn',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = usrProfile.Id,
                TimezoneSidKey = 'America/Los_Angeles',
                Username = 'tebaanalyst@testorg.com'
        );

        insert usr;
        return usr;
    }

    public static User getUserWithProfileSales() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = 'TEBA Sales - Training'];
        //UserRole usrRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'WRO_Dzia_Szkole'];

        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'pl',
                LocaleSidKey = 'pl',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                //UserRoleId = usrRole.Id,
                TimezoneSidKey = 'America/Los_Angeles',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;
        return usr;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ ZTestJobScheduleManager ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static void prepareCampaigns() {
        List<Marketing_Campaign__c> extCampaignsList = new List<Marketing_Campaign__c>();

        Marketing_Campaign__c extCampaign1 = new Marketing_Campaign__c(
                RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
                Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
                Campaign_Name__c = 'extCampaignName_1',
                External_Id__c = '941'
        );

        Marketing_Campaign__c extCampaign2 = new Marketing_Campaign__c(
                RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
                Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
                Campaign_Name__c = 'extCampaignName_2',
                External_Id__c = '942'
        );

        Marketing_Campaign__c extCampaign3 = new Marketing_Campaign__c(
                RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL),
                Campaign_Communication_Form__c = CommonUtility.MARKETING_COMMUNICATION_FROM_FCC,
                Campaign_Name__c = 'extCampaignName_3',
                External_Id__c = '943'
        );

        extCampaignsList.add(extCampaign1);
        extCampaignsList.add(extCampaign2);
        extCampaignsList.add(extCampaign3);

        insert extCampaignsList;
    }

    public static List<String> prepareContacts() {

        List<Contact> testContacts = new List<Contact>();
        List<String> contactsIds = new List<String>();

        Contact cnt1 = new Contact(
                LastName = 'testContact1',
                Phone = '123456789',
                Email = 'testContact1@gmail.com',
                The_Same_Mailling_Address__c = false,
                A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
                RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
                External_Contact_Id__c = 'testExternalId1'
        );

        Contact cnt2 = new Contact(
                LastName = 'testContact2',
                Phone = '987654321',
                Email = 'testContact2@gmail.com',
                The_Same_Mailling_Address__c = false,
                A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
                RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
                External_Contact_Id__c = 'testExternalId2'
        );

        testContacts.add(cnt1);
        testContacts.add(cnt2);

        insert testContacts;

        for (Contact cnt : testContacts) {
            contactsIds.add(cnt.Id);
        }

        return contactsIds;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ ZTestIntegrationProductsHelper ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Map<Integer, sObject> IPH_prepareDataForTest_Statistics(Boolean withSpecialties) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));

        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = retMap.get(0).Id,
                Study_Start_Date__c = System.today()
        ), true));

        if (withSpecialties) {
            retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                    Course_Offer_from_Specialty__c = retMap.get(1).Id,
                    Study_Start_Date__c = System.today()
            ), true));
            retMap.put(3, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                    Course_Offer_from_Specialty__c = retMap.get(1).Id,
                    Study_Start_Date__c = System.today()
            ), true));
        }

        retMap.put(4, ZDataTestUtility.createForeignLanguage(new Offer__c(
                Course_Offer_from_Language__c = retMap.get(1).Id,
                Study_Start_Date__c = System.today()
        ), true));

        retMap.put(5, ZDataTestUtility.createPriceBook(new Offer__c(
                Offer_from_Price_Book__c = retMap.get(1).Id,
                Study_Start_Date__c = System.today()
        ), true));

        retMap.put(6, ZDataTestUtility.createPriceBook(new Offer__c(
                Offer_from_Price_Book__c = retMap.get(1).Id,
                Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
                Study_Start_Date__c = System.today()
        ), true));

        retMap.put(7, ZDataTestUtility.createSpecializationOffer(new Offer__c(
                Offer_from_Specialization__c = retMap.get(1).Id,
                Study_Start_Date__c = System.today()
        ), true));

        retMap.put(8, ZDataTestUtility.createPriceBook(new Offer__c(
                Offer_from_Price_Book__c = retMap.get(2).Id,
                Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
                Study_Start_Date__c = System.today()
        ), true));

        retMap.put(9, ZDataTestUtility.createPriceBook(new Offer__c(
                Offer_from_Price_Book__c = retMap.get(2).Id,
                Study_Start_Date__c = System.today()
        ), true));

        configPriceBook(retMap.get(5).Id, 50);
        configPriceBook(retMap.get(6).Id, 5);
        configPriceBook(retMap.get(8).Id, 5);
        configPriceBook(retMap.get(9).Id, 50);

        return retMap;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------- ZTestIntegrationIPressoRemoveContacts  --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class RemoveContactsCalloutMock_Success implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            return res;
        }
    }

    public class RemoveContactsCalloutMock_Failed implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setStatusCode(404);
            return res;
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------- ZTestIntegrationIpressoContactCreatSend MOCK CLASS----------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class IPCCS_SuccessSendCalloutMock implements HttpCalloutMock {
        Id candidateId;

        public IPCCS_SuccessSendCalloutMock(Id candidateId) {
            this.candidateId = candidateId;
        }

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setBody('[{"crm_contact_id":"' + candidateId + '","ipresso_contact_id":"123","status_code":"200"}]');
            res.setStatusCode(200);
            return res;
        }
    }

    public class IPCCS_FailedCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setBody('[{"crm_contact_id":"' + candidate.Id + '","ipresso_contact_id":null,"status_code":"404"}]');
            res.setStatusCode(404);
            return res;
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public class SendCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            return res;
        }
    }

    public class SendFailedCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setStatusCode(404);
            return res;
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------- ZTestIntegrationIPressoConsentSync--------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Map<Integer, sObject> IPCS_prepareData() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW, Accepted_by_default__c = true), true));
        retMap.put(1, ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true));
        return retMap;
    }

    public class ConsentsSendCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            List<Catalog__c> consents = [
                    SELECT Id, IPressoId__c
                    FROM Catalog__c
            ];

            IntegrationIPressoConsentSync.ACKReceiver_JSON receiver = new IntegrationIPressoConsentSync.ACKReceiver_JSON();
            receiver.crm_consent_id = consents.get(0).Id;

            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            res.setBody(JSON.serialize(receiver));
            return res;
        }
    }

    public class ConsentsSendFailedCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            List<Catalog__c> consents = [
                    SELECT Id, IPressoId__c
                    FROM Catalog__c
            ];

            IntegrationIPressoConsentSync.ACKReceiver_JSON receiver = new IntegrationIPressoConsentSync.ACKReceiver_JSON();
            receiver.crm_consent_id = consents.get(0).Id;

            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setStatusCode(404);
            res.setBody(JSON.serialize(receiver));
            return res;
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------- ZTestIntegrationIPressoCampaignSender ------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public static Id IPCS_prepareDataTags() {
        Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_TAGS), true);
        Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

        return iPressoCampaign.Id;
    }

    public class IPCS_MassEnrollmentSendCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            return res;
        }
    }

    public class IPCS_SendSegmentsMock implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            res.setBody('{"code":201,"data":{"id":"85","contacts_in_segment":1},"message":"Created"}');
            return res;
        }
    }

    public class IPCS_EnrollmentSendFailedCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setStatusCode(404);
            return res;
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------- ZTestIntegrationGoogleCalendarEvent ------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class Calendar_SendCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {

            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setBody('{"status":200,"events":[{"event":{"attendees":[{"email":"marcin.naliwajko@enxoo.com","responseStatus":"needsAction"},{"email":"maciej.simm@enxoo.com","responseStatus":"accepted"},{"email":"tamara.rubin@enxoo.com","responseStatus":"needsAction","self":true},{"email":"monika.szlendak@enxoo.com","responseStatus":"accepted"},{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com","organizer":true,"responseStatus":"accepted"},{"displayName":"Sala Konferencyjna 3&4","email":"enxoo.com_2d39343637393736333433@resource.calendar.google.com","resource":true,"responseStatus":"accepted"},{"email":"jaroslaw.kalicki@enxoo.com","responseStatus":"accepted"},{"email":"dorota.salem@enxoo.com","responseStatus":"accepted"},{"email":"magdalena.kubis@enxoo.com","responseStatus":"accepted"},{"email":"michal.stebach@enxoo.com","optional":true,"responseStatus":"accepted"},{"email":"jacek.czernuszenko@enxoo.com","responseStatus":"needsAction"},{"email":"anna.seredyn@enxoo.com","responseStatus":"accepted"},{"email":"dominika.ozieblo@enxoo.com","responseStatus":"accepted"},{"email":"radoslaw.sulek@enxoo.com","responseStatus":"accepted"}],"created":{"value":1502112439000,"dateOnly":false,"timeZoneShift":0},"creator":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"description":"Lets meet weekly to discuss resources allocation, projects plans and issues. The main point will be short review of each project with budget, resource allocation, plan and issues. We can use \\"Project Review\\" object within Salesforce to prepare that individually for each project.","end":{"dateTime":{"value":1510677000000,"dateOnly":false,"timeZoneShift":60}},"etag":"\\"3018930917426000\\"","hangoutLink":"https://meet.google.com/dsc-uiwe-rbr","htmlLink":"https://www.google.com/calendar/event?eid=Z3N0ZHBmdWIyMThscHM0ODhkcDlocmRxdWdfMjAxNzExMTRUMTUwMDAwWiB0YW1hcmEucnViaW5AZW54b28uY29t","iCalUID":"gstdpfub218lps488dp9hrdqug@google.com","id":"gstdpfub218lps488dp9hrdqug_20171114T150000Z","kind":"calendar#event","location":"S, Sala Konferencyjna 3&4, będzie co 2 tygodnie","organizer":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"originalStartTime":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"recurringEventId":"gstdpfub218lps488dp9hrdqug","reminders":{"useDefault":true},"sequence":1,"start":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"status":"confirmed","summary":"Project management meeting","updated":{"value":1509465458713,"dateOnly":false,"timeZoneShift":0}},"calendarId":"tamara.rubin@enxoo.com"}]}');

            res.setStatusCode(200);

            return res;
        }
    }

    public class Calendar_SendFalseCalloutMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest req) {

            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setBody('{"status":404,"events":[{"event":{"attendees":[{"email":"marcin.naliwajko@enxoo.com","responseStatus":"needsAction"},{"email":"maciej.simm@enxoo.com","responseStatus":"accepted"},{"email":"tamara.rubin@enxoo.com","responseStatus":"needsAction","self":true},{"email":"monika.szlendak@enxoo.com","responseStatus":"accepted"},{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com","organizer":true,"responseStatus":"accepted"},{"displayName":"Sala Konferencyjna 3&4","email":"enxoo.com_2d39343637393736333433@resource.calendar.google.com","resource":true,"responseStatus":"accepted"},{"email":"jaroslaw.kalicki@enxoo.com","responseStatus":"accepted"},{"email":"dorota.salem@enxoo.com","responseStatus":"accepted"},{"email":"magdalena.kubis@enxoo.com","responseStatus":"accepted"},{"email":"michal.stebach@enxoo.com","optional":true,"responseStatus":"accepted"},{"email":"jacek.czernuszenko@enxoo.com","responseStatus":"needsAction"},{"email":"anna.seredyn@enxoo.com","responseStatus":"accepted"},{"email":"dominika.ozieblo@enxoo.com","responseStatus":"accepted"},{"email":"radoslaw.sulek@enxoo.com","responseStatus":"accepted"}],"created":{"value":1502112439000,"dateOnly":false,"timeZoneShift":0},"creator":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"description":"Lets meet weekly to discuss resources allocation, projects plans and issues. The main point will be short review of each project with budget, resource allocation, plan and issues. We can use \\"Project Review\\" object within Salesforce to prepare that individually for each project.","end":{"dateTime":{"value":1510677000000,"dateOnly":false,"timeZoneShift":60}},"etag":"\\"3018930917426000\\"","hangoutLink":"https://meet.google.com/dsc-uiwe-rbr","htmlLink":"https://www.google.com/calendar/event?eid=Z3N0ZHBmdWIyMThscHM0ODhkcDlocmRxdWdfMjAxNzExMTRUMTUwMDAwWiB0YW1hcmEucnViaW5AZW54b28uY29t","iCalUID":"gstdpfub218lps488dp9hrdqug@google.com","id":"gstdpfub218lps488dp9hrdqug_20171114T150000Z","kind":"calendar#event","location":"S, Sala Konferencyjna 3&4, będzie co 2 tygodnie","organizer":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"originalStartTime":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"recurringEventId":"gstdpfub218lps488dp9hrdqug","reminders":{"useDefault":true},"sequence":1,"start":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"status":"confirmed","summary":"Project management meeting","updated":{"value":1509465458713,"dateOnly":false,"timeZoneShift":0}},"calendarId":"tamara.rubin@enxoo.com"}]}');

            res.setStatusCode(404);

            return res;
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------- ZTestIntegrationExtCampaignCreation ---------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> prepareValidData() {
        List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> requestJSON = new List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON>();

        // id, name, form
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_www = new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_0', 'testName0', 'WWW', ''
        );
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_fcc =  new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_1', 'testName1', 'FCC', ''
        );
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_ipresso =  new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_2', 'testName2', 'iPresso', ''
        );

        requestJSON.add(campaign_www);
        requestJSON.add(campaign_fcc);
        requestJSON.add(campaign_ipresso);

        return requestJSON;
    }

    public static List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> prepareInvalidData_400() {
        List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> requestJSON = new List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON>();

        // id, name, form
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_xxx = new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_0', 'testName0', 'XXX', ''
        );

        requestJSON.add(campaign_xxx);

        return requestJSON;
    }

    public static List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> prepareInvalidData_500() {
        List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> requestJSON = new List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON>();

        // id, name, form
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_www = new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_0', 'testName0', 'WWW', ''
        );
        IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON campaign_fcc = new IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON(
                'extCampaignName_0', 'testName1', 'FCC', ''
        );

        requestJSON.add(campaign_www);
        requestJSON.add(campaign_fcc);

        return requestJSON;
    }

    public static List<Marketing_Campaign__c> getExternalCampaigns(){
        String recordTypeOfCampaign = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL);

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns =
        [
                SELECT External_Id__c, Campaign_Name__c, Campaign_Communication_Form__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :recordTypeOfCampaign
        ];

        return listOfExternalMarketingCampaigns;
    }

}