@isTest
global class ZTestIntegrationContactConsentsResource {

    @isTest static void test_integration() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/';
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsResource.sendConsents();

        System.assertEquals(200, res.statusCode);
    }

    @isTest static void test_integration_withDefinedCatalog() {
    	ZDataTestUtility.prepareCatalogData(true, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/';
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsResource.sendConsents();

        System.assertEquals(200, res.statusCode);
    }

    @isTest static void test_integration_withConsents() {
    	ZDataTestUtility.prepareCatalogData(true, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/consents/';
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationContactConsentsResource.sendConsents();

        System.assertEquals(200, res.statusCode);
    }
}