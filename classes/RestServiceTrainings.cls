@RestResource(urlMapping = '/trainings/*')
global with sharing class RestServiceTrainings {

//===============================================================================================
// *** REST GET *** :  Requires the id of Contact and return results as Response Wrapper class
//===============================================================================================

    @HttpGet
    global static ScheduleWrapper getTrainingSchedules(){
        String trainingId = RestContext.request.params.get('Id');
        System.debug('Send Id is: ' + trainingId);
        List<Offer__c> schedules = [
                SELECT Name, Training_Offer_from_Schedule__c, Training_Offer_from_Schedule__r.Trade_Name__c, Number_of_Seats__c, Price_per_Person__c,
                        Price_per_Person_Student_Graduate__c, Valid_From__c, Valid_To__c, Start_Time__c, Training_Place__c, Street__c, City__c, Launched__c
                FROM Offer__c
                WHERE Training_Offer_from_Schedule__r.Name = :trainingId AND Active__c = true
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
        ];

        ScheduleWrapper wrapper = new ScheduleWrapper();
        wrapper = ScheduleWrapper.serializeSchedules(schedules);
        System.debug('Wrapper returned = ' + wrapper);
        return wrapper;
    }


//====================================================================================
// *** REST POST *** : Require field should not be empty, method to post a new Contact
//====================================================================================

    @HttpPost
    global static ResponseWrapper postTrainingEnrollment(){
        Id participantResultRT = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT);
        Id openTrainingTypeRT = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING);

        RestResponse res = RestContext.response;
        res.statusCode = 400; //For most cases its true

        String json = RestContext.request.requestBody.toString();
        //ErrorLogger.msg(jsoN);
        //try{
        String jsonStr = EncodingUtil.urlDecode(json, 'UTF-8');
        Json2Apex parsed = Json2Apex.parse(jsonStr);
        System.debug('JSON = ' + parsed);

        // helper objects
        //List<Contact> toCheckDuplicates = new List<Contact>();
        //List<Contact> insertedContacts = new List<Contact>();
        //List<Enrollment__c> enrollmentList = new List<Enrollment__c>();

        // duplicate check objects
        Set<String> firstNameList = new Set<String>();
        Set<String> lastNameList = new Set<String>();
        Set<String> peselList = new Set<String>();
        Set<String> phoneList = new Set<String>();
        Set<String> emailList = new Set<String>();

        if (parsed.attendees == null || parsed.attendees.isEmpty()){
            return new ResponseWrapper('ATTENDEES_NOT_FOUND', 'Invalid message. Check if attendees are in a message', json);
        }

        //*********************************
        //Get Training Data
        //*********************************
        Offer__c trainingRecord;
        try{
            trainingRecord = [SELECT Id, Name, Price_per_Person__c, Price_per_Person_Student_Graduate__c, Training_Offer_from_Schedule__r.Type__c,
                    Training_Offer_from_Schedule__r.University_from_Training__r.Name, Training_Offer_from_Schedule__r.University_from_Training__r.Parent.Name
            FROM Offer__c
            WHERE Name = :parsed.trainingCode LIMIT 1];
        }
        catch(QueryException e){
            return new ResponseWrapper('TRAINING_ID_NOT_FOUND', 'Invalid training Id', json);
        }

        //*********************************
        //Entity for user consents
        //*********************************
        String trainingEntity = trainingRecord.Training_Offer_from_Schedule__r.University_from_Training__r.Parent.Name;
        if (String.isBlank(trainingEntity)){
            trainingEntity = trainingRecord.Training_Offer_from_Schedule__r.University_from_Training__r.Name;
        }
        //3 letter code
        String entity = CommonUtility.getEntityByMarketingEntity(trainingEntity);

        if (String.isBlank(entity)){
            return new ResponseWrapper('TRAINING_ENTITY_NOT_FOUND', 'Selected training doesnt have entity', json);
        }

        //*********************************
        //Handle account - only for company
        //*********************************
        Id accountId;
        if (String.isNotBlank(parsed.nip)){
            try{
                accountId = RestServiceTrainingsHelper.handleCompany(parsed);
            }
            catch(DmlException e){
                return new ResponseWrapper('INVALID_COMPANY_DATA', 'Error during inserting Company data', json);
            }
        }

        //*********************************
        //Parsing attendees
        //*********************************
        List<Contact> parsedAttendees;
        List<Contact> noDuplicateAttendees;
        try{
            parsedAttendees = RestServiceTrainingsHelper.parseAttendees(parsed, accountId, entity);
            //fill eventual duplicate objects
            for(Contact c: parsedAttendees){
                firstNameList.add(c.FirstName);
                lastNameList.add(c.LastName);
                if (String.isNotBlank(c.Pesel__c)) peselList.add(c.Pesel__c);
                if (String.isNotBlank(c.Phone)) phoneList.add(c.Phone);
                if (String.isNotBlank(c.Email)) emailList.add(c.Email);
            }

            //Search for duplicates
            List<Contact> potentialContactDuplicates = [
                    SELECT Id, The_Same_Mailling_Address__c, FirstName, LastName, Pesel__c, Phone, Email, RecordTypeId, MailingStreet,
                            MailingPostalCode, MailingCity, MailingState, MailingCountry, MailingAddress, OtherAddress, OtherStreet, OtherPostalCode,
                            OtherCity, OtherCountry, OtherState, Industry__c, Department, Department_Business_Area__c, Position__c, AccountId,
                            Contact_Person__c, CreatedDate, Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c,
                            Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, Consent_PUCA_ADO__c
                    FROM Contact
                    WHERE Pesel__c IN :peselList OR (FirstName IN :firstNameList AND LastName IN :lastNameList
                    AND (Phone IN :phoneList OR Email IN :emailList))
            ];

            //Handle duplicate contacts
            //noDuplicateAttendees = RestServiceTrainingsHelper.handleContactDuplicates(parsedAttendees, potentialContactDuplicates, parsed);
            System.debug('parsedAttendees ' + parsedAttendees);
            noDuplicateAttendees = parsedAttendees;

            //Set<Id> contactIds = new Set<Id>();
            //List<Contact> noDuplicateAttendeesWithoutDuplicateId = new List<Contact>();
            //for (Contact c : noDuplicateAttendees) {
            //    if (!contactIds.contains(c.Id)) {
            //        noDuplicateAttendeesWithoutDuplicateId.add(c);
            //        contactIds.add(c.Id);
            //    }
            //}
            CommonUtility.preventTriggeringEmailValidation = true;
            //CommonUtility.preventTriggeringEmailValidation = true;
            CommonUtility.preventChangingEmailTwice = true;
            //noDuplicateAttendees = noDuplicateAttendeesWithoutDuplicateId;
            upsert noDuplicateAttendees;

            if (String.isNotBlank(parsed.futureTrainingInfo) && parsed.type.equalsIgnoreCase(CommonUtility.PRIVATE_FORM_TYPE)){
                Note n = new Note();
                n.ParentId = noDuplicateAttendees.get(0).Id;
                n.Body = parsed.futureTrainingInfo;
                n.Title = 'W przyszłości jestem zainteresowany szkoleniami z obszaru';
                insert n;
            }
        }
        catch(Exception e){
            ErrorLogger.log(e);
            return new ResponseWrapper('INVALID_ATTENDEES_DATA', 'Error during parsing Attendees data', json);
        }

        //*************************
        //Enrollment processing
        //*************************

        Set<Id> contactIds = new Set<Id>();

        for(Contact con: noDuplicateAttendees){
            contactIds.add(con.Id);
        }

        Set<Enrollment__c> duplicateEnrollments = new Set<Enrollment__c>([
                SELECT Participant__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
                AND Participant__c IN :contactIds
                AND Training_Offer_for_Participant__c = :trainingRecord.Id AND Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION
        ]);

        //Clear contacts which are already signed to this training
        List<Contact> noEnrollmentDuplicatesForContacts = RestServiceTrainingsHelper.clearAlreadyExistingEnrollments(duplicateEnrollments, noDuplicateAttendees);
        List<Contact> noEnrollmentDuplicatesForContactsForConsents = new List<Contact>();
        noEnrollmentDuplicatesForContactsForConsents.addAll(noEnrollmentDuplicatesForContacts);
        //*************************
        //Contact Person processing
        //*************************
        Boolean isContactPersonInAttendeesList = false;
        Contact contactPerson = new Contact();
        if (String.isNotBlank(parsed.contactPersonFirstName) && String.isNotBlank(parsed.contactPersonLastName) && (String.isNotBlank(parsed.contactPersonPhone) || String.isNotBlank(parsed.contactPersonEmail))){
            List<Contact> contactPersonDuplicate = [SELECT  Id, The_Same_Mailling_Address__c, FirstName, LastName, Pesel__c, Phone, Email, RecordTypeId, MailingStreet, MailingPostalCode, MailingCity, MailingState, MailingCountry, MailingAddress, OtherAddress, OtherStreet, OtherPostalCode, OtherCity, OtherCountry, OtherState, Industry__c, Department, Department_Business_Area__c, Position__c, AccountId, Contact_Person__c, Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c,
                    Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, Consent_PUCA_ADO__c FROM Contact WHERE FirstName = :parsed.contactPersonFirstName AND LastName =: parsed.contactPersonLastName AND (Phone =: parsed.contactPersonPhone OR Email =: parsed.contactPersonEmail)];
            try{
                //DUPLICATE IN ATTENDEES
                if (contactPersonDuplicate.isEmpty()){
                    for(Contact c: noDuplicateAttendees){
                        if (c.FirstName.equalsIgnoreCase(parsed.contactPersonFirstName.trim()) && c.LastName.equalsIgnoreCase(parsed.contactPersonLastName.trim()) && (c.Phone.equalsIgnoreCase(parsed.contactPersonPhone) || c.Email.equalsIgnoreCase(parsed.contactPersonEmail))){
                            c.Contact_Person__c = true;
                            isContactPersonInAttendeesList = true;
                            contactPerson = c;
                            update c;
                        }
                    }
                    //NEW CONTACT
                    if (!isContactPersonInAttendeesList){
                        contactPerson.FirstName = parsed.contactPersonFirstName;
                        contactPerson.AccountId = accountId;
                        contactPerson.LastName = parsed.contactPersonLastName;
                        contactPerson.Phone = parsed.contactPersonPhone;
                        contactPerson.Email = parsed.contactPersonEmail;
                        contactPerson.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CONTACTPERSON);
                        contactPerson.Source__c = CommonUtility.CONTACT_SOURCE_RESTAPI_TRAININGS;
                        if (String.isNotBlank(parsed.contactPersonDepartment)) contactPerson.Department_Business_Area__c = RestServiceTrainingsHelper.getPicklistValues('Contact', 'Department_Business_Area__c', parsed.contactPersonDepartment);
                        if (String.isNotBlank(parsed.contactPersonPosition)) contactPerson.Position__c = RestServiceTrainingsHelper.getPicklistValues('Contact','Position__c', parsed.contactPersonPosition);
                        if (String.isNotBlank(parsed.businessArea)) contactPerson.Industry__c = RestServiceTrainingsHelper.getPicklistValues('Contact', 'Industry__c', parsed.businessArea);

                        CommonUtility.preventTriggeringEmailValidation = true;

                        insert contactPerson;
                        noEnrollmentDuplicatesForContactsForConsents.add(contactPerson);
                    }
                }
                //DUPLICATE IN DB
                else{
                    contactPerson = contactPersonDuplicate.get(0);
                    contactPerson.Contact_Person__c = true;
                    if (String.isBlank(contactPerson.AccountId)) contactPerson.AccountId = accountId;
                    if (String.isBlank(contactPerson.Position__c)) contactPerson.Position__c = RestServiceTrainingsHelper.getPicklistValues('Contact','Position__c', parsed.contactPersonPosition);
                    if (String.isBlank(contactPerson.Department_Business_Area__c)) contactPerson.Department_Business_Area__c = RestServiceTrainingsHelper.getPicklistValues('Contact', 'Department_Business_Area__c', parsed.contactPersonDepartment);
                    if (String.isBlank(contactPerson.Industry__c)) contactPerson.Industry__c = RestServiceTrainingsHelper.getPicklistValues('Contact', 'Industry__c', parsed.businessArea);

                    CommonUtility.preventTriggeringEmailValidation = true;
                    update contactPerson;
                    noEnrollmentDuplicatesForContactsForConsents.add(contactPerson);
                }
            }
            catch(Exception e){
                return new ResponseWrapper('INVALID_CONTACTPERSON_DATA', 'Error during parsing Contact Person data', json);
            }
        }

        //Update contact consents
        SYstem.debug('noEnrollmentDuplicatesForContactsForConsents ' + noEnrollmentDuplicatesForContactsForConsents);
        RestServiceTrainingsHelper.updateContactConsents(noEnrollmentDuplicatesForContactsForConsents, parsed.personalDataProcessingAgreement, parsed.commercialInfoAgreement, parsed.contactAgreement, entity, parsed.type);

        //****************************
        //Training Enrollment creation
        //****************************
        if (!noEnrollmentDuplicatesForContacts.isEmpty()){
            System.debug('Adding enrollments');
            Enrollment__c enrollment = new Enrollment__c();
            enrollment.Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI;
            enrollment.Source__c = RestServiceTrainingsHelper.getPicklistValues('Enrollment__c', 'Source__c', parsed.source);
            enrollment.The_Same_Data_as_on_Contact__c = parsed.sameAsContact;
            enrollment.The_Invoice_after_Payment__c = parsed.invoiceAfterPayment;
            enrollment.Way_of_Invoice_Delivery__c = RestServiceTrainingsHelper.getPicklistValues('Enrollment__c', 'Way_of_Invoice_Delivery__c', parsed.invoiceDeliveryChannel);
            enrollment.Please_Issue_a_Proforma_Invoice__c = parsed.invoiceProforma;
            enrollment.Description_of_Service_on_the_Invoice__c = parsed.invoiceDescription;
            //invoice part
            if (!parsed.sameAsContact){ //invoice additional data provided
                if (parsed.type == CommonUtility.COMPANY_FORM_TYPE){
                    enrollment.Company_Name__c = parsed.invoiceCompanyName;
                    enrollment.NIP_for_Invoice__c = parsed.invoiceNip;
                }
                else{
                    enrollment.First_Name_for_Invoice__c = parsed.invoiceFirstName;
                    enrollment.Last_Name_for_Invoice__c = parsed.invoiceLastName;
                }
                enrollment.City_for_Invoice__c = parsed.invoiceCity;
                enrollment.Postal_Code_for_Invoice__c = parsed.invoicePostalCode;
                enrollment.Street_for_Invoice__c = parsed.invoiceStreet;
                enrollment.Country_for_Invoice__c = parsed.invoiceCountry;
            }
            else{ //no invoice additional data provided
                if (parsed.type == CommonUtility.COMPANY_FORM_TYPE){
                    enrollment.Company_Name__c = parsed.companyName;
                    enrollment.NIP_for_Invoice__c = parsed.nip;
                    enrollment.City_for_Invoice__c = parsed.companyCity;
                    enrollment.Postal_Code_for_Invoice__c = parsed.companyPostalCode;
                    enrollment.Street_for_Invoice__c = parsed.companyStreet;
                }
                else{
                    enrollment.First_Name_for_Invoice__c = noEnrollmentDuplicatesForContacts.get(0).FirstName;
                    enrollment.Last_Name_for_Invoice__c = noEnrollmentDuplicatesForContacts.get(0).LastName;
                    enrollment.City_for_Invoice__c = noEnrollmentDuplicatesForContacts.get(0).MailingCity;
                    enrollment.Postal_Code_for_Invoice__c = noEnrollmentDuplicatesForContacts.get(0).MailingPostalCode;
                    enrollment.Street_for_Invoice__c = noEnrollmentDuplicatesForContacts.get(0).MailingStreet;
                }
            }

            enrollment.Training_Offer__c = trainingRecord.Id;
            if (String.isNotBlank(parsed.voucher)) enrollment.Discount_Code__c = parsed.voucher;
            if (String.isNotBlank(parsed.supportId)) enrollment.Support_ID__c = parsed.supportId;
            enrollment.RecordTypeId = openTrainingTypeRT;

            if (String.isNotBlank(accountId)){
                enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES;
                enrollment.Company__c = accountId;
            }
            else{
                enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
            }
            if (String.isNotBlank(contactPerson.Id)) enrollment.Reporting_Person__c = contactPerson.Id;

            /* Prepare Consents for Schedule */
            enrollment.Consent_Terms_of_Service_Acceptation__c = true;
            enrollment.Consent_Date_processing_for_contract__c = true;

            if (parsed.personalDataProcessingAgreement) enrollment.Consent_Marketing__c = true;
            if (parsed.commercialInfoAgreement) enrollment.Consent_Electronic_Communication__c = true;
            if (parsed.contactAgreement) enrollment.Consent_Direct_Communications__c = true;

            try{
                insert enrollment;
            }
            catch(DmlException e){
                return new ResponseWrapper('INVALID_ENROLLMENT_DATA', e.getMessage(), json);
            }

            //**************************************
            //Participant Result Enrollment creation
            //**************************************
            List<Enrollment__c> enrollmentList = new List<Enrollment__c>();

            Map<Id, Contact> contactMap = new Map<Id, Contact>();
            for (Contact c : noEnrollmentDuplicatesForContacts){
                Contact potentialContact = contactMap.get(c.Id);

                if (potentialContact == null) {
                    contactMap.put(c.Id, c);
                }
                else {
                    if (c.CreatedDate < potentialContact.CreatedDate && c.Id == potentialContact.Id) {
                        contactMap.put(c.Id, c);
                    }
                }
            }

            System.debug('contactMap ' + contactMap);
            for (Id contactId : contactMap.keySet()){
                Enrollment__c enrollParticipantResult = new Enrollment__c();
                enrollParticipantResult.Source__c = 'WWW';
                enrollParticipantResult.The_Same_Data_as_on_Contact__c = parsed.sameAsContact;
                enrollParticipantResult.The_Invoice_after_Payment__c = parsed.invoiceAfterPayment;
                enrollParticipantResult.Way_of_Invoice_Delivery__c = parsed.invoiceDeliveryChannel;
                enrollParticipantResult.Please_Issue_a_Proforma_Invoice__c = parsed.invoiceProforma;
                enrollParticipantResult.Description_of_Service_on_the_Invoice__c = parsed.invoiceDescription;
                enrollParticipantResult.First_Name_for_Invoice__c = parsed.invoiceFirstName;
                enrollParticipantResult.Last_Name_for_Invoice__c = parsed.invoiceLastName;
                enrollParticipantResult.City_for_Invoice__c = parsed.invoiceCity;
                enrollParticipantResult.Postal_Code_for_Invoice__c = parsed.invoicePostalCode;
                enrollParticipantResult.Street_for_Invoice__c = parsed.invoiceStreet;
                enrollParticipantResult.Country_for_Invoice__c = parsed.invoiceCountry;
                enrollParticipantResult.Participant__c = contactId;
                System.debug('contactId ' + contactId);
                enrollParticipantResult.Training_Offer_for_Participant__c = trainingRecord.Id;
                enrollParticipantResult.Enrollment_Training_Participant__c = enrollment.Id;
                enrollParticipantResult.RecordTypeId = participantResultRT;
                System.debug('parsed.attendeeType ' + parsed.attendeeType);
                if (parsed.attendeeType != null && parsed.attendeeType){
                    enrollParticipantResult.Student_Graduate_the_same_University__c = true;
                }

                if (!enrollParticipantResult.Student_Graduate_the_same_University__c) {
                    enrollParticipantResult.Student_Graduate_the_same_University__c = contactMap.get(contactId).TECH_attendeeType__c;
                }
                System.debug('enrollParticipantResult.Student_Graduate_the_same_University__c ' + enrollParticipantResult.Student_Graduate_the_same_University__c);
                enrollParticipantResult.Enrollment_Value__c = (enrollParticipantResult.Student_Graduate_the_same_University__c? trainingRecord.Price_per_Person_Student_Graduate__c : trainingRecord.Price_per_Person__c);
                enrollParticipantResult.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
                if (String.isNotBlank(accountId)) enrollParticipantResult.Company__c = accountId;

                enrollmentList.add(enrollParticipantResult);
            }

            System.debug('enrollmentList ' + enrollmentList);
            insert enrollmentList;
        }

        for (Contact c : noDuplicateAttendees) {
            c.Confirmed_Contact__c = true;
        }

        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            CommonUtility.preventChangingEmailTwice = true;
            update noDuplicateAttendees;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

        res.statusCode = 200;
        return new ResponseWrapper();
    }

//**************************************************
//Response class Wrapper
//**************************************************
    global class ResponseWrapper{

        public Boolean success;
        public String statusCode;
        public String message;

        public ResponseWrapper(String statusCode, String message, String json){
            this.statusCode = statusCode;
            this.message = message;
            this.success = false;
        }

        public ResponseWrapper(){
            this.success = true;
        }
    }

}