global class DocGen_GenerateAgreementCity implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateAgreementCity() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
        if (methodName == 'generateOathCity') {
            return DocGen_GenerateAgreementCity.generateOathCity(objectId);
        }
        else {
            return DocGen_GenerateAgreementCity.generateAgreementCity(objectId);             
        }
    }

    global static String generateAgreementCity(String objectId) {    
        Enrollment__c studyEnr = [
            SELECT Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.University_Study_Offer_from_Course__r.University__r.BillingCity, 
                   Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Course_Offer_From_Specialty__r.University_Study_Offer_from_Course__r.University__r.BillingCity
            FROM Enrollment__c
            WHERE Id = :objectId
        ];
        
        String result = '';
        if (studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.University_Study_Offer_from_Course__r.University__r.BillingCity != null) {
            result += studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.University_Study_Offer_from_Course__r.University__r.BillingCity;
        }
        else {
            result += studyEnr.Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Course_Offer_From_Specialty__r.University_Study_Offer_from_Course__r.University__r.BillingCity;
        }
        
        return result;
    }

    global static String generateOathCity(String objectId) {  
        Enrollment__c studyEnr = [
            SELECT Enrollment_From_documents__r.University_Name__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];
        
        String studyUniversity = studyEnr.Enrollment_From_documents__r.University_Name__c;
        String result = studyUniversity == CommonUtility.MARKETING_ENTITY_WROCLAW ? 'we' : 'w';

        List<String> splitedStudyUniversity = studyUniversity.split(' ');
        result += ' ' + VocativeRule.generateNameInVocative(splitedStudyUniversity.get(1));

        return result;
    }
}