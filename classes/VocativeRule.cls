//should be changed to LocativeRule but to fix this error too much resources are needed 

public class VocativeRule {

    public String nameSuffix;
    public String vocativeNameSuffix;

    public VocativeRule(String suffix, String vocativeSuffix) {
        nameSuffix = suffix;
        vocativeNameSuffix = vocativeSuffix;
    }


	public static String generateNameInVocative(String nameInNominative) {
		if (nameInNominative == null) {
			return null;
		}

	    List<VocativeRule> vocativeRules = new List<VocativeRule>();
	    vocativeRules.add(new VocativeRule('aw','awiu')); //Wrocław
	    vocativeRules.add(new VocativeRule('nia','ni')); //Gdynia
	    vocativeRules.add(new VocativeRule('ańsk','ańsku')); //Gdańsk
	    vocativeRules.add(new VocativeRule('ole','olu')); //Opole
	    vocativeRules.add(new VocativeRule('ań','aniu')); //Poznań
	    vocativeRules.add(new VocativeRule('uń','uniu')); //Toruń
	    vocativeRules.add(new VocativeRule('cin','cinie')); //Szczecin
	    vocativeRules.add(new VocativeRule('orzów','orzowie')); //Chorzów
	    vocativeRules.add(new VocativeRule('szcz','szczy')); //Bydgoszcz
	 
	    for(VocativeRule rule : vocativeRules) {
	        if(nameInNominative.endsWithIgnoreCase(rule.nameSuffix)) {
	            return nameInNominative.removeEndIgnoreCase(rule.nameSuffix) + rule.vocativeNameSuffix;
	        }
	    }
	    return nameInNominative;
	}
}