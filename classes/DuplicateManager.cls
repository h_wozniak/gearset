/**
* @author       Wojciech Słodziak
* @description  Class for duplicate verirfication on Contact records.
**/

public without sharing class DuplicateManager {

    // allows to skip deduplication check 
    public static Boolean skipDeduplicationCheck = false;


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- INVOKATION METHOD ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // search for duplicates in database and tick and set lookup checkbox on both contacts 
    @future
    public static void checkForDuplicatesAsync(String contactsToCheckDuplicates) {
        contactsToCheckDuplicates = contactsToCheckDuplicates.replaceAll('\r', '__r').replaceAll('\n', '__n');
        List<Contact> contactToUpdate = (List<Contact>)System.JSON.deserialize(contactsToCheckDuplicates, List<Contact>.class);

        checkForDuplicates(contactToUpdate);
    }

    // search for duplicates in database and tick and set lookup checkbox on both contacts
    public static void checkForDuplicates(List<Contact> contactsToCheckDuplicates) {
        Set<String> nameList = new Set<String>();
        Set<String> firstNameList = new Set<String>();
        Set<String> phoneList = new Set<String>();
        Set<String> mobilePhoneList = new Set<String>();
        Set<String> emailList = new Set<String>();
        Set<String> peselList = new Set<String>();
        for (Contact c : contactsToCheckDuplicates) {
            if (String.isNotBlank(c.Name)) nameList.add(c.FirstName + ' ' + c.LastName);
            if (String.isNotBlank(c.FirstName)) firstNameList.add(c.FirstName);
            if (String.isNotBlank(c.Phone)) phoneList.add(c.Phone);
            if (String.isNotBlank(c.MobilePhone)) mobilePhoneList.add(c.MobilePhone);
            if (String.isNotBlank(c.Email)) emailList.add(c.Email);
            if (String.isNotBlank(c.Pesel__c)) peselList.add(c.Pesel__c);
        }

        Map<Id, Id> contactIdMapToMerge = new  Map<Id, Id>();

        try {
            List<Contact> potentialDuplicates = [
                    SELECT Id, RecordTypeId, FirstName, LastName, Pesel__c, Phone, MobilePhone, Email, Additional_Emails__c, Additional_Phones__c,
                            Is_Potential_Duplicate__c, Potential_Duplicate__c, Foreigner__c, Account.RecordTypeId, AccountId
                    FROM Contact
                    WHERE (
                            Name IN :nameList
                            OR (
                                    FirstName IN :firstNameList
                                    AND (
                                            (Email IN :emailList)
                                            OR (Phone IN :phoneList)
                                            OR (MobilePhone IN :mobilePhoneList)
                                    )
                            )
                            OR (Foreigner__c = false AND Pesel__c IN :peselList)
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE)
                    AND Confirmed_Contact__c = true
            ];

            potentialDuplicates.addAll([
                    SELECT Id, RecordTypeId, FirstName, LastName, Pesel__c, Phone, MobilePhone, Email, Additional_Emails__c, Additional_Phones__c,
                            Is_Potential_Duplicate__c, Potential_Duplicate__c, Foreigner__c, Account.RecordTypeId, AccountId
                    FROM Contact
                    WHERE (
                            Name IN :nameList
                            OR (
                                    FirstName IN :firstNameList
                                    AND (
                                            (Email IN :emailList)
                                            OR (Phone IN :phoneList)
                                            OR (MobilePhone IN :mobilePhoneList)
                                    )
                            )
                            OR (Foreigner__c = false AND Pesel__c IN :peselList)
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CONTACTPERSON)
                    AND Confirmed_Contact__c = true
            ]);

            Map<Id, Contact> contactMapToUpdate = new Map<Id, Contact>();
            Set<Id> deduplicatedContactIds = new Set<Id>();

            for (Contact c: contactsToCheckDuplicates) {
                Boolean anyDuplicateFound = false;

                contactMapToUpdate.put(c.Id, new Contact(
                        Id = c.Id,
                        Is_Potential_Duplicate__c = false,
                        Potential_Duplicate__c = null
                ));

                for (Contact pDup : potentialDuplicates) {
                    if (c.Id != pDup.Id) {
                        Boolean duplicationMatch = false;

                        if (DuplicateManager.rule1Check(c, pDup)) {
                            duplicationMatch = true;
                            contactIdMapToMerge.put(c.Id, pDup.Id);
                        }

                        if (!duplicationMatch && DuplicateManager.rule2Check(c, pDup)) {
                            duplicationMatch = true;
                        }

                        if (!duplicationMatch && DuplicateManager.rule3Check(c, pDup)) {
                            duplicationMatch = true;
                        }

                        if (!duplicationMatch && DuplicateManager.rule4Check(c, pDup)) {
                            duplicationMatch = true;
                        }

                        if (duplicationMatch) {
                            contactMapToUpdate.put(c.Id, new Contact(
                                    Id = c.Id,
                                    Is_Potential_Duplicate__c = true,
                                    Potential_Duplicate__c = pDup.Id
                            ));

                            if (!pDup.Is_Potential_Duplicate__c) {
                                pDup.Is_Potential_Duplicate__c = true;
                                pDup.Potential_Duplicate__c = c.Id;
//                                contactMapToUpdate.put(pDup.Id, pDup);
                                contactMapToUpdate.put(pDup.Id, new Contact(
                                        Id = pDup.Id,
                                        Is_Potential_Duplicate__c = true,
                                        Potential_Duplicate__c = c.Id
                                ));
                            }

                            anyDuplicateFound = true;
                        }
                    }
                }

                if (c.Is_Potential_Duplicate__c && !anyDuplicateFound) {
                    deduplicatedContactIds.add(c.Id);
                }
            }

            // call future method to clear other possible records that might be pointing to current deduplicated records;
            if (!System.isFuture() && !System.isBatch() && !DuplicateManager.skipDeduplicationCheck && !deduplicatedContactIds.isEmpty()) {
                DuplicateManager.deduplicateOtherRecords(deduplicatedContactIds);
            }
            // update contact's duplication status
            if (!contactMapToUpdate.isEmpty()) {
                update contactMapToUpdate.values();
            }

            // call method to merge contact which are designated by 100% duplicate rule (for Contacts)
            if (!contactIdMapToMerge.isEmpty()) {
                ContactMergeManager.mergeContacts(contactIdMapToMerge);
            }

        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- RULE CHECKER METHODS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // checking FirstName, LastName, Pesel and Email
    private static Boolean rule1Check(Contact c, Contact pDup) {
        if (!c.Foreigner__c
                && String.isNotBlank(c.FirstName) && String.isNotBlank(pDup.FirstName) && c.FirstName.equalsIgnoreCase(pDup.FirstName)
                && c.LastName.equalsIgnoreCase(pDup.LastName)
                && (
                (String.isNotBlank(c.Pesel__c) && String.isNotBlank(pDup.Pesel__c) && c.Pesel__c == pDup.Pesel__c)
                        || (String.isBlank(c.Pesel__c) && String.isBlank(pDup.Pesel__c) && c.Pesel__c == pDup.Pesel__c)
                        || (String.isBlank(c.Pesel__c) || String.isBlank(pDup.Pesel__c) && c.Pesel__c != pDup.Pesel__c)
        )
                && String.isNotBlank(c.Email) && String.isNotBlank(pDup.Email) && c.Email == pDup.Email
                && (
                (c.RecordTypeId  == pDup.RecordTypeId && c.RecordTypeId != null && pDup.RecordTypeId != null)
                        || (c.RecordTypeId == null && pDup.RecordTypeId == null && c.Account.RecordTypeId != null && pDup.Account.RecordTypeId != null
                        && c.Account.RecordTypeId == pDup.Account.RecordTypeId)
        )
                ) {
            return true;
        }

        return false;
    }

    // checking Nationality (!Foreigner) and Pesel
    private static Boolean rule2Check(Contact c, Contact pDup) {
        if (!c.Foreigner__c && !pDup.Foreigner__c
                && (
                (c.RecordTypeId  == pDup.RecordTypeId && c.RecordTypeId != null && pDup.RecordTypeId != null)
                        || (c.RecordTypeId == null && pDup.RecordTypeId == null && c.Account.RecordTypeId != null && pDup.Account.RecordTypeId != null
                        && c.Account.RecordTypeId == pDup.Account.RecordTypeId)
        )
                && (
                (String.isNotBlank(c.Pesel__c) && String.isNotBlank(pDup.Pesel__c) && c.Pesel__c == pDup.Pesel__c)
        )
                ) {
            return true;
        }

        return false;
    }

    // checking FirstName, LastName and one of contacting fields
    private static Boolean rule3Check(Contact c, Contact pDup) {
        if (String.isNotBlank(c.FirstName) && String.isNotBlank(pDup.FirstName) && c.FirstName.equalsIgnoreCase(pDup.FirstName)
                && (
                (c.RecordTypeId  == pDup.RecordTypeId && c.RecordTypeId != null && pDup.RecordTypeId != null)
                        || (c.RecordTypeId == null && pDup.RecordTypeId == null && c.Account.RecordTypeId != null && pDup.Account.RecordTypeId != null
                        && c.Account.RecordTypeId == pDup.Account.RecordTypeId)
        )
                && c.LastName.equalsIgnoreCase(pDup.LastName)) {
            Set<String> setForC;
            Set<String> setForDup;

            if ((String.isNotBlank(c.Email) || String.isNotBlank(c.Additional_Emails__c))
                    && (String.isNotBlank(pDup.Email) || String.isNotBlank(pDup.Additional_Emails__c))) {
                setForC = new Set<String>();
                setForDup = new Set<String>();

                setForC.add(c.Email);
                setForDup.add(pDup.Email);

                setForC.addAll(DuplicateManager.splitAdditionalInfoField(c.Additional_Emails__c));
                setForDup.addAll(DuplicateManager.splitAdditionalInfoField(pDup.Additional_Emails__c));

                if (DuplicateManager.containStringsInCommon(setForC, setForDup)) {
                    return true;
                }
            }

            if ((String.isNotBlank(c.Phone) || String.isNotBlank(c.MobilePhone) || String.isNotBlank(c.Additional_Phones__c))
                    && (String.isNotBlank(pDup.Phone) || String.isNotBlank(pDup.MobilePhone) || String.isNotBlank(pDup.Additional_Phones__c))) {
                setForC = new Set<String>();
                setForDup = new Set<String>();

                setForC.add(c.Phone);
                setForC.add(c.MobilePhone);
                setForDup.add(pDup.Phone);
                setForDup.add(pDup.MobilePhone);

                setForC.addAll(DuplicateManager.splitAdditionalInfoField(c.Additional_Emails__c));
                setForDup.addAll(DuplicateManager.splitAdditionalInfoField(pDup.Additional_Emails__c));

                if (DuplicateManager.containStringsInCommon(setForC, setForDup)) {
                    return true;
                }
            }
        }

        return false;
    }


    // checking FirstName and one of Phones or Emails (at the same time)
    private static Boolean rule4Check(Contact c, Contact pDup) {
        if (String.isNotBlank(c.FirstName) && String.isNotBlank(pDup.FirstName) && c.FirstName.equalsIgnoreCase(pDup.FirstName)
                && (
                (c.RecordTypeId  == pDup.RecordTypeId && c.RecordTypeId != null && pDup.RecordTypeId != null)
                        || (c.RecordTypeId == null && pDup.RecordTypeId == null && c.Account.RecordTypeId != null && pDup.Account.RecordTypeId != null
                        && c.Account.RecordTypeId == pDup.Account.RecordTypeId)
        )) {
            Set<String> setForC;
            Set<String> setForDup;

            Boolean emailMatch = false;
            if ((String.isNotBlank(c.Email) || String.isNotBlank(c.Additional_Emails__c))
                    && (String.isNotBlank(pDup.Email) || String.isNotBlank(pDup.Additional_Emails__c))) {
                setForC = new Set<String>();
                setForDup = new Set<String>();

                setForC.add(c.Email);
                setForDup.add(pDup.Email);

                setForC.addAll(DuplicateManager.splitAdditionalInfoField(c.Additional_Emails__c));
                setForDup.addAll(DuplicateManager.splitAdditionalInfoField(pDup.Additional_Emails__c));

                if (DuplicateManager.containStringsInCommon(setForC, setForDup)) {
                    emailMatch = true;
                }
            }

            Boolean phoneMatch = false;
            if ((String.isNotBlank(c.Phone) || String.isNotBlank(c.MobilePhone) || String.isNotBlank(c.Additional_Phones__c))
                    && (String.isNotBlank(pDup.Phone) || String.isNotBlank(pDup.MobilePhone) || String.isNotBlank(pDup.Additional_Phones__c))) {
                setForC = new Set<String>();
                setForDup = new Set<String>();

                setForC.add(c.Phone);
                setForC.add(c.MobilePhone);
                setForDup.add(pDup.Phone);
                setForDup.add(pDup.MobilePhone);

                setForC.addAll(DuplicateManager.splitAdditionalInfoField(c.Additional_Phones__c));
                setForDup.addAll(DuplicateManager.splitAdditionalInfoField(pDup.Additional_Phones__c));

                if (DuplicateManager.containStringsInCommon(setForC, setForDup)) {
                    phoneMatch = true;
                }
            }

            return emailMatch && phoneMatch;
        }

        return false;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- DEDUPLICATION METHOD --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @future
    // called if one record is not duplicate anymore or after delete of other records
    public static void deduplicateOtherRecords(Set<Id> deduplicatedContactIds) {
        List<Contact> contactList = [
                SELECT Id, Potential_Duplicate__c, Is_Potential_Duplicate__c
                FROM Contact
                WHERE (Potential_Duplicate__c IN :deduplicatedContactIds AND Potential_Duplicate__c != null)
                OR (Potential_Duplicate__c = null AND Is_Potential_Duplicate__c = true)
        ];

        for (Contact c : contactList) {
            c.Is_Potential_Duplicate__c = false;
            c.Potential_Duplicate__c = null;
        }

        try {
            DuplicateManager.skipDeduplicationCheck = true;
            update contactList;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Boolean containStringsInCommon(Set<String> firstSet, Set<String> secondSet) {
        for (String item : firstSet) {
            if (item != null && secondSet.contains(item)) {
                return true;
            }
        }

        return false;
    }

    private static Set<String> splitAdditionalInfoField(String fieldValue) {
        if (fieldValue == null) {
            return new Set<String>();
        }

        List<String> splitVals = fieldValue.split('; ');
        if (splitVals.isEmpty()) {
            splitVals.add(fieldValue);
        }

        return new Set<String>(splitVals);
    }
}