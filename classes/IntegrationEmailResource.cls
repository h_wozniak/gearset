/**
*   @author         Grzegorz Długosz
*   @description    This class is used to inform the system that is making a request if given e-mail address already exists in salesforce.
**/

@RestResource(urlMapping = '/check-email/*')
global without sharing class IntegrationEmailResource {


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HTTP METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @HttpPOST
    global static void checkEmail() {
    	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();

        IntegrationManager.ESBEmailRequest_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                res.statusCode = 200;
                List<Contact> cntList;

                if (String.isNotEmpty(parsedJson.email)) {
                    cntList = [SELECT Email FROM Contact WHERE Email = :parsedJson.email LIMIT 1];
                    if (cntList.size() > 0) {
                        IntegrationManager.ESBEmailResponse_JSON resp = new IntegrationManager.ESBEmailResponse_JSON(true);
                        res.responseBody = Blob.valueOf(JSON.serialize(resp));
                    }
                    else {
                        IntegrationManager.ESBEmailResponse_JSON resp = new IntegrationManager.ESBEmailResponse_JSON(false);
                        res.responseBody = Blob.valueOf(JSON.serialize(resp));
                    }
                } else {
                    res.statusCode = 400;
                    res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'email');  
                }
            }
            catch (Exception e) {
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }

        }

    }

    private static IntegrationManager.ESBEmailRequest_JSON parse(String jsonBody) {
        return (IntegrationManager.ESBEmailRequest_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.ESBEmailRequest_JSON.class);
    }

}