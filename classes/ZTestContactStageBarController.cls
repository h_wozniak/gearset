@isTest
private class ZTestContactStageBarController {

    @isTest static void testContactStageBar() {

        PageReference pageRef = Page.ContactStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(null, true);
        ApexPages.StandardController stdController = new ApexPages.standardController(c);

        Test.startTest();
        ContactStageBarController cntrl = new ContactStageBarController(stdController);
        Test.stopTest();

        System.assert(!cntrl.currentStatusesList.isEmpty());
    }

}