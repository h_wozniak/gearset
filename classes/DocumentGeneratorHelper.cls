/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store globally used variables, methods, etc... for Document Generator
**/

global without sharing class DocumentGeneratorHelper {
    
    public static final String PL_OFFER_DEGREE_FULL_I = 'Studia pierwszego stopnia';
    public static final String PL_OFFER_DEGREE_FULL_II = 'Studia drugiego stopnia';
    public static final String PL_OFFER_DEGREE_FULL_II_PG = 'Studia drugiego stopnia z SP';
    public static final String PL_OFFER_DEGREE_FULL_U = 'Studia jednolite magisterskie';
    public static final String PL_OFFER_DEGREE_FULL_PG = 'Studia podyplomowe';
    public static final String PL_OFFER_DEGREE_FULL_MBA = 'Studia MBA';

    public static final String EN_OFFER_DEGREE_FULL_I = 'First degree studies';
 	public static final String EN_OFFER_DEGREE_FULL_II = 'Second degree studies';
    public static final String EN_OFFER_DEGREE_FULL_II_PG = 'Second degree studies with SP';
    public static final String EN_OFFER_DEGREE_FULL_U = 'Unified Master';
    public static final String EN_OFFER_DEGREE_FULL_PG = 'Postgraduate studies';
    public static final String EN_OFFER_DEGREE_FULL_MBA = 'MBA Studies';
    
    public static final String RU_OFFER_DEGREE_FULL_I = 'Первая степень';
    public static final String RU_OFFER_DEGREE_FULL_II = 'Второй цикл';
    public static final String RU_OFFER_DEGREE_FULL_II_PG = 'Второй цикл SP';
    public static final String RU_OFFER_DEGREE_FULL_U = 'Исследования цикла';
    public static final String RU_OFFER_DEGREE_FULL_PG = 'аспирант';
    public static final String RU_OFFER_DEGREE_FULL_MBA = 'курсы MBA';
}