public with sharing class LEX_Add_Specialization_Controller {

	@AuraEnabled
	public static Boolean hasEditAccess(String offerId) {
		return LEXServiceUtilities.hasEditAccess(offerId);
	}

	@AuraEnabled
	public static Boolean isCorrectDegree(String offerId) {
		Boolean result = false;
		Offer__c offer = [SELECT Degree__c FROM Offer__c WHERE Id = :offerId LIMIT 1];
		if (offer.Degree__c == 'II+PG') {
			result = true;
		}
		return result;
	}

	@AuraEnabled
	public static String getSpecializationRTId() {
		return CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_SPECIALIZATION);
	}
}