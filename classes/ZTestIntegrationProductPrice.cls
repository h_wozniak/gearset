@isTest
private class ZTestIntegrationProductPrice {



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- TEST DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest1() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));

        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = retMap.get(0).Id,
                Enrollment_only_for_Specialties__c = true,
                Number_of_Semesters__c = 7
        ), true));

        retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = retMap.get(1).Id
        ), true));

        retMap.put(3, ZDataTestUtility.createPriceBook(new Offer__c(
                Offer_from_Price_Book__c = retMap.get(2).Id,
                Entry_fee__c = 500,
                Onetime_Price__c = 5000
        ), true));

        configPriceBook(retMap.get(3).Id, 100);
        PriceBookManager.activatePriceBooks(new List<Id> { retMap.get(3).Id });

        // create Time Discount
        CommonUtility.preventEditingDiscounts = true;
        retMap.put(4, ZDataTestUtility.createTimeDiscount(new Discount__c(
                Offer_from_Offer_DC__c = ((Offer__c) retMap.get(0)).University_Study_Offer_from_Course__c,
                Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
                Discount_Value__c = 70,
                Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
                Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE,
                Trade_Name__c = 'TimeDiscountTradeName'
        ), true));

        retMap.put(5, ZDataTestUtility.createOfferTimeDiscount(new Discount__c(
                Offer_from_Offer_DC__c = retMap.get(0).Id,
                Discount_from_Offer_DC__c = retMap.get(4).Id
        ), true));

        // code Discount
        retMap.put(6, ZDataTestUtility.createCodeDiscount(new Discount__c(Trade_Name__c = 'CodeDiscountTradeName'), true));
        retMap.put(7, ZDataTestUtility.createPromoCode(new Discount__c(
                Discount_from_Promo_Code__c = retMap.get(6).Id,
                Promo_Code__c = 'CODE_XYZ'
        ), true));

        // create Time Discount Graduate
        CommonUtility.preventEditingDiscounts = true;
        retMap.put(8, ZDataTestUtility.createTimeDiscount(new Discount__c(
                Offer_from_Offer_DC__c = ((Offer__c) retMap.get(0)).University_Study_Offer_from_Course__c,
                Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
                Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE,
                Discount_Value__c = 70,
                Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
                Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE,
                Trade_Name__c = 'TimeDiscountTradeName'
        ), true));

        retMap.put(9, ZDataTestUtility.createOfferTimeDiscount(new Discount__c(
                Offer_from_Offer_DC__c = retMap.get(0).Id,
                Discount_from_Offer_DC__c = retMap.get(8).Id
        ), true));

        //create company discount
        retMap.put(10, ZDataTestUtility.createCompany(new Account(
                NIP__c = '6234233541'
        ), true));

        retMap.put(11, ZDataTestUtility.createCompanyDiscount(new Discount__c(
                Degrees__c = CommonUtility.OFFER_DEGREE_I,
                Trade_Name__c = 'CompanyDiscountTradeName',
                University_Names__c = RecordVals.WSB_NAME_WRO,
                University_for_sharing__c = 'WSB Wrocław'
        ), true));

        retMap.put(12, ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(
                Discount_from_Company_DC__c = retMap.get(11).Id,
                Company_from_Company_Discount__c = retMap.get(10).Id
        ), true));

        // retMap.put(13, ZDataTestUtility.createOfferCompanyDiscount(new Discount__c(
        //         Discount_from_Offer_DC__c = retMap.get(11).Id,
        //         Offer_from_Offer_DC__c = retMap.get(1).Id
        // ), true));

        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    private static String getProductName(Id prodId) {
        return [SELECT Name FROM Offer__c WHERE Id = :prodId].Name;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TEST POSITIVE CASES -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_ProductPrice_TimeDiscount() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(500, responseObj.entry_fee);
        System.assertEquals(700, responseObj.price_catalog);

        System.assertEquals(1, responseObj.discount_trade_names.size());
        System.assertEquals('TimeDiscountTradeName', responseObj.discount_trade_names[0]);

        System.assertEquals(90, responseObj.price_first_installment);
    }

    @isTest static void test_ProductPrice_CompanyDiscount() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = '6234233541';
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(500, responseObj.entry_fee);
        System.assertEquals(700, responseObj.price_catalog);

        System.assertEquals(1, responseObj.discount_trade_names.size());
        System.assertEquals('CompanyDiscountTradeName', responseObj.discount_trade_names[0]);

        System.assertEquals(28, responseObj.price_first_installment);
    }

    @isTest static void test_ProductPrice_TimeDiscount_graduate() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = true;
        requestJSON.wsb_graduate = true;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(500, responseObj.entry_fee);
        System.assertEquals(700, responseObj.price_catalog);

        System.assertEquals(1, responseObj.discount_trade_names.size());
        System.assertEquals('TimeDiscountTradeName', responseObj.discount_trade_names[0]);

        System.assertEquals(90, responseObj.price_first_installment);
    }

    @isTest static void test_ProductPrice_TimeDiscount_graduat2() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = true;
        requestJSON.polish_card_document_expiration_date = '20.10.2020';
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = true;
        requestJSON.wsb_graduate = true;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();
    }

    @isTest static void test_ProductPrice_TimeAndCodeDiscount() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = 'CODE_XYZ';
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(2, responseObj.discount_trade_names.size());

        System.assert(90 > responseObj.price_first_installment);
    }

    @isTest static void test_ProductPrice_TimeAndCodeDiscount_OneTime() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME;
        requestJSON.installments = 1;
        requestJSON.discount_code = 'CODE_XYZ';
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = false;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(2, responseObj.discount_trade_names.size());

        //System.assert(90 > responseObj.price_first_installment);
    }

    @isTest static void test_ProductPrice_TimeAndCodeAndRecPersonDiscount() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = 'CODE_XYZ';
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = false;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(3, responseObj.discount_trade_names.size());
    }

    @isTest static void test_ProductPrice_SecCourseAndCodeAndRecPersonDiscount() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = 'CODE_XYZ';
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = true;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        IntegrationProductPriceHelper.ProductPriceResponse_JSON responseObj =
                (IntegrationProductPriceHelper.ProductPriceResponse_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationProductPriceHelper.ProductPriceResponse_JSON.class);

        System.assertEquals(3, responseObj.discount_trade_names.size());
        System.assertEquals(
                CommonUtility.getPicklistLabelMap(Discount__c.Discount_Type_Studies__c).get(CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE),
                responseObj.discount_trade_names[0]
        );
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TEST NEGATIVE CASES -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_ProductPrice_ProductNotFound() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = 'FAKE_NAME';
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = true;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(400, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj =
                (IntegrationError.Error_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(801, responseObj.error_code);
    }

    @isTest static void test_ProductPrice_InvalidFieldValue() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = 'INVALID FIELD VALUE';
        requestJSON.installments = 2;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = true;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(500, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj =
                (IntegrationError.Error_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(600, responseObj.error_code);
    }

    @isTest static void test_ProductPrice_InvalidTuitionSystem() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME;
        requestJSON.installments = 10;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = true;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        //System.assertEquals(400, RestContext.response.statusCode);

        //IntegrationError.Error_JSON responseObj = 
        //    (IntegrationError.Error_JSON) 
        //    JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        //System.assertEquals(803, responseObj.error_code);
    }

    @isTest static void test_ProductPrice_InvalidInstallmentsVariant() {
        Map<Integer, sObject> dataMap = prepareDataForTest1();

        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        IntegrationProductPriceHelper.ProductPriceRequest_JSON requestJSON = new IntegrationProductPriceHelper.ProductPriceRequest_JSON();
        requestJSON.product_name = getProductName(dataMap.get(2).Id);
        requestJSON.citizenships = new List<String> { CommonUtility.CONTACT_CITIZENSHIP_POLISH };
        requestJSON.polish_card_document = false;
        requestJSON.polish_card_document_expiration_date = null;
        requestJSON.tuition_system = CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED;
        requestJSON.installments = 7;
        requestJSON.discount_code = null;
        requestJSON.company_for_discount_tax_id = null;
        requestJSON.recommended = true;
        requestJSON.next_product = true;
        requestJSON.teb_graduate = false;
        requestJSON.wsb_graduate = false;
        requestJSON.school_certificate_with_honors = false;

        req.requestBody = Blob.valueOf(JSON.serialize(requestJSON));

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(400, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj =
                (IntegrationError.Error_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(804, responseObj.error_code);
    }

    @isTest static void test_ProductPrice_InvalidJSON() {
        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf('NOT A CORRECT JSON STTRING {}[]!!');

        Test.startTest();
        IntegrationProductPriceResource.getProductPrice();
        Test.stopTest();

        System.assertEquals(400, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj =
                (IntegrationError.Error_JSON)
                        JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(601, responseObj.error_code);
    }

}