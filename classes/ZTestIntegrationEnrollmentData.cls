/**
*   @author         Sebastian Łasisz
*   @description    This class is used to test if functionality used to retrieve enrollment for given id
**/
@isTest
private class ZTestIntegrationEnrollmentData {
    
    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];
        
        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }
        
        update pb.InstallmentConfigs__r;
    }

    @isTest static void getEnrollmentData_null() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/enrollment/';
        req.addParameter('enrollment_id', null);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationEnrollmentData.getEnrollmentData();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(400, res.statusCode);
    }

    @isTest static void getEnrollmentData_withoutPB() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        ZDataTestUtility.prepareCatalogData(true, false);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(MobilePhone = '1234', Consent_Direct_Communications_ADO__c = CommonUtility.MARKETING_ENTITY_GDANSK), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/enrollment/';
        req.addParameter('enrollment_id', studyEnr.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationEnrollmentData.getEnrollmentData();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(400, res.statusCode);
    }

    @isTest static void getEnrollmentData() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        ZDataTestUtility.prepareCatalogData(true, false);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(MobilePhone = '1234', Consent_Direct_Communications_ADO__c = CommonUtility.MARKETING_ENTITY_GDANSK), true);
        Qualification__c career = ZDataTestUtility.createWorkExperience(new Qualification__c(Work_Experience__c = candidate.Id), true);
        
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
            	Candidate_Student__c = candidate.Id,
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/enrollment/';
        req.addParameter('enrollment_id', studyEnr.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationEnrollmentData.getEnrollmentData();

        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(200, res.statusCode);
    }
}