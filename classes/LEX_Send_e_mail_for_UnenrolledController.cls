public with sharing class LEX_Send_e_mail_for_UnenrolledController {

	@AuraEnabled
	public static String checkUnenrolled(String recordId) {
		Enrollment__c enr = [SELECT Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];
		return enr.Unenrolled__c ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String sendEmailWithDocumentsUnenrolled(String recordId) {
		return LEXStudyUtilities.sendEmailWithDocumentsUnenrolled(recordId) ? 'SUCCESS' : 'FAILURE';
	}
}