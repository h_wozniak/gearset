/**
* @description  This class is a Trigger Handler for Training Enrollments - rewriting field values (for emails/preview)
**/

public without sharing class TH_EnrollmentRewrites extends TriggerHandler.DelegateBase {

    List<Enrollment__c> trainingEnrollmentsToRewriteStatus;
    List<Enrollment__c> enrollmentParticipantResultToCopyStatus;
    List<Enrollment__c> enrollmentPRToCopyStatusToMainEnrollments;
    List<Enrollment__c> enrollmentsToRewriteEndDateFromSchedule;
    List<Enrollment__c> enrollmentsToCopyUniverNameFromSchedule;
    List<Enrollment__c> eprToCopyUniverNameFromSchedule;
    Set<Id> schedulesToCopyForEmailFields;
    List<Enrollment__c> enrollmentsToCopyContactDataFromUniversity;
    List<Enrollment__c> enrollmentsToCopyInvidualContact;



    public override void prepareBefore() {
        enrollmentParticipantResultToCopyStatus = new List<Enrollment__c>();
        enrollmentsToRewriteEndDateFromSchedule = new List<Enrollment__c>();
        enrollmentsToCopyUniverNameFromSchedule = new List<Enrollment__c>();
        eprToCopyUniverNameFromSchedule = new List<Enrollment__c>();
        enrollmentsToCopyContactDataFromUniversity = new List<Enrollment__c>();
    }

    public override void prepareAfter() {
        trainingEnrollmentsToRewriteStatus = new List<Enrollment__c>();
        enrollmentPRToCopyStatusToMainEnrollments = new List<Enrollment__c>();
        schedulesToCopyForEmailFields = new Set<Id>();
        enrollmentsToCopyInvidualContact = new List<Enrollment__c>();
    }

    public override void beforeInsert(List<sObject> o ) {
        List<Enrollment__c> newEnrollmentList = (List<Enrollment__c>)o;
        for(Enrollment__c newEnrollment : newEnrollmentList) {

            /* Wojciech Słodziak */
            // on Enrollment_Participant_Result insert get status from Enrollment_Training (parent record)
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c == null) {
                enrollmentParticipantResultToCopyStatus.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Enrollment insert get schedule end date from Schedule
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)) {
                enrollmentsToRewriteEndDateFromSchedule.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Enrollment insert get University Name from Schedule
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING) 
                || newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {
                enrollmentsToCopyUniverNameFromSchedule.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Enrollment Participant Result insert get University Name from Schedule
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)) {
                eprToCopyUniverNameFromSchedule.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Enrollment insert get University Contact Data
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING) 
                || newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {
                enrollmentsToCopyContactDataFromUniversity.add(newEnrollment);
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        Enrollment__c newEnrollment;

        for(Id key : newEnrollments.keySet()) {
            newEnrollment = newEnrollments.get(key);

            /* Wojciech Słodziak */
            // rewrite status from Enrollment participant to Enrollment. Used only for individual enrollments (Company__c == null) for openTrainings
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c != null) {
                enrollmentPRToCopyStatusToMainEnrollments.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // copy for email fields from Schedule on Main Enrollment insert
            if ((
                    newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING) 
                    || newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
                ) 
                && newEnrollment.Training_Offer__c != null
            ) {
                schedulesToCopyForEmailFields.add(newEnrollment.Training_Offer__c);
            }

            /* Sebastian Łasisz */
            // update client field with participant on invidual enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Company_from_Participant_Result__c == null) {
                enrollmentsToCopyInvidualContact.add(newEnrollment);
            }

        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        
    }
 
    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;

        for (Id key : oldEnrollments.keySet()) {
            Enrollment__c oldEnrollment = oldEnrollments.get(key);
            Enrollment__c newEnrollment = newEnrollments.get(key);

            /* Wojciech Słodziak */
            // rewrite status from Enrollment to Enrollment participant (rt different than Enrollment_Participant_Result)
            if (newEnrollment.RecordTypeId != CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c != oldEnrollment.Status__c 
                && 
                (
                    newEnrollment.recordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING) 
                    && newEnrollment.Company__c == null 
                    || newEnrollment.recordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
                )
            ) {
                trainingEnrollmentsToRewriteStatus.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // rewrite status from Enrollment participant to Enrollment. Used only for individual enrollments (Company__c == null) for openTrainings
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c != oldEnrollment.Status__c) {
                enrollmentPRToCopyStatusToMainEnrollments.add(newEnrollment);
            }
            
        }
        
    }

    //public override void beforeDelete(Map<Id, sObject> old) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {

        /* Wojciech Słodziak */
        // keeping Status__c in sync (by Enrollment_Training_Participant__c lookup)
        if (trainingEnrollmentsToRewriteStatus != null && !trainingEnrollmentsToRewriteStatus.isEmpty()) {
            Set<Id> teIdList = new Set<Id>();
            for (Enrollment__c te : trainingEnrollmentsToRewriteStatus) {
                teIdList.add(te.Id);
            }

            List<Enrollment__c> participantResultList = [
                SELECT Id, Status__c, Enrollment_Training_Participant__c 
                FROM Enrollment__c 
                WHERE Enrollment_Training_Participant__c IN :teIdList 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
            ];

            for (Enrollment__c te : trainingEnrollmentsToRewriteStatus) {
                for (Enrollment__c pr : participantResultList) {
                    if (te.Id == pr.Enrollment_Training_Participant__c) {
                        if (pr.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                            pr.Status__c = te.Status__c;
                        }
                    }
                }
            }

            try {
                update participantResultList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }
        if (enrollmentParticipantResultToCopyStatus != null && !enrollmentParticipantResultToCopyStatus.isEmpty()) {
            Set<Id> teIdList = new Set<Id>();
            for (Enrollment__c te : enrollmentParticipantResultToCopyStatus) {
                teIdList.add(te.Enrollment_Training_Participant__c);
            }

            List<Enrollment__c> trainingEnrollmentList = [SELECT Id, Status__c FROM Enrollment__c WHERE Id IN :teIdList];
            for (Enrollment__c te : trainingEnrollmentList) {
                for (Enrollment__c pr : enrollmentParticipantResultToCopyStatus) {
                    if (te.Id == pr.Enrollment_Training_Participant__c) {
                        pr.Status__c = te.Status__c;
                    }
                }
            }
        }
        // keeping Status__c in sync for individual enrollments when Status changes on EnrollmentTrainingParticipant
        if (enrollmentPRToCopyStatusToMainEnrollments != null && !enrollmentPRToCopyStatusToMainEnrollments.isEmpty()) {
            Set<Id> teIdList = new Set<Id>();
            for (Enrollment__c tpr : enrollmentPRToCopyStatusToMainEnrollments) {
                teIdList.add(tpr.Enrollment_Training_Participant__c);
            }

            List<Enrollment__c> mainEnrollmentList = [
                SELECT Id, Status__c 
                FROM Enrollment__c 
                WHERE Id IN :teIdList AND Company__c = null 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)
            ];

            for (Enrollment__c te : mainEnrollmentList) {
                for (Enrollment__c pr : enrollmentPRToCopyStatusToMainEnrollments) {
                    if (te.Id == pr.Enrollment_Training_Participant__c) {
                        te.Status__c = pr.Status__c;
                    }
                }
            }

            try {
                update mainEnrollmentList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // rewrite Schedule End Date to hidden field on Enrollment (used for timed workflow that changed status on Enrollment when scheduled training ends)
        if (enrollmentsToRewriteEndDateFromSchedule != null && !enrollmentsToRewriteEndDateFromSchedule.isEmpty()) {
            Set<Id> scheduleIds = new Set<Id>(); 
            for (Enrollment__c enr : enrollmentsToRewriteEndDateFromSchedule) {
                scheduleIds.add(enr.Training_Offer__c);
            }

            List<Offer__c> scheduleList = [
                SELECT Id, Valid_To__c 
                FROM Offer__c 
                WHERE Id IN :scheduleIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Enrollment__c enr : enrollmentsToRewriteEndDateFromSchedule) {
                for (Offer__c sch : scheduleList) {
                    if (enr.Training_Offer__c == sch.Id) {
                        enr.Schedule_End_Time_Copy__c = sch.Valid_To__c;
                    }
                }
            }
        }

        /* Wojciech Słodziak */
        // on Enrollment insert get University Name from Schedule (required for sharing rules)
        if (enrollmentsToCopyUniverNameFromSchedule != null && !enrollmentsToCopyUniverNameFromSchedule.isEmpty()) {
            Set<Id> scheduleIds = new Set<Id>(); 
            for (Enrollment__c enr : enrollmentsToCopyUniverNameFromSchedule) {
                scheduleIds.add(enr.Training_Offer__c);
            }

            List<Offer__c> scheduleList = [
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :scheduleIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Enrollment__c enr : enrollmentsToCopyUniverNameFromSchedule) {
                for (Offer__c sch : scheduleList) {
                    if (enr.Training_Offer__c == sch.Id) {
                        enr.University_Name__c = sch.University_Name__c;
                    }
                }
            }
        }

        /* Wojciech Słodziak */
        // on Enrollment insert get University Contact Data
        if (enrollmentsToCopyContactDataFromUniversity != null && !enrollmentsToCopyContactDataFromUniversity.isEmpty()) {
            Set<Id> scheduleIds = new Set<Id>(); 
            for (Enrollment__c enr : enrollmentsToCopyContactDataFromUniversity) {
                scheduleIds.add(enr.Training_Offer__c);
            }

            List<Offer__c> scheduleList = [
                SELECT Id, University_Contact_Data__c 
                FROM Offer__c 
                WHERE Id IN :scheduleIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Enrollment__c enr : enrollmentsToCopyContactDataFromUniversity) {
                for (Offer__c sch : scheduleList) {
                    if (enr.Training_Offer__c == sch.Id) {
                        enr.University_Contact_Data__c = sch.University_Contact_Data__c;
                    }
                }
            }
        }

        /* Wojciech Słodziak */
        // on Enrollment Participant Result insert get University Name from Schedule (required for sharing rules)
        if (eprToCopyUniverNameFromSchedule != null && !eprToCopyUniverNameFromSchedule.isEmpty()) {
            Set<Id> scheduleIds = new Set<Id>(); 
            for (Enrollment__c enr : eprToCopyUniverNameFromSchedule) {
                scheduleIds.add(enr.Training_Offer_for_Participant__c);
            }
            List<Offer__c> scheduleList = [
                SELECT Id, University_Name__c 
                FROM Offer__c 
                WHERE Id IN :scheduleIds 
                AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
            ];

            for (Enrollment__c enr : eprToCopyUniverNameFromSchedule) {
                for (Offer__c sch : scheduleList) {
                    if (enr.Training_Offer_for_Participant__c == sch.Id) {
                        enr.University_Name__c = sch.University_Name__c;
                    }
                }
            }
        }

        /* Sebastian Łasisz */
        // update client field with participant on invidual enrollment
        if(enrollmentsToCopyInvidualContact != null && !enrollmentsToCopyInvidualContact.isEmpty()) {
            Set<Id> mainEnrollmentIds = new Set<Id>(); 
            for (Enrollment__c enr : enrollmentsToCopyInvidualContact) {
                mainEnrollmentIds.add(enr.Enrollment_Training_Participant__c);
            }

            List<Enrollment__c> trainingEnrollmentList = [
                SELECT Id, Company__c, Individual_Contact_on_Enrollment__c, Enrollment_Training_Participant__c 
                FROM Enrollment__c 
                WHERE Id IN :mainEnrollmentIds AND Company__c = null
            ];

            for (Enrollment__c te : trainingEnrollmentList) {
                for (Enrollment__c pr : enrollmentsToCopyInvidualContact) {
                    if (te.Id == pr.Enrollment_Training_Participant__c) {
                        te.Individual_Contact_on_Enrollment__c = pr.Participant__c;
                    }
                }
            }

            try {
                update trainingEnrollmentList;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }

        /* Wojciech Słodziak */
        // copy for email fields from Schedule on Main Enrollment insert
        if (schedulesToCopyForEmailFields != null && !schedulesToCopyForEmailFields.isEmpty()) {
            EnrollmentManager_Trainings.copyForEmailFields(schedulesToCopyForEmailFields);
        }
        
    }

}