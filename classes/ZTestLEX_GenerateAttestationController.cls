@IsTest
private class ZTestLEX_GenerateAttestationController {

    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true));
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id, Candidate_Student__c = retMap.get(0).Id), true));

        return retMap;
    }

    private static User createUser() {
        User testUser = new User();
        testUser.UserName = 'testUser1@company12345612345xx.com';
        testUser.Email = 'testuser1@company.com';
        testUser.LastName = 'user';
        testUser.FirstName = 'test';
        testUser.Alias = 'alias';
        testUser.EmailEncodingKey = 'UTF-8';
        testUser.LanguageLocaleKey = 'pl';
        testUser.WSB_Department__c = RecordVals.WSB_NAME_WRO;
        testUser.LocaleSidKey = 'pl';
        testUser.TimeZoneSidKey = 'Europe/Berlin';
        testUser.ProfileId = ZDataTestUtility.getProfile(CommonUtility.PROFILE_SALES_BR).Id;
        testUser.UserRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id;

        insert testUser;

        return testUser;
    }

    @IsTest static void test_hasEditAccess() {
        User testUser = createUser();

        System.runAs(testUser) {
            Test.startTest();
            Map<Integer, sObject> dataMap = prepareData();
            Test.stopTest();

            Boolean result = LEX_GenerateAttestationController.hasEditAccess(dataMap.get(3).Id);
            System.assertEquals(result, true);
        }
    }

    @IsTest static void test_checkRecord() {
        User testUser = createUser();

        System.runAs(testUser) {
            Test.startTest();
            Map<Integer, sObject> dataMap = prepareData();
            Test.stopTest();

            Boolean result = LEX_GenerateAttestationController.checkRecord(dataMap.get(3).Id);
            System.assertEquals(result, true);
        }
    }

    @IsTest static void test_createAttestationDocument() {
        User testUser = createUser();

        System.runAs(testUser) {
            Test.startTest();
            Map<Integer, sObject> dataMap = prepareData();
            Test.stopTest();

            LEX_GenerateAttestationController.Result_JSON result = LEX_GenerateAttestationController.createAttestationDocument(dataMap.get(3).Id);
            System.assertEquals(result.success, false);
        }
    }

    @IsTest static void test_createAttestationDocument_2() {
        User testUser = createUser();


        Test.startTest();

        System.runAs(testUser) {
            CommonUtility.allowCreatingDocuments = true;
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ATTESTATION), true);
            Map<Integer, sObject> dataMap = prepareData();

            LEX_GenerateAttestationController.Result_JSON result = LEX_GenerateAttestationController.createAttestationDocument(dataMap.get(3).Id);
            System.assertEquals(result.success, true);
        }

        Test.stopTest();
    }

    @IsTest static void test_generateAttestationDocument() {
        Test.startTest();
        
        CommonUtility.allowCreatingDocuments = true;
        ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ATTESTATION), true);
        Map<Integer, sObject> dataMap = prepareData();
        Test.stopTest();

        LEX_GenerateAttestationController.Result_JSON result = LEX_GenerateAttestationController.createAttestationDocument(dataMap.get(3).Id);

        Boolean generationResult = LEX_GenerateAttestationController.generateAttestationDocument(result.message);
        System.assertEquals(generationResult, true);

        
    }
}