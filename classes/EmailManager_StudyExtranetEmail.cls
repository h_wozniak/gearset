/**
* @author       Sebastian Łasisz
* @description  Class to send Extranet Information Email - based on workflows.
**/

public without sharing class EmailManager_StudyExtranetEmail {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private enum WorfklowEmailNames { // also names of templates
        X29_I_Extranet, /* I U */
        X29_II_Extranet, /* II II_PG */
        X29_PG_Extranet, /* PG */
        X29_MBA_Extranet /* MBA */
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE DESIGNATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String designateWorkflowNameForEnrollment(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U) {
            workflow = WorfklowEmailNames.X29_I_Extranet;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            workflow = WorfklowEmailNames.X29_II_Extranet;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            workflow = WorfklowEmailNames.X29_PG_Extranet;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            workflow = WorfklowEmailNames.X29_MBA_Extranet;
        }

        return workflow != null? (language + '_' + workflow.name()) : null;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ INVOKATION METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method for sending Extranet Information email
    public static void sendExtranetInformationnEmail_fromButton(Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Degree__c, Candidate_Student__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
        ];

        List<EmailTemplate> emailTemplates = [
            SELECT Id, Name
            FROM EmailTemplate
        ];

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName = EmailManager_StudyExtranetEmail.designateWorkflowNameForEnrollment(enrollment);

            if (workflowTriggerName != null) {
                recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
                
                String emailTemplateId = '';
                String degree = '';
                if (enrollment.Degree__c == CommonUtility.OFFER_DEGREE_I ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_U) {
                    degree = CommonUtility.OFFER_DEGREE_I;
                }
                else if (enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II_PG ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II) {
                    degree = CommonUtility.OFFER_DEGREE_II;
                }
                else {
                    degree = enrollment.Degree__c;
                }

                String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
                for (EmailTemplate et : emailTemplates) {
                    if (degree == CommonUtility.OFFER_DEGREE_PG) degree = 'SP';
                    if (et.Name == '[' + language + '][29] Dostęp do Extranetu i numer albumu') {
                        emailTemplateId = et.Id;
                    }
                }

                tasksToInsert.add(EmailManager.createEmailTask(
                    Label.title_ExtranetSent,
                    enrollment.Id,
                    enrollment.Candidate_Student__c,
                    EmailManager.prepareFakeEmailBody(emailTemplateId, enrollment.Id),
                    null
                ));
            } else {
                enrollmentsWithFailure.add(enrollment);
            }
        }
        
        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }


        if (!enrollmentsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_StudyExtranetEmail.class.getName() 
                + ' ' + JSON.serialize(enrollmentsWithFailure));
        }
    }

    // method for sending Extranet Information email
    public static void sendExtranetInformationnEmail(Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Degree__c, Candidate_Student__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
            AND Dont_send_automatic_emails__c = false
        ];

        List<EmailTemplate> emailTemplates = [
            SELECT Id, Name
            FROM EmailTemplate
        ];

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName = EmailManager_StudyExtranetEmail.designateWorkflowNameForEnrollment(enrollment);

            if (workflowTriggerName != null) {
                recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
                
                String emailTemplateId = '';
                String degree = '';
                if (enrollment.Degree__c == CommonUtility.OFFER_DEGREE_I ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_U) {
                    degree = CommonUtility.OFFER_DEGREE_I;
                }
                else if (enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II_PG ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II) {
                    degree = CommonUtility.OFFER_DEGREE_II;
                }
                else {
                    degree = enrollment.Degree__c;
                }

                String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
                for (EmailTemplate et : emailTemplates) {
                    if (degree == CommonUtility.OFFER_DEGREE_PG) degree = 'SP';
                    if (et.Name == '[' + language + '][29] Dostęp do Extranetu i numer albumu') {
                        emailTemplateId = et.Id;
                    }
                }

                tasksToInsert.add(EmailManager.createEmailTask(
                    Label.title_ExtranetSent,
                    enrollment.Id,
                    enrollment.Candidate_Student__c,
                    EmailManager.prepareFakeEmailBody(emailTemplateId, enrollment.Id),
                    null
                ));
            } else {
                enrollmentsWithFailure.add(enrollment);
            }
        }
        
        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }


        if (!enrollmentsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_StudyExtranetEmail.class.getName() 
                + ' ' + JSON.serialize(enrollmentsWithFailure));
        }
    }

}