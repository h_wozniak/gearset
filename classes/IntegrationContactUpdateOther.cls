/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send updated Contact to other systems
**/
global without sharing class IntegrationContactUpdateOther {

    @Future(callout=true)
    public static void sendUpdatedContactToOtherSystems(Map<String, String> contacts) {
        sendUpdatedContactToOtherSystemsSync(contacts);
    }

    public static void sendUpdatedContactToOtherSystemsSync(Map<String, String> contacts) {
        Set<Id> contactsToPrepare = new Set<Id>();

        for (String contactId : contacts.keySet()) {
            if (contactId != null) {
                String contactIDAfterSplit = contactId.split(';').get(0);
                if (contactIDAfterSplit != null) {
                    Set<String> otherSystems;
                    if (!CommonUtility.contactsToSendToOtherSystems.containsKey(contactIDAfterSplit)) {
                        otherSystems = new Set<String>();
                        contactsToPrepare.add(contactIDAfterSplit);
                    } else {
                        otherSystems = CommonUtility.contactsToSendToOtherSystems.get(contactIDAfterSplit);
                    }

                    for (String contactSystem : contacts.get(contactId).split(';')) {
                        otherSystems.add(contactSystem);
                    }

                    CommonUtility.contactsToSendToOtherSystems.put(contactIDAfterSplit, otherSystems);
                }
            }
        }

        contactsToPrepare.remove(null);

        HttpRequest httpReq = new HttpRequest();

        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

        String createDidactics = esb.Update_Contacts_Other_Systems__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_PUT);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        List<Contact> contactsToUpdate = [
                SELECT Id, System_Updating_Contact__c, (SELECT Id FROM EducationalAgreements__r), (
                        SELECT Id
                        FROM Campaign_Contact__r
                        WHERE Campaign_Member__r.Active__c = true
                        AND Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__c != null
                ), (
                        SELECT Id
                        FROM iPressoContacts__r
                        WHERE iPressoId__c != null
                )
                FROM Contact
                WHERE Id IN :contactsToPrepare
        ];

        List<IntegrationContactHelper.ContactToUpdateWrapper> contactWrappersToSend;

        if (contactsToUpdate.size() > 0) {
            contactWrappersToSend = new List<IntegrationContactHelper.ContactToUpdateWrapper>();
            for (Contact contactToUpdate : contactsToUpdate) {
                IntegrationContactHelper.ContactToUpdateWrapper wrapper = new IntegrationContactHelper.ContactToUpdateWrapper();
                wrapper.contact_id = contactToUpdate.Id;

                if (contactToUpdate.EducationalAgreements__r.size() > 0
                        && CommonUtility.contactsToSendToOtherSystems.get(contactToUpdate.Id).contains(RecordVals.UPDATING_SYSTEM_EXPERIA)) {
                    wrapper.ifExperia = true;
                }
                //if (CommonUtility.contactsToSendToOtherSystems.get(contactToUpdate.Id).contains('ZPI')) {
                //    wrapper.ifZPI = true;
                //}
                if (contactToUpdate.iPressoContacts__r.size() > 0 &&
                        CommonUtility.contactsToSendToOtherSystems.get(contactToUpdate.Id).contains(RecordVals.UPDATING_SYSTEM_MA)) {
                    wrapper.ifMA = true;
                }
                if (contactToUpdate.Campaign_Contact__r.size() > 0 &&
                        CommonUtility.contactsToSendToOtherSystems.get(contactToUpdate.Id).contains(RecordVals.UPDATING_SYSTEM_FCC)) {
                    wrapper.ifFCC = true;
                }

                if (wrapper.ifExperia || wrapper.ifZPI || wrapper.ifMA || wrapper.ifFCC) {
                    contactWrappersToSend.add(wrapper);
                }
            }
        }

        if (contactWrappersToSend != null && contactWrappersToSend.size() > 0) {
            String jsonToSend = IntegrationContactHelper.prepareResponse(contactWrappersToSend);
            httpReq.setBody(jsonToSend);

            if (!Test.isRunningTest()) {
                Http http = new Http();
                HTTPResponse httpRes = http.send(httpReq);

                Boolean success = httpRes.getStatusCode() == 200;

                if (!success) {
                    ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationContactUpdateOther.class.getName()
                            + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + contactsToPrepare);
                }
            }
        }
    }
}