/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to iPresso Business Logic
**/

public without sharing class IPressoManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- REMOVING EMAIL LOGIC ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     * Method for detecting removed email address. If found remove iPresso contact.
     *
     * @param emailRemovalWrappers List of email wrapper containing contact Id, old emails, new emails.
     */
    public static void removeIPressoContactOnEmailDeletion(List<EmailRemovalWrapper> emailRemovalWrappers) {
        /* Prepare map with email addresses differences */
        List<String> emailsToFindIpressoContact = new List<String>();
        for (EmailRemovalWrapper erw : emailRemovalWrappers) {
            for (String oldEmail : erw.oldEmails) {
                if (!erw.newEmails.contains(oldEmail)) {
                    emailsToFindIpressoContact.add(oldEmail);
                }
            }
        }

        List<Marketing_Campaign__c> iPressoContactsToRemove = prepareIPressoContact(emailsToFindIpressoContact);

        try {
            delete iPressoContactsToRemove;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- CHANGING EMAIL LOGIC ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     *
     * Method used to:
     * - create new iPresso contacts (when iPresso contact with given email doesn't exist)
     * - repin old iPresso contact to newly created contact (when iPresso contact with given email exist and user agrees for that operation)
     *      !! if method is run from Integration it is assumed that user agrees to repin iPresso contact !!
     *
     * @param contactsToAddError List of contacts used to repin iPresso contact from already existing contact to newly crated one.
     * @param emailRemovalWrappers List of wrappers containing contact Id, old values of emails, new values of emails.
     */
    public static void changeEmailAddressOnContact(List<Contact> contactsToAddError, List<EmailRemovalWrapper> emailRemovalWrappers) {
        /* Prepare map with email addresses differences */
        Map<Id, Set<String>> newEmailsToIdMap = new Map<Id, Set<String>>();
        List<String> emailsToFindIpressoContact = new List<String>();
        for (EmailRemovalWrapper erw : emailRemovalWrappers) {
            Set<String> newEmailsForContact = new Set<String>();

            for (String newEmail : erw.newEmails) {
                if (!erw.oldEmails.contains(newEmail)) {
                    if (erw.contactId != null) {
                        newEmailsForContact.add(newEmail);
                    }

                    emailsToFindIpressoContact.add(newEmail);
                }
            }

            newEmailsToIdMap.put(erw.contactId, newEmailsForContact);
        }

        List<Marketing_Campaign__c> iPressoContactsToCheckEmail = prepareIPressoContact(emailsToFindIpressoContact);

        /* If Contact doesnt have any emails connected with iPresso Id, create new iPresso Contacts */
        List<Marketing_Campaign__c> newIPressoContacts = prepareIPressoContactsToUpdate(iPressoContactsToCheckEmail, newEmailsToIdMap);

        for (Marketing_Campaign__c iPressoContactToCheckEmail : iPressoContactsToCheckEmail) {
            Id contactIdToRepin;

            for (Contact contactToAddError : contactsToAddError) {
                if (contactToAddError.Email == iPressoContactToCheckEmail.Lead_Email__c
                        || (contactToAddError.Additional_Emails__c != null && contactToAddError.Additional_Emails__c.contains(iPressoContactToCheckEmail.Lead_Email__c))) {
                    if (!CommonUtility.preventTriggeringEmailValidation) {
                        contactToAddError.addError(prepareResponseForDuplicateEmail(contactToAddError, iPressoContactToCheckEmail, newIPressoContacts), false);
                    } else {
                        contactIdToRepin = contactToAddError.Id;
                    }
                }

                /* If contact is found with duplicated email, reping iPresso contact */
                if (contactIdToRepin != null) {
                    iPressoContactToCheckEmail.Contact_from_iPresso__c = contactIdToRepin;
                    newIPressoContacts.add(iPressoContactToCheckEmail);
                }
            }
        }

        try {
            Database.upsert(newIPressoContacts, false);

            /* Prepare set of contacts to send to iPresso */
            Set<Id> contactIds = new Set<Id>();
            for (Marketing_Campaign__c iPresso : newIPressoContacts) {
                contactIds.add(iPresso.Contact_from_iPresso__c);
            }

            if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
                if (!CommonUtility.preventSendingContactToIPressoAsAsync) {
                    if (contactIds.size() <= 10) {
                        IntegrationIpressoContactCreationSender.sendIPressoContactListAtFuture(contactIds);
                    } else {
                        Database.executebatch(new IntegrationIpressoContactCreationBatch(contactIds), 10);
                    }
                }
            }
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /**
     * @author Sebastian Lasisz (I am not responsible of this code. They made me write it, against my will.)
     *
     * Method responsible for displaying error message when there's duplicate iPresso contact. Message provides user with 2 options
     * - accept, which repins iPresso contact to newly created record
     * - cancel, which cancels whole operation
     *
     * @param contactToUpdate Newly created/edited contact that has duplicated email.
     * @param iPressoContact Current iPresso contact that might be repinned.
     * @param iPressoContacts Rest of iPresso contacts that are supposed to be upserted in current transaction.
     *
     * @return HTML value that performs DML operation or cancels current transaction.
     */
    private static String prepareResponseForDuplicateEmail(Contact contactToUpdate, Marketing_Campaign__c iPressoContact, List<Marketing_Campaign__c> iPressoContacts) {
        String jsonContactObj = JSON.serialize(contactToUpdate);

        String jsonIPressoContactObj = JSON.serialize(iPressoContact);
        String jsonIPressoContactsObj = JSON.serialize(iPressoContacts);

        String script =
                '<script src="/soap/ajax/22.0/connection.js" type="text/javascript"></script>'
                        + '<script src="/soap/ajax/22.0/apex.js" type="text/javascript"></script>'
                        + '<script src="/resource/jQuery"></script>'
                        + '<script src="/resource/jsModal"></script>'
                        + '<script>'
                        + 'function doSomething() {'
                        + 'sforce.connection.sessionId = "' + UserInfo.getSessionId() + '";'
                        + 'j$ = jQuery.noConflict();'
                        + 'var jsonContactObj = \'' + jsonContactObj + '\';'
                        + 'jQuery.modal(\'<div style="height:70px; width:70px; color: black; background-color: white; border-radius: 10px; padding:20px 20px 20px 20px; -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.75);"><div style="font-weight: bold; text-align: center;">' + Label.modal_Loading + '...</div><div style="padding: 15px 20px;"><div style=" height: 31px; width: 31px; background: url(/resource/AjaxLoaderGif) no-repeat" ></div></div></div>\');'
                        + 'var jsonIPressoContactObj = \'' + jsonIPressoContactObj + '\';'
                        + 'var jsonIPressoContactsObj = \'' + jsonIPressoContactsObj + '\';'
                        + 'var result = sforce.apex.execute("WebserviceUtilities", "updateContactAndRepinIPressoContact", {contactJSON: jsonContactObj, iPressoJSON : jsonIPressoContactObj, iPressoContactsJSON : jsonIPressoContactsObj});'
                        + 'window.location.replace("/" + result);'
                        + '}'
                        + '</script>';

        script += ' ' + Label.msg_error_duplicateEmail;
        script += ' (<a style="color: blue;" href="/' + iPressoContact.Contact_from_iPresso__r.Id + '">' + (String.isBlank(iPressoContact.Contact_from_iPresso__r.FirstName) ? '' : iPressoContact.Contact_from_iPresso__r.FirstName + ' ') + iPressoContact.Contact_from_iPresso__r.LastName + '</a>). ' + Label.msg_error_duplicateEmail2;

        if (contactToUpdate.Id != null) {
            script += ('<table style="margin: 0 auto;"><tbody><tr><td><a style="cursor: pointer; text-decoration: underline;" href="/' + contactToUpdate.Id + '">' + Label.title_dontChangeEmail + '</a></td><td style="color: #c00;"> | </td><td><a style="cursor: pointer; text-decoration: underline;" onclick="doSomething()">' + Label.title_changeEmail + '</a></td></tr></tbody></table>');
        } else {
            script += ('<table style="margin: 0 auto;"><tbody><tr><td><a style="cursor: pointer; text-decoration: underline;" href="/' + WebserviceUtilities.getPrefixWebservice('Contact') + '/o">' + Label.title_dontChangeEmail + '</a></td><td style="color: #c00;"> | </td><td><a style="cursor: pointer; text-decoration: underline;" onclick="doSomething()">' + Label.title_changeEmail + '</a></td></tr></tbody></table>');
        }


        return script;
    }


    /* Method for repining ipresso contact for when error happens */
    public static String repinIPressoContact(Contact contactToUpdate, Marketing_Campaign__c iPressoContact, List<Marketing_Campaign__c> iPressoContactsToInsert) {
        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            if (CommonUtility.preventChangingEmailTwice) {
                upsert contactToUpdate;
            }

            List<Contact> contactExists = [
                    SELECT Id
                    FROM Contact
                    WHERE Id = :contactToUpdate.Id
            ];

            if (!contactExists.isEmpty()) {
                iPressoContact.Contact_from_iPresso__c = contactToUpdate.Id;
                update iPressoContact;

                /* For Merging determine whether ipresso contact has been already repinned, if yes dont create another instance of ipresso contact */
                Set<Id> contactIdsToDetermineIPressoContacts = new Set<Id>();
                for (Marketing_Campaign__c iPressoContactToValidate : iPressoContactsToInsert) {
                    contactIdsToDetermineIPressoContacts.add(iPressoContactToValidate.Contact_from_iPresso__c);
                }

                List<Marketing_Campaign__c> iPressoContacts = [
                        SELECT Id, Contact_from_iPresso__c, Lead_Email__c
                        FROM Marketing_Campaign__c
                        WHERE Contact_from_iPresso__c IN :contactIdsToDetermineIPressoContacts
                        AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                ];

                for (Marketing_Campaign__c iPressoContactToValidate : iPressoContacts) {
                    for (Integer i = 0; i < iPressoContactsToInsert.size(); i++) {
                        if (iPressoContactsToInsert.get(i).Contact_from_iPresso__c == iPressoContactToValidate.Contact_from_iPresso__c
                                && iPressoContactsToInsert.get(i).Lead_Email__c == iPressoContactToValidate.Lead_Email__c
                                && iPressoContactsToInsert.get(i).Id == null) {
                            iPressoContactsToInsert.remove(i);
                            i = i > 0 ? i-- : 0;
                        }
                    }
                }

                insert iPressoContactsToInsert;

            }

            return contactToUpdate.Id;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        return null;
    }


    /**
     * @author Sebastian Lasisz
     *
     * Check whether added email is already added to another contact. If not add new iPresso Contact (TO OPTIMIZE!! or potentially remove)
     *
     * @param wrapperToAddiPressoContacts List of Email wrapper consisting of contact Id, new emails values and old emails values.
     */
    public static void addNewIPressoContacts(List<EmailRemovalWrapper> wrapperToAddiPressoContacts) {
        List<String> emailsToFindIpressoContact = new List<String>();
        for (EmailRemovalWrapper erw : wrapperToAddiPressoContacts) {
            for (String newEmail : erw.newEmails) {
                if (!erw.oldEmails.contains(newEmail)) {
                    emailsToFindIpressoContact.add(newEmail);
                }
            }
        }

        List<Marketing_Campaign__c> iPressoContactsToCheckEmail = prepareIPressoContact(emailsToFindIpressoContact);
        List<EmailIPressoWrapper> newEmailsToAddiPressoContacts = new List<EmailIPressoWrapper>();
        if (iPressoContactsToCheckEmail.size() == 0) {
            Set<String> newEmails = new Set<String>();
            newEmails.addAll(emailsToFindIpressoContact);

            for (EmailRemovalWrapper contactToAddError : wrapperToAddiPressoContacts) {
                for (String email : emailsToFindIpressoContact) {
                    if (contactToAddError.newEmails.contains(email)) {
                        newEmailsToAddiPressoContacts.add(new EmailIPressoWrapper(contactToAddError.contactId, email));
                    }
                }
            }
        } else {
            for (Marketing_Campaign__c existingIPressoContact : iPressoContactsToCheckEmail) {
                for (EmailRemovalWrapper contactToAddError : wrapperToAddiPressoContacts) {
                    Set<String> newEmails = contactToAddError.newEmails;
                    for (String email : newEmails) {
                        if (email != existingIPressoContact.Lead_Email__c) {
                            newEmailsToAddiPressoContacts.add(new EmailIPressoWrapper(contactToAddError.contactId, email));
                        }
                    }
                }
            }
        }

        try {
            List<Marketing_Campaign__c> iPressoMembers = createNewIPressoContactsForNewEmails(newEmailsToAddiPressoContacts);
            insert iPressoMembers;

            Set<Id> contactIds = new Set<Id>();
            for (Marketing_Campaign__c iPresso : iPressoMembers) {
                contactIds.add(iPresso.Contact_from_iPresso__c);
            }
            if (!Test.isRunningTest() && !System.isFuture() && !System.isBatch()) {
                if (!CommonUtility.preventSendingContactToIPressoAsAsync) {
                    if (contactIds.size() <= 10) {
                        IntegrationIpressoContactCreationSender.sendIPressoContactListAtFuture(contactIds);
                    } else {
                        IntegrationIpressoContactCreationBatch ipressoBatch = new IntegrationIpressoContactCreationBatch(contactIds);
                        Database.executebatch(ipressoBatch, 10);
                    }
                }
            }
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- ADDING SCORING TO IPRESSO CONTACT LOGIC ----------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     *
     * update main scoring on contact after adding scoring to ipresso contact
     *
     * @param iPressoContacts List of Ipresso Contacts to update scoring on Contact based on Email field.
     */
    public static void updateMainScoringOnContact(List<Marketing_Campaign__c> iPressoContacts) {
        Set<Id> contactIds = new Set<Id>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
            contactIds.add(iPressoContact.Contact_from_iPresso__c);
        }

        List<Contact> contactsToUpdateScoring = [
                SELECT Id, Lead_Scoring__c, Email
                FROM Contact
                WHERE Id IN :contactIds
        ];

        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
            for (Contact contactToUpdateScoring : contactsToUpdateScoring) {
                if (contactToUpdateScoring.Id == iPressoContact.Contact_from_iPresso__c
                        && contactToUpdateScoring.Email == iPressoContact.Lead_Email__c) {
                    contactToUpdateScoring.Lead_Scoring__c = iPressoContact.Scoring__c;
                }
            }
        }

        try {
            update contactsToUpdateScoring;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- REMOVING CONTACT LOGIC ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    /**
     * @author Sebastian Łasisz
     *
     * Disallow deleting contact record through merging if Contact has any iPresso Ids connected
     *
     * @param personAccountsToCheck List of Contacts to validate whether records can be deleted.
     */
    public static void disableRemovingContactConnectedToIPresso(List<Contact> contactsToCheck) {
        Set<String> emails = new Set<String>();
        Set<Id> contactIds = new Set<Id>();
        for (Contact contactToCheck : contactsToCheck) {
            emails = splitEmails(contactToCheck.Email, contactToCheck.Additional_Emails__c);
            contactIds.add(contactToCheck.Id);
        }

        List<Marketing_Campaign__c> iPressoContacts = [
                SELECT Id, Contact_from_iPresso__c, Lead_Email__c
                FROM Marketing_Campaign__c
                WHERE Lead_Email__c IN :emails
                AND IpressoId__c != null
                AND Contact_from_iPresso__c IN :contactIds
        ];

        for (Contact contactToCheck : contactsToCheck) {
            for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
                if (contactToCheck.Email == iPressoContact.Lead_Email__c
                        || contactToCheck.Additional_Emails__c.contains(iPressoContact.Lead_Email__c)) {
                    contactToCheck.addError(Label.msg_error_cantMerge);
                }
            }
        }
    }

    /**
     * @author Sebastian Łasisz, Radosław Urbański
     *
     * Disallow deleting person acocount record through merging if Contact has any iPresso Ids connected
     *
     * @param personAccountsToCheck List of Person Accounts to validate whether records can be deleted.
     */

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ ADDING NEW CONTACT LOGIC ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for parsing queried iPresso Contacts and newly created contacts. For records with new email without iPresso record creates new iPresso contact.
     *
     * @param iPressoContactsToCheckEmail List of existing iPresso Contacts in CRM.
     * @param emailsToFindIpressoContact Map containing contact Ids with their new emails.
     *
     * @return List of iPresso Contact containing records based on freshly inserted contacts (new emails only - ignore emails that are already in db).
     */
    private static List<Marketing_Campaign__c> prepareIPressoContactsToUpdate(List<Marketing_Campaign__c> iPressoContactsToCheckEmail, Map<Id, Set<String>> emailsToFindIpressoContact) {
        List<EmailIPressoWrapper> newEmailsToAddiPressoContacts = new List<EmailIPressoWrapper>();

        // if there is no ipresso contacts for given list of emails.
        if (iPressoContactsToCheckEmail.size() == 0) {
            for (Id contactId : emailsToFindIpressoContact.keySet()) {
                for (String email : emailsToFindIpressoContact.get(contactId)) {
                    newEmailsToAddiPressoContacts.add(new EmailIPressoWrapper(contactId, email));
                }
            }
        } else {
            Set<String> processedEmails = new Set<String>();

            /* Process contacts that do have any relation with previously created iPresso Contact - using same Email*/
            /* Take in consideration only those iPresso contacts that has same contact Id but different email */
            for (Marketing_Campaign__c iPressoContactToCheckEmail : iPressoContactsToCheckEmail) {
                for (Id contactId : emailsToFindIpressoContact.keySet()) {
                    for (String email : emailsToFindIpressoContact.get(contactId)) {
                        if (email == iPressoContactToCheckEmail.Lead_Email__c) {
                            processedEmails.add(iPressoContactToCheckEmail.Lead_Email__c);
                        } else if (!processedEmails.contains(email)) {
                            newEmailsToAddiPressoContacts.add(new EmailIPressoWrapper(contactId, email));
                            processedEmails.add(iPressoContactToCheckEmail.Lead_Email__c);
                        }
                    }

                }
            }
        }

        return createNewIPressoContactsForNewEmails(newEmailsToAddiPressoContacts);
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for creating new iPresso contacts based on Email Wrapper.
     *
     * @param newEmailsToAddiPressoContacts List of Email Wrappers having contact Id and Email that are neccessary for creating new iPresso contact.
     *
     * @return List of iPresso Contacts that will be inserted to DB.
     */
    public static List<Marketing_Campaign__c> createNewIPressoContactsForNewEmails(List<EmailIPressoWrapper> newEmailsToAddiPressoContacts) {
        List<Marketing_Campaign__c> newIPressoContacts = new List<Marketing_Campaign__c>();
        for (EmailIPressoWrapper emailToAddIPressoContact : newEmailsToAddiPressoContacts) {
            newIPressoContacts.add(createCampaignContactIPresso(emailToAddIPressoContact.contactId, emailToAddIPressoContact.Email));
        }

        return newIPressoContacts;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- MARKETING CAMPAIGN LOGIC ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     *
     * Helper method to create campaign member for iPresso Campaign
     *
     * @param campaignId Id of campaign to add junction to.
     * @param contactId Id of contact that iPresso Contact should be linked.
     *
     * @return New iPresso Member that will be inserted.
     */
    public static Marketing_Campaign__c createCampaignMemberIPresso(Id campaignId, Id contactId) {
        Marketing_Campaign__c newCampaignMember = new Marketing_Campaign__c();
        newCampaignMember.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER);
        newCampaignMember.Campaign_Contact__c = contactId;
        newCampaignMember.Campaign_Member__c = campaignId;

        return newCampaignMember;
    }

    /**
     * @author Sebastian Lasisz
     *
     * Helper method to create iPresso Contact for given Contact Id with given Email address.
     *
     * @param contactId Id of contact that iPresso Contact should be linked.
     * @param email Address email of iPresso Contact.
     *
     * @return New iPresso Contact that will be inserted.
     */
    public static Marketing_Campaign__c createCampaignContactIPresso(Id contactId, String email) {
        Marketing_Campaign__c newCampaignMember = new Marketing_Campaign__c();
        newCampaignMember.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT);
        newCampaignMember.Contact_from_iPresso__c = contactId;
        newCampaignMember.Lead_Email__c = email;

        return newCampaignMember;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz
     * Helper method for IpressoManager methods. Responsible for finding Marketing Campaigns with given email list.
     *
     * @param emailsToFindIpressoContact List of changed emails taken from edited Contact.
     *
     * @return List of iPresso contacts that are assigned to given list of emails.
     */
    private static List<Marketing_Campaign__c> prepareIPressoContact(List<String> emailsToFindIpressoContact) {
        List<Marketing_Campaign__c> iPressoContactsToFilter = [
                SELECT Id, Lead_Email__c, Contact_from_iPresso__c, Contact_from_iPresso__r.FirstName, Contact_from_iPresso__r.LastName
                FROM Marketing_Campaign__c
                WHERE Lead_Email__c IN :emailsToFindIpressoContact
                AND Lead_Email__c != null
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        return iPressoContactsToFilter;
    }

    /**
     * @author Sebastian Lasisz
     *
     * Helper method for removeIPressoContactOnEmailDeletion to make list of emails based on addtional and main email
     *
     * @param mainEmail Email address based on main field (Email, Person Email).
     * @param additionalEmails Email address based on Additional Emails field.
     *
     * @return Set of unique email addresses for given options.
     */
    public static Set<String> splitEmails(String mainEmail, String additionalEmails) {
        List<String> listsOfAdditionalEmails = new List<String>();
        if (additionalEmails != null) {
            listsOfAdditionalEmails = additionalEmails.split('\\; ');
        }
        if (mainEmail != null) {
            listsOfAdditionalEmails.add(mainEmail);
        }

        Set<String> setOfEmails = new Set<String>();
        setOfEmails.addAll(listsOfAdditionalEmails);
        return setOfEmails;
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for determining global consents for given Contact (triggers from Integration).
     *
     * @param contactWithConsent Contact containing information about global consents.
     * @param iPressoConsentList List of consent definitions to determine global consents for Person Account.
     *
     * @return Map of consent Ids with its values for given contact.
     */
    public static Map<String, Boolean> prepareGlobalConsentsForContact(Contact contactWithConsent, List<Catalog__c> iPressoConsentList) {
        List<Catalog__c> catalogConsentsToValidate = new List<Catalog__c>();
        for (Catalog__c consent : iPressoConsentList) {
            // Add to list consents that are only considered as global
            if (CommonUtility.getOptionsFromMultiSelect(consent.ADO__c).size() > 1) {
                catalogConsentsToValidate.add(consent);
            }
        }

        Map<String, Boolean> consentsForContact = new Map<String, Boolean>();

        for (Catalog__c catalogConsent : catalogConsentsToValidate) {
            if (contactWithConsent != null && CatalogManager.getApiADONameFromConsentName(catalogConsent.Name) != null
                    && contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)) != null) {

                Set<String> contactAcceptedADO = CommonUtility.getOptionsFromMultiSelect((String) contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)));
                Set<String> catalogSelectedADO = CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c);
                consentsForContact.put(catalogConsent.IPressoId__c, contactAcceptedADO.containsAll(catalogSelectedADO));
            } else {
                consentsForContact.put(catalogConsent.IPressoId__c, false);
            }
        }

        return consentsForContact;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class EmailRemovalWrapper {
        public String contactId;
        public Set<String> oldEmails;
        public Set<String> newEmails;

        public EmailRemovalWrapper(String contactId, Set<String> oldEmails, Set<String> newEmails) {
            this.contactId = contactId;
            this.oldEmails = oldEmails;
            this.newEmails = newEmails;
        }
    }

    public class EmailIPressoWrapper {
        public String contactId;
        public String email;

        public EmailIPressoWrapper(String contactId, String email) {
            this.contactId = contactId;
            this.email = email;
        }
    }
}