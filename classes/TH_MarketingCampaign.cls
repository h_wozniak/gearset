public without sharing class TH_MarketingCampaign extends TriggerHandler.DelegateBase {

    List<Marketing_Campaign__c> marketingCampaignToRewriteDetails;
    List<Marketing_Campaign__c> marketingCampaignToCheckDuplicates;
    List<Marketing_Campaign__c> newMarketingCampaignsToCheckDetailsAfterClearing;
    List<Marketing_Campaign__c> oldMarketingCampaignsToCheckDetailsAfterClearing;
    List<Marketing_Campaign__c> marketingCampaingsToCheckDetailsRelation;
    List<Marketing_Campaign__c> marketingCampaignToCheckDuplicatesOnOffer;
    List<Marketing_Campaign__c> marketingCampaignsToUpdateActiveDetails;
    List<Marketing_Campaign__c> marketingCampaignsToCheckAttachedDetails;
    List<Marketing_Campaign__c> marketingCampaignToRewriteNameToDetails;
    List<Marketing_Campaign__c> marketingCampaignsToAddClassifiers;
    List<Marketing_Campaign__c> marketingSalesCampaignsToAddClassifiers;
    List<Marketing_Campaign__c> returnedLeadsToCreateContacts;
    List<Marketing_Campaign__c> marketingCampaignsToCheckIfCanActivate;
    List<Marketing_Campaign__c> iPressoContactsToUpdateScoringOnContact;
    List<Marketing_Campaign__c> insertedCampaignMembersWithoutDetails;
    Map<String, String> contactIdsToUpdateOtherSystems;
    Set<Id> marketingCampaignToChangeStatusOnWelcomeCall;
    Set<Id> marketingCampaignsToUpdateMembersAfterResignationWelcomeCall;
    Set<Id> marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll;
    Set<Id> newMarketingMembersWelcomeCallToSetClassifier;
    Set<Id> marketingCampaignsToSendMembersToFCC;
    Set<Id> marketingCampaignsToAttachSystemClassifiers;
    Set<Id> externalMarketingCampaignsToAddSystemClassifiers;
    Set<String> iPressoContactsToRemoveFromIPresso;
    Set<Id> checkSynchronizedStatusOnTags;
    Set<Id> contactsToCheckPhonesAfterInsertion;
    Set<Id> contactsToCheckZPIStatus;

    public override void prepareBefore() {
        marketingCampaignToCheckDuplicates = new List<Marketing_Campaign__c>();
        newMarketingCampaignsToCheckDetailsAfterClearing = new List<Marketing_Campaign__c>();
        oldMarketingCampaignsToCheckDetailsAfterClearing = new List<Marketing_Campaign__c>();
        marketingCampaingsToCheckDetailsRelation = new List<Marketing_Campaign__c>();
        marketingCampaignToCheckDuplicatesOnOffer = new List<Marketing_Campaign__c>();
        marketingCampaignsToUpdateActiveDetails = new List<Marketing_Campaign__c>();
        marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll = new Set<Id>();
        marketingCampaignsToCheckAttachedDetails = new List<Marketing_Campaign__c>();
        contactIdsToUpdateOtherSystems = new Map<String, String>();
        returnedLeadsToCreateContacts = new List<Marketing_Campaign__c>();
        marketingCampaignsToCheckIfCanActivate = new List<Marketing_Campaign__c>();
        iPressoContactsToRemoveFromIPresso = new Set<String>();
        checkSynchronizedStatusOnTags = new Set<Id>();
    }

    public override void prepareAfter() {
        newMarketingMembersWelcomeCallToSetClassifier = new Set<Id>();
        marketingCampaignToRewriteDetails = new List<Marketing_Campaign__c>();
        marketingCampaignsToUpdateMembersAfterResignationWelcomeCall = new Set<Id>();
        marketingCampaignToChangeStatusOnWelcomeCall = new Set<Id>();
        marketingCampaignsToAddClassifiers = new List<Marketing_Campaign__c>();
        marketingSalesCampaignsToAddClassifiers = new List<Marketing_Campaign__c>();
        marketingCampaignsToSendMembersToFCC = new Set<Id>();
        contactIdsToUpdateOtherSystems = new Map<String, String>();
        marketingCampaignsToAttachSystemClassifiers = new Set<Id>();
        marketingCampaignToRewriteNameToDetails = new List<Marketing_Campaign__c>();
        externalMarketingCampaignsToAddSystemClassifiers = new Set<Id>();
        iPressoContactsToUpdateScoringOnContact = new List<Marketing_Campaign__c>();
        contactsToCheckPhonesAfterInsertion = new Set<Id>();
        contactsToCheckZPIStatus = new Set<Id>();
        insertedCampaignMembersWithoutDetails = new List<Marketing_Campaign__c>();
    }

    public override void beforeInsert(List<sObject> o) {
        List<Marketing_Campaign__c> newMCList = (List<Marketing_Campaign__c>)o;
        for(Marketing_Campaign__c newMC : newMCList) {

            /* Sebastian Łasisz */
            // check duplicates for campaign member
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignToCheckDuplicates.add(newMc);
            }

            /* Sebastian Łasisz */
            // check duplicates for campaign offer
            if  (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_OFFER)) {
                marketingCampaignToCheckDuplicatesOnOffer.add(newMc);
            }

            /* Sebastian Łasisz */
            // set active details based on filled information
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignsToUpdateActiveDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // update Campaign Member Enrollment when Classifier is set to Resignation on Confirmation of Enrollment Campaign
            if (!CommonUtility.skipEnrOrCampaignUpdate && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER) 
                && newMc.Current_Classifier__c == CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_FINISHEDFAILED) {
                marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll.add(newMc.Id);
            }    

            /* Sebastian Łasisz */       
            // check whether Campaign Details is attached to proper lookup
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignsToCheckAttachedDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // Disable activating campaign where communication form is set to fcc and external id was not given
            if ((newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)
                || newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES))
                && newMc.Active__c == true && newMc.Campaign_Communication_Form__c == CommonUtility.MARKETING_COMMUNICATION_FROM_FCC 
                && newMc.External_campaign__c == null) {
                marketingCampaignsToCheckIfCanActivate.add(newMc);
                //newMc.addError(' ' + Label.msg_error_CantActivateCampaignWithNoExternalId + ': <a target="_blank" href="/' + newMc.Id + '">' + newMc.Name + '</a>', false);
            }
            
            /* Sebastian Łasisz */
            // Create contact based on information given in returned lead
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
                && (newMc.Lead_Email__c != null || newMc.Lead_Phone__c != null)) {
                returnedLeadsToCreateContacts.add(newMc);
            }
            
            /* Sebastian Łasisz */
            // Check if contact has filled at least email or phone before adding returned lead
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
                && (newMc.Lead_Email__c == null && newMc.Lead_Phone__c == null)) {
                newMc.addError(Label.msg_error_PhoneOrEmailFilled);
            }

            /* Sebastian Łasisz */
            // on Marketing iPresso Contact insertion change status of synchronisation on main campaign - used to remove campaigns
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)) {
                checkSynchronizedStatusOnTags.add(newMc.Campaign_Member__c);
            } 
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        for (Id key : o.keySet()) {
            Marketing_Campaign__c newMC = (Marketing_Campaign__c)o.get(key);

            /* Sebastian Łasisz*/
            // Add marketing campaign member's name and id to marketing details
            if (newMC.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                && (newMc.Campaign_Details__c != null || newMc.Campaign_from_Campaign_Details_2__c != null)) {
                marketingCampaignToRewriteDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // update Campaign Member Enrollment when Classifier is set to Resignation on Welcome Call Campaign
            if (!CommonUtility.skipEnrOrCampaignUpdate && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER) 
                && newMc.Current_Classifier__c == CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_RESIGNATION) {
                marketingCampaignsToUpdateMembersAfterResignationWelcomeCall.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // set Classifier on Campaign Member when added to Welcome Call
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                && newMc.Campaign_Member__c != null) {
                newMarketingMembersWelcomeCallToSetClassifier.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Change members' status on Welcome Call campaign when member is added to Lack of Documents campaign
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignToChangeStatusOnWelcomeCall.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Change members' classifier when theres phone duplicate in database
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                contactsToCheckPhonesAfterInsertion.add(newMc.Campaign_Contact__c);
            }

            /* Sebastian Łasisz */
            // Change members' classifier when candidate is signed from ZPI
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                contactsToCheckZPIStatus.add(newMc.Campaign_Contact__c);
            }

            /* Sebastian Łasisz */
            // Add Classifiers to Marketing Campaign
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)) {
                marketingCampaignsToAddClassifiers.add(newMc);
            }

            /* Sebastian Łasisz */
            // Add Classifiers to Marketing Sales Campaign
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)) {
                marketingSalesCampaignsToAddClassifiers.add(newMc);
            }

            /* Sebastian Łasisz */
            // Send newly created Campaign Members to FCC
            if (!Test.isRunningTest() && !CommonUtility.preventsSendingCalloutAsAtFuture && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignsToSendMembersToFCC.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Attach syster classifiers to Campaign that is connected to FCC
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)
                && newMc.External_campaign__c != null
                && newMc.Campaign_Communication_Form__c == CommonUtility.MARKETING_COMMUNICATION_FROM_FCC) {
                marketingCampaignsToAttachSystemClassifiers.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Rewrite Marketing Campaign Name to Campaign Details
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignToRewriteNameToDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // create system classifiers for external campaigns
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL)) {
                externalMarketingCampaignsToAddSystemClassifiers.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // pdate main scoring on contact after adding scoring to ipresso contact
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                && newMc.Scoring__c != null) {
                iPressoContactsToUpdateScoringOnContact.add(newMc);
            }

            /* Sebastian Lasisz */
            // On Insertion add campaign details (usable from importing only)
            if (!CommonUtility.preventDetailsCreation
                && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                insertedCampaignMembersWithoutDetails.add(newMc);
            }
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Marketing_Campaign__c> oldMarketingCampaigns = (Map<Id, Marketing_Campaign__c>)old;
        Map<Id, Marketing_Campaign__c> newMarketingCampaigns = (Map<Id, Marketing_Campaign__c>)o;
        for (Id key : oldMarketingCampaigns.keySet()) {
            Marketing_Campaign__c oldMC = oldMarketingCampaigns.get(key);
            Marketing_Campaign__c newMC = newMarketingCampaigns.get(key);

            /* Sebastian Łasisz */
            // check duplicates for campaign member
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignToCheckDuplicates.add(newMc);
            }

            /* Sebastian Łaisz */
            // update Campaign Member from Campaign Details on Campaign Details
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                newMarketingCampaignsToCheckDetailsAfterClearing.add(newMc);
                oldMarketingCampaignsToCheckDetailsAfterClearing.add(oldMc);
            }

            /* Sebastian Łasisz */
            // check duplicates for campaign offer
            if  (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_OFFER)) {
                marketingCampaignToCheckDuplicatesOnOffer.add(newMc);
            }

            /* Sebastian Łasisz */
            // set active details based on filled information
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignsToUpdateActiveDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // update Campaign Member Enrollment when Classifier is set to Resignation on Confirmation of Enrollment Campaign
            if (!CommonUtility.skipEnrOrCampaignUpdate && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER) 
                && newMc.Classifier_from_Campaign_Member__c != oldMc.Classifier_from_Campaign_Member__c 
                && newMc.Current_Classifier__c == CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_FINISHEDFAILED) {
                marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll.add(newMc.Id);
            }    

            /* Sebastian Łasisz */       
            // check whether Campaign Details is attached to proper lookup
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaignsToCheckAttachedDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // Disable editing campaign member when classifier is set to success or resignation
            if (!CommonUtility.skipVerificationOfClassifier && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                && oldMC.Classifier_from_Campaign_Member__c != newMc.Classifier_from_Campaign_Member__c 
                && oldMC.Classifier_Closing_from_Classifier__c) {
                newMc.addError(Label.msg_error_cantEditCampaign);
            }

            /* Sebastian Łasisz */
            // Disable activating campaign where communication form is set to fcc and external id was not given
            if ((newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)
                || newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES))
                && newMc.Active__c == true && newMc.Campaign_Communication_Form__c == CommonUtility.MARKETING_COMMUNICATION_FROM_FCC 
                && newMc.External_campaign__c == null) {
                marketingCampaignsToCheckIfCanActivate.add(newMc);
                //newMc.addError(' ' + Label.msg_error_CantActivateCampaignWithNoExternalId + ': <a target="_blank" href="/' + newMc.Id + '">' + newMc.Name + '</a>', false);
            }
            
            /* Sebastian Łasisz */
            // Check if contact has filled at least email or phone before adding returned lead
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
                && (newMc.Lead_Email__c == null && newMc.Lead_Phone__c == null)) {
                newMc.addError(Label.msg_error_PhoneOrEmailFilled);
            }

            /* Sebastian Łasisz */
            // Send updated contact to FCC
            if (!CommonUtility.skipContactUpdateInOtherSystems && !Test.isRunningTest() 
                && !CommonUtility.preventSendingContactUpdateCalloutsAsAtFuture 
                && oldMc.Classifier_from_Campaign_Member__c != newMc.Classifier_from_Campaign_Member__c) {
                contactIdsToUpdateOtherSystems.put(newMc.Campaign_Contact__c + ';' + contactIdsToUpdateOtherSystems.size(), RecordVals.UPDATING_SYSTEM_FCC);
            }

            /* Sebastian Łasisz */
            // on Marketing iPresso Contact update change status of synchronisation on main campaign - used to remove campaigns
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
                && newMc.Synchronization_Status__c != oldMc.Synchronization_Status__c
                && oldMc.Synchronization_Status__c == CommonUtility.SYNCSTATUS_FINISHED) {
                checkSynchronizedStatusOnTags.add(newMc.Campaign_Member__c);
            } 
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Marketing_Campaign__c> oldMarketingCampaigns = (Map<Id, Marketing_Campaign__c>)old;
        Map<Id, Marketing_Campaign__c> newMarketingCampaigns = (Map<Id, Marketing_Campaign__c>)o;
        for (Id key : oldMarketingCampaigns.keySet()) {
            Marketing_Campaign__c oldMC = oldMarketingCampaigns.get(key);
            Marketing_Campaign__c newMC = newMarketingCampaigns.get(key);

            /* Sebastian Łasisz*/
            // Add marketing campaign member's name and id to marketing details
            if (newMC.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                && (newMc.Campaign_Details__c != null || newMc.Campaign_from_Campaign_Details_2__c != null)) {
                marketingCampaignToRewriteDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // update Campaign Member Enrollment when Classifier is set to Resignation on Welcome Call Campaign
            if (!CommonUtility.skipEnrOrCampaignUpdate && newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER) 
                && newMc.Classifier_from_Campaign_Member__c != oldMc.Classifier_from_Campaign_Member__c 
                && newMc.Current_Classifier__c == CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_RESIGNATION) {
                marketingCampaignsToUpdateMembersAfterResignationWelcomeCall.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Attach syster classifiers to Campaign that is connected to FCC
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN)
                && newMc.External_Campaign__c != oldMc.External_Campaign__c
                && newMc.External_Campaign__c != null
                && newMc.Campaign_Communication_Form__c != oldMc.Campaign_Communication_Form__c
                && newMc.Campaign_Communication_Form__c == CommonUtility.MARKETING_COMMUNICATION_FROM_FCC) {
                marketingCampaignsToAttachSystemClassifiers.add(newMc.Id);
            }

            /* Sebastian Łasisz */
            // Rewrite Marketing Campaign Name to Campaign Details
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                && newMc.Campaign_Member__c != oldMc.Campaign_Member__c) {
                marketingCampaignToRewriteNameToDetails.add(newMc);
            }

            /* Sebastian Łasisz */
            // pdate main scoring on contact after adding scoring to ipresso contact
            if (newMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                && newMc.Scoring__c != oldMc.Scoring__c) {
                iPressoContactsToUpdateScoringOnContact.add(newMc);
            }

            /* Sebastian Łasisz */
            // Send updated contact to iPresso
            if (!CommonUtility.skipContactUpdateInOtherSystems && !Test.isRunningTest() 
                && !CommonUtility.preventSendingContactUpdateCalloutsAsAtFuture 
                && oldMc.Contact_from_iPresso__c != newMc.Contact_from_iPresso__c) {
                contactIdsToUpdateOtherSystems.put(newMc.Contact_from_iPresso__c + ';' + contactIdsToUpdateOtherSystems.size(), RecordVals.UPDATING_SYSTEM_MA);
            }
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Marketing_Campaign__c> oldMcs = (Map<Id, Marketing_Campaign__c>)old;
        Marketing_Campaign__c oldMc;
        for (Id key : oldMcs.keySet()) {
            oldMc = oldMcs.get(key);

            /* Sebastian Łasisz */
            // clear Campaign Member from Campaign Details on Campaign Details
            if (oldMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)) {
                marketingCampaingsToCheckDetailsRelation.add(oldMc);
            }

            /* Sebastian Łasisz */
            // disable deleting synchronised iPresso contacts
            if (oldMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_MEMBER)
                && oldMc.Synchronization_Status__c == CommonUtility.SYNCSTATUS_FINISHED) {
                //oldMc.addError(Label.msg_error_cantDeleteIPressoContact);
            }

            /* Sebastian Łasisz */
            // Remove iPresso Contacts from IPresso
            if (oldMc.RecordTypeId == CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                && !Test.isRunningTest()) {
                iPressoContactsToRemoveFromIPresso.add(oldMc.iPressoId__c);
            }
        }
    }

    public override void afterDelete(Map<Id, sObject> old) { }

    public override void finish() {

        /* Sebastian Łasisz */
        // KEEP AT TOP - Add Classifiers to Marketing Campaign
        if (marketingCampaignsToAddClassifiers != null && !marketingCampaignsToAddClassifiers.isEmpty()) {
            ClassifierManager.createClassifiersForMarketingCampaign(marketingCampaignsToAddClassifiers);
        }

        /* Sebastian Łasisz */
        // KEEP AT TOP - Add Classifiers to Marketing Sales Campaign
        if (marketingSalesCampaignsToAddClassifiers != null && !marketingSalesCampaignsToAddClassifiers.isEmpty()) {
            ClassifierManager.createClassifiersForMarketingSalesCampaign(marketingSalesCampaignsToAddClassifiers);
        }

        /* Sebastian Lasisz */
        // KEEP AT TOP - add campaign details to newly created members
        if (insertedCampaignMembersWithoutDetails != null && !insertedCampaignMembersWithoutDetails.isEmpty()) {
            MarketingCampaignSalesManager.createCampaignDetailsForMembers(insertedCampaignMembersWithoutDetails); 
        }

        /* Sebastian Łasisz */
        // Attach syster classifiers to Campaign that is connected to FCC
        if (marketingCampaignsToAttachSystemClassifiers != null && !marketingCampaignsToAttachSystemClassifiers.isEmpty()) {
            ClassifierManager.createSystemClassifiers(marketingCampaignsToAttachSystemClassifiers);
        }

        /* Sebastian Łasisz */
        // Attach system classifiers to newlu created external campaign
        if (externalMarketingCampaignsToAddSystemClassifiers != null && !externalMarketingCampaignsToAddSystemClassifiers.isEmpty()) {
            Database.executebatch(new CreateExternalClassifiersBatch(externalMarketingCampaignsToAddSystemClassifiers), 100);
        }

        /* Sebastian Łasisz */
        // update main scoring on contact after adding scoring to ipresso contact
        if (iPressoContactsToUpdateScoringOnContact != null && !iPressoContactsToUpdateScoringOnContact.isEmpty()) {
            IPressoManager.updateMainScoringOnContact(iPressoContactsToUpdateScoringOnContact);
        }

        /* Sebastian Łasisz */
        // Disable activating campaign where communication form is set to fcc and external id was not given
        if (marketingCampaignsToCheckIfCanActivate != null && !marketingCampaignsToCheckIfCanActivate.isEmpty()) {
            MarketingCampaignManager.disableActivatingCampaign(marketingCampaignsToCheckIfCanActivate);
        }

        /* Sebastian Łasisz*/
        // Add marketing campaign member's name and id to marketing details
        if (marketingCampaignToRewriteDetails != null && !marketingCampaignToRewriteDetails.isEmpty()) {
            MarketingCampaignManager.addValuesToCampaignDetailsFromCampaign(marketingCampaignToRewriteDetails);
        }

        /* Sebastian Łasisz */
        // update contacts in other system on update
        if (contactIdsToUpdateOtherSystems != null && !contactIdsToUpdateOtherSystems.isEmpty()) {
            IntegrationContactUpdateOther.sendUpdatedContactToOtherSystems(contactIdsToUpdateOtherSystems);
        }

        /* Sebastian Łasisz */
        // Send newly created Campaign Members to FCC
        if (marketingCampaignsToSendMembersToFCC != null && !marketingCampaignsToSendMembersToFCC.isEmpty()) {
            IntegrationFCCContactSender.sendNewFCCRecordsAsync(marketingCampaignsToSendMembersToFCC);
        }

        /* Sebastian Łasisz */
        // check duplicates for campaign member
        if (marketingCampaignToCheckDuplicates != null && !marketingCampaignToCheckDuplicates.isEmpty()) {
            MarketingCampaignManager.checkDuplicatesForCampaignMember(marketingCampaignToCheckDuplicates);
        }

        /* Sebastian Łasisz */
        // check duplicates for campaign offer
        if (marketingCampaignToCheckDuplicatesOnOffer != null && !marketingCampaignToCheckDuplicatesOnOffer.isEmpty()) {
            MarketingCampaignManager.checkDuplicatesForCampaignOffer(marketingCampaignToCheckDuplicatesOnOffer);
        }

        /* Sebastian Łasisz */
        // Remove iPresso Contacts from IPresso
        if (iPressoContactsToRemoveFromIPresso != null && !iPressoContactsToRemoveFromIPresso.isEmpty()) {
            if (System.IsBatch() == false) {
            	IntegrationIPressoRemoveContacts.removeIPressoContactListAtFuture(iPressoContactsToRemoveFromIPresso);
        	}
        }

        /* Sebastian Łaisz */
        // update Campaign Member from Campaign Details on Campaign Details
        if (newMarketingCampaignsToCheckDetailsAfterClearing != null && !newMarketingCampaignsToCheckDetailsAfterClearing.isEmpty()
            && oldMarketingCampaignsToCheckDetailsAfterClearing != null && !oldMarketingCampaignsToCheckDetailsAfterClearing.isEmpty()) {
            MarketingCampaignManager.updateMarketingMembersOnDetails(newMarketingCampaignsToCheckDetailsAfterClearing, oldMarketingCampaignsToCheckDetailsAfterClearing);
        }

        /* Sebastian Łasisz */
        // clear Campaign Member from Campaign Details on Campaign Details
        if (marketingCampaingsToCheckDetailsRelation != null && !marketingCampaingsToCheckDetailsRelation.isEmpty()) {
            MarketingCampaignManager.clearMarketingMembersOnDetails(marketingCampaingsToCheckDetailsRelation);
        }

        /* Sebastian Łasisz */
        // set active details based on filled information
        if (marketingCampaignsToUpdateActiveDetails != null && !marketingCampaignsToUpdateActiveDetails.isEmpty()) {
            MarketingCampaignManager.updateActiveCampaingDetailsOnMember(marketingCampaignsToUpdateActiveDetails);
        }

        /* Sebastian Łasisz */
        // on Marketing iPresso Contact insertion/update change status of synchronisation on main campaign - used to remove campaigns
        if (checkSynchronizedStatusOnTags != null && !checkSynchronizedStatusOnTags.isEmpty()) {
            MarketingCampaignManager.changeSynchronizedStatusOnTags(checkSynchronizedStatusOnTags);
        } 

        /* Sebastian Łasisz */
        // Change members' status on Welcome Call campaign when member is added to Lack of Documents campaign
        if (marketingCampaignToChangeStatusOnWelcomeCall != null && !marketingCampaignToChangeStatusOnWelcomeCall.isEmpty()) {
            MarketingCampaignManager.updateWelcomeCallWhenInsertedToLackOfDoc(marketingCampaignToChangeStatusOnWelcomeCall);
        }

        /* Sebastian Łasisz */
        // set Classifier on Campaign Member when added to Welcome Call
        if (newMarketingMembersWelcomeCallToSetClassifier != null && !newMarketingMembersWelcomeCallToSetClassifier.isEmpty()) {
            MarketingCampaignManager.updateCampaignMembersWelcomeCall(newMarketingMembersWelcomeCallToSetClassifier);
        }

        /* Sebastian Łasisz */
        // update Campaign Member Enrollment when Classifier is set to Resignation
        if (marketingCampaignsToUpdateMembersAfterResignationWelcomeCall != null && !marketingCampaignsToUpdateMembersAfterResignationWelcomeCall.isEmpty()) {
            MarketingCampaignManager.updateCampaignMemberOnResignationWelcomeCall(marketingCampaignsToUpdateMembersAfterResignationWelcomeCall);
        }

        /* Sebastian Łasisz */
        // update Campaign Member Enrollment when Classifier is set to Resignation on Confirmation of Enrollment Campaign
        if (marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll != null && !marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll.isEmpty()) {
            MarketingCampaignManager.updateCampaignMemberOnResignationEnrollmentConfirmation(marketingCampaignsToUpdateMembersAfterResignationConfirmationOfEnroll);
        }    

        /* Sebastian Łasisz */       
        // check whether Campaign Details is attached to proper lookup
        if (marketingCampaignsToCheckAttachedDetails != null && !marketingCampaignsToCheckAttachedDetails.isEmpty()) {
            MarketingCampaignManager.checkWhetherDetailsIsProperlyAttached(marketingCampaignsToCheckAttachedDetails);
        }

        /* Sebastian Łasisz */
        // Rewrite Marketing Campaign Name to Campaign Details
        if (marketingCampaignToRewriteNameToDetails != null && !marketingCampaignToRewriteNameToDetails.isEmpty() ) {
            MarketingCampaignManager.rewriteCampaignNameToDetails(marketingCampaignToRewriteNameToDetails);
        }
            
        /* Sebastian Łasisz */
        // Create contact based on information given in inserted Returned Lead
        if (returnedLeadsToCreateContacts != null && !returnedLeadsToCreateContacts.isEmpty()) {
            ContactManager.createContactsBasedOnReturnedLead(returnedLeadsToCreateContacts);
        }

        /* Sebastian Łasisz */
        // Change members' classifier when theres phone duplicate in database
        if (contactsToCheckPhonesAfterInsertion != null && !contactsToCheckPhonesAfterInsertion.isEmpty()) {
            MarketingCampaignSalesManager.changeClassifierOnDuplicatePhone(contactsToCheckPhonesAfterInsertion);
        }

        /* Sebastian Łasisz */
        // Change members' classifier when candidate is signed from ZPI
        if (contactsToCheckZPIStatus != null && !contactsToCheckZPIStatus.isEmpty()) {
            MarketingCampaignSalesManager.changeClassifierOnZPIEnrollment(contactsToCheckZPIStatus);
        }
    }
}