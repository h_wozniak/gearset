/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Extranet Informations
**/

global class WorkshopReminderSchoolBatch implements Database.Batchable<sObject> {
    Datetime dayOfEventFrom;
    Datetime dayOfEventTo;

    global WorkshopReminderSchoolBatch(Integer daysBeforeEventStarts) {
        Date dayOfEvent = System.today().addDays(daysBeforeEventStarts);
        dayOfEventFrom = Datetime.newInstanceGMT(dayOfEvent.year(), dayOfEvent.month(), dayOfEvent.day(), 0, 0, 0);
        dayOfEventTo = Datetime.newInstanceGMT(dayOfEvent.year(), dayOfEvent.month(), dayOfEvent.day(), 23, 59, 59);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, University__c, WhatId
                FROM Event
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)
                AND StartDateTime >= :dayOfEventFrom AND StartDateTime <= :dayOfEventTo
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Event> eventList) {
        Set<Id> eventIds = new Set<Id>();
        for (Event event : eventList) {
            eventIds.add(event.Id);
        }

        EmailManager_WorkshopReminderSchool.sendWorkshopProposal(eventIds);
    }

    global void finish(Database.BatchableContext BC) {
    }

}