public with sharing class LEX_Change_Price_BookController {
	
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String wasIsUnenrolled(String recordId) {
		Enrollment__c enr = [SELECT Id, WasIs_Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];
		return enr.WasIs_Unenrolled__c ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String forbiddenStatus(String recordId) {
		Set<String> forbiddenStatuses = new Set<String> {
			'Unconfirmed',
			'Signed contract',
			'Didactics in KS',
			'Payments in KS',
			'Resignation'
		};

		Enrollment__c enr = [SELECT Id, Status__c FROM Enrollment__c WHERE Id = :recordId];
		return (forbiddenStatuses.contains(enr.Status__c) ? 'TRUE' : 'FALSE');
	}
}