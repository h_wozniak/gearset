@isTest
private class ZTestDocGen_GenerateAgreementCity {
    private static Map<Integer, sObject> prepareDataForTest(String entity) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        Account univ = ZDataTestUtility.createUniversity(new Account(Name = entity, BillingCity = 'Poznań'), true);
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(University__c = univ.Id, Degree__c = CommonUtility.OFFER_DEGREE_PG, Study_Start_Date__c = Date.newInstance(2017, 4, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        Test.stopTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

    private static Map<Integer, sObject> prepareDataForTest_specialty(String entity) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        Account univ = ZDataTestUtility.createUniversity(new Account(Name = entity, BillingCity = 'Poznań'), true);
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(University__c = univ.Id, Degree__c = CommonUtility.OFFER_DEGREE_PG, Study_Start_Date__c = Date.newInstance(2017, 4, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = retMap.get(1).Id), true));
        Test.startTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id), true));
        Test.stopTest();
        retMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(3).Id), true));
        retMap.put(5, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

	@isTest static void test_generateOathCity_Wroclaw() {
		Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.MARKETING_ENTITY_WROCLAW);

		String result = DocGen_GenerateAgreementCity.executeMethod(dataMap.get(3).Id, null, 'generateOathCity');

		System.assertEquals(result, 'we Wrocławiu');
	}

	@isTest static void test_generateOathCity_Other() {
		Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.MARKETING_ENTITY_POZNAN);

		String result = DocGen_GenerateAgreementCity.executeMethod(dataMap.get(3).Id, null, 'generateOathCity');

		System.assertEquals(result, 'w Poznaniu');
	}

	@isTest static void test_generateAgreementCity() {
		Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.MARKETING_ENTITY_POZNAN);

		String result = DocGen_GenerateAgreementCity.executeMethod(dataMap.get(3).Id, null, 'generateAgreementCity');

		System.assertEquals(result, 'Poznań');		
	}

	@isTest static void test_generateAgreementCity_specialty() {
		Map<Integer, sObject> dataMap = prepareDataForTest_specialty(CommonUtility.MARKETING_ENTITY_POZNAN);

		String result = DocGen_GenerateAgreementCity.executeMethod(dataMap.get(4).Id, null, 'generateAgreementCity');

		System.assertEquals(result, 'Poznań');		
	}
}