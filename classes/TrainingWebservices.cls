/**
*   @author         Wojciech Słodziak
*   @description    Class for storing metods called as webservices (mostly JS Buttons). Only for Trainings
**/

global with sharing class TrainingWebservices {

    /* Wojciech Słodziak */
    // webservice to mark enrollments as Paid (on related List)
    webservice static String markEnrollmentsAsPaid(String objId, String sObjectType) {
        if (sObjectType == 'Offer__c') {
            List<Enrollment__c> relatedMainEnrollments = [
                SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c 
                WHERE Training_Offer__c = :objId 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
            ];

            if (!relatedMainEnrollments.isEmpty()) {
                for (Enrollment__c rme : relatedMainEnrollments) {
                    if (rme.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                        rme.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                    }
                    else {
                        rme.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                    }
                }
                try {
                    update relatedMainEnrollments;
                    return 'true';
                } catch(Exception e) {
                    ErrorLogger.log(e);
                    return String.valueOf(e);
                }
            }
            else {
                return 'no_main_enrollment';
            }
        }
        else {
            Enrollment__c mainEnrollment = [
                SELECT Id, Status__c, Last_Active_status__c
                FROM Enrollment__c 
                WHERE Id = :objId LIMIT 1
            ];

            if (mainEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                mainEnrollment.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
            }
            else {
                mainEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
            }
                
            try {
                update mainEnrollment;
                return 'true';
            } catch(Exception e) {
                ErrorLogger.log(e);
                return String.valueOf(e);
            }            
        }
    }


    /* Wojciech Słodziak */
    // webservice to mark participant as accepted from reserve list
    webservice static Boolean markParticipantsAsAccepted(String[] idArray) {
        List<Enrollment__c> relatedParticipants = [SELECT Id, Status__c FROM Enrollment__c WHERE Id IN :idArray];
        List<Enrollment__c> participantsToUpdate = new List<Enrollment__c>();
        for (Enrollment__c participant : relatedParticipants) {
            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST || participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS) {
                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
                participantsToUpdate.add(participant);
            }  
        }
        try {
            if (!participantsToUpdate.isEmpty()) {update participantsToUpdate;}
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }


    /* Wojciech Słodziak */
    /* Update All Training Participants' statuses to PAID. Called from Participants' related list */
    webservice static Boolean markParticipantsAsPaid(String[] idArray) {
        List<Id> mainEnrollmentIds = new List<Id>();
        List<Id> trainingSchedulesIds = new List<Id>();

        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];

        List<Enrollment__c> relatedParticipantsToUpdate = new List<Enrollment__c>();

        for (Enrollment__c participant : relatedParticipants) {
            mainEnrollmentIds.add(participant.Enrollment_Training_Participant__c);
            trainingSchedulesIds.add(participant.Training_Offer_for_Participant__c);
        }

        List<Enrollment__c> mainEnrollmentList = [
            SELECT Id, Status__c, RecordTypeId 
            FROM Enrollment__c 
            WHERE Id IN :mainEnrollmentIds
        ];

        List<Offer__c> trainingSchedulesList = [
            SELECT Id, Launched__c 
            FROM Offer__c 
            WHERE Id IN : trainingSchedulesIds
        ];

        for (Enrollment__c participant : relatedParticipants) {
            for (Offer__c trainingSchedule : trainingSchedulesList) {
                if (participant.Training_Offer_for_Participant__c == trainingSchedule.Id) {
                    for (Enrollment__c mainEnrollment : mainEnrollmentList) {
                        if (participant.Enrollment_Training_Participant__c == mainEnrollment.Id) {
                            if (participant.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                                if (mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {

                                    if (participant.Status__c != CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST) {
                                        if (trainingSchedule.Launched__c) {
                                            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                                participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                            }
                                            else{
                                                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED;
                                            }
                                        } else {
                                            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                                participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                            }
                                            else {
                                                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP;     
                                            }   
                                        }
                                    }

                                } else if (mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)) {
                                    if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                        participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                    }
                                    else {
                                        participant.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                    }
                                }
                                
                                relatedParticipantsToUpdate.add(participant);
                            }
                        }
                    }
                }
            }
        }

        try {
            if (!relatedParticipantsToUpdate.isEmpty()) {
                update relatedParticipantsToUpdate;
            }
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }


    /* Sebastian Łasisz */
    // webservice to check if offer has any specializations
    webservice static Boolean hasSpecializations(String offerId) {
        Offer__c selectedOffer = [SELECT Id, Degree__c, Course_Offer_from_Specialty__c FROM Offer__c WHERE Id = :offerId];
        
        if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            List<Offer__c> specializationsOnMainOffer = [SELECT Id, Name FROM Offer__c WHERE Offer_from_Specialization__c =: selectedOffer.Course_Offer_from_Specialty__c];
            List<Offer__c> specializationOnSpecialtyOffer = [SELECT Id, Name FROM Offer__c WHERE Offer_from_Specialization__c = :selectedOffer.Id];
            specializationsOnMainOffer.addAll(specializationOnSpecialtyOffer);
            
            if (specializationsOnMainOffer.size() > 0) {
                return true;
            }

            return false;
        }

        return true;
    }


}