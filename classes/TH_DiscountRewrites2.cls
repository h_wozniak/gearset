/**
* @description  This class is a Trigger Handler for Study Discounts - rewriting field values (for emails/preview)
**/

public without sharing class TH_DiscountRewrites2 extends TriggerHandler.DelegateBase {
    
    List<Discount__c> offerCompanyDCToCopyFields;
    List<Discount__c> offerTimeDCToCopyFields;
    List<Discount__c> discountsToCopyFields;
    List<Discount__c> timeCodeDiscountsToCopyFields;
    List<Discount__c> companyDiscountsToCopyFields;

    public override void prepareBefore() {
        offerCompanyDCToCopyFields = new List<Discount__c>();
        offerTimeDCToCopyFields = new List<Discount__c>();
        discountsToCopyFields = new List<Discount__c>();
        timeCodeDiscountsToCopyFields = new List<Discount__c>();
        companyDiscountsToCopyFields = new List<Discount__c>();
    }

    public override void prepareAfter() {}



    public override void beforeInsert(List<sObject> o ) {
        List<Discount__c> newDiscountList = (List<Discount__c>)o;
        for(Discount__c newDiscount : newDiscountList) {

            /* Wojciech Słodziak */
            // on Offer Company Discount Connector insert copy fields
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_COMPANY_DC)) {
                offerCompanyDCToCopyFields.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // on Offer Time Discount Connector insert copy fields
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)) {
                offerTimeDCToCopyFields.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // update fields on Manual Discounts record when new Discount is created
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) 
                || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)) {
                discountsToCopyFields.add(newDiscount);
            }
            
            /* Sebastian Łasisz */
            // update fields on Time Discounts and Code Discounts record when new Discount is created
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_TIME_DISCOUNT)
                || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_CODE_DISCOUNT)) {
                timeCodeDiscountsToCopyFields.add(newDiscount);
            }
            
            /* Sebastian Łasisz */
            // update fields on Company Discounts record when new Discount is created
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT)) {
                companyDiscountsToCopyFields.add(newDiscount);
            }
        }
    }

    //public override void afterInsert(Map<Id, sObject> o) {}

    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}
 
    //public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    //public override void beforeDelete(Map<Id, sObject> old) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {

        /* Wojciech Słodziak */
        // on Offer Company Discount Connector insert copy fields
        if (offerCompanyDCToCopyFields != null && !offerCompanyDCToCopyFields.isEmpty()) {
            Set<Id> discountIds = new Set<Id>();
            for (Discount__c offerDC : offerCompanyDCToCopyFields) {
                discountIds.add(offerDC.Discount_from_Offer_DC__c);
            }

            Map<Id, Discount__c> discountMap = new Map<Id, Discount__c>([
                SELECT Id, Discount_Type_Studies__c 
                FROM Discount__c 
                WHERE Id IN :discountIds
            ]);
            
            for (Discount__c offerDC : offerCompanyDCToCopyFields) {
                Discount__c discount = discountMap.get(offerDC.Discount_from_Offer_DC__c);

                if (discount != null) {
                    offerDC.Discount_Type_Studies__c = discount.Discount_Type_Studies__c;
                }
            }
        }

        /* Wojciech Słodziak */
        // on Offer Time Discount Connector insert copy fields
        if (offerTimeDCToCopyFields != null && !offerTimeDCToCopyFields.isEmpty()) {
            Set<Id> discountIds = new Set<Id>();
            for (Discount__c offerDC : offerTimeDCToCopyFields) {
                discountIds.add(offerDC.Discount_from_Offer_DC__c);
            }

            Map<Id, Discount__c> discountMap = new Map<Id, Discount__c>([
                SELECT Id, Discount_Type_Studies__c, Discount_Kind__c, Discount_Value__c 
                FROM Discount__c 
                WHERE Id IN :discountIds
            ]);
            
            for (Discount__c offerDC : offerTimeDCToCopyFields) {
                Discount__c discount = discountMap.get(offerDC.Discount_from_Offer_DC__c);

                if (discount != null) {
                    offerDC.Discount_Type_Studies__c = discount.Discount_Type_Studies__c;
                    offerDC.Discount_Kind__c = discount.Discount_Kind__c;
                    offerDC.Discount_Value__c = discount.Discount_Value__c;
                }
            }
        }

        /* Sebastian Łasisz */
        // update fields on Manual Discounts record when new Discount is created
        if (discountsToCopyFields != null && !discountsToCopyFields.isEmpty()) {
            Set<Id> studyEnrollmentsIds = new Set<Id>();

            for (Discount__c discountToUpdateFields : discountsToCopyFields) {
                studyEnrollmentsIds.add(discountToUpdateFields.Enrollment__c);
            }

            Map<Id, Enrollment__c> studyEnrollments = new Map<Id, Enrollment__c>([
                SELECT Id, University_Name__c, OwnerId
                FROM Enrollment__c 
                WHERE Id IN :studyEnrollmentsIds
            ]);

            for (Discount__c discountToUpdateFields : discountsToCopyFields) {
                discountToUpdateFields.University_for_sharing__c = studyEnrollments.get(discountToUpdateFields.Enrollment__c).University_Name__c;
                discountToUpdateFields.OwnerId = studyEnrollments.get(discountToUpdateFields.Enrollment__c).OwnerId;
            }
        }
            
        /* Sebastian Łasisz */
        // update fields on Time Discounts and Code Discounts record when new Discount is created
        if (timeCodeDiscountsToCopyFields != null && !timeCodeDiscountsToCopyFields.isEmpty()) {
            User u = [SELECT Id, WSB_Department__c, UserRole.Name, UserRole.DeveloperName FROM User WHERE Id = :UserInfo.getUserId()];

            if (u.UserRole.DeveloperName != null && u.WSB_Department__c != null && 
                u.WSB_Department__c != CommonUtility.USER_WSB_INT && u.WSB_Department__c != CommonUtility.USER_WSB_ADM) {
                for (Discount__c timeDiscountToCopyFields : timeCodeDiscountsToCopyFields) {
                    if (u.UserRole.DeveloperName == CommonUtility.WSB_ROLE_AKADEMIA) {
                        timeDiscountToCopyFields.University_for_sharing__c = RecordVals.WSB_NAME_WRO + ', ' + RecordVals.WSB_NAME_OPO + ', ' + 
                            RecordVals.WSB_NAME_GDA + ', ' + RecordVals.WSB_NAME_GDY + ', ' + RecordVals.WSB_NAME_POZ + ', ' + 
                            RecordVals.WSB_NAME_SZC + ', ' + RecordVals.WSB_NAME_CHO + ', ' + RecordVals.WSB_NAME_BYD + ', ' + 
                            RecordVals.WSB_NAME_TOR;
                    }
                    else if (u.UserRole.DeveloperName == CommonUtility.WSB_ROLE_WRO) {
                        timeDiscountToCopyFields.University_for_sharing__c = RecordVals.WSB_NAME_WRO + ', ' + RecordVals.WSB_NAME_OPO;
                    }
                    else if (u.UserRole.DeveloperName == CommonUtility.WSB_ROLE_GDA) {
                        timeDiscountToCopyFields.University_for_sharing__c = RecordVals.WSB_NAME_GDA + ', ' + RecordVals.WSB_NAME_GDY;
                    }
                    else if (u.UserRole.DeveloperName == CommonUtility.WSB_ROLE_POZ) {
                        timeDiscountToCopyFields.University_for_sharing__c = RecordVals.WSB_NAME_POZ + ', ' + RecordVals.WSB_NAME_SZC + ', ' + 
                            RecordVals.WSB_NAME_CHO;
                    }
                    else if (u.UserRole.DeveloperName == CommonUtility.WSB_ROLE_TOR) {
                        timeDiscountToCopyFields.University_for_sharing__c = RecordVals.WSB_NAME_BYD + ', ' + RecordVals.WSB_NAME_TOR;
                    }
                    else {
                        timeDiscountToCopyFields.University_for_sharing__c = u.WSB_Department__c;
                    }
                }
            }
        }

    }

}