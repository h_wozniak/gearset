/**
*   @author         Grzegorz Długosz, Sebastian Łasisz
*   @description    This class is a helper class for IntegrationFCCInteractionReceiver
**/

global class IntegrationFCCInteractionHelper {
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- INVOKE METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String buildInteractionImportJson() {

        List<Marketing_Campaign__c> listOfFCCCampaigns = [
            SELECT External_Id__c 
            FROM Marketing_Campaign__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL) 
            AND Campaign_Communication_Form__c = :CommonUtility.MARKETING_COMMUNICATION_FROM_FCC
        ];

        Set<Integer> setOfExtCampaignIds = new Set<Integer>();

        for (Marketing_Campaign__c campaign : listOfFCCCampaigns) {
            setOfExtCampaignIds.add(Integer.valueOf(campaign.External_Id__c));
        }

        FCC_Last_Interaction_Retrival_Date__c fccLastInteraction = FCC_Last_Interaction_Retrival_Date__c.getOrgDefaults();
        String dateOfLastInteraction = fccLastInteraction.Date_From__c.format('yyyy-MM-dd HH:mm');
        String currentTime = DateTime.now().format('yyyy-MM-dd HH:mm');
        IntegrationFCCInteractionHelper.InteractionSender_JSON interactionToSend = new IntegrationFCCInteractionHelper.InteractionSender_JSON(setOfExtCampaignIds, dateOfLastInteraction, currentTime);

        return JSON.serialize(interactionToSend);
    }

    public static String sendInteractionRequestFinal() {
        HttpRequest httpReq = new HttpRequest();
        
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createImportCallsEndPoint = esb.Create_Import_Calls_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createImportCallsEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationFCCInteractionHelper.buildInteractionImportJson();
        httpReq.setBody(jsonToSend);
        
        Boolean success = false;
        
        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        success = (httpRes.getStatusCode() == 200);

        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationFCCInteractionHelper.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n');
        }
        else {
            return httpRes.getBody();
        }

        return null;
    }

    public static Task createTask(Id contactId, String classifierName, IntegrationFCCInteractionHelper.InteractionReceiver_JSON newTaskJSON) {
        Task newTask = new Task();
        newTask.Subject = 'Call';
        newTask.WhoId = contactId;
        newTask.Status = newTaskJSON.call_status;
        newTask.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CALL);
        newTask.Agent_Id__c = newTaskJSON.agent_id;
        newTask.Agent_Name__c = newTaskJSON.agent_name;
        newTask.Call_Type__c = newTaskJSON.call_type;
        newTask.ActivityDate = CommonUtility.stringToDate(newTaskJSON.interaction_date);
        newTask.Phone_Number__c = newTaskJSON.phone_number;
        newTask.Classifier_Id__c = newTaskJSON.classifiers_id;
        newTask.Classifier_Name__c = classifierName;

        if (newTaskJSON.call_status == 'ANSWER' || newTaskJSON.call_status == 'ANSWERED') {
            newTask.Call_Answered__c = true;
        }
        else {
            newTask.Call_Answered__c = false;
        }

        return newTask;      
    }

    public static List<Task> prepareTasksToInsert(Set<String> setOfExtContId, List<String> classifiersIds, List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> scope) {
        List<Marketing_Campaign__c> listOfMarketingCampaigns = [
            SELECT External_Id__c, Campaign_Contact__r.Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND External_Id__c IN :setOfExtContId
        ];

        List<Marketing_Campaign__c> listOfClassifierNames = [
            SELECT Classifier_Name__c, Classifier_Id__c 
            FROM Marketing_Campaign__c 
            WHERE Classifier_Id__c IN :classifiersIds
        ];

        Map<String, String> mapOfContactIds = new Map<String, String>();
        Map<String, String> mapOfClassifierIdToName = new Map<String, String>();

        for (Marketing_Campaign__c currentCamp : listOfMarketingCampaigns) {
            mapOfContactIds.put(currentCamp.External_Id__c, currentCamp.Campaign_Contact__r.Id);
        }

        for (Marketing_Campaign__c currClassifier : listOfClassifierNames) {
            mapOfClassifierIdToName.put(currClassifier.Classifier_Id__c, currClassifier.Classifier_Name__c);
        }
        List<Task> tasksToInsert = new List<Task>();

        for (IntegrationFCCInteractionHelper.InteractionReceiver_JSON newTaskJSON : scope) {
            Id contactId = mapOfContactIds.get(newTaskJSON.external_contact_id);
            String classifierName = mapOfClassifierIdToName.get(newTaskJSON.classifiers_id);

            if (contactId != null && classifierName != null) {
                tasksToInsert.add(IntegrationFCCInteractionHelper.createTask(contactId, classifierName, newTaskJSON));
            }
        }

        return tasksToInsert;
    }

    public static List<Marketing_Campaign__c> prepareCampaignMembersToUpdate(Set<String> setOfExtContId, List<String> classifiersIds, List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> scope) {
        List<Marketing_Campaign__c> campaignMembers = [
            SELECT Id, External_Id__c, Campaign_Member__c, Classifier_from_Campaign_Member__c, Classifier_from_Campaign_Member__r.Classifier_Closing__c
            FROM Marketing_Campaign__c
            WHERE External_Id__c IN :setOfExtContId
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
        ];

        List<Marketing_Campaign__c> classifiers = [
            SELECT Id, Classifier_from_External_Classifier__c, Classifier_from_External_Classifier__r.Classifier_ID__c, Campaign_from_Classifier__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Classifier_from_External_Classifier__r.Classifier_ID__c IN :classifiersIds
        ];

        for (Marketing_Campaign__c campaignMember : campaignMembers) {
            DateTime lastInteractionDate;
            for (IntegrationFCCInteractionHelper.InteractionReceiver_JSON newTaskJSON : scope) {
                for (Marketing_Campaign__c classifier : classifiers) {
                    if (campaignMember.Campaign_Member__c == classifier.Campaign_from_Classifier__c
                        && classifier.Classifier_from_External_Classifier__r.Classifier_ID__c == newTaskJSON.classifiers_id
                        && campaignMember.External_Id__c == newTaskJSON.external_contact_id
                        && !campaignMember.Classifier_from_Campaign_Member__r.Classifier_Closing__c) {
                        
                        if (lastInteractionDate == null) {
                            campaignMember.Classifier_from_Campaign_Member__c = classifier.Id;
                        }
                        else if (lastInteractionDate <= IntegrationFCCInteractionHelper.stringToDateTime(newTaskJSON.interaction_date)) {
                            campaignMember.Classifier_from_Campaign_Member__c = classifier.Id;
                        }
                        lastInteractionDate = IntegrationFCCInteractionHelper.stringToDateTime(newTaskJSON.interaction_date);
                    }
                }
            }
        }   

        return campaignMembers;     
    }

    public static DateTime stringToDateTime(String value) {
        DateTime v = DateTime.valueof(value);
        return v.addHours(1);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global class InteractionSender_JSON {
        public Set<Integer> fcc_campaign_id;
        public String date_from;
        public String date_to;

        public InteractionSender_JSON(Set<Integer> fcc_campaign_id, String date_from, String date_to) {
            this.fcc_campaign_id = fcc_campaign_id;
            this.date_from = date_from;
            this.date_to = date_to;
        }
    }

    global class InteractionReceiver_JSON implements Comparable {
        public String external_contact_id;
        public String interaction_date;
        public String agent_id;
        public String agent_name;
        public String call_type;
        public String call_status;
        public String phone_number;
        public String classifiers_id;

        global InteractionReceiver_JSON() {}

        global InteractionReceiver_JSON(
            String external_contact_id, 
            String interaction_date, 
            String agent_id, 
            String agent_name, 
            String call_type, 
            String call_status, 
            String phone_number,
            String classifiers_id) 
        {
            this.external_contact_id = external_contact_id;
            this.interaction_date = interaction_date;
            this.agent_id = agent_id;
            this.agent_name = agent_name;
            this.call_type = call_type;
            this.call_status = call_status;
            this.phone_number = phone_number;
            this.classifiers_id = classifiers_id;
        }

        global Integer compareTo(Object compareTo) {
            InteractionReceiver_JSON compareToOppy = (InteractionReceiver_JSON)compareTo;

            Integer returnValue = 0;
            if (stringToDateTime(this.interaction_date) < stringToDateTime(compareToOppy.interaction_date)) {
                returnValue = -1;
            } else if (stringToDateTime(this.interaction_date) > stringToDateTime(compareToOppy.interaction_date)) {
                returnValue = 1;
            }
            
            return returnValue;              
        }
    }


    public static List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> parse(String jsonBody) {
        return (List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>.class);
    }


}