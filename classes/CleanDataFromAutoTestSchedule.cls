/**
*   @author         Karolina Bolinger
*   @description    schedulable class that runs batches daily to delete data from automatic tests.
**/

global class CleanDataFromAutoTestSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        CleanDataFromAutoTestBatch cleanDataFromAutoTest = new CleanDataFromAutoTestBatch();
        Database.executebatch(cleanDataFromAutoTest, 200);
    }
}