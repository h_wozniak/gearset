@isTest
global class ZTestIntegrationIPressoControlPanelLink {

	@isTest static void test_receiverScoringRequest_1() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.SendCalloutMock());

        IntegrationIPressoControlPanelLink.AckReceiver_JSON resp = new IntegrationIPressoControlPanelLink.AckReceiver_JSON();

        resp.url = 'www.test.com';
        resp.error_code = '200';
        resp.message = 'Something went ok';

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoControlPanelLink.AckReceiver_JSON result = IntegrationIPressoControlPanelLink.retrievePanelLink();
        Test.stopTest();

        System.assertEquals(result, null);
	}

	@isTest static void test_receiverScoringRequest_2() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse(); 
        RestContext.request = req;
        RestContext.response = res;

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.SendFailedCalloutMock());

        IntegrationIPressoControlPanelLink.AckReceiver_JSON resp = new IntegrationIPressoControlPanelLink.AckReceiver_JSON();

        resp.url = 'www.test.com';
        resp.error_code = '200';
        resp.message = 'Something went ok';

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoControlPanelLink.AckReceiver_JSON result = IntegrationIPressoControlPanelLink.retrievePanelLink();
        Test.stopTest();

        System.assertEquals(result, null);
	}
}