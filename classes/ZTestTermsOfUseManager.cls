@isTest
private class ZTestTermsOfUseManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------- retrieverAttachmentsFromTermsOfUse -------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    @isTest static void test_retrieverAttachmentsFromTermsOfUse() {
        Boolean errorOccured;

        try {
            List<Attachment> attachments = TermsOfUseManager.retrieverAttachmentsFromTermsOfUse(null);
            System.assert(attachments.isEmpty());
        }
        catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);
    }

    @isTest static void test_retrieverAttachmentsFromTermsOfUse_2() {
        List<Catalog__c> catalogs = new List<Catalog__c>();

        List<Attachment> attachments = TermsOfUseManager.retrieverAttachmentsFromTermsOfUse(catalogs);
        System.assert(attachments.isEmpty());
    }

    @isTest static void test_retrieverAttachmentsFromTermsOfUse_3() {
        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat][Wyższe][WSB Poznań]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        List<Attachment> attachments = TermsOfUseManager.retrieverAttachmentsFromTermsOfUse(catalogs);
        System.assert(attachments.isEmpty());
    }

    @isTest static void test_retrieverAttachmentsFromTermsOfUse_4() {
        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat][Wyższe][WSB Poznań]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Attachment> attachments = TermsOfUseManager.retrieverAttachmentsFromTermsOfUse(catalogs);
        System.assert(!attachments.isEmpty());
        System.assertEquals(attachments.size(), 1);
    }

    @isTest static void test_retrieverAttachmentsFromTermsOfUse_5() {
        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat][Wyższe][WSB Poznań]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);
        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Attachment> attachments = TermsOfUseManager.retrieverAttachmentsFromTermsOfUse(catalogs);
        System.assert(!attachments.isEmpty());
        System.assertEquals(attachments.size(), 1);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ retrieveRequiredTermsOfUseToList -------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_retrieveRequiredTermsOfUseToList() {
        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(null, null, null, null);
        System.assert(result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_2() {
        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat][Wyższe][WSB Poznań]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(null, null, null, null);
        System.assert(result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_3() {
        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(null, null, null, null);
        System.assert(result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_4() {
        Test.startTest();
        ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(null, null, null, null);
        System.assert(result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_5() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, null);
        System.assert(result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_6() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, language);
        System.assert(!result.isEmpty());
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_7() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = 'Polish'), true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);
        Catalog__c termOfUsePL = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[PL][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);
        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUsePL.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, language);
        System.assert(!result.isEmpty());
        System.assertEquals(result.size(), 1);
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_8() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = 'English'), true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);
        Catalog__c termOfUsePL = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[PL][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);
        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUsePL.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, language);
        System.assert(!result.isEmpty());
        System.assertEquals(result.size(), 2);
    }

    @isTest static void test_retrieveRequiredTermsOfUseToList_9() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = 'Russian'), true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);
        Catalog__c termOfUsePL = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[PL][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);
        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUsePL.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<Catalog__c> result = TermsOfUseManager.retrieveRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, language);
        System.assert(!result.isEmpty());
        System.assertEquals(result.size(), 2);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- addRequiredTermsOfUseToList -------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_addRequiredTermsOfUseToList() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        Boolean errorOccured;
        try {
            List<String> result = TermsOfUseManager.addRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, null, null, language);
            System.assert(!result.isEmpty());
        }
        catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);
    }

    @isTest static void test_addRequiredTermsOfUseToList_2() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][Wyższe][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<String> result = TermsOfUseManager.addRequiredTermsOfUseToList(studyEnr.Degree__c, studyEnr.University_Name__c, new List<Messaging.EmailFileAttachment>(), null, language);
        System.assert(!result.isEmpty());
    }

    @isTest static void test_addRequiredTermsOfUseToList_3() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][SP][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<String> result = TermsOfUseManager.addRequiredTermsOfUseToList('PG', studyEnr.University_Name__c, new List<Messaging.EmailFileAttachment>(), null, language);
        System.assert(!result.isEmpty());
    }

    @isTest static void test_addRequiredTermsOfUseToList_4() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();

        studyEnr = [
                SELECT Id, Degree__c, University_Name__c, Language_of_Enrollment__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        Catalog__c termOfUse = ZDataTestUtility.createTermOfUse(new Catalog__c(Name = '[EN][Regulamin][Opłat][MBA][WSB Wrocław]'), true);

        List<Catalog__c> catalogs = new List<Catalog__c>();
        catalogs.add(termOfUse);

        ZDataTestUtility.createAttachment(new Attachment(Name = 'test-att1', Body = Blob.valueOf('body'), ParentId = termOfUse.Id), true);

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);
        List<String> result = TermsOfUseManager.addRequiredTermsOfUseToList('MBA', studyEnr.University_Name__c, new List<Messaging.EmailFileAttachment>(), null, language);
        System.assert(!result.isEmpty());
    }
}