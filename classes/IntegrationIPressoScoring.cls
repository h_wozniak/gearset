/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs every 1 hour and imports scoring from iPresso
**/

/**
*   To initiate the IntegrationIPressoScoring execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once every 1hour ---
*   System.schedule('IntegrationIPressoScoring', '0 0 * * * ? *', new IntegrationIPressoScoring());
**/

global class IntegrationIPressoScoring implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new IntegrationIPressoScoringBatch(), 100);
    }
}