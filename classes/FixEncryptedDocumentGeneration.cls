global class FixEncryptedDocumentGeneration implements Database.Batchable<sobject>, Database.AllowsCallouts {
    Datetime startDate;
    Datetime endDate;
    Set<Id> studyEnrIds;

    global FixEncryptedDocumentGeneration(Datetime startDate, Datetime endDate) {
        this.startDate = startDate;
        this.endDate = endDate;

        List<Enrollment__c> studyEnrs = [SELECT Id, RecordTypeId, CreatedDate, Status__c
            FROM Enrollment__c
            WHERE RecordTypeId  =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            AND CreatedDate >: startDate
            AND CreatedDate <: endDate
            AND (not Status__c IN ('Resignation', 'Payments in KS', 'Unconfirmed'))
        ];

        this.studyEnrIds = new Set<Id>();
        for (Enrollment__c studyEnr: studyEnrs) {
            studyEnrIds.add(studyEnr.Id);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator ([
            SELECT Id, RecordTypeId, CreatedDate, Language_of_Main_Enrollment__c, Reference_Number__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.TECH_Is_Encrypted__c,
             Enrollment_from_Documents__r.Status__c, Enrollment_from_Documents__r.University_Name__c, Document__r.Name, Document__r.Id, Degree__c, Current_File__c, Current_File_Name__c
             FROM Enrollment__c
             WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
             AND (Document__r.RecordTypeId =: CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT)
             OR Document__r.Name = 'Kwestionariusz osobowy z podaniem'
             OR Document__r.Name = '[PL] Kwestionariusz osobowy z podaniem')
             AND Enrollment_from_Documents__c IN :studyEnrIds
        ]);
    }

    global void execute(Database.BatchableContext bc, List<Enrollment__c> scope) {
        Set<Id> docEnrIds = new Set<Id>();
        for (Enrollment__c enrollment: scope) {
            docEnrIds.add(enrollment.Id);
        }

        List<Attachment> attachmentOnDoc = [
                Select Id, Name, ParentId
                From Attachment
                Where ParentId IN :docEnrIds
                AND Name LIKE '%[nieszyfrowany]%'
                ORDER BY CreatedDate DESC
        ];

        List<Enrollment__c> docEnrToUpdate = new List<Enrollment__c>();

        for (Enrollment__c doc : scope) {
            for (Attachment att : attachmentOnDoc) {
                if (att.ParentId == doc.Id) {
                    if (doc.Current_File_Name__c != att.Name) {
                        doc.Current_File__c = att.Id;
                        doc.Current_File_Name__c = att.Name;
                        docEnrToUpdate.add(doc);
                    }

                    break;
                }
            }
        }

        try {
            CommonUtility.skipDocumentValidations = true;
            if (!docEnrToUpdate.isEmpty()) {
                Database.update(docEnrToUpdate, false);
            }
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}