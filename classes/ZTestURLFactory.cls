@isTest
private class ZTestURLFactory {

	@isTest static void test_generateUrl() {
		Test.startTest();
 		String result = URLFactory.generateUrl('', true);
		Test.stopTest();

		System.assertEquals(result, 'fault');
	}

	@isTest static void test_generateUrl_properData() {
		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

		Test.startTest();
 		String result = URLFactory.generateUrl('{"sObjectFromId":"' + studyEnrollment.Id + '","sObjectToName":"Enrollment__c","sObjectFromName":"Enrollment__c","recordTypeDevName":"Enrollment_Foreign_Language","fieldFromApiNames":["Name"],"fieldToApiNames":["Enrollment_from_Language__c"]}', true);
		Test.stopTest();

		System.assertEquals(result, 'fault');
	}

	@isTest static void test_getFieldIds() {
		String[] fields = new String[]{'Name', 'Candidate_Student__c'};

		Test.startTest();
 		String[] result = URLFactory.getFieldIds(fields, 'some url');
		Test.stopTest();

		System.assertEquals(result.size(), 0);
	}

	@isTest static void test_getLabelForField_emptyFields() {
		Test.startTest();
 		String[] result = URLFactory.getLabelForField('Enrollment__c', new String[0]);
		Test.stopTest();

		System.assertEquals(result.size(), 0);
	}

	@isTest static void test_getLabelForField() {
		String[] fields = new String[]{'Name', 'Candidate_Student__c'};

		Test.startTest();
 		String[] result = URLFactory.getLabelForField('Enrollment__c', fields);
		Test.stopTest();

		System.assertEquals(result.size(), 2);
	}

	@isTest static void test_getPrefix_enrollment() {
		Test.startTest();
 		String result = URLFactory.getPrefix('Enrollment__c');
		Test.stopTest();

		System.assertNotEquals(result, '');
	}

	@isTest static void test_conditionalConversion_null() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion(null);
		Test.stopTest();

		System.assertEquals(result, '');
	}

	@isTest static void test_conditionalConversion_true() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion('true');
		Test.stopTest();

		System.assertEquals(result, '1');
	}

	@isTest static void test_conditionalConversion_false() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion('false');
		Test.stopTest();

		System.assertEquals(result, '0');
	}

	@isTest static void test_conditionalConversion_number() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion('10.00');
		Test.stopTest();

		System.assertEquals(result, '10,00');
	}

	@isTest static void test_conditionalConversion_date() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion('12.10.1991');
		Test.stopTest();

		System.assertEquals(result, '12.10.1991');
	}

	@isTest static void test_conditionalConversion_Properdate() {
		Test.startTest();
 		String result = URLFactory.conditionalConversion('1991-10-12');
		Test.stopTest();

		System.assertEquals(result, '12.10.1991');
	}
}