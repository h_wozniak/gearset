/**
*   @author         Wojciech Słodziak
*   @description    Batch class summarizing number of finalized Study Enrollments for Sales Targets.
**/

global without sharing class StudySalesTargetComputationsBatch implements Database.Batchable<sObject> {

    Boolean isRunManually;

    global StudySalesTargetComputationsBatch(Boolean isRunManually) {
        this.isRunManually = isRunManually;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, University_Study_Offer_Id__c, Educational_Advisor__c, Enrollment_Date__c, Source_Studies__c
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            AND Status__c IN :EnrollmentManager_Studies.finalizedEnrStatusesForContactForTargets
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Enrollment__c> enrList = (List<Enrollment__c>) scope;

        List<Target__c> targetList = getListOfApplicableTargets(enrList);

        Map<String, List<Enrollment__c>> targetKeyToEnrListMap = getMapOfKeyTargetKeysToEnrList(enrList);


        Set<Target__c> targetListToUpdate = new Set<Target__c>();
        for (Target__c studyOfferTarget : targetList) {
            targetListToUpdate.add(studyOfferTarget);

            studyOfferTarget.Realization_Amount_Batch_Helper__c += targetKeyToEnrListMap.get(studyOfferTarget.University_Offer_Study__c).size();

            // Subtargets
            for (Target__c subtarget : studyOfferTarget.Subtargets__r) {
                targetListToUpdate.add(subtarget);
                subtarget.Realization_Amount_Batch_Helper__c += getAmountForSubtarget(targetKeyToEnrListMap.get(studyOfferTarget.University_Offer_Study__c), subtarget);
            }

            // Team Targets
            for (Target__c teamTarget : studyOfferTarget.TeamTargets__r) {
                List<Enrollment__c> enrListForTarget = targetKeyToEnrListMap.get('' + studyOfferTarget.University_Offer_Study__c + teamTarget.Team__c);
                
                if (enrListForTarget != null) {
                    targetListToUpdate.add(teamTarget);
                    teamTarget.Realization_Amount_Batch_Helper__c += enrListForTarget.size();
                }
            }

            // Educational Advisor Targets
            for (Target__c eduAdvTarget : studyOfferTarget.EducationalAdvisorTargets__r) {
                List<Enrollment__c> enrListForTarget = targetKeyToEnrListMap.get('' + studyOfferTarget.University_Offer_Study__c + eduAdvTarget.Educational_Advisor__c);

                if (enrListForTarget != null) {
                    targetListToUpdate.add(eduAdvTarget);
                    eduAdvTarget.Realization_Amount_Batch_Helper__c += enrListForTarget.size();
                }
            }
        }


        try {
            update new List<Target__c>(targetListToUpdate);
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
    
    // on finish execute another batch, which performs final update on Targets (to generate only 1 row in Field Update History)
    global void finish(Database.BatchableContext BC) {
        StudySalesTargetFinalizationBatch sstfb = new StudySalesTargetFinalizationBatch(isRunManually);
        Database.executeBatch(sstfb);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // methods returns list of Targets that are influenced by queried by batch Enrollments
    private static List<Target__c> getListOfApplicableTargets(List<Enrollment__c> enrList) {
        Set<Id> univOffStudyIds = new Set<Id>();
        Set<Id> educationalAdvIds = new Set<Id>();
        for (Enrollment__c studyEnr : enrList) {
            univOffStudyIds.add(studyEnr.University_Study_Offer_Id__c);
            educationalAdvIds.add(studyEnr.Educational_Advisor__c);
        }

        List<Target__c> targetList = [
            SELECT Id, Realization_Amount_Batch_Helper__c, University_Offer_Study__c,
                (SELECT Id, Realization_Amount_Batch_Helper__c, Educational_Advisor__c
                FROM EducationalAdvisorTargets__r
                WHERE Educational_Advisor__c IN :educationalAdvIds),
                (SELECT Id, Realization_Amount_Batch_Helper__c, Applies_from__c, Applies_to__c
                FROM Subtargets__r),
                (SELECT Id, Realization_Amount_Batch_Helper__c, Team__c
                FROM TeamTargets__r)
            FROM Target__c
            WHERE University_Offer_Study__c IN :univOffStudyIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_TARGET)
        ];

        return targetList;
    }


    // methods returns map of Target Keys to corresponding Enrollments List
    private static Map<String, List<Enrollment__c>> getMapOfKeyTargetKeysToEnrList(List<Enrollment__c> enrList) {
        Map<String, List<Enrollment__c>> targetKeyToEnrListMap = new Map<String, List<Enrollment__c>>();

        for (Enrollment__c studyEnr : enrList) {
            matchEnrToTargetKey(targetKeyToEnrListMap, studyEnr);
        }

        return targetKeyToEnrListMap;
    }


    // helper map for matchEnrToTargetKey
    private final static Map<String, String> sourceToTeamMap = new Map<String, String> {
        CommonUtility.ENROLLMENT_SOURCE_ST_CC => CommonUtility.TARGET_TEAM_CC,
        CommonUtility.ENROLLMENT_SOURCE_ST_EO => CommonUtility.TARGET_TEAM_EO
    };
    // method fills map with Target Key to corresponding Enrollments List
    private static void matchEnrToTargetKey(Map<String, List<Enrollment__c>> targetKeyToEnrListMap, Enrollment__c enrToMatch) {
        // Target RT - Study Enrollment Target (key is University Study Offer Id)
        putEnrToMapOfLists(targetKeyToEnrListMap, enrToMatch, enrToMatch.University_Study_Offer_Id__c);

        // Target RT - Team Target (key is University Study Offer Id + Team__c picklist val mapped from Source_Studies__c)
        if (enrToMatch.Source_Studies__c != null) {
            if (sourceToTeamMap.keySet().contains(enrToMatch.Source_Studies__c)) {
                putEnrToMapOfLists(targetKeyToEnrListMap, enrToMatch, '' + enrToMatch.University_Study_Offer_Id__c + sourceToTeamMap.get(enrToMatch.Source_Studies__c));
            }
        }

        // Target RT - Educational Advisor Target (key is University Study Offer Id + EduAdv User Id)
        if (enrToMatch.Source_Studies__c == CommonUtility.ENROLLMENT_SOURCE_ST_EA && enrToMatch.Educational_Advisor__c != null) {
            putEnrToMapOfLists(targetKeyToEnrListMap, enrToMatch, '' + enrToMatch.University_Study_Offer_Id__c + enrToMatch.Educational_Advisor__c);
        }
    }


    // method puts Enrollment to list under specific key or creates new List if one doesn't exist yet
    private static void putEnrToMapOfLists(Map<String, List<Enrollment__c>> targetKeyToEnrListMap, Enrollment__c enrToPut, String key) {
        if (targetKeyToEnrListMap.containsKey(key)) {
            List<Enrollment__c> enrListFromMap = targetKeyToEnrListMap.get(key);
            enrListFromMap.add(enrToPut);
        } else {
            List<Enrollment__c> enrList = new List<Enrollment__c> { enrToPut };
            targetKeyToEnrListMap.put(key, enrList);
        }
    }


    // methods counts Enrollments that are between Subtarget Application dates
    private static Decimal getAmountForSubtarget(List<Enrollment__c> enrList, Target__c subtarget) {
        Decimal amount = 0;
        Datetime appliesFromPrecise = DateTime.newInstance(subtarget.Applies_from__c.year(), subtarget.Applies_from__c.month(), subtarget.Applies_from__c.day(), 
                                                           0, 0, 0);

        Datetime appliesToPrecise = DateTime.newInstance(subtarget.Applies_to__c.year(), subtarget.Applies_to__c.month(), subtarget.Applies_to__c.day(), 
                                                         23, 59, 59);

        for (Enrollment__c studyEnr : enrList) {
            if (studyEnr.Enrollment_Date__c >= appliesFromPrecise && studyEnr.Enrollment_Date__c <= appliesToPrecise) {
                amount++;
            }
        }

        return amount;
    }
    
}