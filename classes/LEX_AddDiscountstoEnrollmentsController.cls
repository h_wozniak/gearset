public with sharing class LEX_AddDiscountstoEnrollmentsController {
	
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String addCompanyDiscountToEnrollments(String recordId) {
		return LEXStudyUtilities.addCompanyDiscountToEnrollments(recordId);
	}
}