@isTest
global class ZTestFixSendingTemplateScheduleBatch {

    private static Map<Integer, sObject> prepareDataForTest_OfferWithPB1(Boolean pbActive, Decimal instAmount, Boolean config) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 7
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = pbActive
        ), true));
        retMap.put(3, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(1).Id,
            Active__c = pbActive
        ), true));
        retMap.put(4, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
            Synchronization_Status__c = CommonUtility.SYNCSTATUS_FAILED,
            Active__c = pbActive
        ), true));

        //config PriceBook
        if (config) {
            configPriceBook(retMap.get(2).Id, instAmount);
            configPriceBook(retMap.get(3).Id, instAmount);
            configPriceBook(retMap.get(4).Id, instAmount);
        }

        return retMap;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- HELPER METHODS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_getPBRecordTypeIdByDegree() {
        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_I)
        );

        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_PG)
        );

        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_MBA)
        );
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

	@isTest static void test_FixSendingTemplateScheduleBatch() {
		Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(false, 100, true);
 
        List<Offer__c> pbBefore = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Synchronization_Status__c = :CommonUtility.SYNCSTATUS_FAILED
        ];

        System.assertEquals(pbBefore.size(), 1);

        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock());

		Test.startTest();
        FixSendingTemplateScheduleBatch batch3Days = new FixSendingTemplateScheduleBatch();
        Database.executebatch(batch3Days, 10);
		Test.stopTest();
 
        List<Offer__c> pbAfter = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Synchronization_Status__c = :CommonUtility.SYNCSTATUS_FAILED
        ];

        System.assertEquals(pbAfter.size(), 0);
	}

    @isTest static void test_FixSendingTemplateScheduleBatch_Failed() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(false, 100, true);
 
        List<Offer__c> pbBefore = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Synchronization_Status__c = :CommonUtility.SYNCSTATUS_FAILED
        ];

        System.assertEquals(pbBefore.size(), 1);

        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentSchedulesMock_Fail());

        Test.startTest();
        FixSendingTemplateScheduleBatch batch3Days = new FixSendingTemplateScheduleBatch();
        Database.executebatch(batch3Days, 10);
        Test.stopTest();
 
        List<Offer__c> pbAfter = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Synchronization_Status__c = :CommonUtility.SYNCSTATUS_FAILED
        ];

        System.assertEquals(pbAfter.size(), 1);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class IntegrationPaymentSchedulesMock implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
            String createPaymentEndPoint = esb.Create_Payment_Schedules_End_Point__c;
            String esbIp = esb.Service_Url__c;
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);

            return res;
        }
    }

    global class IntegrationPaymentSchedulesMock_Fail implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
            String createPaymentEndPoint = esb.Create_Payment_Schedules_End_Point__c;
            String esbIp = esb.Service_Url__c;
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setStatusCode(500);

            return res;
        }
    }
}