/**
 * Created by Sebastian on 25.01.2018.
 */

public with sharing class LEX_AddManualDiscount {

    @AuraEnabled
    public static Boolean checkIfCanEditUneditableEnrollment() {
        return CommonUtility.checkIfCanEditUneditableEnrollment();
    }

    @AuraEnabled
    public static Enrollment__c getEnrollment(Id recordId) {
        Enrollment__c enrollmentFromDiscount = [
                SELECT Id, Value_after_Discount__c, Language_of_Enrollment__c, Status__c, Unenrolled_Status__c, WasIs_Unenrolled__c, Unenrolled__c
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        return enrollmentFromDiscount;
    }

    @AuraEnabled
    public static Discount__c getNewDiscount(Id enrollmentFromDiscountId) {
        Discount__c newDiscount = new Discount__c();
        newDiscount.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT);
        newDiscount.Enrollment__c = enrollmentFromDiscountId;
        newDiscount.Applied__c = true;

        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_DISCRETIONARY;

        Map<String, String> translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('PL', Discount__c.Discount_Type_Studies__c);
        newDiscount.Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('EN', Discount__c.Discount_Type_Studies__c);
        newDiscount.English_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('RU', Discount__c.Discount_Type_Studies__c);
        newDiscount.Russian_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        return newDiscount;
    }

    @AuraEnabled
    public static List<PickListValue_Wrapper> getPicklistLabelMap(String fieldname) {
        Schema.DescribeSObjectResult objSchema = Discount__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getMap();

        List<PickListValue_Wrapper> wrapper = new List<PickListValue_Wrapper>();
        for (Schema.PicklistEntry entry : fieldmap.get(fieldname).getDescribe().getPicklistValues()) {
            wrapper.add(new PickListValue_Wrapper(entry.label, entry.value));
        }

        return wrapper;
    }

    @AuraEnabled
    public static Discount__c fillDiscountInfo(Discount__c newDiscount) {
        newDiscount.Applies_to__c = null;
        newDiscount.Discount_Value__c = null;
        newDiscount.Applied_through__c = null;
        newDiscount.Discount_Kind__c = null;
        newDiscount.Delivery_Documents_Date__c = null;
        if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE) {
            newDiscount.Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION;
            newDiscount.Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE;
            newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
            newDiscount.Discount_Value__c = 50;
        } else if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_RECPERSON) {
            newDiscount.Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION;
            newDiscount.Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY;
            newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT;
            newDiscount.Discount_Value__c = 100;
        }

        Map<String, String> translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('PL', Discount__c.Discount_Type_Studies__c);
        newDiscount.Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('EN', Discount__c.Discount_Type_Studies__c);
        newDiscount.English_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('RU', Discount__c.Discount_Type_Studies__c);
        newDiscount.Russian_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        return newDiscount;
    }

    @AuraEnabled
    public static String saveRecord(Discount__c newDiscount) {
        List<Discount__c> discountsToInsert = new List<Discount__c>();
        newDiscount.Name = CommonUtility.getPicklistLabelMap(Discount__c.Discount_Type_Studies__c).get(newDiscount.Discount_Type_Studies__c);

        discountsToInsert.add(newDiscount);

        if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_VIS) {
        } else if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_RECPERSON && newDiscount.Recommended_by__c != null) {

            if (recEnrEqualsDiscountedEnr(newDiscount)) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_CantAddRecPersonDiscountToSameEnr));
                return (Label.msg_error_CantAddRecPersonDiscountToSameEnr);
            }

            Discount__c parallelDiscount = new Discount__c();

            parallelDiscount.Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('PL', Label.title_DiscountPersonRecommendation);
            parallelDiscount.English_Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('EN', Label.title_DiscountPersonRecommendation);
            parallelDiscount.Russian_Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('RU', Label.title_DiscountPersonRecommendation);

            parallelDiscount.Name = Label.title_DiscountPersonRecommendation;
            parallelDiscount.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT);
            parallelDiscount.Enrollment__c = newDiscount.Recommended_by__c;
            parallelDiscount.Recommended_Person__c = newDiscount.Enrollment__c;
            parallelDiscount.Applied__c = newDiscount.Applied__c;
            parallelDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
            parallelDiscount.Applies_to__c = newDiscount.Applies_to__c;
            parallelDiscount.Applied_through__c = newDiscount.Applied_through__c;
            parallelDiscount.Discount_Kind__c = newDiscount.Discount_Kind__c;
            parallelDiscount.Discount_Value__c = newDiscount.Discount_Value__c;

            discountsToInsert.add(parallelDiscount);
        }

        try {
            insert discountsToInsert;
            return 'true';
        } catch(Exception e) {
            return CommonUtility.trimErrorMessage(e.getMessage());
        }
    }


    private static Boolean recEnrEqualsDiscountedEnr(Discount__c newDiscount) {
        return newDiscount.Enrollment__c == newDiscount.Recommended_by__c;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class PickListValue_Wrapper {
        @AuraEnabled  public String label {get; set;}
        @AuraEnabled  public String value {get; set;}

        public PickListValue_Wrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}