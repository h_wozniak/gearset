global class CreateCampaignContactBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    Datetime dayOfEnrollmentFrom;
    Datetime dayOfEnrollmentTo;
    Set<Id> campaignIds = new Set<Id>();

    global CreateCampaignContactBatch(Integer daysAfterEnrollment) {
        Date dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
        dayOfEnrollmentFrom = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 0, 0, 0);
        dayOfEnrollmentTo = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 23, 59, 59);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY) 
            AND CreatedDate >= :dayOfEnrollmentFrom
            AND CreatedDate <= :dayOfEnrollmentTo
            AND Status__c = :CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
            AND Source_Studies__c != :CommonUtility.ENROLLMENT_SOURCE_ST_EA
            AND Guided_group__c = false
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrollmentIDs = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrollmentIDs.add(enr.Id);
        }
        
        CommonUtility.preventsSendingCalloutAsAtFuture = true;
        CommonUtility.preventSendingWelcomeCallToFcc = true;
        campaignIds.addAll(MarketingCampaignManager.createContactCampaing(enrollmentIDs, CommonUtility.MARKETING_CAMPAIGN_CONFIRMATIONOFENROLLING));
    }
    
    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            IntegrationFCCContactSender.sendNewFCCRecordsSync(campaignIds);
        }
    }
    
}