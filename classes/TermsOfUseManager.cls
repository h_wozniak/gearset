/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store methods related to Terms of Use Documents
**/

public without sharing class TermsOfUseManager {

	/* Sebastian Łasisz */
	// Method use to retrieve all valid terms of use for current enrollment process
	public static List<Catalog__c> retrieveRequiredTermsOfUseToList(String degree, String universityName, Date createdDate, String enrollmentLanguage) {
		List<String> termsOfUseDefinitions = new List<String> {
				RecordVals.CATALOG_TERMS_OF_USE_PAYMENTS,
				RecordVals.CATALOG_TERMS_OF_USE_DISCOUNTS,
				RecordVals.CATALOG_TERMS_OF_USE_STUDIES
		};

		List<String> termsOfUseNamesToRetrieve = new List<String>();
		Set<String> languages = new Set<String>();

		languages.add(CommonUtility.LANGUAGE_CODE_PL);
		if (enrollmentLanguage != CommonUtility.LANGUAGE_CODE_PL) {
			languages.add(CommonUtility.LANGUAGE_CODE_EN);
		}

		for (String termsOfUseDefinition : termsOfUseDefinitions) {
			for (String language : languages) {
				String termsOfUseName = '';
				if (language != null) {
					termsOfUseName += '[' + language + ']';
				}

				termsOfUseName += '[' + RecordVals.CATALOG_TERMS_OF_USE + '][' + termsOfUseDefinition + ']';
				if (degree != null) {
					if (degree == CommonUtility.OFFER_DEGREE_I || degree == CommonUtility.OFFER_DEGREE_II || degree == CommonUtility.OFFER_DEGREE_II_PG || degree == CommonUtility.OFFER_DEGREE_U) {
						termsOfUseName += '[' + CommonUtility.TEMPLATE_NAME_HIGHER_EDUCATION + ']';
					} else if (degree == CommonUtility.OFFER_DEGREE_PG) {
						termsOfUseName += '[SP]';
					} else {
						termsOfUseName += '[' + degree + ']';
					}
				}
				if (universityName != null) {
					termsOfUseName += '[' + universityName + ']';
				}

				termsOfUseNamesToRetrieve.add(termsOfUseName);
			}
		}

		List<Catalog__c> termsOfUse = [
				SELECT Id, Name, Activation_Date__c
				FROM Catalog__c
				WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_TERMSOFUSE)
				AND Activation_Date__c <= :createdDate
				AND Name IN :termsOfUseNamesToRetrieve
				ORDER BY Activation_Date__c ASC
		];

		Map<String, Catalog__c> validatedTermsOfUse = new Map<String, Catalog__c>();
		for (Catalog__c tos : termsOfUse) {
			validatedTermsOfUse.put(tos.Name, tos);
		}

		return validatedTermsOfUse.values();
	}


	/* Sebastian Łasisz */
	// Method use to retrieve all valid attachments for given Terms of Use
	public static List<Attachment> retrieverAttachmentsFromTermsOfUse(List<Catalog__c> retrievedTermsOfUseList) {
		Set<id> attachmentParentIds = new Set<Id>();
		for (Catalog__c retrievedTermsOfUse : retrievedTermsOfUseList) {
			attachmentParentIds.add(retrievedTermsOfUse.Id);
		}

		List<Attachment> attachmentList = [
				SELECT Id, ParentId, Name, Body
				FROM Attachment
				WHERE ParentId IN :attachmentParentIds
				ORDER BY CreatedDate ASC
		];

		Map<Id, Attachment> validatedAttachmentMap = new Map<Id, Attachment>();
		for (Attachment att : attachmentList) {
			validatedAttachmentMap.put(att.ParentId, att);
		}

		return validatedAttachmentMap.values();
	}

	public static List<String> addRequiredTermsOfUseToList(String degree, String universityName, List<Messaging.EmailFileAttachment> emailAttachmentList, Date createdDate, String language) {
		List<Catalog__c> retrievedTermsOfUseList = retrieveRequiredTermsOfUseToList(degree, universityName, createdDate, language);
		List<Attachment> retrievedAttachmentList = retrieverAttachmentsFromTermsOfUse(retrievedTermsOfUseList);

		List<String> attachmentList = new List<String>();
		for (Catalog__c retrievedTermsOfUse : retrievedTermsOfUseList) {
			for (Attachment retrievedAttachment : retrievedAttachmentList) {
				if (retrievedAttachment.ParentId == retrievedTermsOfUse.Id) {
					Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
					efa.setFileName(CommonUtility.swapSpecialPolishChars(retrievedAttachment.Name));
					efa.setBody(retrievedAttachment.Body);

					attachmentList.add(retrievedAttachment.Name);
					emailAttachmentList.add(efa);
				}
			}
		}

		return attachmentList;
	}
}