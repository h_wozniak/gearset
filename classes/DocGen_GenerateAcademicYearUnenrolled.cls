global class DocGen_GenerateAcademicYearUnenrolled implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateAcademicYearUnenrolled() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
        if (methodName == 'startingAcademicYear') {
            return generateStartingAcademicyear(objectId);
        }
        return DocGen_GenerateAcademicYearUnenrolled.generateAcademicYear(objectId); 
    }

    global static String generateAcademicYear(String objectId){    
        Enrollment__c studyEnr = [
            SELECT Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, Enrollment_From_documents__r.Starting_Semester__c,
                   Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];
        
        String result = '';
        if (studyEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null) {
            Date studyStartDate = studyEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c;

            Integer startingSemester = Integer.valueOf(studyEnr.Enrollment_From_documents__r.Starting_Semester__c);
            if (startingSemester > 1) {           
                while (startingSemester > 1) {     
                    if (studyStartDate.month() <= 3) {
                        studyStartDate = studyStartDate.addMonths(7);
                    }
                    else {
                        studyStartDate = studyStartDate.addMonths(5);
                    }

                    startingSemester--;
                }
            }

            if (Integer.valueOf(studyStartDate.month()) < 9) {
                return (String.valueOf(studyStartDate.year() - 1) + '/' + String.valueOf(studyStartDate.year()));
            }
            else {
                return studyStartDate.year() + '/' + (String.valueOf(studyStartDate.year() + 1));
            }
        }
        else {
            Date studyStartDate = studyEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c;

            Integer startingSemester = Integer.valueOf(studyEnr.Enrollment_From_documents__r.Starting_Semester__c);
            if (startingSemester > 1) {           
                while (startingSemester > 1) {     
                    if (studyStartDate.month() <= 3) {
                        studyStartDate = studyStartDate.addMonths(7);
                    }
                    else {
                        studyStartDate = studyStartDate.addMonths(5);
                    }

                    startingSemester--;
                }
            }
            
            if (Integer.valueOf(studyStartDate.month()) < 9) {
                return (String.valueOf(studyStartDate.year() - 1) + '/' + String.valueOf(studyStartDate.year()));
            }
            else {
                return studyStartDate.year() + '/' + (String.valueOf(studyStartDate.year() + 1));
            }
        }
        
        return result;
    }

    global static String generateStartingAcademicyear(String objectId) {
        Enrollment__c docEnr = [
            SELECT Enrollment_From_documents__r.Starting_Semester__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        Integer academicYear = Integer.valueOf(docEnr.Enrollment_From_documents__r.Starting_Semester__c);
        return String.valueOf(math.mod(academicYear, 2) == 0 ? (academicYear / 2) : (academicYear + 1) / 2);
    }
}