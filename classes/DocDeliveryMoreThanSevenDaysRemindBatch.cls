/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Study Enrollment confirmation reminder
**/

global class DocDeliveryMoreThanSevenDaysRemindBatch implements Database.Batchable<sObject> {
    
    Date dayOfDeadline;
    Integer docListCounter;

    global DocDeliveryMoreThanSevenDaysRemindBatch(Integer daysBeforeDeadline, Integer docCounter) {
        dayOfDeadline = System.today().addDays(daysBeforeDeadline);
        docListCounter = docCounter;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id
            FROM Enrollment__c 
            WHERE Documents_Delivered__c = false
            AND Remaining_Days_For_Document_Delivery__c > 7
            AND Delivery_Deadline__c = :dayOfDeadline
            AND Unenrolled__c = false
            AND RequiredDocListEmailedCount__c = :docListCounter
            AND RequiredDocListEmailedCount__c < 3
            AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
            AND Dont_send_automatic_emails__c = false
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrIds = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrIds.add(enr.Id);
        }

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrIds);
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}