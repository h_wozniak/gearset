/**
*   @author         Sebastian Łasisz
*   @description    Batch class that retrieves High School events from Google Calendar based on Custom Metadata.
**/

global class RetrieveHighSchoolVisitsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                       Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ]);
    }

    global void execute(Database.BatchableContext BC, List<WSB_Calendar_Settings__mdt> wSBCalendarSettigs) {
        for (WSB_Calendar_Settings__mdt wSBCalendarSettig : wSBCalendarSettigs) {
            IntegrationGoogleCalendarEvent.getEventsList(wSBCalendarSettig, 'primary');
        }
    }

    global void finish(Database.BatchableContext BC) {
    }

}