/**
*   @author         Wojciech Słodziak
*   @description    Class used for sending emails on error - mostly used in try catch
**/

public without sharing class ErrorLogger {

    static User u;
    static Profile uProfile;
    static Organization org;

    public static void log(Exception e) {
        List<String> addressList = getErrorMessageAddressListFromCustomSettings();

        if (!addressList.isEmpty()) {
            getUserAndOrg();

            String exc = 'EXCEPTION TYPE: ' + e.getTypeName() + '\n';
            exc += '...CAUSE:   ' + e.getCause() + '\n';
            exc += '...LINE NO: ' + e.getLineNumber() + '\n';
            exc += '...MESSAGE: ' + e.getMessage() + '\n';
            exc += '...STACK TRACE: ' + e.getStackTraceString() + '\n';
            exc += '...USER NAME: ' + u.Name + '\n';
            exc += '...USER USERNAME: ' + u.UserName + '\n';
            exc += '...USER PROFILE: ' + uProfile.Name + '\n';


            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(addressList);
            mail.setSubject('Error Occurred - ' + org.Name);
            mail.setPlainTextBody('The following error occurred in system: ' + exc);

            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            } catch(Exception unhandledExc) {}
        }
    }

    public static void logAndMsg(Exception e, String msg) {
        List<String> addressList = getErrorMessageAddressListFromCustomSettings();

        if (!addressList.isEmpty()) {
            getUserAndOrg();

            String finalMsg = 'EXCEPTION TYPE: ' + e.getTypeName() + '\n';
            finalMsg += '...CAUSE:   ' + e.getCause() + '\n';
            finalMsg += '...LINE NO: ' + e.getLineNumber() + '\n';
            finalMsg += '...MESSAGE: ' + e.getMessage() + '\n';
            finalMsg += '...STACK TRACE: ' + e.getStackTraceString() + '\n';
            finalMsg += '...USER NAME: ' + u.Name + '\n';
            finalMsg += '...USER USERNAME: ' + u.UserName + '\n';
            finalMsg += '...USER PROFILE: ' + uProfile.Name + '\n';

            if(msg != null) {
                finalMsg += '\n\nThe following message was sent from system:\n' + msg + '\n\n';
            }

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(addressList);
            mail.setSubject('Error Occurred - ' + org.Name);
            mail.setPlainTextBody('The following error occurred in system: ' + finalMsg);

            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            } catch(Exception unhandledExc) {}
        }
    }

    public static void msg(String msg) {
        List<String> addressList = getErrorMessageAddressListFromCustomSettings();

        if (!addressList.isEmpty()) {
            if(msg != null) {
                getUserAndOrg();

            
                String finalMsg = 'The following message was sent from system:\n' + msg + '\n\n';
                finalMsg += '[USER NAME]: ' + u.Name + '\n';
                finalMsg += '[USER USERNAME]: ' + u.UserName + '\n';
                finalMsg += '[USER PROFILE]: ' + uProfile.Name + '\n';

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(addressList);
                mail.setSubject('Apex Code Message');
                mail.setPlainTextBody(finalMsg);

                try {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                } catch(Exception unhandledExc) {}
            }
        }

    }

    public static void configMsg(String msg) {
        List<String> addressList = getConfigMessageAddressListFromCustomSettings();

        if (!addressList.isEmpty()) {
            if(msg != null) {
                getUserAndOrg();

                String finalMsg = 'The following message was sent from system:\n' + msg + '\n\n';
                finalMsg += '[USER NAME]: ' + u.Name + '\n';
                finalMsg += '[USER USERNAME]: ' + u.UserName + '\n';
                finalMsg += '[USER PROFILE]: ' + uProfile.Name + '\n';

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(addressList);
                mail.setSubject('Critical Admin Message');
                mail.setPlainTextBody(finalMsg);

                try {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                } catch(Exception unhandledExc) {}
            }
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- CONFIG RETRIEVE ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static void getUserAndOrg() {
        if (u == null || org == null) {
            Id uId = UserInfo.getUserId();
            u = [SELECT Id, Name, UserName FROM User WHERE Id = :uId];
            Id pId = UserInfo.getProfileId();
            uProfile = [SELECT Id, Name FROM Profile WHERE Id = :pId];
            org = [Select Name From Organization];
        }
    }

    private static List<String> getErrorMessageAddressListFromCustomSettings() {
        List<String> stringEmailList = new List<String>();
        Map<String, ErrorLogger_Email__c> emailList = ErrorLogger_Email__c.getAll();

        for (ErrorLogger_Email__c e : emailList.values()) {
            if (e.Active__c) {
                stringEmailList.add(e.Email__c);
            }
        }

        return stringEmailList;
    }

    private static List<String> getConfigMessageAddressListFromCustomSettings() {
        List<String> stringEmailList = new List<String>();
        Map<String, ConfigurationErrorLogger_Email__c> emailList = ConfigurationErrorLogger_Email__c.getAll();

        for (ConfigurationErrorLogger_Email__c e : emailList.values()) {
            if (e.Active__c) {
                stringEmailList.add(e.Email__c);
            }
        }

        return stringEmailList;
    }

}