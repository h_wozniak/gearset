/**
*   @author         Sebastian Łasisz
*   @description    batch class that creates external classifiers for imported external campaigns
**/

global class IntegrationClassifierCreationBatch implements Database.Batchable<IntegrationClassifierCreation.Campaign_JSON> {
	List<Marketing_Campaign__c> externalCampaigns;
	List<Marketing_Campaign__c> existingClassifiers;
	List<IntegrationClassifierCreation.Campaign_JSON> parsedJson;

	global IntegrationClassifierCreationBatch(List<Marketing_Campaign__c> externalCampaigns, List<Marketing_Campaign__c> existingClassifiers, List<IntegrationClassifierCreation.Campaign_JSON> parsedJson) {
		this.externalCampaigns = externalCampaigns;
		this.existingClassifiers = existingClassifiers;
		this.parsedJson = parsedJson;
	}
    
    global Iterable<IntegrationClassifierCreation.Campaign_JSON> start(Database.BatchableContext BC) {
        return parsedJson;
    }

    global void execute(Database.BatchableContext BC, List<IntegrationClassifierCreation.Campaign_JSON> scope) {
    	List<Marketing_Campaign__c> classifiersToAdd = new List<Marketing_Campaign__c>();

    	for (IntegrationClassifierCreation.Campaign_JSON campaign : scope) {
			for (IntegrationClassifierCreation.Classifier_JSON classifierFromJSON : campaign.classifiers) {
    			classifiersToAdd.add(IntegrationClassifierCreation.createClassifier(existingClassifiers, classifierFromJSON, campaign, externalCampaigns));
    		}
        }

        try {
        	upsert classifiersToAdd;
        }
        catch (Exception e) {
        	ErrorLogger.log(e);
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}