@isTest
private class ZTestEnrollmentVerificationManager {
    
    private static void createCatalogConsents() {
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT,
            Consent_ADO_API_Name__c = 'Consent_Marketing_ADO__c',
            Consent_API_Name__c = 'Consent_Marketing__c'
        ), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_2,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_3,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT,
            Consent_ADO_API_Name__c = 'Consent_PUCA_ADO__c',
            Consent_API_Name__c = 'Consent_PUCA__c'
        ), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_4,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_5,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
    }

    @isTest static void test_verificationActions() {
        IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON jsonObj = new IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON();
        jsonObj.consents = new List<IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON>();
        
        IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON consent1 = new IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON();
        consent1.name = RecordVals.MARKETING_CONSENT_1;
        consent1.value = true;
        jsonObj.consents.add(consent1);

        IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON consent2 = new IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON();
        consent2.name = RecordVals.MARKETING_CONSENT_2;
        consent2.value = false;
        jsonObj.consents.add(consent2);

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            Confirmed_Contact__c = false
        ), true);

        createCatalogConsents();

        Enrollment__c enr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
            Candidate_Student__c = candidate.Id,
            Enrollment_JSON__c = JSON.serialize(jsonObj)
        ), true);


        enr.VerificationDate__c = System.now();

        List<Marketing_Campaign__c> mcList = [SELECT Id, Consent__c FROM Marketing_Campaign__c WHERE Contact_from_Consent__c = :candidate.Id];

        System.assert(mcList.isEmpty());

        Test.startTest();
        update enr;
        Test.stopTest();

        List<Task> taskList = [SELECT Id, Subject FROM Task WHERE Subject = :Label.title_EventCandidateVerifiedEnrollment];
        System.assert(!taskList.isEmpty());

        Enrollment__c enrAfter = [SELECT Id, Status__c FROM Enrollment__c WHERE Id = :enr.Id];
        System.assertNotEquals(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED, enrAfter.Status__c);

        Contact candidateAfter = [SELECT Confirmed_Contact__c, Consent_PUCA_ADO__c FROM Contact WHERE Id = :candidate.Id];
        System.assert(candidateAfter.Confirmed_Contact__c);

        System.assertEquals(1, CommonUtility.getOptionsFromMultiSelect(candidateAfter.Consent_PUCA_ADO__c).size());
    }
    
}