/**
* @author       Wojciech Słodziak, Sebastian Łasisz
* @description  Class for storing methods related to Enrollment__c object for Training Record Types
**/


public without sharing class EnrollmentManager_Trainings {
    
    // Author: Wojciech Słodziak
    // Desc: Method that recalulcated Discount Value based on Discounts and actual Enrollment Value
    // Details: Each percent discount is calculeted based on Enrollment_Value__c
    public static void recalculateDiscountValue(Set<Id> enrollmentIds) {
        List<Enrollment__c> enrollmentList = [SELECT Id, Discount_Value__c, Enrollment_Value__c FROM Enrollment__c WHERE Id IN :enrollmentIds];
        List<Discount__c> discountList = [SELECT Id, Discount_Kind__c, Discount_Value__c, Enrollment__c FROM Discount__c WHERE Enrollment__c IN : enrollmentIds];

        for (Enrollment__c e : enrollmentList) {
            e.Discount_Value__c = 0; 
            for (Discount__c d : discountList) {
                if (d.Enrollment__c == e.Id) {
                    if (e.Enrollment_Value__c != null && e.Discount_Value__c != null) {
                        if (d.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_PERCENTAGE) {
                            e.Discount_Value__c += e.Enrollment_Value__c * d.Discount_Value__c / 100;
                        } else if (d.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT) {
                            e.Discount_Value__c += d.Discount_Value__c;
                        }
                    }
                }
            }
        }

        try {
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }


    // Author: Wojciech Słodziak
    // Desc: Method copies specific for_email fields from Schedule to Enrollments
    public static void copyForEmailFields(Set<Id> scheduleIds) {
        List<Offer__c> scheduleList = [
            SELECT Id, Valid_From__c, Valid_To__c, Payment_Deadline__c, University_City_in_Vocative__c, 
            Training_Offer_from_Schedule__r.Training_Administrator__c, Training_Offer_from_Schedule__r.Training_Administrator__r.Email, 
            Training_Offer_from_Schedule__r.University_from_Training__r.Phone, Street__c, City__c, Bank_Account_No__c  
            FROM Offer__c 
            WHERE Id IN :scheduleIds 
            AND (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
            OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
        ];

        List<Enrollment__c> enrollmentList = [SELECT Id, Training_Offer__c FROM Enrollment__c WHERE Training_Offer__c IN :scheduleIds]; //main enrollments

        for (Offer__c sch : scheduleList) {
            for (Enrollment__c enr : enrollmentList) {
                if (sch.Id == enr.Training_Offer__c) {
                    enr.Valid_From_for_email__c = sch.Valid_From__c;
                    enr.Valid_To_for_email__c = sch.Valid_To__c;
                    enr.Payment_Deadline_for_email__c = sch.Payment_Deadline__c;
                    enr.University_City_in_Vocative_for_email__c = sch.University_City_in_Vocative__c;
                    enr.Training_Administrator_for_email__c = sch.Training_Offer_from_Schedule__r.Training_Administrator__c;
                    enr.Street_for_email__c = sch.Street__c;
                    enr.City_for_email__c = sch.City__c;
                    enr.Bank_Account_No_for_email__c = sch.Bank_Account_No__c;
                    enr.Training_Administrator_Email_for_email__c = sch.Training_Offer_from_Schedule__r.Training_Administrator__r.Email;
                    enr.University_Phone_for_email__c = sch.Training_Offer_from_Schedule__r.University_from_Training__r.Phone;
                }
            }
        }

        try {
            update enrollmentList;
        } catch(Exception ex) {
            ErrorLogger.log(ex);
        }
    }


    /* Wojciech Słodziak */
    // Desc: update Enrollment_Value__c on Training enrollment when new Participant_Enrollment_Result is inserted
    public static void updateTrainingEnrollmentsValue(Set<Id> trainingEnrollmentsToUpdateValue) {
        List<String> nonCumulativeStatusesList = new List<String> { CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS, CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST };

        List<Enrollment__c> enrollmentList = [SELECT Id, Enrollment_Value__c 
                                              FROM Enrollment__c 
                                              WHERE Id IN :trainingEnrollmentsToUpdateValue 
                                              AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)];

        List<Enrollment__c> participantERList = [SELECT Id, Enrollment_Training_Participant__c, Enrollment_Value__c 
                                                 FROM Enrollment__c 
                                                 WHERE Enrollment_Training_Participant__c IN :trainingEnrollmentsToUpdateValue 
                                                 AND Status__c NOT IN :nonCumulativeStatusesList
                                                 AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)];
        
        //match
        for (Enrollment__c en : enrollmentList) {
            en.Enrollment_Value__c = 0;
            for (Enrollment__c per : participantERList) {
                if (en.Id == per.Enrollment_Training_Participant__c && per.Enrollment_Value__c != null) {
                    en.Enrollment_Value__c += per.Enrollment_Value__c;
                }
            }
        }

        try {
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }        
    }

    /* Beniamin Cholewa */
    // Potential duplicate participants detection
    public static void participantDuplicateDetection(List<Enrollment__c> potentialDuplicateParticipants, Set<Id> potentialDuplicateMainSchedulesIds) {
        List<Enrollment__c> allEnrollmentsToCheck = [SELECT Id, Training_Offer_for_Participant__c, Participant__c 
                                                     FROM Enrollment__c 
                                                     WHERE Training_Offer_for_Participant__c IN :potentialDuplicateMainSchedulesIds
                                                     AND Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION];

        List<Enrollment__c> enrollmentsToUpdate = new List<Enrollment__c>();
        
        for (Enrollment__c newEnrollment : potentialDuplicateParticipants) {
            for (Enrollment__c existingEnrollment : allEnrollmentsToCheck) {
                if (newEnrollment.Training_Offer_for_Participant__c == existingEnrollment.Training_Offer_for_Participant__c
                    && newEnrollment.Participant__c == existingEnrollment.Participant__c) {
                    if(!CommonUtility.isLightningEnabled()) {
                        newEnrollment.addError(' '+ Label.msg_error_duplicateParticipantOnEnrollment + ': <a target="_blank" href="/' 
                            + existingEnrollment.Id  + '">' + 'Link' + '</a>', false);
                    } else {
                        newEnrollment.addError(' '+ Label.msg_error_duplicateParticipantOnEnrollment + ': ' 
                            + existingEnrollment.Id, false);
                    }

                } else {
                    enrollmentsToUpdate.add(newEnrollment);
                }
            }
        }
    }    

    /* Beniamin Cholewa */
    // formula for assigning 'Reserve list' status when necessary
    public static void updateParticipantStatusToReserveList(List<Enrollment__c> participantsToCheckReserveList) {
        Map<Id, Enrollment__c> participantsMap = new Map<Id, Enrollment__c>();
        Map<Id, Set<Id>> participantsOnSchedulesMap = new Map<Id, Set<Id>>();

        for (Enrollment__c en : participantsToCheckReserveList) {
            participantsMap.put(en.Training_Offer_for_Participant__c, en);
        }
        
        Map<Id, Offer__c> trainingSchedulesIdsMap = new Map<Id, Offer__c>([
            SELECT Id, Number_Of_Seats__c, Launched__c 
            FROM Offer__c 
            WHERE Id in : participantsMap.keySet()
        ]);
        
        
        List<Enrollment__c> childEnrollments = [
            SELECT Id, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Training_Offer_for_Participant__c in : trainingSchedulesIdsMap.keySet()
            AND Status__c != : CommonUtility.ENROLLMENT_STATUS_RESIGNATION
        ];

        for (Enrollment__c en : childEnrollments) {
            Set<Id> idList;
            if (participantsOnSchedulesMap.containsKey(en.Training_Offer_for_Participant__c)) {
                idList = participantsOnSchedulesMap.get(en.Training_Offer_for_Participant__c);
            } else {
                idList = new Set<Id>();
            }
            idList.add(en.Id);
            participantsOnSchedulesMap.put(en.Training_Offer_for_Participant__c, idList);
        }

        for (Enrollment__c participant : participantsToCheckReserveList) {
            if (!participantsOnSchedulesMap.containsKey(participant.Training_Offer_for_Participant__c)) {
                participantsOnSchedulesMap.put(participant.Training_Offer_for_Participant__c, new Set<Id>());
            }
        }

        for (Enrollment__c participant : participantsToCheckReserveList) {
            Set<Id> idList = participantsOnSchedulesMap.get(participant.Training_Offer_for_Participant__c);
            if (trainingSchedulesIdsMap.get(participant.Training_Offer_for_Participant__c).Number_Of_Seats__c
                > idList.size()) {
                idList.add(participant.Training_Offer_for_Participant__c);
                participantsOnSchedulesMap.put(participant.Training_Offer_for_Participant__c, idList);
                
            } else if (trainingSchedulesIdsMap.get(participant.Training_Offer_for_Participant__c).Launched__c) {
                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS;
            } else {
                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST;
            }
        }
    }

}