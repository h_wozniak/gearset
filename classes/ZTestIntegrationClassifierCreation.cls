@isTest
private class ZTestIntegrationClassifierCreation {

	@isTest static void test_receiveClassifierInformation() {  
		Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaign(null, true);

        String body = '[{"crm_campaign_id":"' + campaign.Id + '","external_campaign_id":null,"classifiers":[{"id":"123","name":"Nowy","is_recall":false,"is_closing":false,"answered":false,"failed":false,"no_answer":false,"busy":false,"mail":false}]}]';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        IntegrationClassifierCreation.receiveClassifierInformation();
        Test.stopTest();
	}

	@isTest static void test_receiveClassifierInformation_incorrectJSON() {  
		Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaign(null, true);

        String body = '{"crm_campaign_id":"' + campaign.Id + '","external_campaign_id":null,"classifiers":[{"id":"123","name":"Nowy","is_recall":false,"is_closing":false,"answered":false,"failed":false,"no_answer":false,"busy":false,"mail":false}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationClassifierCreation.receiveClassifierInformation();
        }
        catch (Exception e) {
        	System.assert(e.getMessage().contains(IntegrationError.getErrorJSONBlobByCode(601).toString() ));
        }
        Test.stopTest();
	}
}