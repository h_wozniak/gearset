/**
* @author       Wojciech Słodziak
* @description  Class for storing methods related to Target__c object
**/

public without sharing class TargetManager {


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- VALIDATIONS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    // method for checking if Total Target Realization of Subtargets is contained in Main Target - Target Realization 
    public static void checkIfTargetContainsSubtargets(List<Target__c> subtargetList) {
        Set<Id> mainTargetIds = new Set<Id>();
        Set<Id> subtargetIds = new Set<Id>();
        for (Target__c subtarget : subtargetList) {
            mainTargetIds.add(subtarget.Study_Target_from_Subtarget__c);
            subtargetIds.add(subtarget.Id);
        }

        List<Target__c> mainTargetList = [
            SELECT Id, Target_Realization__c,
                (SELECT Id, Target_Realization__c
                FROM Subtargets__r
                WHERE Id NOT IN :subtargetIds)
            FROM Target__c
            WHERE Id IN :mainTargetIds
        ];

        for (Target__c mainTarget : mainTargetList) {
            Decimal totalSubtargetTargetRealization = 0;

            for (Target__c exisitngSubtarget : mainTarget.Subtargets__r) {
                totalSubtargetTargetRealization += exisitngSubtarget.Target_Realization__c;
            }

            for (Target__c newSubtarget : subtargetList) {
                if (newSubtarget.Study_Target_from_Subtarget__c == mainTarget.Id) {
                    totalSubtargetTargetRealization += newSubtarget.Target_Realization__c;

                    if (totalSubtargetTargetRealization > mainTarget.Target_Realization__c) {
                        newSubtarget.addError(' '+ Label.msg_error_SubtargetRealizationOverMax + ' ' + totalSubtargetTargetRealization + 
                            ' (max: ' + mainTarget.Target_Realization__c + ').');
                    }
                }
            }
        }
    }

    // method for checking if Subtarget Applies from / to dates are not overlapping
    public static void checkIfSubtargetDatesOverlap(List<Target__c> subtargetList) {
        Set<Id> mainTargetIds = new Set<Id>();
        for (Target__c subtarget : subtargetList) {
            mainTargetIds.add(subtarget.Study_Target_from_Subtarget__c);
        }

        List<Target__c> subtargetsToCheckAgainst = [
            SELECT Id, Study_Target_from_Subtarget__c, Applies_from__c, Applies_to__c
            FROM Target__c
            WHERE Study_Target_from_Subtarget__c IN :mainTargetIds
        ];

        subtargetsToCheckAgainst.addAll(subtargetList);

        for (Target__c subtargetTCA : subtargetsToCheckAgainst) {
            for (Target__c newSubtarget : subtargetList) {
                if (subtargetTCA.Study_Target_from_Subtarget__c == newSubtarget.Study_Target_from_Subtarget__c
                    && subtargetTCA.Id != newSubtarget.Id
                    && subtargetTCA !== newSubtarget
                    && ((
                            newSubtarget.Applies_from__c >= subtargetTCA.Applies_from__c 
                            && newSubtarget.Applies_from__c <= subtargetTCA.Applies_to__c
                        ) || (
                            newSubtarget.Applies_to__c >= subtargetTCA.Applies_from__c 
                            && newSubtarget.Applies_to__c <= subtargetTCA.Applies_to__c
                        ) || (
                            newSubtarget.Applies_from__c < subtargetTCA.Applies_from__c 
                            && newSubtarget.Applies_to__c > subtargetTCA.Applies_to__c
                        )
                    )
                ) {
                    newSubtarget.addError(Label.msg_error_SubtargetDatesOverlap);
                }
            }
        }
    }

    // method for checking if Team Target for specific Team is already added to Main Target
    public static void checkIfTeamTargetAlreadyExists(List<Target__c> teamTargetList) {
        Set<Id> mainTargetIds = new Set<Id>();
        for (Target__c teamTarget : teamTargetList) {
            mainTargetIds.add(teamTarget.Target_from_Team_Target__c);
        }

        List<Target__c> teamTargetsToCheckAgainst = [
            SELECT Id, Target_from_Team_Target__c, Team__c
            FROM Target__c
            WHERE Target_from_Team_Target__c IN :mainTargetIds
        ];

        teamTargetsToCheckAgainst.addAll(teamTargetList);

        for (Target__c teamTargetTCA : teamTargetsToCheckAgainst) {
            for (Target__c newTeamTarget : teamTargetList) {
                if (teamTargetTCA.Target_from_Team_Target__c == newTeamTarget.Target_from_Team_Target__c
                    && teamTargetTCA.Id != newTeamTarget.Id
                    && teamTargetTCA !== newTeamTarget
                    && teamTargetTCA.Team__c == newTeamTarget.Team__c) {
                    newTeamTarget.Team__c.addError(Label.msg_error_TeamTargetAlreadyExists);
                }
            }
        }
    }

}