/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to retrieve arrears from Experia
**/

/**
*   To initiate the RetrieveArrearsSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 1:00 ---
*   System.schedule('RetrieveArrearsSchedule', '0 0 1 * * ? *', new RetrieveArrearsSchedule());
**/


global class RetrieveArrearsSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new RetrieveArrearsBatch(), 200);
    }
}