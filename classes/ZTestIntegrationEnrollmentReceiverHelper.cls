@isTest
private class ZTestIntegrationEnrollmentReceiverHelper {

	private static String prepareJSON() {
		return '{"updating_system":"ZPI","updating_system_contact_id":"string","first_name":"Zdziszek","middle_name":"string","last_name":"Kocur","family_name":"Smith","father_name":"David","mother_name":"Mom","gender":"Male","personal_id":"99101010535","birthdate":"1999-10-10","place_of_birth":"Poland","country_of_birth":"Poland","country_of_origin":"Poland","nationality":["polish"],"residence_document":{"type":"Visa","validity_date":"2016-10-10"},"polish_card":true,"polish_card_validity_date":"2017-10-10","identity_document":{"type":"Identity Card","identity_document_number":"ACT956616","issue_country":"Poland"},"health_insurance_validity_date":"2030-10-10","teb_graduate":true,"wsb_graduate":{"school":"Wrocław"},"career":[{"company_name":"Enxoo","position":"Software Engineer","date_start":"2015-10-10","date_end":"2020-10-10","address":{"street":"Ruska 3","city":"Wrocław","postal_code":"50-243","country":"Poland"}}],"finished_trainings":[{"name":"string","class_year":"1234"}],"addresses":[{"type":"Home","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Starachowice","street":"Lelewela"},{"type":"Mailing","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Wrocław","street":"Kościuszki"}],"phones":[{"type":"Default","phoneNumber":"123456789"}],"email":"sedi1ster@hotmail.com","consents":[{"name":"Marketing","value":true}],"enrollment_data":{"enrollment_language" : "pl", "enrollment_id_zpi":"string","product_name":"TOR_I_10_2016_BW_LIC_WEEK","tuition_system":"Guaranteed Graded Tuition","number_of_installments":12,"discount_code":null,"company_for_discount":{"name":"string","tax_id":"string"},"selected_languages":[{"code":"German","level":"Basic"}],"university":{"name":"WSB Wrocław","city":"Wrocław","finished_course":"Archeologia","graduation_year":"2015","diploma":{"status":"Diploma acquired","defense_date":"2015-10-10","diploma_number":"123456","issue_country":"Poland","issue_city":"Wrocław","issue_date":"2016-10-10","title":"Master"}},"secondary_school":{"name":"Moja najlepsza szkoła","type":"High School","city":"Starachowice","a_level_status":"Passed","a_level_id":"Moje id","a_level_issue_date":"2015-10-10","examination_committee_name":"Regional Examination Commission in Jaworzno","certificate_city":"Starachowice","certificate_country":"Poland","graduation_year":"2015-10-10"}}}';
	}

	private static String prepareJSON(String contactId, String offerName, String finishedUniversityName) {
		return '{"updating_system":"ZPI","updating_system_contact_id":"' + contactId + '","first_name":"Zdziszek","middle_name":"string","last_name":"Kocur","family_name":"Smith","father_name":"David","mother_name":"Mom","gender":"Male","personal_id":"99101010535","birthdate":"1999-10-10","place_of_birth":"Poland","country_of_birth":"Poland","country_of_origin":"Poland","nationality":["polish"],"residence_document":{"type":"Visa","validity_date":"2016-10-10"},"polish_card":true,"polish_card_validity_date":"2017-10-10","identity_document":{"type":"Identity Card","identity_document_number":"ACT956616","issue_country":"Poland"},"health_insurance_validity_date":"2030-10-10","teb_graduate":true,"wsb_graduate":{"school":"Wrocław"},"career":[{"company_name":"Enxoo","position":"Software Engineer","date_start":"2015-10-10","date_end":"2020-10-10","address":{"street":"Ruska 3","city":"Wrocław","postal_code":"50-243","country":"Poland"}}],"finished_trainings":[{"name":"string","class_year":"1234"}],"addresses":[{"type":"Other","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Starachowice","street":"Lelewela"},{"type":"Mailing","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Wrocław","street":"Kościuszki"}],"phones":[{"type":"Default","phoneNumber":"123456789"}],"email":"sedi1ster@hotmail.com","consents":[{"name":"Marketing","value":true}],"enrollment_data":{"enrollment_language" : "pl", "enrollment_id_zpi":"string","product_name":"' + offerName + '","tuition_system":"Guaranteed Graded Tuition","number_of_installments":12,"discount_code":null,"company_for_discount":{"name":"string","tax_id":"string"},"selected_languages":[{"code":"English","level":"Basic"}],"university":{"name":"' + finishedUniversityName + '","city":"Wrocław","finished_course":"Archeologia","graduation_year":"2015","diploma":{"status":"Diploma acquired","defense_date":"2015-10-10","diploma_number":"123456","issue_country":"Poland","issue_city":"Wrocław","issue_date":"2016-10-10","title":"Master"}},"secondary_school":{"name":"Moja najlepsza szkoła","type":"High School","city":"Starachowice","a_level_status":"Passed","a_level_id":"Moje id","a_level_issue_date":"2015-10-10","examination_committee_name":"Regional Examination Commission in Jaworzno","certificate_city":"Starachowice","certificate_country":"Poland","graduation_year":"2015-10-10"}}}';
	}

	private static String prepareJSON(String offerName, String finishedUniversityName) {
		return '{"updating_system":"ZPI","updating_system_contact_id":"string","first_name":"Zdziszek","middle_name":"string","last_name":"Kocur","family_name":"Smith","father_name":"David","mother_name":"Mom","gender":"Male","personal_id":"99101010535","birthdate":"1999-10-10","place_of_birth":"Poland","country_of_birth":"Poland","country_of_origin":"Poland","nationality":["polish"],"residence_document":{"type":"Visa","validity_date":"2016-10-10"},"polish_card":true,"polish_card_validity_date":"2017-10-10","identity_document":{"type":"Identity Card","identity_document_number":"ACT956616","issue_country":"Poland"},"health_insurance_validity_date":"2030-10-10","teb_graduate":true,"wsb_graduate":{"school":"Wrocław"},"career":[{"company_name":"Enxoo","position":"Software Engineer","date_start":"2015-10-10","date_end":"2020-10-10","address":{"street":"Ruska 3","city":"Wrocław","postal_code":"50-243","country":"Poland"}}],"finished_trainings":[{"name":"string","class_year":"1234"}],"addresses":[{"type":"Other","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Starachowice","street":"Lelewela"},{"type":"Mailing","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Wrocław","street":"Kościuszki"}],"phones":[{"type":"Default","phoneNumber":"123456789"}],"email":"sedi1ster@hotmail.com","consents":[{"name":"Marketing","value":true}],"enrollment_data":{"enrollment_language":"pl", "enrollment_id_zpi":"string","product_name":"' + offerName + '","tuition_system":"Guaranteed Graded Tuition","number_of_installments":12,"discount_code":null,"company_for_discount":{"name":"string","tax_id":"string"},"selected_languages":[{"code":"English","level":"Basic"}],"university":{"name":"' + finishedUniversityName + '","city":"Wrocław","finished_course":"Archeologia","graduation_year":"2015","diploma":{"status":"Diploma acquired","defense_date":"2015-10-10","diploma_number":"123456","issue_country":"Poland","issue_city":"Wrocław","issue_date":"2016-10-10","title":"Master"}},"secondary_school":{"name":"Moja najlepsza szkoła","type":"High School","city":"Starachowice","a_level_status":"Passed","a_level_id":"Moje id","a_level_issue_date":"2015-10-10","examination_committee_name":"Regional Examination Commission in Jaworzno","certificate_city":"Starachowice","certificate_country":"Poland","graduation_year":"2015-10-10"}}}';
	}

	@isTest static void test_validateJSON() {
		String validJSON = prepareJSON();

		Test.startTest();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Boolean errorOccured = false;
		try {
			IntegrationEnrollmentReceiverHelper.validateJSON(parsedJSON);
		}
		catch (Exception e) {
			errorOccured = true;
		}

		System.assert(!errorOccured);

		Test.stopTest();
	}

	@isTest static void test_dateToString_valid() { 
		Test.startTest();
		Date result = IntegrationEnrollmentReceiverHelper.dateToString('2015-05-02');
		Test.stopTest();

		System.assertEquals(result, Date.newInstance(2015, 5, 2));
	}

	@isTest static void test_dateToString_Invalid() { 
		Test.startTest();
		Date result = IntegrationEnrollmentReceiverHelper.dateToString(null);
		Test.stopTest();

		System.assertEquals(result, null);
	}

	@isTest static void test_fillEnrollmentInfo() { 
		Enrollment__c studyEnr = new Enrollment__c();
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		String validJSON = prepareJSON();

		Test.startTest();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Enrollment__c studyEnrWithData = IntegrationEnrollmentReceiverHelper.fillEnrollmentInfo(String.valueOf(candidate.Id), studyEnr, parsedJSON, courseOffer);

		Test.stopTest();

		System.assertEquals(studyEnrWithData.Candidate_Student__c, candidate.id);
		System.assertEquals(studyEnrWithData.Company_for_Discount_Name__c, 'string');
	}

	@isTest static void test_updateFinishedUniversity() { 
		Account university = ZDataTestUtility.createUniversity(null, true);
		String validJSON = prepareJSON('', university.Name);
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Test.startTest();
		Qualification__c finishedUniversity = IntegrationEnrollmentReceiverHelper.updateFinishedUniversity(CommonUtility.OFFER_DEGREE_II, candidate, parsedJSON.enrollment_data.university);
		Test.stopTest();

		System.assertEquals(finishedUniversity.Status__c, 'Diploma acquired');
		System.assertEquals(finishedUniversity.City_of_Diploma_Issue__c, 'Wrocław');
	}

	@isTest static void test_updateCareerInformation() { 
		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Test.startTest();
		List<Qualification__c> careers = IntegrationEnrollmentReceiverHelper.updateCareerInformation(candidate, parsedJSON.career);
		Test.stopTest();

		System.assertEquals(careers.size(), 1);

		System.assertEquals(careers.get(0).Position__c, 'Software Engineer');
	}

	@isTest static void test_updateAddresses() {
		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Test.startTest();
		Contact candidateAfterIntegration = IntegrationEnrollmentReceiverHelper.updateAddresses(candidate, parsedJSON.addresses);
		Test.stopTest();

		System.assertNotEquals(candidateAfterIntegration, null);

		System.assertEquals(candidateAfterIntegration.MailingCountry, 'Poland');
		System.assertEquals(candidateAfterIntegration.OtherStreet, 'Lelewela');
	}

	@isTest static void test_updatePhones() {
		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Test.startTest();
		Contact candidateAfterIntegration = IntegrationEnrollmentReceiverHelper.updatePhones(candidate, parsedJSON.phones);
		Test.stopTest();

		System.assertNotEquals(candidateAfterIntegration, null);

		System.assertEquals(candidateAfterIntegration.Phone, '123456789');
		System.assertEquals(candidateAfterIntegration.MobilePhone, null);
	}

	@isTest static void test_updateNationalities() {
		List<String> nationalities = new List<String>();
		nationalities.add('polskie');
		nationalities.add('rosyjskie');

		Test.startTest();
		String result = IntegrationEnrollmentReceiverHelper.updateNationalities(nationalities);
		Test.stopTest();

		//System.assertEquals(result, 'polish;russian');
	}

	@isTest static void test_updateSelectedLanguages_withoutLanguageOnoffer() {
		Enrollment__c studyEnr = new Enrollment__c();
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Test.startTest();
		List<Enrollment__c> selectedLanguages = IntegrationEnrollmentReceiverHelper.updateSelectedLanguages(studyEnr.Id, courseOffer, parsedJSON.enrollment_data.selected_languages);
		Test.stopTest();

		System.assertEquals(selectedLanguages.size(), 0);
	}

	@isTest static void test_updateSelectedLanguages_withLanguageOnoffer() {
		Enrollment__c studyEnr = new Enrollment__c();
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		Offer__c languageOffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = courseOffer.Id, Language__c = 'German'), true);

		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		Test.startTest();
		List<Enrollment__c> selectedLanguages = IntegrationEnrollmentReceiverHelper.updateSelectedLanguages(studyEnr.Id, courseOffer, parsedJSON.enrollment_data.selected_languages);
		Test.stopTest();

		System.assertEquals(selectedLanguages.size(), 1);
		System.assertEquals(selectedLanguages.get(0).Language_Level__c, 'Basic');
	}

	@isTest static void test_getCandidateRecord() {
		Enrollment__c studyEnr = new Enrollment__c();
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		String validJSON = prepareJSON();
		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);
		
		Test.startTest();
		Contact candidateAfterIntegration = IntegrationEnrollmentReceiverHelper.getCandidateRecord(candidate, parsedJSON);
		Test.stopTest();

		System.assertNotEquals(candidateAfterIntegration, null);
	}

	@isTest static void test_createEnrollment_withoutContact() { 
        Catalog__c con1 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1,
                Consent_ADO_API_Name__c = 'Consent_Marketing_ADO__c',
                Consent_API_Name__c = 'Consent_Marketing__c'
        ), true);
        Catalog__c con2 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_2,
                Consent_ADO_API_Name__c = 'Consent_Electronic_Communication_ADO__c',
                Consent_API_Name__c = 'Consent_Electronic_Communication__c'
        ), true);
        Catalog__c con3 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3,
                Consent_ADO_API_Name__c = 'Consent_PUCA_ADO__c',
                Consent_API_Name__c = 'Consent_PUCA__c'
        ), true);
        Catalog__c con4 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_4,
                Consent_ADO_API_Name__c = 'Consent_Direct_Communications_ADO__c',
                Consent_API_Name__c = 'Consent_Direct_Communications__c'
        ), true);
        Catalog__c con5 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_5,
                Consent_ADO_API_Name__c = 'Consent_Graduates_ADO__c',
                Consent_API_Name__c = 'Consent_Graduates__c'
        ), true);
        Catalog__c enr1 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_1,
                Consent_API_Name__c = 'Consent_Terms_of_Service_Acceptation__c'
        ), true);
        Catalog__c enr2 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_2,
                Consent_API_Name__c = 'Consent_Date_processing_for_contract__c'
        ), true);

		Enrollment__c studyEnr = new Enrollment__c();
		Account university = ZDataTestUtility.createUniversity(null, true);
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		Offer__c courseOfferAfterInsert = [
			SELECT Id, Name
			FROM Offer__c
			WHERE Id = :courseOffer.Id
		];

		String validJSON = prepareJSON(courseOfferAfterInsert.name, university.Name);

		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		List<Offer__c> productList = [
            SELECT Id, Name, University_Name__c, Course_Offer_from_Specialty__c 
            FROM Offer__c 
            WHERE Name = :parsedJson.enrollment_data.product_name
        ];

        System.assertEquals(productList.size(), 1);
		
		Test.startTest();
		IntegrationEnrollmentReceiverHelper.createEnrollment(parsedJSON, validJSON);
		Test.stopTest();
	}

	@isTest static void test_createEnrollment_withContact() {
        ZDataTestUtility.prepareCatalogData(true, true);

		Account university = ZDataTestUtility.createUniversity(null, true);
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		Qualification__c workExperience = ZDataTestUtility.createWorkExperience(new Qualification__c(Work_Experience__c = candidate.Id), true);

		Offer__c languageOffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = courseOffer.Id, Language__c = 'German'), true);

		Test.startTest();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);

		Offer__c courseOfferAfterInsert = [
			SELECT Id, Name
			FROM Offer__c
			WHERE Id = :courseOffer.Id
		];

		Account universityAfterInsert = [
			SELECT Id, Name
			FROM Account
			WHERE Id = :university.Id
		];

		Test.stopTest();
		
		String validJSON = prepareJSON(candidate.Id, courseOfferAfterInsert.name, universityAfterInsert.Name);

		IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = IntegrationEnrollmentReceiver.parse(validJSON);

		List<Offer__c> productList = [
            SELECT Id, Name, University_Name__c, Course_Offer_from_Specialty__c 
            FROM Offer__c 
            WHERE Name = :parsedJson.enrollment_data.product_name
        ];

        System.assertEquals(productList.size(), 1);
		
		//Test.startTest();
		IntegrationEnrollmentReceiverHelper.createEnrollment(parsedJSON, validJSON);
	}
}