public with sharing class NewContactController {

	public List<SelectOption> rt {get; set;}
	public String chosenRT {get; set;}

	public NewContactController(apexPages.StandardController controller) {
		rt = new List<SelectOption>();	
		List<RecordType> contactsRT = [SELECT Id, DeveloperName, toLabel(Name), SObjectType FROM RecordType WHERE SObjectType = 'Contact'
										  AND (DeveloperName = : CommonUtility.CONTACT_RT_CONTACTPERSON 
										   OR DeveloperName = : CommonUtility.CONTACT_RT_CANDIDATE
										   OR DeveloperName = : CommonUtility.CONTACT_RT_PROSPECT ) ];
		for(RecordType cRT : contactsRT){
			rt.add(new SelectOption(cRT.Id, cRT.Name));
		}	
	}
	public PageReference Back(){
		return new PageReference('/003');
	}

	public PageReference Next(){
		return new PageReference('/003/e?retURL=%2F003%2Fo&nooverride=1&RecordType='+chosenRT+'&ent=Contact');
	}
}