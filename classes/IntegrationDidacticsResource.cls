/**
*   @author         Sebastian Łasisz
*   @description    Rest Resource class for receiving acknowledgements for Enrollment transfers to Experia.
**/

@RestResource(urlMapping = '/didactics-update/*')
global without sharing class IntegrationDidacticsResource {


    @HttpPut
    global static void receiveEnrollmentInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;     
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String enrollmentId = RestContext.request.params.get('recrutation_id');
        if (enrollmentId == null || enrollmentId == '') {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(605);
            return ;
        }

        Enrollment__c didactcsToUpdate;
        try {
            didactcsToUpdate = [
                SELECT Id, Didactics_Status__c, Portal_login__c, Current_Semester__c, Current_Semester_change_Date__c, 
                       Bank_Account_No_for_email__c, Course_or_Specialty_from_EduAgr__c, Student_no__c, Trade_Name__c,
                       Degree__c, Kind__c, Mode__c, Number_of_Semesters_EducationalArg__c, Student_from_Educational_Agreement__c
                FROM Enrollment__c
                WHERE Id = :enrollmentId
            ];
        }
        catch (Exception e) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(1000);            
            return ;
        }

        String jsonBody = req.requestBody.toString();        
        IntegrationManager.Didactics_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            ErrorLogger.log(ex);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
            return ;
        }

        try {
            validateJSON(parsedJson);
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(800, e.getMessage()));
            res.statusCode = 400;           
            return ;
        }

        if (parsedJson != null) {
            try {
                didactcsToUpdate.Didactics_Status__c = parsedJson.didactics_status;
                didactcsToUpdate.Portal_login__c = parsedJson.portal_login;
                didactcsToUpdate.Current_Semester__c = Decimal.valueOf(parsedJson.current_semester);
                didactcsToUpdate.Current_Semester_change_Date__c = stringToDate(parsedJson.current_semester_chg_date);
                didactcsToUpdate.Bank_Account_No_for_email__c = parsedJson.bank_acc_nbr;

                if (parsedJson.product_name != null && parsedJson.product_name != '') {
                    try {
                        Offer__c product = [SELECT Id, Name FROM Offer__c WHERE Name =: parsedJson.product_name];
                        didactcsToUpdate.Course_or_Specialty_from_EduAgr__c = product.Id;
                    }
                    catch (Exception e) {
                        res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(1001, parsedJson.product_name));
                        res.statusCode = 400;           
                        return ;
                    }
                }            
                else {    
                    didactcsToUpdate.Trade_Name__c = parsedJson.trade_name;
                    didactcsToUpdate.Degree__c = parsedJson.degree;
                    didactcsToUpdate.Kind__c = parsedJson.kind;
                    didactcsToUpdate.Mode__c = parsedJson.mode;
                }
                
                didactcsToUpdate.Student_no__c = parsedJson.student_nbr;
                didactcsToUpdate.Number_of_Semesters_EducationalArg__c = parsedJson.number_of_semesters;
                CommonUtility.preventCatchingErrorOnEnrUpdate = true;
                update didactcsToUpdate;

                if (parsedJson.didactics_status == CommonUtility.ENROLLMENT_DIDACTICS_STATUS_FINISHED_EDUCATION) {
                    Qualification__c univeristyOnContact = [
                        SELECT Id, WSB_Educational_Agreement__c, Gained_Title__c, Diploma_Number__c, Country_of_Diploma_Issue__c, Diploma_Issue_Date__c, Defense_Date__c, Status__c
                        FROM Qualification__c
                        WHERE WSB_Educational_Agreement__c = :didactcsToUpdate.Id
                        LIMIT 1
                    ];

                    if (parsedJson.finished_university != null) {
                        IntegrationManager.FinishedUniversity_JSON finished_university = parsedJson.finished_university;
                        univeristyOnContact.Gained_Title__c = finished_university.obtained_title;
                        univeristyOnContact.Diploma_Number__c = finished_university.diploma_number;
                        univeristyOnContact.Country_of_Diploma_Issue__c = finished_university.country_of_diploma_issue;
                        univeristyOnContact.Diploma_Issue_Date__c = stringToDate(finished_university.diploma_issue_date);
                        univeristyOnContact.Defense_Date__c = stringToDate(finished_university.defense_date);
                        if (finished_university.status == null || finished_university.status == '') {
                             univeristyOnContact.Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_NO_DATA;
                        }
                        else {
                            univeristyOnContact.Status__c = finished_university.status;
                        }

                        update univeristyOnContact;
                    }

                    Contact candidate = [
                        SELECT Graduate__c 
                        FROM Contact 
                        WHERE Id = :didactcsToUpdate.Student_from_Educational_Agreement__c
                    ];

                    candidate.Graduate__c = true;
                    update candidate;
                }


                res.statusCode = 200;
            }
            catch (Exception e) {
                if (e.getMessage().contains('UNABLE_TO_LOCK_ROW')) {
                    ErrorLogger.logAndMsg(e, 'Sending Didactics to DLQ for retry in 2 minutes. Colliding with Payment Schedule Update');
                }
                else {
                    ErrorLogger.log(e);
                }

                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600, e.getMessage());
            }
        }
    }

    private static void validateJSON(IntegrationManager.Didactics_JSON parsedJson) {
        if (parsedJson.didactics_status == null) {
            throw new InvalidFieldValueException('didactics_status');
        }
        if (parsedJson.trade_name == null) {
            throw new InvalidFieldValueException('trade_name');
        }
    }
    
    private static IntegrationManager.Didactics_JSON parse(String jsonBody) {
        return (IntegrationManager.Didactics_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.Didactics_JSON.class);
    }

    private static Date stringToDate(String stringDate) {
        if (stringDate != null) {
            List<String> dateValues = stringDate.split('-');
            return Date.newInstance(Integer.valueOf(dateValues.get(0)), Integer.valueOf(dateValues.get(1)), Integer.valueOf(dateValues.get(2)));
        }
        return null;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- EXCEPTION CLASSES ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class InvalidFieldValueException extends Exception {}
}