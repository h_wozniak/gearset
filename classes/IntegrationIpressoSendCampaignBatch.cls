/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends segments or tags to iPresso
**/

global class IntegrationIpressoSendCampaignBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    Id campaignId;
    String criteriaType;
    Marketing_Campaign__c iPressoCampaign;
    Integer errorCounter = 0;
    Boolean updateNotCreate;

    public IntegrationIpressoSendCampaignBatch(Id campaignId, String criteriaType) {
        this.campaignId = campaignId;
        this.criteriaType = criteriaType;

        iPressoCampaign = [
                SELECT Id, iPresso_Criteria_Type__c, iPresso_Crireria_Type_Value__c, iPresso_Crireria_Segment_Lifetime__c, TECH_Sent_To_Ipresso__c, Segment_External_Id__c
                FROM Marketing_Campaign__c
                WHERE Id = :campaignId
        ];

        updateNotCreate = ipressoCampaign.TECH_Sent_To_Ipresso__c;
        iPressoCampaign.Synchronization_Status__c = CommonUtility.SYNCSTATUS_INPROGRESS;

        try {
            update iPressoCampaign;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Synchronization_Status__c, Campaign_Contact__c
                FROM Marketing_Campaign__c
                WHERE Campaign_Member__c = :campaignId
                AND Synchronization_Status__c != :CommonUtility.SYNCSTATUS_FINISHED
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Marketing_Campaign__c> iPressoMembers) {
        iPressoCampaign = [
                SELECT Id, iPresso_Criteria_Type__c, iPresso_Crireria_Type_Value__c, iPresso_Crireria_Segment_Lifetime__c, TECH_Sent_To_Ipresso__c, Segment_External_Id__c
                FROM Marketing_Campaign__c
                WHERE Id = :campaignId
        ];

        Set<Id> contactIds = new Set<Id>();
        for (Marketing_Campaign__c iPressoMember : iPressoMembers) {
            contactIds.add(iPressoMember.Campaign_Contact__c);
        }

        List<Marketing_Campaign__c> iPressoContacts = [
                SELECT Id, iPressoId__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                AND Contact_from_iPresso__c IN :contactIds
                AND iPressoId__c != null
        ];

        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

        String jsonToSend;
        String endPoint;
        if (iPressoCampaign.iPresso_Criteria_Type__c == CommonUtility.MARKETING_CRITERIA_TYPE_TAGS) {
            jsonToSend = IntegrationIPressoCreateSegments.prepareIPressoContactListForTags(iPressoContacts, iPressoCampaign);
            endPoint = !updateNotCreate ? esb.Add_Tags__c : esb.Update_Tags__c;
        }
        else {
            jsonToSend = IntegrationIPressoCreateSegments.prepareIPressoContactListForSegment(iPressoContacts, iPressoCampaign, iPressoCampaign.Segment_External_Id__c);
            endPoint = (iPressoCampaign.Segment_External_Id__c == null) ? esb.Create_Segment__c : esb.Update_Segments__c;
        }

        HttpRequest httpReq = new HttpRequest();
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + endPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        httpReq.setBody(jsonToSend);

        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = httpRes.getStatusCode() == 200;
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoCreateSegments.class.getName()
                    + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + contactIds);
            errorCounter++;
        }

        IntegrationIPressoCreateSegments.updateSyncStatus_onFinish(iPressoMembers, success);

        if (success && criteriaType == CommonUtility.MARKETING_CRITERIA_TYPE_TAGS) {
            IntegrationIPressoCreateSegments.updateContacts_onFinish(contactIds, iPressoCampaign.iPresso_Crireria_Type_Value__c);
        }

        if (success && criteriaType == CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS) {
            IntegrationIPressoCreateSegments.SegmentResponse_JSON response = parse(httpRes.getBody());
            IntegrationIPressoCreateSegments.Data_JSON data = response.data;

            Marketing_Campaign__c campaign = new Marketing_Campaign__c(
                    Id = campaignId,
                    Segment_External_Id__c = data.id
            );

            try {
                update campaign;
            }
            catch (Exception e) {
                ErrorLogger.log(e);
            }
        }

        updateNotCreate = true;
    }

    global void finish(Database.BatchableContext BC) {
        if (errorCounter == 0 && criteriaType == CommonUtility.MARKETING_CRITERIA_TYPE_TAGS) {
            iPressoCampaign.TECH_Synchronized_All_Contacts__c = true;
        }

        if (errorCounter == 0 && criteriaType == CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS) {
            iPressoCampaign.Segment_Expiration_Date__c = System.now().addHours(Integer.valueOf(iPressoCampaign.iPresso_Crireria_Segment_Lifetime__c));
        }

        iPressoCampaign.TECH_Sent_To_Ipresso__c = true;
        iPressoCampaign.Synchronization_Status__c = CommonUtility.SYNCSTATUS_FINISHED;

        try {
            update iPressoCampaign;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    private static IntegrationIPressoCreateSegments.SegmentResponse_JSON parse(String jsonBody) {
        return (IntegrationIPressoCreateSegments.SegmentResponse_JSON) System.JSON.deserialize(jsonBody, IntegrationIPressoCreateSegments.SegmentResponse_JSON.class);
    }
}