global class FixBatch implements Database.Batchable<sObject> {
    Set<Id> toUpdaste = new Set<Id>();
    
    global FixBatch(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            select Id
            from Enrollment__c 
            where CreatedDate > 2018-03-25T13:49:04.000+0000
            and CreatedDate <= Today
            and Status__c IN ('Confirmed', 'Collection of documents', 'Documents collected')
            and Unenrolled_Status__c = null
            and Annex_or_Related_Enr__c = null
            //and RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            and Value_after_Discount__c = 0
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> eToPayments = new Set<Id>();
        for(Enrollment__c e : enrList) {
        eToPayments.add(e.Id);
        }
        toUpdaste.addAll(eToPayments);
        PaymentScheduleManager.generatePaymentSchedules(toUpdaste);
    }
    
    global void finish(Database.BatchableContext BC) {
        
           //PaymentScheduleManager.generatePaymentSchedules(toUpdaste);
        
    }
    
}