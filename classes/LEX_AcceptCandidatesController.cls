public with sharing class LEX_AcceptCandidatesController {
	
	String currentEnrollmentId {get;set;}	
	List<Enrollment__c> selectedCandidates {get;set;}
	List<Id> selectedIds {get;set;}
	Id parentId {get; set;}


	public LEX_AcceptCandidatesController(ApexPages.StandardSetController stdSetController) {
		stdSetController.addFields(new List<String>{'id'});
		selectedCandidates = (List<Enrollment__c>)stdSetController.getSelected();
		for(sObject enr : stdSetController.getRecords()) {
			System.debug(enr);
		}
		try {
			parentId = [SELECT Enrollment_Training_Participant__c FROM Enrollment__c WHERE Id IN :stdSetController.getRecords()][0].Id;
		} catch(Exception ex) {
			parentId = '';
			ErrorLogger.log(ex);
		}

		selectedIds = new List<Id>();
		for(Enrollment__c enr : selectedCandidates) {
			selectedIds.add(enr.Id);
		}
	}

	public String getSelectedIds() {
		return JSON.serialize(selectedIds);
	}

	public String getCurrentEnrollmentId() {
    	return parentId;
	}

	@AuraEnabled
	public static Boolean hasEditAccessList(List<Id> contacts) {
		return LEXServiceUtilities.hasEditAccessList(contacts);
	}
	@AuraEnabled
	public static Boolean markParticipantsAsAccepted(List<Id> contacts) {
		return TrainingWebservices.markParticipantsAsAccepted(contacts);
	}
}