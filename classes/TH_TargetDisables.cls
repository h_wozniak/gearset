/**
* @description  This class is a Trigger Handler for Target - field edit disabling
**/

public without sharing class TH_TargetDisables extends TriggerHandler.DelegateBase  {

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Target__c> oldTargets = (Map<Id, Target__c>)old;
        Map<Id, Target__c> newTargets = (Map<Id, Target__c>)o;
        Target__c oldTarget;
        Target__c newTarget;
        for (Id key : newTargets.keySet()) {
            oldTarget = oldTargets.get(key);
            newTarget = newTargets.get(key);

            /* Wojciech Słodziak */
            // disable editing of Target_from_EduAdv_Target__c Lookup field
            if (newTarget.Target_from_EduAdv_Target__c != oldTarget.Target_from_EduAdv_Target__c) {
                newTarget.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Target__c', 'Target_from_EduAdv_Target__c'));
            }

            /* Wojciech Słodziak */
            // disable editing of Study_Target_from_Subtarget__c Lookup field
            if (newTarget.Study_Target_from_Subtarget__c != oldTarget.Study_Target_from_Subtarget__c) {
                newTarget.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Target__c', 'Study_Target_from_Subtarget__c'));
            }

            /* Wojciech Słodziak */
            // disable editing of Target_from_Team_Target__c Lookup field
            if (newTarget.Target_from_Team_Target__c != oldTarget.Target_from_Team_Target__c) {
                newTarget.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Target__c', 'Target_from_Team_Target__c'));
            }

        }
    }

}