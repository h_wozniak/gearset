public with sharing class LEX_ContactStageBar {

    @AuraEnabled
    public static List<StatusWrapper> getStatusesList(Id recordId) {
        Map<String, String> fieldLabelMap = CommonUtility.getPicklistLabelMap(Schema.Contact.Statuses__c);
        Contact contact = [SELECT Statuses__c FROM Contact WHERE Id = :recordId OR AccountId = :recordId];
        List<StatusWrapper>  statusesList = new List<StatusWrapper> ();

        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_INTERESTED, fieldLabelMap, 'icons/interested.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_CANDIDATE, fieldLabelMap, 'icons/candidate.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_ACCEPTED, fieldLabelMap, 'icons/accepted.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_STUDENT, fieldLabelMap, 'icons/student.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_GRADUATE, fieldLabelMap, 'icons/graduateC.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_PARTICIPANT, fieldLabelMap, 'icons/ebook.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_FOREIGNER, fieldLabelMap, 'icons/foreignerchiny.png'));
        statusesList.add(new StatusWrapper(CommonUtility.CONTACT_STATUSES_DUPLICATE, fieldLabelMap, 'icons/duplicate2.png', true));

        for (StatusWrapper status : statusesList) {
            if (contact.Statuses__c != null && contact.Statuses__c.contains(status.value)) {
                status.isActive = true;
            }
        }

        return statusesList;
    }

    public class StatusWrapper {
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public String iconUrl { get; set; }
        @AuraEnabled public Boolean warning { get; set; }
        @AuraEnabled public Boolean isActive { get; set; }

        public StatusWrapper(String value, Map<String, String> fieldLabelMap, String iconUrl) {
            this.value = value;
            label = fieldLabelMap.get(value);
            this.iconUrl = iconUrl;
            isActive = false;
        }

        public StatusWrapper(String value, Map<String, String> fieldLabelMap, String iconUrl, Boolean warning) {
            this(value, fieldLabelMap, iconUrl);
            this.warning = warning;
        }
    }

}