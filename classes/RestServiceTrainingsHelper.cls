public with sharing class RestServiceTrainingsHelper {

    public static Id handleCompany(JSON2Apex jsonObj){
        Id accountId;
        List<Account> accountDuplicate = new List<Account>([SELECT Id, NIP__c, Name, Company_Size__c, Industry, ShippingStreet, ShippingPostalCode, ShippingCity FROM Account WHERE NIP__c =: jsonObj.nip]);
        if (accountDuplicate.isEmpty())
            accountId = createNewAccount(jsonObj);
        else
                accountId = overwriteDuplicateAccount(jsonObj, accountDuplicate.get(0));
        return accountId;
    }

    public static Id createNewAccount(JSON2Apex json){
        Account newCompany = new Account();
        newCompany.RecordTypeId = CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_COMPANY);
        newCompany.Name = json.companyName;
        newCompany.NIP__c = json.nip;
        newCompany.BillingCity = json.companyCity;
        newCompany.BillingStreet = json.companyStreet;
        newCompany.ShippingStreet = json.companyStreet;
        newCompany.BillingPostalCode = json.companyPostalCode;
        newCompany.ShippingPostalCode = json.companyPostalCode;
        newCompany.ShippingCity = json.companyCity;
        newCompany.Company_Size__c = getPicklistValues('Account', 'Company_Size__c', json.companySize);
        newCompany.Industry = getPicklistValues('Account', 'Industry', json.businessArea);
        try{
            insert newCompany;
        }
        catch(DmlException e){
            ErrorLogger.log(e);
            throw e;
        }
        return newCompany.Id;
    }

    private static Id overwriteDuplicateAccount(JSON2Apex json, Account accDuplicate){
        if (accDuplicate.ShippingCity == null || !accDuplicate.ShippingCity.equals(json.companyCity))
            accDuplicate.ShippingCity = json.companyCity;
        if (accDuplicate.ShippingStreet == null || !accDuplicate.ShippingStreet.equals(json.companyStreet))
            accDuplicate.ShippingStreet = json.companyStreet;
        if (accDuplicate.ShippingPostalCode == null || !accDuplicate.ShippingPostalCode.equals(json.companyPostalCode))
            accDuplicate.ShippingPostalCode = json.companyPostalCode;

        if (String.isEmpty(accDuplicate.Company_Size__c)) accDuplicate.Company_Size__c = getPicklistValues('Account', 'Company_Size__c', json.companySize);
        if (String.isEmpty(accDuplicate.Industry)) accDuplicate.Industry = getPicklistValues('Account', 'Industry', json.businessArea);
        try{
            update accDuplicate;
        }
        catch(DmlException e){
            ErrorLogger.log(e);
            throw e;
        }
        return accDuplicate.Id;
    }

    public static List<Contact> parseAttendees(JSON2Apex json, Id accountId, String entity){
        List<Contact> returnList = new List<Contact>();
        for(JSON2Apex.Attendees newCandidates: json.attendees){
            Contact contact = new Contact();
            contact.FirstName = newCandidates.firstName;
            contact.LastName = newCandidates.lastName;
            contact.Phone = newCandidates.phone;
            contact.Email = newCandidates.email;
            contact.Position__c = getPicklistValues('Contact', 'Position__c', newCandidates.position);
            contact.Department_Business_Area__c = getPicklistValues('Contact', 'Department_Business_Area__c', newCandidates.department);
            contact.Source__c = CommonUtility.CONTACT_SOURCE_RESTAPI_TRAININGS;

            if (newCandidates.pesel != null) {
                contact.Pesel__c = newCandidates.pesel;
                contact.Nationality__c = contact.Pesel__c.contains('00000') ? 'Other' : 'Polish';
            }

            contact.MailingCity = newCandidates.mailingCity;
            contact.MailingStreet = newCandidates.mailingStreet;
            contact.MailingCountry = newCandidates.mailingCountry;
            contact.MailingState = newCandidates.mailingState;
            contact.MailingPostalCode = newCandidates.mailingPostalCode;
            contact.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE);
            contact.Department = entity;
            if (newCandidates.attendeeType != null) {
                contact.TECH_attendeeType__c = newCandidates.attendeeType;
            }
            contact.Confirmed_Contact__c = false;
            if (String.isNotBlank(accountId)) contact.AccountId = accountId;
            //if (contact.Pesel__c.contains('00000')) contact.Foreigner__c = true;
            if (String.isNotBlank(json.businessArea)) contact.Industry__c = getPicklistValues('Contact', 'Industry__c', json.businessArea);
            returnList.add(contact);
        }
        return returnList;
    }

    public static List<Contact> handleContactDuplicates(List<Contact> inputContacts, List<Contact> potentialDuplicates, JSON2Apex parsed){
        List<Contact> returnList = new List<Contact>();

        if (potentialDuplicates.size() > 0){
            for(Contact c: inputContacts) {
                for(Contact dup : potentialDuplicates) {
                    //Duplicate check rule for Contacts
                    System.debug('Wejsciowy Kontakt = ' + inputContacts + ' Duplikat = ' + dup );
                    if (!c.Foreigner__c && String.isNotBlank(c.Pesel__c) && c.Pesel__c == dup.Pesel__c || c.FirstName == dup.FirstName && c.LastName == dup.LastName && (String.isNotBlank(c.Phone) && c.Phone == dup.Phone || String.isNotBlank(c.MobilePhone) && c.MobilePhone == dup.MobilePhone || String.isNotBlank(c.Email) && c.Email == dup.Email)) {
                        if (String.isNotBlank(c.MailingCity) && String.isNotBlank(c.MailingStreet) && String.isNotBlank(c.MailingPostalCode)){
                            /*Here's some logic behind assigning addresses
                            If there's only Mailing address already in a system and new address is different than existing one OR
                            If there are already both Mailing and Other Addresses and new address is different than existing Other Address:
                                we don't change Mailing address. We assign new address to otherAddress */
                            if ((dup.The_Same_Mailling_Address__c && !(c.MailingCity.equals(dup.MailingCity) && c.MailingStreet.equals(dup.MailingStreet))) || (!dup.The_Same_Mailling_Address__c && !(c.MailingStreet.equals(dup.OtherStreet) && c.MailingCity.equals(dup.OtherCity) && c.MailingPostalCode.equals(dup.OtherPostalCode)))){
                                dup.The_Same_Mailling_Address__c = false;
                                dup.OtherStreet = c.MailingStreet;
                                dup.OtherCity = c.MailingCity;
                                dup.OtherPostalCode = c.MailingPostalCode;
                            }
                            else if (String.isBlank(dup.MailingStreet) && String.isBlank(dup.MailingCity) && String.isBlank(dup.MailingPostalCode)){
                                dup.The_Same_Mailling_Address__c = true;
                                dup.MailingStreet = c.MailingStreet;
                                dup.MailingCity = c.MailingCity;
                                dup.MailingPostalCode = c.MailingPostalCode;
                            }
                        }
                        if (String.isEmpty(dup.Phone)) dup.Phone = c.Phone;
                        if (String.isEmpty(dup.Email)) dup.Email = c.Email;
                        if (String.isEmpty(dup.Position__c)) dup.Position__c = c.Position__c;
                        if (String.isEmpty(dup.Department_Business_Area__c)) dup.Department_Business_Area__c = c.Department_Business_Area__c;
                        if (String.isEmpty(dup.Pesel__c)) dup.Pesel__c = c.Pesel__c;
                        if (String.isEmpty(dup.Department)) dup.Department = c.Department;
                        if (String.isEmpty(dup.AccountId)) dup.AccountId = c.AccountId;
                        if (String.isEmpty(dup.Industry__c)) dup.Industry__c = c.Industry__c;
                        c.Id = dup.Id;
                        createDuplicateNote(c, dup.Id, parsed);
                        returnList.add(dup);
                    }
                }
            }
        }
        //Add those without duplicates
        for(Contact c: inputContacts){
            if (c.Id == null){
                returnList.add(c);
            }
        }
        return returnList;
    }

    //***********************************************************
    //Get consents definitions for enrolmment
    //***********************************************************

    public static List<Contact> clearAlreadyExistingEnrollments(Set<Enrollment__c> duplicateEnrollments, List<Contact> insertedContacts){
        List<Contact> outputContacts = new List<Contact>();
        Set<Id> participantIds = new Set<Id>();
        for (Enrollment__c enr: duplicateEnrollments){
            participantIds.add(enr.Participant__c);
        }

        for (Contact c: insertedContacts){
            if (!participantIds.contains(c.Id))
                outputContacts.add(c);
        }
        return outputContacts;
    }

    //***********************************************************
    //Update contact consents
    //***********************************************************

    public static void updateContactConsents(List<Contact> contactList, Boolean marketingCons, Boolean contactCons, Boolean directCons, String usEntity, String formType) {
        //user Entity
        String entity = CommonUtility.getMarketingEntity(usEntity);
        List<Schema.SObjectField> fieldsToValidate = new List<Schema.SObjectField>();

        if (marketingCons) fieldsToValidate.add(Contact.Consent_Marketing_ADO__c);
        if (contactCons) fieldsToValidate.add(Contact.Consent_Electronic_Communication_ADO__c);
        if (directCons) fieldsToValidate.add(Contact.Consent_Direct_Communications_ADO__c);

        Map<Id, Contact> contactMapToRemoveDuplicates = new Map<Id, Contact>();

        for (Contact contactToFixConsents : contactList) {
            for (Schema.SObjectField fieldToValidate : fieldsToValidate) {
                contactToFixConsents = MarketingConsentManager.updateConsentOnContact(entity, contactToFixConsents, fieldToValidate);
                contactMapToRemoveDuplicates.put(contactToFixConsents.Id, contactToFixConsents);
            }
        }

        try {
            update contactMapToRemoveDuplicates.values();
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* --------------------------------------------------------------------------------------------
    ** CREATE NOTE FOR DUPLICATE CONTACT VALUES
    **      PARAMS  - Contact - duplicate input values
    ** ----------------------------------------------------------------------------------------- */
    public static void createDuplicateNote(Contact c, Id dupId, JSON2Apex json){
        Note note = new Note();
        note.ParentId = dupId;
        note.Title = 'Wykryto potencjalny duplikat w dniu ' + Date.today().format() + ' ' + c.FirstName + ' ' + c.LastName;
        note.Body = 'Dane Kontaktu CRM:' + '\n' +
                'Imię= ' + c.FirstName + '\n' +
                'Nazwisko= ' + c.LastName + '\n' +
                'Telefon= ' + c.Phone + '\n' +
                'Email= ' + c.Email + '\n' +
                'Stanowisko= ' + c.Position__c + '\n' +
                'Departament= ' + c.Department + '\n' +
                'Pesel= ' + c.Pesel__c + '\n' +
                'Adres= ' + c.MailingStreet + ' ' + c.MailingCity + ' ' + c.MailingPostalCode + '\n' +
                json.createDuplicateStringInfoForRecruitment();
        insert note;
    }

    /* --------------------------------------------------------------------------------------------
    ** GET PICKLIST VALUES - method to remotely get picklist values and compare with REST input
    **      PARAMS  - sObjectName - object to query
    **              - fieldAPI - field to query
    **              - compareValue - API input value to compare
    ** ----------------------------------------------------------------------------------------- */

    public static String getPicklistValues(String sObjectName, String fieldAPI, String compareValue) {
        System.debug('-- PARAM SOBJECTNAME: ' + sObjectName);
        System.debug('-- PARAM FIELDAPI: ' + fieldAPI);

        Map<String, Schema.SObjectType> objGlobal = Schema.getGlobalDescribe();
        Schema.SObjectType objType = objGlobal.get(sObjectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> objDescribeFieldMap = objDescribe.fields.getMap();

        Schema.DescribeFieldResult fieldResult = objDescribeFieldMap.get(fieldAPI).getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry entry : picklistValues) {
            if (compareValue.equals(entry.getLabel())){
                System.debug('Picked value is ' + entry.getValue());
                return entry.getValue();
            }
        }
        return compareValue;
    }
}