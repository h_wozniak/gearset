@isTest
private class ZTestSendTrainingConfirmationMassEmail {


    private static Map<Integer, sObject> prepareData() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(
                Type__c = CommonUtility.OFFER_TYPE_CLOSED
        ), true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
                Trade_Name__c = 'Obsługa komputera',
                Training_Offer_from_Schedule__c = retMap.get(0).Id,
                Active__c = true,
                Bank_Account_No__c = '123',
                Number_of_Seats__c = 5,
                Offer_Value__c = 1000,
                Valid_From__c = System.today(),
                Valid_To__c = System.today(),
                Start_Time__c = '10:50',
                Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
                Training_Place__c = 'Hotel WSB',
                City__c = 'Wrocław',
                Street__c = 'Warszawska 10',
                Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
                Training_Offer__c = retMap.get(1).Id,
                Company__c = ZDataTestUtility.createCompany(new Account(
                        NIP__c = '3672620147'
                ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
                Enrollment_Training_Participant__c = retMap.get(2).Id,
                Training_Offer_for_Participant__c = retMap.get(1).Id,
                Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));

        return retMap;
    }

    @isTest static void testSendTrainingConfirmationMassEmailWithoutTemplate() {
        Map<Integer, sObject> dataMap = prepareData();

        PageReference pageRef = Page.SendTrainingConfirmationMassEmail;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController(dataMap.get(1));
        SendTrainingConfirmationMassEmail cntrl = new SendTrainingConfirmationMassEmail(stdController);

        Test.startTest();
        System.assertNotEquals(0, cntrl.getTemplates().size());

        cntrl.sendEmail();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == Label.msg_error_NoMailTemplateSelected) {
                messageFound = true;
            }
        }
        System.assert(messageFound);

        cntrl.loadTemplateForPreview();
        Test.stopTest();

    }

    @future
    private static void prepareEmailTemplate() {
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'name';
        validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        validEmailTemplate.Subject = 'Your Subject Here';
        validEmailTemplate.Body = 'testBody';
        insert validEmailTemplate;
    }

    @isTest static void testSendTrainingConfirmationMassEmailWithoutParticipants() {
        Map<Integer, sObject> dataMap = prepareData();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareEmailTemplate();
            Test.stopTest();
        }

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        EmailTemplate em = [SELECT Id, Name, DeveloperName, TemplateType, FolderId, Subject, isActive FROM EmailTemplate LIMIT 1];

        SendTrainingConfirmationMassEmail stce = new SendTrainingConfirmationMassEmail(stdController);
        stce.selectedTemplate = em.Id;

        stce.sendEmail();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == Label.msg_error_NoRecipientsChecked) {
                messageFound = true;
            }
        }
        System.assert(messageFound);
    }

    @isTest static void testSendTrainingConfirmationMassEmailWithParticipants() {
        Map<Integer, sObject> dataMap = prepareData();

        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareEmailTemplate();
            Test.stopTest();
        }

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        List<Enrollment__c> participants = ZDataTestUtility.createEnrollmentParticipants(new Enrollment__c(Training_Offer_for_Participant__c = dataMap.get(1).Id), 1, true);

        SendTrainingConfirmationMassEmail stce = new SendTrainingConfirmationMassEmail(stdController);

        List<Id> recipientIds = new List<Id>();
        for(Enrollment__c p : participants) {
            recipientIds.add(p.Id);
        }

        List<EmailTemplate> em = [SELECT Id, Name, DeveloperName, Body, TemplateType, FolderId, Subject, isActive FROM EmailTemplate WHERE Folder.DeveloperName = :CommonUtility.EMAIL_TEMPLATE_FOLDER_NAME_GRUPA_WSB_TRAININGS];
        stce.selectedTemplate = em[1].Id;

        for (SendTrainingConfirmationMassEmail.Recipient_Wrapper rw : stce.targetRecipients) {
            rw.checked = true;
        }

        stce.sendEmail();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == Label.msg_error_NoRecipientsChecked) {
                messageFound = true;
            }
        }
        System.assert(!messageFound);

        stce.loadTemplateForPreview();
        System.assertEquals(true, stce.isVFEmail);
    }

    @isTest static void testSendTrainingConfirmationMassEmailWithParticipants_customMessage() {
        Map<Integer, sObject> dataMap = prepareData();

        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareEmailTemplate();
            Test.stopTest();
        }

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        List<Enrollment__c> participants = ZDataTestUtility.createEnrollmentParticipants(new Enrollment__c(Training_Offer_for_Participant__c = dataMap.get(1).Id), 1, true);

        SendTrainingConfirmationMassEmail stce = new SendTrainingConfirmationMassEmail(stdController);

        List<Id> recipientIds = new List<Id>();
        for(Enrollment__c p : participants) {
            recipientIds.add(p.Id);
        }

        stce.selectedTemplate = 'CUSTOM_MESSAGE';

        for (SendTrainingConfirmationMassEmail.Recipient_Wrapper rw : stce.targetRecipients) {
            rw.checked = true;
        }

        stce.sendEmail();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == Label.msg_error_NoRecipientsChecked) {
                messageFound = true;
            }
        }
        System.assert(!messageFound);

        stce.loadTemplateForPreview();
        System.assertEquals(stce.mBody, null);
        System.assertEquals(stce.mSubject, null);

    }
 
 @isTest static void testSendTrainingConfirmationMassEmailWithParticipants_VFEmail() {
        Map<Integer, sObject> dataMap = prepareData();

        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareEmailTemplate();
            Test.stopTest();
        }

        ApexPages.StandardController stdController = new ApexPages.standardController((Offer__c)dataMap.get(1));

        List<Enrollment__c> participants = ZDataTestUtility.createEnrollmentParticipants(new Enrollment__c(Training_Offer_for_Participant__c = dataMap.get(1).Id), 1, true);

        SendTrainingConfirmationMassEmail stce = new SendTrainingConfirmationMassEmail(stdController);

        List<Id> recipientIds = new List<Id>();
        for(Enrollment__c p : participants) {
            recipientIds.add(p.Id);
        }
        List<EmailTemplate> em = [SELECT Id, Name, DeveloperName, Body, TemplateType, FolderId, Subject, isActive FROM EmailTemplate WHERE TemplateType = 'visualforce'];
     	for (EmailTemplate e : em) {
            if(e.TemplateType == 'visualforce')
                stce.templatesListVF.add(e.Id);    
        }
        stce.selectedTemplate = em[1].Id;

        for (SendTrainingConfirmationMassEmail.Recipient_Wrapper rw : stce.targetRecipients) {
            rw.checked = true;
        }

        stce.sendEmail();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == Label.msg_error_NoRecipientsChecked) {
                messageFound = true;
            }
        }
        System.assert(!messageFound);
        stce.loadTemplateForPreview();
        System.assertEquals(true, stce.isVFEmail);
    }
}