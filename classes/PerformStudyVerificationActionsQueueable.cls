/**
*   @author         Sebastian Łasisz
*   @description    This class is used to finalize verification of contact
**/

public with sharing class PerformStudyVerificationActionsQueueable implements Queueable, Database.AllowsCallouts {
	Set<Id> enrIds;

	public PerformStudyVerificationActionsQueueable(Set<Id> enrIds) {
		this.enrIds = enrIds;
	}

	public void execute(QueueableContext qc) {
		EnrollmentVerificationManager.performStudyVerificationActionsAtFuture(enrIds);
	}
}