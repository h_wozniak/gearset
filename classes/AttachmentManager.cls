/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to Attachment object
**/


public without sharing class AttachmentManager {

    /* Sebastian Łasisz */
    // update current file on Enrollment Document with previous attachment
    public static void updateCurrentFileOnEnrollment(Set<Id> enrollmentsToUpdateCurrentFile) {
        List<Enrollment__c> enrollmentsWithAttachment = [
            SELECT Id, Current_File__c, Current_File_Name__c
            FROM Enrollment__c
            WHERE Id IN :enrollmentsToUpdateCurrentFile
            AND RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            ORDER BY CreatedDate
        ];

        List<Attachment> attachmentsOnEnrollment = [
            SELECT Id, Name, ParentId
            FROM Attachment
            WHERE parentId IN :enrollmentsToUpdateCurrentFile
            ORDER By CreatedDate ASC
        ];

        for (Enrollment__c docsWithAttachment : enrollmentsWithAttachment) {
            Attachment lastAttachment = null;
            docsWithAttachment.Current_File__c = null;
            docsWithAttachment.Current_File_Name__c = null;

            for (Attachment attachmentOnEnrollment : attachmentsOnEnrollment) {
                if (attachmentOnEnrollment.ParentId == docsWithAttachment.Id) {
                    lastAttachment = attachmentOnEnrollment;
                }
            }

            if (lastAttachment != null) {
                docsWithAttachment.Current_File__c = lastAttachment.Id;
                docsWithAttachment.Current_File_Name__c = lastAttachment.Name;
            }
        }

        try {
            CommonUtility.skipDocumentValidations = true;
            update enrollmentsWithAttachment;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }


    /* Sebastian Łasisz */
    // send Document List after Oath and Agreement Documents finished generating, it can be done automatically only once
    public static void checkSendReqDocList(Set<Id> docToCheckGenerationCompletion) {
        List<Enrollment__c> docsWithCreatedAttachments = [
            SELECT Id, Enrollment_from_Documents__c
            FROM Enrollment__c
            WHERE Id IN :docToCheckGenerationCompletion
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
            AND Enrollment_from_Documents__r.RequiredDocListEmailedCount__c = 0
        ];

        Set<Id> studyEnrIds = new Set<Id>();
        for (Enrollment__c doc : docsWithCreatedAttachments) {
            studyEnrIds.add(doc.Enrollment_from_Documents__c);
        }

        Set<Id> enrToSendReqDocList = determineEnrollmentsToSendEmail(studyEnrIds);

        if (!enrToSendReqDocList.isEmpty()) {
            System.enqueueJob(new SendEmailWithDocumentsQueueable(enrToSendReqDocList));
        }
    }

    public static Set<Id> determineEnrollmentsToSendEmail(Set<Id> studyEnrIds) {
        List<Enrollment__c> studyEnrRelatedDocs = [
                SELECT Id, Enrollment_from_Documents__r.RequiredDocListEmailedCount__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.Degree__c, Document__c,
                        Enrollment_from_Documents__r.Language_of_Enrollment__c,
                (SELECT Id
                FROM Attachments
                ORDER BY CreatedDate LIMIT 1)
                FROM Enrollment__c
                WHERE Enrollment_from_Documents__c IN :studyEnrIds
                AND (
                        Document__c = :CatalogManager.getDocumentAgreementId()
                        OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                        OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                        OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                        OR Document__c = :CatalogManager.getDocumentOathId()
                        OR Document__c = :CatalogManager.getDocumentOathPLId()
                )
        ];

        Set<Id> enrToSendReqDocList = new Set<Id>();
        for (Id studyEnrId : studyEnrIds) {
            Integer generatedCount = 0;
            Boolean mbaORSp = false;
            String language = '';

            for (Enrollment__c doc : studyEnrRelatedDocs) {
                if (studyEnrId == doc.Enrollment_from_Documents__c) {
                    mbaORSp = (doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_PG || doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_MBA);
                    language = doc.Enrollment_from_Documents__r.Language_of_Enrollment__c;
                    if (!doc.Attachments.isEmpty()) {
                        if (!mbaORSp) {
                            generatedCount++;
                        }
                        else if (mbaORSp && doc.Document__c != CatalogManager.getDocumentOathId()) {
                            generatedCount++;
                        }
                    }
                }
            }

            if (language != CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                if (generatedCount == 3 && !mbaORSp) { // Oath and Agreement and Questionnare Documents
                    enrToSendReqDocList.add(studyEnrId);
                }

                if (generatedCount == 2 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnrId);
                }
            }
            else {
                if (generatedCount == 6 && !mbaORSp) { // Oath and Agreement and Questionnare Documents
                    enrToSendReqDocList.add(studyEnrId);
                }

                if (generatedCount == 4 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnrId);
                }
            }
        }

        return enrToSendReqDocList;
    }

    /* Wojciech Słodziak */
    // send Document List after Agreement Document finished generating
    public static void checkSendReqDocList_Unenrolled(Set<Id> generatedDocIds) {
        List<Enrollment__c> docsWithCreatedAttachments = [
            SELECT Id, Enrollment_from_Documents__c
            FROM Enrollment__c
            WHERE Id IN :generatedDocIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        Set<Id> studyEnrIds = new Set<Id>();
        for (Enrollment__c doc : docsWithCreatedAttachments) {
            studyEnrIds.add(doc.Enrollment_from_Documents__c);
        }

        List<Enrollment__c> studyEnrRelatedDocs = [
            SELECT Id, Enrollment_from_Documents__r.RequiredDocListEmailedCount__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.Degree__c, Document__c,
                (SELECT Id
                FROM Attachments
                WHERE (NOT(Name LIKE : '%' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + '%'))
                ORDER BY CreatedDate LIMIT 1)
            FROM Enrollment__c
            WHERE Enrollment_from_Documents__c IN :studyEnrIds
            AND (
                Document__c = :CatalogManager.getDocumentAgreementUnenrolledId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
            )
        ];

        Set<Id> enrToSendReqDocList = new Set<Id>();
        for (Id studyEnrId : studyEnrIds) {
            Integer generatedCount = 0;

            for (Enrollment__c doc : studyEnrRelatedDocs) {
                if (studyEnrId == doc.Enrollment_from_Documents__c) {
                    if (!doc.Attachments.isEmpty()) {
                        generatedCount++;
                    }
                }
            }

            if (generatedCount == 3) {
                enrToSendReqDocList.add(studyEnrId);
            }
        }

        if (!enrToSendReqDocList.isEmpty()) {
            EmailManager_UnenrolledDocumentListEmail.sendRequiredDocumentList(enrToSendReqDocList);
        }
    }

    /* Karolina Kowalik */
    // confirm when Returning Lead has attachment
    public static void confirmReturningLeadsWithAttachment(Set<Id> returningLeadsWithAttachment) {
        List<Marketing_Campaign__c> returningLeads = [
            SELECT Id, Confirmed__c
            FROM Marketing_Campaign__c
            WHERE Id IN :returningLeadsWithAttachment
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
        ];

        for (Marketing_Campaign__c returningLead :returningLeads) {
            returningLead.Confirmed__c = true;
        }
        try {
            update returningLeads;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
}