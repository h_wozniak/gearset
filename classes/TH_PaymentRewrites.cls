/**
* @description  This class is a Trigger Handler for Trainings - rewriting field values (for emails/preview)
**/

public without sharing class TH_PaymentRewrites extends TriggerHandler.DelegateBase {
    List<Payment__c> paymentsToUpdateFields;
    
    public override void prepareBefore() {
        paymentsToUpdateFields = new List<Payment__c>();
    }
    
    public override void prepareAfter() { }
    
    public override void beforeInsert(List<sObject> o) {
        List<Payment__c> newPayments = (List<Payment__c>)o;
        for(Payment__c newPayment : newPayments) {

            /* Sebastian Łasisz */
            // update University field on Payment record when new Payment is created
            if (newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)) {
                paymentsToUpdateFields.add(newPayment);
            }
        }
    }
    
    //public override void afterInsert(Map<Id, sObject> o) {}
    
    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}
    
    //public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}
    
    public override void finish() {

        /* Sebastian Łasisz */
        // update University field on Payment record when new Payment before insert
        if (paymentsToUpdateFields != null && !paymentsToUpdateFields.isEmpty()) {
            Set<Id> studyEnrollmentsIds = new Set<Id>();
            
            for (Payment__c paymentToUpdateUniversityField : paymentsToUpdateFields) {
                studyEnrollmentsIds.add(paymentToUpdateUniversityField.Enrollment_from_Schedule_Installment__c);
            }
    
            Map<Id, Enrollment__c> studyEnrollments = new Map<Id, Enrollment__c>([
                SELECT Id, University_Name__c 
                FROM Enrollment__c 
                WHERE Id IN :studyEnrollmentsIds
            ]);
    
            for (Payment__c paymentToUpdateUniversityField : paymentsToUpdateFields) {
            	paymentToUpdateUniversityField.University_Name__c = studyEnrollments.get(paymentToUpdateUniversityField.Enrollment_from_Schedule_Installment__c).University_Name__c;
            }
        }
    }
}