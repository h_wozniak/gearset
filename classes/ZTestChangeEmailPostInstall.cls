@isTest
private class ZTestChangeEmailPostInstall {
    
    @isTest static void testBatch() {
    	Test.startTest();
        List<Contact> contacts = ZDataTestUtility.createCandidateContacts(null, 1, true);
    	List<Contact> contactsPreScript = [SELECT Id, Email from Contact];
        
    	PostInstallUpdateActions  postinstall = new PostInstallUpdateActions ();
    	Test.testInstall(postinstall, null);
    	Test.testInstall(postinstall, new Version(1,0), true);        
    	Test.stopTest();
    
        List<Contact> contactsPostScript = [SELECT Id, Email from Contact];
    
        for (Contact contactPreScript : contactsPreScript) {
            for (Contact contactPostScript : contactsPostScript) {
                if (contactPostScript.Id == contactPreScript.Id) {
                    System.assertNotEquals(contactPostScript.Email, contactPreScript.Email);
                }
            }
    
        }
    }
}