@isTest
private class ZTestEmailTemplateComponentController {

    private static Map<Integer, sObject> prepareDataForTest(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
    }
    
    @isTest(SeeAllData = true)
    static void test_getCompanyLogoURL() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = RecordVals.WSB_NAME_WRO;

        Document doc = [
            SELECT Id 
            FROM Document
            WHERE DeveloperName = :EmailTemplateComponentController.WRO_LOGO_API
            LIMIT 1
        ];

        Test.startTest();
        String url = etcc.getCompanyLogoURL();
        Test.stopTest();

        System.assertNotEquals(null, url);
        System.assert(url.contains(doc.Id));
    }

    @isTest(SeeAllData = true)
    static void test_getCompanyLogoURL_notExisting() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = 'UnrealWSBName';


        Test.startTest();
        String url = etcc.getCompanyLogoURL();
        Test.stopTest();

        System.assertEquals(null, url);
    }

    @isTest(SeeAllData = true)
    static void test_FBLogoURL() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        Document doc = [
            SELECT Id 
            FROM Document
            WHERE DeveloperName = :EmailTemplateComponentController.FB_LOGO_API
            LIMIT 1
        ];

        Test.startTest();
        String url = etcc.getFBLogoURL();
        Test.stopTest();

        System.assertNotEquals(null, url);
        System.assert(url.contains(doc.Id));
    }

    @isTest(SeeAllData = true)
    static void test_INLogoURL() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        Document doc = [
            SELECT Id 
            FROM Document
            WHERE DeveloperName = :EmailTemplateComponentController.IN_LOGO_API
            LIMIT 1
        ];

        Test.startTest();
        String url = etcc.getINLogoURL();
        Test.stopTest();

        System.assertNotEquals(null, url);
        System.assert(url.contains(doc.Id));
    }

    @isTest(SeeAllData = true)
    static void test_YTLogoURL() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        Document doc = [
            SELECT Id 
            FROM Document
            WHERE DeveloperName = :EmailTemplateComponentController.YT_LOGO_API
            LIMIT 1
        ];

        Test.startTest();
        String url = etcc.getYTLogoURL();
        Test.stopTest();

        System.assertNotEquals(null, url);
        System.assert(url.contains(doc.Id));
    }

    @isTest(SeeAllData = true)
    static void test_TWLogoURL() {
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        Document doc = [
            SELECT Id 
            FROM Document
            WHERE DeveloperName = :EmailTemplateComponentController.TW_LOGO_API
            LIMIT 1
        ];

        Test.startTest();
        String url = etcc.getTWLogoURL();
        Test.stopTest();

        System.assertNotEquals(null, url);
        System.assert(url.contains(doc.Id));
    }

    static void test_getUniversityAccountData() {
        Account acc = ZDataTestUtility.createUniversity(null, true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;

        Test.startTest();
        Account accData = etcc.getUniversityAccountData();
        Test.stopTest();

        System.assertNotEquals(null, accData);
    }

    @isTest static void test_getDegreeMBA() {
        Account acc = ZDataTestUtility.createUniversity(null, true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = null;

        Test.startTest();
        Boolean result = etcc.getDegreeMBA();
        Test.stopTest();

        System.assertEquals(result, false);
    }

    @isTest static void test_getDegreeMBA_1() {
        Account acc = ZDataTestUtility.createUniversity(null, true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = studyEnrollment.Id;

        Test.startTest();
        Boolean result = etcc.getDegreeMBA();
        Test.stopTest();

        System.assertEquals(result, false);
    }

    @isTest static void test_getDegreeMBA_2() {
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl'), true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = studyEnrollment.Id;

        Test.startTest();
        Boolean result = etcc.getDegreeMBA();
        Test.stopTest();

        System.assertEquals(result, false);
    }

    @isTest static void test_getDegreeMBA_3() {
        Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_MBA);
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl, 12345'), true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = dataMap.get(2).Id;

        Test.startTest();
        Boolean result = etcc.getDegreeMBA();
        Test.stopTest();

        System.assertEquals(result, true);
    }

    @isTest static void test_getMBAOfficeName() {
        Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_MBA);
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl, 12345'), true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = dataMap.get(2).Id;

        Test.startTest();
        String result = etcc.getMBAOfficeName();
        Test.stopTest();

        System.assertEquals(result, 'Biura Programu MBA');        
    }

    @isTest static void test_getMBAOfficeName_2() {
        Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_MBA);
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl, 12345'), true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = null;

        Test.startTest();
        String result = etcc.getMBAOfficeName();
        Test.stopTest();

        System.assertEquals(result, 'Biura Rekrutacji');        
    }

    @isTest static void test_getUniversityAccountData_1() {
        Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_MBA);
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl, 12345'), true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = acc.Name;
        etcc.enrollmentId = null;

        Test.startTest();
        Account result = etcc.getUniversityAccountData();
        Test.stopTest();

        System.assertNotEquals(result, null);  
    }

    @isTest static void test_getUniversityAccountData_2() {
        Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_MBA);
        Account acc = ZDataTestUtility.createUniversity(new Account(MBA_office_address__c = 'Biuro Programu MBA, ul. Sportowa 29 pokój 222, 41-506 Chorzów, emba@chorzow.wsb.pl, 12345'), true);
        EmailTemplateComponentController etcc = new EmailTemplateComponentController();

        etcc.universityNameParam = null;
        etcc.enrollmentId = null;

        Test.startTest();
        Account result = etcc.getUniversityAccountData();
        Test.stopTest();

        System.assertEquals(result, null);  
    }
}