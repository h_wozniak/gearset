/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to create Marketing Campaign Contacts For Lack of Documents Marketing Campaign
**/

/**
*   To initiate the EnrollmentConfirmationEmailReminder execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('CampaignLackOfDocumentsInsertBatch', '0 0 8 * * ? *', new CampaignLackOfDocumentsInsertBatch());
**/


global class CampaignLackOfDocumentsInsertBatch implements Schedulable {

    global void execute(SchedulableContext sc) {
        CreateCampaignLackOfDocumentsBatch batch3Days = new CreateCampaignLackOfDocumentsBatch(0);
        Database.executebatch(batch3Days, 30);

        ActivityLackOfDocumentsBatach activityBatch = new ActivityLackOfDocumentsBatach(10);
        Database.executebatch(activityBatch, 30);        
    }
}