/**
*   @author         Karolina Bolinger
*   @description    This class is used to send Price Book to WWW sites.
**/

@RestResource(urlMapping = '/price-book/*')
global without sharing class IntegrationPriceBook {
    
    @HttpGet
    global static void sendPriceBook() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        try {
            res.responseBody = Blob.valueOf(IntegrationPriceBookHelper.preparePriceBooksToSend());
            res.statusCode = 200;
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;    
        }
    }
}