/**
*   @author         Sebastian Łasisz
*   @description    This class is used to send Enrollment to Experia
**/

global without sharing class IntegrationEnrollmentSender {
    
    @future(callout=true)
    public static void sendEnrollmentListAtFuture(Set<Id> studyEnrollmentsIds, Boolean sendingPayments, Boolean sendingInitialFees) {
        IntegrationEnrollmentSender.sendEnrollmentList(studyEnrollmentsIds, sendingPayments, sendingInitialFees);
    }

    public static Boolean sendEnrollmentListSync(Set<Id> studyEnrollmentsIds) {
        return IntegrationEnrollmentSender.sendEnrollmentList_fromPayments(studyEnrollmentsIds);
    }

    public static Boolean sendEnrollmentList(Set<Id> studyEnrollmentsIds, Boolean sendingPayments, Boolean sendingInitialFees) {
        HttpRequest httpReq = new HttpRequest();
    
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createDidactics = esb.Create_Mass_Didactics_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationEnrollmentHelper.prepareEnrollmentsToSend(studyEnrollmentsIds);
        httpReq.setBody(jsonToSend);


        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Set<Id> invalidRecords = new Set<Id>();
        try {
            Error_JSON errors = parseErrors(httpRes.getBody());

            if (errors.invalid_records != null) {
                Set<Id> potentailInvalidRecords = new Set<Id>();
                for (Record_JSON record : errors.invalid_records) {
                    potentailInvalidRecords.add(record.enrollment_id);
                }

                Map<Id, Enrollment__c> educationalAgreements = new Map<Id, Enrollment__c>([
                    SELECT Id, Source_Enrollment__c
                    FROM Enrollment__c
                    WHERE Id IN :potentailInvalidRecords
                ]);
                
                for (Record_JSON record : errors.invalid_records) {
                    invalidRecords.add(educationalAgreements.get(record.enrollment_id).Source_Enrollment__c);
                    studyEnrollmentsIds.remove(educationalAgreements.get(record.enrollment_id).Source_Enrollment__c);
                }
            }
        }
        catch (Exception e) {
            //no  errors happened
            System.debug(e);
        }

        Boolean success = (httpRes.getStatusCode() == 200 || httpRes.getStatusCode() == 202);

        IntegrationEnrollmentHelper.updateSyncStatus_inProgress(true, studyEnrollmentsIds, parseErrorMsg(httpRes.getBody()), sendingPayments, sendingInitialFees);
        IntegrationEnrollmentHelper.updateSyncStatus_inProgress(false, invalidRecords, parseErrorMsg(httpRes.getBody()), sendingPayments, sendingInitialFees);

        return success; 
    }

    public static Boolean sendEnrollmentList_fromPayments(Set<Id> studyEnrollmentsIds) {
        HttpRequest httpReq = new HttpRequest();
    
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createDidactics = esb.Create_Mass_Didactics_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationEnrollmentHelper.prepareEnrollmentsToSend(studyEnrollmentsIds);
        httpReq.setBody(jsonToSend);


        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        try {
            Error_JSON errors = parseErrors(httpRes.getBody());

            Set<Id> potentailInvalidRecords = new Set<Id>();
            for (Record_JSON record : errors.invalid_records) {
                potentailInvalidRecords.add(record.enrollment_id);
            }

            Map<Id, Enrollment__c> educationalAgreements = new Map<Id, Enrollment__c>([
                SELECT Id, Source_Enrollment__c
                FROM Enrollment__c
                WHERE Id IN :potentailInvalidRecords
            ]);

            Set<Id> invalidRecords = new Set<Id>();
            for (Record_JSON record : errors.invalid_records) {
                invalidRecords.add(educationalAgreements.get(record.enrollment_id).Source_Enrollment__c);
                studyEnrollmentsIds.remove(educationalAgreements.get(record.enrollment_id).Source_Enrollment__c);
            }
            
            IntegrationEnrollmentHelper.updateSyncStatus_inProgress(false, invalidRecords, parseErrorMsg(httpRes.getBody()), true, false);
        }
        catch (Exception e) {
            //no error occured
            System.debug(e);
        }

        Boolean success = (httpRes.getStatusCode() == 200 || httpRes.getStatusCode() == 202);
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationEnrollmentSender.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + studyEnrollmentsIds);            
        }
        else {
            IntegrationPaymentScheduleHelper.updateSyncStatus_inProgress(true, studyEnrollmentsIds, '', false);
        }


        return success; 
    }

    private static String parseErrorMsg(String jsonBody) {
        if (jsonBody.contains('"status_code":200')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":rekrutacja została zaktualizowana}';
        if (jsonBody.contains('"status_code":201')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":rekrutacja została stworzona}';
        if (jsonBody.contains('"status_code":400')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":niepoprawne dane - komunikat nie może być przetworzony}';
        if (jsonBody.contains('"status_code":405')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":nie można zmienić rekrutacji na podany nowy produkt}';
        if (jsonBody.contains('"status_code":406')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":nie znaleziono produktu o podanym ID}';
        if (jsonBody.contains('"status_code":409')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":zduplikowany produkt/rekrutacja}';
        if (jsonBody.contains('"status_code":500')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":błąd serwera}';

        return jsonBody;
    }
    
    private static Error_JSON parseErrors(String jsonBody) {
        return (Error_JSON) System.JSON.deserialize(jsonBody, Error_JSON.class);
    }

    public class Error_JSON {
        public String error_code;
        public List<Record_JSON> invalid_records;
    }

    public class Record_JSON {
        public String error_message;
        public String enrollment_id;
    }

}