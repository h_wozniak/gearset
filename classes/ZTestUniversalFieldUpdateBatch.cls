@isTest
private class ZTestUniversalFieldUpdateBatch {
    
    @isTest static void test_Batch() {
        List<Contact> contactList = ZDataTestUtility.createCandidateContacts(new Contact(
            Family_Name__c = 'Simpson',
            OtherCity = 'Warsaw'
        ), 2, true);

        Test.startTest();
        Database.executeBatch(
            new UniversalFieldUpdateBatch(
                'SELECT Id, Family_Name__c, OtherCity FROM Contact',
                new List<Schema.sObjectField> {Contact.Family_Name__c, Contact.OtherCity},
                new List<Object> {'Kowalsky', 'Paris'}
            )
        );
        Test.stopTest();

        List<Contact> contactListAfter = [
            SELECT Id, Family_Name__c, OtherCity
            FROM Contact
        ];

        for (Contact c : contactListAfter) {
            System.assertEquals('Kowalsky', c.Family_Name__c);
            System.assertEquals('Paris', c.OtherCity);
        }
    }

}