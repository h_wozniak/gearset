@isTest
private class ZTestLanguageManager {

	@isTest public static void testValidateAndUpdateLanguage() {
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		List<Offer__c> offerLanguageList = ZDataTestUtility.createForeignLanguages(new Offer__c(Course_Offer_from_language__c = courseOffer.Id), 1, true);

		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);

		List<Enrollment__c> enrollmentsLanguage = new List<Enrollment__c>();

		Test.startTest();

		for (Offer__c offerLanguage : offerLanguageList) {
			enrollmentsLanguage.add(ZDataTestUtility.createEnrollmentLanguage(new Enrollment__c(
				Enrollment_from_Language__c = studyEnrollment.Id, 
				Course_or_Specialty_Offer__c = courseOffer.Id, 
				Foreign_Language__c = offerLanguage.Language__c
			), true));			
		}

		Test.stopTest();

		List<Enrollment__c> languageEnrollments = [
			SELECT Id 
			FROM Enrollment__c 
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE)
		];

		System.assertEquals(languageEnrollments.size(), 1);
	}

	@isTest public static void testValidateAndUpdateLanguageWithSpecialtyOffer() {
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);

		List<Offer__c> offerLanguageList = ZDataTestUtility.createForeignLanguages(new Offer__c(Course_Offer_from_language__c = specialtyOffer.Id), 1, true);

		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id), true);

		List<Enrollment__c> enrollmentsLanguage = new List<Enrollment__c>();

		Test.startTest();

		for (Offer__c offerLanguage : offerLanguageList) {
			enrollmentsLanguage.add(ZDataTestUtility.createEnrollmentLanguage(new Enrollment__c(
				Enrollment_from_Language__c = studyEnrollment.Id, 
				Course_or_Specialty_Offer__c = specialtyOffer.Id, 
				Foreign_Language__c = offerLanguage.Language__c
			), true));			
		}

		Test.stopTest();

		List<Enrollment__c> languageEnrollments = [
			SELECT Id 
			FROM Enrollment__c 
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE)
		];

		System.assertEquals(languageEnrollments.size(), 1);
	}

	@isTest public static void testValidateAndUpdateLanguageWithoutLanguageOnOffer() {
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);

		Test.startTest();
		
		try {
			List<Enrollment__c> enrollmentsLanguage = new List<Enrollment__c>();
			enrollmentsLanguage.add(ZDataTestUtility.createEnrollmentLanguage(new Enrollment__c(Enrollment_from_Language__c = studyEnrollment.Id), true));
		}
		catch (Exception e) {
			System.assert(e.getMessage().contains(Label.msg_error_notExistingLanguageOnEnrollment));
		}

		Test.stopTest();

		List<Enrollment__c> languageEnrollments = [
			SELECT Id 
			FROM Enrollment__c 
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE)
		];

		System.assertEquals(languageEnrollments.size(), 0);
	}

	@isTest public static void testValidateAndUpdateLanguageWithDuplicateLanguageOnEnrollment() {
		Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

		List<Offer__c> offerLanguageList = ZDataTestUtility.createForeignLanguages(new Offer__c(Course_Offer_from_language__c = courseOffer.Id), 1, true);

		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);

		List<Enrollment__c> enrollmentsLanguage = new List<Enrollment__c>();

		Test.startTest();
		
		for (Offer__c offerLanguage : offerLanguageList) {
			enrollmentsLanguage.add(ZDataTestUtility.createEnrollmentLanguage(new Enrollment__c(
				Enrollment_from_Language__c = studyEnrollment.Id, 
				Course_or_Specialty_Offer__c = courseOffer.Id, 
				Foreign_Language__c = offerLanguage.Language__c
			), true));			
		}

		try {
			enrollmentsLanguage.add(ZDataTestUtility.createEnrollmentLanguage(new Enrollment__c(
				Enrollment_from_Language__c = studyEnrollment.Id, 
				Course_or_Specialty_Offer__c = courseOffer.Id, 
				Foreign_Language__c = offerLanguageList[0].Language__c
			), true));
		}
		catch (Exception e) {
			System.assert(e.getMessage().contains(Label.msg_error_duplicateLanguageOnEnrollment));
		}

		Test.stopTest();	

		List<Enrollment__c> languageEnrollments = [
			SELECT Id 
			FROM Enrollment__c 
			WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE)
		];

		System.assertEquals(languageEnrollments.size(), 1);	
	}
}