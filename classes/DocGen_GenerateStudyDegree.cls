global class DocGen_GenerateStudyDegree implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateStudyDegree() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateStudyDegree.generateStudyDegree(objectId);  
    }

    global static String generateStudyDegree(String objectId) {
        Enrollment__c enrollment = [SELECT Degree__c, Enrollment_from_Documents__r.Degree__c FROM Enrollment__c WHERE Id =: objectId];
        
        String degree = '';
        
        List<String> valuesToBuildResponse;
        String selectedValueToBuildResponse;

        if (enrollment.Degree__c != null) {
            degree = enrollment.Degree__c;
        }
        else {
            degree = enrollment.Enrollment_from_Documents__r.Degree__c;
        }
        
        valuesToBuildResponse = new List<String> { 'licencjackie/', 'inżynierskie/', 'jednolite magisterskie/', 'studia pierwszego stopnia/', 'studia drugiego stopnia/', 'studia drugiego stopnia z SP' };
        selectedValueToBuildResponse = generateFullDegreeName(degree);
        String response = '';
        
        for (String valueToBuildResponse : valuesToBuildResponse) {
            if (valueToBuildResponse.substring(0, valueToBuildResponse.length() - 1) == (selectedValueToBuildResponse)) {
                response += '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + valueToBuildResponse + '</w:t></w:r>';
            }
            else {
                response += '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + valueToBuildResponse + '</w:t></w:r>';
            }
        }
        return response;
    }
    
    private static String generateFullDegreeName(String degree) {
        if (degree == CommonUtility.OFFER_DEGREE_I) return 'studia pierwszego stopnia';
        if (degree == CommonUtility.OFFER_DEGREE_II) return 'studia drugiego stopnia';
        if (degree == CommonUtility.OFFER_DEGREE_II_PG) return 'studia drugiego stopnia z SP';
        if (degree == CommonUtility.OFFER_DEGREE_U) return 'jednolite magisterskie';
        return '';
        
    }
}