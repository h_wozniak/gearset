/**
*   @author         Radosław Urbański
*   @description    This class is used to delete document to enrollment.
**/

@RestResource(urlMapping = '/enrollment-document-delete/*')
global without sharing class IntegrationDocumentDelete {
	
	/* ------------------------------------------------------------------------------------------------ */
	/* ---------------------------------------- HTTP METHODS ------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------ */

	@HttpPOST
	global static void deleteDocument() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String enrollmentId = req.params.get('enrollment_id');
		if (enrollmentId == null) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id');
			return;
		}

		String jsonBody = req.requestBody.toString();

		List<DeleteDocumentRequest_JSON> parsedJson;

		try {
			parsedJson = parse(jsonBody);
		} catch (Exception ex) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
		}

		if (parsedJson != null) {
			List<Enrollment__c> enrollmentList = [SELECT Id FROM Enrollment__c WHERE Id = :enrollmentId];
			if (enrollmentList.isEmpty()) {
				res.statusCode = 400;
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(604, 'enrollmentId');
				return;
			}

			Map<Id, String> documentFromRequestMap = new Map<Id, String>();
			for (DeleteDocumentRequest_JSON docFromReq : parsedJson) {
				res.statusCode = 400;
				if (String.isEmpty(docFromReq.document_id)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'document_id');
					return;
				} else if (String.isEmpty(docFromReq.uploaded_file_name)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_name');
					return;
				} else {
					documentFromRequestMap.put(Id.valueOf(docFromReq.document_id), docFromReq.uploaded_file_name);
				}
			}

			Map<Id, Enrollment__c> documentInCRMMap = new Map<Id, Enrollment__c>([
                SELECT Id, Document_Accepted__c 
                FROM Enrollment__c 
                WHERE Id IN :documentFromRequestMap.keySet()
            ]);
            
			if (documentInCRMMap.size() != documentFromRequestMap.size()) {
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(604, 'document_id');
				return;
			}

			List<Attachment> attachmentList = [
                SELECT Id, Name, CreatedDate, ParentId 
                FROM Attachment 
                WHERE ParentId IN :documentInCRMMap.keySet() 
                AND Name IN :documentFromRequestMap.values()
            ];

			Map<Id, Attachment> documentAttachmentMap = new Map<Id, Attachment>();
			for (Enrollment__c doc : documentInCRMMap.values()) {
				if (doc.Document_Accepted__c) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(1300, 'document_id' + doc.Id);
					return;
				}
				for (Attachment att : attachmentList) {
					if (att.ParentId == doc.Id && att.Name == documentFromRequestMap.get(doc.Id) && documentAttachmentMap.get(doc.Id) == null) {
						documentAttachmentMap.put(doc.Id, att);
					} else if (att.ParentId == doc.Id && att.Name == documentFromRequestMap.get(doc.Id) && documentAttachmentMap.get(doc.Id).CreatedDate < att.CreatedDate) {
						documentAttachmentMap.put(doc.Id, att);
					}
				}
				if (documentAttachmentMap.get(doc.Id) == null) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(604, 'uploaded_file_name for document_id' + doc.Id);
					return;
				}
			}
			
			try {
				delete documentAttachmentMap.values();
				IntegrationError.Success_JSON successJSON = new IntegrationError.Success_JSON();
				successJSON.status_code = 200;
				successJSON.message = 'success';

				res.statusCode = 200;
				res.responseBody = Blob.valueOf(JSON.serialize(successJSON));

			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
			}
		}
	}

	private static List<DeleteDocumentRequest_JSON> parse(String jsonBody) {
		return (List<DeleteDocumentRequest_JSON>) System.JSON.deserialize(jsonBody, List<DeleteDocumentRequest_JSON>.class);
	}

	/* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    global class DeleteDocumentRequest_JSON {
        public String document_id;
        public String uploaded_file_name;
    }
}