/**
* @author       Wojciech Słodziak
* @description  This class is a controller for EnrollmentTraining.page, wchich is a wizard of training enrollment
**/
public with sharing class EnrollmentTrainingController {
    
    public final String ENRTYPE_IND = 'ind';
    public final String ENRTYPE_COM = 'com';

    public Enrollment_Wrapper ew { get; set; }
    public Offer__c schedule { get; set; }

    public Boolean isEdit { get; set; }
    public Boolean isOpen { get; set; }
    public List<SelectOption> enrollmentTypes { get; set; }
    public String selectedEnrollmentType { get; set; }
    public Boolean nextParticipant { get; set; }

    public String contactPersonName { get; set; }

    public String byMailPV { get { return CommonUtility.ENROLLMENT_INVOICE_DELIVERY_EMAIL; } }

    private Map<Id, Enrollment__c> enrollmentsToDelete { get; set; }

    private String retURL;

    public EnrollmentTrainingController() {
        Id scheduleId = ApexPages.currentPage().getParameters().get('scheduleId');
        Id candidateId = ApexPages.currentPage().getParameters().get('candidateId');
        Id companyId = ApexPages.currentPage().getParameters().get('companyId');
        Id enrollmentId = ApexPages.currentPage().getParameters().get('enrollmentId');
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        String entity;

        if (scheduleId != null) {
            schedule = [
                SELECT Id, Name, RecordTypeId, Training_Offer_from_Schedule__r.Trade_Name__c, Training_Place__c, Price_per_Person__c, 
                       Price_per_Person_Student_Graduate__c, Offer_Value__c, Valid_From__c, Valid_To__c, Start_Time__c, Number_of_Seats__c, 
                       Training_Offer_from_Schedule__r.University_from_Training__c, University_Name__c 
                FROM Offer__c 
                WHERE Id = :scheduleId
            ];
            entity = CommonUtility.getMarketingEntityByUniversityName(schedule.University_Name__c);
        }

        isEdit = false;
        ew = new Enrollment_Wrapper();
        if (enrollmentId != null) {
            isEdit = true;
            enrollmentsToDelete = new Map<Id, Enrollment__c>();

            //load existing enrollment data
            ew.mainEnrollment = [
                SELECT Id, Name, RecordTypeId, Company__c, Reporting_Person__c, Reporting_Person__r.Name, Please_Issue_a_Proforma_Invoice__c, 
                       The_Same_Data_as_on_Contact__c, Company_Name__c, Street_for_Invoice__c, Postal_Code_for_Invoice__c, City_for_Invoice__c, 
                       Country_for_Invoice__c, Way_of_Invoice_Delivery__c, Email_for_Invoice_Delivery__c, Description_of_Service_on_the_Invoice__c, 
                       The_Invoice_after_Payment__c, NIP_for_Invoice__c, First_Name_for_Invoice__c, Last_Name_for_Invoice__c, Source__c,
                       Educational_Advisor__c, Training_Offer__c 
                FROM Enrollment__c 
                WHERE Id = :enrollmentId
            ];

            ew.prList = [
                SELECT Id, Name, Participant__c, Enrollment_Training_Participant__c, Student_Graduate_the_same_University__c 
                FROM Enrollment__c 
                WHERE Enrollment_Training_Participant__c =: enrollmentId 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
            ];

            schedule = [
                SELECT Id, Name, RecordTypeId, Training_Offer_from_Schedule__r.Trade_Name__c, Training_Place__c, Price_per_Person__c, 
                       Price_per_Person_Student_Graduate__c, Offer_Value__c, Valid_From__c, Valid_To__c, Start_Time__c, Number_of_Seats__c, 
                       Training_Offer_from_Schedule__r.University_from_Training__c, University_Name__c 
                FROM Offer__c 
                WHERE Id = :ew.mainEnrollment.Training_Offer__c
            ];

            contactPersonName = ew.mainEnrollment.Reporting_Person__r.Name;
        } else {
            ew.mainEnrollment = getFreshEnrollment();
            ew.consentList = getConsents(entity);
        }

        isOpen = (schedule.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE));

        enrollmentTypes = getEnrollmentTypes(ew.mainEnrollment.RecordTypeId);

        selectedEnrollmentType = (enrollmentTypes.size() > 1 && ew.mainEnrollment.Company__c == null && companyId == null? ENRTYPE_IND : ENRTYPE_COM);

        if (candidateId != null) {
            addParticipant(candidateId);
        }
        
        if (companyId != null) {
            addCompany(companyId);
        }

    }


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- INIT METHODS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    private Id getEnrollmentRT(String scheduleRtId) {
        if (scheduleRtId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)) {
            return CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING);
        } else if (scheduleRtId ==  CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
            return CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING);
        }
        return null;
    }

    private List<SelectOption> getEnrollmentTypes(Id enrollmentRT) {
        List<SelectOption> typesToReturn = new List<SelectOption>();
        if (enrollmentRT == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {
            typesToReturn.add(new SelectOption(ENRTYPE_IND, Label.title_Individual));
            typesToReturn.add(new SelectOption(ENRTYPE_COM,  Label.title_CompanyInstitution));
        } else if (enrollmentRT == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)) {
            typesToReturn.add(new SelectOption(ENRTYPE_COM,  Label.title_CompanyInstitution));
        }
        return typesToReturn;
    }

    private Enrollment__c getFreshEnrollment() {
        Enrollment__c enrollment = new Enrollment__c();
        enrollment.RecordTypeId = getEnrollmentRT(schedule.RecordTypeId);
        enrollment.Training_Offer__c = schedule.Id;
        enrollment.The_Same_Data_as_on_Contact__c = true;
        enrollment.Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_CRM;
        return enrollment;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- DATA LOAD METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private List<Consent_Wrapper> getConsents(String entity) {
        List<Consent_Wrapper> enrollmentConsents = new List<Consent_Wrapper>();
        
        List<Catalog__c> consentsDefinitions = [
            SELECT Id, Name 
            FROM Catalog__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS) 
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
            AND Type__c = :CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
            AND (Kind__c = :CommonUtility.CATALOG_KIND_TRAINING
            OR Kind__c = :CommonUtility.CATALOG_KIND_TRAINING_AND_STUDIES)
            AND Consent_API_Name__c != null
            AND ADO__c = :entity
        ];

        for (Catalog__c cd : consentsDefinitions) {
            Consent_Wrapper cw = new Consent_Wrapper();
            Marketing_Campaign__c consent = new Marketing_Campaign__c();
            consent.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_ENROLLMENT);
            consent.Base_Consent__c = cd.Id;
            consent.Consent_Source__c = CommonUtility.MARKETING_CONSENT_SRC_MANUAL;
            consent.Consent_Date__c = System.today();
            cw.consent = consent;

            cw.name = cd.Name;
            cw.checked = false;

            enrollmentConsents.add(cw);
        }

        return enrollmentConsents;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- USER ACTIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public void enrollmentTypeChanged() {
        ew.mainEnrollment = getFreshEnrollment();
        contactPersonName = null;
        nextParticipant = false;
        ew.prList.clear();
    }

    public void removePerson() {
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        Enrollment__c deleted = ew.prList.remove(index);
        if (isEdit) {
            if (deleted.Id != null) {
                enrollmentsToDelete.put(deleted.Id, deleted);
            }
        }
    }

    public void addParticipant() {
        Id contactId = ApexPages.currentPage().getParameters().get('contactId');

        //check for duplicates
        if (checkIfContactAlreadyAdded(contactId)) {
            return;
        }

        addParticipant(contactId);
    }

    private void addParticipant(Id contactId) {
        Enrollment__c participantResult = new Enrollment__c();
        participantResult.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT);
        participantResult.Participant__c = contactId;
        participantResult.Student_Graduate_the_same_University__c = ContactManager.isWSBGraduate(contactId);
        ew.prList.add(participantResult);
        nextParticipant = false;

        if (selectedEnrollmentType == ENRTYPE_IND) {
            ew.mainEnrollment.The_Invoice_after_Payment__c = false;
            fillContactInfo();
        }
    }
    
    private void addCompany(Id companyId) {
        ew.mainEnrollment.Company__c = companyId;
        fillCompanyInfo();
    }

    public void setContactPerson() {
        Id contactId = ApexPages.currentPage().getParameters().get('contactId');
        ew.mainEnrollment.Reporting_Person__c = contactId;
        contactPersonName = ApexPages.currentPage().getParameters().get('name');
    }

    public void nextPerson() {
        nextParticipant = true;
    }

    public PageReference cancel() {
        if (retURL != null) {
            return new PageReference(retURL);
        }
        return null;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private Boolean checkIfContactAlreadyAdded(Id contactId) {
        for (Enrollment__c participant : ew.prList) {
            if (participant.Participant__c == contactId) {
                return true;
            }
        }
        return false;
    }

    public void fillCompanyInfo() {
        if (ew.mainEnrollment.Company__c != null) {
            Account accountData = [SELECT ShippingStreet, Name, ShippingPostalCode, ShippingCity, ShippingCountry, NIP__c 
                                   FROM Account 
                                   WHERE Id = :ew.mainEnrollment.Company__c];

            ew.mainEnrollment.Company_Name__c = accountData.Name;
            ew.mainEnrollment.Street_for_Invoice__c = accountData.ShippingStreet;
            ew.mainEnrollment.Postal_Code_for_Invoice__c = accountData.ShippingPostalCode;
            ew.mainEnrollment.City_for_Invoice__c = accountData.ShippingCity;
            ew.mainEnrollment.Country_for_Invoice__c = accountData.ShippingCountry;
            ew.mainEnrollment.NIP_for_Invoice__c = accountData.NIP__c;  
        }
    }

     public void fillContactInfo() {
        if (ew.prList != null && ew.prList.size() > 0 && ew.mainEnrollment.The_Invoice_after_Payment__c) {
            Contact contactData = [SELECT MailingStreet, FirstName, LastName, MailingPostalCode, MailingCity, MailingCountry 
                                   FROM Contact 
                                   WHERE Id = :ew.prList[0].Participant__c];

            ew.mainEnrollment.First_Name_for_Invoice__c = contactData.FirstName;
            ew.mainEnrollment.Last_Name_for_Invoice__c = contactData.LastName;
            ew.mainEnrollment.Street_for_Invoice__c = contactData.MailingStreet;
            ew.mainEnrollment.Postal_Code_for_Invoice__c = contactData.MailingPostalCode;
            ew.mainEnrollment.City_for_Invoice__c = contactData.MailingCity;
            ew.mainEnrollment.Country_for_Invoice__c = contactData.MailingCountry;
        } else {
            ew.mainEnrollment.First_Name_for_Invoice__c = null;
            ew.mainEnrollment.Last_Name_for_Invoice__c = null;
            ew.mainEnrollment.Street_for_Invoice__c = null;
            ew.mainEnrollment.Postal_Code_for_Invoice__c = null;
            ew.mainEnrollment.City_for_Invoice__c = null;
            ew.mainEnrollment.Country_for_Invoice__c = null;
        }
    }

    private Boolean consentsAccepted() {
        for (Consent_Wrapper cw : ew.consentList) {
            System.debug('cw.checked ' + cw.checked);
            if (!(cw.checked)) {
                return false;
            }
        }
        return true;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- SAVE ACTION ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public PageReference saveEnrollment() {
        if (String.isBlank(contactPersonName)) {
            ew.mainEnrollment.Reporting_Person__c = null;
        }

        if (!isEdit && !consentsAccepted()) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info, Label.info_err_ConsentsHaveToBeAccepted));
            return null;
        }

        if (ew.mainEnrollment.Way_of_Invoice_Delivery__c == byMailPV) {
            if (ew.mainEnrollment.Email_for_Invoice_Delivery__c == null) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info, Label.msg_error_ProvideEmailForInvoice));
                return null;
            }
        } else {
            ew.mainEnrollment.Email_for_Invoice_Delivery__c = null;
        }

        if (selectedEnrollmentType == ENRTYPE_COM && ew.mainEnrollment.Reporting_Person__c == null) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info, Label.msg_error_NoReportingPerson));
            return null;
        }

        if (!isEdit) {
            if (selectedEnrollmentType == ENRTYPE_COM) { //company
                ew.mainEnrollment.The_Invoice_after_Payment__c = true;
                if (schedule.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                    ew.mainEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_NEW;
                    ew.mainEnrollment.Enrollment_Value__c = schedule.Offer_Value__c;
                } else {
                    ew.mainEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES;
                }
            } else { //individual
                ew.mainEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
            }
            ew.mainEnrollment.University__c = schedule.Training_Offer_from_Schedule__r.University_from_Training__c;
        }
        
        Savepoint sp = Database.setSavepoint();
        try{
            //deleting first will prevent accidental duplicates check
            if (isEdit && !enrollmentsToDelete.isEmpty()) {
                List<Enrollment__c> participantsToDelete = new List<Enrollment__c>(enrollmentsToDelete.values());
                new WithoutSharingHelperClass().deleteObjects(participantsToDelete);
            }

            if (!isEdit) {
                ew.mainEnrollment.Consent_Terms_of_Service_Acceptation__c = true;
                ew.mainEnrollment.Consent_Date_processing_for_contract__c = true;
            }

            //main enrollment
            upsert ew.mainEnrollment;

            for (Enrollment__c participant : ew.prList) {
                if (participant.Id == null) {
                    participant.Enrollment_Training_Participant__c = ew.mainEnrollment.Id;
                    participant.Company_from_Participant_Result__c = ew.mainEnrollment.Company__c;
                    participant.Training_Offer_for_Participant__c = ew.mainEnrollment.Training_Offer__c;
                    if (ew.mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {
                        participant.Enrollment_Value__c = (participant.Student_Graduate_the_same_University__c? 
                                                           schedule.Price_per_Person_Student_Graduate__c : schedule.Price_per_Person__c);
                    }

                    if (ew.mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING) 
                        && selectedEnrollmentType == ENRTYPE_COM) {
                        participant.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
                    }
                }
            }
            //child enrollments
            upsert ew.prList;

            // create PUCA if needed
            createUpdateMC3IfNeeded(CommonUtility.getMarketingEntityByUniversityName(schedule.University_Name__c));

        } catch(Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);

            if (!isEdit) {
                ew.mainEnrollment.Id = null;
                for (Enrollment__c pr : ew.prList) {
                    pr.Id = null;
                }
            }

            return null;
        }

        return new PageReference('/' + ew.mainEnrollment.Id);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MARKETING CONSENTS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private void createUpdateMC3IfNeeded(String entity) {
        Set<Id> contactIds = new Set<Id>();
        for (Enrollment__c pr : ew.prList) {
            contactIds.add(pr.Participant__c);            
        }

        List<Contact> contactsToAddMC3 = [
            SELECT Id, Consent_PUCA_ADO__c
            FROM Contact
            WHERE Id IN :contactIds
        ];

        for (Contact contactToAddMC3 : contactsToAddMC3) {
            contactToAddMC3 = MarketingConsentManager.updateConsentOnContact(entity, contactToAddMC3, Contact.Consent_PUCA_ADO__c);
        }
        
        MarketingConsentManager.updateContactWithoutSharingList(contactsToAddMC3);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- WRAPPERS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private class Enrollment_Wrapper {
        public Enrollment__c mainEnrollment { get; set; }
        public List<Enrollment__c> prList { get; set; }
        public List<Consent_Wrapper> consentList { get; set; }

        public Enrollment_Wrapper() {
            prList = new List<Enrollment__c>();
        }
    }

    @TestVisible
    private class Consent_Wrapper {
        public Marketing_Campaign__c consent { get; set; }
        public String name { get; set; }
        public Boolean checked { get; set; }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- WITHOUT SHARING ACTIONS ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private without sharing class WithoutSharingHelperClass {
        public void deleteObjects(List<SObject> objList) {
            delete objList;
        }
    }

}