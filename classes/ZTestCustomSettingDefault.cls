@isTest
private class ZTestCustomSettingDefault {

    @isTest static void testCustomSettingDefault() {
        Integer number_of_training_verifications = [SELECT COUNT() FROM TrainingVerification__c ALL ROWS];
        Test.startTest();
        CustomSettingDefault  csd = new CustomSettingDefault();
        CustomSettingDefault.initStudyAbbreviations();
        CustomSettingDefault.initTrainingVerification();
        System.assertNotEquals(number_of_training_verifications, [SELECT COUNT() FROM TrainingVerification__c ALL ROWS]);
        Test.stopTest();
    }

    @isTest static void testAdditionalCharges() {
        Test.startTest();
        CustomSettingDefault.initAdditionalCharges();
        System.assertEquals(1, AdditionalCharges__c.getAll().size());
        Test.stopTest();
    }
}