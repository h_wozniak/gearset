/**
*   @author         Sebastian Łasisz
*   @description    This class is used to test if functionality used to retrieve contact data
**/
@isTest
private class ZTestIntegrationContactData {

    @isTest static void getContactData_null() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/contact-data/';
        req.addParameter('person_id', null);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationContactData.getContactData();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(400, res.statusCode);
    }

    @isTest static void getContactData() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        ZDataTestUtility.prepareCatalogData(true, false);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(MobilePhone = '1234', Consent_Direct_Communications_ADO__c = CommonUtility.MARKETING_ENTITY_GDANSK), true);
        ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id), true);
        ZDataTestUtility.createWorkExperience(new Qualification__c(Work_Experience__c = candidate.Id), true);

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/contact-data/';
        req.addParameter('person_id', candidate.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationContactData.getContactData();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assertEquals(200, res.statusCode);
    }
}