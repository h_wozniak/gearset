/**
*   @author         Radosław Urbański
*   @description    This class is used to upload document to enrollment.
**/

@RestResource(urlMapping = '/enrollment-document-upload/*')
global without sharing class IntegrationDocumentUpload {
	
	/* ------------------------------------------------------------------------------------------------ */
	/* ---------------------------------------- HTTP METHODS ------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------ */

	@HttpPOST
	global static void uploadDocument() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String enrollmentId = req.params.get('enrollment_id');
		if (enrollmentId == null) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id');
			return;
		}

		String jsonBody = req.requestBody.toString();

		UploadDocumentRequest_JSON parsedJson;

		try {
			parsedJson = parse(jsonBody);
		} catch (Exception ex) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
		}

		if (parsedJson != null) {

			try {
				res.statusCode = 400;

				if (String.isEmpty(parsedJson.document_id)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'document_id');
				} else if (String.isEmpty(parsedJson.uploaded_file_name)) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_name');
				} else if (parsedJson.uploaded_file_content == null) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_content');
				} else {

					List<Enrollment__c> enrollmentList = [SELECT Id FROM Enrollment__c WHERE Id = :enrollmentId];

					if (enrollmentList.isEmpty()) {
						res.responseBody = IntegrationError.getErrorJSONBlobByCode(604);
						return;
					} else {
						List<Enrollment__c> documentList = [SELECT Id FROM Enrollment__c WHERE Id = :parsedJson.document_id];
						if (documentList.isEmpty()) {
							res.responseBody = IntegrationError.getErrorJSONBlobByCode(604);
							return;
						}
						Attachment uploadAttachment = new Attachment(Name = (CommonUtility.UPLOADED_DOCUMENT_PREFIX + parsedJson.uploaded_file_name), Body = parsedJson.uploaded_file_content, 
							ParentId = documentList.get(0).Id);
						insert uploadAttachment;
						IntegrationError.Success_JSON successJSON = new IntegrationError.Success_JSON();
                		successJSON.status_code = 200;
                		successJSON.message = 'success';

                		res.statusCode = 200; 
                		res.responseBody = Blob.valueOf(JSON.serialize(successJSON));
					}
				}
			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
			}
		}
	}

	private static UploadDocumentRequest_JSON parse(String jsonBody) {
		return (UploadDocumentRequest_JSON) System.JSON.deserialize(jsonBody, UploadDocumentRequest_JSON.class);
	}

	/* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    global class UploadDocumentRequest_JSON {
        public String document_id;
        public String uploaded_file_name;
        public Blob uploaded_file_content;
    }
}