/**
*   @author         Sebastian Lasisz
*   @description    Utility class, that is used to store globally used variables, methods, etc...
**/
public with sharing class LEX_GenerateAttestationController {

    /**
     * @author Sebastian Lasisz
     *
     * Method that check whether user has edit access on record.
     * @param recordId
     *
     * @return
     */
    @AuraEnabled public static Boolean hasEditAccess(String recordId) {
        return LEXServiceUtilities.hasEditAccess(recordId);
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method that checks whether enrollment is for proper degree.
     * @param recordId Id of study Enrollment.
     *
     * @return Bool's value if attestation document can be created for given study enrollment.
     */
    @AuraEnabled public static Boolean checkRecord(String recordId) {
        Enrollment__c studyEnrollment = [
                SELECT Id, Degree__c
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        if (studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            return false;
        }

        return true;
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for upserting Attestation document (executed when user click button on layout).
     * @param enrollmentId Study enrollment Id to create or update new attestation document.
     *
     * @return Wrapper containing whether operation was successful. Additionally contains Id of newly created document or error message.
     */
    @AuraEnabled public static Result_JSON createAttestationDocument(Id enrollmentId) {
        Id attestationDocumentId = CatalogManager.getDocumentAttestationId();

        if (attestationDocumentId != null) {
            List<Enrollment__c> attestationDocuments = [
                    SELECT Id
                    FROM Enrollment__c
                    WHERE Enrollment_from_Documents__c = :enrollmentId
                    AND Document__c = :attestationDocumentId
            ];

            Enrollment__c attestationDocument;
            if (!attestationDocuments.isEmpty()) {
                attestationDocument = attestationDocuments.get(0);
                attestationDocument.Current_File__c = CommonUtility.DURING_GENERATION;
            }
            else {
                attestationDocument = DocumentManager.createDocumentEnrollment(enrollmentId, attestationDocumentId, null);
                attestationDocument.Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH;
            }

            try {
                CommonUtility.skipDocumentValidations = true;
                upsert attestationDocument;
            } catch (Exception e) {
                ErrorLogger.configMsg(e.getMessage());
                return new Result_JSON(false, CommonUtility.trimErrorMessage(e.getMessage()));
            }

            return new Result_JSON(true, attestationDocument.Id);
        } else {
            ErrorLogger.configMsg(CommonUtility.DOCUMENT_UNDEFINED_ERROR + ' ' + attestationDocumentId);
            return new Result_JSON(false, (CommonUtility.DOCUMENT_UNDEFINED_ERROR + ' ' + attestationDocumentId));
        }
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for generating PDF document for given enrollment Id.
     * @param enrollmentId Id of document enrollment.
     *
     * @return Bool's value whether generation was successful.
     */
    @AuraEnabled public static Boolean generateAttestationDocument(Id enrollmentId) {
        Enrollment__c attestation = [
                SELECT Language_of_Enrollment__c, University_Name__c, Reference_Number__c, Degree__c, Enrollment_from_Documents__r.Degree__c
                FROM Enrollment__c
                WHERE Id = :enrollmentId
        ];

        try {
            DocumentGeneratorManager.generateDocumentSync(
                    attestation.Language_of_Enrollment__c,
                    RecordVals.CATALOG_DOCUMENT_ATTESTATION,
                    attestation.Enrollment_from_Documents__r.Degree__c,
                    null,
                    attestation.Reference_Number__c,
                    enrollmentId,
                    true,
                    false
            );

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER WRAPPER ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class Result_JSON {
        @AuraEnabled public Boolean success { get; set; }
        @AuraEnabled public String message {get; set; }

        public Result_JSON(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
}