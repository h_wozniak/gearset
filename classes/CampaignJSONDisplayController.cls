/**
* @author       Sebastian Łasisz
* @description  Controller for CampaignJSONDisplay.
**/

public with sharing class CampaignJSONDisplayController {

    public Id campaignId { get; set; }
    public Marketing_Campaign__c campaign { get; set; }

    public String campaignJSON { get; set; }

    public CampaignJSONDisplayController() {
        campaignId = Apexpages.currentPage().getParameters().get('campaignId');
        
        campaign = [SELECT Campaign_JSON__c FROM Marketing_Campaign__c WHERE Id = :campaignId];

        if (campaign.Campaign_JSON__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_NoCampaignJSON));
        } else {
            // deprettyfy
            campaignJSON = JSON.serialize(
                JSON.deserialize(campaign.Campaign_JSON__c, IntegrationManager.Prospect_JSON.class)
            );
        }
    }
}