@isTest
private class ZTestImportController {



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- HELPER METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static User createPlLangUser_forTrainings() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SALES_TRAINING];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Jan Opiekun',
                LanguageLocaleKey = 'pl',
                LocaleSidKey = 'pl',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    private static User createPlLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'pl',
                LocaleSidKey = 'pl',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- POSITIVE CASES ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_positive_ImportTrainings() {
        User u = createPlLangUser_forTrainings();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_TOR), true);
            ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_GDA), true);
            ZDataTestUtility.createContactPerson(new Contact(FirstName = 'Jan', LastName = 'Szkoleniowiec'), true);

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportTrainings' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_TRAININGS;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> toList = [SELECT Id FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)];
            System.assertEquals(3, toList.size());


            List<Offer__c> schList = [
                    SELECT Id
                    FROM Offer__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                    OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
            ];

            System.assertEquals(4, schList.size());
        }
    }



    @isTest static void test_positive_ImportCourses() {
        User u = createPlLangUser_forStudies();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Account univ = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                    University__c = univ.Id
            ), true);

            ZDataTestUtility.createRandomUser('Jan Opiekun');

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportCourses' LIMIT 1];

            cntrl.helperOffer.University_Study_Offer_from_Course__c = univOfferStudy.Id;

            cntrl.importType = cntrl.IMPORT_COURSES;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> courseList = [SELECT Id FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)];
            System.assertEquals(3, courseList.size());

            List<Offer__c> langList = [SELECT Id FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE)];
            System.assertEquals(6, langList.size());
        }
    }



    @isTest static void test_positive_ImportSpecialties() {
        User u = createPlLangUser_forStudies();

        CustomSettingDefault.initStudyAbbreviations();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Account univ = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                    University__c = univ.Id,
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Study_Start_Date__c = Date.newInstance(2016, 3, 1)
            ), true);


            Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                    University_Study_Offer_from_Course__c = univOfferStudy.Id,
                    Short_Name__c = 'ABC',
                    Kind__c = CommonUtility.OFFER_KIND_ENGINEERING,
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME
            ), true);

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportSpecialties' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_SPECIALTIES;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> specList = [
                    SELECT Id
                    FROM Offer__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
            ];

            System.assertEquals(4, specList.size());
        }
    }

    @isTest static void test_positive_ImportCoursesAndSpecialties() {
        User u = createPlLangUser_forStudies();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Account univ = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                    University__c = univ.Id
            ), true);

            ZDataTestUtility.createRandomUser('Jan Opiekun');

            ImportController cntrl = new ImportController();

            StaticResource srCourses = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportCourses_REF' LIMIT 1];
            StaticResource srSpecs = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportSpecialties_REF' LIMIT 1];

            cntrl.helperOffer.University_Study_Offer_from_Course__c = univOfferStudy.Id;

            cntrl.importType = cntrl.IMPORT_COURSESPEC;
            cntrl.fileBody1 = srCourses.Body;
            cntrl.fileBody2 = srSpecs.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> courseList = [SELECT Id FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)];
            System.assertEquals(3, courseList.size());

            List<Offer__c> langList = [SELECT Id FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE)];
            System.assertEquals(6, langList.size());

            List<Offer__c> specList = [
                    SELECT Id
                    FROM Offer__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
            ];

            System.assertEquals(4, specList.size());
        }
    }


    @isTest static void test_positive_PriceBooks() {
        User u = createPlLangUser_forStudies();

        CustomSettingDefault.initStudyAbbreviations();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Account univ = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                    University__c = univ.Id,
                    Degree__c = CommonUtility.OFFER_DEGREE_I,
                    Study_Start_Date__c = Date.newInstance(2016, 3, 1)
            ), true);


            Offer__c courseOffer1 = ZDataTestUtility.createCourseOffer(new Offer__c(
                    University_Study_Offer_from_Course__c = univOfferStudy.Id,
                    Short_Name__c = 'ABC',
                    Kind__c = CommonUtility.OFFER_KIND_ENGINEERING,
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME
            ), true);

            Offer__c courseOffer2 = ZDataTestUtility.createCourseOffer(new Offer__c(
                    University_Study_Offer_from_Course__c = univOfferStudy.Id,
                    Short_Name__c = 'DEF',
                    Kind__c = CommonUtility.OFFER_KIND_ENGINEERING,
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME
            ), true);


            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportPriceBooks' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_PRICEBOOKS;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> pbList = [
                    SELECT Id
                    FROM Offer__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)
            ];
        }
    }


    @isTest static void test_positive_PriceBooks_Onetime() {
        User u = createPlLangUser_forStudies();

        CustomSettingDefault.initStudyAbbreviations();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Account univ = ZDataTestUtility.createUniversity(new Account(Name = RecordVals.WSB_NAME_WRO), true);
            Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                    University__c = univ.Id,
                    Degree__c = CommonUtility.OFFER_DEGREE_MBA,
                    Study_Start_Date__c = Date.newInstance(2016, 3, 1)
            ), true);


            Offer__c courseOffer1 = ZDataTestUtility.createCourseOffer(new Offer__c(
                    University_Study_Offer_from_Course__c = univOfferStudy.Id,
                    Short_Name__c = 'ABC',
                    Kind__c = CommonUtility.OFFER_KIND_COM_MBA,
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME
            ), true);

            Offer__c courseOffer2 = ZDataTestUtility.createCourseOffer(new Offer__c(
                    University_Study_Offer_from_Course__c = univOfferStudy.Id,
                    Short_Name__c = 'DEF',
                    Kind__c = CommonUtility.OFFER_KIND_COM_MBA,
                    Mode__c = CommonUtility.OFFER_MODE_FULL_TIME
            ), true);

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportPriceBooks_Onetime' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_PRICEBOOKS;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Offer__c> pbList = [
                    SELECT Id, Onetime_Price__c, Installment_Variants__c
                    FROM Offer__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
            ];
        }
    }

    @isTest static void test_positive_PromoCode() {
        User u = createPlLangUser_forStudies();

        CustomSettingDefault.initStudyAbbreviations();

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            Discount__c codeDiscount = ZDataTestUtility.createCodeDiscount(new Discount__c(Name = 'Promocje na Wrocław'), true);

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportPromoCode' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_PROMOCODES;
            cntrl.fileBody1 = sr.Body;

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();

            List<Discount__c> promoCodeList = [
                    SELECT Id
                    FROM Discount__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_PROMO_CODE)
            ];

            System.assertEquals(9, promoCodeList.size());
        }
    }

    @isTest static void test_positive_ReturnedLeads() {
        User u = createPlLangUser_forStudies();
        CustomSettingDefault.initStudyAbbreviations();
        ZDataTestUtility.prepareCatalogData(true, true);

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            ImportController cntrl = new ImportController();
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportReturnedLeads' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_RETURNEDLEADS;
            cntrl.fileBody1 = sr.Body;
            cntrl.importSeparator = 'TAB';

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(pageMessages.size(), 0);

            cntrl.acceptImport();
            Test.stopTest();
        }

        List<Marketing_Campaign__c> returnedLeads = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_RETURNING_LEADS)
        ];

        System.assertEquals(returnedLeads.size(), 1);
    }

    @isTest static void test_positive_iPressoContacts() {
        User u = createPlLangUser_forStudies();

        CustomSettingDefault.initStudyAbbreviations();

        List<Contact> contacts = ZDataTestUtility.createCandidateContacts(null, 2, true);

        String fileValue = '';
        Integer listSize = contacts.size();
        for (Contact c : contacts) {
            if (listSize > 0) {
                fileValue += c.Id + '\r\n';
                listSize--;
            }
            else {
                fileValue += c.Id;
            }
        }

        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            ImportController cntrl = new ImportController();
            Marketing_Campaign__c helperCampaign = ZDataTestUtility.createMarketingCampaigniPresso(null, true);
            cntrl.helperCampaign.Campaign_Member__c = helperCampaign.Id;

            cntrl.importType = cntrl.IMPORT_IPRESSOCONTACTS;
            cntrl.fileBody1 = Blob.valueOf(fileValue);

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();
        }
    }

    @isTest static void test_positive_TrainingParticipants() {
        User u = createPlLangUser_forStudies();
        CustomSettingDefault.initStudyAbbreviations();
        CommonUtility.skipNameEditDisabling = true;

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Adam', LastName = 'Nowak', Email = 'adam.nowak.test@testx.com'), true);
        Offer__c training = ZDataTestUtility.createOpenTrainingSchedule(null, true);
        training.Name = 'TS-00009-001';
        update training;
        Enrollment__c trainingEnrollment = ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(Training_Offer__c = training.Id), true);
        Enrollment__c participantResults = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(Participant__c = candidate.Id,
                Training_Offer_for_Participant__c = training.Id, Enrollment_Training_Participant__c = trainingEnrollment.Id), true);


        System.runAs(u) {
            Test.setCurrentPage(Page.Import);

            ImportController cntrl = new ImportController();

            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'TEST_ImportTrainingParticipants' LIMIT 1];

            cntrl.importType = cntrl.IMPORT_TRAININGPARTICIPANTS;
            cntrl.fileBody1 = sr.Body;
            cntrl.importSeparator = 'TAB';

            Test.startTest();
            cntrl.importData();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size());

            cntrl.acceptImport();
            Test.stopTest();
        }
    }
}