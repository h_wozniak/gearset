global class DocGen_GenerateFullDegreeName implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateFullDegreeName() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateFullDegreeName.generateFullDegreeName(objectId);  
    }

    global static String generateFullDegreeName(String objectId) {
        Enrollment__c enrollment = [
            SELECT Degree__c, Enrollment_from_Documents__r.Degree__c, Language_of_Main_Enrollment__c
            FROM Enrollment__c 
            WHERE Id =: objectId
        ];
        
        String degree = '';

        if (enrollment.Degree__c != null) {
            degree = enrollment.Degree__c;
        }
        else {
            degree = enrollment.Enrollment_from_Documents__r.Degree__c;
        }

        if (enrollment.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_POLISH) {
            if (degree == CommonUtility.OFFER_DEGREE_I) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_I.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_II.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_II_PG.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_U) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_U.toLowerCase();
            }   
            if (degree == CommonUtility.OFFER_DEGREE_PG) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_PG.toLowerCase();
            }  
            if (degree == CommonUtility.OFFER_DEGREE_MBA) {
                return DocumentGeneratorHelper.PL_OFFER_DEGREE_FULL_MBA;
            }  
        }
        else if (enrollment.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
            if (degree == CommonUtility.OFFER_DEGREE_I) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_I.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_II.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_II_PG.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_U) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_U.toLowerCase();
            }   
            if (degree == CommonUtility.OFFER_DEGREE_PG) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_PG.toLowerCase();
            }  
            if (degree == CommonUtility.OFFER_DEGREE_MBA) {
                return DocumentGeneratorHelper.EN_OFFER_DEGREE_FULL_MBA;
            }  
        }
        else if (enrollment.Language_of_Main_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN) {
            if (degree == CommonUtility.OFFER_DEGREE_I) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_I.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_II.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_II_PG.toLowerCase();
            }
            if (degree == CommonUtility.OFFER_DEGREE_U) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_U.toLowerCase();
            }   
            if (degree == CommonUtility.OFFER_DEGREE_PG) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_PG.toLowerCase();
            }  
            if (degree == CommonUtility.OFFER_DEGREE_MBA) {
                return DocumentGeneratorHelper.RU_OFFER_DEGREE_FULL_MBA;
            }  
        }

        return '';
    }
}