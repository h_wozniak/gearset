@isTest
private class ZTestGeneratePriceBookOnEnrollmentXML {
    
    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];
        
        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }
        
        update pb.InstallmentConfigs__r;
    }
    
    @isTest static void testGeneratePriceBookXML() {
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();
        
        String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(studyEnrollment.Id, null, null);
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutPriceBook() {
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c( 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();
        
        try {
            String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(studyEnrollment.Id, null, null);
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }        
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutEnrollment() {
        Test.startTest();
        try {
            String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(null, null, null);
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }
        Test.stopTest();
        
    }
    
    @isTest static void testGeneratePriceBookXML_SP_Opole() {
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_PG, Study_Start_Date__c = System.today()), true);

        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5, University_Study_Offer_from_Course__c = uniOffer.Id
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Price_Book_from_Enrollment__c = priceBook.Id,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();

        enxoodocgen__Document_Template__c  template = new enxoodocgen__Document_Template__c ();
        template.Name = '[Test][' + CommonUtility.OFFER_DEGREE_MBA + '][' + RecordVals.WSB_NAME_OPO + ']';

        insert template;
        
        String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(studyEnrollment.Id, template.Id, 'PriceBookSP');
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGeneratePriceBookXML_SP_Wroclaw() {
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_PG, Study_Start_Date__c = System.today()), true);

        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5, University_Study_Offer_from_Course__c = uniOffer.Id
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Price_Book_from_Enrollment__c = priceBook.Id,
                Installments_per_Year__c = '2'),
            true);
        Test.stopTest();

        enxoodocgen__Document_Template__c  template = new enxoodocgen__Document_Template__c ();
        template.Name = '[Test][' + CommonUtility.OFFER_DEGREE_MBA + '][' + RecordVals.WSB_NAME_WRO + ']';

        insert template;
        
        String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(studyEnrollment.Id, template.Id, 'PriceBookSP');
        
        System.assertNotEquals('', result);
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutPriceBook_SP() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_PG, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));

        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c( 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2',
                Course_or_Specialty_Offer__c = retMap.get(1).Id),
            true));
        Test.stopTest();        
        
        try {
            String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(retMap.get(2).Id, null, 'PriceBookSP');
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }
    }
    
    @isTest static void testGeneratePriceBookXMLWithoutEnrollment_SP() {
        Test.startTest();
        try {
            String result = DocGen_GeneratePriceBookOnEnrollment.executeMethod(null, null, 'PriceBookSP');
        }
        catch (Exception e) {
            System.assertEquals(String.valueOf(e), 'System.QueryException: List has no rows for assignment to SObject');
        }
        Test.stopTest();        
    }
}