/**
* @author       Wojciech Słodziak
* @description  Batch class for creating or updating Educational Agreements. At the end callout to Experia is invoked.
* @notes        TO BE CALLED WITH BATCH SIZE = 1
**/

global class EducationalAgreementUpsertBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    Set<Id> studyEnrIds;
    Boolean sendingPayments;
    Boolean sendingInitialFees;
    List<ActivityManager.ChangeStatusActivity> contactsToSendAsActivity = new List<ActivityManager.ChangeStatusActivity>();
    
    global EducationalAgreementUpsertBatch(Set<Id> studyEnrIds, Boolean sendingPayments, Boolean sendingInitialFees) {
        this.studyEnrIds = studyEnrIds;
        this.sendingPayments = sendingPayments;
        this.sendingInitialFees = sendingInitialFees;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Candidate_Student__c, Educational_Agreement__c, Course_or_Specialty_Offer__c, Candidate_Student__r.Statuses__c
            FROM Enrollment__c 
            WHERE Id IN :studyEnrIds
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<Id> contactIds = new Set<Id>();
        for (Enrollment__c studyEnr : (List<Enrollment__c>) scope) {
            contactIds.add(studyEnr.Candidate_Student__c);
            EducationalAgreementUpsertBatch.createOrUpdateEducationalAgreement(studyEnr);
        }   

        List<Contact> contacts = [
            SELECT Id, Statuses__c, LastModifiedDate
            FROM Contact
            WHERE Id IN :contactIds
        ];

        for (Contact contact : contacts) {
            for (Enrollment__c studyEnr : (List<Enrollment__c>) scope) {
                if (studyEnr.Candidate_Student__c == contact.Id) {
                    contactsToSendAsActivity.add(new ActivityManager.ChangeStatusActivity(contact.Id, studyEnr.Candidate_Student__r.Statuses__c, contact.Statuses__c, contact.LastModifiedDate));
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            IntegrationEnrollmentSender.sendEnrollmentList(studyEnrIds, sendingPayments, sendingInitialFees);
            System.enqueueJob(new ChangeStatusOnContactActivity(contactsToSendAsActivity, 'zmiana_statusu', 'poprzedni_status', 'obecny_status'));
        }
    }


    // method for creating/updating Enrollment record of Enrollment_Educational_Agreement RT for future managment of Candidate's education
    // after initial enrollment process is finished, or annexed
    public static void createOrUpdateEducationalAgreement(Enrollment__c studyEnr) {
        // copy data from enrollment to Educational Agreement
        Enrollment__c educationalAgr = studyEnr.clone(false, true, false, false);
        educationalAgr.Source_Enrollment__c = studyEnr.Id;
        
        // if studyEnr has no Educational Agreement yet then it is set to null and inserted with upsert call
        educationalAgr.Id = studyEnr.Educational_Agreement__c;
        educationalAgr.Educational_Agreement__c = null;
        educationalAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        educationalAgr.Student_from_Educational_Agreement__c = educationalAgr.Candidate_Student__c;
        educationalAgr.Candidate_Student__c = null;
        educationalAgr.Course_or_Specialty_from_EduAgr__c = educationalAgr.Course_or_Specialty_Offer__c;
        educationalAgr.Course_or_Specialty_Offer__c = null;
        upsert educationalAgr;

        if (studyEnr.Educational_Agreement__c == null) {
            // attach new Educational Agreement to studyEnr
            Enrollment__c studyEnrToUpdate = new Enrollment__c(
                Id = studyEnr.Id,
                Educational_Agreement__c = educationalAgr.Id
            );
            update studyEnrToUpdate;
        }
    }

}