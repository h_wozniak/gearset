/**
*   @author         Sebastian Łasisz
*   @description    batch class that creates records from Returned Leads Import
**/

global class ImportReturnedLeadsBatch  implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    List<Marketing_Campaign__c> returnedLeadsToInsert;
    Integer counter;
    List<String> failedBatchesCounter = new List<String>();
    String userEmail;
    Integer numberOfBatches;
    Set<Id> contactIdsToSendToIpresso;
    Map<String, String> contactsToUpdateInOtherSystem = new Map<String, String>();

    global ImportReturnedLeadsBatch(List<Marketing_Campaign__c> returnedLeadsToInsert, String userEmail, Integer numberOfBatches) {
        this.returnedLeadsToInsert = returnedLeadsToInsert;
        this.userEmail = userEmail;
        this.numberOfBatches = numberOfBatches;
        contactIdsToSendToIpresso = new Set<Id>();
        counter = 0;
    }

    global Iterable<sObject> start(Database.BatchableContext BC) {
        return returnedLeadsToInsert;
    }

    global void execute(Database.BatchableContext BC, List<Marketing_Campaign__c> returnedLeads) {
        counter++;
        Savepoint sp = Database.setSavepoint();

        try {
            CommonUtility.preventSendingContactToIPressoAsAsync = true;
            CommonUtility.preventsSendingCalloutAsAtFuture = true;
            CommonUtility.preventSendingContactUpdateCalloutsAsAtFuture = true;
            CommonUtility.skipContactUpdateInOtherSystems = true;
            insert returnedLeads;
        }
        catch (Exception e) {
            failedBatchesCounter.add('(' + counter + ', ' + numberOfBatches + ')');
            ErrorLogger.log(e);
            Database.rollback(sp);
        }

        /* Prepare map to send contacts to other systems synchronously */
        Set<Id> contactsToSendToIpresso = new Set<Id>();
        for (Marketing_Campaign__c returnedLead : returnedLeads) {
            if (returnedLead.Contact_From_Returning_Leads__c != null) {
                contactsToUpdateInOtherSystem.put(returnedLead.Contact_From_Returning_Leads__c + ';' + contactsToUpdateInOtherSystem.size(), RecordVals.UPDATING_SYSTEM_MA + ';' + RecordVals.UPDATING_SYSTEM_EXPERIA);
            }
        }

        contactIdsToSendToIpresso.addAll(CommonUtility.contactIdsToSendToIpresso);
    }

    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            if (contactIdsToSendToIpresso.size() <= 10) {
                IntegrationIpressoContactCreationSender.sendIPressoContactList(contactIdsToSendToIpresso);
            } else {
                IntegrationIpressoContactCreationBatch ipressoBatch = new IntegrationIpressoContactCreationBatch(contactIdsToSendToIpresso);
                Database.executebatch(ipressoBatch, 10);
            }
        }

        CommonUtility.preventSendingContactUpdateCalloutsAsAtFuture = true;
        IntegrationContactUpdateOther.sendUpdatedContactToOtherSystemsSync(contactsToUpdateInOtherSystem);

        sendEmailToUser('Import zwrotek został zakończony', userEmail, failedBatchesCounter);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static void sendEmailToUser(String msg, String email, List<String> failedBatchesCounter) {
        if (email != null) {
            if(msg != null) {
                String finalMsg = 'Następująca wiadomość została wysłana z systemu:\n' + msg + '\n\n';

                if (failedBatchesCounter.size() > 0) {
                    finalMsg += 'Rekordy, które nie zostały zaimportowane: ' + '\n';
                    for (String failedBatchCounter : failedBatchesCounter) {
                        finalMsg += failedBatchCounter + '\n';
                    }
                    finalMsg += 'Należy poprawić dane w pliku do importu, a następnie zaimportować je ponownie.' + '\n';
                }

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new List<String> { email });
                mail.setSubject('Import zwrotek');
                mail.setPlainTextBody(finalMsg);

                try {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                } catch(Exception unhandledExc) {}
            }
        }
    }
}