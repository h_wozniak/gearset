public without sharing class TaskManager {
    
    // adds tasks assigned to training administrator related to training schedules (for Type Open)  
    public static void assignTasks(Set<Id> trainingScheduleIdsToAssignTasks) {
        List<Offer__c> trainingSchedules = [
            SELECT Id, Valid_From__c, Valid_To__c, RecordTypeId, Training_Offer_from_Schedule__r.Training_Administrator__c, Training_Offer_from_Schedule__r.Type__c  
            FROM Offer__c 
            WHERE Id in :trainingScheduleIdsToAssignTasks
            AND (Training_Offer_from_Schedule__r.Type__c = :CommonUtility.OFFER_TYPE_OPEN 
            OR Training_Offer_from_Schedule__r.Type__c = :CommonUtility.OFFER_TYPE_CLOSED)
        ];

        if (!trainingSchedules.isEmpty()) {
            CustomSettingDefault.initTrainingVerification();
            TrainingVerification__c listVerification = TrainingVerification__c.getInstance('ListVerification');
            TrainingVerification__c groupVerification = TrainingVerification__c.getInstance('GroupVerification');
            TrainingVerification__c paymentVerification = TrainingVerification__c.getInstance('PaymentVerification');
            TrainingVerification__c closedPaymentVerification = TrainingVerification__c.getInstance('ClosedOfferPaymentVerification');

            List<Task> taskReminders = new List<Task>();
            Task newTask1, newTask7, newTask10, newTaskClosed;

            for (Offer__c tS : trainingSchedules) {
                if (tS.Training_Offer_from_Schedule__r.Type__c == CommonUtility.OFFER_TYPE_OPEN && tS.Training_Offer_from_Schedule__r.Training_Administrator__c != null) {
                    if (listVerification != null) {
                        newTask1 = new Task();
                        newTask1.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_DEFAULT);
                        newTask1.IsReminderSet = true;
                        newTask1.Status = listVerification.Status__c;
                        newTask1.Priority = listVerification.Priority__c;
                        newTask1.Subject = listVerification.Subject__c;
                        newTask1.ActivityDate = tS.Valid_To__c + Integer.ValueOf(listVerification.Reminder_Difference__c);
                        newTask1.ReminderDateTime = DateTime.newInstance(newTask1.ActivityDate, Time.newInstance(1, 0, 1, 0));                      
                        newTask1.WhatId = tS.Id;
                        newTask1.OwnerId = tS.Training_Offer_from_Schedule__r.Training_Administrator__c;
                        newTask1.Created_from_Trigger__c = true;
                        taskReminders.add(newTask1);
                    }
                    if (groupVerification != null) {
                        newTask7 = new Task();
                        newTask7.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_DEFAULT);
                        newTask7.IsReminderSet = true;
                        newTask7.Status = groupVerification.Status__c;
                        newTask7.Priority = groupVerification.Priority__c;
                        newTask7.Subject = groupVerification.Subject__c;
                        newTask7.ActivityDate = tS.Valid_From__c - Integer.ValueOf(groupVerification.Reminder_Difference__c);
                        newTask7.ReminderDateTime = DateTime.newInstance(newTask7.ActivityDate, Time.newInstance(1, 0, 1, 0));                      
                        newTask7.WhatId = tS.Id;
                        newTask7.OwnerId = tS.Training_Offer_from_Schedule__r.Training_Administrator__c;
                        newTask7.Created_from_Trigger__c = true;
                        taskReminders.add(newTask7);
                    }
                    if (paymentVerification != null) {
                        newTask10 = new Task();
                        newTask10.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_DEFAULT);
                        newTask10.IsReminderSet = true;
                        newTask10.Status = paymentVerification.Status__c;
                        newTask10.Priority = paymentVerification.Priority__c;
                        newTask10.Subject = paymentVerification.Subject__c;
                        newTask10.ActivityDate = tS.Valid_From__c - Integer.ValueOf(paymentVerification.Reminder_Difference__c);
                        newTask10.ReminderDateTime = DateTime.newInstance(newTask10.ActivityDate, Time.newInstance(1, 0, 1, 0));                        
                        newTask10.WhatId = tS.Id;
                        newTask10.OwnerId = tS.Training_Offer_from_Schedule__r.Training_Administrator__c;
                        newTask10.Created_from_Trigger__c = true;
                        taskReminders.add(newTask10);   
                    }
                } else if (tS.Training_Offer_from_Schedule__r.Type__c == CommonUtility.OFFER_TYPE_CLOSED) {
                    if (closedPaymentVerification != null) {
                        newTaskClosed = new Task();
                        newTaskClosed.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_DEFAULT);
                        newTaskClosed.IsReminderSet = true;
                        newTaskClosed.Status = closedPaymentVerification.Status__c;
                        newTaskClosed.Priority = closedPaymentVerification.Priority__c;
                        newTaskClosed.Subject = closedPaymentVerification.Subject__c;
                        newTaskClosed.ActivityDate = tS.Valid_To__c + Integer.ValueOf(closedPaymentVerification.Reminder_Difference__c);
                        newTaskClosed.ReminderDateTime = DateTime.newInstance(newTaskClosed.ActivityDate, Time.newInstance(1, 0, 1, 0));                        
                        newTaskClosed.WhatId = tS.Id;
                        newTaskClosed.OwnerId = tS.Training_Offer_from_Schedule__r.Training_Administrator__c;
                        newTaskClosed.Created_from_Trigger__c = true;
                        taskReminders.add(newTaskClosed);
                    }
                }                                    
            }
            
            try {
                insert taskReminders;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }
    }
    
    /* Mateusz Pruszyński */
    // if training administrator gets changed, all related tasks should be reassigned  
    public static void reasignTasksOnTrainingAdministratorChange(Set<Id> trainingOfferIdsToReassignTasks) {
        List<Offer__c> trainingSchedules = [
            SELECT Id, Training_Offer_from_Schedule__r.Training_Administrator__c 
            FROM Offer__c 
            WHERE Training_Offer_from_Schedule__c in :trainingOfferIdsToReassignTasks
        ];

        if (!trainingSchedules.isEmpty()) {
            Set<Id> whatIds = new Set<Id>();
            for (Offer__c tS : trainingSchedules) {
                whatIds.add(tS.Id);
            }

            List<Task> taskReminders = [
                SELECT Id, WhatId 
                FROM Task 
                WHERE WhatId in :whatIds 
                AND Created_from_Trigger__c = true
            ];

            if (!taskReminders.isEmpty()) {
                for (Task singleTask : taskReminders) {
                    for (Offer__c tS : trainingSchedules) {
                        if (singleTask.WhatId == tS.Id) {
                            singleTask.OwnerId = tS.Training_Offer_from_Schedule__r.Training_Administrator__c;
                            break;
                        }
                    }
                }

                try {
                    update taskReminders;
                } catch (Exception ex) {
                    ErrorLogger.log(ex);
                }
            }
        }           
    }
    
    /* Mateusz Pruszyński */
    // if schedule start date gets changed, all related tasks' reminders should get changed (due date)  
    public static void changeTasksReminersOnScheduleStartDateChange(Set<Id> trainingScheduleIdsUpdateTasksFrom) {
        List<Offer__c> trainingSchedules = [
            SELECT Id, Valid_From__c 
            FROM Offer__c 
            WHERE Id in :trainingScheduleIdsUpdateTasksFrom 
            AND Training_Offer_from_Schedule__r.Type__c = :CommonUtility.OFFER_TYPE_OPEN
        ];

        if (!trainingSchedules.isEmpty()) {
            CustomSettingDefault.initTrainingVerification();
            TrainingVerification__c groupVerification = TrainingVerification__c.getInstance('GroupVerification');
            TrainingVerification__c paymentVerification = TrainingVerification__c.getInstance('PaymentVerification');

            List<Task> taskReminders = [SELECT Id, WhatId, Subject 
                                        FROM Task 
                                        WHERE WhatId in :trainingScheduleIdsUpdateTasksFrom]; 

            if (!taskReminders.isEmpty()) {
                for (Task singleTask : taskReminders) {
                    for (Offer__c tS : trainingSchedules) {
                        if (singleTask.WhatId == tS.Id) {
                            if (singleTask.Subject == groupVerification.Subject__c) {
                                singleTask.IsReminderSet = true;
                                singleTask.ActivityDate = tS.Valid_From__c - Integer.ValueOf(groupVerification.Reminder_Difference__c);
                                singleTask.ReminderDateTime = DateTime.newInstance(singleTask.ActivityDate, Time.newInstance(1, 0, 1, 0));
                            }
                            else if (singleTask.Subject == paymentVerification.Subject__c) {
                                singleTask.IsReminderSet = true;
                                singleTask.ActivityDate = tS.Valid_From__c - Integer.ValueOf(paymentVerification.Reminder_Difference__c);
                                singleTask.ReminderDateTime = DateTime.newInstance(singleTask.ActivityDate, Time.newInstance(1, 0, 1, 0));                                  
                            }
                        }
                    }
                }

                try {
                    update taskReminders
;                } catch (Exception ex) {
                    ErrorLogger.log(ex);
                }
            }                   
        }
    }
    
    /* Mateusz Pruszyński */
    // if schedule end date gets changed, all related tasks' reminders should get changed (due date)  
    public static void changeTasksRemindersOnScheduleEndDateChagen(Set<Id> trainingScheduleIdsUpdateTasksTo) {
        List<Offer__c> trainingSchedules = [
            SELECT Id, Valid_To__c 
            FROM Offer__c 
            WHERE Id in :trainingScheduleIdsUpdateTasksTo 
            AND (Training_Offer_from_Schedule__r.Type__c = :CommonUtility.OFFER_TYPE_OPEN 
            OR Training_Offer_from_Schedule__r.Type__c = :CommonUtility.OFFER_TYPE_CLOSED)
        ];

        if (!trainingSchedules.isEmpty()) {
            CustomSettingDefault.initTrainingVerification();
            TrainingVerification__c listVerification = TrainingVerification__c.getInstance('ListVerification');

            List<Task> taskReminders = [
                SELECT Id, WhatId 
                FROM Task 
                WHERE WhatId in :trainingScheduleIdsUpdateTasksTo 
                AND Subject = :listVerification.Subject__c
            ];

            if (!taskReminders.isEmpty()) {
                for (Task oldTask1 : taskReminders) {
                    for (Offer__c tS : trainingSchedules) {
                        if (oldTask1.WhatId == tS.Id) {                      
                            oldTask1.IsReminderSet = true;
                            oldTask1.ActivityDate = tS.Valid_To__c + Integer.ValueOf(listVerification.Reminder_Difference__c);
                            oldTask1.ReminderDateTime = DateTime.newInstance(oldTask1.ActivityDate, Time.newInstance(1, 0, 1, 0));                          
                        }
                    }
                }

                try {
                    update taskReminders;
                } catch (Exception ex) {
                    ErrorLogger.log(ex);
                }                   
            }
        }
    }
    
    /* Sebastian Łasisz */
    // remove tasks from not launched training
    public static void removeTasksFromNotLaunchedTraining(Set<Id> schedulesWithTasksToDelete) {
        List<Task> currentTasksOnTraining = [SELECT Id, OwnerId, Created_from_Trigger__c, Status 
                                             FROM Task 
                                             WHERE WhatId IN :schedulesWithTasksToDelete 
                                             AND Status != :CommonUtility.TASK_STATUS_COMPLETED
                                             AND Created_from_Trigger__c = true
                                             AND RecordTypeId =: CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_DEFAULT)];
        try {
            delete currentTasksOnTraining;
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
}