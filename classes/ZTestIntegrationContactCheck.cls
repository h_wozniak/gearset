/**
*   @author         Radosław Urbański
*   @description    This class is used to test functionality of IntegrationContactCheck which should inform external systems if given contact already exists in CRM. 
**/

@isTest
private class ZTestIntegrationContactCheck {

	@isTest static void test_checkContact_contactExist() {
		// given
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'John', LastName = 'Doe', Email = 'john.doe@testemailx.com'), true);

		String requestBody = '{"first_name":"John","last_name":"Doe","email":"john.doe@testemailx.com"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(200, res.statusCode);
        System.assertEquals('{"person_id":"'+c.Id+'"}', res.responseBody.toString());
	}

	@isTest static void test_checkContact_contactExistWithAdditionalEmail() {
		// given
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'John', LastName = 'Doe', Additional_Emails__c = 'john.doe@testemailx.com'), true);

		String requestBody = '{"first_name":"John","last_name":"Doe","email":"john.doe@testemailx.com"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(200, res.statusCode);
        System.assertEquals('{"person_id":"'+c.Id+'"}', res.responseBody.toString());
	}

	@isTest static void test_checkContact_contactDoesntExist() {
		// given
		String requestBody = '{"first_name":"John","last_name":"Doe","email":"john.doe@testemailx.com"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(400, res.statusCode);
	}
	
	@isTest static void test_checkContact_missingRequestFirstName() {
		// given
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'John', LastName = 'Doe', Email = 'john.doe@testemailx.com'), true);

		String requestBody = '{"first_name":"","last_name":"Doe","email":"john.doe@testemailx.com"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'first_name'), res.responseBody);
	}

	@isTest static void test_checkContact_missingRequestLastName() {
		// given
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'John', LastName = 'Doe', Email = 'john.doe@testemailx.com'), true);

		String requestBody = '{"first_name":"John","last_name":"","email":"john.doe@testemailx.com"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'last_name'), res.responseBody);
	}

	@isTest static void test_checkContact_missingRequestEmail() {
		// given
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'John', LastName = 'Doe', Email = 'john.doe@testemailx.com'), true);

		String requestBody = '{"first_name":"John","last_name":"Doe","email":""}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);
        
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        
		// when
		Test.startTest();
        IntegrationContactCheck.checkContact();
        Test.stopTest();

		//then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'email'), res.responseBody);
	}
}