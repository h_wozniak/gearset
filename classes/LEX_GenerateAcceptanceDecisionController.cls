public with sharing class LEX_GenerateAcceptanceDecisionController {


	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String checkRecord(String recordId) {
		String retStatus = 'NO_ERROR';

		Enrollment__c enr = [SELECT Id, Degree__c, Status__c, Unenrolled__c, Acceptation_Status__c FROM Enrollment__c WHERE Id = :recordId];
		if(enr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
			retStatus = 'DEGREE_PG';
		}else if(enr.Unenrolled__c) {
			retStatus = 'UNENROLLED';
		} else if(enr.Status__c != CommonUtility.ENROLLMENT_STATUS_DC) {
			retStatus = 'NO_DOCS_COLLECTED';
		} else if(enr.Acceptation_Status__c != CommonUtility.OFFER_ACCEPTATION_STATUS_MINISTER) {
			retStatus = 'NO_MINISTER_CONSENT';
		}
		return retStatus;
	}

	@AuraEnabled
	public static String createAcceptanceDecision(String recordId) {
		Enrollment__c enr = [SELECT Language_of_enrollment__c, University_Name__c FROM Enrollment__c WHERE Id = :recordId];
		Map<String, Object> returnMap = new Map<String, Object>();
		String accDecisionId = LEXStudyUtilities.createAcceptanceDecisionDocument(recordId);
		List<String> accIds = accDecisionId.split(' ');
		returnMap.put('acceptanceDocId', accDecisionId);
		returnMap.put('referenceNumber1', LEXStudyUtilities.getCustomSettings(accIds[0]));
		returnMap.put('referenceNumber2', accIds.size() == 2 ? LEXStudyUtilities.getCustomSettings(accIds[1]) : 0);
		System.debug('Output: '+JSON.serialize(returnMap));
		return JSON.serialize(returnMap);
	}

	@AuraEnabled
	public static String generateAcceptanceDecision(String recordId, String createAcceptanceDecisionJSON) {
		System.debug('Input: '+createAcceptanceDecisionJSON);
		Enrollment__c enr = [SELECT Language_of_enrollment__c, University_Name__c FROM Enrollment__c WHERE Id = :recordId];
		Map<String, Object> acceptanceDecisionMap = (Map<String, Object>)JSON.deserializeUntyped(createAcceptanceDecisionJSON);

		String acceptanceDocId = (string)acceptanceDecisionMap.get('acceptanceDocId');
		decimal referenceNumber1 = (decimal)acceptanceDecisionMap.get('referenceNumber1');
		decimal referenceNumber2 = (decimal)acceptanceDecisionMap.get('referenceNumber2');
		
		Boolean retVal =	LEXStudyUtilities.generateAcceptanceDecisionDocumentOnEnrollment(acceptanceDocId,
																							enr.Language_of_enrollment__c, 
																							enr.University_Name__c, 
				  																			referenceNumber1,
																							referenceNumber2);
		return retVal ? 'TRUE' : 'FALSE';
	}
}