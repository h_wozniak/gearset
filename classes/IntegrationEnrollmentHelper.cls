/**
*   @author         Sebastian Łasisz
*   @description    This class is used to create Enrollment JSON that is used to communicate with Experia
**/

public without sharing class IntegrationEnrollmentHelper {

    public static Map<Id, Contact> finishedCourses;
    public static Map<Id, Contact> workExperienceMapOnContact;
    public static Map<String, String> gainedTitleMap;
    public static Map<String, String> nationalityTransaltionMap;
    public static Map<String, String> countryOfOriginMap;
    public static Map<String, String> translatedPickListValues;

    public static Map<String, String> addressesTranslatonMap = new Map<String, String>{
        'Lower Silesia' => 'dolnośląskie',
        'Kuyavian-Pomeranian' => 'kujawsko-pomorskie',
        'Lublin' => 'lubelskie',
        'Lubusz' => 'lubuskie',
        'Łódź' => 'łódzkie',
        'Lesser Poland' => 'małopolskie',
        'Masovian' => 'mazowieckie',
        'Opole' => 'opolskie',
        'Subcarpathian' => 'podkarpackie',
        'Podlaskie' => 'podlaskie',
        'Pomeranian' => 'pomorskie',
        'Silesia' => 'śląskie',
        'Świętokrzyskie' => 'świętokrzyskie',
        'Greater Poland' => 'wielkopolskie',
        'Warmian-Masurian' => 'warmińsko-mazurskie',
        'West Pomeranian' => 'zachodniopomorskie'
    };

    public static Map<String, String> languageOfEnrollmentTranslationMap = new Map <String, String>{
        'Polish' => 'Polish',
        'Russian' => 'Polish',
        'English' => 'English'
    };

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- INVOKE METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String prepareEnrollmentsToSend(Set<Id> enrollmentIds) {
        List<Enrollment__c> enrollmentsToPrepare = [
            SELECT Name, Candidate_Student__c, Candidate_Student__r.FirstName, Candidate_Student__r.LastName, Candidate_Student__r.Phone, Candidate_Student__r.Email, Study_Start_Date__c,
            Unenrolled__c, Valid_From_for_email__c, Starting_Semester__c, Second_Course__c, Installments_per_Year__c, Tuition_System__c, Specialization__r.Trade_Name__c, University_Name__c,
            Educational_Agreement__r.Student_no__c, Enrollment_Source__c, Course_or_Specialty_Offer__r.Leading_Language__c, 
            Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c, Degree__c,
            Company_for_Discount_Name__c, Company_for_Discount_Tax_Id__c, Do_not_set_up_Student_Card__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, EFS__c, Language_of_Enrollment__c,
            Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
            Candidate_Student__r.Additional_Emails__c, Course_or_Specialty_Offer__r.Active_From__c, Course_or_Specialty_Offer__r.Name, Candidate_Student__r.Gender__c, 
            Candidate_Student__r.Pesel__c, Candidate_Student__r.Middle_Name__c, Candidate_Student__r.Family_Name__c, Candidate_Student__r.Father_Name__c, Candidate_Student__r.School_Certificate_Country__c,
            Candidate_Student__r.Mother_Name__c, Candidate_Student__r.Birthdate, Candidate_Student__r.Place_of_Birth__c, Candidate_Student__r.Foreigner__c, 
            Candidate_Student__r.Country_of_Origin__c, Candidate_Student__r.Nationality__c, Candidate_Student__r.OtherCountry, Consent_Date_processing_for_contract__c,
            Candidate_Student__r.OtherPostalCode, Candidate_Student__r.OtherState, Candidate_Student__r.OtherCity, Candidate_Student__r.OtherStreet, Candidate_Student__r.MailingCountry,
            Candidate_Student__r.MailingPostalCode, Candidate_Student__r.MailingState, Candidate_Student__r.MailingCity, Candidate_Student__r.MailingStreet,
            Candidate_Student__r.MobilePhone, Course_or_Specialty_Offer__r.University_Name__c, Educational_Agreement__c,
            Candidate_Student__r.Polish_Card_Document__c, Candidate_Student__r.Expiration_Date_Polish_Card_Document__c, Candidate_Student__r.Study_Authorization_Document__c,         
            Candidate_Student__r.Consent_Direct_Communications__c, Candidate_Student__r.Consent_Electronic_Communication__c, Candidate_Student__r.Consent_Graduates__c, 
            Candidate_Student__r.Consent_Marketing__c, Candidate_Student__r.Consent_Direct_Communications_ADO__c, Candidate_Student__r.Consent_Electronic_Communication_ADO__c, 
            Candidate_Student__r.Consent_Graduates_ADO__c, Candidate_Student__r.Consent_Marketing_ADO__c,
            Candidate_Student__r.Expiration_Date_Authorization_Doc__c, Candidate_Student__r.Identity_Document__c, Candidate_Student__r.Number_and_Series__c, 
            Candidate_Student__r.Country_of_Issue__c, Candidate_Student__r.Health_Insurance_Expiration_Date__c, Candidate_Student__r.Country_of_Birth__c, Finished_University__r.City_of_Diploma_Issue__c,
            Candidate_Student__r.School__c, Candidate_Student__r.School_Name__c, Candidate_Student__r.School__r.Name,
            Candidate_Student__r.School__r.BillingCity, Candidate_Student__r.School_Town__c, Enrollment_Date__c,
            Candidate_Student__r.School__r.School_Type__c, Candidate_Student__r.School_Type__c, Candidate_Student__r.A_Level__c, 
            Candidate_Student__r.School_Certificate_With_Honors__c, Candidate_Student__r.WSB_Graduate__c, Candidate_Student__r.TEB_Graduate__c, Finished_University__r.University_from_Finished_University__r.Name, 
            Finished_University__r.University_from_Finished_University__r.BillingCity, Candidate_Student__r.A_Level_Issue_Date__c, Candidate_Student__r.A_Level_Id__c , Candidate_Student__r.Year_of_Graduation__c, Candidate_Student__r.School_Certificate_Town__c,
            Finished_University__r.ZPI_University_Name__c, Finished_University__r.Status__c, Candidate_Student__r.Regional_Examination_Commission__c ,
            Finished_University__r.ZPI_University_City__c, Initial_Specialty_Declaration__r.Specialty_Trade_Name__c,
            Finished_University__r.University_from_Finished_University__c, Initial_Specialty_Declaration__c, Signed_Contract_Date__c,
            Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Guided_group__c, WSB_Passport__c,
            Price_Book_from_Enrollment__c, Price_Book_from_Enrollment__r.Price_Book_Type__c, Price_Book_from_Enrollment__r.PriceBook_From__c,
            Finished_University__c, Finished_University__r.Finished_Course__c, Price_Book_from_Enrollment__r.Name, Unenrolled_Signed_Contract_Date__c,
            Finished_University__r.Defense_Date__c, Finished_University__r.Diploma_Number__c, Finished_University__r.Country_of_Diploma_Issue__c, 
            Finished_University__r.Gained_Title__c, Finished_University__r.Diploma_Issue_Date__c, Finished_University__r.Graduation_Year__c,
                (SELECT Base_Consent__r.Name, Consent__c, Consent_Date__c, Refusal_Date__c, Consent_Source__c, Consent_Storage_Location__c, Contact_From_Consent__c
                FROM Consents__r),
                (SELECT Id, Foreign_Language__c, Language_Level__c, Language_Certification__c FROM Languages__r)
            FROM Enrollment__c
            WHERE Id IN :enrollmentIds
        ];

        gainedTitleMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Qualification__c.Gained_Title__c);
        nationalityTransaltionMap = DictionaryTranslator.getTranslatedPicklistValues(CommonUtility.LANGUAGE_CODE_PL, Contact.Nationality__c);
        countryOfOriginMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Country_of_Origin__c);
        translatedPickListValues = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Regional_Examination_Commission__c);

        Set<Id> contactsId = new Set<Id>();
        for (Enrollment__c enrollmentToPrepare : enrollmentsToPrepare) {
            contactsId.add(enrollmentToPrepare.Candidate_Student__c);
        }

        workExperienceMapOnContact = new Map<Id, Contact>(
            [SELECT Id, (SELECT Id, Company_Name__c, Position__c, Employed_From__c, Employed_Till__c, 
                         Street_of_Employment__c, Postal_Code__c, City_of_Employment__c, Country_of_Employment__c
                          FROM WorkExperience__r )
             FROM Contact
             WHERE Id IN :contactsId
            ]);

        finishedCourses = new Map<Id, Contact>(
            [SELECT Id, (SELECT Id, Completion_Year__c, Name_of_completed_programme__c FROM FInished_Courses__r )
             FROM Contact
             WHERE Id IN :contactsId
            ]);

        List<Enrollment_JSON> enrollmentJSONList = new List<Enrollment_JSON>();
        for (Enrollment__c enrollmentToPrepare : enrollmentsToPrepare) {
            String entity = CommonUtility.getMarketingEntityByUniversityName(enrollmentToPrepare.Course_or_Specialty_Offer__r.University_Name__c);
            //List<MarketingConsents_MassEnrollment_JSON> consents = buildConsentJSONList(entity, enrollmentToPrepare);
            enrollmentJSONList.add(IntegrationEnrollmentHelper.buildEnrollmentJSON(enrollmentToPrepare));
        }

        return JSON.serialize(enrollmentJSONList);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE BUILDER ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static Enrollment_JSON buildEnrollmentJSON(Enrollment__c studyEnrollment) {
        Enrollment_JSON enrollmentToSend = new Enrollment_JSON();
        enrollmentToSend.enrollment_id = studyEnrollment.Educational_Agreement__c;
        enrollmentToSend.person_id = studyEnrollment.Candidate_Student__c;
        enrollmentToSend.first_name = studyEnrollment.Candidate_Student__r.FirstName;
        enrollmentToSend.last_name = studyEnrollment.Candidate_Student__r.LastName;
        enrollmentToSend.phones = IntegrationEnrollmentHelper.createPhone(studyEnrollment.Candidate_Student__r.Phone, studyEnrollment.Candidate_Student__r.MobilePhone);
        enrollmentToSend.email = studyEnrollment.Candidate_Student__r.Email;
        enrollmentToSend.emails_others = IntegrationEnrollmentHelper.splitEmails(studyEnrollment.Candidate_Student__r.Additional_Emails__c);
        enrollmentToSend.addresses = IntegrationEnrollmentHelper.createAddress(studyEnrollment);        
        enrollmentToSend.enrollment_data = IntegrationEnrollmentHelper.createRecrutationData(studyEnrollment);

        return enrollmentToSend;
    }
    
    @TestVisible
    private static List<Phone_JSON> createPhone(String phoneNumber, String mobilePhone) {
        List<Phone_JSON> phones = new List<Phone_JSON>();
        if (phoneNumber != null && phoneNumber != '') {
            Phone_JSON phone = new Phone_JSON();
            phone.type = 'Default';
            phone.phoneNumber = phoneNumber;        
            phones.add(phone);
        }
        if (mobilePhone != null && mobilePhone != '') {
            Phone_JSON cellPhone = new Phone_JSON();
            cellPhone.type = 'Mobile';
            cellPhone.phoneNumber = mobilePhone;        
            phones.add(cellPhone);
        }
        return phones;
    }

    @TestVisible
    private static List<Address_JSON> createAddress(Enrollment__c studyEnrollment) {
        List<Address_JSON> addressesToSend = new List<Address_JSON>();
        Address_JSON homeAddressFromCandidate = new Address_JSON();
        homeAddressFromCandidate.type = 'home';
        homeAddressFromCandidate.country = studyEnrollment.Candidate_Student__r.OtherCountry;
        homeAddressFromCandidate.postal_code = studyEnrollment.Candidate_Student__r.OtherPostalCode;
        homeAddressFromCandidate.state = addressesTranslatonMap.get(studyEnrollment.Candidate_Student__r.OtherState);
        homeAddressFromCandidate.city = studyEnrollment.Candidate_Student__r.OtherCity;
        homeAddressFromCandidate.street = studyEnrollment.Candidate_Student__r.OtherStreet;
        homeAddressFromCandidate.post = homeAddressFromCandidate.city; 
        addressesToSend.add(homeAddressFromCandidate); 

        Address_JSON mailingAddressFromCandidate = new Address_JSON();
        mailingAddressFromCandidate.type = 'mailing';
        mailingAddressFromCandidate.country = studyEnrollment.Candidate_Student__r.MailingCountry;
        mailingAddressFromCandidate.postal_code = studyEnrollment.Candidate_Student__r.MailingPostalCode;
        mailingAddressFromCandidate.state = addressesTranslatonMap.get(studyEnrollment.Candidate_Student__r.MailingState);
        mailingAddressFromCandidate.city = studyEnrollment.Candidate_Student__r.MailingCity;
        mailingAddressFromCandidate.street = studyEnrollment.Candidate_Student__r.MailingStreet;
        mailingAddressFromCandidate.post = mailingAddressFromCandidate.city;  
        addressesToSend.add(mailingAddressFromCandidate);

        return addressesToSend;      
    }

    private static RecrutationData_JSON createRecrutationData(Enrollment__c studyEnrollment) {
        RecrutationData_JSON recrutationDataFromCandidate = new RecrutationData_JSON();
        recrutationDataFromCandidate.locked = Approval.isLocked(studyEnrollment.Id);
        recrutationDataFromCandidate.product_id = studyEnrollment.Course_or_Specialty_Offer__r.Name;
        DateTime dT = studyEnrollment.Enrollment_Date__c;
        recrutationDataFromCandidate.registration_date = Date.newinstance(dT.year(), dT.month(), dT.day());
        recrutationDataFromCandidate.initial_specialty_declaration = studyEnrollment.Initial_Specialty_Declaration__r.Specialty_Trade_Name__c;
        recrutationDataFromCandidate.registration_language = studyEnrollment.Language_of_Enrollment__c != null ? languageOfEnrollmentTranslationMap.get(studyEnrollment.Language_of_Enrollment__c).toLowerCase() : null;
        recrutationDataFromCandidate.personal_data = IntegrationEnrollmentHelper.createPersonalData(studyEnrollment);  

        if (!studyEnrollment.Unenrolled__c) {
            if (studyEnrollment.Signed_Contract_Date__c != null) {      
                DateTime dT2 = studyEnrollment.Signed_Contract_Date__c;
                recrutationDataFromCandidate.signed_contract_date = Date.newinstance(dT2.year(), dT2.month(), dT2.day());
            }
        }
        else {
            if (studyEnrollment.Unenrolled_Signed_Contract_Date__c != null) {      
                DateTime dT2 = studyEnrollment.Unenrolled_Signed_Contract_Date__c;
                recrutationDataFromCandidate.signed_contract_date = Date.newinstance(dT2.year(), dT2.month(), dT2.day());
            }            
        }

        recrutationDataFromCandidate.recrutation_parameters = IntegrationEnrollmentHelper.createRecrutationParameters(studyEnrollment);
        recrutationDataFromCandidate.finished_school = IntegrationEnrollmentHelper.createFinishedSchool(studyEnrollment);
        recrutationDataFromCandidate.career = IntegrationEnrollmentHelper.createCareers(studyEnrollment);

        return recrutationDataFromCandidate;
    }

    private static PersonalDate_JSON createPersonalData(Enrollment__c studyEnrollment) {        
        PersonalDate_JSON personalData = new PersonalDate_JSON();
        personalData.middle_name = studyEnrollment.Candidate_Student__r.Middle_Name__c == null ? '' : studyEnrollment.Candidate_Student__r.Middle_Name__c;
        personalData.family_name = studyEnrollment.Candidate_Student__r.Family_Name__c;
        personalData.nationality = prepareNationalities(studyEnrollment.Candidate_Student__r.Nationality__c);
        personalData.polish_card = studyEnrollment.Candidate_Student__r.Polish_Card_Document__c;
        personalData.polish_card_expire_date = studyEnrollment.Candidate_Student__r.Expiration_Date_Polish_Card_Document__c;
        personalData.personal_id = studyEnrollment.Candidate_Student__r.Pesel__c;
        personalData.birthdate = studyEnrollment.Candidate_Student__r.Birthdate;
        personalData.gender = studyEnrollment.Candidate_Student__r.Gender__c;

        personalData.residency_permit = 
        (studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c != null 
        && studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c != CommonUtility.CONTACT_AUTHORIZATION_DOCUMENT_NONE) 
            ? studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c.toLowerCase() 
            : null;

        personalData.residency_permit_expire_date = studyEnrollment.Candidate_Student__r.Expiration_Date_Authorization_Doc__c;
        personalData.father_name = studyEnrollment.Candidate_Student__r.Father_Name__c;
        personalData.mother_name = studyEnrollment.Candidate_Student__r.Mother_Name__c;
        personalData.identity_document = IntegrationEnrollmentHelper.mapIdentityDocumentPicklist(studyEnrollment.Candidate_Student__r.Identity_Document__c);
        personalData.identity_document_number = studyEnrollment.Candidate_Student__r.Number_and_Series__c;
        personalData.identity_document_country = countryOfOriginMap.get(studyEnrollment.Candidate_Student__r.Country_of_Issue__c);
        personalData.country_of_birth = countryOfOriginMap.get(studyEnrollment.Candidate_Student__r.Country_of_Birth__c);
        personalData.place_of_birth = studyEnrollment.Candidate_Student__r.Place_of_Birth__c;
        personalData.country_of_origin = countryOfOriginMap.get(studyEnrollment.Candidate_Student__r.Country_of_Origin__c);
        personalData.foreigner = studyEnrollment.Candidate_Student__r.Foreigner__c;
        personalData.foreigner_terms = pareseAdmissionRules(studyEnrollment.Admission_Rules_for_Studies__c);
        personalData.foreigner_terms_from = studyEnrollment.Admission_Rules_From__c;
        personalData.foreigner_terms_to = studyEnrollment.Admission_Rules_To__c;
        personalData.health_insurance_expire_date = studyEnrollment.Candidate_Student__r.Health_Insurance_Expiration_Date__c;

        return personalData;
    }

    private static RecrutationParameters_JSON createRecrutationParameters(Enrollment__c studyEnrollment) {
        RecrutationParameters_JSON recrutationParameters = new RecrutationParameters_JSON();
        recrutationParameters.unenrolled = studyEnrollment.Unenrolled__c;
        recrutationParameters.efs = studyEnrollment.EFS__c;
        recrutationParameters.place_of_recrutation = IntegrationEnrollmentHelper.mapPlaceOfRecrutationPicklist(studyEnrollment.Enrollment_Source__c);
        recrutationParameters.start_date = studyEnrollment.Study_Start_Date__c;
        recrutationParameters.start_semester = Integer.valueOf(studyEnrollment.Starting_Semester__c);
        recrutationParameters.language = studyEnrollment.Course_or_Specialty_Offer__r.Leading_Language__c != null ? studyEnrollment.Course_or_Specialty_Offer__r.Leading_Language__c.toLowerCase() : null;
        recrutationParameters.second_course = studyEnrollment.Second_Course__c;
        recrutationParameters.guided_group = studyEnrollment.Guided_group__c;
        recrutationParameters.wsb_passport = studyEnrollment.WSB_Passport__c;
        recrutationParameters.index_number = studyEnrollment.Educational_Agreement__r.Student_no__c != null ? Integer.valueOf(studyEnrollment.Educational_Agreement__r.Student_no__c) : null;
        recrutationParameters.postgraduate_course = studyEnrollment.Specialization__r.Trade_Name__c;
        recrutationParameters.pricebook_id = studyEnrollment.Price_Book_from_Enrollment__r.Name;
        if (studyEnrollment.Price_Book_from_Enrollment__r.Price_Book_Type__c != null) {
            recrutationParameters.pricebook_type = studyEnrollment.Price_Book_from_Enrollment__r.Price_Book_Type__c.toLowerCase();    
        }
        if (studyEnrollment.Price_Book_from_Enrollment__r.PriceBook_From__c != null) {    
            DateTime dT = studyEnrollment.Price_Book_from_Enrollment__r.PriceBook_From__c;
            recrutationParameters.pricebook_active_from = Date.newinstance(dT.year(), dT.month(), dT.day());
        }
        recrutationParameters.installments = studyEnrollment.Installments_per_Year__c != null ? Integer.valueOf(studyEnrollment.Installments_per_Year__c) : null;
        recrutationParameters.tuition_system = IntegrationEnrollmentHelper.mapTuitionPicklist(studyEnrollment.Tuition_System__c);
        recrutationParameters.chosen_language = IntegrationEnrollmentHelper.createLanguages(studyEnrollment);
        recrutationParameters.company_name = studyEnrollment.Company_for_Discount_Name__c;
        recrutationParameters.company_vat_identification_number = studyEnrollment.Company_for_Discount_Tax_Id__c;
        recrutationParameters.student_card = studyEnrollment.Do_not_set_up_Student_Card__c;

        return recrutationParameters;
    }

    public static List<ChosenLangugage_JSON> createLanguages(Enrollment__c studyEnrollment) {
        List<ChosenLangugage_JSON> languages = new List<ChosenLangugage_JSON>();
        for (Enrollment__c lang : studyEnrollment.Languages__r) {
            ChosenLangugage_JSON newLang = new ChosenLangugage_JSON();
            newLang.name = lang.Foreign_Language__c != null ? lang.Foreign_Language__c.toLowerCase() : null;
            newLang.level = lang.Language_Level__c != null ? lang.Language_Level__c.toLowerCase() : null;

            languages.add(newLang);
        }

        return languages;
    }

    public static String pareseAdmissionRules(String admissionRuleStatus) {
        if (admissionRuleStatus != null) {
            if (admissionRuleStatus.contains('PSC1')) {
                return 'PSC1';
            }
            else if (admissionRuleStatus.contains('PSC2')) {
                return 'PSC2';
            }
            else {
                return 'Inne';
            }
        }
        return null;
    }

    private static FinishedSchool_JSON createFinishedSchool(Enrollment__c studyEnrollment) {
        FinishedSchool_JSON finishedSchool = new FinishedSchool_JSON();
        finishedSchool.a_level_date = studyEnrollment.Candidate_Student__r.A_Level_Issue_Date__c;
        finishedSchool.a_level_number = studyEnrollment.Candidate_Student__r.A_Level_Id__c;
        finishedSchool.a_level_city = studyEnrollment.Candidate_Student__r.School_Certificate_Town__c;

        String translatedCountry = countryOfOriginMap.get(studyEnrollment.Candidate_Student__r.School_Certificate_Country__c);
        finishedSchool.a_level_country = translatedCountry != null ? translatedCountry : studyEnrollment.Candidate_Student__r.School_Certificate_Country__c;
        finishedSchool.regional_examination_commission = translatedPickListValues.get(studyEnrollment.Candidate_Student__r.Regional_Examination_Commission__c);
        
        if (studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_U) {            
            finishedSchool.school_name = studyEnrollment.Candidate_Student__r.School__c == null ? 
                                         studyEnrollment.Candidate_Student__r.School_Name__c :
                                         studyEnrollment.Candidate_Student__r.School__r.Name;
            finishedSchool.school_city = studyEnrollment.Candidate_Student__r.School__c == null ? 
                                         studyEnrollment.Candidate_Student__r.School_Town__c :
                                         studyEnrollment.Candidate_Student__r.School__r.BillingCity;

            finishedSchool.school_type = 'szkoła średnia';
            if (studyEnrollment.Candidate_Student__r.Year_of_Graduation__c != null && studyEnrollment.Candidate_Student__r.Year_of_Graduation__c.isNumeric()) {
                finishedSchool.finished_year = Integer.valueOf(studyEnrollment.Candidate_Student__r.Year_of_Graduation__c);
            }
            // finishedSchool.school_status = studyEnrollment.Candidate_Student__r.A_Level__c;
        }
        else if (studyEnrollment.Finished_University__c != null && (studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_II_PG || studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_MBA || studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_PG)) {
            finishedSchool.finished_year = studyEnrollment.Finished_University__r.Graduation_Year__c != null ? 
                                           Integer.valueOf(studyEnrollment.Finished_University__r.Graduation_Year__c) 
                                           : null;

            if (studyEnrollment.Finished_University__r.University_from_Finished_University__c == null) {
                finishedSchool.school_name = studyEnrollment.Finished_University__r.ZPI_University_Name__c;
                finishedSchool.school_city = studyEnrollment.Finished_University__r.ZPI_University_City__c;
                finishedSchool.school_type = 'szkoła wyższa';
            }
            else {
                finishedSchool.school_name = studyEnrollment.Finished_University__r.University_from_Finished_University__r.Name;
                finishedSchool.school_city = studyEnrollment.Finished_University__r.University_from_Finished_University__r.BillingCity;
                finishedSchool.school_type = 'szkoła wyższa';
            }

            finishedSchool.finished_course = studyEnrollment.Finished_University__r.Finished_Course__c;
            finishedSchool.diploma_defence_date = studyEnrollment.Finished_University__r.Defense_Date__c;
            finishedSchool.diploma_number = studyEnrollment.Finished_University__r.Diploma_Number__c;

            String translatedDiplomaCountry = countryOfOriginMap.get(studyEnrollment.Finished_University__r.Country_of_Diploma_Issue__c);
            finishedSchool.diploma_country = translatedDiplomaCountry != null ? translatedDiplomaCountry : studyEnrollment.Finished_University__r.Country_of_Diploma_Issue__c;

            finishedSchool.diploma_city = studyEnrollment.Finished_University__r.City_of_Diploma_Issue__c;
            finishedSchool.diploma_date = studyEnrollment.Finished_University__r.Diploma_Issue_Date__c;
            if (studyEnrollment.Finished_University__r.Gained_Title__c != null) {
                finishedSchool.gained_title = gainedTitleMap.get(studyEnrollment.Finished_University__r.Gained_Title__c);
            }
            finishedSchool.school_status = IntegrationEnrollmentHelper.mapStatusPicklist(studyEnrollment.Finished_University__r.Status__c);
        }

        finishedSchool.certificate_with_honours = studyEnrollment.Candidate_Student__r.School_Certificate_With_Honors__c;
        finishedSchool.wsb_group_graduate = studyEnrollment.Candidate_Student__r.WSB_Graduate__c;
        finishedSchool.teb_education_graduate = studyEnrollment.Candidate_Student__r.TEB_Graduate__c;
        finishedSchool.finished_courses = IntegrationEnrollmentHelper.finishedCourses(studyEnrollment);

        return finishedSchool;
    }

    private static List<FinishedCourse_JSON> finishedCourses(Enrollment__c studyEnrollment) {
        Contact c = finishedCourses.get(studyEnrollment.Candidate_Student__c);

        List<FinishedCourse_JSON> courses = new List<FinishedCourse_JSON>();
        if (c != null) {
            for (Qualification__c finishedCourse : c.FInished_Courses__r) {
                FinishedCourse_JSON course = new FinishedCourse_JSON();
                course.course = finishedCourse.Name_of_completed_programme__c;
                course.year = Integer.valueOf(finishedCourse.Completion_Year__c);

                courses.add(course);
            }
        }

        return courses;
    }

    private static List<Career_JSON> createCareers(Enrollment__c studyEnrollment) {
        Contact c = workExperienceMapOnContact.get(studyEnrollment.Candidate_Student__c);

        List<Career_JSON> careers = new List<Career_JSON>();
        if (c != null) {
            for (Qualification__c workExperience : c.WorkExperience__r) {
                Career_JSON career = new Career_JSON();
                career.company_name = workExperience.Company_Name__c;
                career.position = workExperience.Position__c;
                career.employed_from = workExperience.Employed_From__c;
                career.employed_to = workExperience.Employed_Till__c;
                career.company_address = workExperience.Street_of_Employment__c;
                career.company_zip_code = workExperience.Postal_Code__c;
                career.company_city = workExperience.City_of_Employment__c;
                career.company_country = workExperience.Country_of_Employment__c;

                careers.add(career);
            }
        }

        return careers;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static List<String> prepareNationalities(String nationalitiesToPrepare) {
        List<String> nationalities = CommonUtility.getOptionsFromMultiSelectToList(nationalitiesToPrepare);

        List<String> newNationalities = new List<String>();

        for (String nationality : nationalities) {
            newNationalities.add(nationalityTransaltionMap.get(nationality));
        }

        return newNationalities;
    }

    private static List<String> splitEmails(String additionalEmails) {
        if (additionalEmails != null) {
            List<String> listsOfAdditionalEmails = additionalEmails.split('\\; ');
            return listsOfAdditionalEmails;
        }
        
        return new List<String>();
    }

    // method for updating PriceBook Synchronization Status after sending to ESB
    public static void updateSyncStatus_inProgress(Boolean success, Set<Id> enrIds, String errorMSG, Boolean sendingPaymentsToKS, Boolean sendingInitialFees) {
        List<Enrollment__c> pbsToUpdate = [
            SELECT Id, Enrollment_Error_JSON__c, TECH_Integration_Status__c, Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id IN :enrIds
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        ];

        for (Enrollment__c studyEnr : pbsToUpdate) {
            studyEnr.Synchronization_Status__c = (success? CommonUtility.SYNCSTATUS_INPROGRESS : CommonUtility.SYNCSTATUS_FAILED);
            studyEnr.Enrollment_Error_JSON__c = errorMSG;

            if (!sendingPaymentsToKS && !sendingInitialFees) {
                studyEnr.TECH_Integration_Status__c = CommonUtility.ENROLLMENT_INTEGRATION_ENROLLMENTKS;
            }
        }

        try {
            update pbsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    // method for updating PriceBook Synchronization Status after receiving ack from ESB
    public static void updateSyncStatus_onFinish(Boolean success, Set<Id> enrIds, String errorMSG) {
        List<Enrollment__c> pbsToUpdate = new List<Enrollment__c>();

        for (Id enrId : enrIds) {
            Enrollment__c pbToUpdate = new Enrollment__c(
                Id = enrId,
                Synchronization_Status__c = (success? CommonUtility.SYNCSTATUS_FINISHED : CommonUtility.SYNCSTATUS_FAILED),
                Enrollment_Error_JSON__c = errorMSG
            );

            pbsToUpdate.add(pbToUpdate);
        }

        try {
            update pbsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- PICKLIST MAPPING ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String mapStatusPicklist(String value) {
        if (value == CommonUtility.CONTACT_DIPLOMA_AQUIRED) {
            return 'with diploma';
        }
        else if (value == CommonUtility.CONTACT_DIPLOMA_WAITING_FOR_RECEIPTION) {
            return 'after defence';
        }
        else if (value == CommonUtility.CONTACT_DIPLOMA_WAITING_FOR_THESIS) {
            return 'before defence';
        }
        else {
            return value != null ? value.toLowerCase() : null;
        }
    }

    public static String mapTuitionPicklist(String value) {
        if (value == CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED) {
            return 'constant';
        }
        else {
            return 'variable';
        }
    }

    public static String mapPlaceOfRecrutationPicklist(String value) {
        if (value == CommonUtility.ENROLLMENT_ENR_SOURCE_CRM) {
            return 'recrutation';
        }
        else {
            return 'online';
        }
    }

    public static String mapIdentityDocumentPicklist(String value) {
        if (value == CommonUtility.CONTACT_IDENTITY_DOCUMENT_CARD) {
            return 'personal id';
        }
        else {
            return value != null ? value.toLowerCase() : null;
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    public class Enrollment_JSON {
        public String enrollment_id;
        public String person_id;
        public String first_name;
        public String last_name;
        public List<Phone_JSON> phones;
        public String email;
        public List<String> emails_others;
        public List<Address_JSON> addresses;
        public RecrutationData_JSON enrollment_data;
    }
    
    public class RecrutationData_JSON {
        public Boolean locked;
        public String product_id;
        public String initial_specialty_declaration;
        public String registration_language;
        public Date registration_date;
        public Date signed_contract_date;
        public PersonalDate_JSON personal_data;
        public RecrutationParameters_JSON recrutation_parameters;
        public FinishedSchool_JSON finished_school;
        public List<Career_JSON> career;
    }
    
    public class Address_JSON {
        public String type;
        public String country;
        public String postal_code;
        public String state;
        public String city;
        public String street;
        public String post;
    }
    
    public class MarketingConsents_JSON {
        public String type;
        public String name;
        public String department;
        public Boolean value;
        public Date consent_date;
        public Date refusal_date;
        public String source;
        public String storage_location;
    }
    
    public class Phone_JSON {
        public String type;
        public String phoneNumber;
    }

    public class PersonalDate_JSON {
        public String middle_name;
        public String family_name;
        public List<String> nationality;
        public Boolean polish_card;
        public Date polish_card_expire_date;
        public String personal_id;
        public Date birthdate;
        public String gender;
        public String residency_permit;
        public Date residency_permit_expire_date;
        public String father_name;
        public String mother_name;
        public String identity_document;
        public String identity_document_number;
        public String identity_document_country;
        public String country_of_birth;
        public String place_of_birth;
        public String country_of_origin;
        public Boolean foreigner;
        public String foreigner_terms;
        public Date foreigner_terms_from;
        public Date foreigner_terms_to;
        public Date health_insurance_expire_date;
    }

    public class RecrutationParameters_JSON {
        public Boolean unenrolled;
        public Boolean efs;
        public String place_of_recrutation;
        public Date start_date;
        public Integer start_semester;
        public String language;
        public Boolean second_course;
        public Boolean guided_group;
        public Boolean wsb_passport;
        public Integer index_number;
        public String postgraduate_course;
        public String pricebook_id;
        public String pricebook_type;
        public Date pricebook_active_from;
        public Integer installments;
        public String tuition_system;
        public List<ChosenLangugage_JSON> chosen_language;
        public String company_name;
        public String company_vat_identification_number;
        public Boolean student_card;
    }

    public class ChosenLangugage_JSON {
        public String name;
        public String level;
    }

    public class FinishedSchool_JSON {
        public Date a_level_date;
        public String a_level_number;
        public String a_level_city;
        public String a_level_country;
        public String regional_examination_commission;
        public String school_name;
        public String school_city;
        public String school_type;
        public Integer finished_year;
        public String finished_course;
        public String school_status;
        public Date diploma_defence_date;
        public String diploma_number;
        public String diploma_city;
        public String diploma_country;
        public Date diploma_date;
        public String gained_title;
        public Boolean certificate_with_honours;
        public Boolean wsb_group_graduate;
        public Boolean teb_education_graduate;
        public List<FinishedCourse_JSON> finished_courses;
    }

    public class FinishedCourse_JSON {
        public String course;
        public Integer year;
    }

    public class Career_JSON {
        public String company_name;
        public String position;
        public Date employed_from;
        public Date employed_to;
        public String company_address;
        public String company_zip_code;
        public String company_city;
        public String company_country;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ESB RESPONSE MODEL DEFINITION ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public class Enrollment_Ack_JSON {
        public String enrollment_id;
        public String recrutation_id;
    }

}