public without sharing class ConfigCheckerController {

    /**
    * @author       Mateusz Wolak-Ksiazek
    * @description  This class is a controller for ConfigChecker.page, which allows checking system configuration
    **/

    @RemoteAction
    public static Response_Wrapper checkDocuments() {
        String[] docList = new String[] {
            RecordVals.CATALOG_DOCUMENT_AGREEMENT,
            RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED,
            RecordVals.CATALOG_DOCUMENT_ACCEPTANCE,
            RecordVals.CATALOG_DOCUMENT_OATH,
            RecordVals.CATALOG_DOCUMENT_DIPLOMA,
            RecordVals.CATALOG_DOCUMENT_ATTESTATION_DIPLOMA,
            RecordVals.CATALOG_DOCUMENT_ENTRYFEE_PAYCONF,
            RecordVals.CATALOG_DOCUMENT_CERTIFICATE_OF_PROFESSIONAL_EXPERIENCE,
            RecordVals.CATALOG_DOCUMENT_ENTRY_BUSINESS_REGISTER
        };

        List<Catalog__c> catalogDocuments = [
            SELECT Name 
            FROM Catalog__c 
            WHERE Name IN :docList
        ];

        if (catalogDocuments.size() != docList.size()) {
            Set<String> missingNameSet = new Set<String>();
            missingNameSet.addAll(docList);

            for (Catalog__c doc : catalogDocuments) {
                missingNameSet.remove(doc.Name);
            }
            
            List<String> missList = new List<String>();
            missList.addAll(missingNameSet);
            return new Response_Wrapper(false, Label.msg_error_MissingCatalogDocuments, missList);
        } else {
            return new Response_Wrapper(true);
        }
    }

    @RemoteAction
    public static Response_Wrapper checkUniversities() {
        String[] universityList = new String[] {
            RecordVals.WSB_NAME_POZ,
            RecordVals.WSB_NAME_WRO,
            RecordVals.WSB_NAME_TOR,
            RecordVals.WSB_NAME_GDA,
            RecordVals.WSB_NAME_GDY,
            RecordVals.WSB_NAME_BYD,
            RecordVals.WSB_NAME_OPO,
            RecordVals.WSB_NAME_SZC,
            RecordVals.WSB_NAME_CHO
        };

        List<Account> universityNames = [
            SELECT Name
            FROM Account
            WHERE Name IN :universityList
        ];

        if (universityNames.size() != universityList.size()) {
            Set<String> missingNameSet = new Set<String>();
            missingNameSet.addAll(universityList);

            for (Account wsb : universityNames) {
                missingNameSet.remove(wsb.Name);
            }

            List<String> missList = new List<String>();
            missList.addAll(missingNameSet);
            return new Response_Wrapper(false, Label.msg_error_MissingWsbNames, missList);
        } else {
            return new Response_Wrapper(true);
        }
     }

    @RemoteAction
    public static Response_Wrapper checkMarketingConsent() {

        String[] marketingConsentSet = new String[] {
            RecordVals.MARKETING_CONSENT_ENR_1,
            RecordVals.MARKETING_CONSENT_ENR_2,
            RecordVals.MARKETING_CONSENT_1,
            RecordVals.MARKETING_CONSENT_2,
            RecordVals.MARKETING_CONSENT_3,
            RecordVals.MARKETING_CONSENT_4,
            RecordVals.MARKETING_CONSENT_5,
            RecordVals.MARKETING_CONSENT_PRE_GDA,
            RecordVals.MARKETING_CONSENT_PRE_POZ,
            RecordVals.MARKETING_CONSENT_PRE_TOR,
            RecordVals.MARKETING_CONSENT_PRE_WRO
        };

        List<Catalog__c> marketingNames = [
            SELECT Name
            FROM Catalog__c
            WHERE Name IN :marketingConsentSet
        ];

        if (marketingNames.size() != marketingConsentSet.size()) {
            Set<String> missingNameSet = new Set<String>();
            missingNameSet.addAll(marketingConsentSet);

            for (Catalog__c mark : marketingNames) {
                missingNameSet.remove(mark.Name);
            }

            List<String> missList = new List<String>();
            missList.addAll(missingNameSet);
            return new Response_Wrapper(false, Label.msg_error_MissingMarketingConsents, missList);
        } else {
            return new Response_Wrapper(true);
        }
    }

    @RemoteAction
    public static Response_Wrapper checkFooters() {

        /* TODO: Expand image API list */
        String[] footerSet = new String[] {
            EmailTemplateComponentController.FB_LOGO_API,
            EmailTemplateComponentController.IN_LOGO_API,
            EmailTemplateComponentController.TW_LOGO_API,
            EmailTemplateComponentController.YT_LOGO_API,

            EmailTemplateComponentController.POZ_LOGO_API
        };

        List<Document> docList = [
            SELECT DeveloperName
            FROM Document
            WHERE DeveloperName IN :footerSet
        ];

        if (docList.size() != footerSet.size()) {
            Set<String> missingNameSet = new Set<String>();
            missingNameSet.addAll(footerSet);

            for (Document foot : docList) {
                missingNameSet.remove(foot.DeveloperName);
            }

            List<String> missList = new List<String>();
            missList.addAll(missingNameSet);
            return new Response_Wrapper(false, Label.msg_error_MissingFooter, missList);
        } else {
            return new Response_Wrapper(true);
        }
    }

    @RemoteAction
    public static Response_Wrapper checkStudentCardCS() {
        AdditionalCharges__c studentCardChargeCS = AdditionalCharges__c.getInstance(RecordVals.CS_AC_NAME_STUDENTCARD);

        if (studentCardChargeCS != null) {
            return new Response_Wrapper(true);
        } else {
            return new Response_Wrapper(false, Label.msg_error_MissingCustomSetting, new List<String> { RecordVals.CS_AC_NAME_STUDENTCARD });
        }
    }



    @TestVisible
    private class Response_Wrapper {
        @TestVisible Boolean result;
        @TestVisible String message;
        @TestVisible List<String> missingList;

        public Response_Wrapper(Boolean result) {
            this(result, null, null);
        }

        public Response_Wrapper(Boolean result, String message, List<String> missingList) {
            this.result = result;
            if (result) {
                this.message = Label.msg_info_Success;
                this.missingList = missingList;
            } else {
                this.message = Label.msg_error_ErrorMessage + ' ' + message;
                this.missingList = missingList;
            }
        }  
    }
}