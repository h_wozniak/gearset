public with sharing class LEX_Recalculate_TargetsController {
	
	@AuraEnabled
	public static Boolean executeStudiesBatch() {
		return SalesTargetComputations.executeStudiesBatch(true);
	}

	@AuraEnabled
	public static Boolean executeTrainingsBatch() {
		return SalesTargetComputations.executeTrainingsBatch(true);
	}
}