/**
*   @author         Sebastian Łasisz
*   @description    This class is used to create Enrollment with data from ZPI
**/

@RestResource(urlMapping = '/create_enrollment/*')
global without sharing class IntegrationEnrollmentReceiver {
    

    @HttpPost
    global static void receiveEnrollmentInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();
        
        IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(601));
        }

        Savepoint sp = Database.setSavepoint();
        if (parsedJson != null) {
            try {
                IntegrationEnrollmentReceiverHelper.Response_JSON response = IntegrationEnrollmentReceiverHelper.createEnrollment(parsedJson, jsonBody);
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(JSON.serialize(response));
            }
            catch(IntegrationEnrollmentReceiverHelper.InvalidFieldValueException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(700, e.getMessage()));
                Database.rollback(sp);
            }
            catch(IntegrationEnrollmentReceiverHelper.SecondarySchoolNotFoundException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(705));       
                Database.rollback(sp);         
            }
            catch(IntegrationEnrollmentReceiverHelper.ProductNotFoundException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(701));
                Database.rollback(sp);
            }
            catch(IntegrationEnrollmentReceiverHelper.UniversityAccountNotFoundException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(702));
                Database.rollback(sp);
            }
            catch(IntegrationEnrollmentReceiverHelper.SpecializationNotFoundException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(703));
                Database.rollback(sp);
            }
            catch(IntegrationEnrollmentReceiverHelper.CandidateNotFoundException e) {
                ErrorLogger.log(e);
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(704));
                Database.rollback(sp);
            }
            catch(Exception e) {
                String msg = CommonUtility.trimErrorMessage(e.getMessage());
                if (msg != null) {
                    ErrorLogger.logAndMsg(e, CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationEnrollmentReceiver.class.getName() + '\n' + msg);
                    res.statusCode = 400;
                    res.responseBody = IntegrationError.getErrorJSONBlobByCode(606, msg);
                } else {
                    ErrorLogger.logAndMsg(e, CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationEnrollmentReceiver.class.getName() + '\n' + jsonBody);
                    res.statusCode = 400;
                    res.responseBody = IntegrationError.getErrorJSONBlobByCode(600, e.getMessage());
                }
                Database.rollback(sp);
            }
        }
    }
    
    public static IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parse(String jsonBody) {
        return (IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON) System.JSON.deserialize(jsonBody, IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON.class);
    }
}