/**
*   @author         Sebastian Łasisz
*   @editor         Michal Bujak
*   @description    This class is used for retrieve Calendar Events from Google Calendar. Additional parameter calendar_id can be used to determine which calendar we'd like to get.
**/

global without sharing class IntegrationGoogleCalendarEvent {

    public static Boolean getEventsList(WSB_Calendar_Settings__mdt wSBCalendarSettig, String calendarId) {
        GoogleCalendarEvent calendarSettings = new GoogleCalendarEvent(wSBCalendarSettig);
        
        HttpRequest httpReq = new HttpRequest();

        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

        String createDidactics = esb.Google_Calendar_Event__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        if(calendarId != null) {
            httpReq.setBody(JSON.serialize(calendarSettings));

            Http http = new Http();
            System.debug('json: ' + httpReq.getBody());
            HTTPResponse httpRes = http.send(httpReq);

            Boolean success = (httpRes.getStatusCode() == 200);
            if (!success) {
                ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationGoogleCalendarEvent.class.getName()
                        + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + calendarId);
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class GoogleCalendarEvent {
        public String type;
        public String project_id;
        public String private_key_id;
        public String private_key;
        public String client_email;
        public String client_id;
        public String auth_uri;
        public String token_uri;
        public String auth_provider_x509_cert_url;
        public String client_x509_cert_url;
        
        public GoogleCalendarEvent(WSB_Calendar_Settings__mdt WSBCalendarSettig) {
            type = WSBCalendarSettig.Type__c;
            project_id = WSBCalendarSettig.Project_id__c;
            private_key_id = WSBCalendarSettig.Private_Key_Id__c;
            private_key = WSBCalendarSettig.Private_Key__c;
            client_email = WSBCalendarSettig.Client_Email__c;
            client_id = WSBCalendarSettig.Client_id__c;
            auth_uri = WSBCalendarSettig.Auth_URI__c;
            token_uri = WSBCalendarSettig.Token_URI__c;
            auth_provider_x509_cert_url = WSBCalendarSettig.Auth_Provider__c;
            client_x509_cert_url = WSBCalendarSettig.Client_Cert__c;
        }
    }
}