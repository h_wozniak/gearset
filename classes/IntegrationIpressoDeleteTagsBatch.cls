/**
*   @author         Sebastian Łasisz
*   @description    batch class that removes synchronised contacts with tags
**/

global class IntegrationIpressoDeleteTagsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	Set<Id> campaignId;
	List<Marketing_Campaign__c> iPressoCampaigns;

	public IntegrationIpressoDeleteTagsBatch(Set<Id> campaignId) {
		this.campaignId = campaignId;	

    	iPressoCampaigns = [
    		SELECT Id, iPresso_Criteria_Type__c
    		FROM Marketing_Campaign__c
    		WHERE Id IN :campaignId
    	];
	}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
    		SELECT Id, Synchronization_Status__c, Campaign_Contact__c
    		FROM Marketing_Campaign__c
    		WHERE Campaign_Member__c IN :campaignId
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Marketing_Campaign__c> iPressoMembers) {
    	try {
    		delete iPressoMembers;
    	}
    	catch (Exception e) {
    		ErrorLogger.log(e);
    	}
    }
    
    global void finish(Database.BatchableContext BC) {
    	List<Marketing_Campaign__c> campaignsToLock = new List<Marketing_Campaign__c>();
    	List<Marketing_Campaign__c> campaingsToDelete = new List<Marketing_Campaign__c>();

    	for (Marketing_Campaign__c iPressoCampaign : iPressoCampaigns) {
	    	if (iPressoCampaign.iPresso_Criteria_Type__c == CommonUtility.MARKETING_CRITERIA_TYPE_TAGS) {
	    		campaignsToLock.add(iPressoCampaign);
		    }
		    else {
		    	campaingsToDelete.add(iPressoCampaign);
	    	}
	    }

    	try {
            Approval.lock(campaignsToLock);
    		delete campaingsToDelete;
    	}
    	catch (Exception e) {
    		ErrorLogger.log(e);
    	}
    }    
}