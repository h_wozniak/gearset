@isTest
global class ZTestIntegrationEnrollmentSender {
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = retMap.get(0).Id), true));
        
        return retMap;
    }

    @isTest static void test_sendEnrollments() {
        Map<Integer, sObject> dataMap = prepareData();
        Catalog__c catalog = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_2, Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true);
        ZDataTestUtility.createEnrollmentConsent(new Marketing_Campaign__c(Base_Consent__c = catalog.Id, Enrollment__c = dataMap.get(2).Id
        ), true);
        
        Test.setMock(HttpCalloutMock.class, new MassEnrollmentSendCalloutMock());
        
        Test.startTest();
        Set<Id> studyEnrToSend = new Set<Id> { dataMap.get(2).Id };
        Boolean result = IntegrationEnrollmentSender.sendEnrollmentList(studyEnrToSend, false, false);
        Test.stopTest();
        
        System.assert(result);
    }
    
    @isTest static void test_sendEnrollments_resourceUnavailable() {
        Map<Integer, sObject> dataMap = prepareData();
        Catalog__c catalog = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_2, Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true);
        ZDataTestUtility.createEnrollmentConsent(new Marketing_Campaign__c(Base_Consent__c = catalog.Id, Enrollment__c = dataMap.get(2).Id
        ), true);
        
        Test.setMock(HttpCalloutMock.class, new EnrollmentSendFailedCalloutMock());
        
        Test.startTest();
        Set<Id> studyEnrToSend = new Set<Id> { dataMap.get(2).Id };
        Boolean result = IntegrationEnrollmentSender.sendEnrollmentList(studyEnrToSend, false, false);
        Test.stopTest();
        
        System.assert(!result);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class MassEnrollmentSendCalloutMock implements HttpCalloutMock {
        
        global HttpResponse respond(HTTPRequest req) {
            Map<Integer, sObject> dataMap = prepareData();
            
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setBody('{"error_code":202,"invalid_records":null}');
            res.setStatusCode(200);
            return res;
        }
    }
    
    global class EnrollmentSendFailedCalloutMock implements HttpCalloutMock {
        
        global HttpResponse respond(HTTPRequest req) {
            Map<Integer, sObject> dataMap = prepareData();
            
            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setBody('{"error_code":202,"invalid_records":null}');
            res.setStatusCode(404);
            return res;
        }
    }
}