@isTest
private class ZTestPriceBookManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- TEST DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_OfferWithPB1(Boolean pbActive, Decimal instAmount, Boolean config) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 7
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = pbActive
        ), true));
        retMap.put(3, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(1).Id,
            Active__c = pbActive
        ), true));
        retMap.put(4, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
            Active__c = pbActive
        ), true));

        //config PriceBook
        if (config) {
            configPriceBook(retMap.get(2).Id, instAmount);
            configPriceBook(retMap.get(3).Id, instAmount);
            configPriceBook(retMap.get(4).Id, instAmount);
        }

        return retMap;
    }

    private static Map<Integer, sObject> prepareDataForTest_OfferWithPB2(Boolean pbActive, Decimal instAmount, Boolean config) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 7
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = pbActive
        ), true));
        retMap.put(3, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
            Active__c = pbActive
        ), true));

        //config PriceBook
        if (config) {
            configPriceBook(retMap.get(2).Id, instAmount);
            configPriceBook(retMap.get(3).Id, instAmount);
        }

        return retMap;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- HELPER METHODS --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_getPBRecordTypeIdByDegree() {
        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_I)
        );

        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_PG)
        );

        System.assertEquals(
            CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA),
            PriceBookManager.getPBRecordTypeIdByDegree(CommonUtility.OFFER_DEGREE_MBA)
        );
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }
    


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- CONFIG VALIDATION METHODS ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    
    @isTest static void test_activatePriceBooks() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(false, 100, true);
 
        Offer__c pbBefore = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Id = :dataMap.get(1).Id
        ];

        System.assert(!pbBefore.Active__c);

        Test.startTest();
        PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });
        Test.stopTest();

        Offer__c pb = [
            SELECT Id, Active__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        Offer__c course = [
            SELECT Id, Current_Price_Book_in_use_from__c
            FROM Offer__c
            WHERE Id = :dataMap.get(0).Id
        ];

        System.assertNotEquals(course.Current_Price_Book_in_use_from__c, null);
    }

    @isTest static void test_activatePriceBooksFailure() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(false, 100, false);
 
        Test.startTest();
        Boolean success = PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });
        Test.stopTest();

        System.assert(!success);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- PRICE BOOK DESIGNATION ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_getProperPriceBookForOffer_Course() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(0).Id, false);
        Test.stopTest();
        
        System.assertEquals(dataMap.get(2).Id, priceBook.Id);
    }

    @isTest static void test_getProperPriceBookForOffer_CourseForeigner() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(0).Id, true);
        Test.stopTest();
        
        System.assertEquals(dataMap.get(4).Id, priceBook.Id);
    }

    @isTest static void test_getProperPriceBookForOffer_Spec() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(1).Id, false);
        Test.stopTest();
        
        System.assertEquals(dataMap.get(3).Id, priceBook.Id);
    }

    @isTest static void test_getProperPriceBookForOffer_Spec_fromCourse() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB2(true, 100, true);

        Test.startTest();
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(1).Id, false);
        Test.stopTest();
        
        System.assertEquals(dataMap.get(2).Id, priceBook.Id);
    }

    @isTest static void test_getProperPriceBookForOffer_SpecForeignerOnCourse() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(1).Id, true);
        Test.stopTest();
        
        System.assertEquals(dataMap.get(3).Id, priceBook.Id);
    }

    @isTest static void test_getProperPriceBookForOffer_SpecForeignerOnSpec() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        //create additional Foreigner Price Book on Specialty
        dataMap.put(5, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = dataMap.get(1).Id,
            Price_Book_Type__c = CommonUtility.OFFER_PBTYPE_FOREIGNERS,
            Active__c = true
        ), true));

        Offer__c pb = [SELECT Id, Currently_in_use__c FROM Offer__c WHERE Id = :dataMap.get(5).Id];

        System.assert(pb.Currently_in_use__c);

        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(dataMap.get(1).Id, true);
        Test.stopTest();
        
        System.assertEquals(pb.Id, priceBook.Id);
    }

    @isTest static void test_connectProperPriceBookSpec_Trigger() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = dataMap.get(1).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2'
        ), true);
        Test.stopTest();
        
        Enrollment__c studyEnrAfterInsert = [
            SELECT Id, Price_Book_from_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assertEquals(studyEnrAfterInsert.Price_Book_from_Enrollment__c, dataMap.get(3).Id);
    }

    @isTest static void test_connectProperPriceBookCourse_Trigger() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB1(true, 100, true);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = dataMap.get(0).Id,
            Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
            Installments_per_Year__c = '2'
        ), true);
        Test.stopTest();
        
        Enrollment__c studyEnrAfterInsert = [
            SELECT Id, Price_Book_from_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assertEquals(studyEnrAfterInsert.Price_Book_from_Enrollment__c, dataMap.get(2).Id);
    }



    @isTest static void test_clearPriceBookTuitionInstallments_Trigger() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB2(true, 100, true);

        Offer__c priceBookBefore = [
            SELECT Id,
                (SELECT Id, Graded_Price_1_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertNotEquals(null, priceBookBefore.InstallmentConfigs__r[0].Graded_Price_1_Year__c);


        Test.startTest();
        Offer__c priceBook = new Offer__c(
            Id = dataMap.get(2).Id,
            Graded_tuition__c = false
        );
        update priceBook;
        Test.stopTest();

        Offer__c priceBookAfter = [
            SELECT Id,
                (SELECT Id, Graded_Price_1_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(null, priceBookAfter.InstallmentConfigs__r[0].Graded_Price_1_Year__c);
    }




}