public with sharing class TrainingParticipantController {

	public Enrollment__c participant { get; set; }
	public String recordId;
	public Boolean canEdit { get; set; }

	public TrainingParticipantController(ApexPages.StandardController controller) {
		canEdit = true;
		recordId = Apexpages.currentPage().getParameters().get('candidate');

		participant = [
			SELECT Exam_Pass__c, Training_Evaluation__c, Training_Exam_Grade__c, Attendance__c 
			FROM Enrollment__c 
			WHERE Id = :recordId
		];
		
		if (!LEXServiceUtilities.hasEditAccess(recordId)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.msg_error_InsufficientPrivileges));
			canEdit = false;
		}
	}

	public PageReference save() {
		try {
			update participant;
		} catch(Exception e) {
			ApexPages.addMessages(e);
			return null;
		}
		return new PageReference('/'+recordId);
	}

	public PageReference back() {
		return new PageReference('/'+recordId);
	}
}