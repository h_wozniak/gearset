/**
*   @author         Grzegorz Długosz
*   @description    This class is used to make dictionaries available for external external systems.
**/
global with sharing class IntegrationUniversityDictionaryHelper {

       private static final List<String> listOfLanguageCodes = new List<String> {
        CommonUtility.INTEGRATION_LANGUAGE_CODE_PL,
        CommonUtility.INTEGRATION_LANGUAGE_CODE_EN,
        CommonUtility.INTEGRATION_LANGUAGE_CODE_RU
    };


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- Logic to get Dictionary --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */ 

    public static String getDictionaryUniversities(String dateFromRequest, Boolean wsb) {
        List<Account> listOfAccounts;

        DateTime dateFrom = CommonUtility.stringToDateTimeGMT(dateFromRequest);

        if (wsb) {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c 
                FROM Account 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                AND LastModifiedDate >= :dateFrom
            ];
        }
        else {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c
                FROM Account 
                WHERE ((RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY_EXT))
                OR (RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)))
                AND LastModifiedDate >= :dateFrom
            ];
        }

        List<Dictionary_JSON> listOfUniversities = new List<Dictionary_JSON>();

        for (Account acc : listOfAccounts) {
            listOfUniversities.add(new Dictionary_JSON(acc.Id, acc.Name, acc.BillingCity, acc.University_Type__c));
        }
        
        return JSON.serialize(listOfUniversities);
    }

    public static String getDictionaryUniversitiesNoDate(Boolean wsb) {
        List<Account> listOfAccounts;

        if (wsb) {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c 
                FROM Account 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
            ];
        }
        else {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c
                FROM Account 
                WHERE ((RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY_EXT)) 
                OR (RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)))
            ];
        }

        List<Dictionary_JSON> listOfUniversities = new List<Dictionary_JSON>();

        for (Account acc : listOfAccounts) {
            listOfUniversities.add(new Dictionary_JSON(acc.Id, acc.Name, acc.BillingCity, acc.University_Type__c));
        }
        
        return JSON.serialize(listOfUniversities);
    }

    public static String getDictionaryUniversities_iPresso(String dateFromRequest, Boolean wsb) {
        List<Account> listOfAccounts;

        DateTime dateFrom = CommonUtility.stringToDateTimeGMT(dateFromRequest);

        if (wsb) {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c 
                FROM Account 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                AND LastModifiedDate >= :dateFrom
                AND (NOT Name LIKE :'ODN%')
            ];
        }
        else {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c
                FROM Account 
                WHERE ((RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY_EXT))
                OR (RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)))
                AND LastModifiedDate >= :dateFrom
            ];
        }

        List<Dictionary_JSON> listOfUniversities = new List<Dictionary_JSON>();

        for (Account acc : listOfAccounts) {
            String newName = CommonUtility.swapSpecialPolishChars(acc.Name.replace('WSB ', '')).toLowerCase();
            listOfUniversities.add(new Dictionary_JSON(newName, acc.Name, acc.BillingCity, acc.University_Type__c));
        }
        
        return JSON.serialize(listOfUniversities);
    }

    public static String getDictionaryUniversitiesNoDate_Ipresso(Boolean wsb) {
        List<Account> listOfAccounts;

        if (wsb) {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c 
                FROM Account 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                AND (NOT Name LIKE :'ODN%')
            ];
        }
        else {
            listOfAccounts = [
                SELECT Id, Name, BillingCity, University_Type__c
                FROM Account 
                WHERE ((RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY_EXT)) 
                OR (RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)))
            ];
        }

        List<Dictionary_JSON> listOfUniversities = new List<Dictionary_JSON>();

        for (Account acc : listOfAccounts) {
            String newName = CommonUtility.swapSpecialPolishChars(acc.Name.replace('WSB ', '')).toLowerCase();
            listOfUniversities.add(new Dictionary_JSON(newName, acc.Name, acc.BillingCity, acc.University_Type__c));
        }
        
        return JSON.serialize(listOfUniversities);
    }

    public static Map<String, Id> getUniversityDictionaryMap() {
        Map<String, Id> universityDictionaryMap = new Map<String, Id>();

        List<Account> universities = [
            SELECT Id, Name
            FROM Account
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
        ];

        for (Account university : universities) {
            universityDictionaryMap.put(university.Name, university.Id);
        }

        return universityDictionaryMap;
    }

    public static Map<String, String> getUniversityDictionaryMap_New() {
        Map<String, String> universityDictionaryMap = new Map<String, String>();

        List<Account> universities = [
            SELECT Id, Name
            FROM Account
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
        ];

        for (Account university : universities) {
            universityDictionaryMap.put(university.Name, university.Name.replace('WSB ', ''));
        }

        return universityDictionaryMap;
    }

    public static List<String> univeristyList(Map<String, String> universityDictionaryMap, List<String> departments) {
        List<String> departmentIdList = new List<String>();
        
        for (String department : departments) {
            if (universityDictionaryMap.get(department) != null) {
                departmentIdList.add(CommonUtility.swapSpecialPolishChars(universityDictionaryMap.get(department)).toLowerCase());
            }
        }

        return departmentIdList;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class Dictionary_JSON {
        public String id;
        public String university_name;
        public String university_city;
        public String university_type;

        public Dictionary_JSON(String id, String university_name, String university_city, String university_type) {
            this.id = id;
            this.university_name = university_name;
            this.university_city = university_city;
            this.university_type = university_type;
        }
    }
}