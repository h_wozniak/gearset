/**
* @author       Wojciech Słodziak
* @description  Class to send DocumentList reminder - based on workflows.
**/

public without sharing class EmailManager_DocumentListReminderEmail {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private enum WorfklowEmailNames { // also names of templates
        X26_I_StudyEnrDocListReminder, /* I U | docList sent once before */
        X26_II_StudyEnrDocListReminder, /* II | docList sent once before */
        X26_PG_StudyEnrDocListReminder, /* PG | docList sent once before */
        X26_MBA_StudyEnrDocListReminder, /* MBA | docList sent once before */
        X27_I_StudyEnrDocListReminder, /* I U | docList sent twice before */
        X27_II_StudyEnrDocListReminder, /* II | docList sent twice before */
        X27_PG_StudyEnrDocListReminder, /* PG | docList sent twice before */
        X27_MBA_StudyEnrDocListReminder /* MBA | docList sent twice before */
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE DESIGNATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String designateWorkflowNameForEnrollment(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U) {
            workflow = EmailManager_DocumentListReminderEmail.designateWorkflowName_I_U(studyEnr);
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            workflow = EmailManager_DocumentListReminderEmail.designateWorkflowName_II(studyEnr);
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            workflow = EmailManager_DocumentListReminderEmail.designateWorkflowName_PG(studyEnr);
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            workflow = EmailManager_DocumentListReminderEmail.designateWorkflowName_MBA(studyEnr);
        }

        return workflow != null? (language + '_' + workflow.name()) : null;
    }

    private static WorfklowEmailNames designateWorkflowName_I_U(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        if (studyEnr.RequiredDocListEmailedCount__c == 1) {
            workflow = WorfklowEmailNames.X26_I_StudyEnrDocListReminder;
        } else if (studyEnr.RequiredDocListEmailedCount__c == 2) {
            workflow = WorfklowEmailNames.X27_I_StudyEnrDocListReminder;
        }

        return workflow;
    }

    private static WorfklowEmailNames designateWorkflowName_II(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        if (studyEnr.RequiredDocListEmailedCount__c == 1) {
            workflow = WorfklowEmailNames.X26_II_StudyEnrDocListReminder;
        } else if (studyEnr.RequiredDocListEmailedCount__c == 2) {
            workflow = WorfklowEmailNames.X27_II_StudyEnrDocListReminder;
        }

        return workflow;
    }

    private static WorfklowEmailNames designateWorkflowName_PG(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        if (studyEnr.RequiredDocListEmailedCount__c == 1) {
            workflow = WorfklowEmailNames.X26_PG_StudyEnrDocListReminder;
        } else if (studyEnr.RequiredDocListEmailedCount__c == 2) {
            workflow = WorfklowEmailNames.X27_PG_StudyEnrDocListReminder;
        }

        return workflow;
    }

    private static WorfklowEmailNames designateWorkflowName_MBA(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        if (studyEnr.RequiredDocListEmailedCount__c == 1) {
            workflow = WorfklowEmailNames.X26_MBA_StudyEnrDocListReminder;
        } else if (studyEnr.RequiredDocListEmailedCount__c == 2) {
            workflow = WorfklowEmailNames.X27_MBA_StudyEnrDocListReminder;
        }

        return workflow;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ INVOKATION METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method for sending required document list reminder for study Enrollment
    public static void sendRequiredDocumentListReminder(Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Degree__c, RequiredDocListEmailedCount__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
            AND Documents_Collected__c = false
            AND RequiredDocListEmailedCount__c != 0
            AND Dont_send_automatic_emails__c = false
        ];

        List<EmailTemplate> emailTemplates = [
            SELECT Id, Name
            FROM EmailTemplate
        ];

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName = EmailManager_DocumentListReminderEmail.designateWorkflowNameForEnrollment(enrollment);

            if (workflowTriggerName != null) {
                recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
                
                enrollment.RequiredDocListEmailedCount__c++;
                enrollment.RequiredDocumentsEmailedDate__c = System.today();

                List<String> workflowToTemplateName = workflowTriggerName.split('_');
                String firstPart = workflowToTemplateName.get(1).replaceAll('X', '');

                String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
                String templateName = '[' + language + '][26] Przypomnienie o konieczności dostarczenia dokumentów';
                if (enrollment.RequiredDocListEmailedCount__c == 3) {
                    templateName = '[' + language + '][27] Przypomnienie o konieczności dostarczenia dokumentów';
                }

                String emailTemplateId = '';
                for (EmailTemplate et : emailTemplates) {
                    if (et.Name == templateName) {
                        emailTemplateId = et.Id;
                    }
                }
                
                tasksToInsert.add(EmailManager.createEmailTask(
                    Label.title_EventDocListSent + ' (' + enrollment.RequiredDocListEmailedCount__c + ')',
                    enrollment.Id,
                    enrollment.Candidate_Student__c,
                    EmailManager.prepareFakeEmailBody(emailTemplateId, enrollment.Id),
                    null
                ));
            } else {
                enrollmentsWithFailure.add(enrollment);
            }
        }
        
        try {
            insert tasksToInsert;
            update enrollmentList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }

        if (!enrollmentsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_DocumentListReminderEmail.class.getName() 
                + ' ' + JSON.serialize(enrollmentsWithFailure));
        }
    }

}