/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to create Marketing Campaign Contacts
**/

/**
*   To initiate the EnrollmentConfirmationEmailReminder execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('CreateCampaignContactBatch', '0 0 8 * * ? *', new CreateCampaignContactBatch());
**/


global class CampaignContactInsertBatch implements Schedulable {

    global void execute(SchedulableContext sc) {
        CreateCampaignContactBatch batch8Days = new CreateCampaignContactBatch(8);
        Database.executebatch(batch8Days, 30);
    }
}