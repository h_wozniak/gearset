/**
*   @author         Wojciech Słodziak
*   @description    Schedulable class for scheduling Study and Training Sales Target Computations
**/

global without sharing class SalesTargetComputations implements Schedulable {

    global void execute(SchedulableContext sc) {
        executeStudiesBatch(false);
        executeTrainingsBatch(false);
    }

    webservice static Boolean executeStudiesBatch(Boolean isRunManually) {
        Set<String> classNameSet = new Set<String> {
            StudySalesTargetComputationsBatch.class.getName(),
            StudySalesTargetFinalizationBatch.class.getName()
        };

        if (checkIfJobsInProgress(classNameSet)) {
            return false;
        }

        StudySalesTargetComputationsBatch sstcb = new StudySalesTargetComputationsBatch(isRunManually);
        Database.executeBatch(sstcb);

        return true;
    }

    webservice static Boolean executeTrainingsBatch(Boolean isRunManually) {
        Set<String> classNameSet = new Set<String> {
            TrainingSalesTargetComputationsBatch.class.getName(),
            TrainingSalesTargetFinalizationBatch.class.getName()
        };

        if (checkIfJobsInProgress(classNameSet)) {
            return false;
        }

        TrainingSalesTargetComputationsBatch tstcb = new TrainingSalesTargetComputationsBatch(isRunManually);
        Database.executeBatch(tstcb);

        return true;
    }

    // method for checking if job is not already in progress in case it was run concurrently (which may result in wrong calculations)
    private static Boolean checkIfJobsInProgress(Set<String> classNameSet) {
        Set<String> inProgressStatuses = new Set<String> {
            CommonUtility.ASYNCAPEX_STATUS_HOLDING,
            CommonUtility.ASYNCAPEX_STATUS_QUEUED,
            CommonUtility.ASYNCAPEX_STATUS_PERPARING,
            CommonUtility.ASYNCAPEX_STATUS_PROCESSING
        };

        List<AsyncApexJob> jobList = [
            SELECT Id, ApexClass.Name
            FROM AsyncApexJob 
            WHERE ApexClass.Name IN :classNameSet
            AND Status IN :inProgressStatuses
        ];

        return !jobList.isEmpty();
    }

}