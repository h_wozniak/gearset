public with sharing class LEX_Transfer_Enrollment_to_KSController {

	@AuraEnabled
	public static String checkRecord(String recordId) {

		Enrollment__c enr = [SELECT Status__c, Unenrolled_Status__c FROM Enrollment__c WHERE Id = :recordId];
		Boolean permissionSetAccess = LEXStudyUtilities.CheckIfCanSendToKS();
		Boolean permissionSetAccessToKS = LEXStudyUtilities.checkIfCanEditUneditableEnrollment();

		if (permissionSetAccess || enr.Unenrolled_Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT ||
			permissionSetAccessToKS || enr.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT ) {
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String createEducationAgreement(String recordId) {
		return LEXStudyUtilities.createEducationAgreement(new List<String>{recordId});
	}

	@AuraEnabled
	public static String transferEnrollmentsToKS(String recordId) {
		return LEXStudyUtilities.transferEnrollmentsToKS(new List<String>{recordId}, false, false);
	}
}