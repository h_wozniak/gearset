@isTest
private class ZTestChangeStatusOnContactActivity {

	@isTest static void test_ChangeStatusOnContactActivity() {
		Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email='test@test.co'), true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co', iPressoId__c = '1234'), true);

		List<ActivityManager.ChangeStatusActivity> changeActivity = new List<ActivityManager.ChangeStatusActivity>();
		changeActivity.add(new ActivityManager.ChangeStatusActivity(candidate.Id, 'Candidate', 'Graduate', System.now()));

		Test.startTest();
		System.enqueueJob(new ChangeStatusOnContactActivity(changeActivity, 'zmiana_statusu', 'stary_status', 'nowy_status'));
		Test.stopTest();
	}
}