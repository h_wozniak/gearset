/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to iPresso Activity Business Logic
**/

public without sharing class ChangeStatusOnContactActivity  implements Queueable, Database.AllowsCallouts {
	List<ActivityManager.ChangeStatusActivity> changeStatusActivities;
	String activityName;
	String previousStatusName;
	String newStatusName;

	public ChangeStatusOnContactActivity(List<ActivityManager.ChangeStatusActivity> changeStatusActivities, String activityName, String previousStatusName, String newStatusName) {
		this.changeStatusActivities = changeStatusActivities;
		this.activityName = activityName;
		this.previousStatusName = previousStatusName;
		this.newStatusName = newStatusName;
	}

	public void execute(QueueableContext qc) {
		changeStatusOnContactActivity(changeStatusActivities, activityName, previousStatusName, newStatusName);
	}

    /* 
        Method used to send activity to iPresso when Minor Status (Prospect, MQL, SAL, SQL) is changed or Main Status is changed (Candidate, Interested).
        Parameter activityName is responsible for determining which Status is supposed to be taken in consideration.
    */
    public static void changeStatusOnContactActivity(List<ActivityManager.ChangeStatusActivity> changeStatusActivities, String activityName, String previousStatusName, String newStatusName) {
        List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON> iPressoActivities = new List<IntegrationIPressoSendActivities.IPressoActivityContact_JSON>();

        Set<Id> contactIds = new Set<Id>();
        for (ActivityManager.ChangeStatusActivity changeStatusActivity : changeStatusActivities) {
            contactIds.add(changeStatusActivity.contactId);
        }

        Map<Id, Contact> contacts = new Map<Id, Contact>([
            SELECT Id,
            (SELECT Id, iPressoId__c FROM iPressoContacts__r WHERE iPressoId__c != null)
            FROM Contact
            WHERE Id IN :contactIds
        ]);

        Map<String, String> statusesMap = new Map<String, String> {
            'Interested' => 'zainteresowany',
            'Candidate' => 'kandydat',
            'Accepted' => 'przyjęty',
            'Student' => 'Student',
            'Graduate' => 'absolwent',
            'Foreigner' => 'Obcokrajowiec',
            'Duplicate' => 'Duplikat'
        };

        for (ActivityManager.ChangeStatusActivity changeStatusActivity : changeStatusActivities) {
            Contact contactWithData = contacts.get(changeStatusActivity.contactId);
            for (Marketing_Campaign__c contact : contactWithData.iPressoContacts__r) {
                iPressoActivities.add(ActivityManager.prepareActivityForChangeStatus(contact.iPressoId__c, changeStatusActivity, activityName, translateStatusToPL(previousStatusName, statusesMap), translateStatusToPL(newStatusName, statusesMap)));
            }
        }

        if (!Test.isRunningTest()) {
            IntegrationIPressoSendActivities.sendActivitiesListSync(JSON.serialize(iPressoActivities));
        }
    }

    public static String translateStatusToPL(String text, Map<String, String> statusesMap) {
        for (String value : statusesMap.keySet()) {
            text = text.replaceAll(value, statusesMap.get(value));
        }

        return text;
    }
}