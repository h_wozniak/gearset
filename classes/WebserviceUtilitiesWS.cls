/**
*   @author         Wojciech Słodziak, editor Mateusz Pruszyński
*   @description    Utility class, that is used to store methods to be called from js buttons
**/
global without sharing class WebserviceUtilitiesWS {


    /* Wojciech Słodziak */
    // Method that allows deleting records without having standard permissions
    webservice static Boolean deleteRecords(List<Id> recordIds, String objName) {
        try {
            deleteRecordsWithExc(recordIds, objName);
        } catch (Exception ex) {
            if (!ex.getMessage().contains(Label.msg_error_cantDeleteIPressoContact)) {
                ErrorLogger.log(ex);
            }
            return false;
        }
        return true;
    }

    /* Wojciech Słodziak */
    // Method that allows deleting records without having standard permissions
    // throws Exception up in stack
    public static void deleteRecordsWithExc(List<Id> recordIds, String objName) {
        List<sObject> recordList = new List<sObject>();
        for (Id rId : recordIds) {
            sObject sObj = Schema.getGlobalDescribe().get(objName).newSObject();
            sObj.Id = rId;
            recordList.add(sObj);
        }

        delete recordList;
    }
}