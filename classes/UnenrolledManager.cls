/**
*   @author         Wojciech Słodziak
*   @description    Helper class for managing logic related to unenrolled candidate process.
**/

public without sharing class UnenrolledManager {


    public static Set<String> finalizedUnenrStatuses = new Set<String> {
            CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT,
            CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS,
            CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS
    };

    public static final Integer numberOfUnenrolledSemesters = 2;

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- UNENROLLMENT PROCESS INIT ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz, editor Wojciech Słodziak */
    // method to perform actions caused by entering unenrolled path
    public static void performUnenrolledActions(Set<Id> studyEnrIds) {
        Set<Id> relatedDocIds = new Set<Id> {
                CatalogManager.getDocumentDiplomaId(),
                CatalogManager.getDocumentAgreementUnenrolledPLId(),
                CatalogManager.getDocumentAgreementUnenrolledId()
        };

        Map<Id, Enrollment__c> studyEnrMap = new Map<Id, Enrollment__c>([
                SELECT Id, University_Name__c, Degree__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, Language_of_Enrollment__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, Unenrolled_Status__c,
                (SELECT Id, Delivery_Deadline__c, Enrollment_from_Documents__c, Document__c, Reference_Number__c, Language_of_Enrollment__c
                FROM Documents__r
                WHERE Document__c IN :relatedDocIds)
                FROM Enrollment__c
                WHERE Id IN :studyEnrIds
        ]);

        // update Unenrolled_Status__c
        for (Enrollment__c studyEnr : studyEnrMap.values()) {
            studyEnr.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_COD;
        }

        try {
            update studyEnrMap.values();
        } catch(Exception e) {
            ErrorLogger.log(e);
        }

        UnenrolledManager.handleDocumentChanges(studyEnrMap);
    }

    /* Wojciech Słodziak */
    // helper method for handling changes in documents after turning on unenrolled candidate path
    private static void handleDocumentChanges(Map<Id, Enrollment__c> studyEnrMap) {
        List<Enrollment__c> enrDocumentsToGenerate = new List<Enrollment__c>();
        List<Enrollment__c> enrDocumentsToUpsert = new List<Enrollment__c>();

        for (Enrollment__c studyEnr : studyEnrMap.values()) {
            Date studyStartDate = EnrollmentManager_Studies.getStudyStartDateFromEnr(studyEnr);

            Enrollment__c exisitingAgr;
            Enrollment__c exisitingAgrPL;
            for (Enrollment__c documentOnEnrollment : studyEnr.Documents__r) {
                if (documentOnEnrollment.Document__c == CatalogManager.getDocumentDiplomaId()) {
                    documentOnEnrollment.Delivery_Deadline__c = Date.newInstance(studyStartDate.year(), 11, 30);
                }

                if (documentOnEnrollment.Document__c == CatalogManager.getDocumentAgreementUnenrolledId()) {
                    exisitingAgr = documentOnEnrollment;
                }
                else if (documentOnEnrollment.Document__c == CatalogManager.getDocumentAgreementUnenrolledPLId()) {
                    exisitingAgrPL = documentOnEnrollment;
                }
                else {
                    enrDocumentsToUpsert.add(documentOnEnrollment);
                }
            }

            if (exisitingAgr == null) {
                Enrollment__c docToInsert = DocumentManager.createDocumentEnrollment(
                        studyEnr.Id, CatalogManager.getDocumentAgreementUnenrolledId(), CommonUtility.ENROLLMENT_FDI_STAGE_COD
                );
                docToInsert.Language_of_Enrollment__c = studyEnr.Language_of_Enrollment__c;

                enrDocumentsToUpsert.add(docToInsert);
                enrDocumentsToGenerate.add(docToInsert);
            } else {
                exisitingAgr.For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD;
                exisitingAgr.Current_File__c = CommonUtility.DURING_GENERATION;
                exisitingAgr.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                exisitingAgr.Language_of_Enrollment__c = studyEnr.Language_of_Enrollment__c;

                enrDocumentsToUpsert.add(exisitingAgr);
                enrDocumentsToGenerate.add(exisitingAgr);
            }

            if (studyEnr.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                if (exisitingAgrPL == null) {
                    Enrollment__c docToInsert = DocumentManager.createDocumentEnrollment(
                            studyEnr.Id, CatalogManager.getDocumentAgreementUnenrolledPLId(), null
                    );
                    docToInsert.Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH;

                    enrDocumentsToUpsert.add(docToInsert);
                    enrDocumentsToGenerate.add(docToInsert);
                } else {
                    exisitingAgrPL.For_delivery_in_Stage__c = null;
                    exisitingAgrPL.Current_File__c = CommonUtility.DURING_GENERATION;
                    exisitingAgrPL.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                    exisitingAgrPL.Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH;

                    enrDocumentsToUpsert.add(exisitingAgrPL);
                    enrDocumentsToGenerate.add(exisitingAgrPL);
                }
            }
        }

        try {
            CommonUtility.skipDocumentValidations = true;
            upsert enrDocumentsToUpsert;

            List<GenerateDocumentsQueueable.DocumentWrapper> documentWrapperList = new List<GenerateDocumentsQueueable.DocumentWrapper>();
            for (Enrollment__c docToInsert : enrDocumentsToGenerate) {
                Enrollment__c studyEnr = studyEnrMap.get(docToInsert.Enrollment_from_Documents__c);
                documentWrapperList.add(new GenerateDocumentsQueueable.DocumentWrapper(
                        docToInsert.Language_of_Enrollment__c,
                        RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED,
                        studyEnr.Degree__c,
                        studyEnr.University_Name__c,
                        docToInsert.Reference_Number__c,
                        docToInsert.Id,
                        true,
                        true
                ));
                documentWrapperList.add(new GenerateDocumentsQueueable.DocumentWrapper(
                        docToInsert.Language_of_Enrollment__c,
                        RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED,
                        studyEnr.Degree__c,
                        studyEnr.University_Name__c,
                        docToInsert.Reference_Number__c,
                        docToInsert.Id,
                        true,
                        false
                ));
            }

            System.enqueueJob(new GenerateDocumentsQueueable(documentWrapperList, null, 0));
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------- UNENROLLMENT PROCESS FINALIZATION --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz, editor: Wojciech Słodziak */
    // method to perform cancelation of unenrolled path and return to standard enrollment flow
    // if process is finished (finalizedUnenrStatuses) generate standard Agreement again from different template
    public static void finishUnenrolledPath(Set<Id> studyEnrIds) {
        Set<Id> relatedDocIds = new Set<Id> {
                CatalogManager.getDocumentDiplomaId(),
                CatalogManager.getDocumentAgreementId()
        };

        Map<Id, Enrollment__c> studyEnrMap = new Map<Id, Enrollment__c>([
                SELECT Id, University_Name__c, Degree__c, Unenrolled_Status__c, Delivery_Deadline__c,
                (SELECT Id, Delivery_Deadline__c, Enrollment_from_Documents__c, Document__c, Reference_Number__c
                FROM Documents__r
                WHERE Document__c IN :relatedDocIds)
                FROM Enrollment__c
                WHERE Id IN :studyEnrIds
        ]);

        List<Enrollment__c> enrDocumentsToUpsert = new List<Enrollment__c>();
        List<Enrollment__c> enrDocumentsToGenerate = new List<Enrollment__c>();

        for (Enrollment__c studyEnr : studyEnrMap.values()) {
            if (UnenrolledManager.finalizedUnenrStatuses.contains(studyEnr.Unenrolled_Status__c)) {
                // finalized process
                Enrollment__c exisitingAgr;
                for (Enrollment__c documentOnEnrollment : studyEnr.Documents__r) {
                    if (documentOnEnrollment.Document__c == CatalogManager.getDocumentAgreementId()) {
                        exisitingAgr = documentOnEnrollment;
                    }

                    enrDocumentsToUpsert.add(documentOnEnrollment);
                }

                if (exisitingAgr == null) {
                    Enrollment__c docToInsert = DocumentManager.createDocumentEnrollment(
                            studyEnr.Id, CatalogManager.getDocumentAgreementId(), CommonUtility.ENROLLMENT_FDI_STAGE_AD
                    );
                    enrDocumentsToUpsert.add(docToInsert);
                    enrDocumentsToGenerate.add(docToInsert);
                } else {
                    //exisitingAgr.Current_File__c = CommonUtility.DURING_GENERATION;
                    //exisitingAgr.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                    enrDocumentsToGenerate.add(exisitingAgr);
                }
            } else {
                // cancelled process
                for (Enrollment__c documentOnEnrollment : studyEnr.Documents__r) {
                    if (documentOnEnrollment.Document__c == CatalogManager.getDocumentDiplomaId()) {
                        documentOnEnrollment.Delivery_Deadline__c = studyEnr.Delivery_Deadline__c;
                    }

                    enrDocumentsToUpsert.add(documentOnEnrollment);
                }

            }
        }

        try {
            CommonUtility.skipDocumentValidations = true;
            upsert enrDocumentsToUpsert;

            //for (Enrollment__c docToInsert : enrDocumentsToGenerate) {
            //    Enrollment__c studyEnr = studyEnrMap.get(docToInsert.Enrollment_from_Documents__c);

            //    DocumentGeneratorManager.generateDocument(
            //        RecordVals.CATALOG_DOCUMENT_AGREEMENT, // TODO invoke special post-unenrolled agreement template generation
            //        studyEnr.Degree__c,
            //        studyEnr.University_Name__c,
            //        docToInsert.Reference_Number__c,
            //        docToInsert.Id,
            //        true
            //    );
            //}
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ DOCUMENT MODIFICATION ACTIONS ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz */
    // on Enrollment Study update Status if Agreement's status is changed
    public static void updateStudyEnrollmentStatusOnUnerolledAgreementChange(List<Enrollment__c> documentsWithUnenrAgreement) {
        Set<Id> mainEnrollmentsId = new Set<Id>();
        for (Enrollment__c documentWithAgreement : documentsWithUnenrAgreement) {
            mainEnrollmentsId.add(documentWithAgreement.Enrollment_from_Documents__c);
        }

        List<Enrollment__c> mainEnrollments = [
                SELECT Id, Unenrolled_Status__c
                FROM Enrollment__c
                WHERE Id IN :mainEnrollmentsId
        ];

        for (Enrollment__c documentWithAgreement : documentsWithUnenrAgreement) {
            for (Enrollment__c mainEnrollment : mainEnrollments) {
                if (documentWithAgreement.Enrollment_from_Documents__c == mainEnrollment.Id) {
                    if (documentWithAgreement.Document_Accepted__c) {
                        mainEnrollment.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT;
                        mainEnrollment.Agreement_Date__c = System.today();
                        mainEnrollment.Unenrolled_Signed_Contract_Date__c = documentWithAgreement.Acceptance_Date__c;
                    }
                    else {
                        mainEnrollment.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_COD;
                        mainEnrollment.Agreement_Date__c = null;
                        mainEnrollment.Unenrolled_Signed_Contract_Date__c = null;
                    }
                }
            }
        }

        try {
            update mainEnrollments;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- VALIDATIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz */
    // on Enrollment Study check if Status is set properly before updating agreements status (UNENROLLED)
    public static void checkIfEnrollmentIsInProperStatusBeforeUnenrolledAgreement(List<Enrollment__c> unenrolledAgreementsToCheckIfEnrIsInStatus) {
        Set<Id> mainEnrollmentsId = new Set<Id>();
        for (Enrollment__c unenrolledAgreementToCheckIfEnrIsInStatus : unenrolledAgreementsToCheckIfEnrIsInStatus) {
            mainEnrollmentsId.add(unenrolledAgreementToCheckIfEnrIsInStatus.Enrollment_from_Documents__c);
        }

        List<Enrollment__c> mainEnrollments = [
                SELECT Id, Unenrolled_Status__c, Unenrolled__c
                FROM Enrollment__c
                WHERE Id IN :mainEnrollmentsId
        ];

        for (Enrollment__c unenrolledAgreementToCheckIfEnrIsInStatus : unenrolledAgreementsToCheckIfEnrIsInStatus) {
            for (Enrollment__c mainEnrollment : mainEnrollments) {
                if (unenrolledAgreementToCheckIfEnrIsInStatus.Enrollment_from_Documents__c == mainEnrollment.Id) {
                    if (!mainEnrollment.Unenrolled__c || mainEnrollment.Unenrolled_Status__c != CommonUtility.ENROLLMENT_UNENR_STATUS_COD) {
                        unenrolledAgreementToCheckIfEnrIsInStatus.addError(Label.msg_error_CantMarkUnenrolledAgreementAsAccepted);
                    }
                }
            }
        }
    }

}