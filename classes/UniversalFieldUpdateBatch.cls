/**
* @author       Wojciech Słodziak
* @description  Batch class for updating fields on multiple records in Database.
**/

/* ------------------------------------------ example --------------------------------------------
Database.executeBatch(
    new UniversalFieldUpdateBatch(
        'SELECT Id FROM Contact WHERE Confirmed_Contact__c = false',
        new List<Schema.SObjectField> { Contact.Confirmed_Contact__c },
        new List<Object> { true }
    ),
    100
);
------------------------------------------- end of example ------------------------------------- */

global class UniversalFieldUpdateBatch implements Database.Batchable<sObject> {
    
    String query;
    List<Schema.SObjectField> fieldToUpdateList;
    List<Object> fieldValueList;
    
    global UniversalFieldUpdateBatch(String soqlQuery, List<Schema.SObjectField> fieldToUpdateList, List<Object> fieldValueList) {
        this.query = soqlQuery;
        this.fieldToUpdateList = fieldToUpdateList;
        this.fieldValueList = fieldValueList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject obj : scope) {
            for (Integer index = 0; index < fieldToUpdateList.size(); index++) {
                obj.put(fieldToUpdateList.get(index), fieldValueList.get(index));
            }
        }

        ContactMergeManager.isVerificationActionTransaction = true;
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}