/**
*   @author         Sebastian Łasisz
*   @description    batch class that retrieves arrears from Experia for educational agreements
**/

/**
*   To initiate the RetrieveArrearsBatch execute below code in Developer Console (execute Anonymous Code)
*
*   Database.executeBatch(new RetrieveArrearsBatch(), 200)
**/
global class RetrieveArrearsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Portal_Login__c
                FROM Enrollment__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                AND Student_from_Educational_Agreement__c != null
                AND (Balance__c < 0
                OR Didactics_Status__c != :CommonUtility.ENROLLMENT_DIDACTICS_STATUS_FINISHED_EDUCATION)
                AND University_Name__c = :CommonUtility.MARKETING_ENTITY_GDANSK
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        IntegrationArrearsRetrieval.retrieveArrearsForLogins(enrList);
    }

    global void finish(Database.BatchableContext BC) {
    }
}