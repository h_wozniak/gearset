@isTest
private class ZTestIntegrationIpressoContactCreatSend {

	@isTest static void test_sendIPressoContactListAtFuture() {		
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.IPCCS_SuccessSendCalloutMock(iPressoContact.Id));

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);
        IntegrationIpressoContactCreationSender.sendIPressoContactListAtFuture(new Set<Id> { candidate.Id });
        Test.stopTest();

        Marketing_Campaign__c iPressoContactWithId = [
        	SELECT Id, iPressoId__c
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        System.assertEquals(iPressoContactWithId.iPressoId__c, '123');
	}

	@isTest static void test_sendIPressoContactList() {		
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.IPCCS_SuccessSendCalloutMock(iPressoContact.Id));

        Test.startTest();
        IntegrationIpressoContactCreationSender.sendIPressoContactList(new Set<Id> { candidate.Id });
        Test.stopTest();

        Marketing_Campaign__c iPressoContactWithId = [
        	SELECT Id, iPressoId__c
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        System.assertEquals(iPressoContactWithId.iPressoId__c, '123');
	}

	@isTest static void test_prepareIPressoContactList() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        String result = IntegrationIpressoContactCreationSender.prepareIPressoContactList(new Set<Id> { candidate.Id });
        System.assertNotEquals(result, null);
	}

	@isTest static void test_prepareDepartmentForIPresso() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
        	SELECT Id, University_for_sharing__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertNotEquals(candidate.University_for_sharing__c, null);

        IntegrationIpressoContactCreationSender.ContactIPresso_JSON candidate_JSON = new IntegrationIpressoContactCreationSender.ContactIPresso_JSON();
        IntegrationIpressoContactCreationSender.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'poznan');
	}

	@isTest static void test_prepareDepartmentForIPresso_Enrollment() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
        	SELECT Id, University_for_sharing__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);
        Test.stopTest();

        candidate = [
        	SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c, Status__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1)
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań, WSB Wrocław');

        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationIpressoContactCreationSender.ContactIPresso_JSON candidate_JSON = new IntegrationIpressoContactCreationSender.ContactIPresso_JSON();
        IntegrationIpressoContactCreationSender.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'wroclaw');
	}

	@isTest static void test_prepareDepartmentForIPresso_Didactic() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
        	SELECT Id, University_for_sharing__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Enrollment__c didactic = ZDataTestUtility.createEducationalAgreement(new Enrollment__c(Student_from_Educational_Agreement__c = candidate.Id, University_Name__c = 'WSB Gdańsk'), true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationIpressoContactCreationSender.ContactIPresso_JSON candidate_JSON = new IntegrationIpressoContactCreationSender.ContactIPresso_JSON();
        IntegrationIpressoContactCreationSender.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'gdansk');
	}

	@isTest static void test_prepareDepartmentForIPresso_Lead() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
        	SELECT Id, University_for_sharing__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Marketing_Campaign__c lead = ZDataTestUtility.createReturnedLead(new Marketing_Campaign__c(Lead_UniversityWSB__c = 'WSB Gdańsk', Contact_From_Returning_Leads__c = candidate.Id), true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationIpressoContactCreationSender.ContactIPresso_JSON candidate_JSON = new IntegrationIpressoContactCreationSender.ContactIPresso_JSON();
        IntegrationIpressoContactCreationSender.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'gdansk');
	}

    @isTest static void test_prepareDepartmentForIPresso_training() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Offer__c training = ZDataTestUtility.createTrainingOffer(null, true);
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(Participant__c = candidate.Id, Training_Offer_for_Participant__c = training.Id) , true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.Participants__r.size(), 1);
        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        IntegrationIpressoContactCreationSender.ContactIPresso_JSON candidate_JSON = new IntegrationIpressoContactCreationSender.ContactIPresso_JSON();
        IntegrationIpressoContactCreationSender.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertNotEquals(candidate_JSON.department, 'poznan');
    }

	@isTest static void test_prepareConsentsForContact() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        Test.startTest();
        ZDataTestUtility.prepareCatalogData(true, true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);
        Test.stopTest();

        candidate.Consent_Marketing_ADO__c = 'WSB Wrocław';
        update candidate;

        candidate = [
        	SELECT Id, Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c, Consent_PUCA_ADO__c, Consent_PUCA__c,
                   Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c,Consent_Marketing_ADO__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assert(candidate.Consent_Marketing__c, true);
        System.assertEquals(candidate.Consent_Marketing_ADO__c, 'WSB Wrocław');

        Marketing_Campaign__c iPressoContact = [
        	SELECT Id
        	FROM Marketing_Campaign__c
        	WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        List<Catalog__c> consents = [
            SELECT Id, Name, Active_To__c, Active_From__c, iPressoId__c, ADO__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
        ];

        for (Catalog__c consent : consents) {
        	consent.Send_To_iPresso__c = true;
        	consent.IPressoId__c = '1';
        }

        update consents;

        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name, Active_To__c, Active_From__c, iPressoId__c, ADO__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
            AND Send_To_iPresso__c = true
            AND IPressoId__c != null
        ];

        System.assert(catalogConsentsToValidate.size() > 0);

        List<IntegrationIpressoContactCreationSender.MarketingConsents_JSON> result = IntegrationIpressoContactCreationSender.prepareConsentsForContact(candidate, catalogConsentsToValidate);
        System.assert(result.size() > 0);
	}
}