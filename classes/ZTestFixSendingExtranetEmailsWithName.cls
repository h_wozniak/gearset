@isTest
private class ZTestFixSendingExtranetEmailsWithName {
    
    @isTest static void test_Batch() {
    	Enrollment__c didacticts = ZDataTestUtility.createEducationalAgreement(new Enrollment__c(Student_No__c = '1244', Portal_Login__c = '12354'), true);

        Test.startTest();
    	Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = 'Didactics in KS', Educational_Agreement__c = didacticts.Id, Dont_send_automatic_emails__c = false), true);

        Enrollment__c studyEnrBeforeQuery = [
        	SELECT Id, Dont_send_automatic_emails__c, Name
        	FROM Enrollment__c
        	WHERE Id = :studyEnr.Id
        ];

		Database.executeBatch(new FixSendingExtranetEmailsWithNameBatch(new Set<String> { studyEnrBeforeQuery.Name }), 1);
        Test.stopTest();

        Enrollment__c studyEnrAfterQ = [
        	SELECT Id, Dont_send_automatic_emails__c
        	FROM Enrollment__c
        	WHERE Id = :studyEnr.Id
        ];

        System.assertEquals(studyEnrAfterQ.Dont_send_automatic_emails__c, false);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

}