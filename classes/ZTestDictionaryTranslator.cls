@isTest
private class ZTestDictionaryTranslator {
	
	@isTest static void test_getTranslationsJSON() {
        PageReference pageRef = Page.DictionaryTranslator;
        Test.setCurrentPageReference(pageRef);

		ApexPages.currentPage().getParameters().put('langCode', CommonUtility.LANGUAGE_CODE_EN_US);
		ApexPages.currentPage().getParameters().put('sObjFieldName', 'Offer__c.Price_Book_Type__c');

		DictionaryTranslator dt = new DictionaryTranslator();

		Test.startTest();
		String json = dt.getTranslationsJSON();
		Test.stopTest();

		System.assertNotEquals(null, json);
		System.assert(json.contains(CommonUtility.OFFER_PBTYPE_STANDARD));
	}
	
	@isTest static void test_getTranslatedPicklistValues() {
		Map<String, String> resultMap = DictionaryTranslator.getTranslatedPicklistValues(CommonUtility.LANGUAGE_CODE_EN_US, Offer__c.Price_Book_Type__c);

		System.assertNotEquals(null, resultMap);
	}
	
}