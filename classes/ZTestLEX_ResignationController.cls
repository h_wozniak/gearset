@isTest
private class ZTestLEX_ResignationController {

    @isTest static void test_checkStatus() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(null, true);
        String result = LEX_ResignationController.checkStatus(participant.Id);
        System.assertEquals(result, 'TRUE');
    }

    @isTest static void test_checkStatus_2() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES), true);
        String result = LEX_ResignationController.checkStatus(participant.Id);
        System.assertEquals(result, 'FALSE');
    }

    @isTest static void test_hasEditAccess() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(null, true);
        String result = LEX_ResignationController.hasEditAccess(participant.Id);
        System.assertEquals(result, 'SUCCESS');
    }

    @isTest static void test_updateFieldValues() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(null, true);
        String result = LEX_ResignationController.updateFieldValues(participant.Id, 'Enrollment__c', new List<String> {'Status__c'}, new List<String> {'string'}, new List<String> {'Resignation'});

        System.assertEquals(result, 'SUCCESS');
    }
}