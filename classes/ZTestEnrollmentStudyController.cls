@isTest
private class ZTestEnrollmentStudyController {


    private static Map<Integer, sObject> prepareData1() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_ENR_2 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true);

        retMap.put(0, ZDataTestUtility.createCourseSpecialtyOffer(null, true));
        retMap.put(1, ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true));

        
        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];
        
        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }
        
        update pb.InstallmentConfigs__r;
    }



    @isTest static void testSaveEnrollment() {
        //ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareData1();

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getParameters().put('offerId', dataMap.get(0).Id);
        ApexPages.CurrentPage().getParameters().put('candidateId', dataMap.get(1).Id);
        ApexPages.CurrentPage().getParameters().put('retURL', '/');
        
        Test.startTest();
        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        System.assert(!cntrl.showContactLookup);

        cntrl.saveEnrollment();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for (ApexPages.Message message : pageMessages) {
                messageFound = true;   
        }
        System.assert(!messageFound);


        List<Enrollment__c> newEnrollments = [
            SELECT Id
            FROM Enrollment__c 
            WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        ];
        System.assertEquals(1, newEnrollments.size());

        Test.stopTest();
    }

    @isTest static void test_getFinishedUniversities() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        Qualification__c finishedUniversityWithData = [
            SELECT Id, University_from_Finished_University__r.Name, Name, ToLabel(Status__c), Defense_Date__c, Finished_Course__c, Graduation_Year__c, Diploma_Number__c
            FROM Qualification__c
            WHERE Id = :finishedUniversity.Id
        ];

        System.assertEquals(cntrl.finishedUniversitiesList.size(), 1);

        System.assertEquals(
            cntrl.finishedUniversitiesList.get(0).getLabel(), 
            finishedUniversityWithData.University_from_Finished_University__r.Name + ', ' + Label.title_finishedCourse + ': Archeologia, ' + Label.title_graduationYear + ': 1991'
        );

        System.assertEquals(cntrl.selectedUniversity, finishedUniversityWithData.Id);

        Test.stopTest();
    }

    @isTest static void test_prepareLanguageList() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Offer__c languageOnoffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = selectedOffer.Id), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.ew.languageList.size(), 1);
        System.assertEquals(cntrl.ew.languageList.get(0).languageName, languageOnOffer.Language__c);

        Test.stopTest();
    }

    @isTest static void test_preparePostGraduateOfferProductList() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.selectedOffer.Degree__c, CommonUtility.OFFER_DEGREE_II_PG);

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        System.assertEquals(cntrl.ew.postGraduateOfferProductList.size(), 1);
        System.assertEquals(cntrl.ew.postGraduateOfferProductList.get(0).getLabel(), selectedOfferAfterInsert.Specialization__r.Name + ', ' + selectedOfferAfterInsert.Specialization__r.Trade_Name__c);

        Test.stopTest();        
    }

    @isTest static void test_setStudent() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('contactId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        cntrl.setStudent();

        System.assertEquals(cntrl.finishedUniversitiesList.size(), 1);

        Test.stopTest();
    }

    @isTest static void test_setUniversity() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('contactId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        cntrl.setUniversity();

        System.assertEquals(cntrl.finishedUniversitiesList.size(), 1);
        System.assertEquals(cntrl.selectedUniversity, finishedUniversity.Id);

        Test.stopTest();
    }

    @isTest static void test_changeStudent() { 
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        cntrl.changeStudent();

        Test.stopTest();

        System.assertEquals(cntrl.ew.enrollment.Candidate_Student__c, null);
        System.assertEquals(cntrl.finishedUniversitiesList.size(), 0);
    }

    @isTest static void test_tuitionSystemChange() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(null, true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);    

        Test.startTest();
        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        PageReference change = cntrl.tuitionSystemChange();
        Test.stopTest();

        System.assertEquals(change, null);
    }


    @isTest static void test_Cancel_withURL() {
        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('retURL', '/');       

        Test.startTest();
        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        Test.stopTest();

        System.assertNotEquals(cntrl.cancel(), null);
    }


    @isTest static void test_Cancel_withoutURL() {
        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);      

        Test.startTest();
        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        Test.stopTest();

        System.assertEquals(cntrl.cancel(), null);
    }

    @isTest static void test_save() {
        //ZDataTestUtility.prepareCatalogData(true, true);

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);
        Offer__c languageOnoffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = selectedOffer.Id), true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.selectedOffer.Degree__c, CommonUtility.OFFER_DEGREE_II_PG);

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        System.assertEquals(cntrl.ew.postGraduateOfferProductList.size(), 1);
        System.assertEquals(cntrl.ew.postGraduateOfferProductList.get(0).getLabel(), selectedOfferAfterInsert.Specialization__r.Name + ', ' + selectedOfferAfterInsert.Specialization__r.Trade_Name__c);

        cntrl.selectedPostGraduateOfferProduct = cntrl.ew.postGraduateOfferProductList.get(0).getValue();

        cntrl.ew.languageList.get(0).checked = true;
        cntrl.ew.languageList.get(0).languageEnrollment.Language_Level__c = 'Advanced';

        PageReference saveRef = cntrl.saveEnrollment();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();

        Boolean isError = false;
        for (ApexPages.Message msg : pageMessages) {
            isError = true;
            System.debug('msg ' + msg);
        }

        System.assert(!isError);

        Test.stopTest();        

        System.assertEquals(saveRef, null);
    }

    @isTest static void test_save_withoutSelectedPG() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        try {
            PageReference saveRef = cntrl.saveEnrollment();
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(Label.msg_error_productOnOfferNotSelected));
        }

        Test.stopTest();        
    }

    @isTest static void test_save_withoutConsents() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);
        Offer__c languageOnoffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = selectedOffer.Id), true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.selectedOffer.Degree__c, CommonUtility.OFFER_DEGREE_II_PG);

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        System.assertEquals(cntrl.ew.postGraduateOfferProductList.size(), 1);
        System.assertEquals(cntrl.ew.postGraduateOfferProductList.get(0).getLabel(), selectedOfferAfterInsert.Specialization__r.Name + ', ' + selectedOfferAfterInsert.Specialization__r.Trade_Name__c);

        cntrl.selectedPostGraduateOfferProduct = cntrl.ew.postGraduateOfferProductList.get(0).getValue();

        cntrl.ew.languageList.get(0).checked = true;
        
        PageReference saveRef = cntrl.saveEnrollment();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();

        Boolean isError = false;
        for (ApexPages.Message msg : pageMessages) {
            isError = true;
        }

        System.assert(isError);

        Test.stopTest();        

        System.assertEquals(saveRef, null);
    }

    @isTest static void test_fillEnrWithPreviousEnrData() {
        //ZDataTestUtility.prepareCatalogData(true, true);

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);
        Offer__c languageOnoffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = selectedOffer.Id), true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.selectedOffer.Degree__c, CommonUtility.OFFER_DEGREE_II_PG);

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        System.assertEquals(cntrl.ew.postGraduateOfferProductList.size(), 1);
        System.assertEquals(cntrl.ew.postGraduateOfferProductList.get(0).getLabel(), selectedOfferAfterInsert.Specialization__r.Name + ', ' + selectedOfferAfterInsert.Specialization__r.Trade_Name__c);

        cntrl.selectedPostGraduateOfferProduct = cntrl.ew.postGraduateOfferProductList.get(0).getValue();

        cntrl.ew.languageList.get(0).checked = true;
        cntrl.ew.languageList.get(0).languageEnrollment.Language_Level__c = 'Advanced';

        PageReference saveRef = cntrl.saveEnrollment();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();

        Boolean isError = false;
        for (ApexPages.Message msg : pageMessages) {
            isError = true;
        }

        System.assert(!isError);

        Test.stopTest();        

        System.assertEquals(saveRef, null);

        List<Enrollment__c> studyEnr = [
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        ];

        cntrl.fillEnrWithPreviousEnrData(new Enrollment__c(), studyEnr.get(0).Id);
    }

    @isTest static void test_save_disableAnnexing() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'abc',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);

        Catalog__c con1 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1,
                Consent_ADO_API_Name__c = 'Consent_Marketing_ADO__c',
                Consent_API_Name__c = 'Consent_Marketing__c'
        ), true);
        Catalog__c con2 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_2,
                Consent_ADO_API_Name__c = 'Consent_Electronic_Communication_ADO__c',
                Consent_API_Name__c = 'Consent_Electronic_Communication__c'
        ), true);
        Catalog__c con3 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3,
                Consent_ADO_API_Name__c = 'Consent_PUCA_ADO__c',
                Consent_API_Name__c = 'Consent_PUCA__c'
        ), true);
        Catalog__c con4 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_4,
                Consent_ADO_API_Name__c = 'Consent_Direct_Communications_ADO__c',
                Consent_API_Name__c = 'Consent_Direct_Communications__c'
        ), true);
        Catalog__c con5 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_5,
                Consent_ADO_API_Name__c = 'Consent_Graduates_ADO__c',
                Consent_API_Name__c = 'Consent_Graduates__c'
        ), true);
        Catalog__c enr1 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_1,
                Consent_API_Name__c = 'Consent_Terms_of_Service_Acceptation__c'
        ), true);
        Catalog__c enr2 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_2,
                Consent_API_Name__c = 'Consent_Date_processing_for_contract__c'
        ), true);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c selectedOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = selectedOffer.Id), true);
        Offer__c languageOnoffer = ZDataTestUtility.createForeignLanguage(new Offer__c(Course_Offer_from_Language__c = selectedOffer.Id), true);
        Qualification__c finishedUniversity = ZDataTestUtility.createFinishedUniversity(new Qualification__c(Candidate_from_Finished_University__c = candidate.Id, Finished_Course__c = 'Archeologia', Graduation_Year__c = '1991'), true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);

        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.Id);        
        ApexPages.CurrentPage().getParameters().put('offerId', selectedOffer.Id);

        Test.startTest();

        EnrollmentStudyController cntrl = new EnrollmentStudyController();

        System.assertEquals(cntrl.selectedOffer.Degree__c, CommonUtility.OFFER_DEGREE_II_PG);

        Offer__c selectedOfferAfterInsert = [
            SELECT Id, Specialization__r.Name, Specialization__r.Trade_Name__c
            FROM Offer__c
            WHERE Id = :specialization.Id
        ];

        System.assertEquals(cntrl.ew.postGraduateOfferProductList.size(), 1);
        System.assertEquals(cntrl.ew.postGraduateOfferProductList.get(0).getLabel(), selectedOfferAfterInsert.Specialization__r.Name + ', ' + selectedOfferAfterInsert.Specialization__r.Trade_Name__c);

        cntrl.selectedPostGraduateOfferProduct = cntrl.ew.postGraduateOfferProductList.get(0).getValue();

        cntrl.ew.languageList.get(0).checked = true;
        cntrl.ew.languageList.get(0).languageEnrollment.Language_Level__c = 'Advanced';
        cntrl.previousEnr = new Enrollment__c(University_Name__c = 'WSB Kraków', Status__c = CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS);

        PageReference saveRef = cntrl.saveEnrollment();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();

        Boolean isError = false;
        for (ApexPages.Message msg : pageMessages) {
            isError = true;
        }

        System.assert(!isError);

        Test.stopTest();        

        System.assertNotEquals(saveRef, null);
    }
}