@isTest
private class ZTestEnrollmentManager_Trainings {
    
    private static Map<Integer, sObject> prepareDataX() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Enrollment_Value__c = 5,
            Discount_Value__c = 15
        ), true));
        retMap.put(1, ZDataTestUtility.createADHOCDiscount(new Discount__c(
            Enrollment__c = retMap.get(0).Id
        ), true));

        return retMap;
    }


    @isTest static void testEnrollmentManager() {
        Map<Integer, sObject> dataMap = prepareDataX();
        Set<Id> enrollmentIds = new Set<Id>();
        enrollmentIds.add(dataMap.get(0).Id);

        List<Enrollment__c> enrollmentsListBeforeDiscounts = new List<Enrollment__c>();
        enrollmentsListBeforeDiscounts.add((Enrollment__c)dataMap.get(0));

        Test.startTest();
        EnrollmentManager_Trainings.recalculateDiscountValue(enrollmentIds);
        List<Enrollment__c> enrollmentsListAfterDiscounts = [SELECT Id, Discount_Value__c, Enrollment_Value__c FROM Enrollment__c WHERE Id IN :enrollmentIds];
        System.assertNotEquals(enrollmentsListBeforeDiscounts, enrollmentsListAfterDiscounts);
        Test.stopTest();
    }
    

    private static Map<Integer, sObject> prepareData() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        
        return retMap;
    }

    private static Map<Integer, sObject> prepareData2() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Account company = ZDataTestUtility.createCompany(null, true);

        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Company__c = company.Id
        ), true));
        retMap.put(3, company);
        retMap.put(4, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        
        return retMap;
    }
    
    @isTest static void testDuplicateParticipants() {
        Map<Integer, sObject> dataMap = prepareData();


        Test.startTest();
        try {
            ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
                Enrollment_Training_Participant__c = ((Enrollment__c)dataMap.get(3)).Enrollment_Training_Participant__c,
                Training_Offer_for_Participant__c = ((Enrollment__c)dataMap.get(3)).Training_Offer_for_Participant__c,
                Participant__c = ((Enrollment__c)dataMap.get(3)).Participant__c
            ), true);
            System.assert(false);
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.msg_error_duplicateParticipantOnEnrollment)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();

    }

    @isTest static void testUpdateEnrollmentValue() {
        Map<Integer, sObject> dataMap = prepareData();

        Enrollment__c mainEnrollmentBefore = [SELECT Enrollment_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(1000, mainEnrollmentBefore.Enrollment_Value__c);

        Test.startTest();
        ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = ((Enrollment__c)dataMap.get(3)).Enrollment_Training_Participant__c,
            Training_Offer_for_Participant__c = ((Enrollment__c)dataMap.get(3)).Training_Offer_for_Participant__c,
            Enrollment_Value__c = ((Offer__c)dataMap.get(1)).Price_per_Person__c
        ), true);
        Test.stopTest();

        Enrollment__c mainEnrollment = [SELECT Enrollment_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(2000, mainEnrollment.Enrollment_Value__c);
    }

    @isTest static void testDiscountCalculation() {
        Map<Integer, sObject> dataMap = prepareData();

        Discount__c d = ZDataTestUtility.createADHOCDiscount(null, true);
        Discount__c dc = ZDataTestUtility.createDiscountConnector(new Discount__c(
            Discount_from_Discount_Connector__c = d.Id,
            Enrollment__c = dataMap.get(2).Id,
            Discount_Value__c = 50
        ), true);


        Enrollment__c mainEnrollmentBefore = [SELECT Discount_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(500, mainEnrollmentBefore.Discount_Value__c);

        Test.startTest();
        ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = ((Enrollment__c)dataMap.get(3)).Enrollment_Training_Participant__c,
            Training_Offer_for_Participant__c = ((Enrollment__c)dataMap.get(3)).Training_Offer_for_Participant__c,
            Enrollment_Value__c = ((Offer__c)dataMap.get(1)).Price_per_Person__c
        ), true);
        Enrollment__c mainEnrollmentAfter1 = [SELECT Discount_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(1000, mainEnrollmentAfter1.Discount_Value__c);

        Discount__c dcAfterInsert = [SELECT Discount_Value__c FROM Discount__c WHERE Id = :dc.Id];
        dcAfterInsert.Discount_Value__c = 25;
        update dcAfterInsert;

        Enrollment__c mainEnrollmentAfter2 = [SELECT Discount_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(500, mainEnrollmentAfter2.Discount_Value__c);

        delete dcAfterInsert;

        Enrollment__c mainEnrollmentAfter3 = [SELECT Discount_Value__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(0, mainEnrollmentAfter3.Discount_Value__c);
        Test.stopTest();
    }

    @isTest static void testRewriteStatusOnParticipantEnrollment() {
        Map<Integer, sObject> dataMap = prepareData();

        Test.startTest();
        Enrollment__c enrToUpd = New Enrollment__c(Id = dataMap.get(2).Id);
        enrToUpd.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
        update enrToUpd;
        Test.stopTest();

        Enrollment__c prEnrollment = [SELECT Status__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT, prEnrollment.Status__c);
    }

    @isTest static void testRewriteStatusOnMainEnrollment() {
        Map<Integer, sObject> dataMap = prepareData();

        Test.startTest();
        Enrollment__c enrToUpd = New Enrollment__c(Id = dataMap.get(3).Id);
        enrToUpd.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
        update enrToUpd;
        Test.stopTest();

        Enrollment__c mainEnr = [SELECT Status__c FROM Enrollment__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT, mainEnr.Status__c);
    }

    @isTest static void testPotentialCandidatesOnTraining() {
        Map<Integer, sObject> dataMap = prepareData();

        Enrollment__c participantEnr = [SELECT Status__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];
        participantEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED;
        update participantEnr;

        Test.startTest();
        Offer__c schedule = [SELECT Potential_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(1, schedule.Potential_Candidates__c);

        Enrollment__c participantEnr2 = [SELECT Status__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];
        delete participantEnr2;
        Test.stopTest();

        Offer__c scheduleAfter = [SELECT Potential_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(0, scheduleAfter.Potential_Candidates__c);
    }

}