global class DocGen_GenerateOfferTradeName implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateOfferTradeName() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateOfferTradeName.generateOfferTradeName(objectId);  
    }

    global static String generateOfferTradeName(String objectId){    
        Enrollment__c studyEnr = [
            SELECT Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.Trade_Name__c, Language_of_Main_Enrollment__c,
                   Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Specialty_Trade_Name__c,
                   Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Course_Offer_From_Specialty__r.Trade_Name__c,
                   Enrollment_From_Documents__r.Specialization__r.Trade_Name__c,
                   Initial_Specialty_Declaration__r.Trade_Name__c, Enrollment_From_Documents__r.Initial_Specialty_Declaration__r.Trade_Name__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(studyEnr.Language_of_Main_Enrollment__c);
        String specialty = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_specialty');
        String postGraduateProduct = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'msg_relatedPostGraduateProduct');
        
        String result = '';
        if (studyEnr.Initial_Specialty_Declaration__r.Trade_Name__c != null 
            || studyEnr.Enrollment_From_Documents__r.Initial_Specialty_Declaration__r.Trade_Name__c != null) {

            if (studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.Course_Offer_From_Specialty__r.Trade_Name__c != null) {
                result = studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.Course_Offer_From_Specialty__r.Trade_Name__c;
            }
            else {
                result = studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.Trade_Name__c;
            }
        }
        else {
            result =  studyEnr.Enrollment_From_documents__r.Course_Or_Specialty_Offer__r.Trade_Name__c;
            if (studyEnr.Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Specialty_Trade_Name__c != null) {
                result += ', ' + specialty + ': ' + studyEnr.Enrollment_From_Documents__r.Course_Or_Specialty_Offer__r.Specialty_Trade_Name__c;
            }
            if (studyEnr.Enrollment_From_Documents__r.Specialization__r.Trade_Name__c != null) {
                result += ', ' + postGraduateProduct + ': ' + studyEnr.Enrollment_From_Documents__r.Specialization__r.Trade_Name__c;
            }
        }
        
        return result;
    }
}