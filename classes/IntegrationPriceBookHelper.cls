/**
*   @author         Karolina Bolinger
*   @description    Class for creating pricebooks
**/

public with sharing class IntegrationPriceBookHelper {

    public static String preparePriceBooksToSend() {
        /* PREPARE QUERIES */
        String result = ('Query start ' + System.now());
        List<Offer__c> offers = prepareOfferList();
        
        
        /* PREPARE JSON */
        result += ('Prepare JSON start ' + System.now());
        List<PriceBook_JSON> offerJSONList = preparePriceBookJSONList(offers);
        result += ('Prepare JSON stop ' + System.now());

        /* SERIALIZE JSON */
        result += ('Serialize start ' + System.now());
        String jsonSerialized = JSON.serialize(offerJSONList);
        result += ('Serialize stop ' + System.now());

        System.debug('result ' + result);
        return jsonSerialized;
    }
    
    public static  PriceBook_JSON buildPriceBookJSON (Offer__c offerList) {
    	 PriceBook_JSON priceBook = new PriceBook_JSON();
    	 priceBook.product_name = offerList.Name;
    	 priceBook.trade_name = offerList.Trade_Name__c;
    	 priceBook.university_name = offerList.University_Name__c;
    	 priceBook.kind = offerList.Kind__c;
    	 priceBook.mode = offerList.Mode__c;
    	 priceBook.efs = offerList.EFS__c;
    	 priceBook.number_of_semesters = offerList.Number_of_Semesters__c;
    	 
    	 return priceBook;
    }
    
    public static List<PriceBook_JSON> preparePriceBookJSONList (List<Offer__c> offers) {
    	List<PriceBook_JSON> offerJSONList = new List<PriceBook_JSON>();
    	
    	for (Offer__c offer : offers) { 
    		offerJSONList.add(IntegrationPriceBookHelper.buildPriceBookJSON(offer));
    	}
    	
    	return offerJSONList;
    }
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- QUERY METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static List<Offer__c> prepareOfferList() {        
        return [SELECT Id, Name, Trade_Name__c, University_Name__c, Kind__c, Mode__c, EFS__c, Number_of_Semesters__c
                FROM Offer__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
        ];
    }
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class PriceBook_JSON {
        public String product_name; 
        public String trade_name; 
        public String university_name;
        public String kind;
        public String mode;
        public Boolean efs;
        public Decimal number_of_semesters;
        //public Integer installment_variants_count;
        //public Boolean is_graded_tuition;
        //public String pricebook_type;
   
    }
    
    //public class
    
}