/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send emails with Extranet Information
**/

/**
*   To initiate the EnrollmentDocumentDeliveryExtranetEmail execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('EnrollmentDocumentDeliveryExtranetEmail', '0 0 8 * * ? *', new EnrollmentDocumentDeliveryExtranetEmail());
**/

global class EnrollmentDocumentDeliveryExtranetEmail implements Schedulable {
	
    global void execute(SchedulableContext sc) {
        DocDeliveryExtranetBatch extranetEmailBatch = new DocDeliveryExtranetBatch(1);
        Database.executebatch(extranetEmailBatch, 2);
    }
}