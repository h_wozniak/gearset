/**
* @description  This class is a Trigger Handler for Target object.
**/

public without sharing class TH_Target extends TriggerHandler.DelegateBase {

    List<Target__c> subtargetsToValidateTargetRealization;
    List<Target__c> subtargetsToValidateDateOverlapping;
    List<Target__c> teamTargetsToValidateTeam;

    public override void prepareBefore() {
        subtargetsToValidateTargetRealization = new List<Target__c>();
        subtargetsToValidateDateOverlapping = new List<Target__c>();
        teamTargetsToValidateTeam = new List<Target__c>();
    }

    public override void beforeInsert(List<sObject> o) {
        List<Target__c> newTargetList = (List<Target__c>) o;
        for (Target__c newTarget : newTargetList) {

            /* Wojciech SŁodziak */
            // check if total Realization Target on Subtargets exceeds Main Target's Realization Target
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_SUBTARGET)) {
                subtargetsToValidateTargetRealization.add(newTarget);
            }

            /* Wojciech SŁodziak */
            // check Subtarget dates overlapp with exisiting Subtargets
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_SUBTARGET)) {
                subtargetsToValidateDateOverlapping.add(newTarget);
            }

            /* Wojciech SŁodziak */
            // check if Team Target for specific Team already Exists
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TEAM_TARGET)) {
                teamTargetsToValidateTeam.add(newTarget);
            }

        }
    }
    
    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Target__c> oldTargets = (Map<Id, Target__c>)old;
        Map<Id, Target__c> newTargets = (Map<Id, Target__c>)o;
        for (Id key : oldTargets.keySet()) {
            Target__c oldTarget = oldTargets.get(key);
            Target__c newTarget = newTargets.get(key);
        
            /* Wojciech SŁodziak */
            // check if total Realization Target on Subtargets exceeds Main Target's Realization Target
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_SUBTARGET)
                && newTarget.Target_Realization__c != oldTarget.Target_Realization__c) {
                subtargetsToValidateTargetRealization.add(newTarget);
            }

            /* Wojciech SŁodziak */
            // check Subtarget dates overlapp with exisiting Subtargets
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_SUBTARGET)
                && (
                    newTarget.Applies_from__c != oldTarget.Applies_from__c
                    || newTarget.Applies_to__c != oldTarget.Applies_to__c
                )
            ) {
                subtargetsToValidateDateOverlapping.add(newTarget);
            }

            /* Wojciech SŁodziak */
            // check if Team Target for specific Team already Exists
            if (newTarget.RecordTypeId == CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TEAM_TARGET)
                && newTarget.Team__c != oldTarget.Team__c) {
                teamTargetsToValidateTeam.add(newTarget);
            }

        }
    }

    public override void finish() {

        /* Wojciech SŁodziak */
        // check if total Realization Target on Subtargets exceeds Main Target's Realization Target
        if (subtargetsToValidateTargetRealization != null && !subtargetsToValidateTargetRealization.isEmpty()) {
            TargetManager.checkIfTargetContainsSubtargets(subtargetsToValidateTargetRealization);
        }

        /* Wojciech SŁodziak */
        // check Subtarget dates overlapp with exisiting Subtargets
        if (subtargetsToValidateDateOverlapping != null && !subtargetsToValidateDateOverlapping.isEmpty()) {
            TargetManager.checkIfSubtargetDatesOverlap(subtargetsToValidateDateOverlapping);
        }

        /* Wojciech SŁodziak */
        // check if Team Target for specific Team already Exists
        if (teamTargetsToValidateTeam != null && !teamTargetsToValidateTeam.isEmpty()) {
            TargetManager.checkIfTeamTargetAlreadyExists(teamTargetsToValidateTeam);
        }

    }

}