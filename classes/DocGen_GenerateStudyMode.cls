global class DocGen_GenerateStudyMode  implements enxoodocgen.DocGen_StringTokenInterface {

	public DocGen_GenerateStudyMode() {}

	global static String executeMethod(String objectId, String templateId, String methodName) {
		return DocGen_GenerateStudyMode.generateStudyMode(objectId);  
	}
    
    global static String generateStudyMode(String objectId) {
        Enrollment__c enr = [
            SELECT Mode__c, Enrollment_from_Documents__r.Mode__c, Language_of_Main_Enrollment__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id = :objectId
        ];

        if (enr.Mode__c != null) {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Enrollment__c.Mode__c);

        	return translatedField.get(enr.Mode__c);
        }
        else {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Enrollment__c.Mode__c);

        	return translatedField.get(enr.Enrollment_from_Documents__r.Mode__c);
        }
    }
}