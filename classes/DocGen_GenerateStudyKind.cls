global class DocGen_GenerateStudyKind  implements enxoodocgen.DocGen_StringTokenInterface {

	public DocGen_GenerateStudyKind() {}

	global static String executeMethod(String objectId, String templateId, String methodName) {
		return DocGen_GenerateStudyKind.generateStudyKind(objectId);  
	}
    
    global static String generateStudyKind(String objectId) {
        Enrollment__c enr = [
            SELECT Kind__c, Enrollment_from_Documents__r.Kind__c, Language_of_Enrollment__c, Language_of_Main_Enrollment__c,
                Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Kind__c,
                Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Kind__c
            FROM Enrollment__c 
            WHERE Id = :objectId
        ];

        if (enr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Kind__c != null) {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Offer__c.Kind__c);

        	return translatedField.get(enr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Kind__c);
        }
        else {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Offer__c.Kind__c);

        	return translatedField.get(enr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Kind__c);
        }
    }
}