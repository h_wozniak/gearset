public with sharing class LEX_Generate_DocumentController {

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		String retString = 'NO_EDIT_ACCESS';

		Set<String> forbiddenStatuses = new Set<String> {
			CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
			CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
			CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
			CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS,
			CommonUtility.ENROLLMENT_STATUS_RESIGNATION
		};
		Set<String> forbiddenStatusesForPermissionSet = new Set<String> {
			CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
			CommonUtility.ENROLLMENT_STATUS_RESIGNATION
		};
		Set<String> forbiddenUnenrStatuses = new Set<String> {
			CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
			CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
			CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS
		};

		if (LEXServiceUtilities.hasEditAccess(recordId)) {
			Boolean permissionSetAccess =  LEXStudyUtilities.checkIfCanEditUneditableEnrollment();
			Enrollment__c enr = [SELECT Id, Status__c FROM Enrollment__c WHERE Id = :recordId];
			if(!forbiddenStatuses.contains(enr.Status__c) || (permissionSetAccess && !forbiddenStatusesForPermissionSet.contains(enr.Status__c))) {
				retString = 'SUCCESS';
			}
		}
		return retString;
	}

	@AuraEnabled
	public static String generateAgreementDocumentForStudyEnrollment(String recordId) {
		return LEXStudyUtilities.generateAgreementDocumentForStudyEnrollment(recordId) ? 'SUCCESS' : 'FAILURE';
	}
}