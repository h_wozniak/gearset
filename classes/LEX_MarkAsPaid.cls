/**
 * Created by Sebastian on 23.01.2018.
 */

public with sharing class LEX_MarkAsPaid {

    @AuraEnabled
    public static String markEnrollmentsAsPaid(String objId, String sObjectType) {
        if (sObjectType == 'Offer__c') {
            List<Enrollment__c> relatedMainEnrollments = [
                    SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c
                    WHERE Training_Offer__c = :objId
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
            ];

            if (!relatedMainEnrollments.isEmpty()) {
                for (Enrollment__c rme : relatedMainEnrollments) {
                    if (rme.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                        rme.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                    }
                    else {
                        rme.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                    }
                }
                try {
                    update relatedMainEnrollments;
                    return 'true';
                } catch(Exception e) {
                    ErrorLogger.log(e);
                    return String.valueOf(e);
                }
            }
            else {
                return 'no_main_enrollment';
            }
        }
        else {
            Enrollment__c mainEnrollment = [
                    SELECT Id, Status__c, Last_Active_status__c
                    FROM Enrollment__c
                    WHERE Id = :objId LIMIT 1
            ];

            if (mainEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                mainEnrollment.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
            }
            else {
                mainEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
            }

            try {
                update mainEnrollment;
                return 'true';
            } catch(Exception e) {
                ErrorLogger.log(e);
                return String.valueOf(e);
            }
        }
    }

    @AuraEnabled
    public static String markParticipantsAsPaid(List<Id> idArray) {
        List<Id> mainEnrollmentIds = new List<Id>();
        List<Id> trainingSchedulesIds = new List<Id>();

        List<Enrollment__c> relatedParticipants = [
                SELECT Id, Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c
                FROM Enrollment__c
                WHERE Id in :idArray
        ];

        List<Enrollment__c> relatedParticipantsToUpdate = new List<Enrollment__c>();

        for (Enrollment__c participant : relatedParticipants) {
            mainEnrollmentIds.add(participant.Enrollment_Training_Participant__c);
            trainingSchedulesIds.add(participant.Training_Offer_for_Participant__c);
        }

        List<Enrollment__c> mainEnrollmentList = [
                SELECT Id, Status__c, RecordTypeId
                FROM Enrollment__c
                WHERE Id IN :mainEnrollmentIds
        ];

        List<Offer__c> trainingSchedulesList = [
                SELECT Id, Launched__c
                FROM Offer__c
                WHERE Id IN : trainingSchedulesIds
        ];

        for (Enrollment__c participant : relatedParticipants) {
            for (Offer__c trainingSchedule : trainingSchedulesList) {
                if (participant.Training_Offer_for_Participant__c == trainingSchedule.Id) {
                    for (Enrollment__c mainEnrollment : mainEnrollmentList) {
                        if (participant.Enrollment_Training_Participant__c == mainEnrollment.Id) {
                            if (participant.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                                if (mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {

                                    if (participant.Status__c != CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST) {
                                        if (trainingSchedule.Launched__c) {
                                            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                                participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                            }
                                            else{
                                                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED;
                                            }
                                        } else {
                                            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                                participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                            }
                                            else {
                                                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP;
                                            }
                                        }
                                    }

                                } else if (mainEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)) {
                                    if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                                        participant.Last_Active_status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                    }
                                    else {
                                        participant.Status__c = CommonUtility.ENROLLMENT_STATUS_PAID;
                                    }
                                }

                                relatedParticipantsToUpdate.add(participant);
                            }
                        }
                    }
                }
            }
        }

        try {
            if (!relatedParticipantsToUpdate.isEmpty()) {
                update relatedParticipantsToUpdate;
            }
            return 'true';
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return String.valueOf(ex);
        }
    }

    @AuraEnabled
    public static MarkAsPaidWrapper retrieveParticipantsData(Id recordId) {
        List<Offer__c> potentialOffer = [
                SELECT Id, Training_Offer_from_Schedule__r.Type__c
                FROM Offer__c
                WHERE Id = :recordId
        ];

        if (potentialOffer.isEmpty()) {
            List<Enrollment__c> potentialEnrollment = [
                    SELECT Id, Training_Offer__r.Training_Offer_from_Schedule__r.Type__c
                    FROM Enrollment__c
                    WHERE Id = :recordId
            ];

            if (!potentialEnrollment.isEmpty()) {
                return new MarkAsPaidWrapper(potentialEnrollment.get(0).Training_Offer__r.Training_Offer_from_Schedule__r.Type__c, 'Enrollment__c');
            }
        } else {
            return new MarkAsPaidWrapper(potentialOffer.get(0).Training_Offer_from_Schedule__r.Type__c, 'Offer__c');
        }

        return null;
    }

    @AuraEnabled
    public static Boolean hasEditAccess(List<Id> recordIds) {
        Set<Id> recordIdsSet = new Set<Id>();
        recordIdsSet.addAll(recordIds);

        List<UserRecordAccess> uraList = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId IN :recordIds
        ];

        for (UserRecordAccess ura : uraList) {
            if (!ura.HasEditAccess) {
                return false;
            }
        }

        return true;
    }

    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class MarkAsPaidWrapper {
        @AuraEnabled public String trainingType { get; set; }
        @AuraEnabled public String objType { get; set; }

        public MarkAsPaidWrapper(String trainingType, String objType) {
            this.trainingType = trainingType;
            this.objType = objType;
        }
    }

}