public with sharing class LEX_Resignation_for_StudyController {


	private static final Set<String> forbiddenStatuses = new Set<String> {
		CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
		CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS,
		CommonUtility.ENROLLMENT_STATUS_RESIGNATION
	};

	private static final Set<String> forbiddenUnenrStatuses = new Set<String> {
		CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS,
		CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS
	};

	@AuraEnabled
	public static String getAdditionalInfo(String recordId) {
		Enrollment__c enr = [SELECT Previous_Enrollment__c FROM Enrollment__c WHERE Id = :recordId];
		return enr.Previous_Enrollment__c != null ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String checkRecord(String recordId) {

		String retVal = 'FALSE';

		Enrollment__c enr = [SELECT Status__c, Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];

		if ((!forbiddenStatuses.contains(enr.Status__c) && enr.Unenrolled__c == false) ||
		        (!forbiddenUnenrStatuses.contains(enr.Status__c) && enr.Unenrolled__c == true)) {
			retval = 'TRUE';
		}

		return retVal;
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String updateFieldValues(String objId, String sObjectName, String[] fieldNames, String[] fieldTypes, String[] fieldValues) {
		return LEXServiceUtilities.updateFieldValues(objId, sObjectName, fieldNames, fieldTypes, fieldValues) ? 'SUCCESS' : 'FAILURE';
	}

	@AuraEnabled
	public static List<picklistEntryWrapper> getPicklistEntries(String objName, String fieldName) {
		List<picklistEntryWrapper>	 retList = new List<picklistEntryWrapper>();

		Schema.sObjectType sObj = Schema.getGlobalDescribe().get(objName);
		Schema.sObjectField sObjField = sObj.getDescribe().fields.getMap().get(fieldName);
		Schema.DescribeFieldResult fieldResult = sObjField.getDescribe();
		for(Schema.PicklistEntry entry : fieldResult.getPicklistValues()) {
			retList.add(new picklistEntryWrapper(entry.getValue(), entry.getLabel()));
		}
		return retList;
	}

	private Class picklistEntryWrapper {

		@AuraEnabled public String label {get;set;}
		@AuraEnabled public String value {get;set;}
		@AuraEnabled public Boolean disabled {get;set;}
		@AuraEnabled public Boolean escapeItem {get;set;}

		public picklistEntryWrapper(String value, String label, Boolean isDisabled) {
			this.value = value;
			this.label = label;
			this.disabled = isDisabled;
			this.escapeItem = false;
		}
		
		public picklistEntryWrapper(String value, String label) {
			this.value = value;
			this.label = label;
			this.disabled = false;
			this.escapeItem = false;
		}
	}
}