/**
*   @author         Grzegorz Długosz
*   @description    This class is used to make dictionaries available for external external systems.
**/
global with sharing class IntegrationPicklistDictionaryHelper {

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- SERVICE CONFIG ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    
    public static final Map<String, Schema.SObjectField> dictKeyToSObjFieldMap = new Map<String, Schema.SObjectField> {
        'countries' => Contact.Country_of_Origin__c,
        'citizenship' => Contact.Nationality__c,
        'identity_documents' => Contact.Identity_Document__c,
        'gender' => Contact.Gender__c,
        'tuition' => Enrollment__c.Tuition_System__c,
        'language' => Offer__c.Language__c,
        'school_type' => Account.School_Type__c,
        'university_type' => Account.University_Type__c,
        'diploma_status' => Qualification__c.Status__c,
        'a-level_status' => Contact.A_Level__c,
        'language_level' => Enrollment__c.Language_Level__c,
        'residence_documents' => Contact.Study_Authorization_Document__c,
        'phone_type' => Contact.IntegrationPhoneType__c,
        'address_type' => Contact.IntegrationAddressType__c,
        'kind' => Offer__c.Kind__c,
        'degree' => Offer__c.Degree__c,
        'mode' => Offer__c.Mode__c,
        'examination_committee' => Contact.Regional_Examination_Commission__c,
        'position' => Contact.Position__c,
        'areas_of_interest' => Contact.Areas_of_Interest__c,
        'statuses' => Contact.Statuses__c,
        'enrollment_status' => Enrollment__c.Status__c,
        'obtained_title' => Qualification__c.Gained_Title__c
    };

    private static final List<String> listOfLanguageCodes = new List<String> {
        CommonUtility.INTEGRATION_LANGUAGE_CODE_PL,
        CommonUtility.INTEGRATION_LANGUAGE_CODE_EN,
        CommonUtility.INTEGRATION_LANGUAGE_CODE_RU
    };

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- Logic to get Dictionary --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */ 

	public static String getDictionaryPicklist(String dictionaryId) {

        List<Dictionary_JSON> dictionaryList = new List<Dictionary_JSON>();

        for (String language_code : listOfLanguageCodes) {
            Map<String, String> translatedPicklistValues = DictionaryTranslator.getTranslatedPicklistValues(language_code, dictKeyToSObjFieldMap.get(dictionaryId));
            List<DictionaryValues_JSON> dictionaryValues = new List<DictionaryValues_JSON>();

            for (String key : translatedPicklistValues.keySet()) {
                if (key != 'No data' && key != 'Brak danych') {
                    dictionaryValues.add(new DictionaryValues_JSON(removeSpecialCharactersFromString(key), translatedPicklistValues.get(key)));
                }
            }

            dictionaryList.add(new Dictionary_JSON(language_code, dictionaryValues));
        }

        return JSON.serialize(dictionaryList);
    }

    global static String removeSpecialCharactersFromString(String value) {
        if (value != null && value != '') {
            Pattern pt = Pattern.compile('[^a-zA-Z0-9]');
            Matcher match= pt.matcher(value);

            while (match.find()) {
                String s= match.group();
                value = value.replaceAll('\\' + s, '');
            }
        }
        
        return value;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class Dictionary_JSON {
        public String language_code;
        public List<DictionaryValues_JSON> dictionary_values;

        public Dictionary_JSON(String language_code, List<DictionaryValues_JSON> dictionary_values) {
            this.language_code = language_code;
            this.dictionary_values = dictionary_values;
        }
    }

    global class DictionaryValues_JSON {
        public String id;
        public String label;

        public DictionaryValues_JSON(String id, String label) {
            this.id = id;
            this.label = label;
        }
    }

}