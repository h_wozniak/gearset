/**
 * Created by Sebastian on 23.01.2018.
 */

public with sharing class LEX_TrainingAcceptCandidates {

    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }

    @AuraEnabled
    public static Boolean markParticipantsAsAccepted(List<Id> recordIds) {
        List<Enrollment__c> relatedParticipants = [
                SELECT Id, Status__c
                FROM Enrollment__c
                WHERE Id IN :recordIds
        ];

        List<Enrollment__c> participantsToUpdate = new List<Enrollment__c>();
        for (Enrollment__c participant : relatedParticipants) {
            if (participant.Status__c == CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST || participant.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS) {
                participant.Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT;
                participantsToUpdate.add(participant);
            }
        }
        try {
            if (!participantsToUpdate.isEmpty()) {
                update participantsToUpdate;
            }
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }
}