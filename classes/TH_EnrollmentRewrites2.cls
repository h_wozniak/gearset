/**
* @description  This class is a Trigger Handler for Study Enrollments - rewriting field values (for emails/preview)
**/

public without sharing class TH_EnrollmentRewrites2 extends TriggerHandler.DelegateBase {

    List<Enrollment__c> studyEnrToCopyFields;
    List<Enrollment__c> eduAgrToCopyFields;
    List<Enrollment__c> docEnrToCopyFields;
    List<Enrollment__c> eduAgrToRewriteStudentNo;
    List<Enrollment__c> langEnrToCopyFields;
    List<Enrollment__c> studyToRewriteStartDate;
    Set<Id> studyEnrToCopyStatusToEduAgr;
    Set<Id> studyEnrsToUpdateFirstVersionOfDocField;


    public override void prepareBefore() {
        studyEnrToCopyFields = new List<Enrollment__c>();
        eduAgrToCopyFields = new List<Enrollment__c>();
        docEnrToCopyFields = new List<Enrollment__c>();
        langEnrToCopyFields = new List<Enrollment__c>();
        studyToRewriteStartDate = new List<Enrollment__c>();
        studyEnrsToUpdateFirstVersionOfDocField = new Set<Id>();
    }

    public override void prepareAfter() {
        studyEnrToCopyStatusToEduAgr = new Set<Id>();
        eduAgrToRewriteStudentNo = new List<Enrollment__c>();
    }



    public override void beforeInsert(List<sObject> o ) {
        List<Enrollment__c> newEnrollmentList = (List<Enrollment__c>)o;
        for(Enrollment__c newEnrollment : newEnrollmentList) {

            /* Wojciech Słodziak */
            // on Study Enrollment insert set University Name, Degree, Mode, Acceptance Status on Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)) {
                studyEnrToCopyFields.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Educational Agreement insert set University Name, Degree, Mode, Acceptance Status on Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Course_or_Specialty_from_EduAgr__c != null) {
                eduAgrToCopyFields.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Document insert copy multiple fields 
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)) {
                docEnrToCopyFields.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Language insert copy multiple fields 
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE)) {
                langEnrToCopyFields.add(newEnrollment);
            }

            /* Wojciech Słodziak */
            // on Enrollment or Educational Agreement insert set universal lookup to offer
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)) {
                newEnrollment.Course_or_Specialty_Universal__c = newEnrollment.Course_or_Specialty_Offer__c;
            }
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)) {
                newEnrollment.Course_or_Specialty_Universal__c = newEnrollment.Course_or_Specialty_from_EduAgr__c;
            }

            /* Sebastian Łasisz */
            // Set confirmation date on enrollment insert (CRM)
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_CONFIRMED) {
                newEnrollment.Confirmation_Date__c = System.now();
            }

            /* Sebastian Łasisz */
            // On setting up send acceptance email also disable sending automatic emails
            if (newEnrollment.TECH_Send_Acceptance_Email__c == false) {
                //newEnrollment.Dont_send_automatic_emails__c = true;
            }

            /* Sebastian Łasisz */
            // On setting up guided group disable sending automatic emails
            if (newEnrollment.Guided_group__c == true) {
                newEnrollment.Dont_send_automatic_emails__c = true;
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        for(Id key : newEnrollments.keySet()) {
            Enrollment__c newEnrollment = newEnrollments.get(key);

            /* Sebastian Łasisz */
            // on Educational Agreement insert rewrite Student number to Study Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Source_Enrollment__c != null
                && newEnrollment.Student_no__c != null) {
                eduAgrToRewriteStudentNo.add(newEnrollment);
            }      
        }    

    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        for (Id key : oldEnrollments.keySet()) {
            Enrollment__c oldEnrollment = oldEnrollments.get(key);
            Enrollment__c newEnrollment = newEnrollments.get(key);

            /* Wojciech Słodziak */
            // on Educational Agreement Edit set universal lookup to offer
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Course_or_Specialty_from_EduAgr__c != null) {
                newEnrollment.Course_or_Specialty_Universal__c = newEnrollment.Course_or_Specialty_from_EduAgr__c;
                eduAgrToCopyFields.add(newEnrollment);
            }

            /* Sebastian Łasisz */
            // Set Date of Graduation on Enrollment Agreement when Status is set to Finished Education
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Didactics_Status__c != oldEnrollment.Didactics_Status__c
                && newEnrollment.Didactics_Status__c == CommonUtility.ENROLLMENT_DIDACTICS_STATUS_FINISHED_EDUCATION) {
                newEnrollment.Date_of_Graduation__c = System.today();
            }

            /* Sebastian Łasisz */
            // Set Verify Agreement on Enrollment Study when Value after Discount is changed
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Value_after_Discount__c != oldEnrollment.Value_after_Discount__c
                && newEnrollment.Generated_First_Version_of_Agreement__c) {
                newEnrollment.Verify_Agreement__c = true;
            }
            
            /* Karolina Kowalik */
            // Set Enrollment Study 
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)) {
                studyToRewriteStartDate.add(newEnrollment);
            }
            
            /* Sebastian Łasisz */
            // Set verify all documents flag based on verified documents for different types of enrollment
            if (newEnrollment.Unenrolled__c != oldEnrollment.Unenrolled__c
                || newEnrollment.Verify_Documents_Unenrolled__c != oldEnrollment.Verify_Documents_Unenrolled__c
                || newEnrollment.Verify_Documents__c != oldEnrollment.Verify_Documents__c) {
                if (newEnrollment.Unenrolled__c == true) {
                    newEnrollment.Verify_All_Documents__c = newEnrollment.Verify_Documents_Unenrolled__c;
                }
                else {
                    newEnrollment.Verify_All_Documents__c = newEnrollment.Verify_Documents__c;
                }
            }

            /* Sebastian Łasisz */
            // on Enrollment Document Agreement update first version of agreement on enrollment study
            if (newEnrollment.Document__c == CatalogManager.getDocumentAgreementId()
                && newEnrollment.Current_File__c != oldEnrollment.Current_File__c
                && newEnrollment.Current_File__c != CommonUtility.DURING_GENERATION) {
                studyEnrsToUpdateFirstVersionOfDocField.add(newEnrollment.Enrollment_from_Documents__c);
                //newEnrollment.Generated_First_Version_of_Agreement__c = true;
            }

            /* Sebastian Łasisz */
            // on Course or Specialty Offer Change update Universal lookup
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Course_or_Specialty_Offer__c != oldEnrollment.Course_or_Specialty_Offer__c) {
                newEnrollment.Course_or_Specialty_Universal__c = newEnrollment.Course_or_Specialty_Offer__c;
            }

            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Didactics_Status__c != oldEnrollment.Didactics_Status__c){
                newEnrollment.Date_of_Last_Status_Change__c = System.now();
            }

            /* Sebastian Łasisz */
            // On setting up send acceptance email also disable sending automatic emails
            if (newEnrollment.TECH_Send_Acceptance_Email__c != oldEnrollment.TECH_Send_Acceptance_Email__c 
                && newEnrollment.TECH_Send_Acceptance_Email__c == false) {
                //newEnrollment.Dont_send_automatic_emails__c = true;
            }

            /* Sebastian Łasisz */
            // On setting up guided group disable sending automatic emails
            if (newEnrollment.Guided_group__c != oldEnrollment.Guided_group__c && newEnrollment.Guided_group__c == true) {
                newEnrollment.Dont_send_automatic_emails__c = true;
            }
        }
    }
 
    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        for (Id key : oldEnrollments.keySet()) {
            Enrollment__c oldEnrollment = oldEnrollments.get(key);
            Enrollment__c newEnrollment = newEnrollments.get(key);

            /* Wojciech Słodziak */
            // on StudyEnr Status update rewrite it to Educational Agreement
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Status__c != oldEnrollment.Status__c) {
                studyEnrToCopyStatusToEduAgr.add(newEnrollment.Id);
            }

            /* Sebastian Łasisz */
            // on Educational Agreement update rewrite Student number to Study Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
                && newEnrollment.Source_Enrollment__c != null
                && newEnrollment.Student_no__c != oldEnrollment.Student_no__c) {
                eduAgrToRewriteStudentNo.add(newEnrollment);
            }            
        }
    }

    //public override void beforeDelete(Map<Id, sObject> old) {}

    //public override void afterDelete(Map<Id, sObject> old) {}

    public override void finish() {    
        
        /* Wojciech Słodziak */
        // on Study Enrollment insert set University Name, Degree and Mode on Enrollment
        if (studyEnrToCopyFields != null && !studyEnrToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            for (Enrollment__c studyEnr : studyEnrToCopyFields) {
                offerIds.add(studyEnr.Course_or_Specialty_Offer__c);
                contactIds.add(studyEnr.Candidate_Student__c);
            }

            Map<Id, Offer__c> offerMap = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c, Acceptation_Status__c, 
                University_Study_Offer_from_Course__r.Degree__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Degree__c, 
                Mode__c, Course_Offer_from_Specialty__r.Mode__c, Kind__c, Course_Offer_from_Specialty__r.Kind__c, Trade_Name__c, Specialty_Trade_Name__c,
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.University__r.University_City_in_Locative_with_Prefix__c, 
                University_Study_Offer_from_Course__r.University__r.University_City_in_Locative_with_Prefix__c, 
                University_Study_Offer_from_Course__r.University__r.Full_Name__c, 
                University_Study_Offer_from_Course__r.Study_Start_Date__c,
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, EFS__c,
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.University__r.Full_Name__c, 
                University_Study_Offer_from_Course__r.Start_Date_Semester_2__c, University_Study_Offer_from_Course__r.Start_Date_Semester_3__c, University_Study_Offer_from_Course__r.Start_Date_Semester_4__c, 
                University_Study_Offer_from_Course__r.Start_Date_Semester_5__c, University_Study_Offer_from_Course__r.Start_Date_Semester_6__c, University_Study_Offer_from_Course__r.Start_Date_Semester_7__c, 
                University_Study_Offer_from_Course__r.Start_Date_Semester_8__c, University_Study_Offer_from_Course__r.Start_Date_Semester_9__c, University_Study_Offer_from_Course__r.Start_Date_Semester_10__c,
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_2__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_3__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_4__c, 
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_5__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_6__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_7__c, 
                Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_8__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_9__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_10__c 
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            Map<Id, Contact> contactMap = new Map<Id, Contact>([
                SELECT Id, LastName
                FROM Contact
                WHERE Id IN :contactIds
            ]);

            for (Enrollment__c enr : studyEnrToCopyFields) {
                Offer__c offer = offerMap.get(enr.Course_or_Specialty_Offer__c);
                Contact contact = contactMap.get(enr.Candidate_Student__c);

                enr.Degree__c = (offer.University_Study_Offer_from_Course__r.Degree__c != null? offer.University_Study_Offer_from_Course__r.Degree__c
                                 : offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Degree__c);
                enr.University_City_in_Locative_with_Prefix__c = 
                    (offer.University_Study_Offer_from_Course__r.University__r.University_City_in_Locative_with_Prefix__c != null? 
                    offer.University_Study_Offer_from_Course__r.University__r.University_City_in_Locative_with_Prefix__c
                    : offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.University__r.University_City_in_Locative_with_Prefix__c);

                enr.University_Full_Name__c = (offer.University_Study_Offer_from_Course__r.University__r.Full_Name__c != null? 
                    offer.University_Study_Offer_from_Course__r.University__r.Full_Name__c
                    : offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.University__r.Full_Name__c);

                enr.Mode__c = (offer.Mode__c != null? offer.Mode__c : offer.Course_Offer_from_Specialty__r.Mode__c);
                enr.Kind__c = (offer.Kind__c != null? offer.Kind__c : offer.Course_Offer_from_Specialty__r.Kind__c);
                enr.University_Name__c = offer.University_Name__c;
                enr.Acceptation_Status__c = offer.Acceptation_Status__c;
                enr.EFS__c = offer.EFS__c;
                enr.Trade_Name__c = (offer.Specialty_Trade_Name__c != null? offer.Specialty_Trade_Name__c : offer.Trade_Name__c);
                enr.Candidate_Last_Name__c = (contact != null && contact.LastName != null ? contact.LastName : '');
                
                
                /* Karolina Bolinger */
                
                if (enr.Starting_Semester__c != null && CommonUtility.semesterNumberToStartSemesterDate().containsKey(enr.Starting_Semester__c)) { 
	                if (offer.University_Study_Offer_from_Course__c != null &&
	                offer.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c)) != null) {
	                    enr.Study_Start_Date__c = Date.valueof(offer.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c))); 
	                }
	                else if (offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c != null &&
	                offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c)) != null) {
	                	enr.Study_Start_Date__c = Date.valueof(offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c))); 
	                }
	                else if (offer.University_Study_Offer_from_Course__c != null || offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c != null) {
	                    enr.Starting_Semester__c.addError(' ' + Label.msg_error_FillStartDateOfTheSemester);
	                }
                }
            }
        }

        /* Wojciech Słodziak */
        // on Educational Agreement insert set University Name, Degree and Mode on Enrollment
        if (eduAgrToCopyFields != null && !eduAgrToCopyFields.isEmpty()) {
            Set<Id> offerIds = new Set<Id>();
            for (Enrollment__c studyEnr : eduAgrToCopyFields) {
                offerIds.add(studyEnr.Course_or_Specialty_from_EduAgr__c);
            }

            Map<Id, Offer__c> offerMap = new Map<Id, Offer__c>([
                SELECT Id, University_Name__c, Acceptation_Status__c, 
                University_Study_Offer_from_Course__r.Degree__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Degree__c, 
                Mode__c, Course_Offer_from_Specialty__r.Mode__c, Kind__c, Course_Offer_from_Specialty__r.Kind__c, Trade_Name__c, Specialty_Trade_Name__c,
                Number_of_Semesters__c, Course_Offer_from_Specialty__r.Number_of_Semesters__c
                FROM Offer__c 
                WHERE Id IN :offerIds
            ]);

            for (Enrollment__c enr : eduAgrToCopyFields) {
                Offer__c offer = offerMap.get(enr.Course_or_Specialty_from_EduAgr__c);

                enr.Degree__c = (offer.University_Study_Offer_from_Course__r.Degree__c != null? offer.University_Study_Offer_from_Course__r.Degree__c
                                 : offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Degree__c);
                enr.Mode__c = (offer.Mode__c != null? offer.Mode__c : offer.Course_Offer_from_Specialty__r.Mode__c);
                enr.Kind__c = (offer.Kind__c != null? offer.Kind__c : offer.Course_Offer_from_Specialty__r.Kind__c);
                enr.University_Name__c = offer.University_Name__c;
                enr.Acceptation_Status__c = offer.Acceptation_Status__c;
                enr.Number_of_Semesters_EducationalArg__c = (offer.Number_of_Semesters__c != null ? offer.Number_of_Semesters__c : offer.Course_Offer_from_Specialty__r.Number_of_Semesters__c);
                enr.Trade_Name__c = (offer.Specialty_Trade_Name__c != null? offer.Specialty_Trade_Name__c : offer.Trade_Name__c);
            }
        }

        /* Wojciech Słodziak */
        // on Document insert copy multiple fields 
        /* TO REMOVE IF OTHER FUNCTIONALITY IS WORKING PROPERLY */
        Set<Id> studyEnrIds = new Set<Id>();
        if (docEnrToCopyFields != null && !docEnrToCopyFields.isEmpty()) {
            for (Enrollment__c docEnr : docEnrToCopyFields) {
                studyEnrIds.add(docEnr.Enrollment_from_Documents__c);
            }
        }

        if (langEnrToCopyFields != null && !langEnrToCopyFields.isEmpty()) {
            for (Enrollment__c langENr : langEnrToCopyFields) {
                studyEnrIds.add(langENr.Enrollment_from_Language__c);
            }
        }

        Map<Id, Enrollment__c> studyEnrMap = new Map<Id, Enrollment__c>();
        if (!studyEnrIds.isEmpty()) {
            studyEnrMap = new Map<Id, Enrollment__c>([
                SELECT Id, University_Name__c, OwnerId 
                FROM Enrollment__c 
                WHERE Id IN :studyEnrIds
            ]);
        }

        if (docEnrToCopyFields != null && !docEnrToCopyFields.isEmpty()) {          
            for (Enrollment__c docEnr : docEnrToCopyFields) {
                Enrollment__c studyEnr = studyEnrMap.get(docEnr.Enrollment_from_Documents__c);

                docEnr.University_Name__c = studyEnr.University_Name__c;
                docEnr.OwnerId = studyEnr.OwnerId;
            }
        }

        /* Wojciech Słodziak */
        // on Language insert copy multiple fields 
        if (langEnrToCopyFields != null && !langEnrToCopyFields.isEmpty()) {
            for (Enrollment__c langENr : langEnrToCopyFields) {
                Enrollment__c studyEnr = studyEnrMap.get(langENr.Enrollment_from_Language__c);

                langENr.University_Name__c = studyEnr.University_Name__c;
                langENr.OwnerId = studyEnr.OwnerId;
            }
        }

        /* Wojciech Słodziak */
        // on StudyEnr Status update rewrite it to Educational Agreement
        if (studyEnrToCopyStatusToEduAgr != null && !studyEnrToCopyStatusToEduAgr.isEmpty()) {
            List<Enrollment__c> eduAgrList = [
                SELECT Id, Status__c, Source_Enrollment__r.Status__c
                FROM Enrollment__c
                WHERE Source_Enrollment__c IN :studyEnrToCopyStatusToEduAgr
            ];

            for (Enrollment__c eduAgr : eduAgrList) {
                eduAgr.Status__c = eduAgr.Source_Enrollment__r.Status__c;
            }

            try {
                update eduAgrList;
            } catch (Exception e) {
                ErrorLogger.log(e);
            }
        }

        /* Sebastian Łasisz */
        // on Enrollment Document Agreement update first version of agreement on enrollment study
        if (studyEnrsToUpdateFirstVersionOfDocField != null && !studyEnrsToUpdateFirstVersionOfDocField.isEmpty()) {
            List<Enrollment__c> studyEnrs = new List<Enrollment__c>();
            
            for (Id studyEnrId : studyEnrsToUpdateFirstVersionOfDocField) {
                studyEnrs.add(new Enrollment__c(
                    Id = studyEnrId,
                    Generated_First_Version_of_Agreement__c = true 
                ));
            }

            try {
                update studyEnrs;
            }
            catch (Exception e) {
                ErrorLogger.log(e);
            }
        }

        /* Sebastian Łasisz */
        // on Educational Agreement update/insert rewrite Student number to Study Enrollment
        if (eduAgrToRewriteStudentNo != null && !eduAgrToRewriteStudentNo.isEmpty()) {
            List<Enrollment__c> enrollmentsToUpdate = new List<Enrollment__c>();

            for (Enrollment__c eduAgr : eduAgrToRewriteStudentNo) {
                Enrollment__c studyEnr = new Enrollment__c(
                    Id = eduAgr.Source_Enrollment__c,
                    Student_no__c = eduAgr.Student_no__c
                );

                enrollmentsToUpdate.add(studyEnr);
            }

            try {
                update enrollmentsToUpdate;
            }
            catch (Exception e) {
                ErrorLogger.log(e);
            }
        }
        
        /* Karolina Kowalik */
        // update Study Start Date on Enrollment based on semester number
        if (studyToRewriteStartDate != null && !studyToRewriteStartDate.isEmpty()) {
            
            Set<Id> offerIds = new Set<Id>();
            for (Enrollment__c studyEnr : studyToRewriteStartDate) {
                offerIds.add(String.valueOf(studyEnr.Course_or_Specialty_Offer__c));
            }
            
            List<Enrollment__c> enrollmentsToUpdateBasedOnSemester = new List<Enrollment__c>();
            Map<Id, Offer__c> offerMap = new Map<Id, Offer__c>([
                    SELECT University_Study_Offer_from_Course__r.Start_Date_Semester_2__c, Id, University_Study_Offer_from_Course__r.Start_Date_Semester_3__c, 
                    University_Study_Offer_from_Course__r.Start_Date_Semester_4__c, University_Study_Offer_from_Course__r.Start_Date_Semester_5__c, University_Study_Offer_from_Course__r.Start_Date_Semester_6__c, 
                    University_Study_Offer_from_Course__r.Start_Date_Semester_7__c, University_Study_Offer_from_Course__r.Start_Date_Semester_8__c, University_Study_Offer_from_Course__r.Start_Date_Semester_9__c, 
                    University_Study_Offer_from_Course__r.Start_Date_Semester_10__c, 
                    University_Study_Offer_from_Course__r.Study_Start_Date__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
                    Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_2__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_3__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_4__c, 
                	Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_5__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_6__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_7__c, 
                	Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_8__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_9__c, Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Start_Date_Semester_10__c 
                    FROM Offer__c 
                    WHERE Id IN :offerIds
                    ]);
             
           for (Enrollment__c enr : studyToRewriteStartDate) {
                Offer__c offer = offerMap.get(enr.Course_or_Specialty_Offer__c);
                if (enr.Starting_Semester__c != null && CommonUtility.semesterNumberToStartSemesterDate().containsKey(enr.Starting_Semester__c)) { 
	                if (offer.University_Study_Offer_from_Course__c != null &&
	                offer.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c)) != null) {
	                    enr.Study_Start_Date__c = Date.valueof(offer.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c))); 
	                }
	                else if (offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c != null &&
	                offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c)) != null) {
	                	enr.Study_Start_Date__c = Date.valueof(offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.get(CommonUtility.semesterNumberToStartSemesterDate().get(enr.Starting_Semester__c))); 
	                }
	                else if (offer.University_Study_Offer_from_Course__c != null || offer.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c != null) {
	                    enr.Starting_Semester__c.addError(' ' + Label.msg_error_FillStartDateOfTheSemester);
	                }
                }
                enrollmentsToUpdateBasedOnSemester.add(enr);
            }
        }
    }
}