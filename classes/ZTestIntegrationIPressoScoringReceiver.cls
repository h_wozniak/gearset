@isTest
private class ZTestIntegrationIPressoScoringReceiver {

	@isTest static void test_receiverScoringRequest_list_success() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Marketing_Campaign__c iPressoContacts = ZDataTestUtility.createIPressoContact(null, true);

        IntegrationIPressoScoringReceiver.AckResponse_JSON resp = new IntegrationIPressoScoringReceiver.AckResponse_JSON();

        resp.ipresso_contact_id = iPressoContacts.IPressoId__c;
        resp.scoring = '400';

        String jsonBody = JSON.serialize(new List<IntegrationIPressoScoringReceiver.AckResponse_JSON> { resp });
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoScoringReceiver.receiverScoringRequest();
        Test.stopTest();

        Marketing_Campaign__c iPressoContactAfter = [
        	SELECT Id, Scoring__c
        	FROM Marketing_Campaign__c
        	WHERE Id = :iPressoContacts.Id
        ];

        System.assertEquals(iPressoContactAfter.Scoring__c, '400');
	}

	@isTest static void test_receiverScoringRequest_1Contact() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Marketing_Campaign__c iPressoContacts = ZDataTestUtility.createIPressoContact(null, true);

        IntegrationIPressoScoringReceiver.AckResponse_JSON resp = new IntegrationIPressoScoringReceiver.AckResponse_JSON();

        resp.ipresso_contact_id = iPressoContacts.IPressoId__c;
        resp.scoring = '400';

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoScoringReceiver.receiverScoringRequest();
        Test.stopTest();

        Marketing_Campaign__c iPressoContactAfter = [
        	SELECT Id, Scoring__c
        	FROM Marketing_Campaign__c
        	WHERE Id = :iPressoContacts.Id
        ];

        System.assertNotEquals(iPressoContactAfter.Scoring__c, '400');
	}

	@isTest static void test_receiverScoringRequest_1Contact_fail() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        IntegrationIPressoScoringReceiver.AckResponse_JSON resp = new IntegrationIPressoScoringReceiver.AckResponse_JSON();

        resp.ipresso_contact_id = null;
        resp.scoring = '400';

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoScoringReceiver.receiverScoringRequest();
        Test.stopTest();
	}

	@isTest static void test_receiverScoringRequest_wrongId() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        IntegrationIPressoScoringReceiver.AckResponse_JSON resp = new IntegrationIPressoScoringReceiver.AckResponse_JSON();

        resp.ipresso_contact_id = '12312312312';
        resp.scoring = '400';

        String jsonBody = JSON.serialize(new List<IntegrationIPressoScoringReceiver.AckResponse_JSON> { resp });
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationIPressoScoringReceiver.receiverScoringRequest();
        Test.stopTest();
	}
}