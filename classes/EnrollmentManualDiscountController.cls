public with sharing class EnrollmentManualDiscountController {

    public Discount__c newDiscount { get; set; }
    
    public String enrollmentFromDiscountId { get; set; }
    public Enrollment__c enrollmentFromDiscount { get; set; }
    public String selectedVIS { get; set; }

    public Boolean error { get; set; }

    public static String TYPE_SECONDCOURSE { get { return CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE; } }
    public static String TYPE_VIS { get { return CommonUtility.DISCOUNT_TYPEST_VIS; } }
    public static String TYPE_RECPERSON { get { return CommonUtility.DISCOUNT_TYPEST_RECPERSON; } }
    public static String TYPE_TIME { get { return CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT; } }
    public static String TYPE_TIME_GRADUATE { get { return CommonUtility.DISCOUNT_TYPEST_TIMEDISCOUNT_GRADUATE; } }

    public EnrollmentManualDiscountController() {
        enrollmentFromDiscountId = Apexpages.currentPage().getParameters().get('enrollmentId');
        enrollmentFromDiscount = [
            SELECT Id, Value_after_Discount__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id =: enrollmentFromDiscountId
        ];

        newDiscount = new Discount__c();
        newDiscount.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT);
        newDiscount.Enrollment__c = enrollmentFromDiscountId;
        newDiscount.Applied__c = true;
        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_DISCRETIONARY;

        Map<String, String> translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('PL', Discount__c.Discount_Type_Studies__c);
        newDiscount.Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('EN', Discount__c.Discount_Type_Studies__c);
        newDiscount.English_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('RU', Discount__c.Discount_Type_Studies__c);
        newDiscount.Russian_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);
    }

    public PageReference save() {
        List<Discount__c> discountsToInsert = new List<Discount__c>();
        newDiscount.Name = CommonUtility.getPicklistLabelMap(Discount__c.Discount_Type_Studies__c).get(newDiscount.Discount_Type_Studies__c);
        
        discountsToInsert.add(newDiscount);

        if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_VIS) {
        } else if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_RECPERSON && newDiscount.Recommended_by__c != null) {

            if (recEnrEqualsDiscountedEnr()) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, Label.msg_error_CantAddRecPersonDiscountToSameEnr));
                return null;
            }

            Discount__c parallelDiscount = new Discount__c();

            parallelDiscount.Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('PL', Label.title_DiscountPersonRecommendation);
            parallelDiscount.English_Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('EN', Label.title_DiscountPersonRecommendation);
            parallelDiscount.Russian_Trade_Name__c = DictionaryTranslator.getTranslatedPicklistValues('RU', Label.title_DiscountPersonRecommendation);

            parallelDiscount.Name = Label.title_DiscountPersonRecommendation;
            parallelDiscount.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT);
            parallelDiscount.Enrollment__c = newDiscount.Recommended_by__c;
            parallelDiscount.Recommended_Person__c = newDiscount.Enrollment__c;
            parallelDiscount.Applied__c = newDiscount.Applied__c;
            parallelDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
            parallelDiscount.Applies_to__c = newDiscount.Applies_to__c;
            parallelDiscount.Applied_through__c = newDiscount.Applied_through__c;
            parallelDiscount.Discount_Kind__c = newDiscount.Discount_Kind__c;
            parallelDiscount.Discount_Value__c = newDiscount.Discount_Value__c;

            discountsToInsert.add(parallelDiscount);
        }

        try {
            insert discountsToInsert;
            return new PageReference('/' + newDiscount.Enrollment__c);
        } catch(Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }

    public PageReference back() {
        return new PageReference('/' + enrollmentFromDiscountId);
    }

    public void fillDiscountInfo() {
        newDiscount.Applies_to__c = null;
        newDiscount.Discount_Value__c = null;
        newDiscount.Applied_through__c = null;
        newDiscount.Discount_Kind__c = null;
        newDiscount.Delivery_Documents_Date__c = null;
        if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE) {
            newDiscount.Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION;
            newDiscount.Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_WHOLE;
            newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
            newDiscount.Discount_Value__c = 50;
        } else if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_RECPERSON) {
            newDiscount.Applies_to__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION;
            newDiscount.Applied_through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY;
            newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT;
            newDiscount.Discount_Value__c = 100;
        }

        Map<String, String> translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('PL', Discount__c.Discount_Type_Studies__c);
        newDiscount.Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('EN', Discount__c.Discount_Type_Studies__c);
        newDiscount.English_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);

        translatedTradeName = DictionaryTranslator.getTranslatedPicklistValues('RU', Discount__c.Discount_Type_Studies__c);
        newDiscount.Russian_Trade_Name__c = translatedTradeName.get(newDiscount.Discount_Type_Studies__c);
    }


    private Boolean recEnrEqualsDiscountedEnr() {
        return newDiscount.Enrollment__c == newDiscount.Recommended_by__c;
    }
}