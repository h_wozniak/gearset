/**
*   @author         Sebastian Łasisz
*   @description    This class is used to retrieve information about Contact for given Id.
**/

@RestResource(urlMapping = '/enrollment/*')
global without sharing class IntegrationEnrollmentData {


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HTTP METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */


    @HttpGet
    global static void getEnrollmentData() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String enrollmentId = RestContext.request.params.get('enrollment_id');

        if (enrollmentId == null || enrollmentId == '') {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(605);
            return ;
        }

        try {
            res.responseBody = Blob.valueOf(prepareResponse(enrollmentId));
            res.statusCode = 200;
        }
        catch (Exception ex) {
        	System.debug(ex);
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;    
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- PREPARING DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String prepareResponse(Id enrollmentId) {
        Enrollment__c studyEnrollment = [
            SELECT Name, Candidate_Student__c, Candidate_Student__r.FirstName, Candidate_Student__r.LastName, Candidate_Student__r.Phone, Candidate_Student__r.Email, Study_Start_Date__c,
            Unenrolled__c, Valid_From_for_email__c, Starting_Semester__c, Second_Course__c, Installments_per_Year__c, Tuition_System__c, Specialization__r.Trade_Name__c, University_Name__c,
            Educational_Agreement__r.Student_no__c, Enrollment_Source__c, Course_or_Specialty_Offer__r.Leading_Language__c, 
            Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c, Degree__c,
            Company_for_Discount_Name__c, Company_for_Discount_Tax_Id__c, Do_not_set_up_Student_Card__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, EFS__c,
            Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
            Candidate_Student__r.Additional_Emails__c, Course_or_Specialty_Offer__r.Active_From__c, Course_or_Specialty_Offer__r.Name, Candidate_Student__r.Gender__c, 
            Candidate_Student__r.Pesel__c, Candidate_Student__r.Middle_Name__c, Candidate_Student__r.Family_Name__c, Candidate_Student__r.Father_Name__c, Candidate_Student__r.School_Certificate_Country__c,
            Candidate_Student__r.Mother_Name__c, Candidate_Student__r.Birthdate, Candidate_Student__r.Place_of_Birth__c, Candidate_Student__r.Foreigner__c, 
            Candidate_Student__r.Country_of_Origin__c, Candidate_Student__r.Nationality__c, Candidate_Student__r.OtherCountry, Consent_Date_processing_for_contract__c,
            Candidate_Student__r.OtherPostalCode, Candidate_Student__r.OtherState, Candidate_Student__r.OtherCity, Candidate_Student__r.OtherStreet, Candidate_Student__r.MailingCountry,
            Candidate_Student__r.MailingPostalCode, Candidate_Student__r.MailingState, Candidate_Student__r.MailingCity, Candidate_Student__r.MailingStreet,
            Candidate_Student__r.MobilePhone, Course_or_Specialty_Offer__r.University_Name__c, Educational_Agreement__c,
            Candidate_Student__r.Polish_Card_Document__c, Candidate_Student__r.Expiration_Date_Polish_Card_Document__c, Candidate_Student__r.Study_Authorization_Document__c,         
            Candidate_Student__r.Consent_Direct_Communications__c, Candidate_Student__r.Consent_Electronic_Communication__c, Candidate_Student__r.Consent_Graduates__c, 
            Candidate_Student__r.Consent_Marketing__c, Candidate_Student__r.Consent_Direct_Communications_ADO__c, Candidate_Student__r.Consent_Electronic_Communication_ADO__c, 
            Candidate_Student__r.Consent_Graduates_ADO__c, Candidate_Student__r.Consent_Marketing_ADO__c, Discount_Code__c,
            Candidate_Student__r.Expiration_Date_Authorization_Doc__c, Candidate_Student__r.Identity_Document__c, Candidate_Student__r.Number_and_Series__c, 
            Candidate_Student__r.Country_of_Issue__c, Candidate_Student__r.Health_Insurance_Expiration_Date__c, Candidate_Student__r.Country_of_Birth__c, Finished_University__r.City_of_Diploma_Issue__c, Finished_University__r.University_from_Finished_University__r.School_Type__c,
            Candidate_Student__r.School__c, Candidate_Student__r.School_Name__c, Candidate_Student__r.School__r.Name,
            Candidate_Student__r.School__r.BillingCity, Candidate_Student__r.School_Town__c, Enrollment_Date__c, Enrollment_for_next_semester__c, 
            Candidate_Student__r.School__r.School_Type__c, Candidate_Student__r.School_Type__c, Candidate_Student__r.A_Level__c, 
            Candidate_Student__r.School_Certificate_With_Honors__c, Candidate_Student__r.WSB_Graduate__c, Candidate_Student__r.TEB_Graduate__c, Finished_University__r.University_from_Finished_University__r.Name, 
            Finished_University__r.University_from_Finished_University__r.BillingCity, Candidate_Student__r.A_Level_Issue_Date__c, Candidate_Student__r.A_Level_Id__c , Candidate_Student__r.Year_of_Graduation__c, Candidate_Student__r.School_Certificate_Town__c,
            Finished_University__r.ZPI_University_Name__c, Finished_University__r.Status__c, Candidate_Student__r.Regional_Examination_Commission__c ,
            Finished_University__r.ZPI_University_City__c, Initial_Specialty_Declaration__r.Specialty_Trade_Name__c, Trade_Name__c, Trade_Name_Specialty__c,
            Finished_University__r.University_from_Finished_University__c, Initial_Specialty_Declaration__c, Signed_Contract_Date__c,
            Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Guided_group__c, WSB_Passport__c,
            Price_Book_from_Enrollment__c, Price_Book_from_Enrollment__r.Price_Book_Type__c, Price_Book_from_Enrollment__r.PriceBook_From__c,
            Finished_University__c, Finished_University__r.Finished_Course__c, Price_Book_from_Enrollment__r.Name, Unenrolled_Signed_Contract_Date__c,
            Finished_University__r.Defense_Date__c, Finished_University__r.Diploma_Number__c, Finished_University__r.Country_of_Diploma_Issue__c, 
            Finished_University__r.Gained_Title__c, Finished_University__r.Diploma_Issue_Date__c, Finished_University__r.Graduation_Year__c,
                (SELECT Base_Consent__r.Name, Consent__c, Consent_Date__c, Refusal_Date__c, Consent_Source__c, Consent_Storage_Location__c, Contact_From_Consent__c
                FROM Consents__r),
                (SELECT Id, Foreign_Language__c, Language_Level__c, Language_Certification__c FROM Languages__r)
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        Contact candidate = [
        	SELECT Id, Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c,
	               Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c, Consent_Graduates__c,
	               (SELECT Id, Company_Name__c, Position__c, Employed_From__c, Employed_Till__c, 
	                 Street_of_Employment__c, Postal_Code__c, City_of_Employment__c, Country_of_Employment__c
	                  FROM WorkExperience__r )
	        FROM Contact
	        WHERE Id = :studyEnrollment.Candidate_Student__c
        ];

        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name, ADO__c, Active_To__c, Active_From__c, iPressoId__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
        ];        

        Enrollment_JSON enrollment = new Enrollment_JSON();
    	enrollment.first_name = studyEnrollment.Candidate_Student__r.FirstName;
    	enrollment.middle_name = studyEnrollment.Candidate_Student__r.Middle_Name__c;
    	enrollment.last_name = studyEnrollment.Candidate_Student__r.LastName;
    	enrollment.family_name = studyEnrollment.Candidate_Student__r.Family_Name__c;
    	enrollment.father_name = studyEnrollment.Candidate_Student__r.Father_Name__c;
    	enrollment.mother_name = studyEnrollment.Candidate_Student__r.Mother_Name__c;
    	enrollment.gender = studyEnrollment.Candidate_Student__r.Gender__c;
    	enrollment.personal_id = studyEnrollment.Candidate_Student__r.Pesel__c;
    	enrollment.birthdate = String.valueOf(studyEnrollment.Candidate_Student__r.Birthdate);
    	enrollment.place_of_birth = studyEnrollment.Candidate_Student__r.Place_of_Birth__c;
    	enrollment.foreigner = studyEnrollment.Candidate_Student__r.Foreigner__c;
    	enrollment.country_of_origin = studyEnrollment.Candidate_Student__r.Country_of_Origin__c;
    	enrollment.nationality = prepareNationalities(studyEnrollment.Candidate_Student__r.Nationality__c);
    	enrollment.residence_document = prepareResidenceDocument(studyEnrollment);
    	enrollment.identity_document = prepareIdentityDocument(studyEnrollment);
    	enrollment.health_insurance_validity_date = String.valueOf(studyEnrollment.Candidate_Student__r.Health_Insurance_Expiration_Date__c);
    	enrollment.teb_graduate = studyEnrollment.Candidate_Student__r.TEB_Graduate__c;
    	enrollment.wsb_graduate = studyEnrollment.Candidate_Student__r.WSB_Graduate__c;
    	enrollment.school_certificate_with_honours = studyEnrollment.Candidate_Student__r.School_Certificate_With_Honors__c;
    	enrollment.career = prepareCareer(candidate);
    	enrollment.addresses = addressesToList(studyEnrollment);
    	enrollment.phones = phonesToList(studyEnrollment.Candidate_Student__r.Phone, studyEnrollment.Candidate_Student__r.MobilePhone);
    	enrollment.email = studyEnrollment.Candidate_Student__r.Email;
    	enrollment.consents = prepareConsentsForContact(candidate, catalogConsentsToValidate);
    	enrollment.enrollment_data = prepareEnrollmentData(studyEnrollment);

    	return JSON.serialize(enrollment);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static EnrollmentData_JSON prepareEnrollmentData(Enrollment__c studyEnrollment) {
    	EnrollmentData_JSON enrollmentData = new EnrollmentData_JSON();
    	enrollmentData.enrollment_id = studyEnrollment.Id;
    	enrollmentData.product_name = studyEnrollment.Trade_Name__c;
    	enrollmentData.specialization_name = studyEnrollment.Trade_Name_Specialty__c;
    	enrollmentData.for_next_semester = studyEnrollment.Enrollment_for_next_semester__c;
    	enrollmentData.next_product = studyEnrollment.Second_Course__c;
    	enrollmentData.tuition_system = studyEnrollment.Tuition_System__c;
    	enrollmentData.number_of_installments = Integer.valueOf(studyEnrollment.Installments_per_Year__c);
    	enrollmentData.discount_code = studyEnrollment.Discount_Code__c;

    	enrollmentData.company_for_discount = prepareCompanyDiscount(studyEnrollment);
    	enrollmentData.selected_languages = prepareSelectedLanguages(studyEnrollment);
    	enrollmentData.university = prepareUniversity(studyEnrollment);
    	enrollmentData.secondary_school = prepareSecondarySchool(studyEnrollment);

    	return enrollmentData;
    }

    public static University_JSON prepareUniversity(Enrollment__c studyEnrollment) {
    	University_JSON university = new University_JSON();
    	university.diploma = prepareDiploma(studyEnrollment);
    	university.graduation_year = studyEnrollment.Finished_University__r.Graduation_Year__c;
    	university.major = studyEnrollment.Finished_University__r.Finished_Course__c;
    	if (studyEnrollment.Finished_University__r.University_from_Finished_University__c == null) {
    		university.name = studyEnrollment.Finished_University__r.ZPI_University_Name__c;
    		university.city = studyEnrollment.Finished_University__r.ZPI_University_City__c;
    	}
    	else {
    		university.name = studyEnrollment.Finished_University__r.University_from_Finished_University__r.Name;
    		university.city = studyEnrollment.Finished_University__r.University_from_Finished_University__r.BillingCity;
    		university.type = studyEnrollment.Finished_University__r.University_from_Finished_University__r.School_Type__c;
    	}

    	return university;
    }

    public static Diploma_JSON prepareDiploma(Enrollment__c studyEnrollment) {
    	Diploma_JSON diploma = new Diploma_JSON();
    	diploma.diploma_number = studyEnrollment.Finished_University__r.Diploma_Number__c;
    	diploma.examination_date = String.valueOf(studyEnrollment.Finished_University__r.Defense_Date__c);
    	diploma.issue_city = studyEnrollment.Finished_University__r.City_of_Diploma_Issue__c;
    	diploma.issue_country = studyEnrollment.Finished_University__r.Country_of_Diploma_Issue__c;
    	diploma.issue_date = String.valueOf(studyEnrollment.Finished_University__r.Diploma_Issue_Date__c);
    	diploma.status = studyEnrollment.Finished_University__r.Status__c;
    	diploma.title = studyEnrollment.Finished_University__r.Gained_Title__c;

    	return diploma;
    }

    public static SecondarySchool_JSON prepareSecondarySchool(Enrollment__c studyEnrollment) {
    	SecondarySchool_JSON secondarySchool = new SecondarySchool_JSON();
    	secondarySchool.a_level_id = studyEnrollment.Candidate_Student__r.A_Level_Id__c;
    	secondarySchool.a_level_issue_date = String.valueOf(studyEnrollment.Candidate_Student__r.A_Level_Issue_Date__c);
    	secondarySchool.a_level_status = studyEnrollment.Candidate_Student__r.A_Level__c;
    	secondarySchool.certificate_city = studyEnrollment.Candidate_Student__r.School_Certificate_Town__c;
    	secondarySchool.certificate_country = studyEnrollment.Candidate_Student__r.School_Certificate_Country__c;
    	secondarySchool.city = studyEnrollment.Candidate_Student__r.School__c == null ? 
                               studyEnrollment.Candidate_Student__r.School_Town__c :
                               studyEnrollment.Candidate_Student__r.School__r.BillingCity;
    	secondarySchool.examination_committee_name = studyEnrollment.Candidate_Student__r.Regional_Examination_Commission__c;
    	secondarySchool.graduation_year = studyEnrollment.Candidate_Student__r.Year_of_Graduation__c;
    	secondarySchool.name = studyEnrollment.Candidate_Student__r.School__c == null ? 
                               studyEnrollment.Candidate_Student__r.School_Name__c :
                               studyEnrollment.Candidate_Student__r.School__r.Name;
    	secondarySchool.type = studyEnrollment.Candidate_Student__r.School__c == null ? 
                               studyEnrollment.Candidate_Student__r.School_Type__c :
                               studyEnrollment.Candidate_Student__r.School__r.School_Type__c;

        return secondarySchool;
    }

    public static List<SelectedLanguages_JSON> prepareSelectedLanguages(Enrollment__c studyEnrollment) {
        List<SelectedLanguages_JSON> languages = new List<SelectedLanguages_JSON>();
        for (Enrollment__c lang : studyEnrollment.Languages__r) {
            SelectedLanguages_JSON newLang = new SelectedLanguages_JSON();
            newLang.code = lang.Foreign_Language__c;
            newLang.level = lang.Language_Level__c;

            languages.add(newLang);
        }

        return languages;    	
    }

    public static CompanyDiscount_JSON prepareCompanyDiscount(Enrollment__c studyEnrollment) {
    	CompanyDiscount_JSON companyDiscount = new CompanyDiscount_JSON();
    	companyDiscount.name = studyEnrollment.Company_for_Discount_Name__c;
    	companyDiscount.tax_id = studyEnrollment.Company_for_Discount_Tax_Id__c;

    	return companyDiscount;
    }

    public static List<Career_JSON> prepareCareer(Contact candidate) {
        List<Career_JSON> careers = new List<Career_JSON>();

        if (candidate != null) {
            for (Qualification__c workExperience : candidate.WorkExperience__r) {
                Career_JSON career = new Career_JSON();
                career.date_end = String.valueOf(workExperience.Employed_Till__c);
                career.date_start = String.valueOf(workExperience.Employed_From__c);
                career.name = workExperience.Company_Name__c;
                career.position = workExperience.Position__c;

                AddressCareer_JSON address = new AddressCareer_JSON();
                address.city = workExperience.City_of_Employment__c;
                address.country = workExperience.Country_of_Employment__c;
                address.postal_code = workExperience.Postal_Code__c;
                address.street = workExperience.Street_of_Employment__c;
                career.address = address;

                careers.add(career);
            }
        }

        return careers;
    }

    /* Method used to retrieve valid consents for iPresso */
    public static List<Consent_JSON> prepareConsentsForContact(Contact contactWithConsent, List<Catalog__c> catalogConsentsToValidate) {
        List<String> consentNames = new List<String>{ RecordVals.MARKETING_CONSENT_1, RecordVals.MARKETING_CONSENT_2, 
                                                      RecordVals.MARKETING_CONSENT_4, RecordVals.MARKETING_CONSENT_5};

        List<Consent_JSON> consentsForContact = new List<Consent_JSON>();

        for (Catalog__c catalogConsent : catalogConsentsToValidate) {
        	for (String consentName : consentNames) {
	        	if (catalogConsent.Name.contains(consentName)) {
		            if (contactWithConsent != null && CatalogManager.getApiADONameFromConsentName(catalogConsent.Name) != null 
		                && contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)) != null) {                
		                Set<String> contactAcceptedADO = CommonUtility.getOptionsFromMultiSelect((String)contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)));
		                Set<String> catalogSelectedADO = CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c);
		                
		                consentsForContact.add(new Consent_JSON(catalogConsent.Name, contactAcceptedADO.containsAll(catalogSelectedADO))); 
		            }     
		            else {
		                consentsForContact.add(new Consent_JSON(catalogConsent.Name, false)); 
		            }  
		        }
		    }
        }

        return consentsForContact;
    }

    public static IndentityDocumentJSON prepareIdentityDocument(Enrollment__c studyEnrollment) {
        Map<String, String> translatedPickListValues = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Country_of_Origin__c);

    	IndentityDocumentJSON document = new IndentityDocumentJSON();
    	document.document_number = studyEnrollment.Candidate_Student__r.Number_and_Series__c;
    	document.issue_country = translatedPickListValues.get(studyEnrollment.Candidate_Student__r.Country_of_Issue__c);
    	document.type = mapIdentityDocumentPicklist(studyEnrollment.Candidate_Student__r.Identity_Document__c);

    	return document;
    }

    public static ResidenceDocument_JSON prepareResidenceDocument(Enrollment__c studyEnrollment) {
    	ResidenceDocument_JSON document = new ResidenceDocument_JSON();
    	document.type = 
        (studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c != null 
        && studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c != CommonUtility.CONTACT_AUTHORIZATION_DOCUMENT_NONE) 
            ? studyEnrollment.Candidate_Student__r.Study_Authorization_Document__c.toLowerCase() 
            : null;
    	document.validity_date = String.valueOf(studyEnrollment.Candidate_Student__r.Expiration_Date_Authorization_Doc__c);

    	return document;
    }

    public static List<String> prepareNationalities(String nationalitiesToPrepare) {
        List<String> nationalities = CommonUtility.getOptionsFromMultiSelectToList(nationalitiesToPrepare);
        Map<String, String> nationalityTransaltionMap = DictionaryTranslator.getTranslatedPicklistValues(CommonUtility.LANGUAGE_CODE_PL, Contact.Nationality__c);

        List<String> newNationalities = new List<String>();

        for (String nationality : nationalities) {
            newNationalities.add(nationalityTransaltionMap.get(nationality));
        }

        return newNationalities;
    }

    public static List<Address_JSON> addressesToList(Enrollment__c studyEnrollment) {
        List<Address_JSON> contactAddresses = new List<Address_JSON>();

        Address_JSON homeAddressFromCandidate = new Address_JSON();
        homeAddressFromCandidate.type = 'Home';
        homeAddressFromCandidate.country = studyEnrollment.Candidate_Student__r.OtherCountry;
        homeAddressFromCandidate.postal_code = studyEnrollment.Candidate_Student__r.OtherPostalCode;
        homeAddressFromCandidate.city = studyEnrollment.Candidate_Student__r.OtherCity;
        homeAddressFromCandidate.street = studyEnrollment.Candidate_Student__r.OtherStreet;
        contactAddresses.add(homeAddressFromCandidate);

        Address_JSON mailingAddressFromCandidate = new Address_JSON();
        mailingAddressFromCandidate.type = 'Mailing';
        mailingAddressFromCandidate.country = studyEnrollment.Candidate_Student__r.MailingCountry;
        mailingAddressFromCandidate.postal_code = studyEnrollment.Candidate_Student__r.MailingPostalCode;
        mailingAddressFromCandidate.city = studyEnrollment.Candidate_Student__r.MailingCity;
        mailingAddressFromCandidate.street = studyEnrollment.Candidate_Student__r.MailingStreet;
        contactAddresses.add(mailingAddressFromCandidate); 

        return contactAddresses;      
    }

    public static List<Phone_JSON> phonesToList(String phoneNumber, String mobilePhone) {
        List<Phone_JSON> phones = new List<Phone_JSON>();

        if (phoneNumber != null && phoneNumber != '') {
            Phone_JSON phone = new Phone_JSON();
            phone.type = 'Default';
            phone.phone_number = phoneNumber;        
            phones.add(phone);
        }

        if (mobilePhone != null && mobilePhone != '') {
            Phone_JSON cellPhone = new Phone_JSON();
            cellPhone.type = 'Mobile';
            cellPhone.phone_number = mobilePhone;        
            phones.add(cellPhone);
        }

        return phones;        
    }

    public static String mapIdentityDocumentPicklist(String value) {
        //if (value == CommonUtility.CONTACT_IDENTITY_DOCUMENT_CARD) {
            //return 'personal id';
        //}
        //else {
            return value != null ? value.toLowerCase() : null;
        //}
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    global class Enrollment_JSON {
    	public String first_name;
    	public String middle_name;
    	public String last_name;
    	public String family_name;
    	public String father_name;
    	public String mother_name;
    	public String gender;
    	public String personal_id;
    	public String birthdate;
    	public String place_of_birth;
    	public Boolean foreigner;
    	public String country_of_origin;
    	public List<String> nationality;
    	public ResidenceDocument_JSON residence_document;
    	public IndentityDocumentJSON identity_document;
    	public String health_insurance_validity_date;
    	public Boolean teb_graduate;
    	public Boolean wsb_graduate;
    	public Boolean school_certificate_with_honours;
    	public List<Career_JSON> career;
    	public List<Address_JSON> addresses;
    	public List<Phone_JSON> phones;
    	public String email;
    	public List<Consent_JSON> consents;
    	public EnrollmentData_JSON enrollment_data;
    }

    global class Career_JSON {
    	public String name;
    	public String position;
    	public String date_start;
    	public String date_end;
    	public AddressCareer_JSON address;
    }

    global class AddressCareer_JSON {
    	public String street;
    	public String city;
    	public String postal_code;
    	public String country;
    }

    global class ResidenceDocument_JSON {
    	public String type;
    	public String validity_date;
    }

    global class IndentityDocumentJSON {
    	public String type;
    	public String document_number;
    	public String issue_country;
    }

    global class Address_JSON {
        public String type;
        public String country;
        public String postal_code;
        public String state;
        public String city;
        public String street;
    }

    global class Phone_JSON {
        public String type;
        public String phone_number;
    }
    
    global class Consent_JSON {
        public String type;
        public Boolean value;

        public Consent_JSON(String type, Boolean value) {
            this.type = type;
            this.value = value;
        }
    }

    global class EnrollmentData_JSON {
    	public String enrollment_id;
    	public String product_name;
    	public String specialization_name;
    	public Boolean for_next_semester;
    	public Boolean next_product;
    	public String tuition_system;
    	public Integer number_of_installments;
    	public String discount_code;
    	public CompanyDiscount_JSON company_for_discount;
    	public List<SelectedLanguages_JSON> selected_languages;
    	public University_JSON university;
    	public SecondarySchool_JSON secondary_school;
    }

    global class CompanyDiscount_JSON {
    	public String name;
    	public String tax_id;
    }

    global class SelectedLanguages_JSON {
    	public String code;
    	public String level;
    }

    global class University_JSON {
    	public String name;
    	public String city;
    	public String type;
    	public String major;
    	public String graduation_year;
    	public Diploma_JSON diploma;
    }

    global class Diploma_JSON {
    	public String status;
    	public String examination_date;
    	public String diploma_number;
    	public String issue_country;
    	public String issue_city;
    	public String issue_date;
    	public String title;
    }

    global class SecondarySchool_JSON {
    	public String name;
    	public String type;
    	public String city;
    	public String a_level_status;
    	public String a_level_id;
    	public String a_level_issue_date;
    	public String examination_committee_name;
    	public String certificate_city;
    	public String certificate_country;
    	public String graduation_year;
    }
}