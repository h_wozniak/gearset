@isTest
private class ZTestEnrollmentSynchronizationController {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    @isTest static void test_EnrollmentSynchronizationController_1() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            PageReference pageRef = Page.EnrollmentSynchronizationStatusBar;
            Test.setCurrentPageReference(pageRef);

            Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
            ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

            Test.startTest();
            EnrollmentSynchronizationController cntrl = new EnrollmentSynchronizationController(stdController);
            Test.stopTest();

            System.assertEquals(cntrl.backgroundColor, '');
            System.assertEquals(cntrl.enrollmentSychStatus, CommonUtility.SYNCSTATUS_NEW);
            System.assertEquals(cntrl.tdWidth, null);
            System.assertEquals(cntrl.unenrTdWidth, null);
        }
    }

    @isTest static void test_EnrollmentSynchronizationController_2() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            PageReference pageRef = Page.EnrollmentSynchronizationStatusBar;
            Test.setCurrentPageReference(pageRef);

            Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = CommonUtility.SYNCSTATUS_INPROGRESS), true);
            ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

            Test.startTest();
            EnrollmentSynchronizationController cntrl = new EnrollmentSynchronizationController(stdController);
            Test.stopTest();

            System.assertEquals(cntrl.backgroundColor, '#D7F26D');
            System.assertEquals(cntrl.enrollmentSychStatus, CommonUtility.SYNCSTATUS_INPROGRESS);
            System.assertEquals(cntrl.tdWidth, null);
            System.assertEquals(cntrl.unenrTdWidth, null);
        }
    }

    @isTest static void test_EnrollmentSynchronizationController_3() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            PageReference pageRef = Page.EnrollmentSynchronizationStatusBar;
            Test.setCurrentPageReference(pageRef);

            Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = CommonUtility.SYNCSTATUS_FINISHED), true);
            ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

            Test.startTest();
            EnrollmentSynchronizationController cntrl = new EnrollmentSynchronizationController(stdController);
            Test.stopTest();

            System.assertEquals(cntrl.backgroundColor, '#52A849');
            System.assertEquals(cntrl.enrollmentSychStatus, CommonUtility.SYNCSTATUS_FINISHED);
            System.assertEquals(cntrl.tdWidth, null);
            System.assertEquals(cntrl.unenrTdWidth, null);
        }
    }

    @isTest static void test_EnrollmentSynchronizationController_4() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            PageReference pageRef = Page.EnrollmentSynchronizationStatusBar;
            Test.setCurrentPageReference(pageRef);

            Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Synchronization_Status__c = CommonUtility.SYNCSTATUS_FAILED), true);
            ApexPages.StandardController stdController = new ApexPages.standardController(studyEnr);

            Test.startTest();
            EnrollmentSynchronizationController cntrl = new EnrollmentSynchronizationController(stdController);
            Test.stopTest();

            System.assertEquals(cntrl.backgroundColor, '#D84141');
            System.assertEquals(cntrl.enrollmentSychStatus, CommonUtility.SYNCSTATUS_FAILED);
            System.assertEquals(cntrl.tdWidth, null);
            System.assertEquals(cntrl.unenrTdWidth, null);
        }
    }
}