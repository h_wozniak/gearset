/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store methods related to Documents (Templates)
**/

public without sharing class TemplateManager {

    /* Wojciech Słodziak */
    // returns template Id based on Document name, Study Degree and University Name
    public static Id getDocumentTemplateId(String language, String docName, String degree, String universityName) {
        String templateName = '';
        
        if (language != null) {
            templateName += '[' + CommonUtility.getLanguageAbbreviationDocGen(language) + ']';
        }
        else {
            templateName += '[PL]';
        }

        templateName += '[' + docName + ']';
        if (degree != null) {
            if (degree == CommonUtility.OFFER_DEGREE_I || degree == CommonUtility.OFFER_DEGREE_II || degree == CommonUtility.OFFER_DEGREE_II_PG || degree == CommonUtility.OFFER_DEGREE_U) {
                templateName += '[' + CommonUtility.TEMPLATE_NAME_HIGHER_EDUCATION + ']';
            }
            else {
                templateName += '[' + degree + ']';
            }
        }
        if (universityName != null) {
            templateName += '[' + universityName + ']';
        }

        try {
            enxoodocgen__Document_Template__c template = [
                SELECT Id 
                FROM enxoodocgen__Document_Template__c 
                WHERE Name = :templateName 
                LIMIT 1
            ];

            return template.Id;
        } catch (Exception ex) {
            ErrorLogger.configMsg(CommonUtility.DOCUMENT_TEMPLATE_UNDEFINED_ERROR + ' ' + templateName);
        }
        
        return null;
    }

}