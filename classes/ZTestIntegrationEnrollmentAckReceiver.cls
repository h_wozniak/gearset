@isTest
global class ZTestIntegrationEnrollmentAckReceiver {
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = retMap.get(1).Id, 
            Candidate_Student__c = retMap.get(0).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), true));
        retMap.put(3, ZDataTestUtility.createEducationalAgreement(new Enrollment__c(
            Source_Enrollment__c = retMap.get(2).Id
        ), true));
        
        Enrollment__c studyEnr = (Enrollment__c) retMap.get(2);
        studyEnr.Educational_Agreement__c = retMap.get(3).Id;
        update studyEnr;
        
        return retMap;
    }
    
    @isTest static void test_receiveEnrollmentInformation_Code400() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        req.requestBody = Blob.valueOf('this JSON is very wrong...');

        Test.startTest();
        IntegrationEnrollmentAckReceiver.receiveEnrollmentInformation();
        Test.stopTest();

        
        System.assertEquals(400, res.statusCode);
    }
    
    @isTest static void test_receiveEnrollmentInformation_Code200_Success() {
        Map<Integer, sObject> dataMap = prepareData();

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        IntegrationManager.AckResponse_JSON resp = new IntegrationManager.AckResponse_JSON();

        resp.record_id = dataMap.get(3).Id;
        resp.success = true;

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationEnrollmentAckReceiver.receiveEnrollmentInformation();
        Test.stopTest();
        
        Enrollment__c studyEnr = [
            SELECT Id, Status__c, Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS, studyEnr.Status__c);
    }


    @isTest static void test_receiveEnrollmentInformation_Code200_Failed() {
        Map<Integer, sObject> dataMap = prepareData();

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        IntegrationManager.AckResponse_JSON resp = new IntegrationManager.AckResponse_JSON();

        resp.record_id = dataMap.get(3).Id;
        resp.success = false;

        String jsonBody = JSON.serialize(resp);
        
        req.requestBody = Blob.valueOf(jsonBody);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;

        Test.startTest();
        IntegrationEnrollmentAckReceiver.receiveEnrollmentInformation();
        Test.stopTest();
        
        Enrollment__c studyEnr = [
            SELECT Id, Status__c, Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, studyEnr.Status__c);
        System.assertEquals(CommonUtility.SYNCSTATUS_FAILED, studyEnr.Synchronization_Status__c);
    }

}