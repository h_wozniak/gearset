/**
*   @author         Sebastian Łasisz
*   @description    batch class that creates records from Returned Leads Import
**/

public class IntegrationIpressoContactCreationBatch  implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    public Set<Id> contactIds;

    public IntegrationIpressoContactCreationBatch(Set<Id> contactIds) {
        this.contactIds = contactIds;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id
                FROM Contact
                WHERE Id IN :contactIds
        ]);
    }

    public void execute(Database.BatchableContext BC, List<Contact> contactsToSendToIpresso) {
        Set<Id> contactIdsToSendToIpresso = new Set<Id>();
        for (Contact c : contactsToSendToIpresso) {
            contactIdsToSendToIpresso.add(c.Id);
        }

        IntegrationIpressoContactCreationSender.sendIPressoContactList(contactIdsToSendToIpresso);
    }

    public void finish(Database.BatchableContext BC) {}
}