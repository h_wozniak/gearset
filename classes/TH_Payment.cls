/**
* @description  This class is a Trigger Handler for Payments - complex logic
**/

public without sharing class TH_Payment extends TriggerHandler.DelegateBase {
    
    Set<Id> studyEnrollmentsToUpdateValue;
    Set<Id> paymentInstToDeleteAttachedDiscounts;
    Set<Id> studyEnrToCheckIfConfDocReq;
    Set<Id> studyEnrToVerifyStudentCardCbx;
    List<Payment__c> paymentToValidateIfCanEdit;
    
    public override void prepareBefore() {
        paymentInstToDeleteAttachedDiscounts = new Set<Id>();
        paymentToValidateIfCanEdit = new List<Payment__c>();
    }
    
    public override void prepareAfter() { 
        studyEnrollmentsToUpdateValue = new Set<Id>();
        studyEnrToCheckIfConfDocReq = new Set<Id>();
        studyEnrToVerifyStudentCardCbx = new Set<Id>();
    }
    
    //public override void beforeInsert(List<sObject> o) {}
    
    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;
        for (Id key : newPayments.keySet()) {
            Payment__c newPayment = newPayments.get(key);

            /* Sebastian Łasisz */
            // update Enrollment_Value__c on Study enrollment when new Payment Installment is inserted
            // logic doesn't apply to additional charges
            if (!CommonUtility.skipStudyEnrValueCalculation 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && newPayment.Type__c != CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE) {
                studyEnrollmentsToUpdateValue.add(newPayment.Enrollment_from_Schedule_Installment__c);
            }

            /* Wojciech Słodziak */
            // on Entry Inst insert check if Entry Inst Conf Document is required
            if (newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && newPayment.Type__c == CommonUtility.PAYMENT_TYPE_ENTRY_FEE) {
                studyEnrToCheckIfConfDocReq.add(newPayment.Enrollment_from_Schedule_Installment__c);
            }

            /* Wojciech Słodziak */
            // on Student Card Charge insert verify Do_not_set_up_Student_Card__c on Enrollment
            if (newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && newPayment.Type__c == CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
                && newPayment.Description__c == RecordVals.CS_AC_NAME_STUDENTCARD) {
                studyEnrToVerifyStudentCardCbx.add(newPayment.Enrollment_from_Schedule_Installment__c);
            }
        }
    }
    
    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;
        for (Id key : oldPayments.keySet()) {
            Payment__c oldPayment = oldPayments.get(key);
            Payment__c newPayment = newPayments.get(key);

            if (newPayment.PriceBook_Value__c != oldPayment.PriceBook_Value__c
                || newPayment.Discount_Value__c != oldPayment.Discount_Value__c
                || newPayment.Value_after_Discount__c != oldPayment.Value_after_Discount__c
                || newPayment.Type__c != oldPayment.Type__c) {
                paymentToValidateIfCanEdit.add(newPayment);                
            }
        }        
    }
    
    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        Map<Id, Payment__c> newPayments = (Map<Id, Payment__c>)o;
        for (Id key : oldPayments.keySet()) {
            Payment__c oldPayment = oldPayments.get(key);
            Payment__c newPayment = newPayments.get(key);

            /* Sebastian Łasisz */
            // update Enrollment_Value__c on Study enrollment when Payment Installment or Discount Value is changed 
            // logic doesn't apply to additional charges
            if (!CommonUtility.skipStudyEnrValueCalculation 
                && newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST) 
                && (
                    newPayment.Discount_Value__c != oldPayment.Discount_Value__c 
                    || newPayment.PriceBook_Value__c != oldPayment.PriceBook_Value__c
                )
                && newPayment.Type__c != CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
            ) {
                studyEnrollmentsToUpdateValue.add(newPayment.Enrollment_from_Schedule_Installment__c);
            }

            /* Wojciech Słodziak */
            // on Entry Inst Value_after_Discount__c change check if Entry Inst Conf Document is required
            if (newPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && newPayment.Type__c == CommonUtility.PAYMENT_TYPE_ENTRY_FEE
                && newPayment.Value_after_Discount__c != oldPayment.Value_after_Discount__c
                && (newPayment.Value_after_Discount__c == 0 || oldPayment.Value_after_Discount__c == 0)) {
                studyEnrToCheckIfConfDocReq.add(newPayment.Enrollment_from_Schedule_Installment__c);
            }
        }
        
    }
    
    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        Payment__c oldPayment;
        for (Id key : oldPayments.keySet()) {
            oldPayment = oldPayments.get(key);

            /* Wojciech Słodziak */
            // delete attached Discounts from Payment Installment on delete
            if (oldPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)) {
                paymentInstToDeleteAttachedDiscounts.add(oldPayment.Id);
            }
        }      
    }
    
    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Payment__c> oldPayments = (Map<Id, Payment__c>)old;
        for (Id key : oldPayments.keySet()) {
            Payment__c oldPayment = oldPayments.get(key);

            /* Sebastian Łasisz */
            // update Enrollment_Value__c on Study enrollment when Payment is deleted
            // logic doesn't apply to additional charges
            if (!CommonUtility.skipStudyEnrValueCalculation 
                && oldPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && oldPayment.Type__c != CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE) {
                studyEnrollmentsToUpdateValue.add(oldPayment.Enrollment_from_Schedule_Installment__c);
            }

            /* Wojciech Słodziak */
            // on Entry Inst delete check if Entry Inst Conf Document is required
            if (oldPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && oldPayment.Type__c == CommonUtility.PAYMENT_TYPE_ENTRY_FEE) {
                studyEnrToCheckIfConfDocReq.add(oldPayment.Enrollment_from_Schedule_Installment__c);
            }

            /* Wojciech Słodziak */
            // on Student Card Charge delete verify Do_not_set_up_Student_Card__c on Enrollment
            if (oldPayment.RecordTypeId == CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST)
                && oldPayment.Type__c == CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
                && oldPayment.Description__c == RecordVals.CS_AC_NAME_STUDENTCARD) {
                studyEnrToVerifyStudentCardCbx.add(oldPayment.Enrollment_from_Schedule_Installment__c);
            }
        } 
    }
    
    public override void finish() {

        /* Sebastian Łasisz */
        // Check whether payments can be edited
        if (paymentToValidateIfCanEdit != null && !paymentToValidateIfCanEdit.isEmpty()) {
            PaymentManager.checkWhetherCanEditPayment(paymentToValidateIfCanEdit);
        }

        /* Sebastian Łasisz */
        // update Enrollment_Value__c on Study enrollment when Payment is inserted, updated or deleted
        if (studyEnrollmentsToUpdateValue != null && !studyEnrollmentsToUpdateValue.isEmpty()) {
            EnrollmentManager_Studies.updateStudyEnrollmentsValue(studyEnrollmentsToUpdateValue);
        }

        /* Wojciech Słodziak */
        // delete attached Discounts from Payment Installment on delete
        if (paymentInstToDeleteAttachedDiscounts != null && !paymentInstToDeleteAttachedDiscounts.isEmpty()) {
            DiscountManager_Studies.removePaymentDiscConnFromScheduleInstallments(paymentInstToDeleteAttachedDiscounts);
        }

        /* Wojciech Słodziak */
        // on Entry Inst insert, delete or Value_after_Discount__c change check if Entry Inst Conf Document is required
        if (studyEnrToCheckIfConfDocReq != null && !studyEnrToCheckIfConfDocReq.isEmpty()) {
            PaymentManager.updatePaymentConfDocFDIBasedOnEntryInst(studyEnrToCheckIfConfDocReq);
        }

        /* Wojciech Słodziak */
        // on Student Card Charge insert/delete verify Do_not_set_up_Student_Card__c on Enrollment
        if (studyEnrToVerifyStudentCardCbx != null && !studyEnrToVerifyStudentCardCbx.isEmpty()) {
            PaymentManager.verifyDoNotSetUpStudentCardCbx(studyEnrToVerifyStudentCardCbx);
        }

    }
}