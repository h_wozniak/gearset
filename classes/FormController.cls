/**
 * Created by martyna.stanczuk on 2018-07-18.
 */

public without sharing class FormController {

    @AuraEnabled
    public static void saveTheFile(String parentId, String file, String fileName) {

        Attachment oAttachment = new Attachment();
        oAttachment.ParentId = parentId;
        oAttachment.ContentType = 'application/pdf';
        oAttachment.Body = EncodingUtil.base64Decode(file);
        oAttachment.Name = fileName;

        insert oAttachment;
    }
}