/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send emails with Extranet Information
**/

/**
*   To initiate the UnenrolledDocReminderSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('UnenrolledDocReminderSchedule', '0 0 8 * * ? *', new UnenrolledDocReminderSchedule());
**/

global with sharing class UnenrolledDocReminderSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        UnenrolledDocReminderBatch unenrolledDocReminder = new UnenrolledDocReminderBatch(1);
        Database.executebatch(unenrolledDocReminder, 2);
    }
}