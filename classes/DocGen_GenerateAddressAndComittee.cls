global class DocGen_GenerateAddressAndComittee implements enxoodocgen.DocGen_StringTokenInterface {

    public static Map<String, String> addressesTranslatonMap = new Map<String, String>{
        'Lower Silesia' => 'dolnośląskie',
        'Kuyavian-Pomeranian' => 'kujawsko-pomorskie',
        'Lublin' => 'lubelskie',
        'Lubusz' => 'lubuskie',
        'Łódź' => 'łódzkie',
        'Lesser Poland' => 'małopolskie',
        'Masovian' => 'mazowieckie',
        'Opole' => 'opolskie',
        'Subcarpathian' => 'podkarpackie',
        'Podlaskie' => 'podlaskie',
        'Pomeranian' => 'pomorskie',
        'Silesia' => 'śląskie',
        'Świętokrzyskie' => 'świętokrzyskie',
        'Greater Poland' => 'wielkopolskie',
        'Warmian-Masurian' => 'warmińsko-mazurskie',
        'West Pomeranian' => 'zachodniopomorskie'
    };

	public DocGen_GenerateAddressAndComittee() {}

	global static String executeMethod(String objectId, String templateId, String methodName) {  
        if (methodName == 'mailingCity') {
            return generateMailingCity(objectId);
        }
        else if (methodName == 'mailingPostalCode') {
            return generateMailingPostalCode(objectId);
        }
        else if (methodName == 'mailingStreet') {
            return generateMailingStreet(objectId);
        }
        else if (methodName == 'mailingState') {
            return generateMailingState(objectId);
        }
        else if (methodName == 'otherState') {
            return generateOtherState(objectId);
        }
        else if (methodName == 'questionnarePhone') {
            return generatePhoneNumber(objectId);
        }
        else if (methodName == 'nationality') {
            return getNationality(objectId);
        }
        else if (methodName == 'identityDocument') {
            return getIdentityDocument(objectId);
        }

        return DocGen_GenerateAddressAndComittee.generateCommittee(objectId); 
    }

    global static String getNationality(String objectId) {
        Enrollment__c studyEnr = [
            SELECT Id, Enrollment_From_Documents__r.Candidate_Student__r.Nationality__c, Language_of_Main_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(studyEnr.Language_of_Main_Enrollment__c);
        Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Contact.Nationality__c);

        return translatedField.get(studyEnr.Enrollment_From_documents__r.Candidate_Student__r.Nationality__c);

    }

    global static String getIdentityDocument(String objectId) {
        Enrollment__c studyEnr = [
            SELECT Id, Enrollment_From_Documents__r.Candidate_Student__r.Identity_Document__c, Language_of_Main_Enrollment__c
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(studyEnr.Language_of_Main_Enrollment__c);
        Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Contact.Identity_Document__c);

        return translatedField.get(studyEnr.Enrollment_From_documents__r.Candidate_Student__r.Identity_Document__c);

    }

    global static String generatePhoneNumber(String objectId) {
        Enrollment__c studyEnr = [
            SELECT Id, Enrollment_From_Documents__r.Candidate_Student__r.Phone, Enrollment_From_Documents__r.Candidate_Student__r.MobilePhone
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        if (studyEnr.Enrollment_From_Documents__r.Candidate_Student__r.Phone != null) {
            return studyEnr.Enrollment_From_Documents__r.Candidate_Student__r.Phone;
        }
        else if (studyEnr.Enrollment_From_Documents__r.Candidate_Student__r.MobilePhone != null) {
            return studyEnr.Enrollment_From_Documents__r.Candidate_Student__r.MobilePhone;
        }
        else {
            return '';
        }
    }

    global static Enrollment__c getContact(String objectId) {
    	Enrollment__c docEnr = [
    		SELECT Id, Enrollment_From_Documents__r.Candidate_Student__r.MailingStreet, Enrollment_From_Documents__r.Candidate_Student__r.MailingCity, 
    		Enrollment_From_Documents__r.Candidate_Student__r.MailingPostalCode, Enrollment_From_Documents__r.Candidate_Student__r.MailingState, 
    		Enrollment_From_Documents__r.Candidate_Student__r.OtherStreet, Enrollment_From_Documents__r.Candidate_Student__r.OtherCity, 
    		Enrollment_From_Documents__r.Candidate_Student__r.OtherPostalCode, Enrollment_From_Documents__r.Candidate_Student__r.OtherState
    		FROM Enrollment__c
    		WHERE Id = :objectId
    	];

    	return docEnr;
    }

    global static Boolean compareAddresses(Enrollment__c docEnr) {
    	return docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingStreet == docEnr.Enrollment_From_Documents__r.Candidate_Student__r.OtherStreet
    		   && docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingCity == docEnr.Enrollment_From_Documents__r.Candidate_Student__r.OtherCity
    		   && docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingPostalCode == docEnr.Enrollment_From_Documents__r.Candidate_Student__r.OtherPostalCode
    		   && docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingState == docEnr.Enrollment_From_Documents__r.Candidate_Student__r.OtherState;
    }

    global static String generateMailingCity(String objectId) {
    	Enrollment__c docEnr = getContact(objectId);
    	return compareAddresses(docEnr) ? '' : escapeXML(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingCity);
    }

    global static String generateMailingStreet(String objectId) {
    	Enrollment__c docEnr = getContact(objectId);
    	return compareAddresses(docEnr) ? '' : escapeXML(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingStreet);
    }

    global static String generateMailingState(String objectId) {
    	Enrollment__c docEnr = getContact(objectId);
    	return compareAddresses(docEnr) ? '' : escapeXML(addressesTranslatonMap.get(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingState));
    }

    global static String generateOtherState(String objectId) {
        Enrollment__c docEnr = getContact(objectId);
        return escapeXML(addressesTranslatonMap.get(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.OtherState));
    }

    global static String generateMailingPostalCode(String objectId) {
    	Enrollment__c docEnr = getContact(objectId);
    	return compareAddresses(docEnr) ? '' : escapeXML(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.MailingPostalCode);
    }

    global static String generateCommittee(String objectId) {
    	Enrollment__c docEnr = [
    		SELECT Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Selection_Committee__c, 
    		Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Selection_Committee__c, Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Selection_Committee__c,
                Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Selection_Committee__c
    		FROM Enrollment__c
    		WHERE Id = :objectId
    	];

        if (docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Selection_Committee__c != null) {
            return docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Selection_Committee__c;
        }

        if (docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Selection_Committee__c != null) {
            return docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Selection_Committee__c;
        }

    	if (docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Selection_Committee__c != null) {
    		return docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Selection_Committee__c;
    	}

    	return docEnr.Enrollment_From_Documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Selection_Committee__c;
    }

    global static String escapeXML(String text) {
        if (text != null && text != '') {
            text = text.replaceAll('&', '&amp;');
        }

        return text;
    }
}