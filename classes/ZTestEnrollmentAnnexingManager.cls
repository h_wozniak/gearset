@isTest
private class ZTestEnrollmentAnnexingManager {


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------- STANDARD ANNEXING PROCESS ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testCreateAnnex() {
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), true);

        Discount__c discount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Enrollment__c = studyEnr.Id,
            Applied_Through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY,
            Applies_To__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
            Discount_Value__c = 100,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON,
            Applied__c = true
        ), true);

        Test.startTest();
        Id annexId = EnrollmentAnnexingManager.createAnnex(studyEnr.Id);

        System.assertNotEquals(null, annexId);

        Enrollment__c studyEnrAfter1 = [
            SELECT Id, Annexing__c, Annex_or_Related_Enr__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assert(!studyEnrAfter1.Annexing__c);
        Test.stopTest();
    }

    @isTest static void testFinalizeAnnex() {
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), true);

        Discount__c discount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Enrollment__c = studyEnr.Id,
            Applied_Through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY,
            Applies_To__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
            Discount_Value__c = 100,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON,
            Applied__c = true
        ), true);

        Test.startTest();
        Id annexId = EnrollmentAnnexingManager.createAnnex(studyEnr.Id);
        EnrollmentAnnexingManager.finalizeAnnex(studyEnr.Id, annexId);

        System.assertNotEquals(null, annexId);

        Enrollment__c studyEnrAfter1 = [
            SELECT Id, Annexing__c, Annex_or_Related_Enr__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];
        System.assert(studyEnrAfter1.Annexing__c);
        System.assertEquals(annexId, studyEnrAfter1.Annex_or_Related_Enr__c);

        Offer__c studyOffer = [
            SELECT Id, Candidates__c
            FROM Offer__c
            WHERE Id = :studyEnr.Course_or_Specialty_Offer__c
        ];
        System.assertEquals(1, studyOffer.Candidates__c);

        List<Discount__c> copiedDiscountList = [
            SELECT Id
            FROM Discount__c
            WHERE Enrollment__c = :annexId
        ];
        System.assertEquals(1, copiedDiscountList.size());

        EnrollmentAnnexingManager.finalizeAnnexingProcess(new Set<Id> { annexId });
        Test.stopTest();

        Enrollment__c studyEnrAfter2 = [
            SELECT Id, Annexing__c, Status__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assert(!studyEnrAfter2.Annexing__c);
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_RESIGNATION, studyEnrAfter2.Status__c);
    }



    @isTest static void testResignAnnex() {
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
        ), true);

        Discount__c discount = ZDataTestUtility.createManualDiscount(new Discount__c(
            Enrollment__c = studyEnr.Id,
            Applied_Through__c = CommonUtility.DISCOUNT_APPLIEDTHR_1STY,
            Applies_To__c = CommonUtility.DISCOUNT_APPLIESTO_TUITION,
            Discount_Value__c = 100,
            Discount_Kind__c = CommonUtility.DISCOUNT_KIND_AMOUNT,
            Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON,
            Applied__c = true
        ), true);

        Test.startTest();
        Id annexId = EnrollmentAnnexingManager.createAnnex(studyEnr.Id);
        EnrollmentAnnexingManager.finalizeAnnex(studyEnr.Id, annexId);

        System.assertNotEquals(null, annexId);

        Enrollment__c studyEnrAfter1 = [
            SELECT Id, Annexing__c, Annex_or_Related_Enr__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];
        System.assert(studyEnrAfter1.Annexing__c);
        System.assertEquals(annexId, studyEnrAfter1.Annex_or_Related_Enr__c);

        Offer__c studyOffer = [
            SELECT Id, Candidates__c
            FROM Offer__c
            WHERE Id = :studyEnr.Course_or_Specialty_Offer__c
        ];
        System.assertEquals(1, studyOffer.Candidates__c);

        List<Discount__c> copiedDiscountList = [
            SELECT Id
            FROM Discount__c
            WHERE Enrollment__c = :annexId
        ];
        System.assertEquals(1, copiedDiscountList.size());

        Enrollment__c annex = new Enrollment__c(
            Id = annexId,
            Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION,
            Resignation_Reason_List__c = CommonUtility.ENROLLMENT_RESREASON_ANNEXING,
            Resignation_Date__c = System.today()
        );
        update annex;

        Test.stopTest();

        Enrollment__c studyEnrAfter2 = [
            SELECT Id, Annex_or_Related_Enr__c, Annexing__c, Status__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assert(!studyEnrAfter2.Annexing__c);
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, studyEnrAfter2.Status__c);
        System.assertEquals(null, studyEnrAfter2.Annex_or_Related_Enr__c);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------ CHANGE OFFER ANNEXING PROCESS ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testCreateOfferChangeNotAnnex() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Contact cnt = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '10220318580',
            Phone = '123321234',
            Email = 'tom@monkey.com',
            OtherStreet = 'asd',
            OtherState = 'Lower Silesia',
            OtherCountry = 'Poland',
            OtherCity = 'asd',
            OtherPostalCode = '32-123',
            Nationality__c = 'polish',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED,
            School_Name__c = 'WSB Wrocław',
            The_Same_Mailling_Address__c = true
        ), true);


        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_DC,
            Candidate_Student__c = cnt.Id
        ), true);

        Offer__c newOffer = ZDataTestUtility.createCourseOffer(null, true);

        PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('offerId', newOffer.Id);
        ApexPages.CurrentPage().getParameters().put('enrollmentId', studyEnr.Id);


        Test.startTest();
        EnrollmentStudyController cntrl = new EnrollmentStudyController();
        System.assert(!cntrl.showContactLookup);

        cntrl.saveEnrollment();
        Test.stopTest();


        Id annexId = cntrl.ew.enrollment.Id;

        System.assertNotEquals(annexId, studyEnr.Id);

        Enrollment__c studyEnrAfter = [
            SELECT Id, Annexing__c, Status__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];

        System.assert(!studyEnrAfter.Annexing__c);
        System.assertEquals(CommonUtility.ENROLLMENT_STATUS_RESIGNATION, studyEnrAfter.Status__c);
    }

}