@isTest
private class ZTestRestServiceTrainingsHelper {

	@isTest static void test_handleCompany_withNIP() {
		Account company = ZDataTestUtility.createCompany(new Account(NIP__c = '3149014011'), true);

		String json = '{"trainingCode":"TS-00001-003","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","nip":"3149014011","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","companySize":"1-9 (micro)", "businessArea":"Administration"}';
		JSON2Apex result = JSON2Apex.parse(json);

		Id companyId = RestServiceTrainingsHelper.handleCompany(result);
		System.assertEquals(companyId, company.Id);

		List<Account> companies = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_COMPANY)
		];

		System.assertEquals(companies.size(), 1);
	}

	@isTest static void test_handleCompany_withoutNIP() {
		Account company = ZDataTestUtility.createCompany(new Account(NIP__c = '3149014011'), true);

		String json = '{"trainingCode":"TS-00001-003","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","nip":"9270477024","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","companySize":"1-9 (micro)", "businessArea":"Administration","companyName":"Testowa firma"}';
		JSON2Apex result = JSON2Apex.parse(json);

		Id companyId = RestServiceTrainingsHelper.handleCompany(result);
		System.assertNotEquals(companyId, company.Id);

		List<Account> companies = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_COMPANY)
		];

		System.assertEquals(companies.size(), 2);
	}

	@isTest static void test_createDuplicateNote() {
		Account company = ZDataTestUtility.createCompany(new Account(NIP__c = '3149014011'), true);

		String json = '{"trainingCode":"TS-00001-003","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","nip":"9270477024","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","companySize":"1-9 (micro)", "businessArea":"Administration","companyName":"Testowa firma"}';
		JSON2Apex result = JSON2Apex.parse(json);

		Id companyId = RestServiceTrainingsHelper.handleCompany(result);
		System.assertNotEquals(companyId, company.Id);

		List<Account> companies = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_COMPANY)
		];

		System.assertEquals(companies.size(), 2);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		RestServiceTrainingsHelper.createDuplicateNote(candidate, candidate.Id, result);
	}

	@isTest static void test_handleContactDuplicates() {
		Account company = ZDataTestUtility.createCompany(new Account(NIP__c = '3149014011'), true);

		String json = '{"trainingCode":"TS-00001-003","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","nip":"9270477024","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","companySize":"1-9 (micro)", "businessArea":"Administration","companyName":"Testowa firma"}';
		JSON2Apex result = JSON2Apex.parse(json);

		Id companyId = RestServiceTrainingsHelper.handleCompany(result);
		System.assertNotEquals(companyId, company.Id);

		List<Account> companies = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_COMPANY)
		];

		System.assertEquals(companies.size(), 2);

		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		RestServiceTrainingsHelper.handleContactDuplicates(new List<Contact> { candidate }, new List<Contact> { candidate }, result);		
	}
}