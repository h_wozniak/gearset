/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send emails with Study Enrollment confirmation reminder
**/

/**
*   To initiate the EnrollmentDocumentDeliveryEmailReminder execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('EnrollmentDocumentDeliveryEmailReminder', '0 0 8 * * ? *', new EnrollmentDocumentDeliveryEmailReminder());
**/


global class EnrollmentDocumentDeliveryEmailReminder implements Schedulable {

    global void execute(SchedulableContext sc) {
        // 3 days after enrollment (3 days before deadline) [has 7 or less days for delivery]
        DocDeliveryLessThanSevenDaysRemindBatch batchLessThanSevenDayReminder3Days = new DocDeliveryLessThanSevenDaysRemindBatch(3, 1);
        Database.executebatch(batchLessThanSevenDayReminder3Days, 2);
        
        // 6 days after enrollment (1 day before deadline) [has 7 or less days for delivery]
        DocDeliveryLessThanSevenDaysRemindBatch batchLessThanSevenDaysReminder6Days = new DocDeliveryLessThanSevenDaysRemindBatch(1, 2);
        Database.executebatch(batchLessThanSevenDaysReminder6Days, 2);

        // 7 days before deadline [has more than 7 days for delivery]
        DocDeliveryMoreThanSevenDaysRemindBatch batchMoreThanSevenDaysReminder7Days = new DocDeliveryMoreThanSevenDaysRemindBatch(7, 1);
        Database.executebatch(batchMoreThanSevenDaysReminder7Days, 2);
        
        // 4 days before deadline [has more than 7 days for delivery]
        DocDeliveryMoreThanSevenDaysRemindBatch batchMoreThanSevenDaysReminder4Days = new DocDeliveryMoreThanSevenDaysRemindBatch(4, 2);
        Database.executebatch(batchMoreThanSevenDaysReminder4Days, 2);
    }
}