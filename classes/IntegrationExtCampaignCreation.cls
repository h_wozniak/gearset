/**
*   @author         Grzegorz Długosz
*   @description    This class is used to create or/and update given list of campaigns
**/

@RestResource(urlMapping = '/ext_campaign_def_create')
global without sharing class IntegrationExtCampaignCreation {
	
	@HttpPost
    global static void createExtCampaign() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                IntegrationExtCampaignCreationHelper.createExtCampaign(parsedJson);
                res.statusCode = 200;
            }
            catch (IntegrationExtCampaignCreationHelper.InvalidFieldValueException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(800, e.getMessage());
            }
            catch (Exception e) {
                ErrorLogger.logAndMsg(e, CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationExtCampaignCreation.class.getName() + '\n' + jsonBody);
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }
        }
    }

    private static List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON> parse(String jsonBody) {
        return (List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationExtCampaignCreationHelper.ExtCampaignRequest_JSON>.class);
    }
            
}