public with sharing class LEX_Account_AddContactController {
	
	@AuraEnabled
	public static String getContactRecordTypeId() {
		return CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CONTACTPERSON);
	}
}