/**
* @author       Mateusz Pruszyński, editor Wojciech Słodziak
* @description  Controller for SendTrainingConfirmationMassEmail page. Allows user to send emails from specific Email Template folder.
**/
public with sharing class SendTrainingConfirmationMassEmail {

    private final String CUSTOM_MESSAGE = 'CUSTOM_MESSAGE';
    public Offer__c trainingSchedule { get; set; }

    public List<SelectOption> emailTemplates { get; set; }
    public List<String> templatesListVF { get; set; }
    public Boolean isVFEmail { get; set; }
    public String selectedTemplate { get; set; }

    public List<Recipient_Wrapper> targetRecipients { get; set; }

    // for rendering purposes
    public Boolean beforeSend { get; set; }
    public String mBody { get; set; }
    public String mSubject { get; set; }


    public SendTrainingConfirmationMassEmail(ApexPages.StandardController stdController) {
        templatesListVF = new List<String>();
        trainingSchedule = (Offer__c)stdController.getRecord();
        emailTemplates = getTemplates();

        beforeSend = true;

        targetRecipients = getParticipants();

        if (targetRecipients.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.msg_info_NoneEmail));
            beforeSend = false;
        }
    }

    public PageReference sendEmail() {
        if (selectedTemplate == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.msg_error_NoMailTemplateSelected));
            return null;
        }

        System.debug('targetRecipients ' + targetRecipients);
        if (!anyRecipientChecked()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.msg_error_NoRecipientsChecked));
            return null;
        }


        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        for (Recipient_Wrapper recipient : targetRecipients) {
            if (recipient.checked) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setWhatId(recipient.resultsId);
                mail.setTargetObjectId(recipient.recipientId);

                if (selectedTemplate != CUSTOM_MESSAGE) {
                    mail.setTemplateId(selectedTemplate);
                }
                else {
                    mail.setPlainTextBody(mBody);
                    mail.setSubject(mSubject);
                }
                mailList.add(mail);
            }
        }

        try {
            Messaging.sendEmail(mailList);
            beforeSend = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.meg_info_MessagesSent));
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        return null;
    }

    public List<SelectOption> getTemplates() {
        List<SelectOption> emailTemplateOptions = new List<SelectOption>();
        Set<String> emailDeveloperNames = new Set<String> {
                'Przypomnienie_o_p_atno_ci_za_szkolenie',
                'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci',
                'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci_do_11_dni_przed',
                'Email_po_rekrutacji_na_szkolenie_z_danych_do_p_atno_ci_10_3_dni_przed',
                'Email_po_rekrutacji_na_szkolenie_z_danych_d    o_p_atno_ci_2_1_dni_przed'
        };

        emailTemplateOptions.add(new SelectOption('', '--- ' + Label.title_SelectTemplate + ' ---'));

        List<EmailTemplate> emailTemplates = [
                SELECT Id, Name, TemplateType 
                FROM EmailTemplate 
                WHERE IsActive = true AND Folder.DeveloperName = :CommonUtility.EMAIL_TEMPLATE_FOLDER_NAME_GRUPA_WSB_TRAININGS
                AND DeveloperName NOT IN :emailDeveloperNames
        ];

        for (EmailTemplate et : emailTemplates) {
            emailTemplateOptions.add(new SelectOption(et.Id, et.Name));
            if(et.TemplateType == 'visualforce')
                templatesListVF.add(et.Id);    
        }

        emailTemplateOptions.add(new SelectOption(CUSTOM_MESSAGE, Label.title_customMessage));

        return emailTemplateOptions;
    }

    public List<Recipient_Wrapper> getParticipants() {
        List<Recipient_Wrapper> participantList = new List<Recipient_Wrapper>();

        List<Enrollment__c> participantResultList = [
                SELECT Id, Participant__c, Participant__r.Email, Participant__r.Name, toLabel(Status__c)
                FROM Enrollment__c
                WHERE Training_Offer_for_Participant__c = :trainingSchedule.Id
                AND Participant__r.Email != null
        ];

        for (Enrollment__c participant : participantResultList) {
            participantList.add(new Recipient_Wrapper(
                    participant.Id,
                    participant.Participant__c,
                    participant.Participant__r.Name,
                    participant.Participant__r.Email,
                    participant.Status__c
            )
            );
        }

        return participantList;
    }

    private Boolean anyRecipientChecked() {
        for (Recipient_Wrapper recipient : targetRecipients) {
            if (recipient.checked) {
                return true;
            }
        }

        return false;
    }



    private void checkIfVFTemplate() {
        if (templatesListVF.contains(selectedTemplate)) {
           isVFEmail = true;
        } else  {
            isVFEmail = false;
        }
    }

    /* preview */
    private Messaging.SingleEmailMessage prepareSampleEmail() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(selectedTemplate);
        if (targetRecipients.size() > 0) {
            System.debug('resultsId' + targetRecipients[0].resultsId);
            System.debug('recipientId' + targetRecipients[0].recipientId);
            mail.setWhatId(targetRecipients[0].resultsId);
            mail.setTargetObjectId(targetRecipients[0].recipientId);
        }

        return mail;
    }

    public void loadTemplateForPreview() {
        if (selectedTemplate == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.msg_error_NoMailTemplateSelected));
            return;
        }

        if (selectedTemplate != CUSTOM_MESSAGE) {
            checkIfVFTemplate();
            Messaging.SingleEmailMessage m = prepareSampleEmail();
            Savepoint sp = Database.setSavepoint();
            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        m
                });
                Database.rollback(sp);
                if(isVFEmail) {
                    mBody = m.getHtmlBody();
               		System.debug('mbody ' + mBody);
                }
                    
                else
                    mBody = m.getPlainTextBody();
                mSubject = m.getSubject();
            } catch (Exception ex) {
                ErrorLogger.log(ex);
            }
        }
        else {
            mBody = null;
            mSubject = null;
        }
    }




    /* ----- WRAPPERS ----- */

    public class Recipient_Wrapper {
        public Id resultsId { get; set; }
        public Id recipientId { get; set; }
        public String recipientName { get; set; }
        public String recipientEmail { get; set; }
        public String recipientStatus { get; set; }
        public Boolean checked { get; set; }

        public Recipient_Wrapper(Id resId, Id rId, String name, String email, String status) {
            this.checked = false;
            resultsId = resId;
            recipientId = rId;
            recipientName = name;
            recipientEmail = email;
            recipientStatus = status;
        }
    }
}