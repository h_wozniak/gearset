/**
* @author       Sebastian Łasisz
* @description  Class to send Workshop Proposal to High Schools (triggered from button on High School page)
**/

public without sharing class EmailManager_WorkshopProposal {

    @TestVisible private static final String WORKFLOW_TRIGGER_NAME = 'HIGH_SCHOOL_WORKSHOP_PROPOSAL';
    @TestVisible private static final String WORKSHOP_PROPOSAL_DEVELOPER_NAME = 'Mail_do_szk_z_propozycj_warsztat_w';

    /**
     * Method for sending Workshop Proposal to High Schools
     * @author Sebastian Lasisz
     *
     * @param highSchoolIds Set of Ids to determine and send workshop proposals.
     *
     * @return Boolean value whether the operation was successful.
     */
    public static Boolean sendWorkshopProposal(Set<Id> highSchoolIds) {
        List<Account> highSchoolsList = [
                SELECT Id, Contact_Person__c
                FROM Account
                WHERE Id IN :highSchoolIds
                AND Contact_Person__r.Email != null
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)
        ];

        List<EmailTemplate> emailTemplates = [
                SELECT Id, Name
                FROM EmailTemplate
                WHERE DeveloperName = :WORKSHOP_PROPOSAL_DEVELOPER_NAME
        ];

        EmailTemplate emailTemplate = emailTemplates.get(0);

        List<Task> tasksToInsert = new List<Task>();
        List<Account> accountWithFailues = new List<Account>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();

        for (Account highschool : highSchoolsList) {
            if (WORKFLOW_TRIGGER_NAME != null) {
                recordIdToWorkflowTriggerName.put(highschool.Id,  WORKFLOW_TRIGGER_NAME);

                String emailTemplateId = emailTemplate.Id;

                tasksToInsert.add(EmailManager.createEmailTask(
                        Label.msg_sentWorkshopEmail,
                        highschool.Id,
                        highschool.Contact_Person__c,
                        EmailManager.prepareFakeEmailBody(emailTemplateId, highschool.Id),
                        null
                ));
            } else {
                accountWithFailues.add(highschool);
            }
        }

        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }


        if (!accountWithFailues.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_WorkshopProposal.class.getName()
                    + ' ' + JSON.serialize(accountWithFailues));
            return false;
        }

        return true;
    }
}