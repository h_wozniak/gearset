/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store methods related to Documents (Catalogs)
**/

public without sharing class CatalogManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- CATALOG API NAMES ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<String, String> consentAPINameToCatalogNameMap;
    private static Map<String, String> catalogNameToConsentAPINameMap;
    private static Map<String, String> catalogADONameToConsentAPINameMap;
    private static Map<String, String> getUniversalConsentCatalog(Boolean forceSoql, String mapType) {
        if ((consentAPINameToCatalogNameMap == null && catalogNameToConsentAPINameMap == null 
            && catalogADONameToConsentAPINameMap == null) || forceSoql) {
            List<Catalog__c> catalogList = [
                SELECT Id, Name, Consent_API_Name__c, Consent_ADO_API_Name__c
                FROM Catalog__c
                WHERE Active_From__c <= :System.today()
                AND (Active_To__c = null OR Active_To__c >= : System.today())
                AND Consent_API_Name__c != null
            ];

            consentAPINameToCatalogNameMap = new Map<String, String>();
            catalogNameToConsentAPINameMap = new Map<String, String>();
            catalogADONameToConsentAPINameMap = new Map<String, String>();

            for (Catalog__c ctlg : catalogList) {
                consentAPINameToCatalogNameMap.put(ctlg.Consent_API_Name__c, ctlg.Name);
                catalogNameToConsentAPINameMap.put(ctlg.Name, ctlg.Consent_API_Name__c);
                catalogADONameToConsentAPINameMap.put(ctlg.Name, ctlg.Consent_ADO_API_Name__c);
            }
        }

        if (mapType == 'ConsentAPICatalogName') {
            return consentAPINameToCatalogNameMap;
        }
        else if (mapType == 'CatalogNameConsentAPI') {
            return catalogNameToConsentAPINameMap;
        }
        else if (mapType == 'CatalogNameADOAPI') {
            return catalogADONameToConsentAPINameMap;
        }

        return null;
    }

    /* Sebastian Łasisz */
    public static String getCatalogByAPIName(String name) {
        String resultId = CatalogManager.getUniversalConsentCatalog(false, 'ConsentAPICatalogName').get(name);

        if (resultId == null) {
            resultId = CatalogManager.getUniversalConsentCatalog(true, 'ConsentAPICatalogName').get(name);
        }
        
        return resultId;
    }

    /* Sebastian Łasisz */
    public static String getApiNameFromConsentName(String name) {
        String resultId = CatalogManager.getUniversalConsentCatalog(false, 'CatalogNameConsentAPI').get(name);

        if (resultId == null) {
            resultId = CatalogManager.getUniversalConsentCatalog(true, 'CatalogNameConsentAPI').get(name);
        }
        
        return resultId;
    }

    /* Sebastian Łasisz */
    public static String getApiADONameFromConsentName(String name) {
        String resultId = CatalogManager.getUniversalConsentCatalog(false, 'CatalogNameADOAPI').get(name);

        if (resultId == null) {
            resultId = CatalogManager.getUniversalConsentCatalog(true, 'CatalogNameADOAPI').get(name);
        }
        
        return resultId;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- CATALOG LOADUP ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */



    private static Map<String, Id> catalogNameToIdMap;
    private static Map<String, Id> getCatalogIdsMap(Boolean forceSoql) {
        if (catalogNameToIdMap == null || forceSoql) {
            List<Catalog__c> catalogList = [
                SELECT Id, Name 
                FROM Catalog__c
            ];

            catalogNameToIdMap = new Map<String, Id>();

            for (Catalog__c ctlg : catalogList) {
                catalogNameToIdMap.put(ctlg.Name, ctlg.Id);
            }
        }
        return catalogNameToIdMap;
    }

    /* Wojciech Słodziak */
    // universal method for getting Definition record from Catalog by Name 
    public static Id getCatalogByName(String name) {
        Id resultId = CatalogManager.getCatalogIdsMap(false).get(name);

        if (resultId == null) {
            resultId = CatalogManager.getCatalogIdsMap(true).get(name);

            if (resultId == null) {
                ErrorLogger.configMsg(CommonUtility.CATALOG_UNDEFINED_ERROR + ' ' + name);
            }
        }
        
        return resultId;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- DOCUMENTS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Sebastian Łasisz */
    // Get Attestation Document Id based on Catalog Name
    public static Id getDocumentAttestationId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ATTESTATION);
    }

    /* Sebastian Łasisz */
    // Get Agreement Document Id based on Catalog Name
    public static Id getDocumentAgreementId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_AGREEMENT);
    }

    /* Sebastian Łasisz */
    // Get Agreement Document Id based on Catalog Name
    public static Id getDocumentAgreementPlId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_AGREEMENT_PL);
    }
    
    /* Sebastian Łasisz */
    // Get Agreement Document Id for Unenrolled based on Catalog Name
    public static Id getDocumentAgreementUnenrolledId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED);
    }
    
    /* Sebastian Łasisz */
    // Get Agreement Document Id for Unenrolled based on Catalog Name
    public static Id getDocumentAgreementUnenrolledPLId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_AGREEMENT_UNENROLLED_PL);
    }
    
    /* Sebastian Łasisz */
    // Get Oath Document Id based on Catalog Name
    public static Id getDocumentOathId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_OATH);
    }
    
    /* Sebastian Łasisz */
    // Get Oath Document Id based on Catalog Name
    public static Id getDocumentOathPlId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_OATH_PL);
    }

    /* Sebastian Łasisz */
    // Get A Level Certificate Id based on Catalog Name
    public static Id getALevelCertificateId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ALEVEL_CERTIFICATE);
    }
    
    /* Sebastian Łasisz */
    // Get Acceptance Decision Document Id based on Catalog Name
    public static Id getDocumentAcceptanceId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ACCEPTANCE);
    }
    
    /* Sebastian Łasisz */
    // Get Acceptance Decision Document Id based on Catalog Name
    public static Id getDocumentAcceptancePLId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ACCEPTANCE_PL);
    }
    
    /* Sebastian Łasisz */
    // Get Attestation Diploma Document Id based on Catalog Name
    public static Id getDocumentAttestationDiplomaId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ATTESTATION_DIPLOMA);
    }
    
    /* Sebastian Łasisz */
    // Get Certificate Of Professional Experience Id based on Catalog Name
    public static Id getCertificateOfProfessionalExperienceId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_CERTIFICATE_OF_PROFESSIONAL_EXPERIENCE);
    }

    /* Sebastian Łasisz */
    // Get Entry In Business Register Id based on Catalog Name
    public static Id getEntryInBusinessRegisterId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ENTRY_BUSINESS_REGISTER);
    }
    
    /* Sebastian Łasisz */
    // Get Diploma Document Id based on Catalog Name
    public static Id getDocumentDiplomaId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_DIPLOMA);
    }

    /* Wojciech Słodziak */
    // Get Entry Fee Payment Confirmation Document Id based on Catalog Name
    public static Id getDocumentEntryPaymentConfId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_ENTRYFEE_PAYCONF);
    }

    /* Sebastian Łasisz */
    // Get Personal Questionnare Document Id Based on Catalog Name
    public static Id getDocumentPersonalQuestionnareId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE);
    }

    /* Sebastian Łasisz */
    // Get Personal Questionnare Document Id Based on Catalog Name
    public static Id getPLDocumentPersonalQuestionnareId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE_PL);
    }
    /* Sebastian Łasisz */
    // Get Company Discount Document Id Based on Catalog Name
    public static Id getCompanyDiscoutnDocumentId() {
        return CatalogManager.getCatalogByName(RecordVals.CATALOG_DOCUMENT_COMPANY_DISCOUNT);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- VALIDATIONS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // Catalog name duplicate detection
    public static void detectNameDuplicates(List<Catalog__c> catalogsToCheckDup) {
        Set<String> nameList = new Set<String>();
        for (Catalog__c o : catalogsToCheckDup) {
            nameList.add(o.Name);
        }

        List<Catalog__c> potentialDuplicates = [
            SELECT Id, Name 
            FROM Catalog__c 
            WHERE Name IN :nameList FOR UPDATE
        ];

        for (Catalog__c o : catalogsToCheckDup) {
            for (Catalog__c dup : potentialDuplicates) {
                if (o.Id != dup.Id && o.Name == dup.Name) {
                    if(!CommonUtility.isLightningEnabled()) {
                        o.addError(' ' + Label.msg_error_DupFound + ': <a target="_blank" href="/' + dup.Id + '">' + dup.Name + '</a>', false);
                    } else {
                        o.addError(' ' + Label.msg_error_DupFound + ': dup.Id' + ' - ' + dup.Name, false);
                    }
                }
            }
        }
    }

}