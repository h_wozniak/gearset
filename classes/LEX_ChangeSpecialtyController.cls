public with sharing class LEX_ChangeSpecialtyController {

    @AuraEnabled
    public static String getEnrollmentStatus(Id studyEnrId) {
        Enrollment__c studyEnrollment = [
                SELECT Id, Status__c
                FROM Enrollment__c
                WHERE Id = :studyEnrId
        ];

        return studyEnrollment.Status__c;
    }

    @AuraEnabled
    public static Boolean hasEditAccess(Id recordId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :recordId
        ];

        return ura.HasEditAccess;
    }

    @AuraEnabled
    public static String performSave(Id recordId, Id specialtyId, Id specializationId) {
        Enrollment__c studyEnr = new Enrollment__c();
        studyEnr.Id = recordId;
        if (specialtyId != null) {
            studyEnr.Initial_Specialty_Declaration__c = specialtyId;
        }

        if (specializationId != null) {
            studyEnr.Specialization__c = specializationId;
        }

        try {
            update studyEnr;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            return CommonUtility.trimErrorMessage(e.getMessage());
        }

        return null;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- SPECIALTIES  ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @AuraEnabled
    public static List<PickListValue_Wrapper> getPicklistSpecialtyMap(Id recordId) {
        Enrollment__c studyEnr = [
                SELECT Id, Course_or_Specialty_Offer__c, Degree__c
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        return prepareSpecialtiesList(studyEnr.Course_or_Specialty_Offer__c, studyEnr.Degree__c);
    }

    private static List<PickListValue_Wrapper> prepareSpecialtiesList(Id offerId, String degree) {
        List<PickListValue_Wrapper> specialitesOffers = new List<PickListValue_Wrapper>();

        List<Offer__c> specialties = [
                SELECT Id, Name, Specialty_Trade_Name__c,
                (SELECT Id FROM Specializations__r)
                FROM Offer__c
                WHERE Course_Offer_from_Specialty__c = :offerId
                AND Configuration_Status_Specialty__c = true AND Active_from__c <= TODAY
                AND Active_to__c >= TODAY ORDER BY Name
        ];

        for (Offer__c specialty : specialties) {
            if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
                if (specialty.Specializations__r.size() > 0) {
                    specialitesOffers.add(
                            new PickListValue_Wrapper(specialty.Name + ', ' + specialty.Specialty_Trade_Name__c, specialty.Id)
                    );
                }
            }
            else {
                specialitesOffers.add(
                        new PickListValue_Wrapper(specialty.Name + ', ' + specialty.Specialty_Trade_Name__c, specialty.Id)
                );
            }
        }

        return specialitesOffers;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ SPECIALIZATIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @AuraEnabled
    public static List<PickListValue_Wrapper> getPicklistSpecializationMap(Id recordId, Id selectedSpecialty) {
        Enrollment__c studyEnr = [
                SELECT Id, Course_or_Specialty_Offer__c, Degree__c, Course_or_Specialty_Offer__r.RecordTypeId
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        return getPostGraduateOfferProductList(studyEnr.Course_or_Specialty_Offer__c, studyEnr.Degree__c, selectedSpecialty, studyEnr.Course_or_Specialty_Offer__r.RecordTypeId);
    }

    private static List<PickListValue_Wrapper> getPostGraduateOfferProductList(Id offerId, String degree, Id specialtyOffer, Id recordTypeId) {
        List<PickListValue_Wrapper> selectOptionList = new List<PickListValue_Wrapper>();
        if (degree == CommonUtility.OFFER_DEGREE_II_PG || degree == CommonUtility.OFFER_DEGREE_II || degree == CommonUtility.OFFER_DEGREE_PG || degree == CommonUtility.OFFER_DEGREE_MBA) {
            for (Offer__c productOnOffer : preparePostGraduateOfferProductList(offerId, degree)) {
                for (Offer__c spec : productOnOffer.Specializations__r) {
                    selectOptionList.add(
                            new PickListValue_Wrapper(spec.Specialization__r.Name + ', ' + spec.Specialization__r.Trade_Name__c, spec.Specialization__c)
                    );
                }
            }

            if (recordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER) && specialtyOffer != null) {
                selectOptionList.addAll(preparePostGraduateOfferFromSpecialty(specialtyOffer));
            }
        }

        return selectOptionList;
    }

    private static List<Offer__c> preparePostGraduateOfferProductList(Id offerId, String degree) {
        List<Offer__c> postGraduateOffers = new List<Offer__c>();

        if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
            List<Offer__c> postGraduateOfferProductsOnMainOffer = [
                    SELECT Id, Name, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c,
                    (SELECT Id, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c FROM Specializations__r),
                    (SELECT Id, Name, Trade_Name__c FROM Specialties__r)
                    FROM Offer__c
                    WHERE Id = :offerId
            ];

            Map<Id, Id> postGraduateProducts = new Map<Id, Id>();

            for (Offer__c postGraduateOfferProductOnMainOffer : postGraduateOfferProductsOnMainOffer) {
                if (postGraduateOfferProductOnMainOffer.Specializations__r.size() > 0) {
                    postGraduateProducts.put(postGraduateOfferProductOnMainOffer.Specialization__c, postGraduateOfferProductOnMainOffer.Id);
                }
            }

            for (Id postGraduateId : postGraduateProducts.values()) {
                for (Offer__c postGraduateOfferProductOnMainOffer : postGraduateOfferProductsOnMainOffer) {
                    if (postGraduateOfferProductOnMainOffer.Id == postGraduateId) {
                        postGraduateOffers.add(postGraduateOfferProductOnMainOffer);
                    }
                }
            }
        }

        return postGraduateOffers;
    }

    private static List<PickListValue_Wrapper> preparePostGraduateOfferFromSpecialty(Id specialtyOfferId) {
        Offer__c postGraduateOfferProductOnMainOffer = [
                SELECT Id, Name, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c,
                (SELECT Id, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c FROM Specializations__r)
                FROM Offer__c
                WHERE Id = :specialtyOfferId
        ];

        List<PickListValue_Wrapper> selectOptionList = new List<PickListValue_Wrapper>();
        for (Offer__c spec : postGraduateOfferProductOnMainOffer.Specializations__r) {
            selectOptionList.add(
                    new PickListValue_Wrapper(spec.Specialization__r.Name + ', ' + spec.Specialization__r.Trade_Name__c, spec.Specialization__c)
            );
        }

        return selectOptionList;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class PickListValue_Wrapper {
        @AuraEnabled  public String label {get; set;}
        @AuraEnabled  public String value {get; set;}

        public PickListValue_Wrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

}