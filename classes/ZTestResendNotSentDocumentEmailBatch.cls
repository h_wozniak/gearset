@isTest
private class ZTestResendNotSentDocumentEmailBatch {

    @isTest static void test_ResendNotSentDocumentEmailBatch() {
        //Test.startTest();
        ZDataTestUtility.prepareCatalogData(true, true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        //Test.stopTest();

        Enrollment__c queriedStudyEnr = [
            SELECT Id, RequiredDocListEmailedCount__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];  

        System.assertEquals(queriedStudyEnr.RequiredDocListEmailedCount__c, 0);

        Test.startTest();
        ResendNotSentDocumentEmailBatch sendDocumentEmailBatch = new ResendNotSentDocumentEmailBatch();
        Database.executebatch(sendDocumentEmailBatch, 1);
        Test.stopTest();

        Enrollment__c queriedStudyEnrAft = [
            SELECT Id, RequiredDocListEmailedCount__c
            FROM Enrollment__c
            WHERE Id = :studyEnr.Id
        ];  

        System.assertEquals(queriedStudyEnrAft.RequiredDocListEmailedCount__c, 0);
    }

    @isTest static void test_ResendNotSentDocumentEmailBatch_withAttachments() {
        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT
        ), true);

        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_OATH
        ), true);

        ZDataTestUtility.createDocument(new Catalog__c(
            Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE
        ), true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
            Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH,
            Dont_send_automatic_emails__c = true
        ), true);

        Enrollment__c studyDocEnrAgr = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentAgreementId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);
        
        Enrollment__c studyDocEnrAgrOath = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentOathId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);
        
        Enrollment__c studyQuestionnare = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Document__c = CatalogManager.getDocumentPersonalQuestionnareId(), 
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);



        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            EmailTemplate defaultEmailTemplate = new EmailTemplate();
            defaultEmailTemplate.isActive = true;
            defaultEmailTemplate.Name = 'x';
            defaultEmailTemplate.DeveloperName = EmailManager_DocumentListEmail.DocListEmTemplates.X23ac1_StudyEnrDocList.name();
            defaultEmailTemplate.TemplateType = 'text';
            defaultEmailTemplate.FolderId = UserInfo.getUserId();
            defaultEmailTemplate.Subject = 'x';

            insert defaultEmailTemplate;
        }

        //test with one attachment (generated document)
        Attachment att1 = new Attachment(
            Name = 'test-att1',
            Body = Blob.valueOf('body'),
            ParentId = studyDocEnrAgr.Id
        );
        insert att1;

        Enrollment__c studyEnrAfter1 = [SELECT RequiredDocListEmailedCount__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(0, studyEnrAfter1.RequiredDocListEmailedCount__c);

        //test with two attachments (generated documents)
        Attachment att2 = new Attachment(
            Name = 'test-att2',
            Body = Blob.valueOf('body'),
            ParentId = studyDocEnrAgrOath.Id
        );
        insert att2;

        //test with three attachments (generated documents)
        Attachment att3 = new Attachment(
            Name = 'test-att3',
            Body = Blob.valueOf('body'),
            ParentId = studyQuestionnare.Id
        );
        insert att3;

        ResendNotSentDocumentEmailBatch sendDocumentEmailBatch = new ResendNotSentDocumentEmailBatch();
        Database.executebatch(sendDocumentEmailBatch, 1);
        Test.stopTest();   
        
        Enrollment__c studyEnrAfter2 = [SELECT RequiredDocListEmailedCount__c FROM Enrollment__c WHERE Id = :studyEnr.Id];

        System.assertEquals(0, studyEnrAfter2.RequiredDocListEmailedCount__c);     

    }
}