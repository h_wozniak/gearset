/**
* @author       Wojciech Słodziak
* @description  Class to discover ORG Wide email address using custom settings
**/

public class OrgWideEmailManager {
    
    private static List<OrgWideEmailAddress> orgWideEmailList;
    
    private static List<OrganizationWideAddressesConfig__c> orgWideAddressConfigList;

    // returns Email Address Id based on criteria
    private static Integer errorMsgCount = 0;
    public static OrgWideEmailAddress getOrgWideEmailAddress(Boolean forForeigner, String universityName, String degree) {
        OrgWideEmailAddress resultAddress;

        if (orgWideAddressConfigList == null) {
            orgWideAddressConfigList = OrganizationWideAddressesConfig__c.getAll().values();
        }

        if (orgWideEmailList == null) {
            orgWideEmailList = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress];
        }
        
        
        //find foreigner address first
        if (forForeigner) {
            for (OrganizationWideAddressesConfig__c config : orgWideAddressConfigList) {
                if (config.Foreigner__c && universityName == config.University_Name__c) {
                    resultAddress = OrgWideEmailManager.getOrgWideEmailAddressByAddress(config.Email_Address__c);
                }
            }
        }

        //find for degree
        if (!forForeigner) {
            for (OrganizationWideAddressesConfig__c config : orgWideAddressConfigList) {
                if (degree == config.Degree__c && universityName == config.University_Name__c && !config.Foreigner__c) {
                    resultAddress = OrgWideEmailManager.getOrgWideEmailAddressByAddress(config.Email_Address__c);
                }
            }
        }

        //find global
        if (!forForeigner && resultAddress == null) {
            for (OrganizationWideAddressesConfig__c config : orgWideAddressConfigList) {
                if (universityName == config.University_Name__c && config.Degree__c == null && !config.Foreigner__c) {
                    resultAddress = OrgWideEmailManager.getOrgWideEmailAddressByAddress(config.Email_Address__c);
                }
            }
        }

        //find global for WSB
        if (resultAddress == null) {
            resultAddress = OrgWideEmailManager.getOrgWideEmailAddressByDisplayName(CommonUtility.INTEGRATION_USERNAME);
        }

        return resultAddress;
    }

    private static OrgWideEmailAddress getOrgWideEmailAddressByAddress(String address) {
        OrgWideEmailAddress result;

        for (OrgWideEmailAddress orgWideAddress : orgWideEmailList) {
            if (orgWideAddress.Address == address) {
                result = orgWideAddress;
            }
        }

        if (result == null && OrgWideEmailManager.errorMsgCount == 0) {
            ErrorLogger.configMsg(CommonUtility.ORG_WIDE_EMAIL_UNDEFINED_ERROR + ' ' + address);
            OrgWideEmailManager.errorMsgCount++;
        }

        return result;
    }
    
    private static OrgWideEmailAddress getOrgWideEmailAddressByDisplayName(String dispName) {
        OrgWideEmailAddress result;

        for (OrgWideEmailAddress orgWideAddress : orgWideEmailList) {
            if (orgWideAddress.DisplayName == dispName) {
                result = orgWideAddress;
            }
        }

        if (result == null && OrgWideEmailManager.errorMsgCount == 0) {
            ErrorLogger.configMsg(CommonUtility.ORG_WIDE_EMAIL_UNDEFINED_ERROR + ' ' + dispName);
            OrgWideEmailManager.errorMsgCount++;
        }

        return result;
    }
}