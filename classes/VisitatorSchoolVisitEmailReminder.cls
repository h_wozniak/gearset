/**
 * Created by martyna.stanczuk on 2018-05-23.
 */

global class VisitatorSchoolVisitEmailReminder implements Schedulable {

    global void execute(SchedulableContext sc) {
        SchoolVisitEmailReminderBatch batch1Day = new SchoolVisitEmailReminderBatch(1);
        Database.executebatch(batch1Day, 2);
    }

}