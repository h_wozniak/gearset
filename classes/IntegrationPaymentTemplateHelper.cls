/**
*   @author       Wojciech Słodziak
*   @description  Class for generation of Payment Templates for Integartion with Experia.
**/

public without sharing class IntegrationPaymentTemplateHelper {



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- CONFIG ----------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    final static public Map<Boolean, String> tuitionTypeDictMap = new Map<Boolean, String> {
        false => 'st', // fixed
        true => 'zm'   // graded
    };

    final static Map<String, String> pbTypeDictMap = new Map<String, String> {
        CommonUtility.OFFER_PBTYPE_STANDARD => 'pl',
        CommonUtility.OFFER_PBTYPE_FOREIGNERS => 'cu'
    };

    final static String DATE_FORMAT_DESC = 'yyMMdd';
    final static String DATE_FORMAT_POSITION = 'yyyy-MM-dd';

    private static Integer getTuitionInstTitleCode(Integer instVariantThisYear) {
        if (instVariantThisYear == 1) {
            return 4; // TODO Workaround below - Experia does not handle this type at the moment
        } else
        if (instVariantThisYear == 2) {
            return 3;
        }
        return 2;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- INVOKE METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String buildPriceBookPaymentTemplatesJSON(Set<Id> pbIds) {
        List<Offer__c> priceBookList = [
            SELECT Id, Name, Number_of_Semesters_formula__c, Entry_fee__c, Onetime_Price__c, RecordTypeId, Schedule_Model__c, Schedule_Model_2__c, Installment_Variants__c,
            Graded_tuition__c, Price_Book_Type__c, PriceBook_From__c, University_Name__c, Offer_from_Price_Book__r.Name,
            Offer_from_Price_Book__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, Offer_from_Price_Book__r.Degree__c,
            Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
            Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Mode__c, Offer_from_Price_Book__r.Mode__c, 
            Offer_from_Price_Book__r.Kind__c, Offer_from_Price_Book__r.Trade_Name__c, Offer_from_Price_Book__r.Specialty_Trade_Name__c, 
            Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Trade_Name__c, Offer_from_Price_Book__r.Short_Name__c, 
            Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Short_Name__c,
                (SELECT Id, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, 
                Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c WHERE Id IN :pbIds
        ];

        List<PriceBook_JSON> templateList = IntegrationPaymentTemplateHelper.buildPaymentTemplates(priceBookList);

        return JSON.serialize(templateList);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE BUILDER ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static List<PriceBook_JSON> buildPaymentTemplates(List<Offer__c> priceBookList) {
        List<PriceBook_JSON> pbJSONList = new List<PriceBook_JSON>();

        Map<String, PaymentScheduleDeadlineDesignator> psddMap = new Map<String, PaymentScheduleDeadlineDesignator>();

        for (Offer__c priceBook : priceBookList) {
            pbJSONList.add(
                IntegrationPaymentTemplateHelper.buildPriceBook(priceBook, psddMap)
            );
        }

        return pbJSONList;
    }

    // method returns PriceBook_JSON with Templates and Positions
    private static PriceBook_JSON buildPriceBook(Offer__c priceBook, Map<String, PaymentScheduleDeadlineDesignator> psddMap) {
        PriceBook_JSON pbJSON = new PriceBook_JSON();

        /* required variables */
        Date studyStartDate = (priceBook.Offer_from_Price_Book__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null? 
                               priceBook.Offer_from_Price_Book__r.University_Study_Offer_from_Course__r.Study_Start_Date__c :
                               priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c);
        String offerKind = priceBook.Offer_from_Price_Book__r.Kind__c;
        Integer semesterCount = Integer.valueOf(priceBook.Number_of_Semesters_formula__c);
        Integer yearCount = Integer.valueOf(Math.ceil(Double.valueOf(semesterCount) / 2));
        Boolean oddSemesters = Math.mod(semesterCount, 2) == 1;
        Boolean isWinterEnrollment = PaymentScheduleManager.isWinterEnrollment(studyStartDate);
        String idNameString = IntegrationPaymentTemplateHelper.getTemplateIdName(priceBook);
        String degree = priceBook.Offer_from_Price_Book__r.Degree__c;
        Map<Integer, Offer__c> variantToInstConfigMap = PriceBookManager.getInstallmentConfigsMapByInstallmentVariant(priceBook.InstallmentConfigs__r);
        /* eof required variables */


        pbJSON.pricebook_id = priceBook.Id;
        pbJSON.product_id = priceBook.Offer_from_Price_Book__r.Name;

        Set<String> pbVariants = CommonUtility.getOptionsFromMultiSelect(priceBook.Installment_Variants__c);

        for (String variant : pbVariants) {
            // WORKAROUND - Experia is not ready to receive Yearly or One time tuition fees - [WHERE Installment_Variant__c != '1'] TODO
            for (Integer year = 0 ; year < yearCount; year++) {
                Boolean isLastYear = false;
                if ((year+1) == yearCount) {
                    isLastYear = true;
                }
                //if (Integer.valueOf(variant) != 1) {
                if (!(isLastYear && oddSemesters && Integer.valueOf(variant) == 1)
                    && !(isLastYear && Integer.valueOf(variant) == 12)) {
                    pbJSON.templates.add(
                        IntegrationPaymentTemplateHelper.buildTemplate(
                            isLastYear, priceBook, offerKind, degree, studyStartDate, Integer.valueOf(variant), semesterCount, year,
                            oddSemesters, isWinterEnrollment, false, idNameString, variantToInstConfigMap, psddMap
                        )
                    );

                    if (priceBook.Graded_tuition__c) {
                        pbJSON.templates.add(
                            IntegrationPaymentTemplateHelper.buildTemplate(
                                isLastYear, priceBook, offerKind, degree, studyStartDate, Integer.valueOf(variant), semesterCount, year,
                                oddSemesters, isWinterEnrollment, true, idNameString, variantToInstConfigMap, psddMap
                            )
                        );
                    }
                }
            }
        }

        return pbJSON;
    }

    // returns Payment Template with positions for given parameters
    private static Template_JSON buildTemplate(Boolean isLastYear, Offer__c priceBook, String offerKind, String degree, Date studyStartDate, Integer instPerYear, 
                                               Integer semesterCount, Integer yearCount, Boolean oddSemesters, Boolean isWinterEnrollment, 
                                               Boolean gradedTuition, String idNameString,  Map<Integer, Offer__c> variantToInstConfigMap, 
                                               Map<String, PaymentScheduleDeadlineDesignator> psddMap) {

        Template_JSON templateJSON = new Template_JSON();

        /* 
        CCCCCC_XXX_BB_DD_RR
        CCCCCC - nazwa skrócona produktu, do którego jest podpięty dany cennik (czyli jeśli podpięty jest do specjalności, to jest to kod specjalności, a jeśli do kierunku, to kod kierunku)
        XXX - numer cennika w CRM, np. C01, C02...
        BB - liczba rat - od 1 do 12, bez zer wiodących
        DD - system czesnego. Możliwe wartości: st - stałe, zm - zmienne. Cena jednorazowa nie ma tworzonego szablonu płatności
        RR - rok i miesiąc rozpoczęcia studiów na ofercie - rozróżni to rekrutację letnią od zimowej
        */
        templateJSON.id = idNameString + '_' 
                        + instPerYear + '_' 
                        + IntegrationPaymentTemplateHelper.tuitionTypeDictMap.get(gradedTuition) + '_'
                        + String.valueOf(studyStartDate.year()).right(2) + studyStartDate.month();

        Integer instVariantThisYear;
        if (oddSemesters && isLastYear && instPerYear == 1) {
            instVariantThisYear = 2;
        } else if (isLastYear && instPerYear == 12) {
            instVariantThisYear = 10;
        } else {
            instVariantThisYear = instPerYear;
        }

        Decimal valueForThisInstallment;
        if (variantToInstConfigMap.containsKey(instVariantThisYear)) {
            if (!gradedTuition) {
                valueForThisInstallment = variantToInstConfigMap.get(instVariantThisYear).Fixed_Price__c;
            } else {
                valueForThisInstallment = (Decimal) variantToInstConfigMap.get(instVariantThisYear).get(PriceBookManager.gradedPriceFields[yearCount]);
            }
        }

        /* 
        A_BB_CCCCCC_DD_EE_RRMMDD_$$$$$

        gdzie:
        A - numer roku, na który zakładany jest szablon
        BB - liczba rat - od 1 do 12, bez zer wiodących
        CCCCCC - nazwa skrócona produktu, do którego jest podpięty dany cennik (czyli jeśli podpięty jest do specjalności, to jest to kod specjalności, a jeśli do kierunku, to kod kierunku)
        DD - system czesnego. Możliwe wartości: st - stałe, zm - zmienne. Cena jednorazowa nie ma tworzonego szablonu płatności
        EE - typ cennika. Możliwe wartości: pl - standardowy, cu - dla cudzoziemca. Szablonów pochodzących z cenników specjalnych nie wysyłamy
        RRMMDD - data utworzenia cennika w systemie CRM
        $$$$$ - wartość cennikowa pojedynczej raty
        */
        
        templateJSON.description = String.valueOf(yearCount+1) + '_' 
                                   + String.valueOf(instPerYear) + '_'
                                   + priceBook.Offer_from_Price_Book__r.Short_Name__c + '_'
                                   + IntegrationPaymentTemplateHelper.tuitionTypeDictMap.get(gradedTuition) + '_'
                                   + IntegrationPaymentTemplateHelper.pbTypeDictMap.get(priceBook.Price_Book_Type__c) + '_'
                                   + priceBook.PriceBook_From__c.formatGMT(IntegrationPaymentTemplateHelper.DATE_FORMAT_DESC) + '_' + String.valueOf(valueForThisInstallment);

        if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)) {
            templateJSON.positions = buildPositions_I_II_U(
                isLastYear, priceBook, offerKind, degree, studyStartDate, instPerYear, semesterCount, yearCount,
                oddSemesters, isWinterEnrollment, gradedTuition, variantToInstConfigMap, psddMap
            );
        } else 
        if (priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG)
            || priceBook.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)) {
            templateJSON.positions = buildPositions_PG_MBA(
                isLastYear, priceBook, offerKind, degree, studyStartDate, instPerYear, semesterCount, yearCount,
                oddSemesters, isWinterEnrollment, variantToInstConfigMap, psddMap
            );

            // TODO build Onetime_Price__c position - Experia does not handle this type at the moment
        }

        return templateJSON;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------- TEMPLATE BUILDER - POSITIONS - I II U ------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    // method usable for I & II Degree Study Price Books
    // returns Payment Positions for specific Payment Template for given parameters
    private static List<Position_JSON> buildPositions_I_II_U(Boolean isLastYear, Offer__c priceBook, String offerKind, String degree, Date studyStartDate, Integer instPerYear, 
                                                             Integer semesterCount, Integer yearCount, Boolean oddSemesters, Boolean isWinterEnrollment, 
                                                             Boolean gradedTuition, Map<Integer, Offer__c> variantToInstConfigMap, 
                                                             Map<String, PaymentScheduleDeadlineDesignator> psddMap) {

        List<Position_JSON> positionJSONList = new List<Position_JSON>();
              
        Integer serialNo = 1;

        // entry installment
        //Position_JSON entryPosition;
        //if (priceBook.Entry_fee__c != null && priceBook.Entry_fee__c > 0) {
        //    entryPosition = new Position_JSON();
        //    entryPosition.item_no = serialNo++;
        //    entryPosition.title = 1; //based on contract
        //    entryPosition.amount = priceBook.Entry_fee__c;

        //    positionJSONList.add(entryPosition);
        //}

        // tuition installments
        //for (Integer year = 0; year < yearCount; year++) {
            Integer year = yearCount;

            Integer installmentsThisYear = (isLastYear && instPerYear == 12)? 10 : instPerYear;
            Integer actualInstallmentsThisYear = (oddSemesters && isLastYear && installmentsThisYear > 1)? installmentsThisYear / 2 : installmentsThisYear;

            /* Workaround for Experia shenanigans */
            Boolean previousValues = false;
            if (actualInstallmentsThisYear == 1 && installmentsThisYear == 1) {
                actualInstallmentsThisYear = 2;
                previousValues = true;
            }

            for (Integer installmentNum = 1; installmentNum <= actualInstallmentsThisYear; installmentNum++) {

                Integer installmentSemester = (year * 2) + (installmentNum <= Math.ceil(Decimal.valueOf(installmentsThisYear) / 2)? 1 : 2);

                Integer instVariantThisYear;
                if (oddSemesters && isLastYear && instPerYear == 1) {
                    instVariantThisYear = 2;
                } else if (isLastYear && instPerYear == 12) {
                    instVariantThisYear = 10;
                } else {
                    instVariantThisYear = instPerYear;
                }

                Decimal valueForThisInstallment;
                if (variantToInstConfigMap.containsKey(instVariantThisYear)) {
                    if (!gradedTuition) {
                        valueForThisInstallment = variantToInstConfigMap.get(instVariantThisYear).Fixed_Price__c;
                    } else {
                        valueForThisInstallment = (Decimal) variantToInstConfigMap.get(instVariantThisYear).get(PriceBookManager.gradedPriceFields[year]);
                    }
                }

                if (instPerYear == 1) valueForThisInstallment *= 0.5;

                Position_JSON tuitionPosition = new Position_JSON();
                tuitionPosition.item_no = serialNo++;
                tuitionPosition.semester = installmentSemester;
                tuitionPosition.title = IntegrationPaymentTemplateHelper.getTuitionInstTitleCode(instVariantThisYear); //based on contract
                tuitionPosition.amount = valueForThisInstallment;
                tuitionPosition.model = (Math.mod(installmentSemester, 2) == 1? priceBook.Schedule_Model__c : priceBook.Schedule_Model_2__c);

                /* Workaround for Experia shenanigans */
                if (!previousValues) {
                    if (isWinterEnrollment && Math.mod(Integer.valueOf(installmentSemester), 2) == 1) {
                        tuitionPosition.academic_year = studyStartDate.year() + year - 1;
                    } else {
                        tuitionPosition.academic_year = studyStartDate.year() + year;
                    }
                }
                else {
                    if (isWinterEnrollment && Math.mod(Integer.valueOf(installmentSemester), 2) == 1) {
                        tuitionPosition.academic_year = studyStartDate.year() + year - 1;
                    } else {
                        tuitionPosition.academic_year = studyStartDate.year() + year;
                    }
                }

                /* payment date designation */
                String psddMapKey = priceBook.University_Name__c + degree + isWinterEnrollment + studyStartDate + instVariantThisYear 
                                    + yearCount + semesterCount + offerKind;

                PaymentScheduleDeadlineDesignator psdd = psddMap.get(psddMapKey);
                if (psdd == null) {
                    psdd = new PaymentScheduleDeadlineDesignator(
                        priceBook.University_Name__c, degree, isWinterEnrollment, studyStartDate, instVariantThisYear, yearCount, 
                        semesterCount, CommonUtility.ENROLLMENT_STARTINGSEM_1, offerKind, 1
                    );

                    psddMap.put(psddMapKey, psdd); 
                }

                /* Workaround for Experia shenanigans */
                if (previousValues) {
                    Integer fixedInstallmentNum = (Math.mod(installmentNum, 2) == 0) ? (installmentNum - 1) : installmentNum;
                    tuitionPosition.date_of_payment = IntegrationPaymentTemplateHelper.formatDate(
                        psdd.designateDateForInstallment(fixedInstallmentNum, year),
                        IntegrationPaymentTemplateHelper.DATE_FORMAT_POSITION
                    );
                }
                else {
                    tuitionPosition.date_of_payment = IntegrationPaymentTemplateHelper.formatDate(
                        psdd.designateDateForInstallment(installmentNum, year),
                        IntegrationPaymentTemplateHelper.DATE_FORMAT_POSITION
                    );                    
                }
                /* eof payment date designation */

                positionJSONList.add(tuitionPosition);
            //}
        }

        // apply info from first Tuition Position to Entry Position
        //if (entryPosition != null && positionJSONList.size() > 1) {
        //    entryPosition.academic_year = positionJSONList[1].academic_year;
        //    entryPosition.semester = positionJSONList[1].semester;
        //    entryPosition.date_of_payment = positionJSONList[1].date_of_payment;
        //}
    
        return positionJSONList;
    }





    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- TEMPLATE BUILDER - POSITIONS - PG & MBA ----------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method usable for PG & MBA Degree Study Price Books
    // returns Payment Positions for specific Payment Template for given parameters
    private static List<Position_JSON> buildPositions_PG_MBA(Boolean isLastYear, Offer__c priceBook, String offerKind, String degree, Date studyStartDate, Integer instPerYear, 
                                                             Integer semesterCount, Integer yearCount, Boolean oddSemesters, Boolean isWinterEnrollment, 
                                                             Map<Integer, Offer__c> variantToInstConfigMap, Map<String, PaymentScheduleDeadlineDesignator> psddMap) {

        List<Position_JSON> positionJSONList = new List<Position_JSON>();
              
        Integer serialNo = 1;

        // entry installment
        //Position_JSON entryPosition;
        //if (priceBook.Entry_fee__c != null && priceBook.Entry_fee__c > 0) {
        //    entryPosition = new Position_JSON();
        //    entryPosition.item_no = serialNo++;
        //    entryPosition.title = 1; //based on contract
        //    entryPosition.amount = priceBook.Entry_fee__c;

        //    positionJSONList.add(entryPosition);
        //} 

        // tuition installments
        //for (Integer year = 0; year < yearCount; year++) {
            Integer year = yearCount;

            Decimal installmentsThisYear = (isLastYear && (instPerYear == 12 || instPerYear == 11))? 10 : instPerYear;

            Integer actualInstallmentsThisYear = Integer.valueOf((oddSemesters && isLastYear && (installmentsThisYear > 1))? 
                                                                      Math.ceil(installmentsThisYear / 2) : installmentsThisYear);

            /* Workaround for Experia shenanigans */
            Boolean previousValues = false;
            if (actualInstallmentsThisYear == 1 && installmentsThisYear == 1) {
                actualInstallmentsThisYear = 2;
                previousValues = true;
            }
            
            for (Integer installmentNum = 1; installmentNum <= actualInstallmentsThisYear; installmentNum++) {

                Integer installmentSemester = (year * 2) + (installmentNum <= Math.ceil(installmentsThisYear / 2)? 1 : 2);

                Integer instVariantThisYear;
                // exceptions
                if (oddSemesters && isLastYear && instPerYear == 1) {
                    instVariantThisYear = 2;
                } else if (isLastYear && (instPerYear == 12 || instPerYear == 11)) {
                    instVariantThisYear = 10;
                } else {
                    instVariantThisYear = instPerYear;
                }

                Decimal valueForThisInstallment;
                if (variantToInstConfigMap.containsKey(instVariantThisYear)) {
                    valueForThisInstallment = variantToInstConfigMap.get(instVariantThisYear).Fixed_Price__c;
                } else {
                    ErrorLogger.configMsg(CommonUtility.INADEQUATE_PRICEBOOK_CONFIG + ' ' + priceBook.Id);
                    throw new InadequatePriceBookConfigException(priceBook.Id);
                    return null;
                }

                if (instPerYear == 1) valueForThisInstallment *= 0.5;

                Position_JSON tuitionPosition = new Position_JSON();
                tuitionPosition.item_no = serialNo++;
                tuitionPosition.semester = installmentSemester;
                tuitionPosition.title = IntegrationPaymentTemplateHelper.getTuitionInstTitleCode(instVariantThisYear); //based on contract
                tuitionPosition.amount = valueForThisInstallment;
                tuitionPosition.model = (Math.mod(installmentSemester, 2) == 1? priceBook.Schedule_Model__c : priceBook.Schedule_Model_2__c);

                //if (isWinterEnrollment && Math.mod(Integer.valueOf(installmentSemester), 2) == 1) {
                //    tuitionPosition.academic_year = studyStartDate.year() + year - 1;
                //} else {
                //    tuitionPosition.academic_year = studyStartDate.year() + year;
                //}

                /* Workaround for Experia shenanigans */
                if (!previousValues) {
                    if (isWinterEnrollment && Math.mod(Integer.valueOf(installmentSemester), 2) == 1) {
                        tuitionPosition.academic_year = studyStartDate.year() + year - 1;
                    } else {
                        tuitionPosition.academic_year = studyStartDate.year() + year;
                    }
                }
                else {
                    if (isWinterEnrollment && Math.mod(Integer.valueOf(installmentSemester), 2) == 1) {
                        tuitionPosition.academic_year = studyStartDate.year() + year - 1;
                    } else {
                        tuitionPosition.academic_year = studyStartDate.year() + year;
                    }
                }

                /* payment date designation */
                String psddMapKey = priceBook.University_Name__c + degree + isWinterEnrollment + studyStartDate + instVariantThisYear 
                                    + yearCount + semesterCount + offerKind;

                PaymentScheduleDeadlineDesignator psdd = psddMap.get(psddMapKey);
                if (psdd == null) {
                    psdd = new PaymentScheduleDeadlineDesignator(
                        priceBook.University_Name__c, degree, isWinterEnrollment, studyStartDate, instVariantThisYear, yearCount, 
                        semesterCount, CommonUtility.ENROLLMENT_STARTINGSEM_1, offerKind, 1
                    );

                    psddMap.put(psddMapKey, psdd); 
                }

                

                /* Workaround for Experia shenanigans */
                if (previousValues) {
                    Integer fixedInstallmentNum = (Math.mod(installmentNum, 2) == 0) ? (installmentNum - 1) : installmentNum;
                    tuitionPosition.date_of_payment = IntegrationPaymentTemplateHelper.formatDate(
                        psdd.designateDateForInstallment(fixedInstallmentNum, year),
                        IntegrationPaymentTemplateHelper.DATE_FORMAT_POSITION
                    );
                }
                else {
                    tuitionPosition.date_of_payment = IntegrationPaymentTemplateHelper.formatDate(
                        psdd.designateDateForInstallment(installmentNum, year),
                        IntegrationPaymentTemplateHelper.DATE_FORMAT_POSITION
                    );                    
                }
                /* eof payment date designation */

                //tuitionPosition.date_of_payment = IntegrationPaymentTemplateHelper.formatDate(
                //    psdd.designateDateForInstallment(installmentNum, year, false, false),
                //    IntegrationPaymentTemplateHelper.DATE_FORMAT_POSITION
                //);
                /* eof payment date designation */

                positionJSONList.add(tuitionPosition);
            }
        //}

        // apply info from first Tuition Position to Entry Position
        //if (entryPosition != null && positionJSONList.size() > 1) {
        //    entryPosition.academic_year = positionJSONList[1].academic_year;
        //    entryPosition.semester = positionJSONList[1].semester;
        //    entryPosition.date_of_payment = positionJSONList[1].date_of_payment;
        //}
    
        return positionJSONList;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String formatDate(Date d, String dateFormat) {
        if (d != null) {
            return DateTime.newInstance(d.year(), d.month(), d.day()).format(dateFormat);
        }
        return null;
    }

    private static String getTemplateIdName(Offer__c priceBook) {
        String mode = priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Mode__c != null ? priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Mode__c : 
        priceBook.Offer_from_Price_Book__r.Mode__c;

        StudyMode_Abbreviation__c modeAbbreviation = StudyMode_Abbreviation__c.getInstance(mode);

        String idName = (priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Short_Name__c != null?
            priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Short_Name__c : 
            priceBook.Offer_from_Price_Book__r.Short_Name__c) + '_'
            + (priceBook.Offer_from_Price_Book__r.Course_Offer_from_Specialty__r.Short_Name__c != null?
            priceBook.Offer_from_Price_Book__r.Short_Name__c + '_' : '')
            + modeAbbreviation.Abbreviation__c + '_' + priceBook.Name.substring(priceBook.Name.lastIndexOf('C'));

        return CommonUtility.swapSpecialPolishChars(idName);
    }

    // method for updating PriceBook Synchronization Status after sending to ESB
    public static void updateSyncStatus_inProgress(Boolean success, Set<Id> pbIds) {
        List<Offer__c> pbsToUpdate = new List<Offer__c>();

        for (Id pbId : pbIds) {
            Offer__c pbToUpdate = new Offer__c(
                Id = pbId,
                Synchronization_Status__c = (success? CommonUtility.SYNCSTATUS_INPROGRESS : CommonUtility.SYNCSTATUS_FAILED)
            );

            pbsToUpdate.add(pbToUpdate);
        }

        try {
            update pbsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    // method for updating PriceBook Synchronization Status after receiving ack from ESB
    public static void updateSyncStatus_onFinish(Boolean success, Set<Id> pbIds) {
        List<Offer__c> pbsToUpdate = new List<Offer__c>();

        for (Id pbId : pbIds) {
            Offer__c pbToUpdate = new Offer__c(
                Id = pbId,
                Synchronization_Status__c = (success? CommonUtility.SYNCSTATUS_FINISHED : CommonUtility.SYNCSTATUS_FAILED)
            );

            pbsToUpdate.add(pbToUpdate);
        }

        try {
            update pbsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- EXCEPTION CLASSES ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class InadequatePriceBookConfigException extends Exception {}

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class PriceBook_JSON {
        public String pricebook_id;
        public String product_id;
        public List<Template_JSON> templates;

        public PriceBook_JSON() {
            templates = new List<Template_JSON>();
        }
    }

    public class Template_JSON {
        public String id;
        public String description;
        public List<Position_JSON> positions;

        public Template_JSON() {
            positions = new List<Position_JSON>();
        }
    }

    public class Position_JSON {
        public Integer item_no;
        public Integer academic_year;
        public Integer semester;
        public Integer title;
        public Decimal amount;
        public String date_of_payment;
        public String model;
    }

}