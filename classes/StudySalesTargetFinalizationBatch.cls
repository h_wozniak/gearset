/**
*   @author         Wojciech Słodziak
*   @description    Batch class for finalizing StudySalesTargetComputationsBatch calculations, updates Sales Targets if Realization Target is changed.
**/

global without sharing class StudySalesTargetFinalizationBatch implements Database.Batchable<sObject> {

    Boolean isRunManually;
    Set<Id> targetRecordTypeIds;

    global StudySalesTargetFinalizationBatch(Boolean isRunManually) {
        this.isRunManually = isRunManually;

        targetRecordTypeIds = new Set<Id> {
            CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_TARGET),
            CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_SUBTARGET),
            CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_EDUADV_TARGET),
            CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TEAM_TARGET)
        };
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Realization_Amount__c, Realization_Amount_Batch_Helper__c, Last_Update_Date__c
            FROM Target__c
            WHERE RecordTypeId IN :targetRecordTypeIds
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Target__c> targetList = (List<Target__c>) scope;

        for (Target__c target : targetList) {
            target.Realization_Amount__c = target.Realization_Amount_Batch_Helper__c;
            target.Realization_Amount_Batch_Helper__c = 0;
            target.Last_Update_Date__c = System.now();
        }

        try {
            update targetList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (isRunManually) {
            EmailManager.sendNotificationToUser(UserInfo.getUserId(), Label.msg_info_SalesTargetRecalculationFinished);
        }
    }
    
}