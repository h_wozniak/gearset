/**
*   @author         Sebastian Łasisz
*   @description    This class is used to make dictionaries available for external external systems.
**/
global with sharing class IntegrationHighSchoolDictionaryHelper {


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- LOGIC TO GET DICTIONARY --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */ 

    public static String getDictionaryHighSchool(String dateFromRequest) {
        DateTime dateFrom = CommonUtility.stringToDateTimeGMT(dateFromRequest);
        
		List<Account> listOfHighSchools = [
	    	SELECT Id, Name, BillingCity, School_Type__c 
	    	FROM Account 
	    	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)
            AND LastModifiedDate >= :dateFrom
		];
    	
    	List<Dictionary_JSON> listOfHighSchoolsToJSON = new List<Dictionary_JSON>();

    	for (Account highSchool : listOfHighSchools) {
    		listOfHighSchoolsToJSON.add(
                new Dictionary_JSON(
                    highSchool.Id, 
                    highSchool.Name, 
                    highSchool.BillingCity, 
                    IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(highSchool.School_Type__c)
            ));
    	}

    	return JSON.serialize(listOfHighSchoolsToJSON);
    }

    public static String getDictionaryHighSchoolNoDate() {
        
        List<Account> listOfHighSchools = [
            SELECT Id, Name, BillingCity, School_Type__c 
            FROM Account 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)
        ];
        
        List<Dictionary_JSON> listOfHighSchoolsToJSON = new List<Dictionary_JSON>();

        for (Account highSchool : listOfHighSchools) {
            listOfHighSchoolsToJSON.add(
                new Dictionary_JSON(
                    highSchool.Id, 
                    highSchool.Name, 
                    highSchool.BillingCity, 
                    IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(highSchool.School_Type__c)
            ));
        }

        return JSON.serialize(listOfHighSchoolsToJSON);
    }

    public static Map<String, Id> getHighSchoolDictionaryMap() {
        Map<String, Id> highSchoolDictionaryMap = new Map<String, Id>();     
           
        List<Account> highSchools = [
            SELECT Id, Name
            FROM Account 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)
        ];

        for (Account highSchool : highSchools) {
            highSchoolDictionaryMap.put(highSchool.Name, highSchool.Id);
        }

        return highSchoolDictionaryMap;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class Dictionary_JSON {
        public String id;
        public String school_name;
        public String school_city;
        public String school_type;

        public Dictionary_JSON(String id, String school_name, String school_city, String school_type) {
            this.id = id;
            this.school_name = school_name;
            this.school_city = school_city;
            this.school_type = school_type;
        }
    }
}