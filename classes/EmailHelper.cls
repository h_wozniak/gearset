/**
* @author       Wojciech Słodziak
* @description  Helper class for sending emails.
**/

public without sharing class EmailHelper {


    public static void invokeWorkflowEmail(Map<Id, String> recordIdToWorkflowTriggerName) {
        List<SObject> sObjToUpdate = new List<SObject>();

        Schema.SObjectType sObjType = new List<Id>(recordIdToWorkflowTriggerName.keySet())[0].getSObjectType();

        for (Id key : recordIdToWorkflowTriggerName.keySet()) {
            SObject sObj = Schema.getGlobalDescribe().get(sObjType.getDescribe().getName()).newSObject();

            sObj.Id = key;
            sObj.put('WorkflowEmailNameInvokation__c', recordIdToWorkflowTriggerName.get(key));

            sObjToUpdate.add(sObj);
        }

        try {
            update sObjToUpdate; // triggers workflow
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

}