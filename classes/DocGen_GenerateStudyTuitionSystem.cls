global class DocGen_GenerateStudyTuitionSystem implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateStudyTuitionSystem() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateStudyTuitionSystem.generateStudyTuitionSystem(objectId);  
    }
    
    global static String generateStudyTuitionSystem(String objectId) {
        Enrollment__c enr = [
            SELECT Tuition_System__c, Enrollment_from_Documents__r.Tuition_System__c, Language_of_Enrollment__c, Language_of_Main_Enrollment__c
            FROM Enrollment__c 
            WHERE Id = :objectId
        ];

        if (enr.Tuition_System__c != null) {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Enrollment__c.Tuition_System__c);

            return translatedField.get(enr.Tuition_System__c);
        }
        else {
            String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);
            Map<String, String> translatedField = DictionaryTranslator.getTranslatedPicklistValues(languageCode, Enrollment__c.Tuition_System__c);

            return translatedField.get(enr.Enrollment_from_Documents__r.Tuition_System__c);
        }
    }
}