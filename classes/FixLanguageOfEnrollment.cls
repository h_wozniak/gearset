/**
*   @author         Sebastian Łasisz
*   @description    batch class that sets default value of language of enrollment
**/

global class FixLanguageOfEnrollment implements Database.Batchable<sObject>, Database.AllowsCallouts {

	global FixLanguageOfEnrollment() { }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        	SELECT Id
            FROM Enrollment__c
            WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            OR RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT))
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Enrollment__c> enrollments) { 
		for (Enrollment__c enrollment : enrollments) {
			enrollment.Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH;
		}

        Database.update(enrollments, false);
	}

	global void finish(Database.BatchableContext BC) { }
}