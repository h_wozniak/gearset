/**
 * @author Sebastian Łasisz
 * @description  This class is used to retrieve unique document reference number for documents.
**/
public without sharing class CustomSettingManager {

    private static final Integer MAX_ATTEMPTS = 3;
    
    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------- DOCUMENT NUMBER REFERENCE METHODS ----------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    
    // Dear maintainer:
    // 
    // Once you are done trying to 'optimize' this routine,
    // and have realized what a terrible mistake that was,
    // please increment the following counter as a warning
    // to the next guy:
    // 
    // total_hours_wasted_here = 4
    public static Decimal getCurrentDocumentReferenceNumber() {
        Integer attempts = MAX_ATTEMPTS;
        Decimal current = 0;

        while(attempts > 0) {
            try {
                attempts--; 

                // I'm sorry maintainer. Needed to avoid duplicating document reference number and record lock when function is accessed by two different user at exactly same time. Do not remove.
                DocumentReferenceNumber__c docRefNumber;
                if (Test.isRunningTest()) {
                    docRefNumber = DocumentReferenceNumber__c.getOrgDefaults();
                }
                else {
                    docRefNumber = [SELECT ReferenceNumber__c FROM DocumentReferenceNumber__c FOR UPDATE];
                }

                if (docRefNumber.ReferenceNumber__c == null) {
                    docRefNumber = CustomSettingDefault.initDocumentReferenceNumber();
                }

                current = docRefNumber.ReferenceNumber__c;

                docRefNumber.ReferenceNumber__c++;
                update docRefNumber;

                attempts = 0;

                return current;
            } 
            catch (Exception e) {
                //ErrorLogger.log(e);
            }    
        }

        return current;
    }
}