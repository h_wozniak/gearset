@isTest
private class ZTestIntegrationExtCampaignCreation {
            
	
	/* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TEST POSITIVE CASES -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void createExtCampaign_new_campaigns() {
    	RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf(JSON.serialize(ZDataTestUtilityMethods.prepareValidData()));

        Test.startTest();
        	IntegrationExtCampaignCreation.createExtCampaign();
        Test.stopTest();

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns = ZDataTestUtilityMethods.getExternalCampaigns();

        System.assertEquals(200, RestContext.response.statusCode);
        System.assertEquals(3, listOfExternalMarketingCampaigns.size());

        for (Integer i = 0; i < listOfExternalMarketingCampaigns.size(); i++) {
            System.assertEquals('extCampaignName_'+i, listOfExternalMarketingCampaigns.get(i).External_Id__c);
            System.assertEquals('testName'+i, listOfExternalMarketingCampaigns.get(i).Campaign_Name__c);
            if (i == 0) System.assertEquals('WWW', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
            if (i == 1) System.assertEquals('FCC', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
            if (i == 2) System.assertEquals('iPresso', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
        }

    }

    @isTest static void createExtCampaign_update_campaigns() {

        ZDataTestUtility.createExtCampaigns(null, 3, true);

    	RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf(JSON.serialize(ZDataTestUtilityMethods.prepareValidData()));

        Test.startTest();
        	IntegrationExtCampaignCreation.createExtCampaign();
        Test.stopTest();

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns = ZDataTestUtilityMethods.getExternalCampaigns();

        System.assertEquals(3, listOfExternalMarketingCampaigns.size());
        System.assertEquals(200, RestContext.response.statusCode);

        for (Integer i = 0; i < listOfExternalMarketingCampaigns.size(); i++) {
            System.assertEquals('extCampaignName_'+i, listOfExternalMarketingCampaigns.get(i).External_Id__c);
            System.assertEquals('testName'+i, listOfExternalMarketingCampaigns.get(i).Campaign_Name__c);
            if (i == 0) System.assertEquals('WWW', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
            if (i == 1) System.assertEquals('FCC', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
            if (i == 2) System.assertEquals('iPresso', listOfExternalMarketingCampaigns.get(i).Campaign_Communication_Form__c);
        }

    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TEST Negative Cases -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void createExtCampaign_invalid_field_value() {
        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf(JSON.serialize(ZDataTestUtilityMethods.prepareInvalidData_400()));

        Test.startTest();
            IntegrationExtCampaignCreation.createExtCampaign();
        Test.stopTest();

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns = ZDataTestUtilityMethods.getExternalCampaigns();

        System.assertEquals(400, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj = 
            (IntegrationError.Error_JSON) 
            JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(800, responseObj.error_code);
        System.assertEquals(0, listOfExternalMarketingCampaigns.size());
    }

    @isTest static void createExtCampaign_invalid_json() {
        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf('NOT A CORRECT JSON STTRING {}[]!!');

        Test.startTest();
            IntegrationExtCampaignCreation.createExtCampaign();
        Test.stopTest();

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns = ZDataTestUtilityMethods.getExternalCampaigns();

        System.assertEquals(400, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj = 
            (IntegrationError.Error_JSON) 
            JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(601, responseObj.error_code);
        System.assertEquals(0, listOfExternalMarketingCampaigns.size());
    }

    @isTest static void createExtCampaign_duplicate_id() {
        RestRequest req = new RestRequest();
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        req.requestBody = Blob.valueOf(JSON.serialize(ZDataTestUtilityMethods.prepareInvalidData_500()));

        Test.startTest();
            IntegrationExtCampaignCreation.createExtCampaign();
        Test.stopTest();

        List<Marketing_Campaign__c> listOfExternalMarketingCampaigns = ZDataTestUtilityMethods.getExternalCampaigns();

        System.assertEquals(500, RestContext.response.statusCode);

        IntegrationError.Error_JSON responseObj = 
            (IntegrationError.Error_JSON) 
            JSON.deserialize(RestContext.response.responseBody.toString(), IntegrationError.Error_JSON.class);

        System.assertEquals(600, responseObj.error_code);
        System.assertEquals(0, listOfExternalMarketingCampaigns.size());
    }

}