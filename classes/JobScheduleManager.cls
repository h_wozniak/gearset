/**
*   @author         Wojciech Słodziak
*   @description    Manager class for scheduling custom system jobs.
**/

public without sharing class JobScheduleManager {

    public static void scheduleAll() {
        schedule_EnrollmentConfirmationEmailReminder();
        schedule_EnrollmentDocumentDeliveryEmailReminder();
        schedule_SalesTargetComputations();
        schedule_IntegrationFCCInteractionReceiver();
        schedule_CreateCampaignContact();
        schedule_CampaignLackOfDocuments();
        schedule_ResendNotSentDocumentEmail();
        schedule_ExtranetEmail();
        schedule_PaymentTemplates();
        schedule_syncConsentsWithIPresso();
        schedule_removeIpressoCampaigns();
        schedule_UnenrolledDocReminder();
        schedule_retrieveArrears();
        schedule_workshopReminderSchool();
        schedule_VisitatorSchoolVisitEmailReminder();
        schedule_retrieveVisitatorEvents();
    }

    public static void schedule_EnrollmentConfirmationEmailReminder() {
        if (!checkIfJobAlreadyExists(EnrollmentConfirmationEmailReminder.class.getName())) {
            System.schedule(EnrollmentConfirmationEmailReminder.class.getName(), '0 0 5 * * ? *', new EnrollmentConfirmationEmailReminder());
        }
    }

    public static void schedule_EnrollmentDocumentDeliveryEmailReminder() {
        if (!checkIfJobAlreadyExists(EnrollmentDocumentDeliveryEmailReminder.class.getName())) {
            System.schedule(EnrollmentDocumentDeliveryEmailReminder.class.getName(), '0 0 6 * * ? *', new EnrollmentDocumentDeliveryEmailReminder());
        }
    }

    public static void schedule_SalesTargetComputations() {
        if (!checkIfJobAlreadyExists(SalesTargetComputations.class.getName())) {
            System.schedule(SalesTargetComputations.class.getName(), '0 0 2 * * ? *', new SalesTargetComputations());
        }
    }

    public static void schedule_IntegrationFCCInteractionReceiver() {
        if (!checkIfJobAlreadyExists(ScheduleIntegrationFCCInteraction.class.getName())) {
            System.schedule(ScheduleIntegrationFCCInteraction.class.getName(), '0 0 * * * ? *', new ScheduleIntegrationFCCInteraction());
        }
    }

    public static void schedule_ResendNotSentDocumentEmail() {
        if (!checkIfJobAlreadyExists(ResendNotSentDocumentEmail.class.getName())) {
            System.schedule(ResendNotSentDocumentEmail.class.getName(), '0 0 * * * ? *', new ResendNotSentDocumentEmail());
        }
    }

    public static void schedule_CreateCampaignContact() {
        if (!checkIfJobAlreadyExists(CampaignContactInsertBatch.class.getName())) {
            System.schedule(CampaignContactInsertBatch.class.getName(), '0 0 7 * * ? *', new CampaignContactInsertBatch());
        }
    }

    public static void schedule_CampaignLackOfDocuments() {
        if (!checkIfJobAlreadyExists(CampaignLackOfDocumentsInsertBatch.class.getName())) {
            System.schedule(CampaignLackOfDocumentsInsertBatch.class.getName(), '0 0 8 * * ? *', new CampaignLackOfDocumentsInsertBatch());
        }
    }

    public static void schedule_ExtranetEmail() {
        if (!checkIfJobAlreadyExists(EnrollmentDocumentDeliveryExtranetEmail.class.getName())) {
            System.schedule(EnrollmentDocumentDeliveryExtranetEmail.class.getName(), '0 0 7 * * ? *', new EnrollmentDocumentDeliveryExtranetEmail());
        }        
    }

    public static void schedule_UnenrolledDocReminder() {
        if (!checkIfJobAlreadyExists(UnenrolledDocReminderSchedule.class.getName())) {
            System.schedule(UnenrolledDocReminderSchedule.class.getName(), '0 0 8 * * ? *', new UnenrolledDocReminderSchedule());
        }
    }

    public static void schedule_PaymentTemplates() {
        if (!checkIfJobAlreadyExists(IntegrationPaymentTemplateSchedule.class.getName())) {
            System.schedule(IntegrationPaymentTemplateSchedule.class.getName(), '0 0 7 * * ? *', new IntegrationPaymentTemplateSchedule());
        }           
    }

    public static void schedule_syncConsentsWithIPresso() {
        if (!checkIfJobAlreadyExists(IntegrationIPressoConsentSyncSchedule.class.getName())) {
            System.schedule(IntegrationIPressoConsentSyncSchedule.class.getName(), '0 0 7 * * ? *', new IntegrationIPressoConsentSyncSchedule());
        }           
    }

    public static void schedule_removeIpressoCampaigns() {
        if (!checkIfJobAlreadyExists(IntegrationIpressoDeleteTagsSchedule.class.getName())) {
            System.schedule(IntegrationIpressoDeleteTagsSchedule.class.getName(), '0 0 2 * * ? *', new IntegrationIpressoDeleteTagsSchedule());
        }           
    }

    public static void schedule_retrieveArrears() {
        if (!checkIfJobAlreadyExists(RetrieveArrearsSchedule.class.getName())) {
            System.schedule(RetrieveArrearsSchedule.class.getName(), '0 0 1 * * ? *', new RetrieveArrearsSchedule());
        }
    }

    public static void schedule_visitatorSchoolVisitEmailReminder() {
        if (!checkIfJobAlreadyExists(VisitatorSchoolVisitEmailReminder.class.getName())) {
            System.schedule(VisitatorSchoolVisitEmailReminder.class.getName(), '0 0 8 * * ? *', new VisitatorSchoolVisitEmailReminder());
        }
    }

    public static void schedule_workshopReminderSchool() {
        if (!checkIfJobAlreadyExists(WorkshopReminderSchoolSchedule.class.getName())) {
            System.schedule(WorkshopReminderSchoolSchedule.class.getName(), '0 0 4 * * ? *', new WorkshopReminderSchoolSchedule());
        }
    }
    
    public static void schedule_retrieveVisitatorEvents() {
        if (!checkIfJobAlreadyExists(RetrieveHighSchoolVisitsSchedule.class.getName())) {
            System.schedule(RetrieveHighSchoolVisitsSchedule.class.getName(), '0 0 4 * * ? *', new RetrieveHighSchoolVisitsSchedule());
        }
    }

    private static Boolean checkIfJobAlreadyExists(String jobName) {
        List<CronTrigger> jobList = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = :jobName];
        return !jobList.isEmpty();
    }

}