/**
*   @author         Kamil Bałuc, Wojciech Słodziak
*   @description    This class is used to send Candidate data to Experia
**/

global without sharing class IntegrationCandidateSender {

    //@future(callout=true)
    //public static void sendCandidateAsync(Id candidateId) {
    //    //IntegrationCandidateSender.sendCandidateFinal(candidateId);
    //}

    ////public static Boolean sendCandidateSync(Id candidateId) {
    ////    return IntegrationCandidateSender.sendCandidateFinal(candidateId);
    ////}

    //// final method for sending callout
    //private static Boolean sendCandidateFinal(Id candidateId) {
    //    HttpRequest httpReq = new HttpRequest();
        
    //    ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
    //    String createCandidateEndPoint = esb.Create_Candidate_End_Point__c;
    //    String esbIp = esb.Service_Url__c;

    //    httpReq.setEndpoint(esbIp + createCandidateEndPoint);
    //    httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
    //    httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

    //    String jsonToSend = IntegrationCandidateHelper.buildCandidateJSON(candidateId);
    //    httpReq.setBody(jsonToSend);
        
    //    Http http = new Http();
    //    HTTPResponse httpRes = http.send(httpReq);

    //    Boolean success = httpRes.getStatusCode() == 200;
    //    if (!success) {
    //        ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationCandidateSender.class.getName() + '\n' + httpRes.toString() + '\n' + candidateId);
    //    }

    //    return success;
    //}

}