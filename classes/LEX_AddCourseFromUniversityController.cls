public with sharing class LEX_AddCourseFromUniversityController {
	
	@AuraEnabled
	public static String getCourseRecordTypeId() {
		return CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER);
	}

	@AuraEnabled
	public static Offer__c getObjectParms(String offerId) {
		return [SELECT Active_from_default__c, Active_to_default__c FROM Offer__c WHERE Id = :offerId LIMIT 1];
	}
}