@isTest
private class ZTestEnrollmentStudyContactLookupWindow {
    
    @isTest static void test_searchLookup_withSearchText_fullName() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Jan', LastName = 'Kowalski'), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        controller.searchTxt = candidate.FirstName + ' ' + candidate.LastName;
        Test.stopTest();
        
        controller.searchLookup();
        
        System.assertEquals(controller.contactList.size(), 1);
    }
    
    @isTest static void test_searchLookup_withSearchText_firstName() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Jan', LastName = 'Kowalski'), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        controller.searchTxt = candidate.FirstName;
        Test.stopTest();
        
        controller.searchLookup();
        
        System.assertEquals(controller.contactList.size(), 1);
    }
    
    @isTest static void test_searchLookup_withoutSearchText() {
        List<Contact> candidate = ZDataTestUtility.createCandidateContacts(null, 2, true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate[0].id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        Test.stopTest();
        
        controller.searchLookup();
        
        System.assertEquals(controller.contactList.size(), 2);
        
    }
    
    @isTest static void test_checkContact_withDataCompletion() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            Foreigner__c = false, 
            FirstName = 'Jan', 
            LastName = 'Kowalski', 
            Pesel__c = '44072015724', 
            Email = 'test@test.com', 
            Phone = '123456789', 
            MobilePhone = '123456789',
            The_Same_Mailling_Address__c = true, 
            OtherStreet = 'Ruska 3', 
            OtherState = 'Lower Silesia', 
            OtherCountry = 'Poland', 
            OtherCity = 'Wrocław', 
            OtherPostalCode = '00-253', 
            Position__c = 'Programmer', 
            Department_Business_Area__c = 'Finance department',
            Nationality__c = CommonUtility.CONTACT_CITIZENSHIP_POLISH,
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            School_Name__c = 'Szkoła średnia NR 3',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED
        ), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        Test.stopTest();
        
        controller.checkContact();
        
        System.assert(controller.isComplete);
        
    }
    
    @isTest static void test_cancelEdit() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        Test.stopTest();
        
        controller.cancelEdit();
        
        System.assert(!controller.editMode);
    }
    
    @isTest static void test_updateContact_withoutCompletionData() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        System.assert(!controller.isComplete);
        System.assert(controller.editMode);
        
        Test.stopTest();
        
        controller.updateContact();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary().contains(Label.msg_ReqFieldsEnrollment1)) {
                messageFound = true;   
            }
        }
        System.assert(messageFound);
        System.assert(!controller.isComplete);
        System.assert(controller.editMode);
    }
    
    @isTest static void test_updateContact_withCompletionData() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            Foreigner__c = false, 
            FirstName = 'Jan', 
            LastName = 'Kowalski', 
            Pesel__c = '44072015724', 
            Email = 'test@test.com', 
            Phone = '123456789', 
            MobilePhone = '123456789', 
            The_Same_Mailling_Address__c = true, 
            OtherStreet = 'Ruska 3', 
            OtherState = 'Lower Silesia', 
            OtherCountry = 'Poland', 
            OtherCity = 'Wrocław', 
            OtherPostalCode = '00-253', 
            Position__c = 'Programmer', 
            Department_Business_Area__c = 'Finance department',
            Nationality__c = CommonUtility.CONTACT_CITIZENSHIP_POLISH,
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            School_Name__c = 'Szkoła średnia NR 3',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED
        ), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        Test.stopTest();
        
        controller.updateContact();
        
        System.assert(controller.isComplete);
        System.assertEquals(controller.nameMatch, candidate.FirstName + ' ' + candidate.LastName);
        System.assertEquals(controller.idMatch, candidate.Id);
        
    }
    
    @isTest static void test_newRecord() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        System.assert(!controller.isComplete);
        System.assert(controller.editMode);
        
        Test.stopTest();
        
        Boolean mode = controller.newMode;
        
        controller.newRecord();
        
        System.assertEquals(controller.newContact.RecordTypeId, CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE));
        System.assertNotEquals(mode, controller.newMode);
    }
    
    @isTest static void test_insertContact_withoutCompletionData() {
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        System.assert(!controller.isComplete);
        System.assert(controller.editMode);
        
        Test.stopTest();
        
        controller.insertContact();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary().contains(Label.msg_ReqFieldsEnrollment1)) {
                messageFound = true;   
            }
        }
        System.assert(messageFound);
        
        System.assert(!controller.isComplete);
    }
    
    @isTest static void test_insertContact_withCompletionData() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            Foreigner__c = false, 
            FirstName = 'Jan', 
            LastName = 'Kowalski', 
            Pesel__c = '44072015724', 
            Email = 'test@test.com', 
            Phone = '123456789', 
            MobilePhone = '123456789',
            The_Same_Mailling_Address__c = false, 
            MailingStreet = 'Ruska 3', 
            MailingState = 'Lower Silesia', 
            MailingCountry = 'Poland', 
            MailingCity = 'Wrocław', 
            MailingPostalCode = '00-253', 
            Position__c = 'Programmer', 
            Department_Business_Area__c = 'Finance department',
            Nationality__c = CommonUtility.CONTACT_CITIZENSHIP_POLISH,
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Country_of_Origin__c = 'Poland',
            School_Name__c = 'Szkoła średnia NR 3',
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED
        ), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_II);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        controller.newContact = candidate;
        
        Test.stopTest();
        
        controller.insertContact();
        
        System.assert(!controller.isComplete);
    }
    
    @isTest static void test_insertContact_withCompletionData_withInsertedContact() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            Foreigner__c = false, 
            FirstName = 'Jan', 
            LastName = 'Kowalski', 
            Pesel__c = '44072015724', 
            Email = 'test@test.com', 
            Phone = '123456789', 
            MobilePhone = '123456789', 
            MailingStreet = 'Ruska 3', 
            MailingCity = 'Wrocław', 
            MailingPostalCode = '00-253', 
            Position__c = 'Programmer', 
            Department_Business_Area__c = 'Finance department', 
            A_Level__c = CommonUtility.CONTACT_ALEVEL_PASSED
        ), true);
        
        PageReference pageRef = Page.EnrollmentStudyContactLookupWindow;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('degree', CommonUtility.OFFER_DEGREE_I);
        ApexPages.CurrentPage().getParameters().put('candidateId', candidate.id);
        
        Test.startTest();
        EnrollmentStudyContactLookupWindow controller = new EnrollmentStudyContactLookupWindow();
        
        controller.newContact = candidate;
        
        Test.stopTest();
        
        controller.insertContact();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() != ' ') {
                messageFound = true;   
            }
        }
        System.assert(messageFound);
    }
}