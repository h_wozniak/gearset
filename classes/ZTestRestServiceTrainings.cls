@isTest
private class ZTestRestServiceTrainings {

	@isTest static void test_getTrainingSchedules_withoutId() {
		Test.startTest();

		Boolean errorOccured = false;
		try {
			ScheduleWrapper wrapper = RestServiceTrainings.getTrainingSchedules();
		}
		catch (Exception e) {
			errorOccured = true;
		}

		Test.stopTest();

		System.assert(errorOccured);
	}

	@isTest static void test_getTrainingSchedules_withIncorrectId() {
		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.addParameter('Id', openSchedule.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

		ScheduleWrapper wrapper = RestServiceTrainings.getTrainingSchedules();

		System.assertEquals(wrapper.schedules.size(), 0);
	}

	@isTest static void test_getTrainingSchedules_withId() {
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(null, true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :trainingOffer.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        List<Offer__c> schedules = [SELECT Name, Training_Offer_from_Schedule__c, Training_Offer_from_Schedule__r.Trade_Name__c, Number_of_Seats__c, Price_per_Person__c, 
                                    Price_per_Person_Student_Graduate__c, Valid_From__c, Valid_To__c, Start_Time__c, Training_Place__c, Street__c, City__c 
                                    FROM Offer__c 
                                    WHERE Training_Offer_from_Schedule__r.Name = :trainingId AND Active__c = true 
                                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)];

        System.assertEquals(schedules.size(), 1);

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.addParameter('Id', trainingId);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

		ScheduleWrapper wrapper = RestServiceTrainings.getTrainingSchedules();

		System.assertEquals(wrapper.schedules.size(), 1);
	}

	@isTest static void test_test_postTrainingEnrollment_withoutParticipants() {
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(null, true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :trainingOffer.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        String body = '{"trainingCode":"' + trainingId + '","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":null,"invoiceProforma":false,"attendeeType":false,"type":"Private"}';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.requestBody = Blob.valueof(body);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();

        Boolean errorOccured = false;

    	RestServiceTrainings.ResponseWrapper wrapper = RestServiceTrainings.postTrainingEnrollment();	

    	if (wrapper.message.contains('Check if attendees are in a message')) {
    		errorOccured = true;
    	}

        System.assert(errorOccured);
	}

	@isTest static void test_postTrainingEnrollment_noEntity() {
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(null, true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :trainingOffer.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        String body = '{"trainingCode":"' + trainingId + '","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga2","lastName":"Szkoleniowa2","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"14272400761","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga3","lastName":"Szkoleniowa3","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"77111302039","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga4","lastName":"Szkoleniowa4","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"24043006607","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private"}';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.requestBody = Blob.valueof(body);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        RestServiceTrainings.ResponseWrapper wrapper = RestServiceTrainings.postTrainingEnrollment();
        Test.stopTest();	

        System.assert(wrapper.message.contains('Selected training doesnt have entity'));
	}

	@isTest static void test_postTrainingEnrollment() {
		Account university = ZDataTestUtility.createUniversity(new Account(Name = 'WSB Wrocław'), true);
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(University_from_Training__c = university.Id), true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :openSchedule.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        String body = '{"trainingCode":"' + trainingId + '","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga2","lastName":"Szkoleniowa2","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"14272400761","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga3","lastName":"Szkoleniowa3","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"77111302039","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga4","lastName":"Szkoleniowa4","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"24043006607","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private"}';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.requestBody = Blob.valueof(body);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        RestServiceTrainings.ResponseWrapper wrapper = RestServiceTrainings.postTrainingEnrollment();
        Test.stopTest();	
	}

	@isTest static void test_postTrainingEnrollment_CP_duplicates() {
		Account university = ZDataTestUtility.createUniversity(new Account(Name = 'WSB Wrocław'), true);
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(University_from_Training__c = university.Id), true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :openSchedule.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        String body = '{"trainingCode":"' + trainingId + '","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga2","lastName":"Szkoleniowa2","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"14272400761","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga3","lastName":"Szkoleniowa3","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"77111302039","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga4","lastName":"Szkoleniowa4","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"24043006607","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","contactPersonFirstName":"Jadwiga1","contactPersonLastName":"Szkoleniowa1","contactPersonPhone":"987654321"}';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.requestBody = Blob.valueof(body);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Contact duplicatedContact = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Jadwiga1', LastName = 'Szkoleniowa1', Phone = '987654321'), true);
        
        Test.startTest();
        RestServiceTrainings.ResponseWrapper first_wrapper = RestServiceTrainings.postTrainingEnrollment();
        Test.stopTest();	
	}

	@isTest static void test_postTrainingEnrollment_CP_noDuplicates() {
		Account university = ZDataTestUtility.createUniversity(new Account(Name = 'WSB Wrocław'), true);
		Offer__c trainingOffer = ZDataTestUtility.createTrainingOffer(new Offer__c(University_from_Training__c = university.Id), true);

		Offer__c openSchedule = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = trainingOffer.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        Offer__c queriedTrainingOffer = [
        	SELECT Id, Name
        	FROM Offer__c
        	WHERE Id = :openSchedule.Id
        ];

        String trainingId = queriedTrainingOffer.Name;

        String body = '{"trainingCode":"' + trainingId + '","voucher":"","sameAsContact":true,"invoiceAfterPayment":false,"invoiceDeliveryChannel":"Wysyłka na adres email","invoiceFirstName":"","invoiceLastName":"","invoiceCity":"","invoicePostalCode":"","invoiceStreet":"","invoiceDescription":"","source":"","futureTrainingInfo":"","portalRegulationsAgreement":true,"personalDataProcessingAgreement":false,"commercialInfoAgreement":false,"contactAgreement":false,"attendees":[{"firstName":"Jadwiga1","lastName":"Szkoleniowa1","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"29020703425","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga2","lastName":"Szkoleniowa2","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"14272400761","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga3","lastName":"Szkoleniowa3","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"77111302039","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""},{"firstName":"Jadwiga4","lastName":"Szkoleniowa4","phone":"987654321","email":"sebastian.lasisz@enxoo.com","position":"","department":"","pesel":"24043006607","mailingStreet":"x","mailingCity":"x","mailingState":"","mailingPostalCode":"27-200","mailingCountry":""}],"invoiceProforma":false,"attendeeType":false,"type":"Private","contactPersonFirstName":"Jadwiga15","contactPersonLastName":"Szkoleniowa005","contactPersonPhone":"444654321"}';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/trainings/';
        req.requestBody = Blob.valueof(body);

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Contact duplicatedContact = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Sebastian10', LastName = 'Sebastian10', Phone = '1234'), true);
        
        Test.startTest();
        RestServiceTrainings.ResponseWrapper first_wrapper = RestServiceTrainings.postTrainingEnrollment();
        Test.stopTest();	
	}
}