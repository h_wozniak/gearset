/**
*   @author         Radosław Urbański
*   @description    This class is used check that Contact exist in CRM for given parameters.
**/

@RestResource(urlMapping = '/enrollment-required-documents/*')
global without sharing class IntegrationRequiredDocuments {

	/* ------------------------------------------------------------------------------------------------ */
	/* ---------------------------------------- HTTP METHODS ------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------ */

	@HttpGET
	global static void postRequiredDocuments() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String enrollmentId = req.params.get('enrollment_id');

		if (enrollmentId == null) {
			res.statusCode = 400;
			res.responseBody = IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id');
			return;
		} else {
			try {
				res.statusCode = 400;
				if ([SELECT Id FROM Enrollment__c WHERE Id = :enrollmentId].size() == 0) {
					res.responseBody = IntegrationError.getErrorJSONBlobByCode(604);
				} else {
					Map<Id, Enrollment__c> documentsList = new Map<Id, Enrollment__c>([SELECT Id, Document__r.Name, Document_Delivered__c, Document_Accepted__c, Delivery_Deadline__c 
						FROM Enrollment__c 
						WHERE Enrollment_from_Documents__c = :enrollmentId]);

					// create map of documents and their attachments
					Set<Id> enrollemntsIdSet = new Set<Id>();
					for (Enrollment__c e : documentsList.values()) {
						enrollemntsIdSet.add(e.Id);
					}

					List<Attachment> attachmentsList = [SELECT Name, Body, CreatedDate, ParentId FROM Attachment WHERE ParentId IN :enrollemntsIdSet];

					Map<Id, List<Attachment>> documentAttachmentMap = new Map<Id, List<Attachment>>();
					
					for (Attachment att : attachmentsList) {
						if (documentAttachmentMap.containsKey(att.parentId)) {
							List<Attachment> attList = documentAttachmentMap.get(att.parentId);
							attList.add(att);
						} else {
							List<Attachment> attList = new List<Attachment>();
							attList.add(att);
							documentAttachmentMap.put(att.parentId, attList);
						}
					}

					// generate response
					List<RequiredDocument_JSON> documentsToSendList = new List<RequiredDocument_JSON>();

					for (Enrollment__c document : documentsList.values()) {
						Attachment generatedFile;
						Attachment uploadedFile;

						if (documentAttachmentMap.get(document.Id) != null && documentAttachmentMap.get(document.Id).size() > 0) {
							for (Attachment att : documentAttachmentMap.get(document.Id)) {
								if ((generatedFile == null && !att.Name.startsWith(CommonUtility.UPLOADED_DOCUMENT_PREFIX)) 
									|| (generatedFile != null && !att.Name.startsWith(CommonUtility.UPLOADED_DOCUMENT_PREFIX) && generatedFile.CreatedDate < att.CreatedDate)) {
									generatedFile = att;
								} else if ((uploadedFile == null && att.Name.startsWith(CommonUtility.UPLOADED_DOCUMENT_PREFIX)) 
									|| (uploadedFile != null && att.Name.startsWith(CommonUtility.UPLOADED_DOCUMENT_PREFIX) && uploadedFile.CreatedDate < att.CreatedDate)) {
									uploadedFile = att;
								}
							}
						}

						String deliveryDeadlineString = null;
						if (document.Delivery_Deadline__c != null) {
							deliveryDeadlineString = String.valueOf(document.Delivery_Deadline__c);
						}

						RequiredDocument_JSON documentToSend = new RequiredDocument_JSON(String.valueOf(document.Id), document.Document__r.Name, generatedFile,
						        uploadedFile, deliveryDeadlineString, document.Document_Delivered__c, document.Document_Accepted__c);
						documentsToSendList.add(documentToSend);
					}
					res.statusCode = 200;
					res.responseBody = Blob.valueOf(JSON.serialize(documentsToSendList));
				}
			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
			}
		}
	}

	/* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class RequiredDocument_JSON {
        public String document_id;
        public String document_title;
        public String generated_file_name;
        public Blob generated_file_content;
        public String uploaded_file_name;
        public Blob uploaded_file_content;
        public String required_delivery_date;
        public Boolean delivered;
        public Boolean accepted;

        public RequiredDocument_JSON(String document_id, String document_title, Attachment generated_file, Attachment uploaded_file, 
        	String required_delivery_date, Boolean delivered, Boolean accepted) {
			this.document_id = document_id;
			this.document_title = document_title;
			if (generated_file != null) {
				this.generated_file_name = generated_file.Name;
				this.generated_file_content = generated_file.Body;
			}
			if (uploaded_file != null) {
				this.uploaded_file_name = uploaded_file.Name;
				this.uploaded_file_content = uploaded_file.Body;
			}
			this.required_delivery_date = required_delivery_date;
			this.delivered = delivered;
			this.accepted = accepted;
        }
    }
}