@isTest
global class ZTestCampaignDetailController {

    @isTest public static void test_CampaignDetailController() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();

        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('marketingCampaign', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        Test.stopTest();
    }

    @isTest public static void test_pageRender() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('marketingCampaign', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        cdc.pageRender();
        Test.stopTest();
    }

    @isTest public static void test_campaignMemberFields() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('campaignId', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        Test.stopTest();

        System.assertEquals(cdc.campaignMembersFields.size(), 4);
    }

    @isTest public static void test_campaignDetailsFields() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('campaignId', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        Test.stopTest();

        System.assertEquals(cdc.campaignDetailsFields.size(), 2);
    }

    @isTest public static void test_enrollmentResignationFields() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('campaignId', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        Test.stopTest();

        System.assertEquals(cdc.enrollmentResignationFields.size(), 2);
    }

    @isTest public static void test_campaignDetailsMapToString() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('marketingCampaign', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        Test.stopTest();

        System.assertEquals(cdc.campaignMemberLookUps, new List<String>{
                'Campaign_Details__c', 'Campaign_from_Campaign_Details_2__c'
        });
    }

    @isTest public static void test_save() {
        CommonUtility.preventDetailsCreation = true;

        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(null, true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
                new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
                ), true);

        ApexPages.CurrentPage().getParameters().put('marketingCampaign', marketing.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', member.Id);

        Test.startTest();
        CampaignDetailController cdc = new CampaignDetailController();
        cdc.save();
        Test.stopTest();
    }
}