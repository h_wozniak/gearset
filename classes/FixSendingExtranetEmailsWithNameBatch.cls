/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with extranet emails
**/

/* ------------------------------------------ example --------------------------------------------
Database.executeBatch(
    new FixSendingExtranetEmailsWithNameBatch(),
    1
);
------------------------------------------- end of example ------------------------------------- */

global class FixSendingExtranetEmailsWithNameBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    Set<String> enrollmentNames;

	global FixSendingExtranetEmailsWithNameBatch(Set<String> enrollmentNames) { 
        this.enrollmentNames = enrollmentNames;   
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        	SELECT ID, TECH_Extranet_Data_Update_Date__c 
        	FROM Enrollment__c 
        	WHERE (Status__c = 'Didactics in KS' OR Status__c = 'Payments in KS') 
        	AND Educational_Agreement__r.Student_No__c != null 
        	AND Educational_Agreement__r.Portal_Login__c != null 
            AND Name IN :enrollmentNames
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Enrollment__c> studyEnrollments) { 
		Set<Id> enrollmentIds = new Set<Id>();
		for (Enrollment__c studyEnrollment : studyEnrollments) {
			enrollmentIds.add(studyEnrollment.Id);
		}

        EmailManager_StudyExtranetEmail.sendExtranetInformationnEmail(enrollmentIds);
	}

	global void finish(Database.BatchableContext BC) { }
}