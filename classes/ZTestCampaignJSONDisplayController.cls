@isTest
private class ZTestCampaignJSONDisplayController {

	@isTest static void test_CampaignJSONDisplayController() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c campaign = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_JSON__c = '{"hello world" : "yello"}'), true);

		PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getParameters().put('campaignId', campaign.Id);

		Test.startTest();
		CampaignJSONDisplayController cntrl = new CampaignJSONDisplayController();
		System.assert(cntrl.campaignJSON != null);
		Test.stopTest();
	}

	@isTest static void test_CampaignJSONDisplayController_noData() {
		CustomSettingDefault.initDidacticsCampaign();
		Marketing_Campaign__c campaign = ZDataTestUtility.createCampaignMember(null, true);

		PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getParameters().put('campaignId', campaign.Id);

		Test.startTest();
		try {
			CampaignJSONDisplayController cntrl = new CampaignJSONDisplayController();
		}
		catch (Exception e) {
			System.assertEquals(e.getMessage(), '');
		}
		Test.stopTest();
	}
}