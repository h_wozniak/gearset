/**
* @author       Wojciech Słodziak
* @description  Class for storing methods related to Offer__c object for Studies Record Types
**/


public without sharing class OfferManager_Studies {
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ HELPER VALUE SETS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static final Set<Id> studyRecordTypeIds = new Set<Id> {
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_UNIVERSITY_OFFER_STUDY),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_REQUIRED_DOCUMENT),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_PB_INSTALLMENT_CFG),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG),
        CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA)
    };



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ NAME GENERATION ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * @author Sebastian Lasisz     *
     * Method responsible for setting Code_Helper_Num__c and Name for Additional Documents on Offer.
     *
     * @param offerIdsToSetCodeForDoc Set of offer Ids to determine Code Helper Num responsible for creating unique Name.
     */
    public static void setCodeHelperNumAndNameForAdditionalDocument(Set<Id> offerIdsToSetCodeForDoc) {
        List<Offer__c> docList = [
            SELECT Id, Name, Code_Helper_Num__c, Offer_from_Additional_Document__c, Offer_from_Additional_Document__r.Name 
            FROM Offer__c 
            WHERE Offer_from_Additional_Document__c IN :offerIdsToSetCodeForDoc 
            ORDER BY Code_Helper_Num__c DESC NULLS LAST
        ];

        List<Offer__c> docsToUpdate = new List<Offer__c>();

        for (Id offId : offerIdsToSetCodeForDoc) {
            Decimal maxCode = 0;
            for (Offer__c doc : docList) {
                if (doc.Offer_from_Additional_Document__c == offId) {
                    if (doc.Code_Helper_Num__c != null && doc.Code_Helper_Num__c > maxCode) {
                        maxCode = doc.Code_Helper_Num__c;
                    } else {
                        if (doc.Code_Helper_Num__c == null) {
                            doc.Code_Helper_Num__c = ++maxCode;
                            docsToUpdate.add(doc);
                        }
                    }
                }
            }
        }

        for (Offer__c docToUpdate : docsToUpdate) {
            docToUpdate.Name = docToUpdate.Offer_from_Additional_Document__r.Name + '_AD' + String.valueOf(docToUpdate.Code_Helper_Num__c).leftPad(2).replaceAll(' ', '0');
        }

        try {
            CommonUtility.skipNameEditDisabling = true;
            update docsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // Generate Name for University Study Offer
    public static void generateNameForUniversityStudyOffer(List<Offer__c> univOffStToSetName) {
        Set<Id> univerIdList = new Set<Id>();
        for (Offer__c uos : univOffStToSetName) {
            univerIdList.add(uos.University__c);
        }

        List<Account> univerList = [
            SELECT Id, Name 
            FROM Account 
            WHERE Id IN :univerIdList 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
        ];

        for (Offer__c uos : univOffStToSetName) {
            for (Account uni : univerList) {
                if (uos.University__c == uni.Id) {
                    String univerName3Char = uni.Name.substring(4, 7).toUpperCase();
                    String dateString = uos.Study_Start_Date__c.month() + '_' + uos.Study_Start_Date__c.year();
                    uos.Name = univerName3Char + '_' + CommonUtility.getPicklistLabelMap(Offer__c.Degree__c).get(uos.Degree__c) + '_' + dateString;
                }
            }
        } 
    }

    /* Wojciech Słodziak */
    // Generate Name for Course Offer
    public static void generateNameForCourseOffer(List<Offer__c> courseOffToSetName) {
        CustomSettingDefault.initStudyAbbreviations();
        Set<Id> uosIdList = new Set<Id>();
        for (Offer__c course : courseOffToSetName) {
            uosIdList.add(course.University_Study_Offer_from_Course__c);
        }

        List<Offer__c> uosList = [
            SELECT Id, Name 
            FROM Offer__c 
            WHERE Id IN :uosIdList
        ];

        for (Offer__c course : courseOffToSetName) {
            for (Offer__c uos : uosList) {
                if (uos.Id == course.University_Study_Offer_from_Course__c) {
                    StudyKind_Abbreviation__c kindAbbreviation = StudyKind_Abbreviation__c.getInstance(course.Kind__c);
                    StudyMode_Abbreviation__c modeAbbreviation = StudyMode_Abbreviation__c.getInstance(course.Mode__c);
                    if (kindAbbreviation == null || modeAbbreviation == null) {
                        course.addError(Label.msg_error_CustomSettingUndefinedAbbrev);    
                    } else {
                        course.Name = uos.Name + '_' + course.Short_Name__c + '_' + kindAbbreviation.Abbreviation__c + '_' + modeAbbreviation.Abbreviation__c;
                    }
                }
            }
        } 
    }

    /* Wojciech Słodziak */
    // Generate Name for Course Specialty Offer
    public static void generateNameForCourseSpecialtyOffer(List<Offer__c> specOffToSetName) {
        Set<Id> courseIdList = new Set<Id>();
        for (Offer__c spec : specOffToSetName) {
            courseIdList.add(spec.Course_Offer_from_Specialty__c);
        }

        List<Offer__c> courseList = [
            SELECT Id, Name 
            FROM Offer__c 
            WHERE Id IN :courseIdList
        ];

        for (Offer__c spec : specOffToSetName) {
            for (Offer__c course : courseList) {
                if (course.Id == spec.Course_Offer_from_Specialty__c) {
                    spec.Name = course.Name + '_' + spec.Short_Name__c;
                }
            }
        } 
    }

    /**
     * @author Sebastian Lasisz
     * Method responsible for generating enrollment link id used in external system that will let users to enroll
     * for Guided Group offers. To generate the MD5 key function uses random Integer as salt and offer's Name as key.
     *
     * @param offersToSetEnrollmentLink Course or Specialty offers to generate enrollment link id.
     */
    public static void generateEnrollmentLinkForGuidedGroupOffer(List<Offer__c> offersToSetEnrollmentLink) {
        for (Offer__c offer : offersToSetEnrollmentLink) {
            String salt = String.valueOf(Crypto.getRandomInteger());
            String key = salt + offer.Name;
            offer.Enrollment_Link_Id__c = EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(key)));
        }
    }

    /* Wojciech Słodziak */
    // Generate Name for PriceBookInstallmentConfig
    public static void generateNameForPriceBookInstallmentConfig(List<Offer__c> installmentConfigsToSetName) {
        Set<Id> priceBookIdList = new Set<Id>();
        for (Offer__c inst : installmentConfigsToSetName) {
            priceBookIdList.add(inst.Price_Book_from_Installment_Config__c);
        }

        List<Offer__c> priceBookList = [
            SELECT Id, Name 
            FROM Offer__c 
            WHERE Id IN :priceBookIdList
        ];

        for (Offer__c inst : installmentConfigsToSetName) {
            for (Offer__c priceBook : priceBookList) {
                if (priceBook.Id == inst.Price_Book_from_Installment_Config__c) {
                    inst.Name = priceBook.Name + '_R' + inst.Installment_Variant__c;
                }
            }
        } 
    }

    /* Wojciech Słodziak */
    // set Code_Helper_Num__c and Name for PriceBook on Offer
    public static void setCodeHelperNumAndNameForPriceBookOnOffer(Set<Id> offerIdsToSetCodeForPB) {
        List<Offer__c> pbList = [
            SELECT Id, Name, Code_Helper_Num__c, Offer_from_Price_Book__c, Offer_from_Price_Book__r.Name 
            FROM Offer__c 
            WHERE Offer_from_Price_Book__c IN :offerIdsToSetCodeForPB 
            ORDER BY Code_Helper_Num__c DESC NULLS LAST
        ];

        List<Offer__c> pbsToUpdate = new List<Offer__c>();

        for (Id offerId : offerIdsToSetCodeForPB) {
            Decimal maxCode = 0;
            for (Offer__c pb : pbList) {
                if (pb.Offer_from_Price_Book__c == offerId) {
                    if (pb.Code_Helper_Num__c != null && pb.Code_Helper_Num__c > maxCode) {
                        maxCode = pb.Code_Helper_Num__c;
                    } else {
                        if (pb.Code_Helper_Num__c == null) {
                            pb.Code_Helper_Num__c = ++maxCode;
                            pbsToUpdate.add(pb);
                        }
                    }
                }
            }
        }

        for (Offer__c pbToUpdate : pbsToUpdate) {
            pbToUpdate.Name = pbToUpdate.Offer_from_Price_Book__r.Name + '_C' + String.valueOf(pbToUpdate.Code_Helper_Num__c).leftPad(2).replaceAll(' ', '0');
        }

        try {
            CommonUtility.skipNameEditDisabling = true;
            update pbsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // set Code_Helper_Num__c and Name for PriceBook on Course Offer
    public static void setCodeHelperNumAndNameForPriceBookOnCourseOffer(Set<Id> offerIdsToSetCodeForDoc) {
        List<Offer__c> docList = [
            SELECT Id, Name, Code_Helper_Num__c, Offer_from_Document__c, Offer_from_Document__r.Name 
            FROM Offer__c 
            WHERE Offer_from_Document__c IN :offerIdsToSetCodeForDoc 
            ORDER BY Code_Helper_Num__c DESC NULLS LAST
        ];

        List<Offer__c> docsToUpdate = new List<Offer__c>();

        for (Id offId : offerIdsToSetCodeForDoc) {
            Decimal maxCode = 0;
            for (Offer__c doc : docList) {
                if (doc.Offer_from_Document__c == offId) {
                    if (doc.Code_Helper_Num__c != null && doc.Code_Helper_Num__c > maxCode) {
                        maxCode = doc.Code_Helper_Num__c;
                    } else {
                        if (doc.Code_Helper_Num__c == null) {
                            doc.Code_Helper_Num__c = ++maxCode;
                            docsToUpdate.add(doc);
                        }
                    }
                }
            }
        }

        for (Offer__c docToUpdate : docsToUpdate) {
            docToUpdate.Name = docToUpdate.Offer_from_Document__r.Name + '_D' + String.valueOf(docToUpdate.Code_Helper_Num__c).leftPad(2).replaceAll(' ', '0');
        }

        try {
            CommonUtility.skipNameEditDisabling = true;
            update docsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // set Code_Helper_Num__c and Name for Language on Course Offer
    public static void setCodeHelperNumAndNameForLanguageOnCourseOffer(Set<Id> offerIdsToSetCodeForLang) {
        List<Offer__c> langList = [
            SELECT Id, Name, Code_Helper_Num__c, Course_Offer_from_Language__c, Course_Offer_from_Language__r.Name 
            FROM Offer__c 
            WHERE Course_Offer_from_Language__c IN :offerIdsToSetCodeForLang 
            ORDER BY Code_Helper_Num__c DESC NULLS LAST
        ];

        List<Offer__c> langsToUpdate = new List<Offer__c>();

        for (Id offId : offerIdsToSetCodeForLang) {
            Decimal maxCode = 0;
            for (Offer__c langauge : langList) {
                if (langauge.Course_Offer_from_Language__c == offId) {
                    if (langauge.Code_Helper_Num__c != null && langauge.Code_Helper_Num__c > maxCode) {
                        maxCode = langauge.Code_Helper_Num__c;
                    } else {
                        if (langauge.Code_Helper_Num__c == null) {
                            langauge.Code_Helper_Num__c = ++maxCode;
                            langsToUpdate.add(langauge);
                        }
                    }
                }
            }
        }

        for (Offer__c langToUpdate : langsToUpdate) {
            langToUpdate.Name = langToUpdate.Course_Offer_from_Language__r.Name + '_J' + String.valueOf(langToUpdate.Code_Helper_Num__c).leftPad(2).replaceAll(' ', '0');
        }

        try {
            CommonUtility.skipNameEditDisabling = true;
            update langsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Wojciech Słodziak */
    // set Code_Helper_Num__c and Name for Specialization on Specialty
    public static void setCodeHelperNameAndNameForSpecializationOnSpecialty(Set<Id> offerIdsToSetCodeForSpecialization) {
        List<Offer__c> specializationList = [
            SELECT Id, Name, Code_Helper_Num__c, Offer_from_Specialization__c, Offer_from_Specialization__r.Name 
            FROM Offer__c 
            WHERE Offer_from_Specialization__c IN :offerIdsToSetCodeForSpecialization 
            ORDER BY Code_Helper_Num__c DESC NULLS LAST
        ];

        List<Offer__c> specializationsToUpdate = new List<Offer__c>();

        for (Id offId : offerIdsToSetCodeForSpecialization) {
            Decimal maxCode = 0;
            for (Offer__c specialization : specializationList) {
                if (specialization.Offer_from_Specialization__c == offId) {
                    if (specialization.Code_Helper_Num__c != null && specialization.Code_Helper_Num__c > maxCode) {
                        maxCode = specialization.Code_Helper_Num__c;
                    } else {
                        if (specialization.Code_Helper_Num__c == null) {
                            specialization.Code_Helper_Num__c = ++maxCode;
                            specializationsToUpdate.add(specialization);
                        }
                    }
                }
            }
        }

        for (Offer__c specializationToUpdate : specializationsToUpdate) {
            specializationToUpdate.Name = specializationToUpdate.Offer_from_Specialization__r.Name + '_S' + String.valueOf(specializationToUpdate.Code_Helper_Num__c).leftPad(2).replaceAll(' ', '0');
        }

        try {
            CommonUtility.skipNameEditDisabling = true;
            update specializationsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    public static void generateOfferNumber(List<Offer__c> courseOfferList) {
        Set<Id> resultIds = new Set<Id>();
        for(Offer__c offer :courseOfferList){
            resultIds.add(offer.University_Study_Offer_from_Course__c);
        }

        Map<Id, Offer__c> records = new Map<Id, Offer__c>([SELECT Id, Study_Start_Date__c FROM Offer__c WHERE Id IN :resultIds]);

        for (Offer__c courseOffer : courseOfferList) {
            Date dateToCheck = records.get(courseOffer.University_Study_Offer_from_Course__c).Study_Start_Date__c;
            courseOffer.Offer_Number__c = 10;
            if (courseOffer.Semester_Type__c == CommonUtility.CATALOG_PERIOD_WINTER) {
                courseOffer.Offer_Number__c = courseOffer.Offer_Number__c + (dateToCheck.year() - 2018) * 2 - 1;
            } else {
                courseOffer.Offer_Number__c = courseOffer.Offer_Number__c + (dateToCheck.year() - 2018) * 2;
            }
        }
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- STATISTIC METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method usable for updating Course / Specialty statistics on various events (by trigger)
    public static void updateOfferStatistics(Set<Id> offersToUpdateStatistics) {
        List<Offer__c> offerList = [
            SELECT Id, Candidates__c, Accepted_Candidates__c,
            (
                SELECT Id, Status__c, Guided_group__c
                FROM Enrollments__r 
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND Annexing__c = false
                AND Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION
            )
            FROM Offer__c 
            WHERE Id IN :offersToUpdateStatistics
            FOR UPDATE
        ];

        Set<String> candidateStatuses = new Set<String> { CommonUtility.ENROLLMENT_STATUS_CONFIRMED, CommonUtility.ENROLLMENT_STATUS_COD, 
                                                          CommonUtility.ENROLLMENT_STATUS_DC, CommonUtility.ENROLLMENT_STATUS_ACCEPTED };
        Set<String> acceptedStatuses = new Set<String> { CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS, CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS };

        for (Offer__c offer : offerList) {
            offer.Candidates__c = 0;
            offer.Accepted_Candidates__c = 0;
            offer.Candidates_Guided_group__c = 0;
            offer.Accepted_Candidates_Guided_group__c = 0;

            for (Enrollment__c enr : offer.Enrollments__r) {
                if (!enr.Guided_group__c) {
                    if (candidateStatuses.contains(enr.Status__c)) {
                        offer.Candidates__c++;
                    }

                    if (acceptedStatuses.contains(enr.Status__c)) {
                        offer.Accepted_Candidates__c++;
                    }
                }
                else {
                    if (candidateStatuses.contains(enr.Status__c)) {
                        offer.Candidates_Guided_group__c++;
                    }

                    if (acceptedStatuses.contains(enr.Status__c)) {
                        offer.Accepted_Candidates_Guided_group__c++;
                    }                        
                }
            }
        }

        try {
            update offerList;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Wojciech Słodziak */
    // method usable for updating Course statistics when it is recalculated on Specialty (by trigger)
    public static void updateCourseStatistics(Set<Id> coursesToUpdateStatistics) {
        List<Offer__c> courseList = [
            SELECT Id, Candidates__c, Accepted_Candidates__c, Candidates_Guided_group__c, Accepted_Candidates_Guided_group__c,
                (SELECT Id, Candidates__c, Accepted_Candidates__c, Candidates_Guided_group__c, Accepted_Candidates_Guided_group__c
                FROM Specialties__r)
            FROM Offer__c 
            WHERE Id IN :coursesToUpdateStatistics
            FOR UPDATE
        ];

        for (Offer__c course : courseList) {
            course.Candidates__c = 0;
            course.Accepted_Candidates__c = 0;
            course.Candidates_Guided_group__c = 0;
            course.Accepted_Candidates_Guided_group__c = 0;

            for (Offer__c spec : course.Specialties__r) {
                course.Candidates__c += spec.Candidates__c;
                course.Accepted_Candidates__c += spec.Accepted_Candidates__c;
                course.Candidates_Guided_group__c += (spec.Candidates_Guided_group__c != null ? spec.Candidates_Guided_group__c : 0);
                course.Accepted_Candidates_Guided_group__c += (spec.Accepted_Candidates_Guided_group__c != null ? spec.Accepted_Candidates_Guided_group__c : 0);
            }
        }

        try {
            update courseList;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Wojciech Słodziak */
    // method usable for updating University Study Offer statistics when it is recalculated on Course (by trigger)
    public static void updateUnivOffStudyStatistics(Set<Id> univOffStudiesToUpdateStatistics) {
        List<Offer__c> univOffStudyList = [
            SELECT Id, Candidates__c, Accepted_Candidates__c, Candidates_Guided_group__c, Accepted_Candidates_Guided_group__c,
                (SELECT Id, Candidates__c, Accepted_Candidates__c, Candidates_Guided_group__c, Accepted_Candidates_Guided_group__c
                FROM Courses__r)
            FROM Offer__c 
            WHERE Id IN :univOffStudiesToUpdateStatistics
            FOR UPDATE
        ];

        for (Offer__c univOffStudy : univOffStudyList) {
            univOffStudy.Candidates__c = 0;
            univOffStudy.Accepted_Candidates__c = 0;
            univOffStudy.Candidates_Guided_group__c = 0;
            univOffStudy.Accepted_Candidates_Guided_group__c = 0;

            for (Offer__c course : univOffStudy.Courses__r) {
                univOffStudy.Candidates__c += course.Candidates__c;
                univOffStudy.Accepted_Candidates__c += course.Accepted_Candidates__c;
                univOffStudy.Candidates_Guided_group__c += (course.Candidates_Guided_group__c != null ? course.Candidates_Guided_group__c : 0);
                univOffStudy.Accepted_Candidates_Guided_group__c += (course.Accepted_Candidates_Guided_group__c != null ? course.Accepted_Candidates_Guided_group__c : 0);
            }
        }

        try {
            update univOffStudyList;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }


    /* Wojciech Słodziak */
    // recalculate number of specialties on Course
    public static void recalculateNumberOfSpecialtiesOnCourse(Set<Id> coursesToCountSpecialties) {
        List<Offer__c> specList = [
            SELECT Id, Course_Offer_from_Specialty__c 
            FROM Offer__c 
            WHERE Course_Offer_from_Specialty__c IN :coursesToCountSpecialties 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
        ];

        List<Offer__c> courseListToUpdate = new List<Offer__c>();

        for (Id cId : coursesToCountSpecialties) {
            Decimal count = 0;
            for (Offer__c spec : specList) {
                if (cId == spec.Course_Offer_from_Specialty__c) {
                    count++;
                }
            }

            Offer__c c = new Offer__c(Id = cId, Number_of_Specialties__c = count);
            courseListToUpdate.add(c);
        }

        try {
            update courseListToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

    /* Sebastian Łasisz */
    // calculate number of pricebooks on Offer
    public static void calculateNumberOfPriceBooks(Set<Id> courseOfferIdsToCalculateNumberOfPriceBooks) {
        List<Offer__c> offersToUpdate = [
            SELECT Id, Price_Book_Counter__c, (SELECT Id, Price_Book_Type__c, Active__c FROM PriceBooks__r)
            FROM Offer__c
            WHERE Id IN :courseOfferIdsToCalculateNumberOfPriceBooks
        ];

        for (Offer__c offer : offersToUpdate) {
            offer.Price_Book_Counter__c = offer.PriceBooks__r.size();
            Integer counter = 0;
            for (Offer__c pb : offer.PriceBooks__r) {
                if (pb.Price_Book_Type__c == CommonUtility.OFFER_PBTYPE_STANDARD && pb.Active__c) {
                    counter++;
                }
            }
            offer.Standard_Active_PriceBook_Counter__c = counter;
        }

        try {
            update offersToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

    /* Sebastian Łasisz */
    // calculate number of languages on Offer
    public static void calculateNumberOfLanguages(Set<Id> courseOfferIdsToCalculateNumberOfLanguages) {
        List<Offer__c> offersToUpdate = [
            SELECT Id, TECH_Language_Counter__c, (SELECT Id FROM Languages__r)
            FROM Offer__c
            WHERE Id IN :courseOfferIdsToCalculateNumberOfLanguages
        ];

        for (Offer__c offer : offersToUpdate) {
            offer.TECH_Language_Counter__c = offer.Languages__r.size();
        }

        try {
            update offersToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- VALIDATIONS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // Check if the same course is not added as another Specialization
    public static void checkIfCourseIsNotAddedAsSpecialization(List<Offer__c> specializationsToCheckDuplication) {
        Set<Id> specializationToCheckIdList = new Set<Id>();
        Set<Id> offerIdList = new Set<Id>();
        for (Offer__c specialization : specializationsToCheckDuplication) {
            specializationToCheckIdList.add(specialization.Id);
            offerIdList.add(specialization.Offer_from_Specialization__c);
        }
        List<Offer__c> specializationList = [SELECT Id, Specialization__c
                                             FROM Offer__c 
                                             WHERE Offer_from_Specialization__c IN :offerIdList AND Id NOT IN :specializationToCheckIdList];

        specializationList.addAll(specializationsToCheckDuplication);

        for (Offer__c specialization : specializationsToCheckDuplication) {
            for (Offer__c otherS : specializationList) {
                if (specialization !== otherS && (specialization.Id == null || specialization.Id != otherS.Id) && specialization.Specialization__c == otherS.Specialization__c) {
                    specialization.addError(' ' + Label.msg_error_SpecializationDuplicate);
                }
            }
        }
    }

    /* Sebastian Łasisz */
    // Check if the same foreign language is not added as nother language on offer
    public static void checkIfLanguageIsNotAddedOnOffer(List<Offer__c> foreignLanguagesToCheckDuplication) {
        Set<Id> courseOffersIdList = new Set<Id>();
        for (Offer__c foreignLanguageToCheckDuplication : foreignLanguagesToCheckDuplication) {
            courseOffersIdList.add(foreignLanguageToCheckDuplication.Course_Offer_from_Language__c);
        }

        List<Offer__c> languageOffers = [
            SELECT Course_Offer_from_Language__c, Language__c
            FROM Offer__c
            WHERE Course_Offer_from_Language__c IN :courseOffersIdList
        ];

        for (Offer__c foreignLanguageToCheckDuplication : foreignLanguagesToCheckDuplication) {
            for (Offer__c languageOffer : languageOffers) {
                if (languageOffer.Course_Offer_from_Language__c == foreignLanguageToCheckDuplication.Course_Offer_from_Language__c 
                    && languageOffer.Language__c == foreignLanguageToCheckDuplication.Language__c) {
                    foreignLanguageToCheckDuplication.addError(' ' + Label.msg_error_LanguageDuplicate);
                }
            }
        }
    }

    /* Sebastian Łasisz */
    // check if user got privilages to add university offer study
    public static void checkIfUserCanAddUniversityOffer(List<Offer__c> courseOffersToCheckIfCanInsertRecord) {
        User u = [SELECT Id, WSB_Department__c, UserRole.Name, UserRole.DeveloperName FROM User WHERE Id = :UserInfo.getUserId()];

        Set<Id> universityIds = new Set<Id>();
        for (Offer__c courseOfferToCheckIfCanInsertRecord : courseOffersToCheckIfCanInsertRecord) {
            universityIds.add(courseOfferToCheckIfCanInsertRecord.University__c);
        }

        List<Account> universities = [
            SELECT Id, Name
            FROM Account
            WHERE Id IN :universityIds
        ];

        for (Offer__c courseOfferToCheckIfCanInsertRecord : courseOffersToCheckIfCanInsertRecord) {
            for (Account university : universities) {
                if (university.Id == courseOfferToCheckIfCanInsertRecord.University__c) { 

                    if ((u.WSB_Department__c != 'Integration' && u.WSB_Department__c != 'Administrator') 
                    && u.UserRole.DeveloperName != CommonUtility.WSB_ROLE_AKADEMIA)  {

                        Boolean ifError = true;
                        if (u.WSB_Department__c == RecordVals.WSB_NAME_WRO) {
                            if (university.Name != RecordVals.WSB_NAME_WRO && university.Name != RecordVals.WSB_NAME_OPO) {
                                courseOfferToCheckIfCanInsertRecord.addError(Label.msg_error_InsufficientPrivilegesForOffer);
                            }
                            else {
                                ifError = false;
                            }
                        }
                        else if (u.WSB_Department__c == RecordVals.WSB_NAME_GDA) {
                            if (university.Name != RecordVals.WSB_NAME_GDA && university.Name != RecordVals.WSB_NAME_GDY) {
                                courseOfferToCheckIfCanInsertRecord.addError(Label.msg_error_InsufficientPrivilegesForOffer);
                            }
                            else {
                                ifError = false;
                            }
                        }
                        else if (u.WSB_Department__c == RecordVals.WSB_NAME_POZ) {
                            if (university.Name != RecordVals.WSB_NAME_POZ && university.Name != RecordVals.WSB_NAME_SZC && university.Name != RecordVals.WSB_NAME_CHO) {
                                courseOfferToCheckIfCanInsertRecord.addError(Label.msg_error_InsufficientPrivilegesForOffer);
                            }
                            else {
                                ifError = false;
                            }
                        }
                        else if (u.WSB_Department__c == RecordVals.WSB_NAME_TOR) {
                            if (university.Name != RecordVals.WSB_NAME_BYD && university.Name != RecordVals.WSB_NAME_TOR) {
                                courseOfferToCheckIfCanInsertRecord.addError(Label.msg_error_InsufficientPrivilegesForOffer);
                            }
                            else {
                                ifError = false;
                            }
                        }
                        else {
                            if (ifError && u.WSB_Department__c != university.Name) {
                                courseOfferToCheckIfCanInsertRecord.addError(Label.msg_error_InsufficientPrivilegesForOffer);
                            }
                        }
                    }
                }
            }
        }
    }

    /* Wojciech Słodziak */
    // duplicate Detection (KEEP AT BOTTOM)
    public static void duplicateDetection(List<Offer__c> offersToCheckDuplicates) {
        Set<String> nameList = new Set<String>();
        for (Offer__c o : offersToCheckDuplicates) {
            nameList.add(o.Name);
        }

        List<Offer__c> potentialDuplicates = [
            SELECT Id, Name 
            FROM Offer__c 
            WHERE Name IN :nameList
        ];

        for (Offer__c o : offersToCheckDuplicates) {
            for (Offer__c dup : potentialDuplicates) {
                if (o.Id != dup.Id && o.Name == dup.Name) {
                    if(!CommonUtility.isLightningEnabled()) {
                        o.addError(' ' + Label.msg_error_DupFound + ': <a target="_blank" href="/' + dup.Id + '">' + dup.Name + '</a>', false);
                    } else {
                        o.addError(' ' + Label.msg_error_DupFound + ': ' + dup.Id + ' - ' + dup.Name, false);
                    }
                }
            }
        }
    }

    public static void setHigherSemesterAvailability(List<Offer__c> offersToSetAvailability) {
        for(Offer__c offer: offersToSetAvailability){
            Integer numberOfSemesters = Integer.valueOf(offer.Number_of_Semesters__c);
            List<Date> semesterDates = crateListOfSemesterDates(offer, numberOfSemesters);
            Date validationDate = system.today().addMonths(6);
            Boolean datesAreEmpty = areDatesEmpty(semesterDates);
            if(semesterDates[numberOfSemesters-1] != null && semesterDates[numberOfSemesters-1]<validationDate && datesAreEmpty == false){
                offer.Higher_Semester_Enrollment_Avaliable__c = true;
            }
        }
    }

    @TestVisible
    private static List<Date> crateListOfSemesterDates(Offer__c offer, Decimal numberOfSemesters){
        List<Date> allDates = new List<Date>();
        List<Date> semesterDates = new List<Date>();
        allDates.add(offer.Study_Start_Date__c);
        allDates.add(offer.Start_Date_Semester_2__c);
        allDates.add(offer.Start_Date_Semester_3__c);
        allDates.add(offer.Start_Date_Semester_4__c);
        allDates.add(offer.Start_Date_Semester_5__c);
        allDates.add(offer.Start_Date_Semester_6__c);
        allDates.add(offer.Start_Date_Semester_7__c);
        allDates.add(offer.Start_Date_Semester_8__c);
        allDates.add(offer.Start_Date_Semester_9__c);
        allDates.add(offer.Start_Date_Semester_10__c);

        for(Integer i = 0; i< numberOfSemesters;i++) {
            semesterDates.add(allDates[i]);
        }
        return semesterDates;
    }

    @TestVisible
    private static Boolean areDatesEmpty(List<Date> semesterDates) {
        Boolean areEmpty = false;
            for(Date semesterDate: semesterDates){
                if(semesterDate == null) {
                    areEmpty = true;
                    break;
                }
            }

        return areEmpty;
    }
}