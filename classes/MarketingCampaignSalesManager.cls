/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to Marketing_Campaign__c object related to Marketing Campaigns Sales
**/

public without sharing class MarketingCampaignSalesManager {

    /**
     * Method responsible for creating campaign details for Campaign Sales and to attach it to current Campaign Member.
     * @author Sebastian Lasisz
     *
     * @param insertedCampaignMembersWithoutDetails Newly created Campaign Members (from importing).
     */
    public static void createCampaignDetailsForMembers(List<Marketing_Campaign__c> insertedCampaignMembersWithoutDetails) {
        Savepoint sp = Database.setSavepoint();

        Set<Id> campaignIds = new Set<Id>();
        Set<Id> campaignMemberIds = new Set<Id>();
        for (Marketing_Campaign__c campaignMember : insertedCampaignMembersWithoutDetails) {
            campaignIds.add(campaignMember.Campaign_Member__c);
            campaignMemberIds.add(campaignMember.Id);
        }

        Map<Id, Marketing_Campaign__c> campaignSales = new Map<Id, Marketing_Campaign__c>([
                SELECT Id,
                (SELECT Id, Classifier_Name__c FROM Marketing2__r WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW)
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Id IN :campaignIds
        ]);

        Set<Id> contactIds = new Set<Id>();
        for (Marketing_Campaign__c campaignMember : insertedCampaignMembersWithoutDetails) {
            if (campaignSales.keySet().contains(campaignMember.Campaign_Member__c)) {
                contactIds.add(campaignMember.Campaign_Contact__c);
            }
        }

        List<Contact> contactsToFillDetails = [
                SELECT Id, FirstName, LastName, Email, Phone, MobilePhone, Additional_Phones__c, Graduate__c, Areas_of_Interest__c, Source__c,
                (SELECT Id, Date_of_Acqiosotion__c FROM ReturnedLeads__r WHERE Date_of_Acqiosotion__c != null ORDER BY CreatedDate ASC LIMIT 1),
                (SELECT Id, Finished_Course__c, Status__c, University_from_Finished_University__r.RecordTypeId FROM FinishedSchools__r ORDER BY CreatedDate)
                FROM Contact
                WHERE Id IN :contactIds
        ];

        List<Marketing_Campaign__c> detailsToInsert = new List<Marketing_Campaign__c>();
        for (Contact contactsToFillDetail : contactsToFillDetails) {
            detailsToInsert.add(MarketingCampaignSalesManager.createCampaignMemberDetailsForSales(contactsToFillDetail));
        }

        try {
            insert detailsToInsert;
        } catch (Exception e) {
            Database.rollback(sp);
            ErrorLogger.log(e);
            throw e;
            return;
        }

        Map<Id, Marketing_Campaign__c> insertedCampaignMembers = new Map<Id, Marketing_Campaign__c>([
                SELECT Id, Classifier_from_Campaign_Member__c
                FROM Marketing_Campaign__c
                WHERE Id IN :campaignMemberIds
        ]);

        List<Marketing_Campaign__c> campaignsToUpdate = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c campaignToInsert : insertedCampaignMembersWithoutDetails) {
            if (campaignSales.keySet().contains(campaignToInsert.Campaign_Member__c)) {
                for (Marketing_Campaign__c detailToInsert : detailsToInsert) {
                    if (campaignToInsert.Campaign_Contact__c == detailToInsert.Details_Contact_Id__c) {
                        if (insertedCampaignMembers.get(campaignToInsert.Id).Classifier_from_Campaign_Member__c == null) {
                            campaignsToUpdate.add(new Marketing_Campaign__c(
                                    Id = campaignToInsert.Id,
                                    Classifier_from_Campaign_Member__c = campaignSales.get(campaignToInsert.Campaign_Member__c).Marketing2__r.get(0).Id,
                                    Campaign_from_Campaign_Details_2__c = detailToInsert.Id
                            ));
                        }
                        else {
                            campaignsToUpdate.add(new Marketing_Campaign__c(
                                    Id = campaignToInsert.Id,
                                    Campaign_from_Campaign_Details_2__c = detailToInsert.Id
                            ));
                        }

                        campaignsToUpdate.add(new Marketing_Campaign__c(
                                Id = detailToInsert.Id,
                                Campaign_Member_Id__c = campaignToInsert.Id,
                                Campaign_Member_Name__c = campaignToInsert.Name
                        ));
                    }
                }
            }
        }

        try {
            update campaignsToUpdate;
        }
        catch (Exception e) {
            Database.rollback(sp);
            ErrorLogger.log(e);
            throw e;
            return;
        }
    }

    /* Sebastian Łasisz */
    // Helper method for createContactCampaing to create campaign member for Campaign Details
    public static Marketing_Campaign__c createCampaignMember(Id offerCampaingId, Id campaignId, Id contactId, Id campaignDetails, Id offerId, Id classifierId) {
        Marketing_Campaign__c newCampaignMember = new Marketing_Campaign__c();
        newCampaignMember.Id = offerCampaingId != null ? offerCampaingId : null;
        newCampaignMember.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER);
        newCampaignMember.Classifier_from_Campaign_Member__c = classifierId;
        newCampaignMember.Campaign_Contact__c = contactId;
        newCampaignMember.Campaign_Details__c = campaignDetails;
        newCampaignMember.Details_Offer_Id__c = offerId;
        newCampaignMember.Campaign_Member__c = campaignId;

        return newCampaignMember;
    }


    /**
     * Helper method for createContactCampaing to create details for campaign member For Sales Campaign
     * @author Sebastian Łasisz
     * @param  contactDetailsToCreateDetails Contact containing all necessary information to create
     * @param  areasOfInterest               Value that specifies in which areas candidate is interested.
     * @return                               newly created Marketing Campaign object that is not inserted to database.
     */
    public static Marketing_Campaign__c createCampaignMemberDetailsForSales(Contact contactDetailsToCreateDetails, String areasOfInterest) {
        Marketing_Campaign__c newCampaignDetails = new Marketing_Campaign__c();
        newCampaignDetails.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_DETAILS2);
        newCampaignDetails.Details_Contact_First_Name__c = contactDetailsToCreateDetails.FirstName;
        newCampaignDetails.Details_Contact_Last_Name__c = contactDetailsToCreateDetails.LastName;
        newCampaignDetails.Details_Contact_Email__c = contactDetailsToCreateDetails.Email;
        newCampaignDetails.Details_Contact_Phone_Numbers__c =
                (contactDetailsToCreateDetails.Phone != null ? contactDetailsToCreateDetails.Phone : '') + ', ' +
                        (contactDetailsToCreateDetails.MobilePhone != null ? contactDetailsToCreateDetails.MobilePhone : '') + ', ' +
                        (contactDetailsToCreateDetails.Additional_Phones__c != null ? contactDetailsToCreateDetails.Additional_Phones__c : '');
        newCampaignDetails.Campaign_Areas_of_Interest__c = areasOfInterest;

        return newCampaignDetails;
    }

    /**
     * Helper method to create contact members for campaign sales
     * @author Sebastian Łasisz
     * @param  jsonBody        Optional JSON if creation is used from integration. Used to display it on additional page.
     * @param  contactToInsert Contact that will be linked with Marketing Campaign Member.
     * @param  campaignId      Id of Marketing Campaign that will be linked with Marketing Campaign Member.
     * @param  detailsId       Id of previously created Marketing Campaign Details that will be linked with Marketing Campaign Member.
     */
    public static void createMembersOnSalesCampaign(String jsonBody, Contact contactToInsert, Id campaignId, Id detailsId) {
        List<Marketing_Campaign__c> salesCampaign = [
                SELECT Id, (SELECT Id FROM Marketing2__r WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_NEW), (SELECT Id, Campaign_Contact__c FROM Campaign_Members__r)
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Id = :campaignId
                AND Active__c = true
                AND Date_Start__c <= :System.today()
                AND Date_End__c >= :System.today()
        ];

        if (salesCampaign.size() > 0) {

            List<Marketing_Campaign__c> campaignMembersToInsert = new List<Marketing_Campaign__c>();
            Set<Marketing_Campaign__c> campaignMembersToUpdate = new Set<Marketing_Campaign__c>();


            Boolean isInserted = false;
            Id currentOffer = null;
            for (Marketing_Campaign__c currentSalesContact : salesCampaign) {
                for (Marketing_Campaign__c campaignMember : currentSalesContact.Campaign_Members__r) {
                    if (contactToInsert.Id == campaignMember.Campaign_Contact__c) {
                        isInserted = true;
                        currentOffer = campaignMember.Id;
                        break;
                    }
                }
            }
            String classifierId = '';
            if (salesCampaign.size() > 0 && salesCampaign.get(0).Marketing2__r.size() > 0) {
                classifierId = salesCampaign.get(0).Marketing2__r.get(0).Id;
            } else {
                classifierId = null;
            }

            if (isInserted) {
                campaignMembersToUpdate.add(createCampaignMemberDetails2(jsonBody, currentOffer, campaignId, contactToInsert.Id, detailsId, classifierId));
            } else {
                campaignMembersToInsert.add(createCampaignMemberDetails2(jsonBody, null, campaignId, contactToInsert.Id, detailsId, classifierId));
            }
            try {
                if (campaignMembersToInsert != null && campaignMembersToInsert.size() > 0) {
                    insert campaignMembersToInsert;
                }
                if (campaignMembersToUpdate != null && campaignMembersToUpdate.size() > 0) {
                    List<Marketing_Campaign__c> campaignsToUpdate = new List<Marketing_Campaign__c>();
                    campaignsToUpdate.addAll(campaignMembersToUpdate);
                    update campaignsToUpdate;
                }
            } catch (Exception e) {
                if (!e.getMessage().contains(Label.msg_error_cantEditCampaign)) {
                    ErrorLogger.log(e);
                }
            }
        }
    }

    /**
     * Helper method for createContactCampaing to create campaign member for Campaign Details 2
     * @author Sebastian Łasisz
     * @param  jsonBody        Optional JSON if creation is used from integration. Used to display it on additional page.
     * @param  offerCampaingId Id of Marketing Campaign Member, if null, object will be created, else will be upserted.
     * @param  campaignId      Id of Marketing Campaign that will be linked with Marketing Campaign Member.
     * @param  contactId       Id of contact that will be linked with Marketing Campaign Member.
     * @param  campaignDetails Id of previously created Marketing Campaign Details that will be linked with Marketing Campaign Member.
     * @param  classifierId    Id of classifier to attach to Marketing Campaign Member.
     * @return                 newly created Marketing Campaign object that is not inserted to database.
     */
    public static Marketing_Campaign__c createCampaignMemberDetails2(String jsonBody, Id offerCampaingId, Id campaignId, Id contactId, Id campaignDetails, Id classifierId) {
        Marketing_Campaign__c newCampaignMember = new Marketing_Campaign__c();
        newCampaignMember.Id = offerCampaingId != null ? offerCampaingId : null;
        newCampaignMember.Campaign_JSON__c = jsonBody;
        newCampaignMember.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER);
        newCampaignMember.Classifier_from_Campaign_Member__c = classifierId;
        newCampaignMember.Campaign_Contact__c = contactId;
        newCampaignMember.Campaign_from_Campaign_Details_2__c = campaignDetails;
        newCampaignMember.Campaign_Member__c = campaignId;

        return newCampaignMember;
    }

    /**
     * Helper method for createContactCampaing to create details for campaign member For Sales Campaign
     * @author Sebastian Łasisz
     * @param  contactDetailsToCreateDetails Contact containing all necessary information to create
     * @return                               newly created Marketing Campaign object that is not inserted to database.
     */
    public static Marketing_Campaign__c createCampaignMemberDetailsForSales(Contact contactDetailsToCreateDetails) {
        Marketing_Campaign__c newCampaignDetails = new Marketing_Campaign__c();
        newCampaignDetails.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_DETAILS2);
        newCampaignDetails.Details_Contact_First_Name__c = contactDetailsToCreateDetails.FirstName;
        newCampaignDetails.Details_Contact_Last_Name__c = contactDetailsToCreateDetails.LastName;
        newCampaignDetails.Details_Contact_Email__c = contactDetailsToCreateDetails.Email;
        newCampaignDetails.Details_Contact_Phone_Numbers__c =
                (contactDetailsToCreateDetails.Phone != null ? contactDetailsToCreateDetails.Phone : '') + ', ' +
                        (contactDetailsToCreateDetails.MobilePhone != null ? contactDetailsToCreateDetails.MobilePhone : '') + ', ' +
                        (contactDetailsToCreateDetails.Additional_Phones__c != null ? contactDetailsToCreateDetails.Additional_Phones__c : '');

        if (contactDetailsToCreateDetails.ReturnedLeads__r.size() > 0) {
            newCampaignDetails.Details_Date_of_Acqiosotion__c = contactDetailsToCreateDetails.ReturnedLeads__r.get(0).Date_of_Acqiosotion__c;
        }

        newCampaignDetails.Details_Graduated__c = contactDetailsToCreateDetails.Graduate__c;
        newCampaignDetails.Details_Reccomended_Product__c = contactDetailsToCreateDetails.Areas_of_Interest__c;
        newCampaignDetails.Details_Source_of_Acqiosotion__c = contactDetailsToCreateDetails.Source__c;

        if (contactDetailsToCreateDetails.FinishedSchools__r.size() > 0) {
            newCampaignDetails.Details_Graduated_Product__c = contactDetailsToCreateDetails.FinishedSchools__r.get(0).Finished_Course__c;
        }

        for (Qualification__c qualificationToCheck : contactDetailsToCreateDetails.FinishedSchools__r) {
            if (qualificationToCheck.Status__c == CommonUtility.CONTACT_DIPLOMA_AQUIRED
                    && qualificationToCheck.University_from_Finished_University__r.RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)) {
                newCampaignDetails.Details_Graduated_Product__c = qualificationToCheck.Finished_Course__c;
                break;
            }
        }

        newCampaignDetails.Details_Contact_Id__c = contactDetailsToCreateDetails.Id;

        return newCampaignDetails;
    }

    /**
     * changeClassifierOnCampaignSales method is run from trigger when newly created Marketing Campaign Contact meets certain criteria.
     * For given Set of contact Ids method changes classifiers based on given classifier name.
     * @author Sebastian Łasisz
     * @param  contactIdSet   Set of contact Ids to change classifier on Marketing Campaign Sales Contact.
     * @param  classifierName Name of classifier to change on Marketing Campaign Sales Contact.
     *                        Used with CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_CONSENT_REJECT, CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI
     *                        or CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DUPLICATIOM.
     */
    public static void changeClassifierOnCampaignSales2(Set<Id> contactIdSet, String classifierName) {
        List<Marketing_Campaign__c> campaignSalesList = [
                SELECT Id, (SELECT Id FROM Marketing2__r WHERE Classifier_Name__c = :classifierName), (SELECT Id, Campaign_Contact__c FROM Campaign_Members__r WHERE Id IN :contactIdSet)
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Active__c = true
                AND Date_Start__c <= :System.today()
                AND Date_End__c >= :System.today()
        ];

        List<Marketing_Campaign__c> campaignContactsToUpdate = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c campaignSales : campaignSalesList) {
            for (Marketing_Campaign__c campaignContact : campaignSales.Campaign_Members__r) {
                if (!campaignSales.Marketing2__r.isEmpty()) {
                    campaignContact.Classifier_from_Campaign_Member__c = campaignSales.Marketing2__r.get(0).Id;
                    campaignContactsToUpdate.add(campaignContact);
                }
            }
        }

        Database.update(campaignContactsToUpdate, false);
    }

    /**
     * changeClassifierOnCampaignSales method is run from trigger when newly created Marketing Campaign Contact meets certain criteria.
     * For given Set of contact Ids method changes classifiers based on given classifier name.
     * @author Sebastian Łasisz
     * @param  contactIdSet   Set of contact Ids to change classifier on Marketing Campaign Sales Contact.
     * @param  classifierName Name of classifier to change on Marketing Campaign Sales Contact.
     *                        Used with CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_CONSENT_REJECT, CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI
     *                        or CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DUPLICATIOM.
     */
    public static void changeClassifierOnCampaignSales(Set<Id> contactIdSet, String classifierName) {
        List<Marketing_Campaign__c> campaignSalesList = [
                SELECT Id, (SELECT Id FROM Marketing2__r WHERE Classifier_Name__c = :classifierName), (SELECT Id, Campaign_Contact__c FROM Campaign_Members__r WHERE Campaign_Contact__c IN :contactIdSet)
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Active__c = true
                AND Date_Start__c <= :System.today()
                AND Date_End__c >= :System.today()
        ];

        List<Marketing_Campaign__c> campaignContactsToUpdate = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c campaignSales : campaignSalesList) {
            for (Marketing_Campaign__c campaignContact : campaignSales.Campaign_Members__r) {
                if (!campaignSales.Marketing2__r.isEmpty()) {
                    campaignContact.Classifier_from_Campaign_Member__c = campaignSales.Marketing2__r.get(0).Id;
                    campaignContactsToUpdate.add(campaignContact);
                }
            }
        }

        Database.update(campaignContactsToUpdate, false);
    }

    /**
     * changeClassifierOnZPIEnrollment method is called from trigger when newly created enrollment from ZPI is created, contacts are merged or newly Marketing Campaign Contact is created.
     * If given contact has enrollment from ZPI changes Classifier on Campaign Sales to 'ZAPISANY W ZPI'.
     * @author Sebastian Łasisz
     * @param  contactIdsSet Set of contact Ids to check whether they have enrollment that was created from ZPI.
     */
    public static void changeClassifierOnZPIEnrollment(Set<Id> contactIdsSet) {
        List<Enrollment__c> studyEnrollments = [
                SELECT Id, Enrollment_Source__c, Candidate_Student__c
                FROM Enrollment__c
                WHERE Enrollment_Source__c = :CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI
                AND Candidate_Student__c IN :contactIdsSet
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
        ];

        Set<Id> contactIdsToChangeClassifier = new Set<Id>();
        for (Enrollment__c studyEnrollment : studyEnrollments) {
            contactIdsToChangeClassifier.add(studyEnrollment.Candidate_Student__c);
        }

        if (!contactIdsToChangeClassifier.isEmpty()) {
            changeClassifierOnCampaignSales(contactIdsToChangeClassifier, CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI);
        }
    }

    /**
     * changeClassifierOnDuplicatePhone method takes list of newly created Marketing Campaign Sales
     * and looks whether there's already existing Marketing Campaign Contact with same phone number.
     * If there's phone duplicate method changes classifier for newly created Campaign Member to 'DUBEL W BAZIE'.
     * @author Sebastian Łasisz
     * @param  contactIdsList Set of contact Ids to change classifier on Marketing Campaign Sales Contact.
     */
    public static void changeClassifierOnDuplicatePhone(Set<Id> contactIdsList) {
        List<Marketing_Campaign__c> contactsList = [
                SELECT Id, Campaign_Contact__r.Phone, Campaign_Contact__r.MobilePhone, Campaign_Contact__r.Additional_Phones__c, Campaign_Contact__c, Campaign_Member__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Campaign_Contact__c IN :contactIdsList
        ];

        Set<Id> campaignIds = new Set<Id>();
        for (Marketing_Campaign__c contact : contactsList) {
            campaignIds.add(contact.Campaign_Member__c);
        }

        Set<String> phoneSet = preparePhoneSet(contactsList);

        Set<String> additionalPhoneSet = new Set<String>();
        for (String phone : phoneSet) {
            additionalPhoneSet.add('%' + phone + '%');
        }

        List<Marketing_Campaign__c> potentialContactPhoneDuplicates = [
                SELECT Id, Campaign_Contact__r.Phone, Campaign_Contact__r.MobilePhone, Campaign_Contact__r.Additional_Phones__c, Campaign_Contact__c, Campaign_Member__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Campaign_Member__c IN :campaignIds
                AND (Campaign_Contact__r.Phone IN :phoneSet
                OR Campaign_Contact__r.MobilePhone IN :phoneSet
                OR Campaign_Contact__r.Additional_Phones__c LIKE :additionalPhoneSet)
        ];

        Set<Id> contactToChangeClassiferIdsSet = new Set<Id>();
        Set<String> alreadyStoredPhoneNumbers = new Set<String>();

        for (Marketing_Campaign__c contact : contactsList) {
            for (Marketing_Campaign__c potentialContactPhoneDuplicate : potentialContactPhoneDuplicates) {
                if ((contact.Campaign_Contact__c != potentialContactPhoneDuplicate.Campaign_Contact__c)
                        && (contact.Campaign_Member__c == potentialContactPhoneDuplicate.Campaign_Member__c)) {
                    Set<String> contactPhoneSet = preparePhoneSet(new List<Marketing_Campaign__c>{
                            contact
                    });
                    Set<String> potentialContactDuplicateSet = preparePhoneSet(new List<Marketing_Campaign__c>{
                            potentialContactPhoneDuplicate
                    });

                    if (containsAny(contactPhoneSet, potentialContactDuplicateSet)) {
                        contactToChangeClassiferIdsSet.add(contact.Id);
                        alreadyStoredPhoneNumbers.addAll(potentialContactDuplicateSet);

                    }
                }
            }
        }

        changeClassifierOnCampaignSales2(contactToChangeClassiferIdsSet, CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DUPLICATIOM);
    }

    /**
     * Helper method for changeClassifierOnDuplicatePhone.
     * containsAny method takes two set of phone numbers and checkes whether they have any element in common. 
     * @author Sebastian Łasisz
     * @param  potentialDuplicatePhones Set of phone numbers from already existing Marketing Campaign contact.
     * @param  masterPhones             Set of phone numbers from newly created Marketing Campaign contact.
     * @return                          Boolean value whether two sets got any element in common.
     */
    private static Boolean containsAny(Set<String> potentialDuplicatePhones, Set<String> masterPhones) {
        for (string potentialDuplicatePhone : potentialDuplicatePhones) {
            for (String masterPhone : masterPhones) {
                if (potentialDuplicatePhone == masterPhone && potentialDuplicatePhone != null && masterPhone != null) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Helper method for changeClassifierOnDuplicatePhone.
     * preparePhoneSet method takes queried list of contacts and creates set of all phone numbers excluding empty strings and nulls.
     * @author Sebastian Łasisz
     * @param  contactsList 	List of contacts containing queried phone numbers.
     * @return                  Set of phone numbers for given list of contacts.
     */
    private static Set<String> preparePhoneSet(List<Marketing_Campaign__c> contactsList) {
        Set<String> phoneSet = new Set<String>();

        for (Marketing_Campaign__c contact : contactsList) {
            phoneSet.add(contact.Campaign_Contact__r.Phone);
            phoneSet.add(contact.Campaign_Contact__r.MobilePhone);

            if (contact.Campaign_Contact__r.Additional_Phones__c != null) {
                phoneSet.addAll(contact.Campaign_Contact__r.Additional_Phones__c.split('; '));
            }
        }

        phoneSet.remove(null);
        phoneSet.remove('');

        return phoneSet;
    }
}