/**
*   @author         Wojciech Słodziak
*   @description    schedulable class that runs batches daily to send emails with Study Enrollment confirmation reminder
**/

/**
*   To initiate the EnrollmentConfirmationEmailReminder execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('EnrollmentConfirmationEmailReminder', '0 0 8 * * ? *', new EnrollmentConfirmationEmailReminder());
**/


global class EnrollmentConfirmationEmailReminder implements Schedulable {

    global void execute(SchedulableContext sc) {
        StudyConfirmationEmailReminderBatch batch3Days = new StudyConfirmationEmailReminderBatch(3);
        Database.executebatch(batch3Days, 2);
    }
}