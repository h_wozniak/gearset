public with sharing class LEX_Do_Not_Launch_GroupController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
    public static String cancelGroup(Id scheduleId) {
        Offer__c schedule = [
            SELECT Id, Name, Status__c, Launched__c, Active__c, RecordTypeId, Group_Status__c 
            FROM Offer__c 
            WHERE Id = :scheduleId LIMIT 1
        ];

        List<Enrollment__c> participants = [
            SELECT Id, Enrollment_Training_Participant__c, Participant__c, Status__c 
            FROM Enrollment__c 
            WHERE Training_Offer_for_Participant__c = :scheduleId 
            AND Status__c NOT IN (:CommonUtility.ENROLLMENT_STATUS_RESIGNATION) 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
        ];

        
        List<Enrollment__c> mainEnrollmentsToUpdate;
        List<Enrollment__c> participantsToUpdate;
        
        if (schedule.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)) {
            participantsToUpdate = new List<Enrollment__c>();
            for (Enrollment__c participant : participants) {
                if (participant.Status__c != CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                    participant.Last_Active_Status__c = participant.Status__c;
                    participant.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED;
                    participantsToUpdate.add(participant);
                }
            }
        } else if (schedule.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
            Set<Id> trainingEnrollmentIdList = new Set<Id>();
            for (Enrollment__c participant : participants) {
                trainingEnrollmentIdList.add(participant.Enrollment_Training_Participant__c);
            }
            mainEnrollmentsToUpdate = [SELECT Id, Status__c FROM Enrollment__c WHERE Id IN :trainingEnrollmentIdList];
            for (Enrollment__c mEnrollment : mainEnrollmentsToUpdate) {
                if (mEnrollment.Status__c != CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED) {
                    mEnrollment.Last_Active_Status__c = mEnrollment.Status__c;
                    mEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED;
                }
            }
        }

        Savepoint sp = Database.setSavepoint();
        try {
            if (participantsToUpdate != null && !participantsToUpdate.isEmpty()) {
                update participantsToUpdate;
            }
            if (mainEnrollmentsToUpdate != null && !mainEnrollmentsToUpdate.isEmpty()) {
                update mainEnrollmentsToUpdate;
            }

            schedule.Launched__c = false;
            schedule.Active__c = false;
            schedule.Group_Status__c = CommonUtility.OFFER_GROUP_STATUS_NOT_LAUNCHED;
            update schedule;
            
            if (participantsToUpdate == null || participantsToUpdate.isEmpty()) {
                return 'ok-np';
            } else {
                return 'ok';
            }
        } catch (Exception ex) {
            Database.rollback(sp);
            ErrorLogger.log(ex);
            return String.valueOf(ex);
        }
    }
}