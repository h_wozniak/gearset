global without sharing class EnrollmentVerificationController {

	public String verificationString { get; set; }
	public String universityName { get; set; }

	public EnrollmentVerificationController() {
		verificationString = ApexPages.currentPage().getParameters().get('verification');
		List<Enrollment__c> enrollmentList = [SELECT Id, University_Name__c  FROM Enrollment__c WHERE VerificationString__c = :verificationString];
		universityName = enrollmentList.get(0).University_Name__c;
	}


	@RemoteAction
	global static VerificationResponse verifyEnrollment(String verificationString) {
		List<Enrollment__c> enrollmentList = [SELECT Id, VerificationDate__c  FROM Enrollment__c WHERE VerificationString__c = :verificationString FOR UPDATE];

		if (!enrollmentList.isEmpty()) {
			Enrollment__c enrollment = enrollmentList.get(0);

			if (enrollment.VerificationDate__c != null) {
				return new VerificationResponse(false, true);
			}
			
			enrollment.VerificationDate__c = System.now();
            try {
                update enrollment;
				return new VerificationResponse(true, false);
            } catch(Exception e) {
                ErrorLogger.log(e);
            }
		}

		return new VerificationResponse(false, false);
	}


	global class VerificationResponse {
		public Boolean result;
		public Boolean alreadyVerified;

		public VerificationResponse(Boolean result, Boolean alreadyVerified) {
			this.result = result;
			this.alreadyVerified = alreadyVerified;
		}
	}
}