public with sharing class LEX_TransferEnrollmentToKS {

    ApexPages.StandardSetController setCon;

    public String errorMessage {get; set; }
    public Set<Id> enrollmentIds { get; set; }

    public LEX_TransferEnrollmentToKS(ApexPages.StandardSetController controller) {
        setCon = controller;

        List<Enrollment__c> studyEnrollments = setCon.getSelected();

        enrollmentIds = new Set<Id>();
        for (Enrollment__c selectedEnrollment : studyEnrollments) {
            enrollmentIds.add(selectedEnrollment.Id);
        }

        if (enrollmentIds.isEmpty()) {
            errorMessage = Label.msg_info_ChooseOneRecord;
        }
        else if (!hasEditAccessList(enrollmentIds)) {
            errorMessage = Label.msg_error_InsufficientPrivileges;
        }
        else if (!checkIfCanSendToKS()) {
            errorMessage = Label.msg_error_InsufficientPrivilegesPermissionSet;
        }
    }

    public PageReference acceptSendToKS() {
        WebserviceUtilities.WS_Response response = createEducationAgreement(enrollmentIds);
        errorMessage = response.message;

        if (response.result) {
            errorMessage = transferEnrollmentsToKS(enrollmentIds, false).message;
        }

        System.debug(errorMessage);

        return null;
    }

    public Boolean hasEditAccessList(Set<Id> recordIdList) {
        Boolean hasAccess = true;

        List<UserRecordAccess> uraList = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId() AND RecordId IN :recordIdList
        ];

        for (UserRecordAccess ura : uraList) {
            if (!ura.HasEditAccess) {
                hasAccess = false;
            }
        }

        return hasAccess;
    }

    public Boolean checkIfCanSendToKS() {
        List<PermissionSetAssignment> psa = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE PermissionSet.Name = :CommonUtility.PERMISSION_SET_SENDTOKS
                AND AssigneeId = :UserInfo.getUserId()
        ];

        return psa.size() > 0;
    }

    public WebserviceUtilities.WS_Response createEducationAgreement(Set<Id> recordIdSet) {
        WebserviceUtilities.WS_Response response;

        List<Enrollment__c> enrList = new List<Enrollment__c>();

        if (CommonUtility.checkIfCanEditUneditableEnrollment()) {
            enrList = [
                    SELECT Id, Status__c, Candidate_Student__c, Name, Educational_Agreement__c, Course_or_Specialty_Offer__c, Candidate_Student__r.Is_Potential_Duplicate__c, Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Verify_Agreement__c, Candidate_Student__r.Name
                    FROM Enrollment__c
                    WHERE Id IN :recordIdSet
                    AND (
                            Status__c = :CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            ];
        }
        else {
            enrList = [
                    SELECT Id, Status__c, Candidate_Student__c, Name, Educational_Agreement__c, Course_or_Specialty_Offer__c, Candidate_Student__r.Is_Potential_Duplicate__c, Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Verify_Agreement__c, Candidate_Student__r.Name
                    FROM Enrollment__c
                    WHERE Id IN :recordIdSet
                    AND (
                            Status__c = :CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            ];
        }

        if (enrList.size() != recordIdSet.size()) {
            response = new WebserviceUtilities.WS_Response(false, Label.msg_error_EnrollmentsStatusesIncorrectKSTransfer, null);
        }

        if (response == null) {
            for (Enrollment__c studyEnr : enrList) {
                if (studyEnr.Verify_Agreement__c) {
                    response = new WebserviceUtilities.WS_Response(false, Label.msg_error_verifyAgreement, null);
                }
            }
        }

        if (response == null) {
            String duplicateErrorHTML = EnrollmentManager_Studies.candidatePotentialDuplicationCheckList(enrList);
            if (duplicateErrorHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, duplicateErrorHTML, null);
            }
        }

        if (response == null) {
            String missingContactDataHTML = EnrollmentManager_Studies.contactDataIsCompleteOnStudyEnrollments(enrList);
            if (missingContactDataHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, missingContactDataHTML, null);
            }
        }

        if (response == null) {
            String annexedEnrollmentsDataHTML = EnrollmentManager_Studies.enrollmentPotentialAnnexedCheckList(enrList);
            if (annexedEnrollmentsDataHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, annexedEnrollmentsDataHTML, null);
            }
        }

        if (response == null) {
            // call Experia synchronization - will be called with above batch
            IntegrationEnrollmentHelper.updateSyncStatus_inProgress(true, recordIdSet, '', false, false);

            if (recordIdSet.size() == 1) {
                // call method for one enrollment without invoking batch
                EducationalAgreementUpsertBatch.createOrUpdateEducationalAgreement(enrList[0]);
            }

            response = new WebserviceUtilities.WS_Response(
                    true,
                    (recordIdSet.size() > 1? Label.msg_sendingEnrollmentsToKSInProgress : Label.msg_SendingEnrollmentToKSInProgress),
                    null
            );
        }

        return response;
    }

    public WebserviceUtilities.WS_Response transferEnrollmentsToKS(Set<Id> recordIdSet, Boolean sendingPayments) {
        WebserviceUtilities.WS_Response response;

        List<Enrollment__c> enrList = new List<Enrollment__c>();

        if (CommonUtility.checkIfCanEditUneditableEnrollment()) {
            enrList = [
                    SELECT Id, Status__c, Candidate_Student__c, Name, Educational_Agreement__c, Course_or_Specialty_Offer__c, Candidate_Student__r.Is_Potential_Duplicate__c, Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Verify_Agreement__c, Candidate_Student__r.Name
                    FROM Enrollment__c
                    WHERE Id IN :recordIdSet
                    AND (
                            Status__c = :CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            ];
        }
        else {
            enrList = [
                    SELECT Id, Status__c, Candidate_Student__c, Name, Educational_Agreement__c, Course_or_Specialty_Offer__c, Candidate_Student__r.Is_Potential_Duplicate__c, Admission_Rules_for_Studies__c, Admission_Rules_From__c, Admission_Rules_To__c, Verify_Agreement__c, Candidate_Student__r.Name
                    FROM Enrollment__c
                    WHERE Id IN :recordIdSet
                    AND (
                            Status__c = :CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT
                            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                            OR Unenrolled_Status__c = :CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS
                    )
                    AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            ];
        }

        if (enrList.size() != recordIdSet.size()) {
            response = new WebserviceUtilities.WS_Response(false, Label.msg_error_EnrollmentsStatusesIncorrectKSTransfer, null);
        }

        if (response == null) {
            for (Enrollment__c studyEnr : enrList) {
                if (studyEnr.Verify_Agreement__c) {
                    response = new WebserviceUtilities.WS_Response(false, Label.msg_error_verifyAgreement, null);
                }
            }
        }

        if (response == null) {
            String duplicateErrorHTML = EnrollmentManager_Studies.candidatePotentialDuplicationCheckList(enrList);
            if (duplicateErrorHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, duplicateErrorHTML, null);
            }
        }

        if (response == null) {
            String missingContactDataHTML = EnrollmentManager_Studies.contactDataIsCompleteOnStudyEnrollments(enrList);
            if (missingContactDataHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, missingContactDataHTML, null);
            }
        }

        if (response == null) {
            String annexedEnrollmentsDataHTML = EnrollmentManager_Studies.enrollmentPotentialAnnexedCheckList(enrList);
            if (annexedEnrollmentsDataHTML != null) {
                response = new WebserviceUtilities.WS_Response(false, annexedEnrollmentsDataHTML, null);
            }
        }

        if (response == null) {
            if (recordIdSet.size() == 1) {
                // invoke integration
                IntegrationEnrollmentSender.sendEnrollmentListAtFuture(recordIdSet, sendingPayments, false);
            } else {
                // call Batch to upsert Educational Agreements - invokes integration on it's own
                Database.executeBatch(new EducationalAgreementUpsertBatch(recordIdSet, sendingPayments, false), 1);
            }

            response = new WebserviceUtilities.WS_Response(
                    true,
                    (recordIdSet.size() > 1? Label.msg_sendingEnrollmentsToKSInProgress : Label.msg_SendingEnrollmentToKSInProgress),
                    null
            );
        }

        return response;
    }
}