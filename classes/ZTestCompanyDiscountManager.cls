@isTest
private class ZTestCompanyDiscountManager {

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id, (
                        SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                        FROM InstallmentConfigs__r
                )
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }


    @isTest static void test_addCompanyDiscount_Specialty() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));

        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = retMap.get(0).Id), true));

        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account company = ZDataTestUtility.createCompany(null, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_I, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław', Detail_Level__c = 'Specialty Offer'), false);
        companyDiscount.University_Names__c = null;
        companyDiscount.Degrees__c = null;
        insert companyDiscount;

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = company.Id), true);
        ZDataTestUtility.createOfferCompanyDiscount(new Discount__c(Discount_from_Offer_DC__c = companyDiscount.Id, Offer_from_Offer_DC__c = retMap.get(0).Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(1).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 1);

    }

    @isTest static void test_addCompanyDiscount_Course() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account company = ZDataTestUtility.createCompany(null, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_I, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław', Detail_Level__c = 'Course Offer'), false);
        companyDiscount.University_Names__c = null;
        companyDiscount.Degrees__c = null;
        insert companyDiscount;

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = company.Id), true);
        ZDataTestUtility.createOfferCompanyDiscount(new Discount__c(Discount_from_Offer_DC__c = companyDiscount.Id, Offer_from_Offer_DC__c = retMap.get(0).Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 1);
    }

    @isTest static void test_addCompanyDiscount_Course_Parent() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account parentCompany = ZDataTestUtility.createCompany(null, true);
        Account company = ZDataTestUtility.createCompany(new Account(ParentId = parentCompany.Id), true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_I, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław', Detail_Level__c = 'Course Offer'), false);
        companyDiscount.University_Names__c = null;
        companyDiscount.Degrees__c = null;
        insert companyDiscount;

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = parentCompany.Id), true);
        ZDataTestUtility.createOfferCompanyDiscount(new Discount__c(Discount_from_Offer_DC__c = companyDiscount.Id, Offer_from_Offer_DC__c = retMap.get(0).Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 1);
    }

    @isTest static void test_addCompanyDiscount_OfferGeneric() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account company = ZDataTestUtility.createCompany(null, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_I, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław'), true);

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = company.Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 0);
    }

    @isTest static void test_addCompanyDiscount_OfferGeneric2() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account company = ZDataTestUtility.createCompany(null, true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_II, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław'), true);

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = company.Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 1);
    }

    @isTest static void test_addCompanyDiscount_OfferGeneric3() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c uniOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = uniOffer.Id,
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true
        ), true));

        Account parentCompany = ZDataTestUtility.createCompany(null, true);
        Account company = ZDataTestUtility.createCompany(new Account(ParentId = parentCompany.Id), true);
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(AccountId = company.Id), true);

        Discount__c companyDiscount = ZDataTestUtility.createCompanyDiscount(
                new Discount__c(Degrees__c = CommonUtility.OFFER_DEGREE_II, University_Names__c = RecordVals.WSB_NAME_WRO, University_for_sharing__c = 'WSB Wrocław'), true);

        ZDataTestUtility.createAccountCompanyDiscount(new Discount__c(Discount_from_Company_DC__c = companyDiscount.Id, Company_from_Company_Discount__c = parentCompany.Id), true);

        //config PriceBook
        configPriceBook(retMap.get(1).Id, 200);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Candidate_Student__c = candidate.Id,
                Unenrolled__c = false,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        Test.stopTest();

        List<Discount__c> discountConnectors = [
                SELECT Id
                FROM Discount__c
                WHERE Enrollment__c = :retMap.get(2).Id
        ];

        System.assertEquals(discountConnectors.size(), 1);
    }
}