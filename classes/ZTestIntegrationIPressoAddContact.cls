@isTest
global class ZTestIntegrationIPressoAddContact {

    @isTest static void test_receiveContactInformation_newRecord() {
        ZDataTestUtility.createBaseConsent(new Catalog__c(Not_Changeable_Name__c = false, iPressoId__c = '31'), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(Not_Changeable_Name__c = false, iPressoId__c = '43', ADO__c = 'WSB Wrocław; WSB Poznań; WSB Gdańsk; WSB Toruń'), true);
        ZDataTestUtility.createBaseConsent(new Catalog__c(Not_Changeable_Name__c = false, iPressoId__c = '3'), true);

        String body = '{"ipresso_contact_id":"5014","first_name":"testGlobCons123","tags":["first_tag", "second_tag"],"last_name":"testGlobCons123","email":"testglobcons123@test.co","mobile":"123456789","degree":"I","consents":[{"consent_id":"31","consent_value":false},{"consent_id":"43","consent_value":false}]}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        IntegrationIPressoAddContact.receiveContactInformation();
        Test.stopTest();

        List<Contact> contacts = [
                SELECT Id, Tags__c
                FROM Contact
        ];

        System.assert(contacts.size() >= 0);

        System.assertEquals(contacts.get(0).Tags__c, 'first_tag; second_tag');
    }
}