@isTest
private class ZTestEmailManager_DocListEmail {

    private static Map<Integer, sObject> prepareDataForTest_Statistics(Boolean withSpecialties) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));

        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = retMap.get(0).Id
        ), true));

        if (withSpecialties) {
            retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                    Course_Offer_from_Specialty__c = retMap.get(1).Id
            ), true));
        }

        return retMap;
    }

    private static void prepareTemplates() {
        EmailTemplate defaultEmailTemplate = new EmailTemplate();
        defaultEmailTemplate.isActive = true;
        defaultEmailTemplate.Name = 'x';
        defaultEmailTemplate.DeveloperName = EmailManager_DocumentListEmail.DocListEmTemplates.X23b1_StudyEnrDocList.name();
        defaultEmailTemplate.TemplateType = 'text';
        defaultEmailTemplate.FolderId = UserInfo.getUserId();
        defaultEmailTemplate.Subject = 'x';

        insert defaultEmailTemplate;
    }

    @isTest static void test_SendRequiredDocumentList() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        Test.startTest();
        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                Enrollment_from_Documents__c = enrollment.Id,
                For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);


        EmailManager_DocumentListEmail.sendRequiredDocumentList(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Email, Degree__c, University_Name__c,
                        RequiredDocListEmailedCount__c, DocumentList_for_email__c, Documents_Collected__c
                FROM Enrollment__c
                WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(1, enrollmentList[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
        Test.stopTest();
    }

    @isTest static void test_SendRequiredDocumentListButton() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Language_of_Enrollment__c = CommonUtility.ENROLLMENT_LANGUAGE_POLISH), true);

        Test.startTest();
        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                Enrollment_from_Documents__c = enrollment.Id,
                For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);


        EmailManager_DocumentListEmail.sendRequiredDocumentListFinalFromButton(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Email, Degree__c, University_Name__c,
                        RequiredDocListEmailedCount__c, DocumentList_for_email__c, Documents_Collected__c
                FROM Enrollment__c
                WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(1, enrollmentList[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
        Test.stopTest();
    }

    @isTest static void test_getAdditionalDocAttMap_offer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.size(), 1);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_course() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Attachment newAttachment22 = new Attachment();
        newAttachment22.Name = 'Unit Test Attachment';
        newAttachment22.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment22.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment22;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        System.assertEquals(enrollmentList.size(), 1);

        List<Offer__c> additionalDocumentsOfferList = EmailManager_DocumentListEmail.prepareOfferIdSetFromEnrollment(enrollmentList);
        System.assertEquals(additionalDocumentsOfferList.size(), 1);

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_course2() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalOfferDocument2 = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment22 = new Attachment();
        newAttachment22.Name = 'Unit Test Attachment';
        newAttachment22.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment22.ParentId = additionalOfferDocument2.Additional_Document_from_Offer__c;

        insert newAttachment22;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        System.assertEquals(enrollmentList.size(), 1);

        List<Offer__c> additionalDocumentsOfferList = EmailManager_DocumentListEmail.prepareOfferIdSetFromEnrollment(enrollmentList);
        System.assertEquals(additionalDocumentsOfferList.size(), 2);

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 2);
    }

    @isTest static void test_getAdditionalDocAttMap_courseOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDoc2 = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment2 = new Attachment();
        newAttachment2.Name = 'Unit Test Attachment';
        newAttachment2.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment2.ParentId = additionalDoc2.Additional_Document_from_Offer__c;

        insert newAttachment2;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(1).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
        ];

        List<Offer__c> additionalDocumentsOfferList = EmailManager_DocumentListEmail.prepareOfferIdSetFromEnrollment(enrollmentList);
        System.assertEquals(additionalDocumentsOfferList.size(), 2);

        System.debug('additionalDocumentsOfferList ' + additionalDocumentsOfferList);

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.debug('attachmentMap ' + attachmentMap);
        System.debug(additionalOfferDocument.Id);
        System.debug(additionalDoc2.Id);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 2);

    }

    @isTest static void test_getAdditionalDocAttMap_specialty() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);

    }

    @isTest static void test_getAdditionalDocAttMap_specialtyOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 1);
    }

    @isTest static void test_getAdditionalDocAttMap_specialtyAndOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), true);

        Attachment newAttachment = new Attachment();
        newAttachment.Name = 'Unit Test Attachment';
        newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment.ParentId = additionalOfferDocument.Additional_Document_from_Offer__c;

        insert newAttachment;

        Test.startTest();
        Offer__c addDoc2 = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), true);

        Attachment newAttachment2 = new Attachment();
        newAttachment2.Name = 'Unit Test Attachment';
        newAttachment2.Body = Blob.valueOf('Unit Test Attachment Body');
        newAttachment2.ParentId = addDoc2.Additional_Document_from_Offer__c;

        insert newAttachment2;

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);
        
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);
        Test.stopTest();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 2);
    }

    @isTest static void test_getAdditionalDocAttMap_specialtyCourseOffer() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);

        List<Offer__c> additionalDocuments = new List<Offer__c>();

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(0).Id
        ), false));

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), false));

        additionalDocuments.add(ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(2).Id
        ), false));

        insert additionalDocuments;

        List<Attachment> atts = new List<Attachment>();
        for (Offer__c off : additionalDocuments) {
            Attachment newAttachment = new Attachment();
            newAttachment.Name = 'Unit Test Attachment';
            newAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
            newAttachment.ParentId = off.Additional_Document_from_Offer__c;

            atts.add(newAttachment);
        }

        insert atts;

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = dataMap.get(2).Id), true);

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Unenrolled__c, Language_of_Enrollment__c, CreatedDate,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c
                FROM Enrollment__c
                WHERE Id = :studyEnr.Id
                AND Candidate_Student__r.Email != null
                AND TECH_Send_Acceptance_Email__c = true
                AND Dont_send_automatic_emails__c = false
        ];

        Map<Id, List<Attachment>> attachmentMap = EmailManager_DocumentListEmail.getAdditionalDocAttachmentMap(enrollmentList);
        System.assertEquals(attachmentMap.get(studyEnr.Id).size(), 3);
        Test.stopTest();
    }

}