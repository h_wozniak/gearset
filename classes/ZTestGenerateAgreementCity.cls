@isTest
private class ZTestGenerateAgreementCity {

    @isTest static void testGeneratingAgreementCityFromCourseOffer() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);

        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        Test.stopTest();

        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        
        String result = DocGen_GenerateAgreementCity.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, 'Wrocław');
    }
    
    @isTest static void testGeneratingAgreementCityFromSpecialtyOffer() {
        Account university = ZDataTestUtility.createUniversity(new Account(BillingCity = 'Wrocław'), true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, University__c = university.Id), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);
        Test.stopTest();
        
        String result = DocGen_GenerateAgreementCity.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, 'Wrocław');
    }
}