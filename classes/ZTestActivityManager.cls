@isTest
private class ZTestActivityManager {

	@isTest static void test_lackOfDocumentsActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);
		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = contact.Id), true);

		Test.startTest();
		ActivityManager.lackOfDocumentsActivity(new List<Enrollment__c> { studyEnrollment });
		Test.stopTest();
	}
	
	@isTest static void test_informatorActivity() {
		Test.startTest();
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Marketing_Campaign__c activityCampaign = ZDataTestUtility.createMarketingCampaignActivity(null, true);
		Marketing_Campaign__c iPressoMember = ZDataTestUtility.createIPressoMember(new Marketing_Campaign__c(Campaign_Contact__c = contact.Id, Campaign_Member__c = activityCampaign.Id), true);

		ActivityManager.informatorActivity(activityCampaign.Id);

		Test.stopTest();
	}

	@isTest static void test_attendaceAtTheEventActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Test.startTest();
		Task newTask = new Task();
		newTask.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_ATTENDACE_AT_EVENT_ACTIVITY);
        newTask.Subject = 'subject';
        newTask.ActivityDate = System.today();
        newTask.WhoId = contact.Id;
        newTask.Status = 'Completed';
        newTask.OwnerId = CommonUtility.getIntegrationUser().Id;
        newTask.Event_Name__c = 'Saved';
        newTask.Time_of_Event__c = '12:00';

        insert newTask;

		Test.stopTest();

		List<Task> createdTasks = [
			SELECT Id
			FROM Task
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_ATTENDACE_AT_EVENT_ACTIVITY)
		];

		System.assertEquals(createdTasks.size(), 1);
	}

	@isTest static void test_contactActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Test.startTest();
		Task newTask = new Task();
		newTask.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CONTACT_ACTIVITY);
        newTask.Subject = 'subject';
        newTask.ActivityDate = System.today();
        newTask.WhoId = contact.Id;
        newTask.Status = 'Completed';
        newTask.OwnerId = CommonUtility.getIntegrationUser().Id;
        newTask.Effect_of_Contact__c = 'Interested';
        newTask.Category_of_Contact__c = CommonUtility.ENROLLMENT_SOURCE_ST_EA;
        newTask.Contact_Channel__c = 'Phone';
        newTask.Time_of_Event__c = '12:00';

        insert newTask;

		Test.stopTest();

		List<Task> createdTasks = [
			SELECT Id
			FROM Task
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_CONTACT_ACTIVITY)
		];

		System.assertEquals(createdTasks.size(), 1);
	}

	@isTest static void test_marketingPhoneContactActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Test.startTest();
		Task newTask = new Task();
		newTask.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_MARKETING_PHONE_CONTACT_ACTIVITY);
        newTask.Subject = 'subject';
        newTask.ActivityDate = System.today();
        newTask.WhoId = contact.Id;
        newTask.Status = 'Completed';
        newTask.OwnerId = CommonUtility.getIntegrationUser().Id;
        newTask.Effect_of_Talking_with_CC__c = 'Saved';
        newTask.Time_of_Event__c = '12:00';

        insert newTask;

		Test.stopTest();

		List<Task> createdTasks = [
			SELECT Id
			FROM Task
			WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_MARKETING_PHONE_CONTACT_ACTIVITY)
		];

		System.assertEquals(createdTasks.size(), 1);
	}

	@isTest static void test_resignationOnEnrollmentActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);
		Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = contact.Id), true);

		Test.startTest();
		studyEnrollment.Resignation_Reason_List__c = CommonUtility.ENROLLMENT_STATUS_WITHDRAWALOFCONSENT;
		studyEnrollment.Resignation_Date__c = System.today();
		studyEnrollment.Resignation_Comments__c = 'Some comment on resignation';
		studyEnrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
		update studyEnrollment;

		Test.stopTest();
	}

	@isTest static void test_changeStatusOnContactActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Test.startTest();
		contact.Status__c = CommonUtility.CONTACT_STATUS_SQL;
		update contact;

		Test.stopTest();
	}

	@isTest static void test_changeStatusesOnContactActivity() {
		Contact contact = ZDataTestUtility.createCandidateContact(null, true);
		Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = contact.Id, iPressoId__c = '5'), true);

		Test.startTest();
		contact.Is_Potential_Duplicate__c = true;
		update contact;
		
		Test.stopTest();
	}
}