global class DocGen_GenerateStudyModeForUnenrolled  implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateStudyModeForUnenrolled() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateStudyModeForUnenrolled.generateStudyMode(objectId);  
    }

    global static String generateStudyMode(String objectId) {
        Enrollment__c enrollment = [SELECT ToLabel(Mode__c), ToLabel(Enrollment_from_Documents__r.Mode__c) FROM Enrollment__c WHERE Id =: objectId];
        
        String mode = '';

        if (enrollment.Mode__c != null) {
            mode = enrollment.Mode__c;
        }
        else {
            mode = enrollment.Enrollment_from_Documents__r.Mode__c;
        }
        
        if (mode == CommonUtility.OFFER_MODE_FULL_TIME) {
            return ' <w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>';
        }
        if (mode == CommonUtility.OFFER_MODE_WEEKEND) {
            return ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>';
        }
        if (mode == CommonUtility.OFFER_MODE_ONLINE) {
            return ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej on-line</w:t></w:r>';
        }
        return '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + mode + '</w:t></w:r>';
    }
}