/**
* @author       Sebastian Łasisz
* @description  Batch class for merging two lists of contacts together.
**/

/* ------------------------------------------ example --------------------------------------------
Database.executeBatch(
    new MergeSetOfContactsBatch(
        new List<Id>(),
        new List<Id>()
    ),
    1
);
------------------------------------------- end of example ------------------------------------- */

global class MergeSetOfContactsBatch implements Database.Batchable<MergeHelper> {
    List<MergeHelper> mergeHelperList;

    global MergeSetOfContactsBatch(List<Id> masterIds, List<Id> mergedIds) {
    	mergeHelperList = new List<MergeHelper>();

    	for(Integer idCon = 0; idCon < masterIds.size(); idCon++) {
			mergeHelperList.add(new MergeHelper(masterIds[idCon], mergedIds[idCon]));
		}
    }
    
    global Iterable<MergeHelper> start(Database.BatchableContext BC) {
        return mergeHelperList;
    }

    global void execute(Database.BatchableContext BC, List<MergeHelper> scope) {
        for (MergeHelper obj : scope) {
        	StudyWebservices.mergeContacts(obj.masterId, obj.mergedId);
        }
    }
    
    global void finish(Database.BatchableContext BC) {}

    global class MergeHelper {
    	Id masterId;
    	Id mergedId;

    	global MergeHelper(Id masterId, Id mergedId) {
    		this.masterId = masterId;
    		this.mergedId = mergedId;
    	}
    }
    
}