@isTest
global class ZTestIntegrationProductsResource {

    @isTest static void test_integrationProductsSender() {
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationProductsResource.sendProducts();
        Test.stopTest();
    }
}