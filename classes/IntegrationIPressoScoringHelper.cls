/**
*   @author         Sebastian Łasisz
*   @description    This class is a helper class for IntegrationFCCInteractionReceiver
**/

global class IntegrationIPressoScoringHelper {
    
    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- INVOKE METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String buildScoringJSON() {
        iPresso_Last_Scoring_Retrival_Date__c scoringLastRetrieval = iPresso_Last_Scoring_Retrival_Date__c.getOrgDefaults();
        String dateOfLastInteraction = scoringLastRetrieval.Date_From__c.format('yyyy-MM-dd HH:mm');
        String currentTime = DateTime.now().format('yyyy-MM-dd HH:mm');

        IntegrationIPressoScoringHelper.ScoringSender_JSON scoringToRetrieve = new IntegrationIPressoScoringHelper.ScoringSender_JSON(dateOfLastInteraction, currentTime);

        return JSON.serialize(scoringToRetrieve);
    }

    public static String sendInteractionRequest() {
        HttpRequest httpReq = new HttpRequest();
        	
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createImportCallsEndPoint = esb.Import_Scoring_From_iPresso_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createImportCallsEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationIPressoScoringHelper.buildScoringJSON();
        httpReq.setBody(jsonToSend);
        
        Boolean success = false;

        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        success = (httpRes.getStatusCode() == 200);

        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoScoringHelper.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n');
            return null;
        }
        else {
            return httpRes.getBody();
        }            
    }

    global static DateTime stringToDateTime(String value) {
        DateTime v = DateTime.valueof(value);
        return v.addHours(1);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global class ScoringSender_JSON {
        public String date_from;
        public String date_to;

        public ScoringSender_JSON(String date_from, String date_to) {
            this.date_from = date_from;
            this.date_to = date_to;
        }
    }

    global class ScoringReceiver_JSON {
        public String ipresso_contact_id;
        public String contact_scoring;
        public String scoring_calculation_date;
    }


    public static List<IntegrationIPressoScoringHelper.ScoringReceiver_JSON> parse(String jsonBody) {
        return (List<IntegrationIPressoScoringHelper.ScoringReceiver_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationIPressoScoringHelper.ScoringReceiver_JSON>.class);
    }
}