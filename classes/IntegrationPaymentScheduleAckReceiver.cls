/**
*   @author         Wojciech Słodziak
*   @description    Rest Resource class for receiving acknowledgements for Payment Schedules transfers to Experia.
**/

@RestResource(urlMapping = '/payment_schedule_ack/*')
global without sharing class IntegrationPaymentScheduleAckReceiver {


    @HttpPost
    global static void receivePaymentScheduleAcks() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();

        IntegrationManager.AckResponse_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                List<Enrollment__c> eduAgrList = [
                    SELECT Id, Source_Enrollment__c, Source_Enrollment__r.TECH_Integration_Status__c
                    FROM Enrollment__c 
                    WHERE Id = :parsedJson.record_id
                ];

                if (!eduAgrList.isEmpty()) {
                    Boolean sendingInitialFees = (eduAgrList.get(0).Source_Enrollment__r.TECH_Integration_Status__c == CommonUtility.ENROLLMENT_INTEGRATION_INITIALFEES);

                    if (parsedJson.success) {
                        Enrollment__c studyEnr = [
                            SELECT Id, Status__c, Unenrolled_Status__c, Unenrolled__c, TECH_Integration_Status__c
                            FROM Enrollment__c
                            WHERE Id = :eduAgrList[0].Source_Enrollment__c
                        ];

                        if (!sendingInitialFees) {
                            if (!studyEnr.Unenrolled__c) {
                                studyEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS;
                            } else {
                                studyEnr.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS;
                            }

                            update studyEnr;
                        }
                    } else {
                        ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationPaymentScheduleAckReceiver.class.getName() + '\n' + jsonBody);
                    }

                    IntegrationPaymentScheduleHelper.updateSyncStatus_onFinish(parsedJson.success, new Set<Id> { eduAgrList[0].Source_Enrollment__c }, jsonBody, sendingInitialFees);
                } else {
                    ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR_NOT_FOUND + '\n' + IntegrationPaymentScheduleAckReceiver.class.getName() + '\n' + jsonBody);
                }
                res.statusCode = 200;
            } catch (Exception e) {
                ErrorLogger.log(e);
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }
        }
    }
    
    private static IntegrationManager.AckResponse_JSON parse(String jsonBody) {
        return (IntegrationManager.AckResponse_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.AckResponse_JSON.class);
    }

}