public with sharing class LEX_Send_Verification_LinkController {

	@AuraEnabled
	public static String checkStatus(String recordId) {
		Enrollment__c enr = [SELECT Status__c FROM Enrollment__c WHERE Id = :recordId];
		return (enr.Status__c == CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED) ? 'TRUE' : 'FALSE';
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static Boolean sendVerificationEmail(String recordId) {
		return LEXStudyUtilities.sendVerificationEmail(recordId);
	}
}