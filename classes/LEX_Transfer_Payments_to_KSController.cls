public with sharing class LEX_Transfer_Payments_to_KSController {

	@AuraEnabled
	public static String checkRecord(String recordId) {

		Enrollment__c enr = [SELECT Status__c, Unenrolled_Status__c FROM Enrollment__c WHERE Id = :recordId];
		Boolean permissionSetAccess = LEXStudyUtilities.CheckIfCanSendToKS();
		Boolean permissionSetAccessToKS = LEXStudyUtilities.checkIfCanEditUneditableEnrollment();

		if (permissionSetAccess) { 
			if(enr.Unenrolled_Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS ||
			enr.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS || permissionSetAccessToKS) {
				return 'TRUE';
			}
		}
		return 'FALSE';
	}

	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String transferPaymentsToKS(String recordId) {
		LEXStudyUtilities.updatePaymentsWithExpiredDeadline(new List<String>{recordId});
		return LEXStudyUtilities.transferPaymentsToKS(new List<String>{recordId});
	}

	@AuraEnabled
	public static String transferEnrollmentsToKS(String recordId) {
		return LEXStudyUtilities.transferEnrollmentsToKS(new List<String>{recordId}, true, false);
	}
}