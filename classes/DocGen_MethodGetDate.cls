global class DocGen_MethodGetDate implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_MethodGetDate() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
        if (methodName == 'birthdate') {
            return generateBirthdate(objectId);
        }
        else if (methodName == 'studyStartDate') {
            return generateStudyStartDate(objectId);
        }
        return DocGen_MethodGetDate.generateDate(Date.today()); 
    }

    global static String generateDate(Date dateToConvert) { 
        String day = dateToConvert.day() < 10 ? '0' + String.valueOf(dateToConvert.day()) : String.valueOf(dateToConvert.day());
        String month = dateToConvert.month() < 10 ? '0' + String.valueOf(dateToConvert.month()) : String.valueOf(dateToConvert.month());

        return day + '.' + month + '.' + dateToConvert.year();
    }

    global static String generateStudyStartDate(String objectId) {
        Enrollment__c docEnr = [
                SELECT Enrollment_From_Documents__r.Study_Start_Date__c
                FROM Enrollment__c
                WHERE Id = :objectId
        ];

        return docEnr.Enrollment_From_Documents__r.Study_Start_Date__c != null ? generateDate(docEnr.Enrollment_From_Documents__r.Study_Start_Date__c) : '';
    }

    global static String generateBirthdate(String objectId) {
        Enrollment__c docEnr = [
            SELECT Enrollment_From_Documents__r.Candidate_Student__r.Birthdate
            FROM Enrollment__c
            WHERE Id = :objectId
        ];

        return docEnr.Enrollment_From_Documents__r.Candidate_Student__r.Birthdate != null ? 
               generateDate(docEnr.Enrollment_From_Documents__r.Candidate_Student__r.Birthdate)
               : '';
    }
}