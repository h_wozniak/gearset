@isTest
private class ZTestTrainingWebservicesWS {
    

    @isTest static void testOpenTrainingScheduleAcceptGroupLaunchingGroup() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5), true);
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = openTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.acceptGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        Enrollment__c newParticipant = [
            SELECT Status__c, Last_Active_Status__c 
            FROM Enrollment__c 
            LIMIT 1
        ];
        
        System.assertEquals(newParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED);
        
        Offer__c schedule = [
            SELECT Launched__c, Group_Status__c 
            FROM Offer__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
        ];
        
        System.assert(schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_LAUNCHED);
    }

    @isTest static void testOpenTrainingScheduleAcceptGroupReserveList() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5), true);
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = openTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.acceptGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        Enrollment__c newParticipant = [
            SELECT Status__c, Last_Active_Status__c 
            FROM Enrollment__c 
            LIMIT 1
        ];
        
        System.assertEquals(newParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS);
        
        Offer__c schedule = [
            SELECT Launched__c, Group_Status__c 
            FROM Offer__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
        ];
        
        System.assert(schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_LAUNCHED);
    }

    @isTest static void testOpenTrainingScheduleAcceptCancelled() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5), true);
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = openTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED,
            Last_Active_Status__c = CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.acceptGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        Enrollment__c newParticipant = [
            SELECT Status__c, Last_Active_Status__c 
            FROM Enrollment__c 
            LIMIT 1
        ];
        
        System.assertEquals(newParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP);
        
        Offer__c schedule = [
            SELECT Launched__c, Group_Status__c 
            FROM Offer__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
        ];
        
        System.assert(schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_LAUNCHED);
    }

    @isTest static void testClosedTrainingScheduleAcceptGroupNewEnrollment() {
        Offer__c closedTrainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        
        Enrollment__c closedTrainingEnrollment = ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_NEW,
            Training_Offer__c = closedTrainingOffer.Id
        ), true);
        
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = closedTrainingEnrollment.Id, 
            Training_Offer_for_Participant__c = closedTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.acceptGroup(closedTrainingOffer.Id);
        Test.stopTest();
        
        Offer__c schedule = [SELECT Launched__c, Group_Status__c FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)];
        
        System.assert(schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_LAUNCHED);
    }

    @isTest static void testClosedTrainingScheduleAcceptGroupCancelledEnrollment() {
        Offer__c closedTrainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        
        Enrollment__c closedTrainingEnrollment = ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED,
            Training_Offer__c = closedTrainingOffer.Id
        ), true);
        
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = closedTrainingEnrollment.Id, 
            Training_Offer_for_Participant__c = closedTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_RESERVE_LIST
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.acceptGroup(closedTrainingOffer.Id);
        Test.stopTest();
        
        Offer__c schedule = [SELECT Launched__c, Group_Status__c FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)];
        
        System.assert(schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_LAUNCHED);
    }

    @isTest static void testClosedTrainingScheduleAcceptGroupWithoutParticipants() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(null, true);
        
        List<Enrollment__c> participants = [
            SELECT Id, Enrollment_Training_Participant__c, Participant__c, Status__c 
            FROM Enrollment__c 
            WHERE Training_Offer_for_Participant__c = :openTrainingOffer.Id 
            AND Status__c NOT IN (:CommonUtility.ENROLLMENT_STATUS_RESIGNATION) 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
        ];
        
        if (participants != null && participants.size() > 0) {
            try {
                delete participants;
            }
            catch (Exception e) {
                System.assertNotEquals(e, null);
            }
        }
        
        Test.startTest();
        String result = TrainingWebservicesWS.acceptGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, 'noParticipants');
    }

    @isTest static void testOpenTrainingScheduleCancelGroup() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(null, true);
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = openTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_PAID
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.cancelGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        Enrollment__c newParticipant = [
            SELECT Status__c, Last_Active_Status__c 
            FROM Enrollment__c 
            LIMIT 1
        ];
        
        System.assertEquals(newParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED);
        
        Offer__c schedule = [
            SELECT Launched__c, Group_Status__c 
            FROM Offer__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
        ];
        
        System.assert(!schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_NOT_LAUNCHED);
    }

    @isTest static void testClosedTrainingScheduleCancelGroup() {
        Offer__c closedTrainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        
        Enrollment__c closedTrainingEnrollment = ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = closedTrainingOffer.Id
        ), true);
        
        Enrollment__c participantEnrollment = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = closedTrainingEnrollment.Id, 
            Training_Offer_for_Participant__c = closedTrainingOffer.Id, 
            Status__c = CommonUtility.ENROLLMENT_STATUS_PAID
        ), true);
        
        Test.startTest();
        TrainingWebservicesWS.cancelGroup(closedTrainingOffer.Id);
        Test.stopTest();
        
        Enrollment__c newParticipant = [SELECT Status__c, Last_Active_Status__c FROM Enrollment__c LIMIT 1];
        System.assertEquals(newParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED);
        
        Offer__c schedule = [SELECT Launched__c, Group_Status__c FROM Offer__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)];
        
        System.assert(!schedule.Launched__c);
        
        System.assertEquals(schedule.Group_Status__c, CommonUtility.OFFER_GROUP_STATUS_NOT_LAUNCHED);
    }

    @isTest static void testClosedTrainingScheduleCancelGroupWithoutParticipants() {
        Offer__c openTrainingOffer = ZDataTestUtility.createOpenTrainingSchedule(null, true);
        
        List<Enrollment__c> participants = [
            SELECT Id, Enrollment_Training_Participant__c, Participant__c, Status__c 
            FROM Enrollment__c 
            WHERE Training_Offer_for_Participant__c = :openTrainingOffer.Id 
            AND Status__c NOT IN (:CommonUtility.ENROLLMENT_STATUS_RESIGNATION) 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
        ];
        
        if (participants != null && participants.size() > 0) {
            try {
                delete participants;
            }
            catch (Exception e) {
                System.assertNotEquals(e, null);
            }
        }
        
        Test.startTest();
        String result = TrainingWebservicesWS.cancelGroup(openTrainingOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, 'ok-np');
    }
    
}