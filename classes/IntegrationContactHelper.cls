/**
*   @author         Sebastian Łasisz
*   @description    Helper class that is used to send contacts to other systems
**/

public with sharing class IntegrationContactHelper {

    public static Map<String, String> addressesTranslatonMap = new Map<String, String>{
            'Lower Silesia' => 'dolnośląskie',
            'Kuyavian-Pomeranian' => 'kujawsko-pomorskie',
            'Lublin' => 'lubelskie',
            'Lubusz' => 'lubuskie',
            'Łódź' => 'łódzkie',
            'Lesser Poland' => 'małopolskie',
            'Masovian' => 'mazowieckie',
            'Opole' => 'opolskie',
            'Subcarpathian' => 'podkarpackie',
            'Podlaskie' => 'podlaskie',
            'Pomeranian' => 'pomorskie',
            'Silesia' => 'śląskie',
            'Świętokrzyskie' => 'świętokrzyskie',
            'Greater Poland' => 'warmińsko-mazurskie',
            'Warmian-Masurian' => 'wielkopolskie',
            'West Pomeranian' => 'zachodniopomorskie'
    };

    public static Map<String, String> universityDictionaryMap;
    public static Map<String, String> countryMap;
    //public static Map<String, Id> highSchoolDictionaryMap;

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- PREPARING DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String prepareResponse(List<ContactToUpdateWrapper> contactsToUpdateWrapper) {
        universityDictionaryMap = IntegrationUniversityDictionaryHelper.getUniversityDictionaryMap_New();
        countryMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Country_of_Origin__c);
        //highSchoolDictionaryMap = IntegrationHighSchoolDictionaryHelper.getHighSchoolDictionaryMap();

        List<String> consentNames = new List<String>{
                RecordVals.MARKETING_CONSENT_1, RecordVals.MARKETING_CONSENT_2,
                RecordVals.MARKETING_CONSENT_4, RecordVals.MARKETING_CONSENT_5
        };

        List<Catalog__c> catalogConsentsToValidate = [
                SELECT Id, Name, ADO__c, Active_To__c, Active_From__c, iPressoId__c
                FROM Catalog__c
                WHERE (Name IN :consentNames
                OR Name LIKE :RecordVals.MARKETING_CONSENT_5 + '%')
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
                AND Active_From__c <= :System.today()
                AND (Active_To__c = null OR Active_To__c >= :System.today())
        ];
        System.debug('catalogConsentsToValidate ' + catalogConsentsToValidate);

        List<Catalog__c> catalogConsentsToValidateForIPresso = [
                SELECT Id, Name, Active_To__c, Active_From__c, iPressoId__c, ADO__c
                FROM Catalog__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
                AND Active_From__c <= :System.today()
                AND (Active_To__c = null OR Active_To__c >= :System.today())
                AND Send_To_iPresso__c = true
                AND IPressoId__c != null
        ];

        Set<Id> contactId = new Set<Id>();
        for (ContactToUpdateWrapper contactToUpdateWrapper : contactsToUpdateWrapper) {
            contactId.add(contactToUpdateWrapper.contact_id);
        }

        Map<Id, Contact> contactsToSendToOtherSystems = new Map<Id, Contact>([
                SELECT Id, FirstName, LastName, Pesel__c, OtherCountry, OtherPostalCode, OtherCity, OtherStreet, OtherState, MailingState, System_Updating_Contact__c,
                        MailingCountry, MailingPostalCode, MailingCity, MailingStreet, Phone, MobilePhone, Additional_Phones__c, Statuses__c, AccountId,
                        Email, Additional_Emails__c, Account.Name, Nationality__c, University_for_sharing__c, Areas_of_Interest__c, Year_of_Graduation__c,
                        A_Level_Issue_Date__c, Position__c, School__c, School_Type__c, School_Name__c, School__r.Name, School__r.School_Type__c,
                        Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c,
                        Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c, (SELECT Id, Lead_Email__c, iPressoId__c FROM iPressoContacts__r WHERE iPressoId__c != null), (
                        SELECT Id, Name, CLassifier__c, Campaign_Contact__c, Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__c, Campaign_Resignation_Reason__c,
                                Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__r.Classifier_ID__c,
                                Campaign_Member__r.External_campaign__r.External_Id__c, External_Id__c,
                                Campaign_Member__r.External_campaign__r.Classifier_System__c,
                                Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c, Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c,
                                Campaign_from_Campaign_Details_2__r.Details_Graduated__c, Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c,
                                Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c
                        FROM Campaign_Contact__r
                        WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                        AND Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__c != null
                        AND External_Id__c != null
                ), (SELECT Id, Status__c, Degree__c, Mode__c, Course_or_Specialty_Offer__r.Trade_Name__c, Specialization__c, Specialization__r.Trade_Name__c, WSB_graduate__c FROM Enrollments_Studies__r ORDER BY CreatedDate DESC LIMIT 1), (SELECT Id, Date_of_Graduation__c, Thesis_Defense_Date__c FROM EducationalAgreements__r ORDER BY CreatedDate ASC LIMIT 1)
                FROM Contact
                WHERE Id IN :contactId
        ]);

        /* Needed to retrieve additional data for university name */
        Map<Id, Contact> contactsWithUniversityMap = new Map<Id, Contact>([
                SELECT Id, University_for_sharing__c, (SELECT Id, Status__c, Degree__c, University_Name__c FROM Enrollments_Studies__r ORDER BY CreatedDate DESC), (SELECT Id, University_Name__c FROM EducationalAgreements__r ORDER BY CreatedDate DESC), (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC), (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC)
                FROM Contact
                WHERE Id IN :contactId
        ]);

        List<ContactUpdateOther_JSON> contactsToUpdate = new List<ContactUpdateOther_JSON>();
        for (ContactToUpdateWrapper contactToUpdateWrapper : contactsToUpdateWrapper) {
            Contact contactToSendToOtherSystems = contactsToSendToOtherSystems.get(contactToUpdateWrapper.contact_id);

            if (contactToSendToOtherSystems != null) {
                if (contactToUpdateWrapper.ifMA && contactToSendToOtherSystems.iPressoContacts__r.size() > 0) {
                    for (Marketing_Campaign__c iPressoContact : contactToSendToOtherSystems.iPressoContacts__r) {
                        ContactUpdateOther_JSON contactToUpdateOtherSystems = new ContactUpdateOther_JSON();
                        if (contactToUpdateWrapper.ifExperia) {
                            contactToUpdateOtherSystems.experia = prepareExperiaOrZPIResponse(contactToSendToOtherSystems, catalogConsentsToValidate);
                        }
                        if (contactToUpdateWrapper.ifZPI) {
                            contactToUpdateOtherSystems.zpi = prepareExperiaOrZPIResponse(contactToSendToOtherSystems, catalogConsentsToValidate);
                        }
                        if (contactToUpdateWrapper.ifFCC) {
                            contactToUpdateOtherSystems.call_center = prepareCallCenterResponse(contactToSendToOtherSystems, contactToSendToOtherSystems.Campaign_Contact__r);
                        }

                        if (contactToSendToOtherSystems.System_Updating_Contact__c != CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO) {
                            contactToUpdateOtherSystems.marketing_automation = prepareMarketingAutomationResponse(contactToSendToOtherSystems, iPressoContact, catalogConsentsToValidateForIPresso, contactsWithUniversityMap);
                        }

                        contactsToUpdate.add(contactToUpdateOtherSystems);
                    }
                } else {
                    ContactUpdateOther_JSON contactToUpdateOtherSystems = new ContactUpdateOther_JSON();
                    if (contactToUpdateWrapper.ifExperia) {
                        contactToUpdateOtherSystems.experia = prepareExperiaOrZPIResponse(contactToSendToOtherSystems, catalogConsentsToValidate);
                    }
                    if (contactToUpdateWrapper.ifZPI) {
                        contactToUpdateOtherSystems.zpi = prepareExperiaOrZPIResponse(contactToSendToOtherSystems, catalogConsentsToValidate);
                    }
                    if (contactToUpdateWrapper.ifFCC) {
                        contactToUpdateOtherSystems.call_center = prepareCallCenterResponse(contactToSendToOtherSystems, contactToSendToOtherSystems.Campaign_Contact__r);
                    }

                    contactsToUpdate.add(contactToUpdateOtherSystems);
                }
            }

        }

        return JSON.serialize(contactsToUpdate);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- BUILDING JSON METHODS -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Experia_or_ZPI_JSON prepareExperiaOrZPIResponse(Contact contactToSendToOtherSystems, List<Catalog__c> consentsToValidate) {
        Experia_or_ZPI_JSON experiaOrZPI = new Experia_or_ZPI_JSON();
        experiaOrZPI.person_id = contactToSendToOtherSystems.Id;
        experiaOrZPI.first_name = contactToSendToOtherSystems.FirstName;
        experiaOrZPI.last_name = contactToSendToOtherSystems.LastName;
        experiaOrZPI.personal_id = contactToSendToOtherSystems.Pesel__c;
        experiaOrZPI.addresses = addressesToList(contactToSendToOtherSystems);
        experiaOrZPI.phones = phonesToList(contactToSendToOtherSystems.Phone, contactToSendToOtherSystems.MobilePhone);
        experiaOrZPI.email = contactToSendToOtherSystems.Email;
        experiaOrZPI.emails_others = emailsToList(contactToSendToOtherSystems.Email, contactToSendToOtherSystems.Additional_Emails__c, false);

        return experiaOrZPI;
    }

    public static List<CallCenter_JSON> prepareCallCenterResponse(Contact contactToSendToOtherSystems, List<Marketing_Campaign__c> marketingCampaigns) {
        List<CallCenter_JSON> marketingConsentsCC = new List<CallCenter_JSON>();

        for (Marketing_Campaign__c marketingCampaign : marketingCampaigns) {
            CallCenter_JSON callCenter = new CallCenter_JSON();
            if (!marketingCampaign.Campaign_Member__r.External_campaign__r.Classifier_System__c) {
                callCenter.fcc_campaign_id = marketingCampaign.Campaign_Member__r.External_campaign__r.External_Id__c;
            }
            callCenter.contacts = new List<CallCenterContact_JSON>();

            CallCenterContact_JSON member = new CallCenterContact_JSON();
            member.person_id = marketingCampaign.External_Id__c;
            member.first_name = contactToSendToOtherSystems.FirstName;
            member.last_name = contactToSendToOtherSystems.LastName;
            member.phones = phonesToList(contactToSendToOtherSystems.Phone, contactToSendToOtherSystems.MobilePhone, contactToSendToOtherSystems.Additional_Phones__c);
            member.emails = emailsToList(contactToSendToOtherSystems.Email, contactToSendToOtherSystems.Additional_Emails__c, true);
            member.classifier_id = marketingCampaign.Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__r.Classifier_ID__c;
            member.resignation_reason = marketingCampaign.Campaign_Resignation_Reason__c == null ? '' : marketingCampaign.Campaign_Resignation_Reason__c;

            member.crm_link = URL.getSalesforceBaseUrl().toExternalForm() + '/' + marketingCampaign.Campaign_Contact__c;
            member.recommended_product = marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c == null ? '' : marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c;
            member.source_of_acquisition = marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c == null ? '' : marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c;
            member.date_of_acquisition = marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c == null ? '' : String.valueOf(marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c);
            member.graduated = marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Graduated__c ? 'Tak' : 'Nie';
            member.graduated_product_name = marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c == null ? '' : marketingCampaign.Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c;

            callCenter.contacts.add(member);

            marketingConsentsCC.add(callCenter);
        }

        return marketingConsentsCC;
    }

    public static MarketingAutomation_JSON prepareMarketingAutomationResponse(Contact contactToSend, Marketing_Campaign__c iPressoContact, List<Catalog__c> catalogConsentsToValidateForIPresso, Map<Id, Contact> contactsWithUniversityMap) {
        MarketingAutomation_JSON iPressoContactToSend = new MarketingAutomation_JSON();
        iPressoContactToSend.ipresso_contact_id = iPressoContact.iPressoId__c;
        iPressoContactToSend.first_name = contactToSend.FirstName;
        iPressoContactToSend.last_name = contactToSend.LastName;
        iPressoContactToSend.email = iPressoContact.Lead_Email__c;
        iPressoContactToSend.mobile = contactToSend.MobilePhone;
        if (contactToSend.AccountId != null
                && !contactToSend.Account.Name.contains('Migracja') && !contactToSend.Account.Name.contains(CommonUtility.INDIVIDUAL_CONTACT_ACCOUNT)) {
            iPressoContactToSend.company = contactToSend.Account.Name;
        }
        iPressoContactToSend.city = contactToSend.MailingCity;
        iPressoContactToSend.crm_contact_status = contactToSend.Statuses__c;
        iPressoContactToSend.postal_code = contactToSend.MailingPostalCode;
        iPressoContactToSend.street = contactToSend.MailingStreet;
        iPressoContactToSend.country = countryMap.get(contactToSend.MailingCountry);
        iPressoContactToSend.state = contactToSend.MailingState != null ? addressesTranslatonMap.get(contactToSend.MailingState) : null;
        iPressoContactToSend.consents = prepareConsentsForContact(contactToSend, catalogConsentsToValidateForIPresso);
        iPressoContactToSend.nationality = splitAdditionalInfoField(contactToSend.Nationality__c);

        Enrollment__c studyEnrWithResignation = prepareDepartmentForIPresso(iPressoContactToSend, contactsWithUniversityMap.get(contactToSend.Id));

        iPressoContactToSend.area_of_interest = splitAdditionalInfoField(contactToSend.Areas_of_Interest__c);
        iPressoContactToSend.a_level_exam_year = contactToSend.Year_of_Graduation__c;
        iPressoContactToSend.school_name = contactToSend.School__c == null ? contactToSend.School_Name__c : contactToSend.School__c;
        iPressoContactToSend.school_type = contactToSend.School__c == null ? IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.School_Type__c) : IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.School__r.School_Type__c);
        iPressoContactToSend.position = contactToSend.Position__c;

        if (!contactToSend.Enrollments_Studies__r.isEmpty()) {
            if (studyEnrWithResignation == null) {
                iPressoContactToSend.degree = prepareDegree(prepareDegreeFromEnrollment(contactsWithUniversityMap.get(contactToSend.Id)));
            } else {
                iPressoContactToSend.degree = null;
                iPressoContactToSend.interested_degree = studyEnrWithResignation.Degree__c;
            }

            iPressoContactToSend.mode = contactToSend.Enrollments_Studies__r.get(0).Mode__c;
            iPressoContactToSend.product_name = contactToSend.Enrollments_Studies__r.get(0).Course_or_Specialty_Offer__r.Trade_Name__c;
            if (contactToSend.Enrollments_Studies__r.get(0).Specialization__c != null) {
                iPressoContactToSend.postgraduate_product_name = contactToSend.Enrollments_Studies__r.get(0).Specialization__r.Trade_Name__c;
            }
            iPressoContactToSend.WSB_graduate = contactToSend.Enrollments_Studies__r.get(0).WSB_graduate__c;
        }
        if (!contactToSend.EducationalAgreements__r.isEmpty()) {
            if (contactToSend.EducationalAgreements__r.get(0).Date_of_Graduation__c != null) {
                iPressoContactToSend.study_graduation_year = String.valueOf(contactToSend.EducationalAgreements__r.get(0).Date_of_Graduation__c.year());
            }
            iPressoContactToSend.defence_date = String.valueOf(contactToSend.EducationalAgreements__r.get(0).Thesis_Defense_Date__c);
        }

        return iPressoContactToSend;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String prepareDegree(String degree) {
        if (degree == CommonUtility.OFFER_DEGREE_I) {
            return 'studia-i-stopnia';
        } else if (degree == CommonUtility.OFFER_DEGREE_II) {
            return 'studia-ii-stopnia';
        } else if (degree == CommonUtility.OFFER_DEGREE_MBA) {
            return 'studia-mba';
        } else if (degree == CommonUtility.OFFER_DEGREE_PG) {
            return 'studia-podyplomowe';
        } else if (degree == CommonUtility.OFFER_DEGREE_U) {
            return 'studia-jednolitego-magisterskie';
        } else if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
            return 'studia-ii-stopnia';
        }

        return null;
    }

    public static String parseDepartmentForIpresso(String department) {
        Map<String, String> entityList = new Map<String, String>{
                RecordVals.WSB_NAME_GDA => 'gdansk',
                RecordVals.WSB_NAME_GDY => 'gdynia',
                RecordVals.WSB_NAME_POZ => 'poznan',
                RecordVals.WSB_NAME_TOR => 'torun',
                RecordVals.WSB_NAME_WRO => 'wroclaw',
                RecordVals.WSB_NAME_BYD => 'bydgoszcz',
                RecordVals.WSB_NAME_OPO => 'opole',
                RecordVals.WSB_NAME_SZC => 'szczecin',
                RecordVals.WSB_NAME_CHO => 'chorzow'
        };

        return entityList.get(department);
    }

    public static String prepareDegreeFromEnrollment(Contact candidate) {
        if (!candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    return studyEnr.Degree__c;
                }
            }
        }

        return null;
    }

    public static Enrollment__c prepareDepartmentForIPresso(MarketingAutomation_JSON iPressoContactToSend, Contact candidate) {
        Boolean departmentFound = false;
        Enrollment__c enrollmentWithResignation = null;

        if (!candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    iPressoContactToSend.department = parseDepartmentForIpresso(studyEnr.University_Name__c);
                    departmentFound = true;
                    break;
                }
            }
        }

        if (!departmentFound && !candidate.EducationalAgreements__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.EducationalAgreements__r.get(0).University_Name__c);
            departmentFound = true;
        }

        if (!departmentFound && !candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    iPressoContactToSend.department = parseDepartmentForIpresso(studyEnr.University_Name__c);
                    departmentFound = true;
                    enrollmentWithResignation = studyEnr;
                    break;
                }
            }
        }

        if (!departmentFound && !candidate.Participants__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.Participants__r.get(0).Training_Offer_for_Participant__r.University_Name__c);
            departmentFound = true;
        }

        if (!departmentFound && !candidate.ReturnedLeads__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.ReturnedLeads__r.get(0).Lead_UniversityWSB__c);
            departmentFound = true;
        }

        if (!departmentFound && candidate.University_for_sharing__c != null) {
            List<String> university = candidate.University_for_sharing__c.split(', ');
            iPressoContactToSend.department = parseDepartmentForIpresso(university.get(university.size() - 1));
            departmentFound = true;
        }

        return enrollmentWithResignation;
    }

    public static List<Address_JSON> addressesToList(Contact contactToSendToOtherSystems) {
        List<Address_JSON> contactAddresses = new List<Address_JSON>();

        Address_JSON homeAddressFromCandidate = new Address_JSON();
        homeAddressFromCandidate.type = 'Home';
        homeAddressFromCandidate.country = contactToSendToOtherSystems.OtherCountry;
        homeAddressFromCandidate.postal_code = contactToSendToOtherSystems.OtherPostalCode;
        homeAddressFromCandidate.state = contactToSendToOtherSystems.OtherState != null ? addressesTranslatonMap.get(contactToSendToOtherSystems.OtherState) : null;
        homeAddressFromCandidate.city = contactToSendToOtherSystems.OtherCity;
        homeAddressFromCandidate.street = contactToSendToOtherSystems.OtherStreet;
        homeAddressFromCandidate.post = contactToSendToOtherSystems.OtherCity;
        contactAddresses.add(homeAddressFromCandidate);

        Address_JSON mailingAddressFromCandidate = new Address_JSON();
        mailingAddressFromCandidate.type = 'Mailing';
        mailingAddressFromCandidate.country = contactToSendToOtherSystems.MailingCountry;
        mailingAddressFromCandidate.postal_code = contactToSendToOtherSystems.MailingPostalCode;
        mailingAddressFromCandidate.state = contactToSendToOtherSystems.MailingState != null ? addressesTranslatonMap.get(contactToSendToOtherSystems.MailingState) : null;
        mailingAddressFromCandidate.city = contactToSendToOtherSystems.MailingCity;
        mailingAddressFromCandidate.street = contactToSendToOtherSystems.MailingStreet;
        mailingAddressFromCandidate.post = contactToSendToOtherSystems.MailingCity;
        contactAddresses.add(mailingAddressFromCandidate);

        return contactAddresses;
    }

    /* Method used to retrieve valid consents for iPresso */
    public static List<MarketingConsents_JSON> prepareConsentsForContact(Contact contactWithConsent, List<Catalog__c> catalogConsentsToValidate) {
        List<String> consentNames = new List<String>{
                RecordVals.MARKETING_CONSENT_1, RecordVals.MARKETING_CONSENT_2, RecordVals.MARKETING_CONSENT_4
        };
        List<MarketingConsents_JSON> consentsForContact = new List<MarketingConsents_JSON>();

        for (Catalog__c catalogConsent : catalogConsentsToValidate) {
            if (contactWithConsent != null && CatalogManager.getApiADONameFromConsentName(catalogConsent.Name) != null
                    && contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)) != null) {

                Set<String> contactAcceptedADO = CommonUtility.getOptionsFromMultiSelect((String) contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)));
                Set<String> catalogSelectedADO = CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c);
                consentsForContact.add(new MarketingConsents_JSON(catalogConsent.IPressoId__c, contactAcceptedADO.containsAll(catalogSelectedADO)));
            } else {
                consentsForContact.add(new MarketingConsents_JSON(catalogConsent.IPressoId__c, false));
            }
        }

        return consentsForContact;
    }

    private static List<String> splitAdditionalInfoField(String fieldValue) {
        if (fieldValue == null) {
            return new List<String>();
        }

        List<String> splitVals = new List<String>();
        for (String splitVal : fieldValue.split(';')) {
            splitVals.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(splitVal));
        }

        return splitVals;
    }

    private static List<String> splitAdditionalInfoFieldWSB(String fieldValue) {
        if (fieldValue == null) {
            return new List<String>();
        }

        List<String> splitVals = new List<String>();
        for (String splitVal : fieldValue.split(', ')) {
            splitVals.add(splitVal);
        }

        return splitVals;
    }

    public static List<Phone_JSON> phonesToList(String phoneNumber, String mobilePhone) {
        List<Phone_JSON> phones = new List<Phone_JSON>();

        if (phoneNumber != null && phoneNumber != '') {
            Phone_JSON phone = new Phone_JSON();
            phone.type = 'Default';
            phone.phoneNumber = phoneNumber;
            phones.add(phone);
        }

        if (mobilePhone != null && mobilePhone != '') {
            Phone_JSON cellPhone = new Phone_JSON();
            cellPhone.type = 'Mobile';
            cellPhone.phoneNumber = mobilePhone;
            phones.add(cellPhone);
        }

        return phones;

    }

    public static List<String> campaignsToList(List<Marketing_Campaign__c> campaigns) {
        List<String> campaign_ids = new List<String>();
        for (Marketing_Campaign__c campaign : campaigns) {
            campaign_ids.add(campaign.Id);
        }

        return campaign_ids;
    }

    public static List<String> phonesToList(String phone, String mobilePhone, String additionalPhones) {
        List<String> phones = new List<String>();
        if (additionalPhones != null && additionalPhones != '') {
            phones = additionalPhones.split('; ');
        }

        if (phone != null) {
            phones.add(phone);
        }

        if (mobilePhone != null) {
            phones.add(mobilePhone);
        }

        return phones;
    }

    public static List<String> emailsToList(String email, String additionalEmails, Boolean addPrimaryEmail) {
        List<String> emails = new List<String>();
        if (additionalEmails != null && additionalEmails != '') {
            emails = additionalEmails.split('; ');
        }
        if (addPrimaryEmail) emails.add(email);

        return emails;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class ContactUpdateOther_JSON {
        public Experia_or_ZPI_JSON experia;
        public Experia_or_ZPI_JSON zpi;
        public List<CallCenter_JSON> call_center;
        public MarketingAutomation_JSON marketing_automation;
    }

    public class Experia_or_ZPI_JSON {
        public String person_id;
        public String first_name;
        public String last_name;
        public String personal_id;
        public List<Address_JSON> addresses;
        public List<Phone_JSON> phones;
        public String email;
        public List<String> emails_others;
    }

    public class Address_JSON {
        public String type;
        public String country;
        public String postal_code;
        public String state;
        public String city;
        public String street;
        public String post;
    }

    public class Phone_JSON {
        public String type;
        public String phoneNumber;
    }

    public class CallCenter_JSON {
        public String fcc_campaign_id;
        public List<CallCenterContact_JSON> contacts;
    }

    public class CallCenterContact_JSON {
        public String person_id;
        public String first_name;
        public String last_name;
        public List<String> phones;
        public List<String> emails;
        public String classifier_id;
        public String resignation_reason;
        public String crm_link;
        public String recommended_product;
        public String source_of_acquisition;
        public String date_of_acquisition;
        public String graduated;
        public String graduated_product_name;
    }

    @TestVisible
    public class MarketingAutomation_JSON {
        public String ipresso_contact_id;
        public String first_name;
        public String last_name;
        public String email;
        public String crm_contact_status;
        public String mobile;
        public String company;
        public String position;
        public String city;
        public String postal_code;
        public String street;
        public String country;
        public String state;
        public List<String> nationality;
        public String department;
        public List<String> area_of_interest;
        public String a_level_exam_year;
        public String degree;
        public String interested_degree;
        public String mode;
        public String product_name;
        public String postgraduate_product_name;
        public Boolean WSB_graduate;
        public String school_type;
        public String school_name;
        public String study_graduation_year;
        public String defence_date;
        public List<MarketingConsents_JSON> consents;

        public MarketingAutomation_JSON() {
        }
    }

    public class MarketingConsents_JSON {
        public String ipresso_consent_id;
        public Boolean consent_value;

        public MarketingConsents_JSON(String ipresso_consent_id, Boolean consent_value) {
            this.ipresso_consent_id = ipresso_consent_id;
            this.consent_value = consent_value;
        }
    }

    public class ContactToUpdateWrapper {
        public String contact_id;
        public Boolean ifExperia;
        public Boolean ifZPI;
        public Boolean ifMA;
        public Boolean ifFCC;

        public ContactToUpdateWrapper() {
            ifExperia = false;
            ifZPI = false;
            ifMA = false;
            ifFCC = false;
        }
    }
}