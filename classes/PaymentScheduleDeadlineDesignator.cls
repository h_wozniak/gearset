/**
    * @author       Wojciech Słodziak
    * @description  Class for generating Payment Schedule Deadline dates
    **/

    public with sharing class PaymentScheduleDeadlineDesignator {

        /* Exception Types */
        public enum RuleExceptionType { FIRST_YEAR, LAST_YEAR, LAST_YEAR_ENGI, LAST_YEAR_3SEM }

        /* Special Day Type */
        public final static Integer DAY_TYPE_LAST = -1;


        /* Designator Config */
        RuleContainer ruleCont;
        Integer monthOffset;
        Integer yearOffset;
        Date studyStartDate;
        Integer yearCount;
        Integer semesterCount;
        String offerKind;


        public PaymentScheduleDeadlineDesignator(String universityName, String degree, Boolean isWinterEnr, Date studyStartDate, Integer instPerYear, 
                                                 Integer yearCount, Integer semesterCount, String startingSemester, String offerKind, Integer numOfSemesters) {

            /* Trigger Warning !! */
            // TO REPLACE AFTER REWORK OF PAYMENTS, TEMPORARY FIX FOR CURRENT ENROLLMENT PERIOD
            String entity = CommonUtility.getEntityByUniversityName(universityName);
            Date newStartStudyDate;
            if (studyStartDate.month() == 2 && isWinterEnr) {
                newStartStudyDate = studyStartDate.addMonths(1);
            }
            else {
                newStartStudyDate = studyStartDate;
            }
            /* END OF CHANGE */

            monthOffset = PaymentScheduleDeadlineDesignator.calculateMonthOffset(isWinterEnr, newStartStudyDate);

            yearOffset = PaymentScheduleDeadlineDesignator.calculateYearOffset(Integer.valueOf(startingSemester));
            this.studyStartDate = newStartStudyDate;
            this.yearCount = yearCount;
            this.semesterCount = semesterCount;
            this.offerKind = offerKind;

            ruleCont = PaymentScheduleDeadlineConfig.buildRules(universityName, degree, instPerYear, isWinterEnr, numOfSemesters);
        }


        /* ------------------------------------------------------------------------------------------------ */
        /* --------------------------------------- HELPER METHODS ----------------------------------------- */
        /* ------------------------------------------------------------------------------------------------ */

        // method for calculating Month Offset in case Studies start late
        private static Integer calculateMonthOffset(Boolean isWinterEnr, Date studyStartDate) {
            if (isWinterEnr) {
                return studyStartDate.month() - 3;
            } else {
                Integer month = studyStartDate.month();
                if (month < 5) {
                    month += 12;
                }
                return studyStartDate.month() - 10;
            }
        }

        // method for calculating Year Offset in case Starting Semester is other than 1st
        // SOMETHINGS WRONG WITH THIS METHOD, IN NEED OF TESTING, REMOVE/ADD -1 AT THE END TO WORK PROPERLY
        private static Integer calculateYearOffset(Integer startingSemester) {
            // FOR DEV
            return -Integer.valueOf((Math.ceil(Decimal.valueOf(startingSemester) / 2) - 1));
            // FOR UAT
            //return -Integer.valueOf((Math.ceil(Decimal.valueOf(startingSemester) / 2) ));
        }




        /* ------------------------------------------------------------------------------------------------ */
        /* ----------------------------------- DESIGNATION ALGORITHM  ------------------------------------- */
        /* ------------------------------------------------------------------------------------------------ */

        /**
         * Method designates Deadline Date for Installments basing on set of Rules, RuleExc are handled in first order
         * @param instYear - is calculated from 0, is academic year
         * @param instNumInYear - number of Installment in current academic year
         * @return Deadline Date for specific Installment
         */
        public Date designateDateForInstallment(Integer instNumInYear, Integer instYear) {
            Date instDeadline;

            RuleExc ruleExcObj = ruleCont.ruleExcMap.get(instNumInYear);

            if (ruleExcObj != null) {
                if (ruleExcObj.exceptionType == RuleExceptionType.LAST_YEAR_ENGI && instYear == (yearCount - 1) && (offerKind == CommonUtility.OFFER_KIND_ENGINEERING || offerKind == CommonUtility.OFFER_KIND_COM_ENG)) {
                    instDeadline = PaymentScheduleDeadlineDesignator.designateDeadlineDate(ruleExcObj, instNumInYear, instYear, monthOffset, yearOffset, studyStartDate);
                }
                
                if (ruleExcObj.exceptionType == RuleExceptionType.LAST_YEAR_3SEM && instYear == (yearCount - 1) && semesterCount == 3) {
                    instDeadline = PaymentScheduleDeadlineDesignator.designateDeadlineDate(ruleExcObj, instNumInYear, instYear, monthOffset, yearOffset, studyStartDate);
                }

                if (ruleExcObj.exceptionType == RuleExceptionType.FIRST_YEAR && instYear == 0) {
                    instDeadline = PaymentScheduleDeadlineDesignator.designateDeadlineDate(ruleExcObj, instNumInYear, instYear, monthOffset, yearOffset, studyStartDate);
                }

                if (ruleExcObj.exceptionType == RuleExceptionType.LAST_YEAR && instYear == (yearCount - 1)) {
                    instDeadline = PaymentScheduleDeadlineDesignator.designateDeadlineDate(ruleExcObj, instNumInYear, instYear, monthOffset, yearOffset, studyStartDate);
                }
            }
            
            if (instDeadline == null) {
                Rule ruleObj = ruleCont.ruleMap.get(instNumInYear);
                instDeadline = PaymentScheduleDeadlineDesignator.designateDeadlineDate(ruleObj, instNumInYear, instYear, monthOffset, yearOffset, studyStartDate);
            }

            return instDeadline;
        }

        /**
         * Method designates Deadline Date for selected rule
         * @param instYear - is calculated from 0, is academic year
         * @return Deadline Date basing on provided rule and rest of parameters
         */
        private static Date designateDeadlineDate(Rule ruleObj, Integer instNumInYear, Integer instYear, Integer monthOffset, Integer yearOffset, Date studyStartDate) {
            if (ruleObj == null) {
                return null;
            }

            Integer month = ruleObj.month + monthOffset;
            Integer year = studyStartDate.year() + instYear + yearOffset;

            if (month > 12) {
                month -= 12;
            }

            if (month < studyStartDate.month() && instNumInYear != 1
                || (
                    month == studyStartDate.month() 
                    && (instNumInYear == 11 || instNumInYear == 12)
                )
            ) {
                year++;
            }

            Integer day = ruleObj.day;
            Integer daysInMonth = Date.daysInMonth(year, month);
            if (day == DAY_TYPE_LAST) {
                day = Date.daysInMonth(year, month);

                if (month == 2) {
                    day = 28;
                }
            } else {
                if (day > daysInMonth) {
                    day = daysInMonth;
                }
            }

            return Date.newInstance(year, month, day);
        }



        /* ------------------------------------------------------------------------------------------------ */
        /* --------------------------------------- MODEL CLASSES ------------------------------------------ */
        /* ------------------------------------------------------------------------------------------------ */

        public class RuleContainer {

            // Map key is Installment Number in Year (Academic)
            public Map<Integer, Rule> ruleMap;
            public Map<Integer, RuleExc> ruleExcMap;

            public RuleContainer() {
                ruleMap = new Map<Integer, Rule>();
                ruleExcMap = new Map<Integer, RuleExc>();
            }
        }

        public virtual class Rule {
            public Integer day;
            public Integer month;

            public Rule(Integer day, Integer month) {
                this.day = day;
                this.month = month;
            }
        }

        public class RuleExc extends Rule {
            public RuleExceptionType exceptionType;

            public RuleExc(Integer day, Integer month, RuleExceptionType exceptionType) {
                super(day, month);
                this.exceptionType = exceptionType;
            }
        }

    }