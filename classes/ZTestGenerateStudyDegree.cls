@isTest
private class ZTestGenerateStudyDegree {
    
    @isTest static void testDegreeFromEnrollmentI() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(studyEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromEnrollmentII() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Degree__c = CommonUtility.OFFER_DEGREE_II, 
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(studyEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromEnrollmentIIPG() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG, 
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(studyEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromEnrollmentU() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_U, 
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_U, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(studyEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentI() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(documentEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentII() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II, 
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(documentEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentIIPG() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG, 
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(documentEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentU() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_U, 
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Kind__c = CommonUtility.OFFER_KIND_MASTER,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_U, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String response = DocGen_GenerateStudyDegree.executeMethod(documentEnrollment.Id, null, null);
        System.assertNotEquals('', response);
        Test.stopTest();
    }
}