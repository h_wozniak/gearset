/**
* @author       Sebastian Łasisz
* @description  Controller for Aura Component (LEX_Send_Email_To_High_School) responsbile for sending workshop proposal email.
**/
public with sharing class LEX_SendEmaiToHighSchoolController {

    @AuraEnabled
    public static String hasEditAccess(String recordId) {
        return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
    }

    @AuraEnabled public static Boolean validateRecord(String recordId) {
        Account highSchool = [
                SELECT Id, WSB_Area__c, Contact_Person__c, Visit_Contact_Person__c
                FROM Account
                WHERE Id = :recordId
        ];

        if (highSchool.WSB_Area__c != null && highSchool.Contact_Person__c != null && highSchool.Visit_Contact_Person__c != null) {
            return true;
        }

        return false;
    }

    /**
     * Method called from aura component to send email.
     *
     * @param accountId Id of high school to send email to.
     *
     * @return Boolean value whether the operation was successful.
     */
    @AuraEnabled
    public static Boolean sendWorkshopProposal(Id accountId) {
        return EmailManager_WorkshopProposal.sendWorkshopProposal(new Set<Id> { accountId });
    }

    @AuraEnabled
    public static Boolean cleanWorkflowEmailNameInvokation(Id accountId) {
        try {
            Account highSchool = new Account();
            highSchool.Id = accountId;
            highSchool.WorkflowEmailNameInvokation__c = null;
            update highSchool;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            return false;
        }

        return true;
    }
}