@isTest
private class ZTestGenerateStudyDegreeForUnenrolled {
    
    @isTest static void testDegreeFromEnrollmentIIUnenrolled() {
        Offer__c studyOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II
        ), true);

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = studyOffer.Id,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String result = DocGen_GenerateStudyDegreeForUnenrolled.executeMethod(studyEnrollment.Id, null, null);
        System.assertEquals(result, '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia SP/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>studia II stopnia SP</w:t></w:r>');
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromEnrollmentIIPGUnenrolled() {
        Offer__c studyOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG
        ), true);

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = studyOffer.Id,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        Test.startTest();
        String result = DocGen_GenerateStudyDegreeForUnenrolled.executeMethod(studyEnrollment.Id, null, null);
        System.assertEquals(result, '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia</w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>/ studia II stopnia SP</w:t></w:r>');
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentIIUnenrolled() {
        Offer__c studyOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II
        ), true);

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = studyOffer.Id,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String result = DocGen_GenerateStudyDegreeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia SP/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>studia II stopnia SP</w:t></w:r>');
        Test.stopTest();
    }
    
    @isTest static void testDegreeFromDocumentEnrollmentIIPGUnenrolled() {
        Offer__c studyOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II_PG
        ), true);

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = studyOffer.Id,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);

        Test.startTest();
        String result = DocGen_GenerateStudyDegreeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> studia II stopnia</w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>/ studia II stopnia SP</w:t></w:r>');
        Test.stopTest();
    }
}