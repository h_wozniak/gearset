@isTest
private class ZTestIntegrationIPressoRemoveContacts {

	@isTest static void test_removeIPressoContactList_1Contact_success() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        List<Marketing_Campaign__c> iPressoContacts = ZDataTestUtility.createIPressoContacts(null, 1, true);

        Set<String> iPressoIds = new Set<String>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
        	iPressoIds.add(iPressoContact.iPressoId__c);
        }

        Test.startTest();
        Boolean result = IntegrationIPressoRemoveContacts.removeIPressoContactList(iPressoIds);
        Test.stopTest();
        System.assert(result);
	}

	@isTest static void test_removeIPressoContactList_5Contact_success() {
		CustomSettingDefault.initEsbConfig();
		
		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        List<Marketing_Campaign__c> iPressoContacts = ZDataTestUtility.createIPressoContacts(null, 5, true);

        Set<String> iPressoIds = new Set<String>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
        	iPressoIds.add(iPressoContact.iPressoId__c);
        }

        Test.startTest();
        Boolean result = IntegrationIPressoRemoveContacts.removeIPressoContactList(iPressoIds);
        Test.stopTest();
        System.assert(result);
	}

	@isTest static void test_removeIPressoContactList_1Contact_failed() {
		CustomSettingDefault.initEsbConfig();
		
		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Failed());

        List<Marketing_Campaign__c> iPressoContacts = ZDataTestUtility.createIPressoContacts(null, 1, true);

        Set<String> iPressoIds = new Set<String>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
        	iPressoIds.add(iPressoContact.iPressoId__c);
        }

        Test.startTest();
        Boolean result = IntegrationIPressoRemoveContacts.removeIPressoContactList(iPressoIds);
        Test.stopTest();
        System.assert(!result);
	}

	@isTest static void test_removeIPressoContactList_1Contact_success_future() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        List<Marketing_Campaign__c> iPressoContacts = ZDataTestUtility.createIPressoContacts(null, 1, true);

        Set<String> iPressoIds = new Set<String>();
        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
        	iPressoIds.add(iPressoContact.iPressoId__c);
        }

        Test.startTest();
        IntegrationIPressoRemoveContacts.removeIPressoContactListAtFuture(iPressoIds);
        Test.stopTest();
	}
}