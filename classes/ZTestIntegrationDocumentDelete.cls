/**
*   @author         Radosław Urbański
*   @description    This class is used to test functionality of IntegrationDocumentDelete which should delete documents from enrollment. 
**/

@isTest
private class ZTestIntegrationDocumentDelete {
	
	@isTest static void test_DeleteDocument_success() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		Enrollment__c enrollmentDocument = [SELECT Id FROM Enrollment__c WHERE Enrollment_from_Documents__c = :enrollment.Id AND Document__r.Name = :RecordVals.CATALOG_DOCUMENT_AGREEMENT LIMIT 1];
		Attachment att = new Attachment(Name = (CommonUtility.UPLOADED_DOCUMENT_PREFIX + 'Test attachment name'), Body = Blob.valueOf('Test attachment body'), ParentId = enrollmentDocument.Id);
		insert att;

		String requestBody = '[{"document_id":"' + enrollmentDocument.Id + '", "uploaded_file_name":"' + att.Name + '"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Attachment insertAttachment = [SELECT Name, Body FROM Attachment];
        System.assertEquals(att.Name, insertAttachment.Name);
		System.assertNotEquals(null, insertAttachment.Body);

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		List<Attachment> resultAttachment = [SELECT Name, Body FROM Attachment];

		System.assertEquals(200, res.statusCode);
		System.assertNotEquals(null, res.responseBody);
		System.assertEquals(0, resultAttachment.size());
	}

	@isTest static void test_DeleteDocument_missingEnrollmentId() {
		// given
		String requestBody = '[{"document_id":"someId", "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id'), res.responseBody);
	}

	@isTest static void test_DeleteDocument_missingDocumentId() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		String requestBody = '[{"document_id":null, "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'document_id'), res.responseBody);
	}

	@isTest static void test_DeleteDocument_missingUploadedFileName() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		String requestBody = '[{"document_id":"someId", "uploaded_file_name":null}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_name'), res.responseBody);
	}

	@isTest static void test_DeleteDocument_enrollmentDoesntExist() {
		// given
		String requestBody = '[{"document_id":"someId", "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', 'someId2');
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604, 'enrollmentId'), res.responseBody);
	}

	@isTest static void test_DeleteDocument_documentDoesntExist() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

		String requestBody = '[{"document_id":"5003000000D8cuI", "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604, 'document_id'), res.responseBody);
	}

	@isTest static void test_DeleteDocument_CannotDeleteAcceptedDocument() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		Enrollment__c enrollmentDocument = [SELECT Id FROM Enrollment__c WHERE Enrollment_from_Documents__c = :enrollment.Id AND Document__r.Name = :RecordVals.CATALOG_DOCUMENT_OATH LIMIT 1];
		enrollmentDocument.Document_Accepted__c = true;
		update enrollmentDocument;

		String requestBody = '[{"document_id":"' + enrollmentDocument.Id + '", "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(res.responseBody = IntegrationError.getErrorJSONBlobByCode(1300), res.responseBody);
	}

	@isTest static void test_DeleteDocument_AttachmentDoesntExist() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		Enrollment__c enrollmentDocument = [SELECT Id FROM Enrollment__c WHERE Enrollment_from_Documents__c = :enrollment.Id AND Document__r.Name = :RecordVals.CATALOG_DOCUMENT_AGREEMENT LIMIT 1];

		String requestBody = '[{"document_id":"' + enrollmentDocument.Id + '", "uploaded_file_name":"Test document name"}]';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentDelete.deleteDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604, 'uploaded_file_name for document_id' + enrollmentDocument.Id), res.responseBody);
	}
}