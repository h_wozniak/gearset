/**
* @description  This class is a Trigger Handler for Trainings - complex logic
**/

public without sharing class TH_Offer extends TriggerHandler.DelegateBase {


    Set<Id> trainingScheduleIdsToAssignTasks;
    Set<Id> trainingScheduleIdsUpdateTasksFrom;
    Set<Id> trainingScheduleIdsUpdateTasksTo;
    List<Offer__c> trainingOffersToDisallowDeletion;
    Set<Id> trainingOfferIdsToReassignTasks;
    List<Offer__c> trainingsToSetCode;
    List<Offer__c> trainingOfferToSetCodeForSchedules;
    List<Offer__c> offersToCheckDuplicates;
    Set<Id> schedulesToSendOverLimitMail;
    Set<Id> schedulesWithTasksToDelete;


    public override void prepareBefore() {
        trainingsToSetCode = new List<Offer__c>();
        trainingOffersToDisallowDeletion = new List<Offer__c>();
        trainingOfferToSetCodeForSchedules = new List<Offer__c>();
        offersToCheckDuplicates = new List<Offer__c>();
    }

    public override void prepareAfter() {
        trainingScheduleIdsToAssignTasks = new Set<Id>();
        trainingScheduleIdsUpdateTasksFrom = new Set<Id>();
        trainingScheduleIdsUpdateTasksTo = new Set<Id>();
        schedulesToSendOverLimitMail = new Set<Id>();
        trainingOfferIdsToReassignTasks = new Set<Id>();
        schedulesWithTasksToDelete = new Set<Id>();

    }

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Offer__c newOffer = (Offer__c)obj;

            /* Wojciech Słodziak */
            // retrieve Offers to set Code_Helper_Num__c and Name
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)) {
                trainingsToSetCode.add(newOffer);
            }

            /* Beniamin Cholewa */
            // duplicate detection
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                offersToCheckDuplicates.add(newOffer);
            }

            /* Wojciech Słodziak */
            // retrieve Offers to set Code_Helper_Num__c and Name
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                trainingOfferToSetCodeForSchedules.add(newOffer);
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c newOffer;
        for (Id key : newOffers.keySet()) {
            newOffer = newOffers.get(key);

            /* Mateusz Pruszyński */
            // adds tasks assigned to training administrator related to training schedules
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                trainingScheduleIdsToAssignTasks.add(newOffer.Id);
            }
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c oldOffer;
        Offer__c newOffer;
        for (Id key : newOffers.keySet()) {
            oldOffer = oldOffers.get(key);
            newOffer = newOffers.get(key);

            /* Beniamin Cholewa */
            // duplicate detection
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)
                            || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                            || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
            )
                    && newOffer.Name != oldOffer.Name
                    ) {
                offersToCheckDuplicates.add(newOffer);
            }
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c oldOffer;
        Offer__c newOffer;
        for (Id key : newOffers.keySet()) {
            oldOffer = oldOffers.get(key);
            newOffer = newOffers.get(key);

            /* Mateusz Pruszyński */
            // if training administrator gets changed, all related tasks should be reassigned
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)
                    && newOffer.Type__c == CommonUtility.OFFER_TYPE_OPEN && newOffer.Training_Administrator__c != oldOffer.Training_Administrator__c
                    && String.isNotBlank(newOffer.Training_Administrator__c)) {
                trainingOfferIdsToReassignTasks.add(newOffer.Id);
            }

            /* Mateusz Pruszyński */
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)) {
                // if schedule start date gets changed, all related tasks' reminders should get changed (due date)
                if (newOffer.Valid_From__c != oldOffer.Valid_From__c) {
                    trainingScheduleIdsUpdateTasksFrom.add(newOffer.Id);
                }
                // if schedule end date gets changed, all related tasks' reminders should get changed (due date)
                if (newOffer.Valid_To__c != oldOffer.Valid_To__c) {
                    trainingScheduleIdsUpdateTasksTo.add(newOffer.Id);
                }
            }

            /* Wojciech Słodziak */
            // if condition is met send email to schedule training administrator
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                            || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
            )
                    && newOffer.Potential_Candidates__c != null && newOffer.Potential_Candidates__c != oldOffer.Potential_Candidates__c
                    && newOffer.Number_of_Seats__c != null && newOffer.Potential_Candidates__c >= newOffer.Number_of_Seats__c
                    && (
                    oldOffer.Potential_Candidates__c == null
                            || newOffer.Potential_Candidates__c > oldOffer.Potential_Candidates__c
            )
                    )  {
                schedulesToSendOverLimitMail.add(newOffer.Id);
            }

            /* Sebastian Łasisz */
            // remove tasks from not launched training
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                            || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
            )
                    && newOffer.Group_Status__c != oldOffer.Group_Status__c && newOffer.Group_Status__c == CommonUtility.OFFER_GROUP_STATUS_NOT_LAUNCHED
                    ) {
                schedulesWithTasksToDelete.add(newOffer.Id);
            }
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Offer__c oldOffer;
        for (Id key : oldOffers.keySet()) {
            oldOffer = oldOffers.get(key);

            /* Mateusz Pruszyński */
            // does not allow training offers deletion with related active training schedules
            if (oldOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)) {
                trainingOffersToDisallowDeletion.add(oldOffer);
            }
        }
    }

    public override void afterDelete(Map<Id, sObject> old) { }

    public override void finish() {
        /* Mateusz Pruszyński */
        // adds tasks assigned to training administrator related to training schedules (for Type Open)
        if (trainingScheduleIdsToAssignTasks != null && !trainingScheduleIdsToAssignTasks.isEmpty()) {
            TaskManager.assignTasks(trainingScheduleIdsToAssignTasks);
        }

        /* Mateusz Pruszyński */
        // if training administrator gets changed, all related tasks should be reassigned
        if (trainingOfferIdsToReassignTasks != null && !trainingOfferIdsToReassignTasks.isEmpty()) {
            TaskManager.reasignTasksOnTrainingAdministratorChange(trainingOfferIdsToReassignTasks);
        }

        /* Mateusz Pruszyński */
        // if schedule start date gets changed, all related tasks' reminders should get changed (due date)
        if (trainingScheduleIdsUpdateTasksFrom != null && !trainingScheduleIdsUpdateTasksFrom.isEmpty()) {
            TaskManager.changeTasksReminersOnScheduleStartDateChange(trainingScheduleIdsUpdateTasksFrom);
        }

        /* Mateusz Pruszyński */
        // if schedule end date gets changed, all related tasks' reminders should get changed (due date)
        if (trainingScheduleIdsUpdateTasksTo != null && !trainingScheduleIdsUpdateTasksTo.isEmpty()) {
            TaskManager.changeTasksRemindersOnScheduleEndDateChagen(trainingScheduleIdsUpdateTasksTo);
        }

        /* Wojciech Słodziak */
        // set Code_Helper_Num__c and Name for training offers
        if (trainingsToSetCode != null && !trainingsToSetCode.isEmpty()) {
            OfferManager_Trainings.setCodeHelperNameAndNameForTrainingOffers(trainingsToSetCode);
        }

        /* Wojciech Słodziak */
        // set Code_Helper_Num__c and Name for schedules
        if (trainingOfferToSetCodeForSchedules != null && !trainingOfferToSetCodeForSchedules.isEmpty()) {
            OfferManager_Trainings.setCodeHelperNameAndNameForSchedules(trainingOfferToSetCodeForSchedules);
        }


        /* Mateusz Pruszyński */
        // does not allow training offers deletion with related active training schedules
        if (trainingOffersToDisallowDeletion != null && !trainingOffersToDisallowDeletion.isEmpty()) {
            OfferManager_Trainings.disableTrainingOffersDeletionWithActiveSchedule(trainingOffersToDisallowDeletion);
        }

        /* Sebastian Łasisz */
        // remove tasks from not launched training
        if (schedulesWithTasksToDelete != null && !schedulesWithTasksToDelete.isEmpty()) {
            TaskManager.removeTasksFromNotLaunchedTraining(schedulesWithTasksToDelete);
        }

        /* Wojciech Słodziak */
        // if Potential candidates increased and is bigger or equals to Available seats send email to schedule training administrator (KEEP AT BOTTOM)
        if (schedulesToSendOverLimitMail != null && !schedulesToSendOverLimitMail.isEmpty()) {
            EmailManager.sendOverlimitEmailToTrainingAdmin(schedulesToSendOverLimitMail);
        }

        /* Beniamin Cholewa */
        // Offer name duplicate detection (KEEP AT BOTTOM)
        if (offersToCheckDuplicates != null && !offersToCheckDuplicates.isEmpty()) {
            OfferManager_Trainings.duplicateNameOfferDetection(offersToCheckDuplicates);
        }
    }
}