public with sharing class ClassifierMappingController {
	public List<SelectOption> externalClassifiers { get; set; }
	public List<Marketing_Campaign__c> currentClassifiers { get; set; }

	public String retURL;
	public String campaignId;

	public ClassifierMappingController() {
		campaignId = ApexPages.currentPage().getParameters().get('campaignId');
        retURL = ApexPages.currentPage().getParameters().get('retURL');

        Set<String> namesOfClassifiersToAdd = new Set<String> { 
            'vmd (system)', 'odebrany (system)', 'nieodebrany (system)', 'błędny (system)', 'zajęty (system)', 'zamknięcie (system)', 
            'abandon (system)', 'zamknięcie (system, predictive)', 'nieobsłużony (system)', 'recall (system, edycja klasyfikatora)', 
            'recall (system, usunięcie klasyfikatora)', 'zamknięcie (system, edycja klasyfikatora)', 
            'zamknięcie (system, usunięcie klasyfikatora)', 'pominięty (system)', 'transfer (system)', 
            'transfer na zewnątrz (system)', 'konsultacja (system)', 'konsultacja na zewnątrz (system)', 
            'zamknięcie (system, limit połączeń)', 'zamknięcie (system, czarna lista)', 'zamknięcie (system, hlr)', 
            'pds odmowa (system)', 'przekierowanie (system)', 'recall ticket (system)', 'oczekujące nieodebrane (system)'
        };

		currentClassifiers = [
			SELECT Id, Classifier_Name__c, Classifier_from_External_Classifier__c 
			FROM Marketing_Campaign__c
			WHERE Campaign_from_Classifier__c = :campaignId
			AND Classifier_Name__c NOT IN :namesOfClassifiersToAdd
		];

		if (campaignId != null) {
			externalClassifiers = acquireExternalClassifiers(campaignId);
		}
	}

	public List<SelectOption> acquireExternalClassifiers(Id campaignId) {
		Marketing_Campaign__c mainCampaign = [
			SELECT Id, External_campaign__c
			FROM Marketing_Campaign__c
			WHERE Id = :campaignId
		];

		List<Marketing_Campaign__c> externalClassifiers = [
			SELECT Id, Classifier_Name__c
			FROM Marketing_Campaign__c
			WHERE External_Campaign_from_Classifier__c = :mainCampaign.External_campaign__c
			AND Classifier_System__c = false
		];

		List<SelectOption> externalClassifiersSelect = new List<SelectOption>();
		for (Marketing_Campaign__c externalClassifier : externalClassifiers) {
			if (externalClassifier.Classifier_Name__c != null) {
				externalClassifiersSelect.add(new SelectOption(externalClassifier.Id, externalClassifier.Classifier_Name__c));
			}
		}

		return externalClassifiersSelect;
	}

	public PageReference save() {
		Set<Id> externalClassifiers = new Set<Id>();
		for (Marketing_Campaign__c classifier : currentClassifiers) {
			if (!externalClassifiers.contains(classifier.Classifier_from_External_Classifier__c)) {
				externalClassifiers.add(classifier.Classifier_from_External_Classifier__c);
			}
			else {
			    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_cantAttachClassifier));
				return null;
			}
		}

		try {
			update currentClassifiers;
		}
		catch (Exception e) {
			ErrorLogger.log(e);
		}

		return new PageReference('/' + campaignId);
	}

    public PageReference cancel() {
        if (retURL != null) {
            return new PageReference(retURL);
        }
        return new PageReference('/');
    }
}