/**
*   @author         Grzegorz Długosz
*   @description    This class is used to test functionality of IntegrationEmailResource which should
*                   inform external systems if given email already exists in CRM. 
**/

@isTest
private class ZTestIntegrationEmailResource {

    @isTest static void checkExistingEmail_code200() {

        // prepare data (Contact with specified email address)
        Contact cnt = new Contact(
            LastName = 'testName',
            Phone = '999888777',
            Email = 'testEmail@testEmail.com'
        );

        // insert contact into crm
        ZDataTestUtility.createCandidateContact(cnt, true);

        // set required request arguments
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        // json
        String msg = '{"email":"testEmail@testEmail.com"}';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/check-email/';
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        req.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.requestBody = Blob.valueof(msg);

        Test.startTest();        
        	IntegrationEmailResource.checkEmail();
        Test.stopTest();

        System.assertEquals(200, res.statusCode);
        System.assertEquals('{"email_exists":true}', res.responseBody.toString());

    }

    @isTest static void checkNotExistingEmail_code200() {

        // set required request arguments
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        // json
        String msg = '{"email":"emailDoesntExist@testEmail.com"}';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/check-email/';
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        req.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.requestBody = Blob.valueof(msg);

        Test.startTest();        
            IntegrationEmailResource.checkEmail();
        Test.stopTest();

        System.assertEquals(200, res.statusCode);
        System.assertEquals('{"email_exists":false}', res.responseBody.toString());

    }

    @isTest static void badJson_code400() {

        // set required request arguments
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        // json
        String msg = 'this JSON is very wrong...';

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/check-email/';
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        req.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.requestBody = Blob.valueof(msg);

        Test.startTest();        
            IntegrationEmailResource.checkEmail();
        Test.stopTest();

        System.assertEquals(400, res.statusCode);
    }
}