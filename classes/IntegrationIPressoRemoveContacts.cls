/**
*   @author         Sebastian Łasisz
*   @description    This class is used to remove Contact in iPresso
**/

global without sharing class IntegrationIPressoRemoveContacts {

	@future(callout=true)
    public static void removeIPressoContactListAtFuture(Set<String> contactsToSendIds) {
        IntegrationIPressoRemoveContacts.removeIPressoContactList(contactsToSendIds);
    }

    public static Boolean removeIPressoContactList(Set<String> contactsToSendIds) {	    
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

    	String jsonToSend = prepareIPressoContactList(contactsToSendIds);
        System.debug(jsonToSend);
    	String endPoint = esb.Remove_iPresso_Contact__c;
    	 			
        HttpRequest httpReq = new HttpRequest();
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + endPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        httpReq.setBody(jsonToSend);

        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = httpRes.getStatusCode() == 200;
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoRemoveContacts.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n');
        }

        return success; 
    }

    public static String prepareIPressoContactList(Set<String> iPressoIdsToRemove) {
    	List<ContactIPresso_JSON> contactsToSendToIPresso = new List<ContactIPresso_JSON>();

		for (String iPressoId : iPressoIdsToRemove) {
			ContactIPresso_JSON iPressoContactToSend = new ContactIPresso_JSON();
			iPressoContactToSend.ipresso_contact_id = iPressoId;

			contactsToSendToIPresso.add(iPressoContactToSend);
		}

    	return JSON.serialize(contactsToSendToIPresso);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    global class ContactIPresso_JSON {
    	public String ipresso_contact_id;
    }
}