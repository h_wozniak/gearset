/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with extranet emails
**/

/* ------------------------------------------ example --------------------------------------------
Database.executeBatch(
    new FixSendingExtranetEmailsBatch(),
    1
);
------------------------------------------- end of example ------------------------------------- */

global class FixSendingExtranetEmailsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

	global FixSendingExtranetEmailsBatch() { }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        	SELECT ID, TECH_Extranet_Data_Update_Date__c 
        	FROM Enrollment__c 
        	WHERE (Status__c = 'Didactics in KS' OR Status__c = 'Payments in KS') 
        	AND Educational_Agreement__r.Student_No__c != null 
        	AND Educational_Agreement__r.Portal_Login__c != null 
        	AND Dont_send_automatic_emails__c = true
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Enrollment__c> studyEnrollments) { 
		Set<Id> enrollmentIds = new Set<Id>();
		for (Enrollment__c studyEnrollment : studyEnrollments) {
			studyEnrollment.Dont_send_automatic_emails__c = false;
			enrollmentIds.add(studyEnrollment.Id);
		}

		update studyEnrollments;

        EmailManager_StudyExtranetEmail.sendExtranetInformationnEmail(enrollmentIds);
	}

	global void finish(Database.BatchableContext BC) { }
}