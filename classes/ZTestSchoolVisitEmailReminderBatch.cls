@isTest
global class ZTestSchoolVisitEmailReminderBatch {

    @isTest static void test_SchoolVisitEmailReminderBatch_1() {
        Datetime dayOfVisitFrom = System.now().addDays(1);
        Datetime dayOfVisitTo = System.now().addDays(1).addMinutes(30);

        ZDataTestUtility.createHighSchoolVisit(new Event(StartDateTime = dayOfVisitFrom, EndDateTime = dayOfVisitTo), true);

        Test.startTest();
        SchoolVisitEmailReminderBatch schoolVisitReminder = new SchoolVisitEmailReminderBatch(1);
        Database.executeBatch(schoolVisitReminder, 200);
        Test.stopTest();

        List<Task> sentEmails = [
                SELECT Id
                FROM Task
        ];

        System.assertEquals(sentEmails.size(), 1);
    }

    @isTest static void test_SchoolVisitEmailReminderBatch_2() {

        ZDataTestUtility.createHighSchoolVisit(null, true);

        Test.startTest();
        SchoolVisitEmailReminderBatch schoolVisitReminder = new SchoolVisitEmailReminderBatch(1);
        Database.executeBatch(schoolVisitReminder, 200);
        Test.stopTest();

        List<Task> sentEmails = [
                SELECT Id
                FROM Task
        ];

        System.assertEquals(sentEmails.size(), 0);
    }
}