/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to store methods related to Generating Documents from Generator
**/

public without sharing class DocumentGeneratorManager {

    public static final String DEFAULT_PATH = '.';

    public static final String SUCCESS_STATUS = 'Ok';

    /* Wojciech Słodziak */
    // universal method to generate document for various parameters, designates templateId on it's own
    @Future(callout = true)
    public static void generateDocument(String language, String docName, String degree, String universityName, Decimal referenceNumber, Id pinAttachmentToId, Boolean toPdf, Boolean encodeWithPassword) {
        Id templateId = TemplateManager.getDocumentTemplateId(language, docName, degree, universityName);

        CommonUtility.skipDocumentValidations = true;

        if (templateId != null) {
            String extension = (toPdf? '.pdf' : '.docx');

            String fileName;
            if (language == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                fileName = 'REF-' + String.valueOf((Long)referenceNumber) + ' ' + '[' + CommonUtility.getLanguageAbbreviationDocGen(language) + '] ' + docName + ' [' + system.now().format() + ']' + extension;
            }
            else {
                fileName = 'REF-' + String.valueOf((Long)referenceNumber) + ' ' + docName + ' [' + system.now().format() + ']' + extension;
            }

            String password = encodeWithPassword ? retrievePasswordForDocument(pinAttachmentToId) : null;
            fileName = encodeWithPassword ? fileName : '[' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + ']' + fileName;
            String status = enxoodocgen.DocGen.generateDocument(templateId, pinAttachmentToId, fileName, DEFAULT_PATH, password);

            if (status != SUCCESS_STATUS) {
                updateStudyEnrGenerationStatus(pinAttachmentToId);
            }
        }
        else {
            updateStudyEnrGenerationStatus(pinAttachmentToId);
        }
    }

    /* Sebastian Łasisz */
    // universal method to generate document for various parameters, designates templateId on it's own
    public static void generateDocumentSync(String language, String docName, String degree, String universityName, Decimal referenceNumber, Id pinAttachmentToId, Boolean toPdf, Boolean encodeWithPassword) {
        Id templateId = TemplateManager.getDocumentTemplateId(language, docName, degree, universityName);

        CommonUtility.skipDocumentValidations = true;

        if (templateId != null) {
            String extension = (toPdf? '.pdf' : '.docx');

            String fileName;
            if (language == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                fileName = 'REF-' + String.valueOf((Long)referenceNumber) + ' ' + '[' + CommonUtility.getLanguageAbbreviationDocGen(language) + '] ' + docName + ' [' + system.now().format() + ']' + extension;
            }
            else {
                fileName = 'REF-' + String.valueOf((Long)referenceNumber) + ' ' + docName + ' [' + system.now().format() + ']' + extension;
            }

            if (!Test.isRunningTest()) {
                String password = encodeWithPassword ? retrievePasswordForDocument(pinAttachmentToId) : null;
                fileName = encodeWithPassword ? fileName : '[' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + ']' + fileName;
                String status = enxoodocgen.DocGen.generateDocument(templateId, pinAttachmentToId, fileName, DEFAULT_PATH, password);

                if (status != SUCCESS_STATUS) {
                    updateStudyEnrGenerationStatus(pinAttachmentToId);
                }
            }
        }
        else {
            updateStudyEnrGenerationStatus(pinAttachmentToId);
        }
    }

    public static String retrievePasswordForDocument(Id pinAttachmentToId) {
        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_from_Documents__r.Candidate_Student__r.Number_and_Series__c
                FROM Enrollment__c
                WHERE Id = :pinAttachmentToId
        ];

        String potentialPassword = studyEnr.Enrollment_from_Documents__r.Candidate_Student__r.Number_and_Series__c;

        if (potentialPassword != null) {
            return potentialPassword.right(6);
        }

        return null;
    }

    public static void updateStudyEnrGenerationStatus(Id pinAttachmentToId) {
        Enrollment__c studyEnrToUpdate = new Enrollment__c(
                Id = pinAttachmentToId,
                Current_File__c  = CommonUtility.GENERATION_FAILED
        );
        try {
            update studyEnrToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

}