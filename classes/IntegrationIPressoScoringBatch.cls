/**
* @author       Sebastian Łasisz
* @description  Batch class for creating Tasks and updating Campaign Members for FCC Interaction
**/

global class IntegrationIPressoScoringBatch implements Database.Batchable<IntegrationIPressoScoringHelper.ScoringReceiver_JSON>, Database.AllowsCallouts {
    DateTime timeToUpdate;
    String jsonBody;

    global IntegrationIPressoScoringBatch() {
        timeToUpdate = DateTime.now();
    }
    
    global Iterable<IntegrationIPressoScoringHelper.ScoringReceiver_JSON> start(Database.BatchableContext BC) {
        jsonBody = IntegrationIPressoScoringHelper.sendInteractionRequest();

        List<IntegrationIPressoScoringHelper.ScoringReceiver_JSON> scoringList;

        try {
            scoringList = IntegrationIPressoScoringHelper.parse(jsonBody);
        } catch(Exception ex) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoScoringHelper.class.getName() 
            + '\n' + jsonBody + '\n');
        }

        if (scoringList != null && !scoringList.isEmpty()) {
            scoringList.sort();
        }

        return scoringList;
    }

    global void execute(Database.BatchableContext BC, List<IntegrationIPressoScoringHelper.ScoringReceiver_JSON> scope) {
    	Set<String> iPressoIds = new Set<String>();
    	for (IntegrationIPressoScoringHelper.ScoringReceiver_JSON iPressoContact : scope) {
    		iPressoIds.add(iPressoContact.ipresso_contact_id);
    	}

    	List<Marketing_Campaign__c> iPressoContacts = [
    		SELECT Id, iPressoId__c, Scoring__c, Scoring_Calculation_Date__c
    		FROM Marketing_Campaign__c
    		WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
    		AND iPressoId__c IN :iPressoIds
    	];

    	for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
    		for (IntegrationIPressoScoringHelper.ScoringReceiver_JSON iPressoContactFromScope : scope) {
    			if (iPressoContact.iPressoId__c == iPressoContactFromScope.ipresso_contact_id) {
    				iPressoContact.Scoring__c = iPressoContactFromScope.contact_scoring;
    				iPressoContact.Scoring_Calculation_Date__c = IntegrationIPressoScoringHelper.stringToDateTime(iPressoContactFromScope.scoring_calculation_date);
    			}
    		}
    	}

        try {
        	update iPressoContacts;
        } 
        catch(Exception ex) {
            ErrorLogger.logAndMsg(ex, CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationIPressoScoringHelper.class.getName() 
            + '\n' + jsonBody + '\n');
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        iPresso_Last_Scoring_Retrival_Date__c lastScoringRetrieval = iPresso_Last_Scoring_Retrival_Date__c.getOrgDefaults();
        lastScoringRetrieval.Date_From__c = timeToUpdate;

        update lastScoringRetrieval;
    }

}