public with sharing class LEX_Merge_automaticallyController {
	
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static Contact getCurrentContact(String recordId) {
		Contact con = [SELECT Id, Potential_Duplicate__c FROM Contact WHERE Id = :recordId];
		return con;
	}

	@AuraEnabled
	public static String mergeAutomatically(String contactId, String potentialDuplicateId) {
		return LEXStudyUtilities.mergeContacts(contactId, potentialDuplicateId);
	}
}