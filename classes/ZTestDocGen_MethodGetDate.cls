@isTest
private class ZTestDocGen_MethodGetDate {

	@isTest static void test_generateDate() {
		Test.startTest();
		String result = DocGen_MethodGetDate.executeMethod(null, null, 'todaysDate');
		Test.stopTest();

		System.assertNotEquals(result, '');
	}

	@isTest static void test_generateDate_month5() {
		Test.startTest();
		String result = DocGen_MethodGetDate.generateDate(Date.newInstance(2017, 5, 5));
		Test.stopTest();

		System.assertEquals(result, '05.05.2017');
	}

	@isTest static void test_generateDate_month15() {
		Test.startTest();
		String result = DocGen_MethodGetDate.generateDate(Date.newInstance(2017, 10, 15));
		Test.stopTest();

		System.assertEquals(result, '15.10.2017');
	}

	@isTest static void test_generateBirthdate() {
		Contact c = ZDataTestUtility.createCandidateContact(new Contact(Birthdate = Date.newInstance(2017, 10, 10)), true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id), true);
        Enrollment__c docEnr = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnr.Id), true);

		Test.startTest();
		String result = DocGen_MethodGetDate.executeMethod(docEnr.Id, null, 'birthdate');
		Test.stopTest();

		System.assertEquals(result, '10.10.2017');		
	}

	@isTest static void test_generateBirthdate_Empty() {
		Contact c = ZDataTestUtility.createCandidateContact(null, true);
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = c.Id), true);
        Enrollment__c docEnr = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnr.Id), true);

		Test.startTest();
		String result = DocGen_MethodGetDate.executeMethod(docEnr.Id, null, 'birthdate');
		Test.stopTest();

		System.assertEquals(result, '');		
	}
}