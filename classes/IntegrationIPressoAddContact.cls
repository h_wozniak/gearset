/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Candidate Integration
**/

@RestResource(urlMapping = '/iPresso/contact_insert/*')
global without sharing class IntegrationIPressoAddContact {

    @HttpPost
    global static void receiveContactInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();

        IntegrationIPressoAddContact.IPressoContact_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(601, e.getMessage()));
            res.statusCode = 400;
            return ;
        }

        try {
            validateJSON(parsedJson);
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(800, e.getMessage()));
            res.statusCode = 400;
            return ;
        }

        /* Create new Contact from iPresso */
        List<Marketing_Campaign__c> iPressoContactToValidate = [
                SELECT Id, Contact_from_iPresso__c, Contact_from_iPresso__r.University_for_sharing__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                AND Lead_Email__c = :parsedJson.email
                AND iPressoId__c = :parsedJson.ipresso_contact_id
        ];

        Contact contactToInsert = new Contact();

        if (iPressoContactToValidate.size() > 0) {
            contactToInsert = [
                    SELECT Id, Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, University_for_sharing__c, Tags__c
                    FROM Contact
                    WHERE Id = :iPressoContactToValidate.get(0).Contact_from_iPresso__c
            ];
        }

        Map<String, String> addressesTranslatonMap = new Map<String, String>{
                'dolnośląskie' => 'Lower Silesia',
                'kujawsko-pomorskie' => 'Kuyavian-Pomeranian',
                'lubelskie' => 'Lublin',
                'lubuskie' => 'Lubusz',
                'łódzkie' => 'Łódź',
                'małopolskie' => 'Lesser Poland',
                'mazowieckie' => 'Masovian',
                'opolskie' => 'Opole',
                'podkarpackie' => 'Subcarpathian',
                'podlaskie' => 'Podlaskie',
                'pomorskie' => 'Pomeranian',
                'śląskie' => 'Silesia',
                'świętokrzyskie' => 'Świętokrzyskie',
                'warmińsko-mazurskie' => 'Warmian-Masurian',
                'wielkopolskie' => 'Greater Poland',
                'zachodniopomorskie' => 'West Pomeranian'
        };

        contactToInsert.System_Updating_Contact__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO;
        if (contactToInsert.Source__c == null) {
            contactToInsert.Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO;
            contactToInsert.Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO;
        }
        if (parsedJson != null) {
            contactToInsert.FirstName = parsedJson.first_name;
            contactToInsert.LastName = parsedJson.last_name;
            contactToInsert.Email = parsedJson.email;
            contactToInsert.MobilePhone = parsedJson.mobile;
            contactToInsert.Degree__c = CommonUtility.getProperPicklistLabel(Contact.Degree__c, parsedJson.degree);
            contactToInsert.Position__c = CommonUtility.getProperPicklistLabel(Contact.Position__c, parsedJson.position);

            String potentialState = addressesTranslatonMap.get(parsedJson.state);
            contactToInsert.MailingState = potentialState != null ? potentialState : parsedJson.state;

            contactToInsert.MailingCity = parsedJson.city;

            String potentialCountry = DictionaryTranslator.getKeyForTranslatedValue(parsedJson.country, 'PL', Contact.Country_of_Origin__c);
            if (potentialCountry != null) {
                contactToInsert.MailingCountry = potentialCountry;
            }
            else if (parsedJson.country != null) {
                contactToInsert.MailingCountry = parsedJson.country;
            }
            else if (parsedJson.country == null && potentialState != null) {
                contactToInsert.MailingCountry = CommonUtility.CONTACT_COUNTRY_POLAND;
            }

            contactToInsert.MailingPostalCode = parsedJson.postal_code;
            contactToInsert.MailingStreet = parsedJson.street;
            contactToInsert.Year_of_Graduation__c = parsedJson.a_level_exam_year;

            if (iPressoContactToValidate.size() > 0) {
                contactToInsert.University_for_sharing__c = listToString(iPressoContactToValidate.get(0).Contact_from_iPresso__r.University_for_sharing__c, parsedJson.department);
            }
            else {
                contactToInsert.University_for_sharing__c = listToString('', parsedJson.department);
            }

            contactToInsert.Areas_of_Interest__c = listToStringFromPicklist(Contact.Areas_of_Interest__c, null, parsedJson.area_of_interest);
            contactToInsert.Tags__c = prepareTags(contactToInsert.Tags__c, parsedJson.tags);
        }

        /* Create Marketing Consents for Contact */
        List<Catalog__c> catalogConsentsToValidate = [
                SELECT Id, Name, Active_To__c, Active_From__c, ADO__c, IPressoId__c, Consent_ADO_API_Name__c
                FROM Catalog__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
                AND Active_From__c <= :System.today()
                AND (Active_To__c = null OR Active_To__c >= : System.today())
                AND Send_To_iPresso__c = true
                AND IPressoId__c != null
        ];

        Map<String, Boolean> globalConsentValues = IPressoManager.prepareGlobalConsentsForContact(contactToInsert, catalogConsentsToValidate);

        /* Prepare non-global consents first */
        for (IntegrationIPressoAddContact.MarketingConsents_JSON consent : parsedJson.consents) {
            for (Catalog__c catalogConsent : catalogConsentsToValidate) {
                if (consent.consent_id == catalogConsent.IPressoId__c && !globalConsentValues.keySet().contains(consent.consent_id)) {
                    if (consent.consent_value) {
                        contactToInsert = MarketingConsentManager.updateConsentOnContact(CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c), contactToInsert, catalogConsent.Consent_ADO_API_Name__c);
                    }
                    else {
                        contactToInsert = MarketingConsentManager.removeConsentOnContact(CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c), contactToInsert, catalogConsent.Consent_ADO_API_Name__c);
                    }
                }
            }
        }

        /* Prepare global consents if they are changed */
        for (IntegrationIPressoAddContact.MarketingConsents_JSON consent : parsedJson.consents) {
            for (Catalog__c catalogConsent : catalogConsentsToValidate) {
                if (consent.consent_id == catalogConsent.IPressoId__c && globalConsentValues.get(consent.consent_id) != null
                        && consent.consent_value != globalConsentValues.get(consent.consent_id))
                {
                    if (consent.consent_value) {
                        contactToInsert = MarketingConsentManager.updateConsentOnContact(CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c), contactToInsert, catalogConsent.Consent_ADO_API_Name__c);
                    }
                    else {
                        /* For global consents always take all entities even though some might be already taken (iPresso only!) */
                        contactToInsert.put(catalogConsent.Consent_ADO_API_Name__c, null);
                    }
                }
            }
        }

        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            CommonUtility.preventCreatingIPressoContact = true;
            CommonUtility.skipConsentsCreation = true;
            contactToInsert.Confirmed_Contact__c = false;
            upsert contactToInsert;
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600, e.getMessage()));
            res.statusCode = 400;
            return;
        }

        /*
        Apparently when you uncomment this piece of code ipresso contact would be created twice. Keep until proven that exactly 1 contact is created...
         */

        /* Attach iPresso Id to iPresso Contact */
        //Marketing_Campaign__c iPressoContact;
        //if (iPressoContactToValidate.size() > 0) {
        //    iPressoContact = iPressoContactToValidate.get(0);
        //    iPressoContact.Contact_from_iPresso__c = contactToInsert.Id;
        //}
        //else {
        //    iPressoContact = new Marketing_Campaign__c();
        //    iPressoContact.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT);
        //    iPressoContact.Contact_from_iPresso__c = contactToInsert.Id;
        //    iPressoContact.Lead_Email__c = parsedJson.email;
        //    iPressoContact.iPressoId__c = parsedJson.ipresso_contact_id;
        //}

        //try {
        //    //upsert iPressoContact;
        //}
        //catch (Exception e) {
        //    ErrorLogger.log(e);
        //    res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
        //    res.statusCode = 400;
        //    return;
        //}

        /* Update iPresso Contact with proper iPresso Id */
        List<Marketing_Campaign__c> iPressoToUpdate = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
                AND Lead_Email__c = :parsedJson.email
                AND iPressoId__c = null
                AND Contact_from_iPresso__c = :contactToInsert.Id
        ];

        if (iPressoToUpdate.size() > 0) {
            Marketing_Campaign__c iPressoContact = iPressoToUpdate.get(0);
            iPressoContact.iPressoId__c = parsedJson.ipresso_contact_id;

            try {
                update iPressoContact;
            }
            catch (Exception e) {
                ErrorLogger.log(e);
            }
        }

        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            CommonUtility.preventCreatingIPressoContact = true;
            ContactMergeManager.isVerificationActionTransaction = true;
            contactToInsert.Confirmed_Contact__c = true;
            CommonUtility.skipConsentsCreation = true;
            update contactToInsert;
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;
            return;
        }

        IntegrationError.Success_JSON errObj = new IntegrationError.Success_JSON();
        errObj.status_code = 200;
        errObj.message = 'success';

        res.responseBody = Blob.valueOf(JSON.serialize(errObj));
        res.statusCode = 200;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static void validateJSON(IntegrationIPressoAddContact.IPressoContact_JSON parsedJson) {
        if (parsedJson.first_name == null) {
            throw new InvalidFieldValueException('first_name');
        }
        if (parsedJson.last_name == null) {
            throw new InvalidFieldValueException('last_name');
        }
        if (parsedJson.email == null && parsedJson.mobile == null) {
            throw new InvalidFieldValueException('email or mobile');
        }
        if (parsedJson.consents == null) {
            throw new InvalidFieldValueException('consents');
        }
        else {
            for (IntegrationIPressoAddContact.MarketingConsents_JSON consent : parsedJson.consents) {
                if (consent.consent_id == null) {
                    throw new InvalidFieldValueException('consent_id');
                }
                if (consent.consent_value == null) {
                    throw new InvalidFieldValueException('consent_value');
                }
            }
        }
    }

    //public static String parseDepartmentForIpresso(String department) {
    //    Map<String, String> entityList = new Map<String, String> {
    //       'gdansk' => RecordVals.WSB_NAME_GDA,
    //       'gdynia' => RecordVals.WSB_NAME_GDY,
    //       'poznan' => RecordVals.WSB_NAME_POZ,
    //       'torun' => RecordVals.WSB_NAME_TOR,
    //       'wroclaw' => RecordVals.WSB_NAME_WRO,
    //       'bydgoszcz' => RecordVals.WSB_NAME_BYD,
    //       'opole' => RecordVals.WSB_NAME_OPO,
    //       'szczecin' => RecordVals.WSB_NAME_SZC,
    //       'chorzow' => RecordVals.WSB_NAME_CHO
    //    };

    //    return entityList.get(department);
    //}

    global static String listToString(String previousVaulues, List<String> listToConvert) {
        if (previousVaulues != null && previousVaulues != '') {
            //previousVaulues = previousVaulues.replaceAll(' ', ' ').replaceAll('WSB', 'WSB ');
            List<String> previousValuesList = previousVaulues.split(', ');

            Set<String> newValues = new Set<String>();
            newValues.addAll(previousValuesList);
            if (listToConvert != null) {
                newValues.addAll(listToConvert);
            }

            newValues.remove(null);
            listToConvert = new List<String>();
            listToConvert.addAll(newValues);
        }

        String result = '';
        if (listToConvert != null) {
            for (Integer i = 0; i < listToConvert.size(); i++) {
                if (listToConvert.get(i) != null && listToConvert.get(i) != '') {
                    if (i == (listToConvert.size() - 1)) {
                        result += (listToConvert.get(i));
                    }
                    else {
                        result += (listToConvert.get(i)) + ', ';
                    }
                }
            }
        }

        return result;
    }

    global static String prepareTags(String previousVaulues, List<String> listToConvert) {
        if (previousVaulues != null && previousVaulues != '') {
            //previousVaulues = previousVaulues.replaceAll(' ', ' ').replaceAll('WSB', 'WSB ');
            List<String> previousValuesList = previousVaulues.split('; ');

            Set<String> newValues = new Set<String>();
            newValues.addAll(previousValuesList);
            if (listToConvert != null) {
                newValues.addAll(listToConvert);
            }

            newValues.remove(null);
            newValues.remove('');
            listToConvert = new List<String>();
            listToConvert.addAll(newValues);
        }

        String result = '';
        if (listToConvert != null) {
            for (Integer i = 0; i < listToConvert.size(); i++) {
                if (listToConvert.get(i) != null && listToConvert.get(i) != '') {
                    if (i == (listToConvert.size() - 1)) {
                        result += (listToConvert.get(i));
                    }
                    else {
                        result += (listToConvert.get(i)) + '; ';
                    }
                }
            }
        }

        return result;
    }

    global static String listToStringFromPicklist(Schema.SObjectField field, String previousVaulues, List<String> listToConvert) {
        if (previousVaulues != null && previousVaulues != '') {
            List<String> previousValuesList = previousVaulues.split(';');

            Set<String> newValues = new Set<String>();
            newValues.addAll(previousValuesList);
            if (listToConvert != null) {
                newValues.addAll(listToConvert);
            }

            listToConvert = new List<String>();
            listToConvert.addAll(newValues);
        }

        String result = '';
        if (listToConvert != null) {
            for (Integer i = 0; i < listToConvert.size(); i++) {
                if (listToConvert.get(i) != null && listToConvert.get(i) != '') {
                    if (i == (listToConvert.size() - 1)) {
                        result += CommonUtility.getProperPicklistLabel(field, listToConvert.get(i));
                    }
                    else {
                        result += CommonUtility.getProperPicklistLabel(field, listToConvert.get(i)) + '; ';
                    }
                }
            }
        }

        return result;
    }

    private static IPressoContact_JSON parse(String jsonBody) {
        return (IPressoContact_JSON) System.JSON.deserialize(jsonBody, IPressoContact_JSON.class);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- MODEL CLASSES ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class IPressoContact_JSON {
        public String ipresso_contact_id;
        public String first_name;
        public String last_name;
        public String email;
        public String mobile;
        public String position;
        public String country;
        public String state;
        public String city;
        public List<String> tags;
        public String postal_code;
        public String street;
        public List<String> department;
        public List<String> area_of_interest;
        public String a_level_exam_year;
        public String degree;
        public List<MarketingConsents_JSON> consents;
    }

    public class MarketingConsents_JSON {
        public String consent_id;
        public Boolean consent_value;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- EXCEPTION CLASSES ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class InvalidFieldValueException extends Exception {}
}