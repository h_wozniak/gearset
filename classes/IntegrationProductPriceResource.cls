/**
*   @author         Wojciech Słodziak
*   @description    This class is used to calculate and return Product Price for Studies.
**/

@RestResource(urlMapping = '/product-price')
global without sharing class IntegrationProductPriceResource {

    @HttpPost
    global static void getProductPrice() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        IntegrationProductPriceHelper.ProductPriceRequest_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                res.responseBody = Blob.valueOf(IntegrationProductPriceHelper.getProductPriceResponseJSON(parsedJson));
                res.statusCode = 200;
            }
            catch (IntegrationProductPriceHelper.InvalidFieldValueException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(800, e.getMessage());
            }
            catch (IntegrationProductPriceHelper.ProductNotFoundException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(801);
            }
            catch (IntegrationProductPriceHelper.PriceBookNotFoundException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(802);
            }
            catch (IntegrationProductPriceHelper.TuitionSystemUnavailableException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(803);
            }
            catch (IntegrationProductPriceHelper.InstallmentVariantUndefinedException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(804);
            }
            catch (IntegrationProductPriceHelper.CodeDiscountNotFoundException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(805);
            }
            catch (IntegrationProductPriceHelper.CompanyWithGivenNIPNotFoundException e) {
                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(806);
            }
            catch (Exception e) {
                ErrorLogger.logAndMsg(e, CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationProductPriceResource.class.getName() + '\n' + jsonBody);
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }
        }
    }
    
    private static IntegrationProductPriceHelper.ProductPriceRequest_JSON parse(String jsonBody) {
        return (IntegrationProductPriceHelper.ProductPriceRequest_JSON) System.JSON.deserialize(jsonBody, IntegrationProductPriceHelper.ProductPriceRequest_JSON.class);
    }

}