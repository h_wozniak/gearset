public with sharing class LEX_Change_Study_OfferController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String forbiddenStatus(String recordId) {
		Set<String> forbiddenStatuses = new Set<String> {
            'Unconfirmed',
            'Payments in KS',
            'Resignation'
		};

		Enrollment__c enr = [SELECT Id, Status__c, Unenrolled_Status__c, Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];
		if (enr.Unenrolled__c) {
			if (enr.Status__c == 'Resignation') return 'TRUE';
			return !forbiddenStatuses.contains(enr.Unenrolled_Status__c) ? 'TRUE' : 'FALSE';
		}
		else {
			return !forbiddenStatuses.contains(enr.Status__c) ? 'TRUE' : 'FALSE';
		}
	}

	@AuraEnabled
	public static String isEnrollmentAnnexed(String recordId) {
		return LEXStudyUtilities.checkWhetherEnrollmentIsAnnexed(recordId) ? 'TRUE' : 'FALSE';
	}
	
	@AuraEnabled
	public static String wasIsUnenrolled(String recordId) {
		Enrollment__c enr = [SELECT Id, Unenrolled__c, WasIs_Unenrolled__c FROM Enrollment__c WHERE Id = :recordId];
		return (!enr.Unenrolled__c && enr.WasIs_Unenrolled__c) ? 'TRUE' : 'FALSE';
	}
}