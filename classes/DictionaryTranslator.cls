/**
* @author       Wojciech Słodziak
* @description  Class for translating picklist values for selected Language
**/

public without sharing class DictionaryTranslator {


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ HELPER PAGE CONTROLLS ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public String langCode { get; set; }
    public String sObjFieldName { get; set; }
    public String customLabel { get; set; }

    public DictionaryTranslator() {
        langCode = ApexPages.currentPage().getParameters().get('langCode');
        sObjFieldName = ApexPages.currentPage().getParameters().get('sObjFieldName');
        customLabel = ApexPages.currentPage().getParameters().get('customLabel');
    }

    public String getTranslationsJSON() {
        List<String> objAndFieldNames = sObjFieldName.split('\\.');

        Schema.SObjectField field;
        try {
            field = CommonUtility.getSObjectFieldByName(objAndFieldNames[0], objAndFieldNames[1]);
        } catch(Exception e) {
            ErrorLogger.log(e);
        }

        if (field != null) {
            return JSON.serialize(CommonUtility.getPicklistLabelMap(field));
        }

        return null;
    }

    public String getTranslationsJSONField() {
        return customLabel;
    }




    /* ------------------------------------------------------------------------------------------------ */  
    /* ------------------------------------- CALLING METHOD ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static Map<String, String> getTranslatedPicklistValues(String langCode, Schema.SObjectField sObjField) {
        String pageUrl = '/apex/DictionaryTranslator?langCode=' + langCode + '&sObjFieldName=' + 
                        + CommonUtility.getSObjectForField(((Object) sObjField).hashCode()).getDescribe().getName() + '.' 
                        + sObjField.getDescribe().getName();

        PageReference page = new PageReference(pageUrl);
        String html = Test.isRunningTest()? ('<json>{}</json>') : page.getContent().toString();

        String complementData = html.substringBetween('<json>', '</json>');

        if (complementData != null) {
            String jsonString = unescapeRussianChars(CommonUtility.unescapePolishChars(complementData), langCode);
            return (Map<String, String>) JSON.deserialize(jsonString, Map<String, String>.class);
        }

        return new Map<String, String>();
    }

    public static String getTranslatedPicklistValues(String langCode, String customLabel) {
        String pageUrl = '/apex/DictionaryTranslatorLabels?langCode=' + langCode + '&customLabel=' + customLabel;

        PageReference page = new PageReference(pageUrl);
        String html = Test.isRunningTest()? ('<json>{}</json>') : page.getContent().toString();

        String complementData = html.substringBetween('<json>', '</json>');

        if (complementData != null) {
            return unescapeRussianChars(CommonUtility.unescapePolishChars(complementData), langCode);
        }

        return '';
    }

    /* Sebastian Łasisz */
    // Get raw value in picklist from translated value
    public static String getKeyForTranslatedValue(String translatedValue, String langCode, Schema.SObjectField sObjField) {
        Map<String, String> picklistValuesMap = DictionaryTranslator.getTranslatedPicklistValues(langCode, sObjField);
        for (String value : picklistValuesMap.keySet()) {
            if (translatedValue == picklistValuesMap.get(value)) {
                return value;
            }
        }

        return null;
    }

    public static String unescapeRussianChars(String text, String lanCode) {
        if (lanCode == 'RU') {
            for (String value : russianHTMLCharactersToCirlica.keySet()) {
                if (value != null && russianHTMLCharactersToCirlica.get(value) != null) {
                    text = text.replaceAll(value, russianHTMLCharactersToCirlica.get(value));
                }
            }
        }

        return text;
    }

    public static Map<String, String> russianHTMLCharactersToCirlica = new Map<String, String> {
        '&#1040;' => 'А',
        '&#1072;' => 'а',
        '&#1041;' => 'Б',
        '&#1073;' => 'б',
        '&#1042;' => 'В',
        '&#1074;' => 'в',
        '&#1043;' => 'Г',
        '&#1075;' => 'г',
        '&#1044;' => 'Д',
        '&#1076;' => 'д',
        '&#1045;' => 'Е',
        '&#1077;' => 'е',
        '&#1046;' => 'Ж',
        '&#1078;' => 'ж',
        '&#1047;' => 'З',
        '&#1079;' => 'з',
        '&#1048;' => 'И',
        '&#1080;' => 'и',
        '&#1049;' => 'Й',
        '&#1081;' => 'й',
        '&#1050;' => 'К',
        '&#1082;' => 'к',
        '&#1051;' => 'Л',
        '&#1083;' => 'л',
        '&#1052;' => 'М',
        '&#1084;' => 'м',
        '&#1053;' => 'Н',
        '&#1085;' => 'н',
        '&#1054;' => 'О',
        '&#1086;' => 'о',
        '&#1055;' => 'П',
        '&#1087;' => 'п',
        '&#1056;' => 'Р',
        '&#1088;' => 'р',
        '&#1057;' => 'С',
        '&#1089;' => 'с',
        '&#1058;' => 'Т',
        '&#1090;' => 'т',
        '&#1059;' => 'У',
        '&#1091;' => 'у',
        '&#1060;' => 'Ф',
        '&#1092;' => 'ф',
        '&#1061;' => 'Х',
        '&#1093;' => 'х',
        '&#1062;' => 'Ц',
        '&#1094;' => 'ц',
        '&#1063;' => 'Ч',
        '&#1095;' => 'ч',
        '&#1064;' => 'Ш',
        '&#1096;' => 'ш',
        '&#1065;' => 'Щ',
        '&#1097;' => 'щ',
        '&#1066;' => 'Ъ',
        '&#1098;' => 'ъ',
        '&#1067;' => 'Ы',
        '&#1099;' => 'ы',
        '&#1068;' => 'Ь',
        '&#1100;' => 'ь',
        '&#1069;' => 'Э',
        '&#1101;' => 'э',
        '&#1070;' => 'Ю',
        '&#1102;' => 'ю',
        '&#1071;' => 'Я',
        '&#1103;' => 'я'
    };
}