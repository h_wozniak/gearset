public without sharing class TH_Account extends TriggerHandler.DelegateBase {

    List<Account> accsToCheckDuplicates;
    List<Account> acceptedConsentsToSetMC3;
    List<Account> enableCheckboxValueBasedOnAdoValues;
    List<Account> disableCheckboxValueBasedOnAdoValues;
    List<Account> clearEntitiesOnCheckboxReset;

    List<IPressoManager.EmailRemovalWrapper> emailValidationWrapper;
    List<IPressoManager.EmailRemovalWrapper> emailValidationWrapperAutomatic;
    List<IPressoManager.EmailRemovalWrapper> wrapperToAddiPressoContacts;
    List<IPressoManager.EmailRemovalWrapper> emailRemovalWrappers;

    Set<Id> accIdsWithUpdatedBillingCity;
    Set<Id> accIdsWithUpdatedContactData;
    Set<Id> deletedRecordsToDeduplicateOthers;
    Set<Id> accountIdsToDetermineStudentNumber;

    Map<Account, Set<String>> deniedMC3ConsentsToDenyOther;

    public override void prepareBefore() {
        accsToCheckDuplicates = new List<Account>();
        acceptedConsentsToSetMC3 = new List<Account>();
        enableCheckboxValueBasedOnAdoValues = new List<Account>();
        disableCheckboxValueBasedOnAdoValues = new List<Account>();
        clearEntitiesOnCheckboxReset = new List<Account>();
        emailValidationWrapper = new List<IPressoManager.EmailRemovalWrapper>();
        deletedRecordsToDeduplicateOthers = new Set<Id>();
        deniedMC3ConsentsToDenyOther = new Map<Account, Set<String>>();
    }

    public override void prepareAfter() {
        wrapperToAddiPressoContacts = new List<IPressoManager.EmailRemovalWrapper>();
        emailValidationWrapperAutomatic = new List<IPressoManager.EmailRemovalWrapper>();
        emailRemovalWrappers = new List<IPressoManager.EmailRemovalWrapper>();
        accIdsWithUpdatedBillingCity = new Set<Id>();
        accIdsWithUpdatedContactData = new Set<Id>();
        accountIdsToDetermineStudentNumber = new Set<Id>();
    }

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Account aNew = (Account) obj;

            //Author: Wojciech Słodziak, Desc: check duplicate Accounts by NIP
            if (String.isNotBlank(aNew.NIP__c) && !aNew.Name.contains(CommonUtility.INDIVIDUAL_CONTACT_ACCOUNT)) {
                accsToCheckDuplicates.add(aNew);
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        for (Id key : o.keySet()) {
            Account aNew = (Account) o.get(key);
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        for (Id key : old.keySet()) {
            Account aOld = (Account) old.get(key);
            Account aNew = (Account) o.get(key);

            if (!Test.isRunningTest()) {
                if (aOld.OwnerId != aNew.OwnerId) {
                    continue;
                } else {
                    if (!CommonUtility.isAdminUser()
                            && (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                            || aNew.RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_TECHNICAL))) {
                        aNew.addError(Label.msg_error_cantEditAccount);
                    }
                }
            }
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Account> oldOAccounts = (Map<Id, Account>) old;
        Map<Id, Account> newOAccounts = (Map<Id, Account>) o;
        for (Id key : newOAccounts.keySet()) {
            Account aOld = oldOAccounts.get(key);
            Account aNew = newOAccounts.get(key);

            /* Mateusz Pruszyński */
            // If Billing City is updated, all related training schedules' (offer__c object) University_City_in_Vocative__c has to be updated as well
            if (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                    && aOld.BillingCity != aNew.BillingCity && String.isNotBlank(aNew.BillingCity)) {
                accIdsWithUpdatedBillingCity.add(aNew.Id);
            }

            /* Wojciech Słodziak */
            // update University Contact Data on schedules and Enrollments for that University
            if (aNew.RecordTypeId == CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)
                    && aNew.Contact_Data_for_Trainings__c != aOld.Contact_Data_for_Trainings__c) {
                accIdsWithUpdatedContactData.add(aNew.Id);
            }
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Account> oldAccounts = (Map<Id, Account>) old;
        for (Id key : oldAccounts.keySet()) {
            Account aOld = oldAccounts.get(key);
        }
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Account> oldAccounts = (Map<Id, Account>) old;
        for (Id key : oldAccounts.keySet()) {
            Account aOld = oldAccounts.get(key);
        }
    }

    public override void finish() {

        /* Wojciech Słodziak */
        // check duplicate Accounts by NIP
        if (accsToCheckDuplicates != null && !accsToCheckDuplicates.isEmpty()) {
            AccountManager.checkDuplicateNIP(accsToCheckDuplicates);
        }

        /* Mateusz Pruszyński, edit: Wojciech Słodziak (by batch) */
        // if Billing City is updated, all related training schedules' (offer__c object) University_City_in_Vocative__c has to be updated as well
        if (accIdsWithUpdatedBillingCity != null && !accIdsWithUpdatedBillingCity.isEmpty()) {
            Database.executeBatch(new ScheduleCityInVocativeUpdateBatch(accIdsWithUpdatedBillingCity), 2000);
        }

        /* Wojciech Słodziak */
        // update University Contact Data on schedules and Enrollments for that University
        if (accIdsWithUpdatedContactData != null && !accIdsWithUpdatedContactData.isEmpty()) {
            Database.executeBatch(new ScheduleAccountDataUpdateBatch(accIdsWithUpdatedContactData), 2000);
            Database.executeBatch(new EnrollmentAccountDataUpdateBatch(accIdsWithUpdatedContactData), 2000);
        }

        /* Sebastian Łasisz, Radosław Urbański */
        // Check whether added email is already added to another contact. If not add new iPresso Contact
        if (wrapperToAddiPressoContacts != null && !wrapperToAddiPressoContacts.isEmpty()) {
            IPressoManager.addNewIPressoContacts(wrapperToAddiPressoContacts);
        }

        /* Sebastian Łasisz, Radosław Urbański */
        // Remove iPresso Contact on Email Deletion
        if (emailRemovalWrappers != null && !emailRemovalWrappers.isEmpty()) {
            IPressoManager.removeIPressoContactOnEmailDeletion(emailRemovalWrappers);
        }

        /* Sebastian Łasisz */
        // recalculate student number on existing study enrollments (highest value potentially could be changed due to merging)
        if (accountIdsToDetermineStudentNumber != null && !accountIdsToDetermineStudentNumber.isEmpty()) {
            EnrollmentManager_Studies.rewriteStudentNumberOnEnrollment(accountIdsToDetermineStudentNumber);
        }

        /* Wojciech Słodziak, Radosław Urbański */
        // deduplicate other records that might have been pointing to deleted records (KEEP after EMAIL and PHONE updates)
        if (deletedRecordsToDeduplicateOthers != null && !deletedRecordsToDeduplicateOthers.isEmpty()) {
            DuplicateManager.deduplicateOtherRecords(null);
        }
    }
}