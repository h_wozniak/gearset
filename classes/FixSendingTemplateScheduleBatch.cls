/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends failed payment templates
**/

global class FixSendingTemplateScheduleBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

	global FixSendingTemplateScheduleBatch() { }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
        	SELECT Id
            FROM Offer__c
            WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK)
            OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_PG)
            OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_PRICE_BOOK_MBA))
            AND Synchronization_Status__c = :CommonUtility.SYNCSTATUS_FAILED
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Offer__c> priceBooks) { 
		Set<Id> priceBookIds = new Set<Id>();
		for (Offer__c priceBook : priceBooks) {
			priceBookIds.add(priceBook.Id);
		}

        IntegrationPaymentTemplateSender.sendPaymentTemplatesSync(priceBookIds);
	}

	global void finish(Database.BatchableContext BC) { }
}