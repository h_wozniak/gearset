/**
*   @author         Sebastian Łasisz
*   @description    Class used for dispalying errors on Import - mostly used in try catch
**/

public without sharing class ImportError {

    public static ApexPages.Message getError(String fileNum, Integer lineNum, Integer colNum, String errorLabel, String line) {
        String result = '';
        if (fileNum != null) {
            result += '[' + Label.title_File + '_' + fileNum + '] ';
        }
        result += '[' + Label.title_ImportRow + ' ' + lineNum + ', ' + Label.title_ImportColumn + ' ' + colNum + '] ' + errorLabel + ' ' + bold(line);
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, result);
        
        return myMsg;
    }

    public static ApexPages.Message getError(String fileNum, Integer lineNum, String errorLabel, String line) {
        String result = '';
        if (fileNum != null) {
            result += '[' + Label.title_File + '_' + fileNum + '] ';
        }
        result += '[' + Label.title_ImportRow + ' ' + lineNum + '] ' + errorLabel + ' ' + bold(line);
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, result);
        
        return myMsg;
    }
    
    private static String bold(String text) {
        return '<b>' + text + '</b>';
    }
}