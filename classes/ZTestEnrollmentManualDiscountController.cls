@isTest
private class ZTestEnrollmentManualDiscountController {

    private static Map<Integer, sObject> prepareDataForTest(Boolean pbActive, Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5
        ), true));

        retMap.put(1, ZDataTestUtility.createCandidateContact(null, true));

        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = pbActive
        ), true));

        configPriceBook(retMap.get(2).Id, 10.0);

        Test.startTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(
                new Enrollment__c(
                        Candidate_Student__c = retMap.get(1).Id,
                        Course_or_Specialty_Offer__c = retMap.get(0).Id,
                        Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                        VerificationDate__c = System.Today(),
                        Installments_per_Year__c = '2'),
                true));

        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }



    @isTest static void testEnrollmentManualDiscountController() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Map<Integer, sObject> dataMap = prepareDataForTest(true, 100);

        List<Payment__c> payments = [SELECT Id FROM Payment__c];
        System.assert(payments.size() > 0);

        ApexPages.CurrentPage().getParameters().put('enrollmentId', dataMap.get(3).Id);
        Test.stopTest();
        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();

        System.assertEquals(emdc.newDiscount.Enrollment__c, dataMap.get(3).Id);

        try {
            emdc.save();
        }
        catch(Exception e) {
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            Boolean messageFound = false;
            Integer counter = 0;
            for(ApexPages.Message message : pageMessages) {
                if(message.getSummary() != ' ') {
                    messageFound = true;
                }
                counter ++;
            }
            System.assert(messageFound);
        }

        emdc.back();
    }

    @isTest static void testVISDiscountApplication() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Map<Integer, sObject> dataMap = prepareDataForTest(true, 100);

        ApexPages.CurrentPage().getParameters().put('enrollmentId', dataMap.get(3).Id);

        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();
        Test.stopTest();

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        emdc.selectedVIS = '25';
        emdc.newDiscount.Name = 'DiscountX';
        emdc.newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_VIS;
        emdc.newDiscount.Discount_Value__c = 25;
        emdc.newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
        emdc.newDiscount.Active__c = true;
        emdc.save();

        Enrollment__c studyEnr = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Discount__c discount = [
                SELECT Id, Discount_Value__c
                FROM Discount__c
                WHERE Id =: emdc.newDiscount.Id
        ];

        System.assert(valBefore * ((100 - discount.Discount_Value__c)/100) < studyEnr.Value_after_Discount__c);
    }

    @isTest static void testPararelDiscount() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Map<Integer, sObject> dataMap = prepareDataForTest(true, 100);

        ApexPages.CurrentPage().getParameters().put('enrollmentId', dataMap.get(3).Id);

        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();
        Test.stopTest();

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        Enrollment__c studyEnrollment2 = new Enrollment__c();
        studyEnrollment2.Candidate_Student__c = ZDataTestUtility.createCandidateContact(null, true).Id;
        insert studyEnrollment2;

        emdc.selectedVIS = '25';
        emdc.newDiscount.Name = 'DiscountX';
        emdc.newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
        emdc.newDiscount.Discount_Value__c = 25;
        emdc.newDiscount.Recommended_by__c = studyEnrollment2.Id;
        emdc.newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
        emdc.newDiscount.Active__c = true;
        emdc.save();
    }

    @isTest static void testSecondCourseDiscountApplication() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Map<Integer, sObject> dataMap = prepareDataForTest(true, 100);

        ApexPages.CurrentPage().getParameters().put('enrollmentId', dataMap.get(3).Id);
        Test.stopTest();

        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];
        System.debug(studyEnrBefore);

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        emdc.newDiscount.Name = 'DiscountX';
        emdc.newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE;

        emdc.fillDiscountInfo();

        emdc.save();

        Enrollment__c studyEnr = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Discount__c discount = [
                SELECT Id, Discount_Value__c
                FROM Discount__c
                WHERE Id = :emdc.newDiscount.Id
        ];

        System.assert(valBefore > studyEnr.Value_after_Discount__c);
    }


    @isTest static void testRecommendationDiscountApplication_same() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Map<Integer, sObject> dataMap = prepareDataForTest(true, 100);

        ApexPages.CurrentPage().getParameters().put('enrollmentId', dataMap.get(3).Id);
        Test.stopTest();

        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();
        Enrollment__c studyEnr1Before = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Enrollment__c studyEnr2Before = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        emdc.newDiscount.Name = 'DiscountX';
        emdc.newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
        emdc.newDiscount.Recommended_by__c = dataMap.get(3).Id;

        emdc.fillDiscountInfo();


        emdc.save();

        Enrollment__c studyEnr1 = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Enrollment__c studyEnr2 = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        System.assert(studyEnr1Before.Value_after_Discount__c == studyEnr1.Value_after_Discount__c);
    }

    @isTest static void testErrorMessage() {
        PageReference pageRef = Page.EnrollmentManualDiscount;
        Test.setCurrentPageReference(pageRef);

        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                Degree__c = CommonUtility.OFFER_DEGREE_II,
                University__c = ZDataTestUtility.createUniversity(new Account(
                        Name = RecordVals.WSB_NAME_WRO
                ), true).Id
        ), true);

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = courseOffer.Id,
                Installments_per_Year__c = String.valueOf(10),
                Unenrolled__c = true
        ), true);

        ApexPages.currentPage().getParameters().put('enrollmentId', studyEnr.Id);

        EnrollmentManualDiscountController emdc = new EnrollmentManualDiscountController();
        System.assertEquals(CommonUtility.DISCOUNT_TYPEST_DISCRETIONARY, emdc.newDiscount.Discount_Type_Studies__c);

        emdc.newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_CHANCELLOR;

        emdc.fillDiscountInfo();

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        Boolean messageFound = false;
        for (ApexPages.Message msg : pageMessages) {
            if (msg.getSummary().contains(Label.msg_error_OnlyUnenrolleDiscountCanBeAdded)) {
                messageFound = true;
            }
        }

        System.assert(!messageFound);
    }
}