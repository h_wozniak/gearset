public with sharing class LEX_DeleteMassRecords {

    @AuraEnabled
    public static String deleteMassRecords(List<Id> recordIds) {
        Set<Id> recordIdsSet = new Set<Id>();
        recordIdsSet.addAll(recordIds);

        try {
            Database.delete(recordIds);
        }
        catch (Exception e) {
            return CommonUtility.trimErrorMessage(e.getMessage());
        }

        return null;

//        List<Database.DeleteResult> results = Database.delete(recordIds, false);
//
//        Set<String> errors = new Set<String>();
//        for (Database.DeleteResult result : results) {
//            if (!result.isSuccess()) {
//                for(Database.Error err : result.getErrors()) {
//                    errors.add(err.getMessage());
//                }
//            }
//        }
//
//        if (errors != null && !errors.isEmpty()) {
//            return string.join(new List<String>( errors ), ' ');
//        }
//        else {
//            return null;
//        }
    }

    @AuraEnabled
    public static String getEnrollmentStatus(Id studyEnrId) {
        Enrollment__c studyEnrollment = [
                SELECT Id, Status__c
                FROM Enrollment__c
                WHERE Id = :studyEnrId
        ];

        return studyEnrollment.Status__c;
    }

    @AuraEnabled
    public static Boolean hasDeleteAccess(List<Id> recordIds) {
        Set<Id> recordIdsSet = new Set<Id>();
        recordIdsSet.addAll(recordIds);

        List<UserRecordAccess> uraList = [
                SELECT RecordId, HasDeleteAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId IN :recordIds
        ];

        for (UserRecordAccess ura : uraList) {
            if (!ura.HasDeleteAccess) {
                return false;
            }
        }

        return true;
    }
}