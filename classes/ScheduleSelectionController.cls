global with sharing class ScheduleSelectionController {

    /**
    * @author       Wojciech Słodziak
    * @description  This class is a controller for ScheduleSelection.page, which allows schuedule finding and selection
    **/

    public String selectedUniversity { get; set; }
    public String allUniversitiesJSON { get; set; }
    public String trainingType { get; set; }
    public String candidateId { get; set; }
    public String companyId { get; set; }

    public String typeOpen { get{ return CommonUtility.OFFER_TYPE_OPEN;}}
    public String typeClosed { get{ return CommonUtility.OFFER_TYPE_CLOSED;}}

    public String currencySymbol { get; set; }

    public String retURL { get; set; }
    public String retURLtoBack { get; set; }

    public Boolean isLightning {get; set; }

    public ScheduleSelectionController() {
        isLightning = (UserInfo.getUiTheme().contains('Theme4'));
        String enrollmentRT = Apexpages.currentPage().getParameters().get('enrollmentRT');
        trainingType = getTrainingType(enrollmentRT);
        
        //candidate
        candidateId = Apexpages.currentPage().getParameters().get('candidateId');
        //company
        companyId = Apexpages.currentPage().getParameters().get('companyId');

        retURLtoBack = Apexpages.currentPage().getParameters().get('retURL');

        List<Account> universityList = getUniversities();
        allUniversitiesJSON = JSON.serialize(universityList);
        if (universityList.size() > 0) {
            selectedUniversity = universityList[0].Id;
        }

        currencySymbol = CommonUtility.getCurrencySymbolFromIso(CommonUtility.CURRENCY_CODE_PLN).unescapeHtml4();
        retURL = 'apex/ScheduleSelection?enrollmentRT=' + enrollmentRT;
    }

    private List<Account> getUniversities() {
        return [SELECT Id, Name FROM Account WHERE RecordTypeId = : CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_UNIVERSITY)];
    }

    private String getTrainingType(String enrollmentRT) {
        if (enrollmentRT == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)) {
            return CommonUtility.OFFER_TYPE_OPEN;
        } else if (enrollmentRT == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)) {
            return CommonUtility.OFFER_TYPE_CLOSED;
        }
        return null;
    }

    public PageReference back() { 
        if (retURLtoBack != null) {
            return new PageReference(retURLtoBack);
        }
        return null;
    }

    @RemoteAction @ReadOnly
    global static List<Offer__c> getTrainingsForUniversity(Id universityId, String trainingType) {
        if (universityId != null) {
            return [SELECT Id, Name, Trade_Name__c, toLabel(Field__c), Scope_of_Training__c, University_from_Training__c, University_from_Training__r.Name, toLabel(Type__c) 
                    FROM Offer__c 
                    WHERE University_from_Training__c = :universityId AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                    AND Type__c =: trainingType ORDER BY Name];
        } else {
            return [SELECT Id, Name, Trade_Name__c, toLabel(Field__c), Scope_of_Training__c, University_from_Training__c, University_from_Training__r.Name, toLabel(Type__c) 
                    FROM Offer__c 
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                    AND Type__c =: trainingType ORDER BY Name];
        }
    }

    @RemoteAction @ReadOnly
    global static List<Offer__c> getSchedulesForTraining(Id trainingId) {
        return [SELECT Id, Name, Training_Offer_from_Schedule__c, Training_Place__c, Price_per_Person__c, Offer_Value__c, Valid_From__c, Valid_To__c, Start_Time__c, 
                Number_of_Seats__c, Active__c 
                FROM Offer__c 
                WHERE Training_Offer_from_Schedule__c = :trainingId AND Active__c = true ORDER BY Valid_From__c];
    }
}