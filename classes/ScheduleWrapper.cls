global with sharing class ScheduleWrapper {
	@TestVisible
	List<Schedule> schedules;
	
	public ScheduleWrapper() {
		this.schedules = new List<Schedule>();
	}
	
	public class Schedule {
		public String name;
		public String trainingCode;
	    public Decimal numberOfSeats;
	    public Decimal pricePerPerson;
	    public Decimal pricePerPersonGraduate;
	    public Date startDate;
	    public String startTime;
	    public Date endDate;
	    public String trainingPlace;
	    public String street;
	    public String town;
	    public Boolean launched;

	    public Schedule(Offer__c offer) {
	    	this.trainingCode = offer.Name;
	    	this.name = offer.Training_Offer_from_Schedule__r.Trade_Name__c;
	    	this.numberOfSeats = offer.Number_of_Seats__c;
	    	this.pricePerPerson = offer.Price_per_Person__c;
	    	this.pricePerPersonGraduate = offer.Price_per_Person_Student_Graduate__c;
	    	this.startDate = offer.Valid_From__c;
	    	this.startTime = offer.Start_Time__c;
	    	this.endDate = offer.Valid_To__c;
	    	this.trainingPlace = offer.Training_Place__c;
	    	this.street = offer.Street__c;
	    	this.town = offer.City__c;
	    	this.launched = offer.Launched__c;
	    }
	}
	
	public static ScheduleWrapper serializeSchedules(List<Offer__c> offers) {
		ScheduleWrapper schwrapper =  new ScheduleWrapper();
		for (Offer__c off : offers) {
			schwrapper.schedules.add(new Schedule(off));
		}
		return schwrapper;
	}
	
}