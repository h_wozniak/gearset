/**
*   @author         Sebastian Łasisz
*   @description    Schedulable class that runs batches daily to retrieve Google Calendar Events for High School Visits
**/

/**
*   To initiate the RetrieveHighSchoolVisitsSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('RetrieveHighSchoolVisitsSchedule', '0 0 8 * * ? *', new RetrieveHighSchoolVisitsSchedule());
**/

global class RetrieveHighSchoolVisitsSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        if (!Test.isRunningTest()) {
            RetrieveHighSchoolVisitsBatch highSchoolEventsRetriever = new RetrieveHighSchoolVisitsBatch();
            Database.executebatch(highSchoolEventsRetriever, 1);
        }
        else {
            RetrieveHighSchoolVisitsBatch highSchoolEventsRetriever = new RetrieveHighSchoolVisitsBatch();
            Database.executebatch(highSchoolEventsRetriever, 200);
        }
    }
}