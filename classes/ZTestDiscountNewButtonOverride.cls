@isTest
private class ZTestDiscountNewButtonOverride {

    @isTest static void testDiscountNewButtonOverride() {
        PageReference pageRef = Page.DiscountNewButtonOverride;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController(new Discount__c());

        DiscountNewButtonOverride dnbo = new DiscountNewButtonOverride(stdController);

        System.assertNotEquals(null, dnbo.recordTypeList);

        dnbo.continueStep();
    }

}