/**
*   @author         Sebastian Łasisz
*   @description    This class is used to request scoring from iPresso.
**/

@RestResource(urlMapping = '/iPresso/update_scoring/*')
global without sharing class IntegrationIPressoScoringReceiver {

    @HttpPost
    global static void receiverScoringRequest() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        List<IntegrationIPressoScoringReceiver.AckResponse_JSON> parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
            return;
        }

        Set<String> ipressoIds = new Set<String>();
        for (AckResponse_JSON responseJSON : parsedJson) {
            ipressoIds.add(responseJSON.ipresso_contact_id);
        }

        List<Marketing_Campaign__c> ipressoContacts = [
            SELECT Id, iPressoId__c, Scoring__c, Scoring_Calculation_Date__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
            AND iPressoId__c IN :ipressoIds
        ];

        //if (ipressoContacts.size() != parsedJson.size()) {
        //    res.statusCode = 400;
        //    res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
        //    return;
        //}

        for (Marketing_Campaign__c iPressoContact : ipressoContacts) {
            for (AckResponse_JSON responseJSON : parsedJson) {
                if (responseJSON.ipresso_contact_id == iPressoContact.iPressoId__c) {
                    iPressoContact.Scoring__c = responseJSON.scoring;
                    iPressoContact.Scoring_Calculation_Date__c = System.now();
                }
            }
        }

        try {
            update ipressoContacts;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            return;
        }

        res.statusCode = 200;
    }
    
    private static List<IntegrationIPressoScoringReceiver.AckResponse_JSON> parse(String jsonBody) {
        return (List<IntegrationIPressoScoringReceiver.AckResponse_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationIPressoScoringReceiver.AckResponse_JSON>.class);
    }

    // used in communication ESB -> CRM for reading ACKs and Experia forwarded responses
    global class AckResponse_JSON {
        public String ipresso_contact_id;
        public String scoring;
    }

}