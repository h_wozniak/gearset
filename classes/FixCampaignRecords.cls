/**
 * Created by Sebastian.Lasisz on 21.05.2018.
 */

global class FixCampaignRecords implements Database.Batchable<sObject>, Database.AllowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Current_Classifier__c, External_Id__c, CreatedDate, CreatedById, CreatedBy.Name, Campaign_Details__r.Details_Enrollment_Id__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                AND External_Id__c = null
                AND CreatedDate > 2018-04-23T01:02:03Z
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Marketing_Campaign__c> campMembers) {
        Set<Id> enrIds = new Set<Id>();
        for (Marketing_Campaign__c camp : campMembers) {
            enrIds.add(camp.Campaign_Details__r.Details_Enrollment_Id__c);
        }

        Map<Id, Enrollment__c> studyEnr = new Map<Id, Enrollment__c>([
                SELECT Id, Status__c, Delivery_Deadline__c
                FROM Enrollment__c
                WHERE Id IN :enrIds
                AND Delivery_Deadline__c >= TODAY
                AND Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
        ]);

        Set<Id> campIdsToSend = new Set<Id>();

        for (Marketing_Campaign__c camp : campMembers) {
            Enrollment__c enr = studyEnr.get(camp.Campaign_Details__r.Details_Enrollment_Id__c);
            if (enr != null) {
                campIdsToSend.add(camp.id);
            }
        }

        if (!Test.isRunningTest()) {
            IntegrationFCCContactSender.sendNewFCCRecordsSync(campIdsToSend);
        }
    }

    global void finish(Database.BatchableContext BC) { }
}