@isTest
private class ZTestLEX_AddAdditionalCharge {


    @isTest static void testUserAnalysisHasEditAccess() {
        User usr = ZDataTestUtilityMethods.getUserWithProfileAnalysis();

        Id newEnrollmentId = ZDataTestUtility.createOpenTrainingEnrollment(null, true).Id;
        Id newDiscountId = ZDataTestUtility.createADHOCDiscount(null, true).Id;
        Id newOfferId = ZDataTestUtility.createTrainingOffer(null, true).Id;

        System.runAs(usr) {
            Test.startTest();

            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newEnrollmentId));
            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newDiscountId));
            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newOfferId));

            Test.stopTest();
        }
    }

    @isTest static void testUserSalesTrainingHasEditAccess() {
        User usr = ZDataTestUtilityMethods.getUserWithProfileSales();

        Id newEnrollmentId = ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(University_Name__c = 'WSB Wrocław'), true).Id;
        Id newDiscountId = ZDataTestUtility.createADHOCDiscount(null, true).Id;
        Id newOfferId = ZDataTestUtility.createTrainingOffer(null, true).Id;

        System.runAs(usr) {
            Test.startTest();

            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newEnrollmentId)); // No access, specific Role needed (Sharing Rules)
            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newDiscountId));
            System.assert(!LEX_AddAdditionalChargeController.hasEditAccess(newOfferId)); // No access, specific Role needed (Sharing Rules)

            Test.stopTest();
        }
    }

    @isTest static void test_getAdditionalChargesAsPicklistHTML() {
        CustomSettingDefault.initAdditionalCharges();

        System.assert(LEX_AddAdditionalChargeController.getAdditionalChargesAsPicklist(null).size() > 0);
    }

    @isTest static void test_addAdditionalCharge() {
        CustomSettingDefault.initAdditionalCharges();

        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        Payment__c tuitionInst = new Payment__c();
        tuitionInst.RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST);
        tuitionInst.Enrollment_from_Schedule_Installment__c = studyEnr.Id;
        tuitionInst.PriceBook_Value__c = 100;
        tuitionInst.Type__c = CommonUtility.PAYMENT_TYPE_TUITION_FEE;
        tuitionInst.Payment_Deadline__c = System.today();
        tuitionInst.Installment_Year_Calendar__c = '2016';
        insert tuitionInst;

        Test.startTest();
        String response = LEX_AddAdditionalChargeController.addAdditionalCharge(studyEnr.Id, RecordVals.CS_AC_NAME_STUDENTCARD);
        Test.stopTest();

        WebserviceUtilities.WS_Response responseJSON = (WebserviceUtilities.WS_Response) JSON.deserialize(response, WebserviceUtilities.WS_Response.class);

        System.assert(responseJSON.result);

        List<Payment__c> additionalChargeList = [
                SELECT Id, Payment_Deadline__c, Installment_Year_Calendar__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :studyEnr.Id
                AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        System.assert(!additionalChargeList.isEmpty());
    }

}