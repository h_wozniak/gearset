/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Classifiers
**/

@RestResource(urlMapping = '/classifier_create/*')
global without sharing class IntegrationClassifierCreation {

	@HttpPost
    global static void receiveClassifierInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();
        
        List<Campaign_JSON> parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            ErrorLogger.msg(jsonBody);
            ErrorLogger.log(e);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
            return ;
        }

        Set<String> crmExternalCampaignIds = new Set<String>();
        for (Campaign_JSON campaign : parsedJson) {
        	crmExternalCampaignIds.add(campaign.external_campaign_id);
        }

        List<Marketing_Campaign__c> externalCampaigns = [
            SELECT Id, Name, External_Id__c
            FROM Marketing_Campaign__c
            WHERE External_Id__c IN :crmExternalCampaignIds
        ];

        List<Marketing_Campaign__c> existingClassifiers = [
        	SELECT Id, Name, External_Campaign_from_Classifier__r.External_Id__c, Campaign_from_Classifier__c, Classifier_Name__c, Classifier_ID__c
        	FROM Marketing_Campaign__c
        	WHERE (External_Campaign_from_Classifier__r.External_Id__c IN :crmExternalCampaignIds AND External_Campaign_from_Classifier__r.External_Id__c != null)
        ];

	    if (externalCampaigns.size() <= 0) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(1100);
            return ;	    	
	    }

        Database.executeBatch(new IntegrationClassifierCreationBatch(externalCampaigns, existingClassifiers, parsedJson), 15);
        res.statusCode = 200;
        return ;
    }

    public static Marketing_Campaign__c createClassifier(List<Marketing_Campaign__c> existingClassifiers, IntegrationClassifierCreation.Classifier_JSON classifierFromJSON, IntegrationClassifierCreation.Campaign_JSON campaign, List<Marketing_Campaign__c> externalCampaigns) {
         Marketing_Campaign__c classifier = new Marketing_Campaign__c();

        for (Marketing_Campaign__c existingClassifier : existingClassifiers) {  
            if (campaign.external_campaign_id == existingClassifier.External_Campaign_from_Classifier__r.External_Id__c) {
                if (existingClassifier.Classifier_ID__c == classifierFromJSON.id) {
                    classifier.Id = existingClassifier.Id;
                }
            }
        }

        classifier.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_EXTERNAL_CLASSIFIER);

        for (Marketing_Campaign__c externalCampaign : externalCampaigns) {
            if (externalCampaign.External_Id__c == campaign.external_campaign_id) {
                classifier.External_Campaign_from_Classifier__c = externalCampaign.Id;                            
            }
        }
        classifier.Classifier_answered__c = classifierFromJSON.answered;
        classifier.Classifier_busy__c = classifierFromJSON.busy;
        classifier.Classifier_Closing__c = classifierFromJSON.is_closing;
        classifier.Classifier_Failed__c = classifierFromJSON.failed;
        classifier.Classifier_ID__c = classifierFromJSON.id;
        classifier.Classifier_mail__c = classifierFromJSON.mail;
        classifier.Classifier_Name__c = classifierFromJSON.name;
        classifier.Classifier_no_answer__c = classifierFromJSON.no_answer;
        classifier.Classifier_recall__c = classifierFromJSON.is_recall;
        classifier.Classifier_System__c = Integer.valueOf(classifierFromJSON.id) > -30 && Integer.valueOf(classifierFromJSON.id) < 30;

        return classifier;
    }
    
    private static List<Campaign_JSON> parse(String jsonBody) {
        return (List<Campaign_JSON>) System.JSON.deserialize(jsonBody, List<Campaign_JSON>.class);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    global class Campaign_JSON {
    	public String external_campaign_id;
    	public List<Classifier_JSON> classifiers;
    }

    global class Classifier_JSON {
    	public String id;
    	public String name;
    	public Boolean is_recall;
    	public Boolean is_closing;
    	public Boolean answered;
    	public Boolean failed;
    	public Boolean no_answer;
    	public Boolean busy;
    	public Boolean mail;
    }
}