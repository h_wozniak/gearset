/**
* @author       Mateusz Pruszyński
* @description
**/
public with sharing class EnrollmentStudyContactLookupWindow {

    public String searchTxt {get; set;}
    public List<Contact> contactList {get; set;}
    public Boolean newMode {get; set;}
    public Boolean editMode {get; set;}
    public Contact newContact {get; set;}
    public Contact editContact {get; set;}
    public String nameMatch {get; set;}
    public String idMatch {get; set;}
    public String param {get; set;}
    public Boolean isComplete {get; set;}
    public String companyId {get; set;}
    private String condition {get; set;}
    private String query {get; set;}
    public String degree { get; set; }
    public String candidateId { get; set; }

    public EnrollmentStudyContactLookupWindow() {
        degree = ApexPages.currentPage().getParameters().get('degree');
        candidateId = ApexPages.currentPage().getParameters().get('candidateId');

        newMode = false;
        editMode = false;
        isComplete = false;
        newContact = new Contact();

        query = 'SELECT Id, Name, University_for_sharing__c, AccountId, Birthdate, Account.Name, RecordType.Name, Email, Phone ' +
        		'FROM Contact ' +
        		'WHERE RecordTypeId != \'' + CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS) + '\'';

        try {
            contactList = Database.Query(query + ' LIMIT 100');
        } catch(Exception e) {
            ErrorLogger.log(e);
        }

        if (candidateId != null) {
            idMatch = candidateId;
            checkContact();
        }
    }

    public void searchLookup() {
        String currentQuery;
        if (!String.isBlank(searchTxt)) {
            searchTxt = String.escapeSingleQuotes(searchTxt.remove('*'));
            List<String> splitQueries = searchTxt.split(' ');
            if (splitQueries.isEmpty()) {
                splitQueries.add(searchTxt);
            }

            String whereString = '';
            for (Integer i = 0; i < splitQueries.size(); i++) {
                 whereString += ' AND (FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
            }

            currentQuery = query + whereString;
        } else {
            currentQuery = query;
        }

        try {
            contactList = Database.Query(currentQuery + ' LIMIT 100');
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
    }

    public void checkContact() {
        editContact = [SELECT Id, Foreigner__c, FirstName, LastName, Pesel__c, Birthdate, Email, Phone, MobilePhone, MailingStreet, MailingState, MailingCountry,
                       MailingCity, MailingPostalCode, Position__c, Department_Business_Area__c, A_Level__c, Gender__c, Country_of_Origin__c, OtherStreet,
                       OtherState, OtherCountry, OtherCity, OtherPostalCode, The_Same_Mailling_Address__c, School_Name__c, School__c, Nationality__c,
                       OtherCountryCode, MailingCountryCode
                       FROM Contact
                       WHERE Id = :idMatch];

        //check data completion
        if (ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, editContact, null)) {
            isComplete = true;
        } else {
            ApexPages.Message msg;
            msg = new Apexpages.Message(ApexPages.Severity.Info, Label.msg_ReqFieldsEnrollment1);

            ApexPages.addMessage(msg);
            isComplete = false;
            editMode = true;
        }
    }

    public void changeNewMode() {
        newMode = !newMode;
    }

    public void cancelEdit() {
        editMode = false;
    }

    public void updateContact() {
        //check data completion
        if (ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, editContact, null)) {
            try {
                update editContact;
                nameMatch = editContact.FirstName + ' ' + editContact.LastName;
                idMatch = editContact.Id;
                isComplete = true;
            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        } else {
            ApexPages.Message msg;
            msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_ReqFieldsEnrollment1);

            ApexPages.addMessage(msg);

            isComplete = false;
            editMode = true;
        }
    }

    public void newRecord() {
        /* Change #1425 */
        /* Sebastian Łasisz, 02.01.17 */
        /* Domyślna wartość dla pól typu kraj i dokumentu tożsamośći*/
        newContact.Country_of_Origin__c = CommonUtility.CONTACT_COUNTRY_POLAND;
        newContact.Nationality__c = CommonUtility.CONTACT_CITIZENSHIP_POLISH;
        newContact.OtherCountryCode = CommonUtility.LANGUAGE_CODE_PL;
        newContact.Identity_Document__c = CommonUtility.CONTACT_IDENTITY_DOCUMENT_CARD;
        /* End of change */
        newContact.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE);
        changeNewMode();
    }

    public void insertContactWithIPresso() {
        List<Marketing_Campaign__c> iPressoContacts = [
            SELECT Id, Contact_from_iPresso__c, Lead_Email__c
            FROM Marketing_Campaign__c
            WHERE (Lead_Email__c = :newContact.Email
            OR Lead_Email__c = :editContact.Email)
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        //check data completion
        if (ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, newContact, null)) {
            try {
                insert newContact;

                for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
                    iPressoContact.Contact_from_iPresso__c = newContact.Id;
                }

                update iPressoContacts;

                nameMatch = newContact.FirstName + ' ' + newContact.LastName;
                idMatch = newContact.Id;
                isComplete = true;

            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        } else {
            ApexPages.Message msg;
            msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_ReqFieldsEnrollment1);
            ApexPages.addMessage(msg);
            isComplete = false;
        }
    }

    public void insertContact() {
        //check data completion
        if (ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, newContact, null)) {
            try {
                CommonUtility.preventChangingEmailTwice = true;
                if (newContact.AccountId != null) {
                	insert newContact;
                	nameMatch = newContact.FirstName + ' ' + newContact.LastName;
                	idMatch = newContact.Id;
                }
				nameMatch = newContact.FirstName + ' ' + newContact.LastName;
                isComplete = true;

            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        } else {
            ApexPages.Message msg;
            msg = new Apexpages.Message(ApexPages.Severity.Error, Label.msg_ReqFieldsEnrollment1);
            ApexPages.addMessage(msg);
            isComplete = false;
        }
    }

}