@isTest
global class ZTestIntegrationIPressoScoring {

	@isTest static void test_IntegrationIPressoScoringBatch_success_withoutData() {
        CustomSettingDefault.initEsbConfig();
        CustomSettingDefault.initIPressoLastScoringRetrievalDate();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock_Success());

        Test.startTest();
        Database.executeBatch(new IntegrationIPressoScoringBatch(), 100);
        Test.stopTest();
	}

	@isTest static void test_IntegrationIPressoScoring_success_withoutData() {
        CustomSettingDefault.initEsbConfig();
        CustomSettingDefault.initIPressoLastScoringRetrievalDate();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock_Success());

        Test.startTest();
        System.schedule(IntegrationIPressoScoring.class.getName(), '0 0 6 * * ? *', new IntegrationIPressoScoring());
        Test.stopTest();
	}

	@isTest static void test_IntegrationIPressoScoring_success_withData() {
        CustomSettingDefault.initEsbConfig();
        CustomSettingDefault.initIPressoLastScoringRetrievalDate();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock_Success());

        Contact c = ZDataTestUtility.createCandidateContact(null, true);

        List<Marketing_Campaign__c> iPressoContacts = [
        	SELECT Id, Scoring__c, iPressoId__c
        	FROM Marketing_Campaign__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        for (Marketing_Campaign__c iPressoContact : iPressoContacts) {
        	iPressoContact.iPressoId__c = '5';
        }

        update iPressoContacts;

        Test.startTest();
        Database.executeBatch(new IntegrationIPressoScoringBatch(), 100);
        Test.stopTest();

        List<Marketing_Campaign__c> iPressoContactsAfterUpdate = [
        	SELECT Id, Scoring__c, iPressoId__c
        	FROM Marketing_Campaign__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        for (Marketing_Campaign__c iPressoContact : iPressoContactsAfterUpdate) {
        	System.assertEquals(iPressoContact.Scoring__c, 'string');
        }
	}



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    global class IntegrationPaymentTemplateMock_Success implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
            String retrieveScoring = esb.Import_Scoring_From_iPresso_End_Point__c;
            String esbIp = esb.Service_Url__c;

            System.assertEquals(esbIp + retrieveScoring, req.getEndpoint());
            System.assertEquals(IntegrationManager.HTTP_METHOD_POST, req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setBody('[{"ipresso_contact_id":"5","contact_scoring":"string","scoring_calculation_date":"2017-02-23 08:18:45"}]');
            res.setStatusCode(200);

            return res;
        }
    }
}