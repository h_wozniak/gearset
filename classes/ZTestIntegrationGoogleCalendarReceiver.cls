@isTest
global class ZTestIntegrationGoogleCalendarReceiver {

    @isTest static void receiveCalendarEvents() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Position__c = 'Visitator', Email = 'tamara.rubin@enxoo.com'), true);

        String body = '{"status":200,"client_id":"109832929187851981443","events":[{"event":{"attendees":[{"email":"marcin.naliwajko@enxoo.com","responseStatus":"needsAction"},{"email":"maciej.simm@enxoo.com","responseStatus":"accepted"},{"email":"tamara.rubin@enxoo.com","responseStatus":"needsAction","self":true},{"email":"monika.szlendak@enxoo.com","responseStatus":"accepted"},{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com","organizer":true,"responseStatus":"accepted"},{"displayName":"Sala Konferencyjna 3&4","email":"enxoo.com_2d39343637393736333433@resource.calendar.google.com","resource":true,"responseStatus":"accepted"},{"email":"jaroslaw.kalicki@enxoo.com","responseStatus":"accepted"},{"email":"dorota.salem@enxoo.com","responseStatus":"accepted"},{"email":"magdalena.kubis@enxoo.com","responseStatus":"accepted"},{"email":"michal.stebach@enxoo.com","optional":true,"responseStatus":"accepted"},{"email":"jacek.czernuszenko@enxoo.com","responseStatus":"needsAction"},{"email":"anna.seredyn@enxoo.com","responseStatus":"accepted"},{"email":"dominika.ozieblo@enxoo.com","responseStatus":"accepted"},{"email":"radoslaw.sulek@enxoo.com","responseStatus":"accepted"}],"created":{"value":1502112439000,"dateOnly":false,"timeZoneShift":0},"creator":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"description":"Lets meet weekly to discuss resources allocation, projects plans and issues. The main point will be short review of each project with budget, resource allocation, plan and issues. We can use \\"Project Review\\" object within Salesforce to prepare that individually for each project.","end":{"dateTime":{"value":1510677000000,"dateOnly":false,"timeZoneShift":60}},"etag":"\\"3018930917426000\\"","hangoutLink":"https://meet.google.com/dsc-uiwe-rbr","htmlLink":"https://www.google.com/calendar/event?eid=Z3N0ZHBmdWIyMThscHM0ODhkcDlocmRxdWdfMjAxNzExMTRUMTUwMDAwWiB0YW1hcmEucnViaW5AZW54b28uY29t","iCalUID":"gstdpfub218lps488dp9hrdqug@google.com","id":"gstdpfub218lps488dp9hrdqug_20171114T150000Z","kind":"calendar#event","location":"S, Sala Konferencyjna 3&4, będzie co 2 tygodnie","organizer":{"displayName":"Krzysztof Zych","email":"krzysztof.zych@enxoo.com"},"originalStartTime":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"recurringEventId":"gstdpfub218lps488dp9hrdqug","reminders":{"useDefault":true},"sequence":1,"start":{"dateTime":{"value":1510671600000,"dateOnly":false,"timeZoneShift":60}},"status":"confirmed","summary":"Project management meeting","updated":{"value":1509465458713,"dateOnly":false,"timeZoneShift":0}},"calendarId":"tamara.rubin@enxoo.com"}]}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        try {
            IntegrationGoogleCalendarEventReceiver.receiveCalendarEvents();
        } catch (Exception e) {
            System.assertEquals(e.getMessage(), '');
        }
        
        Test.stopTest();

        List<WSB_Calendar_Settings__mdt> wsbSettings = [
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                        Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ];

        System.assert(wsbSettings.size() > 1);

        List<Event> events = [
                SELECT Id, WhoId
                FROM Event
        ];

        System.assertEquals(events.size(), 1);
        System.assertEquals(events.get(0).WhoId, candidate.Id);
    }
}