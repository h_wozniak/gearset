@isTest
private class ZTestDocumentListEmailController {
    
    @isTest static void test_getDocumentList() {
        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
        Test.stopTest();
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD,
            Enrollment_from_Documents__c = studyEnr.Id
        ), true);

        DocumentListEmailController deuc = new DocumentListEmailController();
        deuc.enrollmentId = studyEnr.Id;

        List<DocumentListEmailController.Document_Wrapper> docList = deuc.getDocumentList();

        System.assert(!docList.isEmpty());
        System.assert(!docList[0].delivered);
    }
    
}