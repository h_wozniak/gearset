@isTest
private class ZTestTrainingWebservices {
    
    @isTest static void test_markEnrollmentsAsPaid_Offer_Canceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
        ), true);
        
        Test.startTest();
        String result = TrainingWebservices.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');
        
        System.assertEquals(result, 'true');
        
        List<Enrollment__c> relatedEnrollments = [
            SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c 
            WHERE Training_Offer__c = :trainingOffer.Id 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];
        
        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Last_Active_status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
            
        Test.stopTest();
    }
    
    @isTest static void test_markEnrollmentsAsPaid_Offer_NotCanceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Test.startTest();
        String result = TrainingWebservices.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');
        
        System.assertEquals(result, 'true');
        
        List<Enrollment__c> relatedEnrollments = [
            SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c 
            WHERE Training_Offer__c = :trainingOffer.Id 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];
        
        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
            
        Test.stopTest();
    }
    
    @isTest static void test_markEnrollmentsAsPaid_Offer_WithoutEnrollments() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        
        Test.startTest();
        String result = TrainingWebservices.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');
        
        System.assertEquals(result, 'no_main_enrollment');
            
        Test.stopTest();
    }
    
    @isTest static void test_markEnrollmentsAsPaid_Enrollment_Canceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
        ), true);
        
        Test.startTest();
        String result = TrainingWebservices.markEnrollmentsAsPaid(closedTraining.Id, 'Enrollment__c');
        
        System.assertEquals(result, 'true');
        
        List<Enrollment__c> relatedEnrollments = [
            SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c 
            WHERE Training_Offer__c = :trainingOffer.Id 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];
        
        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Last_Active_status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
            
        Test.stopTest();
    }
    
    @isTest static void test_markEnrollmentsAsPaid_Enrollment_NotCanceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Test.startTest();
        String result = TrainingWebservices.markEnrollmentsAsPaid(closedTraining.Id, 'Enrollment__c');
        
        System.assertEquals(result, 'true');
        
        List<Enrollment__c> relatedEnrollments = [
            SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c 
            WHERE Training_Offer__c = :trainingOffer.Id 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];
        
        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
            
        Test.stopTest();
    }
    
    @isTest static void test_markParticipantsAsAccepted() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS), true);
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        TrainingWebservices.markParticipantsAsAccepted(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [SELECT Id, Status__c FROM Enrollment__c WHERE Id IN :idArray];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT);
        }
    }
    
    @isTest static void test_hasSpecialisation_Course_Specialisation() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = univesityOffer.Id), true);
        Offer__c specialisationOffer = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = courseOffer.Id), true);
        
        Test.startTest();
        Boolean result = TrainingWebservices.hasSpecializations(courseOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, true);
    }
    
    @isTest static void test_hasSpecialisation_Specialty_noSpecialisation() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = univesityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        
        Test.startTest();
        Boolean result = TrainingWebservices.hasSpecializations(specialtyOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, false);
    }
    
    @isTest static void test_hasSpecialisation_Specialty_Specialisation() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II_PG), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = univesityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        Offer__c specialisationOffer = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = specialtyOffer.Id), true);
        
        Test.startTest();
        Boolean result = TrainingWebservices.hasSpecializations(specialtyOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, true);
    }
    
    @isTest static void test_hasSpecialisation_Specialty_Specialisation_notPG() {
        Offer__c univesityOffer = ZDataTestUtility.createUniversityOfferStudy(null, true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = univesityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        Offer__c specialisationOffer = ZDataTestUtility.createSpecializationOffer(new Offer__c(Offer_from_Specialization__c = specialtyOffer.Id), true);
        
        Test.startTest();
        Boolean result = TrainingWebservices.hasSpecializations(specialtyOffer.Id);
        Test.stopTest();
        
        System.assertEquals(result, true);
    }
    
    @isTest static void test_markParticipantsAsPaid_OpenTrainingLaunched_GroupCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5, Launched__c = true), true);
        
        Enrollment__c openTraining = ZDataTestUtility.createOpenTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED, 
                Enrollment_Training_Participant__c = openTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Last_Active_Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
    }
    
    @isTest static void test_markParticipantsAsPaid_OpenTrainingLaunched_GroupNotCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5, Launched__c = true), true);
        
        Enrollment__c openTraining = ZDataTestUtility.createOpenTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Enrollment_Training_Participant__c = openTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED);
        }
    }
    
    @isTest static void test_markParticipantsAsPaid_OpenTrainingNotLaunched_GroupCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5, Launched__c = false), true);
        
        Enrollment__c openTraining = ZDataTestUtility.createOpenTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED, 
                Enrollment_Training_Participant__c = openTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Last_Active_Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
    }
    
    @isTest static void test_markParticipantsAsPaid_OpenTrainingNotLaunched_GroupNotCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Number_of_Seats__c = 5, Launched__c = false), true);
        
        Enrollment__c openTraining = ZDataTestUtility.createOpenTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED, 
                Enrollment_Training_Participant__c = openTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP);
        }
    }
    
    @isTest static void test_markParticipantsAsPaid_ClosedTraining_GroupNotCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED, 
                Enrollment_Training_Participant__c = closedTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
    }
    
    @isTest static void test_markParticipantsAsPaid_ClosedTraining_GroupCancelled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(Number_of_Seats__c = 5), true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
            new Enrollment__c(
                Training_Offer__c = trainingOffer.Id, 
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
        ), true);
        
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(
            new Enrollment__c(
                Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED, 
                Enrollment_Training_Participant__c = closedTraining.Id,
                Training_Offer_for_Participant__c = trainingOffer.Id
        ), true);
        
        String[] idArray = new String[] { participant.Id } ;
        
        Test.startTest();
        Boolean result = TrainingWebservices.markParticipantsAsPaid(idArray);
        Test.stopTest();
        
        List<Enrollment__c> relatedParticipants = [
            SELECT Id, Status__c, Last_Active_Status__c, Enrollment_Training_Participant__c, Training_Offer_for_Participant__c 
            FROM Enrollment__c 
            WHERE Id in :idArray
        ];
        
        for (Enrollment__c relatedParticipant : relatedParticipants) {
            System.assertEquals(relatedParticipant.Last_Active_Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }
    }

}