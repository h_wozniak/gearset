/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send emails with Study Enrollment confirmation reminder
**/

/**
*   To initiate the EnrollmentSndDocDeliveryEmailReminder execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('EnrollmentSndDocDeliveryEmailReminder', '0 0 8 * * ? *', new EnrollmentSndDocDeliveryEmailReminder());
**/


global class EnrollmentSndDocDeliveryEmailReminder implements Schedulable {

    global void execute(SchedulableContext sc) {
        // 3 days after enrollment (4 days before deadline) [has 7 or less days for delivery]
        //DocDeliveryLessThanSevenDaysRemindBatch batchLessThanSevenDayReminder3Days = new DocDeliveryLessThanSevenDaysRemindBatch(3);
        //Database.executebatch(batchLessThanSevenDayReminder3Days, 100);

        // 7 days before deadline [has more than 7 days for delivery]
      //  DocDeliveryMoreThanSevenDaysRemindBatch batchMoreThanSevenDaysReminder7Days = new DocDeliveryMoreThanSevenDaysRemindBatch(7);
        //Database.executebatch(batchMoreThanSevenDaysReminder7Days, 100);
    }

}