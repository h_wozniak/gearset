public with sharing class LEX_ResignationController {

    @AuraEnabled
    public static String checkStatus(String recordId) {
        Enrollment__c enr = [SELECT Status__c FROM Enrollment__c WHERE Id = :recordId];
        return (enr.Status__c != CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES) ? 'TRUE' : 'FALSE';
    }

    @AuraEnabled
    public static String hasEditAccess(String recordId) {
        return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
    }

    @AuraEnabled
    public static String updateFieldValues(String objId, String sObjectName, String[] fieldNames, String[] fieldTypes, String[] fieldValues) {
        return LEXServiceUtilities.updateFieldValues(objId, sObjectName, fieldNames, fieldTypes, fieldValues) ? 'SUCCESS' : 'FAILURE';
    }
}