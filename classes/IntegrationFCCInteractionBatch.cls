/**
* @author       Sebastian Łasisz
* @description  Batch class for creating Tasks and updating Campaign Members for FCC Interaction
**/

global class IntegrationFCCInteractionBatch implements Database.Batchable<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>, Database.AllowsCallouts {
    DateTime timeToUpdate;
    String jsonBody;

    global IntegrationFCCInteractionBatch() {
        timeToUpdate = DateTime.now();
    }
    
    global Iterable<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> start(Database.BatchableContext BC) {
        jsonBody = IntegrationFCCInteractionHelper.sendInteractionRequestFinal();

        List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> listOfInteractcions;

        try {
            listOfInteractcions = IntegrationFCCInteractionHelper.parse(jsonBody);
        } catch(Exception ex) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationFCCInteractionHelper.class.getName() 
            + '\n' + jsonBody + '\n');
        }

        if (listOfInteractcions != null && !listOfInteractcions.isEmpty()) {
            listOfInteractcions.sort();
        }

        return listOfInteractcions;
    }

    global void execute(Database.BatchableContext BC, List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> scope) {
        Set<String> setOfExtContId = new Set<String>();
        List<String> classifiersIds = new List<String>();

        for (IntegrationFCCInteractionHelper.InteractionReceiver_JSON interaction : scope) {
            setOfExtContId.add(interaction.external_contact_id);
            classifiersIds.add(interaction.classifiers_id);
        }

        List<Task> tasksToInsert = IntegrationFCCInteractionHelper.prepareTasksToInsert(setOfExtContId, classifiersIds, scope);
        List<Marketing_Campaign__c> campaignMembers = IntegrationFCCInteractionHelper.prepareCampaignMembersToUpdate(setOfExtContId, classifiersIds, scope);

        try {
            if (tasksToInsert != null && tasksToInsert.size() > 0) {
                insert tasksToInsert;   
            }
            if (campaignMembers != null && campaignMembers.size() > 0) {  
                CommonUtility.skipContactUpdateInOtherSystems = true; 
                update campaignMembers;
            }
        } catch(Exception ex) {
            ErrorLogger.logAndMsg(ex, CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationFCCInteractionHelper.class.getName() 
            + '\n' + jsonBody + '\n');
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        FCC_Last_Interaction_Retrival_Date__c fccLastInteraction = FCC_Last_Interaction_Retrival_Date__c.getOrgDefaults();
        fccLastInteraction.Date_From__c = timeToUpdate;

        update fccLastInteraction;
    }

}