@isTest
private class ZTestEmailManager_StudyExtranetEmail {

    private static void prepareTemplates() {
        EmailTemplate defaultEmailTemplate = new EmailTemplate();
        defaultEmailTemplate.isActive = true;
        defaultEmailTemplate.Name = 'x';
        defaultEmailTemplate.DeveloperName = EmailManager_StudyExtranetEmail.WorfklowEmailNames.X29_I_Extranet.name();
        defaultEmailTemplate.TemplateType = 'text';
        defaultEmailTemplate.FolderId = UserInfo.getUserId();
        defaultEmailTemplate.Subject = 'x';

        insert defaultEmailTemplate;
    }

    @isTest static void test_SendRequiredDocumentList() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }
        
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        Test.startTest();

        studyEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS;
        update studyEnr;

        Enrollment__c studyAgr = ZDataTestUtility.createEducationalAgreement(
        	new Enrollment__c(
        		Source_Enrollment__c = studyEnr.Id,
        		Student_no__c = '12344',
        		Portal_Login__c = 'login'
        ), true);

        studyAgr.TECH_Extranet_Data_Update_Date__c = System.today();
        update studyAgr;

        studyEnr.Educational_Agreement__c = studyAgr.Id;
        update studyEnr;
        
        List<Enrollment__c> enrList = [
            SELECT Id, Status__c, Educational_Agreement__c
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            AND Educational_Agreement__c != null 
            AND Educational_Agreement__r.TECH_Extranet_Data_Update_Date__c = :System.today().addDays(0)
        ];

        System.assertEquals(enrList.size(), 1);
        System.assertEquals(enrList.get(0).Status__c, CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS);
        System.assertNotEquals(enrList.get(0).Educational_Agreement__c, null);

        DocDeliveryExtranetBatch extranetEmailBatch = new DocDeliveryExtranetBatch(0);
        Database.executebatch(extranetEmailBatch, 100);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

}