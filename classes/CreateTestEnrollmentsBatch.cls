/**
* @author       Sebastian Łasisz
* @description  Batch class for creating many enrollments at once
**/

global class CreateTestEnrollmentsBatch implements Database.Batchable<sObject> {
    Set<Id> offerIds;
    String external_id;
    String email;

    global CreateTestEnrollmentsBatch(Set<Id> offerIds, String external_id, String email) {
        this.offerIds = offerIds;
        this.external_id = external_id;
        this.email = email;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Degree__c, University_Name__c, Acceptation_Status__c, Name
                FROM Offer__c
                WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
                AND Enrollment_only_for_Specialties__c = false)
                AND Id NOT IN :offerIds
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject product : scope) {
        	Offer__c offerProduct = (Offer__c)product;

            String body = '{"updating_system":"ZPI","updating_system_contact_id":"' + external_id + '","first_name":"testIPressoZPI","middle_name":"string","last_name":"testIPressoZPI","family_name":"Smith","father_name":"David","mother_name":"Mom","gender":"Male","personal_id":"81070612041","birthdate":"1981-07-06","place_of_birth":"Poland","country_of_birth":"Poland","country_of_origin":"Poland","nationality":["polish"],"residence_document":{"type":"Visa","validity_date":"2016-10-10"},"polish_card":true,"polish_card_validity_date":"2017-10-10","identity_document":{"type":"Identity Card","identity_document_number":"ANV956644","issue_country":"Poland"},"health_insurance_validity_date":"2030-10-10","teb_graduate":true,"wsb_graduate":{"school":"true"},"career":[{"company_name":"Enxoo","position":"Software Engineer","date_start":"2015-10-10","date_end":"2020-10-10","address":{"street":"Ruska 3","city":"Wrocław","postal_code":"50-243","country":"Poland"}}],"finished_trainings":[{"name":"string","class_year":"1234"}],"addresses":[{"type":"Other","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Starachowice","street":"Lelewela"},{"type":"Mailing","country":"Poland","postal_code":"27-200","state":null,"city":"Wrocław","street":"Kościuszki"}],"phones":[{"type":"Default","phoneNumber":"123456789"}],"email":"' + email + '","consents":[{"name":"Marketing WSB Wrocław","value":false}],"enrollment_data":{"enrollment_id_zpi":"string","product_name":"'+ offerProduct.Name + '","tuition_system":"Guaranteed Graded Tuition","number_of_installments":12,"discount_code":null,"enrollment_language":"pl","source":"Educational Advisor","educational_advisor":"de1","company_for_discount":{"name":"string","tax_id":"string"},"selected_languages":[{"code":"English","level":"Basic"}],"university":{"name":"WSB Wrocław","city":"Wrocław","finished_course":"Archeologia","graduation_year":"2015","diploma":{"status":"Diploma acquired","defense_date":"2015-10-10","diploma_number":"123456","issue_country":"Poland","issue_city":"Wrocław","issue_date":"2016-10-10","title":"Master"}},"secondary_school":{"name":"Moja najlepsza szkoła","type":"High School","city":"Starachowice","a_level_status":"Passed","a_level_id":"Moje id","a_level_issue_date":"2015-10-10","examination_committee":"Regional Examination Commission in Jaworzno","certificate_city":"Starachowice","certificate_country":"Poland","graduation_year":"2015-10-10"}}}';

            RestRequest req = new RestRequest();
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(body);

            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;

            CommonUtility.skipContactUpdateInOtherSystems = true;
            IntegrationEnrollmentReceiver.receiveEnrollmentInformation();
        }
    }
    
    global void finish(Database.BatchableContext BC) {}    
}