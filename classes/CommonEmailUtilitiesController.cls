public without sharing class CommonEmailUtilitiesController {

    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- INIT ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public String enrollmentId { get; set; }


    public CommonEmailUtilitiesController() {}



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Enrollment__c studyEnr;
    public Enrollment__c getStudyEnr(Id enrId) {
        if (studyEnr == null && enrId != null) {
            studyEnr = [
                    SELECT Id, Degree__c, Initial_Specialty_Declaration__c, Trade_Name__c, Mode__c, Delivery_Deadline__c,
                            Initial_Specialty_Declaration__r.Specialty_Trade_Name__c, Language_of_Enrollment__c,
                            Course_or_Specialty_Offer__r.Trade_Name__c, Specialization__r.Trade_Name__c,
                            Course_or_Specialty_Offer__r.Specialty_Trade_Name__c,
                            Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
                            Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c,
                            Course_or_Specialty_Offer__r.Number_of_Semesters__c,
                            Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Number_of_Semesters__c
                    FROM Enrollment__c
                    WHERE Id = :enrollmentId
            ];
        }

        return studyEnr;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- METHODS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public Integer getNumberOfRemainingDays() {
        if (enrollmentId != null) {
            Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);

            Integer remainingDays = System.today().daysBetween(studyEnrollment.Delivery_Deadline__c);
            return remainingDays >= 0 ? remainingDays : 0;
        }
        else {
            return 0;
        }
    }

    public String getAcademicYearRepresentation() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);

        if (studyEnrollment != null) {
            Date studyStartDate = (studyEnrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null?
                    studyEnrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c :
                    studyEnrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c);

            if (studyStartDate != null) {
                if (Integer.valueOf(studyStartDate.month()) < 9) {
                    return (String.valueOf(studyStartDate.year() - 1) + '/' + String.valueOf(studyStartDate.year()).substring(2, 4));
                }
                else {
                    return studyStartDate.year() + '/' + (String.valueOf(studyStartDate.year() + 1));
                }
            }
        }

        return null;
    }

    public String getMode() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);
        if (studyEnrollment != null) {
            String language = CommonUtility.getLanguageAbbreviation(studyEnrollment.Language_of_Enrollment__c);
            Map<String, String> translatedPickListValues = DictionaryTranslator.getTranslatedPicklistValues(language, Enrollment__c.Mode__c);

            if (studyEnrollment != null) {
                return translatedPickListValues.get(studyEnrollment.Mode__c);
            }
        }

        return null;

    }

    public String getMBAYearRange() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);

        if (studyEnrollment != null) {
            Date studyStartDate = (studyEnrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null?
                    studyEnrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c :
                    studyEnrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c);

            Decimal numberOfSemesters = (studyEnrollment.Course_or_Specialty_Offer__r.Number_of_Semesters__c != null?
                    studyEnrollment.Course_or_Specialty_Offer__r.Number_of_Semesters__c :
                    studyEnrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.Number_of_Semesters__c);

            if (studyStartDate != null && numberOfSemesters != null) {
                Date studyEndDate = studyStartDate.addYears(Integer.valueOf(Math.ceil(numberOfSemesters / 2)));
                return studyStartDate.year() + '-' + studyEndDate.year();
            }
        }

        return null;
    }

    public String getCourseAndSpecialty() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);

        if (studyEnrollment != null) {
            if (studyEnrollment.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
                if (studyEnrollment.Initial_Specialty_Declaration__c != null) {
                    if (studyEnrollment.Initial_Specialty_Declaration__r.Specialty_Trade_Name__c != null) {
                        return studyEnrollment.Initial_Specialty_Declaration__r.Specialty_Trade_Name__c + ', ' + studyEnrollment.Specialization__r.Trade_Name__c;
                    }
                    else {
                        return studyEnrollment.Initial_Specialty_Declaration__r.Trade_Name__c + ', ' + studyEnrollment.Specialization__r.Trade_Name__c;
                    }
                }
                else {
                    if (studyEnrollment.Course_or_Specialty_Offer__r.Specialty_Trade_Name__c != null) {
                        return studyEnrollment.Course_or_Specialty_Offer__r.Specialty_Trade_Name__c + ', ' + studyEnrollment.Specialization__r.Trade_Name__c;
                    }
                    else {
                        return studyEnrollment.Course_or_Specialty_Offer__r.Trade_Name__c + ', ' + studyEnrollment.Specialization__r.Trade_Name__c;
                    }
                }
            }
            else {
                return studyEnrollment.Trade_Name__c;
            }
        }

        return null;
    }

    public void prepareExpandedDegreeNameForUnenrolledMap() {
        expandedDegreeNameMap = new Map<String, String> {
                CommonUtility.OFFER_DEGREE_I => 'UNENROLLED_OFFER_DEGREE_I',
                CommonUtility.OFFER_DEGREE_II => 'UNENROLLED_OFFER_DEGREE_II',
                CommonUtility.OFFER_DEGREE_II_PG => 'UNENROLLED_OFFER_DEGREE_II_PG',
                CommonUtility.OFFER_DEGREE_PG => 'UNENROLLED_OFFER_DEGREE_PG',
                CommonUtility.OFFER_DEGREE_MBA => 'UNENROLLED_OFFER_DEGREE_MBA',
                CommonUtility.OFFER_DEGREE_U => 'UNENROLLED_OFFER_DEGREE_U'
        };
    }

    public String getExpandedDegreeUnenrolledName() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);
        prepareExpandedDegreeNameForUnenrolledMap();

        if (studyEnrollment != null) {
            String fullName = DictionaryTranslator.getTranslatedPicklistValues(
                    CommonUtility.getLanguageAbbreviation(studyEnrollment.Language_of_Enrollment__c),
                    expandedDegreeNameMap.get(studyEnrollment.Degree__c)
            );

            if (fullName == null) {
                return studyEnrollment.Degree__c;
            }
            else {
                return fullName;
            }
        }

        return null;
    }

    Map<String, String> expandedDegreeNameMap = new Map<String, String>();
    public void prepareExpandedDegreeNameMap() {
        expandedDegreeNameMap = new Map<String, String> {
                CommonUtility.OFFER_DEGREE_I => 'OFFER_DEGREE_I',
                CommonUtility.OFFER_DEGREE_II => 'OFFER_DEGREE_II',
                CommonUtility.OFFER_DEGREE_II_PG => 'OFFER_DEGREE_II_PG',
                CommonUtility.OFFER_DEGREE_PG => 'OFFER_DEGREE_PG',
                CommonUtility.OFFER_DEGREE_MBA => 'OFFER_DEGREE_MBA',
                CommonUtility.OFFER_DEGREE_U => 'OFFER_DEGREE_U'
        };
    }

    public String getExpandedDegreeName() {
        Enrollment__c studyEnrollment = getStudyEnr(enrollmentId);
        prepareExpandedDegreeNameMap();

        if (studyEnrollment != null) {
            String fullName = DictionaryTranslator.getTranslatedPicklistValues(
                    CommonUtility.getLanguageAbbreviation(studyEnrollment.Language_of_Enrollment__c),
                    expandedDegreeNameMap.get(studyEnrollment.Degree__c)
            );

            if (fullName == null) {
                return studyEnrollment.Degree__c;
            }
            else {
                return fullName;
            }
        }

        return null;
    }

}