/**
*   @author         Sebastian Łasisz
*   @description    This class is used to make dictionaries available for external external systems.
**/
global with sharing class IntegrationConsentDictionaryHelper {


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- LOGIC TO GET DICTIONARY --------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */ 

    public static String getDictionaryConsents() {
        
		List<Catalog__c> listOfConsents = [
	    	SELECT Name, Active_From__c, Active_To__c, Content__c, Preamble_Content__c, Mandatory__c, Kind__c, ADO__c, ADO_for_ZPI__c,
            (SELECT Id, Name, Language_Code__c, Trade_Name__c, Preamble_Content__c, Content__c FROM Consent_Languages__r)
	    	FROM Catalog__c 
	    	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
		];

        List<Consent_JSON> listOfConsentsToJSON = new List<Consent_JSON>();
        for (Catalog__c consent : listOfConsents) {     
            Consent_JSON consentJSON = new Consent_JSON();       
            List<ConsentLanguage_JSON> languageConsents = new List<ConsentLanguage_JSON>();

            for (Catalog__c languageConsent : consent.Consent_Languages__r) {
                ConsentLanguage_JSON langConsentJSON = new ConsentLanguage_JSON();
                langConsentJSON.translation_language_code = languageConsent.Language_Code__c;
                langConsentJSON.translation_preamble = languageConsent.Preamble_Content__c;
                langConsentJSON.translation_content = languageConsent.Content__c;

                languageConsents.add(langConsentJSON);
            }

            consentJSON.consent_name = consent.Name;
            consentJSON.consent_preamble = consent.Preamble_Content__c;
            consentJSON.consent_content = consent.Content__c;
            consentJSON.consent_usage = consent.Kind__c;
            consentJSON.consent_mandatory = consent.Mandatory__c;
            consentJSON.consent_active_from = consent.Active_From__c;
            consentJSON.consent_active_to = consent.Active_To__c;
            consentJSON.consent_departments = CommonUtility.getOptionsFromMultiSelectToList(consent.ADO_for_ZPI__c);
            consentJSON.consent_translations = languageConsents;
            listOfConsentsToJSON.add(consentJSON);
        }


    	return JSON.serialize(listOfConsentsToJSON);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ MODEL DEFINITION ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */    
    global class Consent_JSON {
        public String consent_name;
        public String consent_preamble;
        public String consent_content;
        public String consent_usage;
        public List<String> consent_departments;
        public Boolean consent_mandatory;
        public Date consent_active_from;
        public Date consent_active_to;
        public List<ConsentLanguage_JSON> consent_translations;
    }

    global class ConsentLanguage_JSON {
        public String translation_language_code;
        public String translation_preamble;
        public String translation_content;
    }
}