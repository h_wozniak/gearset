@isTest
private class ZTestWebserviceUtilities {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    static User getUserWithProfileAnalysis() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = 'TEBA Contact Center']; 

        User usr = new User(
            Alias = 'tebaAn',
            Email='tebaAn@tebaAn.com', 
            EmailEncodingKey = 'UTF-8',
            LastName = 'TestingAn',
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US',
            ProfileId = usrProfile.Id, 
            TimezoneSidKey = 'America/Los_Angeles',
            Username = 'tebaanalyst@testorg.com'
        );

        insert usr;
        return usr;
    }

    static User getUserWithProfileSales() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = 'TEBA Sales - Training'];
        //UserRole usrRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'WRO_Dzia_Szkole'];

        User usr = new User(
            Alias = 'tebaST', 
            Email = 'tebaST@tebaAn.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'TestingSt', 
            LanguageLocaleKey = 'pl', 
            LocaleSidKey = 'pl', 
            ProfileId = usrProfile.Id, 
            Entity__c = 'WRO', 
            //UserRoleId = usrRole.Id, 
            TimezoneSidKey = 'America/Los_Angeles', 
            Username = 'tebasalestraining@testorg.com'
        );

        insert usr;
        return usr;
    }


    @isTest static void testUserAnalysisHasEditAccess() {
        User usr = getUserWithProfileAnalysis();

        Id newEnrollmentId = ZDataTestUtility.createOpenTrainingEnrollment(null, true).Id;
        Id newDiscountId = ZDataTestUtility.createADHOCDiscount(null, true).Id;
        Id newOfferId = ZDataTestUtility.createTrainingOffer(null, true).Id;

        System.runAs(usr) {
            Test.startTest();

                System.assert(!WebserviceUtilities.hasEditAccess(newEnrollmentId));
                System.assert(!WebserviceUtilities.hasEditAccess(newDiscountId));
                System.assert(!WebserviceUtilities.hasEditAccess(newOfferId));

            Test.stopTest();
        }
    }

    @isTest static void testUserSalesTrainingHasEditAccess() {
        User usr = getUserWithProfileSales();

        Id newEnrollmentId = ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(University_Name__c = 'WSB Wrocław'), true).Id;
        Id newDiscountId = ZDataTestUtility.createADHOCDiscount(null, true).Id;
        Id newOfferId = ZDataTestUtility.createTrainingOffer(null, true).Id;

        System.runAs(usr) {
            Test.startTest();

                System.assert(!WebserviceUtilities.hasEditAccess(newEnrollmentId)); // No access, specific Role needed (Sharing Rules)
                System.assert(!WebserviceUtilities.hasEditAccess(newDiscountId));
                System.assert(!WebserviceUtilities.hasEditAccess(newOfferId)); // No access, specific Role needed (Sharing Rules)

            Test.stopTest();
        }
    }

    @isTest static void testUserAnalysisHasEditAccessList() {
        User usr = getUserWithProfileAnalysis();

        List<Id> newEnrollmentsIds = new List<Id>(new Map<Id, Enrollment__c>(ZDataTestUtility.createOpenTrainingEnrollments(null, 2, true)).keySet());
        List<Id> newDiscountsIds = new List<Id>(new Map<Id, Discount__c>(ZDataTestUtility.createADHOCDiscounts(null, 2, true)).keySet());
        List<Id> newOffersIds = new List<Id>(new Map<Id, Offer__c>(ZDataTestUtility.createTrainingOffers(null, 2, true)).keySet());

        System.runAs(usr) {
            Test.startTest();

                System.assert(!WebserviceUtilities.hasEditAccessList(newEnrollmentsIds));
                System.assert(!WebserviceUtilities.hasEditAccessList(newDiscountsIds));
                System.assert(!WebserviceUtilities.hasEditAccessList(newOffersIds));

            Test.stopTest();
        }

    }

    @isTest static void testUserSalesTrainingHasEditAccessList() {
        User usr = getUserWithProfileSales();

        List<Id> newEnrollmentsIds = new List<Id>(new Map<Id, Enrollment__c>(ZDataTestUtility.createOpenTrainingEnrollments(null, 2, true)).keySet());
        List<Id> newDiscountsIds = new List<Id>(new Map<Id, Discount__c>(ZDataTestUtility.createADHOCDiscounts(null, 2, true)).keySet());
        List<Id> newOffersIds = new List<Id>(new Map<Id, Offer__c>(ZDataTestUtility.createTrainingOffers(null, 2, true)).keySet());

        System.runAs(usr) {
            Test.startTest();

                System.assert(!WebserviceUtilities.hasEditAccessList(newEnrollmentsIds));
                System.assert(!WebserviceUtilities.hasEditAccessList(newDiscountsIds));
                System.assert(!WebserviceUtilities.hasEditAccessList(newOffersIds));

            Test.stopTest();
        }

    }

    @isTest static void testGetNumberByUserLocaleUS() {
        User usr = getUserWithProfileAnalysis();

        System.runAs(usr){
            Test.startTest();

                System.assertEquals('12,345.679', WebserviceUtilities.getNumberByUserLocale(12345.679));

            Test.stopTest();
        }
    }

    @isTest static void testGetNumberByUserLocalePL() {
        User usr = getUserWithProfileSales();

        System.runAs(usr){
            Test.startTest();

                System.assertEquals('1' + String.fromCharArray( new List<integer> { 160 }) + '234', WebserviceUtilities.getNumberByUserLocale(1234));
            
            Test.stopTest();
        }
    }

    @isTest static void testGetPrefixWebservice() {
        Test.startTest();

            System.assertEquals('a03', WebserviceUtilities.getPrefixWebservice('Enrollment__c'));
            System.assertEquals('a05', WebserviceUtilities.getPrefixWebservice('Offer__c'));
            System.assertEquals('a02', WebserviceUtilities.getPrefixWebservice('Discount__c'));

        Test.stopTest();
    }

    @isTest static void testGetRecordTypeId() {
        Test.startTest();

            // Enrollment
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Enrollment__c', 'Enrollment_Open_Training'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Enrollment__c', 'Enrollment_Closed_Training'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Enrollment__c', 'Enrollment_Study'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Enrollment__c', 'Enrollment_Participant_Result'));

            // Offer
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Offer__c', 'Course_Offer'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Offer__c', 'Price_Book'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Offer__c', 'Specialization'));
            System.assertNotEquals(null, WebserviceUtilities.getRecordTypeId('Offer__c', 'University_Offer_Study'));

            // Empty Strings
            try {
                WebserviceUtilities.getRecordTypeId('', '');
                System.assert(false);
            } catch(Exception e) {
                Boolean messageFound = false;
                if(e.getMessage().contains('ObjectName and RecType needs to be delivered!')) {
                    messageFound = true;
                }
                System.assert(messageFound);
            }

            // Wrong recTypeDevName
            try {
                WebserviceUtilities.getRecordTypeId('Enrollment__c', 'wrongRecTypeDevName');
                System.assert(false);
            } catch(Exception e) {
                Boolean messageFound = false;
                if(e.getMessage().contains('No such record type found! [OBJECT: ' + 'Enrollment__c' +' RECTYPE: ' + 'wrongRecTypeDevName' + ']')) {
                    messageFound = true;
                }
                System.assert(messageFound);
            }


        Test.stopTest();
    }

    @isTest static void testGetLabelForFieldUS() {
        User usr = getUserWithProfileAnalysis(); // US Locale

        // Enrollment
        List<String> enrollmentLabelNames = new List<String>();
        enrollmentLabelNames.add('Acceptation Status');
        enrollmentLabelNames.add('Company Name');
        enrollmentLabelNames.add('First Name');
        enrollmentLabelNames.add('Support ID');
        enrollmentLabelNames.add('Value after Discount');

        List<String> enrollmentFieldNames = new List<String>();
        Map<String, Schema.SObjectField> enrollmentSchemaFieldMap = Schema.SObjectType.Enrollment__c.fields.getMap();

        for (String fieldName : enrollmentSchemaFieldMap.keySet()) {
            enrollmentFieldNames.add(fieldName);  
        }

        // Offer
        List<String> offerLabelNames = new List<String>();
        offerLabelNames.add('Active');
        offerLabelNames.add('EFS');
        offerLabelNames.add('Installment Variants');
        offerLabelNames.add('Trainer');
        offerLabelNames.add('University');

        List<String> offerFieldNames = new List<String>();
        Map<String, Schema.SObjectField> offerSchemaFieldMap = Schema.SObjectType.Offer__c.fields.getMap();

        for (String fieldName : offerSchemaFieldMap.keySet()) {
            offerFieldNames.add(fieldName);  
        }
        
        // checks if returned list of label names for specific user locale contains given names
        System.runAs(usr) {
            Test.startTest();

                System.assert((new Set<String>(WebserviceUtilities.getLabelForField('Enrollment__c', enrollmentFieldNames))).containsAll(enrollmentLabelNames));
                System.assert((new Set<String>(WebserviceUtilities.getLabelForField('Offer__c', offerFieldNames))).containsAll(offerLabelNames));                

            Test.stopTest();
        }

    }

    @isTest static void testGetLabelForFieldPL() {
        User usr = getUserWithProfileSales(); // PL Locale

        // Enrollment
        List<String> enrollmentLabelNames = new List<String>();
        enrollmentLabelNames.add('Status akceptacji');
        enrollmentLabelNames.add('Miasto');
        enrollmentLabelNames.add('Rekrutacja');
        enrollmentLabelNames.add('Źródło');
        enrollmentLabelNames.add('Oferta szkolenia');

        List<String> enrollmentFieldNames = new List<String>();
        Map<String, Schema.SObjectField> enrollmentSchemaFieldMap = Schema.SObjectType.Enrollment__c.fields.getMap();

        for (String fieldName: enrollmentSchemaFieldMap.keySet()) {
            enrollmentFieldNames.add(fieldName);  
        }

        // Offer
        List<String> offerLabelNames = new List<String>();
        offerLabelNames.add('Przyjęci');
        offerLabelNames.add('Certyfikat');
        offerLabelNames.add('Kandydaci');
        offerLabelNames.add('Posiada czesne stopniowane');
        offerLabelNames.add('Cena za osobę');

        List<String> offerFieldNames = new List<String>();
        Map<String, Schema.SObjectField> offerSchemaFieldMap = Schema.SObjectType.Offer__c.fields.getMap();

        for (String fieldName: offerSchemaFieldMap.keySet()) {
            offerFieldNames.add(fieldName);  
        }
        
        // checks if returned list of label names for specific user locale contains given names
        System.runAs(usr) {
            Test.startTest();

                System.assert((new Set<String>(WebserviceUtilities.getLabelForField('Enrollment__c', enrollmentFieldNames))).containsAll(enrollmentLabelNames));
                System.assert((new Set<String>(WebserviceUtilities.getLabelForField('Offer__c', offerFieldNames))).containsAll(offerLabelNames));                

            Test.stopTest();
        }

    }

    @isTest static void testGetPicklistAsHTML() {
        User u = createENLangUser_forStudies();

        System.runAs(u) {
            Test.startTest();

            System.assertEquals(
                    '<select id="some-Id"><option value="Attended">Attended</option><option value="Not Attended">Not Attended</option></select>',
                    WebserviceUtilities.getPicklistAsHTML('Enrollment__c', 'Attendance__c', 'some-Id')
            );

            Test.stopTest();
        }
    }

    @isTest static void testUpdateFieldValues() {
        Id newEnrollmentId = ZDataTestUtility.createOpenTrainingEnrollment(null, true).Id;

        List<String> fieldNames = new List<String>();
        fieldNames.add('Status__c');
        fieldNames.add('Resignation_Reason_List__c');
        fieldNames.add('Resignation_Date__c');

        List<String> fieldTypes = new List<String>();
        fieldTypes.add('string');
        fieldTypes.add('string');
        fieldTypes.add('date');

        List<String> fieldValues = new List<String>();
        fieldValues.add('Resignation');
        fieldValues.add('randomReason');
        fieldValues.add('20.06.2014');

        Test.startTest();

            System.assert(WebserviceUtilities.updateFieldValues(newEnrollmentId, 'Enrollment__c', fieldNames, fieldTypes, fieldValues));

            Enrollment__c updatedEnrollment = [SELECT Id, Status__c, Resignation_Reason_List__c, Resignation_Date__c FROM Enrollment__c WHERE Id = :newEnrollmentId];
            
            System.assert(updatedEnrollment.Status__c == 'Resignation');
            System.assert(updatedEnrollment.Resignation_Reason_List__c == 'randomReason');
            System.assert(String.valueOf(updatedEnrollment.Resignation_Date__c) == '2014-06-20');

        Test.stopTest();
    }

    @isTest static void testVerifyContactDataCompletion() {
        Contact cntCompleteTemplate = new Contact(
            The_Same_Mailling_Address__c = false,
            FirstName = 'Jan', 
            LastName = 'Janowski',
            Pesel__c = '48120714157', 
            Phone = '123456789',
            MailingStreet = 'randomStreet', 
            MailingCity = 'randomCity',
            MailingPostalCode = '27-200'
        );

        Id cntIncomplete = ZDataTestUtility.createCandidateContact(null, true).Id;
        Id cntComplete = ZDataTestUtility.createCandidateContact(cntCompleteTemplate, true).Id;

        Test.startTest();

            System.assert(!WebserviceUtilities.verifyContactDataCompletion(cntIncomplete));
            System.assert(WebserviceUtilities.verifyContactDataCompletion(cntComplete));

        Test.stopTest();
    }

    @isTest static void test_getAdditionalChargesAsPicklistHTML() {
        CustomSettingDefault.initAdditionalCharges();

        String picklistHTML = WebserviceUtilities.getAdditionalChargesAsPicklistHTML(null, 'htmlTAG');

        System.assert(picklistHTML.contains('option'));
    }

    @isTest static void test_updateContactAndRepinIPressoContact() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Change_History__c = 'asd'), true);
        Marketing_Campaign__c  iPressoContact = ZDataTestUtility.createIPressoContact(null, true);
        Marketing_Campaign__c  iPressoContac2 = ZDataTestUtility.createIPressoContact(null, false);

        WebserviceUtilities.updateContactAndRepinIPressoContact(JSON.serialize(candidate), JSON.serialize(iPressoContact), JSON.serialize(new List<Marketing_Campaign__c> { iPressoContac2 }));
    }

}