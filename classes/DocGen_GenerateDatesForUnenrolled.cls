/**
* @author       Wojciech Słodziak, Sebastian Łasisz
* @description  Class for storing methods related to generating documents for Enrollment__c object for Unenrolled process
**/

global class DocGen_GenerateDatesForUnenrolled implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateDatesForUnenrolled() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {  
      	if (methodName == 'generateUnenrolledAgreementTill') {
      		return generateUnenrolledAgreementTill(objectId);
      	}
      	else if (methodName == 'generateDateOfWidthrawal') {
      		return generateDateOfWidthrawal(objectId);
      	}
      	else if (methodName == 'generateResignationTill') {
      		return generateResignationTill(objectId);
      	}
      	else return '';
    }

    global static String generateUnenrolledAgreementTill(String objectId) {
      	Enrollment__c docEnr = [
    	    SELECT Id, Enrollment_From_documents__r.University_Name__c,
	        Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, 
	            Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
	        FROM Enrollment__c
	        WHERE Id = :objectId
    	];

    	String periodOfRecrutation = generatePeriodOfRecrutation(docEnr);

      	Catalog__c offerDateAssigment = [
        	SELECT Id, Period_of_Recrutation__c, Unenrolled_Agreement_Till__c
        	FROM Catalog__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_OFFER_DATE_ASSIGMENT)
        	AND Entity__c = :docEnr.Enrollment_From_documents__r.University_Name__c
        	AND Period_of_Recrutation__c = :periodOfRecrutation
      	];

      	return offerDateAssigment.Unenrolled_Agreement_Till__c + '.' + (determineStudyStartDate(docEnr).year());
    }

    global static String generateDateOfWidthrawal(String objectId) {
      	Enrollment__c docEnr = [
    	    SELECT Id, Enrollment_From_documents__r.University_Name__c,
	        Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, 
	            Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
	        FROM Enrollment__c
	        WHERE Id = :objectId
    	];

    	String periodOfRecrutation = generatePeriodOfRecrutation(docEnr);

      	Catalog__c offerDateAssigment = [
        	SELECT Id, Date_of_Free_Withdrawal_of_Unenrolled__c, Period_of_Recrutation__c
        	FROM Catalog__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_OFFER_DATE_ASSIGMENT)
        	AND Entity__c = :docEnr.Enrollment_From_documents__r.University_Name__c
        	AND Period_of_Recrutation__c = :periodOfRecrutation
      	];

      	return offerDateAssigment.Date_of_Free_Withdrawal_of_Unenrolled__c + '.' + (determineStudyStartDate(docEnr).year());
    }

    global static String generateResignationTill(String objectId) {
      	Enrollment__c docEnr = [
    	    SELECT Id, Enrollment_From_documents__r.University_Name__c,
	        Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, 
	            Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
	        FROM Enrollment__c
	        WHERE Id = :objectId
    	];

    	String periodOfRecrutation = generatePeriodOfRecrutation(docEnr);

      	Catalog__c offerDateAssigment = [
        	SELECT Id, Period_of_Recrutation__c, Date_of_Paid_Withdrawal_of_Unenrolled__c
        	FROM Catalog__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_OFFER_DATE_ASSIGMENT)
        	AND Entity__c = :docEnr.Enrollment_From_documents__r.University_Name__c
        	AND Period_of_Recrutation__c = :periodOfRecrutation
      	];

      	return offerDateAssigment.Date_of_Paid_Withdrawal_of_Unenrolled__c + '.' + (determineStudyStartDate(docEnr).year());

    }

    public static String generatePeriodOfRecrutation(Enrollment__c docEnr) {
    	Date dateToCheck = determineStudyStartDate(docEnr);
      	Boolean isWinterEnrollment = dateToCheck.month() >= 2 && dateToCheck.month() <= 7;
      	return isWinterEnrollment ? CommonUtility.CATALOG_PERIOD_WINTER : CommonUtility.CATALOG_PERIOD_SUMMER;
    }

    public static Date determineStudyStartDate(Enrollment__c docEnr) {
	    Date studyStartDate;
	    if (docEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c != null) {
	    	studyStartDate = docEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c;
	    }
	    else {
	    	studyStartDate = docEnr.Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c;
    	}

	    return studyStartDate;
    }
}