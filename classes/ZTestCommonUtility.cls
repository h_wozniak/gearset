@isTest
private class ZTestCommonUtility {

@isTest static void testCommonUtilityGetOptionsFromMultiSelect() {
        Test.startTest();

        System.assertEquals(new Set<String>(), CommonUtility.getOptionsFromMultiSelect(null));
        String fieldValue = 'test;value';
        Set<String> setfieldValue = new Set<String>();
        setFieldValue.add('test');
        setFieldValue.add('value');
        System.assertEquals(setFieldValue, CommonUtility.getOptionsFromMultiSelect(fieldValue));

        Test.stopTest();
    }

    @isTest static void testCommonUtilityListToSOQLString() {
        Test.startTest();
        System.assertEquals(null, CommonUtility.listToSOQLString(null));
        List<String> stringList = new List<String>();
        stringList.add('test');
        stringList.add('this');
        stringList.add('method');
        System.assertNotEquals('', CommonUtility.listToSOQLString(stringList));
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGenerateRandomString() {
        Test.startTest();
        System.assertEquals(5, CommonUtility.generateRandomString(5).length());
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetUserProfile() {
        Test.startTest();
        System.assertNotEquals('', CommonUtility.getUserProfile());
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetPickListLabeelMap() {
        Test.startTest();
        Schema.DescribeFieldResult acc = Account.Industry.getDescribe();
        Schema.sObjectField field = acc.getSObjectField();
        System.assertNotEquals(null, CommonUtility.getPicklistLabelMap(field));
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetConsentSource() {
        Test.startTest();
        System.assertEquals('Manually', CommonUtility.getConsentSource());
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetEntityByMarketingEntity() {
        Test.startTest();
        System.assertEquals('WRO', CommonUtility.getEntityByMarketingEntity('WSB Wrocław'));
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetViews() {
        Test.startTest();
        System.assertNotEquals(null, CommonUtility.getViews('Account'));
        Test.stopTest();
    }

    @isTest static void testCommonUtilityGetFieldLabel() {
        Test.startTest();
        System.assertNotEquals('', CommonUtility.getFieldLabel('Contact', 'FirstName'));
        Test.stopTest();
    }
}