@isTest
private class ZTestIntegrationEnrollmentHelper {
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(
            Phone = '123456789',
            Firstname = 'Bob',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Pesel__c = '05261508023',
            Country_of_Origin__c = 'Poland',
            The_Same_Mailling_Address__c = true,
            OtherCountry = 'Poland',
            OtherState = 'Lower Silesia',
            OtherCity = 'Wrocław',
            OtherStreet = 'Krzyki'
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = retMap.get(0).Id), true));

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;
        
        return retMap;
    }
    

    @isTest static void test_prepareEnrollmentsToSend() {
        Boolean isError = false;
        Map<Integer, sObject> dataMap = prepareData();
        
        Test.startTest();
        
        Set<Id> studyEnrollments = new Set<Id> { dataMap.get(2).Id };
        String resultJSON = IntegrationEnrollmentHelper.prepareEnrollmentsToSend(studyEnrollments);  

        System.assertNotEquals('', resultJSON);      
        
        Test.stopTest();
    }

    @isTest static void test_createPhone() {
        Map<Integer, sObject> dataMap = prepareData();
        Contact candidate = (Contact)dataMap.get(0);
        
        Test.startTest();
        List<IntegrationEnrollmentHelper.Phone_JSON> phones = IntegrationEnrollmentHelper.createPhone(candidate.Phone, null);
        System.assert(phones.size() == 1);
        Test.stopTest();
    }

    @isTest static void test_createAddress() {
        Map<Integer, sObject> dataMap = prepareData();
        Enrollment__c studyEnr = (Enrollment__c)dataMap.get(2);
        
        Test.startTest();
        List<IntegrationEnrollmentHelper.Address_JSON> address = IntegrationEnrollmentHelper.createAddress(studyEnr);
        Test.stopTest();
        
        System.assert(address != null);
    }

    @isTest static void test_createConsents_emptyList() {
        //Test.startTest();
        //List<Marketing_Campaign__c> consentsOnObject = new List<Marketing_Campaign__c>();
        //List<IntegrationEnrollmentHelper.MarketingConsents_MassEnrollment_JSON> consents = IntegrationEnrollmentHelper.buildConsentJSONList(consentsOnObject, new List<Catalog__c>());
        //Test.stopTest();
        
        //System.assert(consents.size() == 0);        
    }

    @isTest static void test_createConsents_properValues() {
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_2,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_4,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_5,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        List<String> consentsToValidate = new List<String>{ RecordVals.MARKETING_CONSENT_1, RecordVals.MARKETING_CONSENT_2, 
                                                            RecordVals.MARKETING_CONSENT_4, RecordVals.MARKETING_CONSENT_5};

        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name
            FROM Catalog__c
            WHERE Name IN :consentsToValidate
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
        ];
        
        Test.startTest();
        List<Marketing_Campaign__c> consentsOnObject = ZDataTestUtility.createEnrollmentConsents(new Marketing_Campaign__c(Enrollment__c = studyEnr.Id), 5, true);
        //List<IntegrationEnrollmentHelper.MarketingConsents_MassEnrollment_JSON> consents = IntegrationEnrollmentHelper.buildConsentJSONList(consentsOnObject, catalogConsentsToValidate);
        Test.stopTest();
        
        //System.assert(consents.size() == 5);        
    }
    
    @isTest static void test_updateSyncStatus_inProgress_Fail() {
        Map<Integer, sObject> dataMap = prepareData();
        
        Test.startTest();
        IntegrationEnrollmentHelper.updateSyncStatus_inProgress(false, new Set<Id> { dataMap.get(2).Id }, '', false, false);
        Test.stopTest();
        
        Enrollment__c studyEnrAfterUpdate = [
            SELECT Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(studyEnrAfterUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_FAILED);
    }
    
    @isTest static void test_updateSyncStatus_inProgress_Success() {
        Map<Integer, sObject> dataMap = prepareData();
        
        Test.startTest();
        IntegrationEnrollmentHelper.updateSyncStatus_inProgress(true, new Set<Id> { dataMap.get(2).Id }, '', false, false);
        Test.stopTest();
        
        Enrollment__c studyEnrAfterUpdate = [
            SELECT Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(studyEnrAfterUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_INPROGRESS);
    }
    
    @isTest static void test_updateSyncStatus_inFinish_Fail() {
        Map<Integer, sObject> dataMap = prepareData();
        
        Test.startTest();
        IntegrationEnrollmentHelper.updateSyncStatus_onFinish(false, new Set<Id> { dataMap.get(2).Id }, '');
        Test.stopTest();
        
        Enrollment__c studyEnrAfterUpdate = [
            SELECT Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(studyEnrAfterUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_FAILED);
    }
    
    @isTest static void test_updateSyncStatus_inFinish_Success() {
        Map<Integer, sObject> dataMap = prepareData();
        
        Test.startTest();
        IntegrationEnrollmentHelper.updateSyncStatus_onFinish(true, new Set<Id> { dataMap.get(2).Id }, '');
        Test.stopTest();
        
        Enrollment__c studyEnrAfterUpdate = [
            SELECT Synchronization_Status__c
            FROM Enrollment__c
            WHERE Id = :dataMap.get(2).Id
        ];
        
        System.assertEquals(studyEnrAfterUpdate.Synchronization_Status__c, CommonUtility.SYNCSTATUS_FINISHED);
    }

}