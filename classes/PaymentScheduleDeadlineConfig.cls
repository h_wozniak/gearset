/**
* @author       Wojciech Słodziak
* @description  Class required for generating Payment Schedule Deadlines. Configuration for all Universities and Degrees is stored (hardcoded) in class methods. 
**/

public with sharing class PaymentScheduleDeadlineConfig {

    private static PaymentScheduleDeadlineDesignator.RuleContainer ruleCont;
    private static final Integer LAST_DAY = PaymentScheduleDeadlineDesignator.DAY_TYPE_LAST;

    public static PaymentScheduleDeadlineDesignator.RuleContainer buildRules(String universityName, String degree, Integer instPerYear, Boolean isWinterEnr, Integer numOfSemesters) {
        PaymentScheduleDeadlineConfig.ruleCont = new PaymentScheduleDeadlineDesignator.RuleContainer();
        
        String entity = CommonUtility.getEntityByUniversityName(universityName);

        if (degree == CommonUtility.OFFER_DEGREE_I || degree == CommonUtility.OFFER_DEGREE_II 
            || degree == CommonUtility.OFFER_DEGREE_II_PG || degree == CommonUtility.OFFER_DEGREE_U
        ) {
            if (isWinterEnr) {
                if (entity == CommonUtility.WSB_ENTITY_GDA) {
                    createRuleSet_Winter_I_II_U_GDA(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_POZ) {
                    createRuleSet_Winter_I_II_U_POZ(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_WRO) {
                    createRuleSet_Winter_I_II_U_WRO(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_TOR) {
                    createRuleSet_Winter_I_II_U_TOR(instPerYear);
                }
            } else {
                if (entity == CommonUtility.WSB_ENTITY_GDA) {
                    createRuleSet_Summer_I_II_U_GDA(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_POZ) {
                    createRuleSet_Summer_I_II_U_POZ(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_WRO) {
                    createRuleSet_Summer_I_II_U_WRO(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_TOR) {
                    createRuleSet_Summer_I_II_U_TOR(instPerYear);
                }
            }
        }

        if (degree == CommonUtility.OFFER_DEGREE_PG) {
            if (isWinterEnr) {
                if (entity == CommonUtility.WSB_ENTITY_GDA) {
                    createRuleSet_Winter_PG_GDA(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_POZ) {
                    createRuleSet_Winter_PG_POZ(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_WRO) {
                    createRuleSet_Winter_PG_WRO(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_TOR) {
                    createRuleSet_Winter_PG_TOR(instPerYear);
                }
            } else {
                if (entity == CommonUtility.WSB_ENTITY_GDA) {
                    createRuleSet_Summer_PG_GDA(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_POZ) {
                    createRuleSet_Summer_PG_POZ(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_WRO) {
                    createRuleSet_Summer_PG_WRO(instPerYear, numOfSemesters);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_TOR) {
                    createRuleSet_Summer_PG_TOR(instPerYear);
                }
            }
        }

        if (degree == CommonUtility.OFFER_DEGREE_MBA) {
            if (!isWinterEnr) { // only for summer enrollment
                if (entity == CommonUtility.WSB_ENTITY_GDA) {
                    createRuleSet_Summer_MBA_GDA(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_POZ) {
                    createRuleSet_Summer_MBA_POZ(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_WRO) {
                    createRuleSet_Summer_MBA_WRO(instPerYear);
                } else 
                if (entity == CommonUtility.WSB_ENTITY_TOR) {
                    createRuleSet_Summer_MBA_TOR(instPerYear);
                }
            }
        }

        return PaymentScheduleDeadlineConfig.ruleCont;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- HELPER METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static void addRule(Integer instNumber, Integer day, Integer month) {
        PaymentScheduleDeadlineConfig.ruleCont.ruleMap.put(instNumber, new PaymentScheduleDeadlineDesignator.Rule(day, month));
    }

    private static void addRuleExc(Integer instNumber, Integer day, Integer month, PaymentScheduleDeadlineDesignator.RuleExceptionType excType) {
        PaymentScheduleDeadlineConfig.ruleCont.ruleExcMap.put(instNumber, new PaymentScheduleDeadlineDesignator.RuleExc(day, month, excType));
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- CONFIG I II U ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static void createRuleSet_Summer_I_II_U_GDA(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 9);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 2);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 10);
            addRule(3, 15, 11);
            addRule(4, 15, 12);
            addRule(5, 15, 1);
            addRule(6, 15, 2);
            addRule(7, 15, 3);
            addRule(8, 15, 4);
            addRule(9, 15, 5);
            addRule(10, 15, 6);
            addRuleExc(10, 1, 6, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 10);
            addRule(3, 15, 11);
            addRule(4, 15, 12);
            addRule(5, 15, 1);
            addRule(6, 15, 2);
            addRule(7, 15, 3);
            addRule(8, 15, 4);
            addRule(9, 15, 5);
            addRule(10, 15, 6);
            addRule(11, 15, 7);
            addRule(12, 15, 8);
        }
    }

    private static void createRuleSet_Summer_I_II_U_TOR(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 9);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 2);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 10);
            addRule(3, 15, 11);
            addRule(4, 15, 12);
            addRule(5, 15, 1);
            addRule(6, 15, 2);
            addRule(7, 15, 3);
            addRule(8, 15, 4);
            addRule(9, 15, 5);
            addRule(10, 15, 6);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 9);
            addRule(2, 15, 10);
            addRule(3, 15, 11);
            addRule(4, 15, 12);
            addRule(5, 15, 1);
            addRule(6, 15, 2);
            addRule(7, 15, 3);
            addRule(8, 15, 4);
            addRule(9, 15, 5);
            addRule(10, 15, 6);
            addRule(11, 15, 7);
            addRule(12, 15, 8);
        }
    }

    private static void createRuleSet_Summer_I_II_U_POZ(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 9);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 2);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 10);
            addRule(3, LAST_DAY, 11);
            addRule(4, LAST_DAY, 12);
            addRule(5, LAST_DAY, 1);
            addRule(6, LAST_DAY, 2);
            addRule(7, LAST_DAY, 3);
            addRule(8, LAST_DAY, 4);
            addRule(9, LAST_DAY, 5);
            addRule(10, LAST_DAY, 6);
            addRuleExc(10, 15, 6, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 10);
            addRule(3, LAST_DAY, 11);
            addRule(4, LAST_DAY, 12);
            addRule(5, LAST_DAY, 1);
            addRule(6, LAST_DAY, 2);
            addRule(7, LAST_DAY, 3);
            addRule(8, LAST_DAY, 4);
            addRule(9, LAST_DAY, 5);
            addRule(10, LAST_DAY, 6);
            addRule(11, LAST_DAY, 7);
            addRule(12, LAST_DAY, 8);
        }
    }

    private static void createRuleSet_Summer_I_II_U_WRO(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 9);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 2);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 10);
            addRule(3, LAST_DAY, 11);
            addRule(4, LAST_DAY, 12);
            addRule(5, LAST_DAY, 1);
            addRule(6, LAST_DAY, 2);
            addRule(7, LAST_DAY, 3);
            addRule(8, LAST_DAY, 4);
            addRule(9, LAST_DAY, 5);
            addRule(10, LAST_DAY, 6);
            addRuleExc(10, 15, 6, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 9);
            addRule(2, LAST_DAY, 10);
            addRule(3, LAST_DAY, 11);
            addRule(4, LAST_DAY, 12);
            addRule(5, LAST_DAY, 1);
            addRule(6, LAST_DAY, 2);
            addRule(7, LAST_DAY, 3);
            addRule(8, LAST_DAY, 4);
            addRule(9, LAST_DAY, 5);
            addRule(10, LAST_DAY, 6);
            addRule(11, LAST_DAY, 7);
            addRule(12, LAST_DAY, 8);
        }
    }



    /* --------------------------------------- WINTER --------------------------------------- */

    private static void createRuleSet_Winter_I_II_U_GDA(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 3);
        } else if (instPerYear == 2) {
            addRule(1, 15, 3);
            addRule(2, 15, 9);
        } else if (instPerYear == 10) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRule(6, 15, 9);
            addRule(7, 15, 10);
            addRule(8, 15, 11);
            addRule(9, 15, 12);
            addRule(10, 15, 1);
        } else if (instPerYear == 12) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRule(6, 15, 8);
            addRule(7, 15, 9);
            addRule(8, 15, 10);
            addRule(9, 15, 11);
            addRule(10, 15, 12);
            addRule(11, 15, 1);
            addRule(12, 15, 2);
        }
    }

    private static void createRuleSet_Winter_I_II_U_TOR(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 3);
        } else if (instPerYear == 2) {
            addRule(1, 15, 3);
            addRule(2, 15, 9);
        } else if (instPerYear == 10) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRule(6, 15, 9);
            addRule(7, 15, 10);
            addRule(8, 15, 11);
            addRule(9, 15, 12);
            addRule(10, 15, 1);
        } else if (instPerYear == 12) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRule(6, 15, 8);
            addRule(7, 15, 9);
            addRule(8, 15, 10);
            addRule(9, 15, 11);
            addRule(10, 15, 12);
            addRule(11, 15, 1);
            addRule(12, 15, 2);
        }
    }

    private static void createRuleSet_Winter_I_II_U_POZ(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 3);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 9);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 4);
            addRule(3, LAST_DAY, 5);
            addRule(4, LAST_DAY, 6);
            addRule(5, LAST_DAY, 7);
            addRuleExc(5, 15, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_ENGI);
            addRule(6, LAST_DAY, 9);
            addRule(7, LAST_DAY, 10);
            addRule(8, LAST_DAY, 11);
            addRule(9, LAST_DAY, 12);
            addRule(10, LAST_DAY, 1);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 4);
            addRule(3, LAST_DAY, 5);
            addRule(4, LAST_DAY, 6);
            addRule(5, LAST_DAY, 7);
            addRule(6, LAST_DAY, 8);
            addRule(7, LAST_DAY, 9);
            addRule(8, LAST_DAY, 10);
            addRule(9, LAST_DAY, 11);
            addRule(10, LAST_DAY, 12);
            addRule(11, LAST_DAY, 1);
            addRule(12, LAST_DAY, 2);
        }
    }

    private static void createRuleSet_Winter_I_II_U_WRO(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, LAST_DAY, 3);
        } else if (instPerYear == 2) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 9);
        } else if (instPerYear == 10) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 4);
            addRule(3, LAST_DAY, 5);
            addRule(4, LAST_DAY, 6);
            addRule(5, LAST_DAY, 7);
            addRuleExc(5, 15, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_ENGI);
            addRule(6, LAST_DAY, 9);
            addRule(7, LAST_DAY, 10);
            addRule(8, LAST_DAY, 11);
            addRule(9, LAST_DAY, 12);
            addRule(10, LAST_DAY, 1);
        } else if (instPerYear == 12) {
            addRule(1, LAST_DAY, 3);
            addRule(2, LAST_DAY, 4);
            addRule(3, LAST_DAY, 5);
            addRule(4, LAST_DAY, 6);
            addRule(5, LAST_DAY, 7);
            addRule(6, LAST_DAY, 8);
            addRule(7, LAST_DAY, 9);
            addRule(8, LAST_DAY, 10);
            addRule(9, LAST_DAY, 11);
            addRule(10, LAST_DAY, 12);
            addRule(11, LAST_DAY, 1);
            addRule(12, LAST_DAY, 2);
        }
    }







    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ CONFIG PG ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static void createRuleSet_Summer_PG_GDA(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRuleExc(5, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_PG_TOR(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRuleExc(5, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_PG_POZ(Integer instPerYear) {
        if (instPerYear == 1) { // this is for one-time whole study installment
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRuleExc(5, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_PG_WRO(Integer instPerYear, Integer numOfSemesters) {
        if (instPerYear == 1) { // this is for one-time whole study installment
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRuleExc(5, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            if (numOfSemesters == 2) {
                addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
            }
            else {
                addRuleExc(10, 15, 9, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
            }
        }
    }



    /* --------------------------------------- WINTER --------------------------------------- */

    private static void createRuleSet_Winter_PG_GDA(Integer instPerYear) {}

    private static void createRuleSet_Winter_PG_TOR(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 3);
        } else if (instPerYear == 2) {
            addRule(1, 15, 3);
            addRule(2, 15, 10);
        } else if (instPerYear == 10) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRuleExc(5, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 10);
            addRule(7, 15, 11);
            addRule(8, 15, 12);
            addRule(9, 15, 1);
            addRule(10, 15, 2);
            addRuleExc(10, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 4);
            addRule(2, 15, 5);
            addRule(3, 15, 6);
            addRule(4, 15, 7);
            addRule(5, 15, 8);
            addRule(6, 15, 9);
            addRule(7, 15, 10);
            addRule(8, 15, 11);
            addRule(9, 15, 12);
            addRule(10, 15, 1);
            addRule(11, 15, 2);
            addRule(12, 15, 3);
        }
    }

    private static void createRuleSet_Winter_PG_POZ(Integer instPerYear) {
        if (instPerYear == 1) { // this is for one-time whole study installment
            addRule(1, 15, 3);
        } else if (instPerYear == 2) {
            addRule(1, 15, 3);
            addRule(2, 15, 10);
        } else if (instPerYear == 10) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRuleExc(5, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR_3SEM);
            addRule(6, 15, 10);
            addRule(7, 15, 11);
            addRule(8, 15, 12);
            addRule(9, 15, 1);
            addRule(10, 15, 2);
            addRuleExc(10, 1, 2, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 3);
            addRule(2, 15, 4);
            addRule(3, 15, 5);
            addRule(4, 15, 6);
            addRule(5, 15, 7);
            addRule(6, 15, 8);
            addRule(7, 15, 9);
            addRule(8, 15, 10);
            addRule(9, 15, 11);
            addRule(10, 15, 12);
            addRule(11, 15, 1);
            addRule(12, 15, 2);
            //addRule(12, 15, 3);
        }
    }

    private static void createRuleSet_Winter_PG_WRO(Integer instPerYear) {}









    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ CONFIG MBA ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private static void createRuleSet_Summer_MBA_GDA(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_MBA_TOR(Integer instPerYear) {
        if (instPerYear == 1) {
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_MBA_POZ(Integer instPerYear) {
        if (instPerYear == 1) { // this is together for 1-inst per year and one-time whole study installment
            addRule(1, 15, 10);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRule(2, 15, 3);
        } else if (instPerYear == 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        } else if (instPerYear == 12 || instPerYear == 11 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRule(11, 15, 8);
            addRule(12, 15, 9);
        }
    }

    private static void createRuleSet_Summer_MBA_WRO(Integer instPerYear) {
        if (instPerYear == 1) { // this is together for 1-inst per year and one-time whole study installment
            addRule(1, 15, 10);
            addRuleExc(1, 31, 10, PaymentScheduleDeadlineDesignator.RuleExceptionType.FIRST_YEAR);
        } else if (instPerYear == 2) {
            addRule(1, 15, 10);
            addRuleExc(1, 31, 10, PaymentScheduleDeadlineDesignator.RuleExceptionType.FIRST_YEAR);
            addRule(2, 15, 3);
        } else if (instPerYear == 10 || instPerYear > 2 && instPerYear < 10) {
            addRule(1, 15, 10);
            addRuleExc(1, 31, 10, PaymentScheduleDeadlineDesignator.RuleExceptionType.FIRST_YEAR);
            addRule(2, 15, 11);
            addRule(3, 15, 12);
            addRule(4, 15, 1);
            addRule(5, 15, 2);
            addRule(6, 15, 3);
            addRule(7, 15, 4);
            addRule(8, 15, 5);
            addRule(9, 15, 6);
            addRule(10, 15, 7);
            addRuleExc(10, 1, 7, PaymentScheduleDeadlineDesignator.RuleExceptionType.LAST_YEAR);
        }
    }

}