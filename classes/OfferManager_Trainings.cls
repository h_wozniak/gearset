/**
* @author       Wojciech Słodziak
* @description  Class for storing methods related to Offer__c object for Training Record Types
**/


public without sharing class OfferManager_Trainings {


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ NAME GENERATION ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // set Code_Helper_Num__c and Name for training offers
    public static void setCodeHelperNameAndNameForTrainingOffers(List<Offer__c> trainingsToSetCode) {
        //get last code
        List<Offer__c> lastOffer = [
                SELECT Id, Code_Helper_Num__c
                FROM Offer__c
                WHERE Code_Helper_Num__c != null
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER)
                ORDER BY Code_Helper_Num__c DESC LIMIT 1
        ];

        Decimal currentLastNum = (lastOffer.size() > 0)? lastOffer[0].Code_Helper_Num__c : 0;
        for (Offer__c tr : trainingsToSetCode) {
            if (tr.Name != tr.TECH_Code_Name__c && tr.TECH_Code_Name__c != null) {
                tr.addError(Label.error_msg_cantChangeName + tr.TECH_Code_Name__c);
            }

            tr.Code_Helper_Num__c = ++currentLastNum;
            tr.Name = 'S-' + String.valueOf(tr.Code_Helper_Num__c).leftPad(5).replaceAll(' ', '0');
            tr.TECH_Code_Name__c = tr.Name;
        }
    }

    public static void setCodeHelperNameAndNameForSchedules(List<Offer__c> trainingOfferToSetCodeForSchedules) {
        Set<Id> trainingOffersIds = new Set<Id>();
        for (Offer__c trainingOfferToSetCodeForSchedule : trainingOfferToSetCodeForSchedules) {
            trainingOffersIds.add(trainingOfferToSetCodeForSchedule.Training_Offer_from_Schedule__c);
        }

        Map<Id, Offer__c> trainingList = new Map<Id, Offer__c>([
                SELECT Id, Name,
                (SELECT Id, Name, Code_Helper_Num__c
                FROM Schedules__r
                WHERE (RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE)
                OR RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE))
                ORDER BY Code_Helper_Num__c DESC NULLS LAST
                )
                FROM Offer__c
                WHERE Id IN :trainingOffersIds
        ]);

        for (Id tOfferId : trainingList.keySet()) {
            Decimal maxCode = 0;

            for (Offer__c schedule : trainingList.get(tOfferId).Schedules__r) {
                if (schedule.Code_Helper_Num__c != null && schedule.Code_Helper_Num__c > maxCode) {
                    maxCode = schedule.Code_Helper_Num__c;
                }
            }

            for (Offer__c schedule : trainingOfferToSetCodeForSchedules) {
                if (schedule.Name != schedule.TECH_Code_Name__c && schedule.TECH_Code_Name__c != null) {
                    schedule.addError(Label.error_msg_cantChangeName + schedule.TECH_Code_Name__c);
                }

                schedule.Code_Helper_Num__c = ++maxCode;
                schedule.Name = 'T' + trainingList.get(tOfferId).Name + '-' + String.valueOf(schedule.Code_Helper_Num__c).leftPad(3).replaceAll(' ', '0');
                schedule.TECH_Code_Name__c = schedule.Name;
            }
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ VARIOUS METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Beniamin Cholewa */
    // update confirmed and potential candidates
    public static void updateCandidateStatisticsOnOffer(Set<Id> offersToUpdateStatistics) {
        List<Offer__c> parentOffers = [
                SELECT Id, Potential_Candidates__c, Confirmed_Candidates__c
                FROM Offer__c
                WHERE Id IN :offersToUpdateStatistics
                FOR UPDATE
        ];

        List<Enrollment__c> childEnrollments = [
                SELECT Id, Status__c, Training_Offer_for_Participant__c
                FROM Enrollment__c
                WHERE Training_Offer_for_Participant__c in :offersToUpdateStatistics
        ];

        Map<Id, Enrollment__c> childEnrollmentsMap = new Map<Id, Enrollment__c>();
        Map<Id, Set<Id>> enrollmentIdsByOfferMap = new Map<Id, Set<Id>>();

        for (Enrollment__c enrollment : childEnrollments) {
            childEnrollmentsMap.put(enrollment.Id, enrollment);
        }

        for (Id enrollmentId : childEnrollmentsMap.keySet()) {
            Set<Id> enrollmentIds;
            Enrollment__c enrollment = childEnrollmentsMap.get(enrollmentId);
            if (enrollmentIdsByOfferMap.containsKey(enrollment.Training_Offer_for_Participant__c)) {
                enrollmentIds = enrollmentIdsByOfferMap.get(enrollment.Training_Offer_for_Participant__c);
            } else {
                enrollmentIds = new Set<Id>();
            }
            enrollmentIds.add(enrollmentId);
            enrollmentIdsByOfferMap.put(enrollment.Training_Offer_for_Participant__c, enrollmentIds);
        }

        for (Offer__c offer : parentOffers) {
            Id offerId = offer.Id;

            offer.Confirmed_Candidates__c = 0;
            offer.Potential_Candidates__c = 0;
            if (enrollmentIdsByOfferMap.containsKey(offerId)) {
                for (Id enrollmentId : enrollmentIdsByOfferMap.get(offerId)) {
                    Enrollment__c enrollment = childEnrollmentsMap.get(enrollmentId);
                    if (enrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED ||
                            enrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_LAUNCHING_GROUP) {
                        offer.Confirmed_Candidates__c++;
                    }
                    if (enrollment.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION &&
                            enrollment.Status__C != CommonUtility.ENROLLMENT_STATUS_GROUP_LACK_OF_SEATS) {
                        offer.Potential_Candidates__c++;
                    }
                }
            }
        }

        try {
            update parentOffers;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- VALIDATIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Mateusz Pruszyński */
    // does not allow training offers deletion with related active training schedules
    public static void disableTrainingOffersDeletionWithActiveSchedule(List<Offer__c> trainingOffersToDisallowDeletion) {
        Set<Id> deletedActiveTrainingOffersIds = new Set<Id>();
        for (Offer__c e : trainingOffersToDisallowDeletion) {
            deletedActiveTrainingOffersIds.add(e.Id);
        }

        List<Offer__c> relatedTrainingSchedules = [
                SELECT Id, Training_Offer_from_Schedule__c
                FROM Offer__c
                WHERE Training_Offer_from_Schedule__c in :deletedActiveTrainingOffersIds
                AND Active__c = true
        ];

        if (!relatedTrainingSchedules.isEmpty()) {
            Map<Offer__c, List<Offer__c>> activeTrainingOffersMap = new Map<Offer__c, List<Offer__c>>();
            for (Offer__c trainingOffer : trainingOffersToDisallowDeletion) {
                List<Offer__c> relatedTSs = new List<Offer__c>();
                activeTrainingOffersMap.put(trainingOffer, relatedTSs);
                for (Offer__c relatedTS : relatedTrainingSchedules) {
                    if (trainingOffer.Id == relatedTS.Training_Offer_from_Schedule__c) {
                        List<Offer__c> rTSs = activeTrainingOffersMap.get(trainingOffer);
                        rTSs.add(relatedTS);
                    }
                }
            }

            if (activeTrainingOffersMap.size() > 0) {
                for (Offer__c o : activeTrainingOffersMap.keySet()) {
                    List<Offer__c> rTS = activeTrainingOffersMap.get(o);
                    if (rTS.size() > 0) {
                        if(!CommonUtility.isLightningEnabled()) {
                            o.addError('<b>' + Label.msg_errorCannotDeleteActiveSchedules + '</b>', false);
                        } else {
                            o.addError(Label.msg_errorCannotDeleteActiveSchedules, false);
                        }
                    }
                }
            }
        }
    }

    /* Beniamin Cholewa */
    // Offer name duplicate detection
    public static void duplicateNameOfferDetection(List<Offer__c> offersToCheckDuplicates) {
        Set<String> nameList = new Set<String>();
        for (Offer__c o : offersToCheckDuplicates) {
            nameList.add(o.Name);
        }

        List<Offer__c> potentialDuplicates = [
                SELECT Id, Name
                FROM Offer__c
                WHERE Name IN :nameList
                FOR UPDATE
        ];

        for (Offer__c o : offersToCheckDuplicates) {
            for (Offer__c dup : potentialDuplicates) {
                if (o.Id != dup.Id && o.Name == dup.Name) {
                    if(!CommonUtility.isLightningEnabled()) {
                        o.addError(' ' + Label.msg_error_DupFound + ': <a target="_blank" href="/' + dup.Id + '">' + dup.Name + '</a>', false);
                    } else {
                        o.addError(' ' + Label.msg_error_DupFound + ': ' + dup.Id + ' - ' + dup.Name, false);
                    }
                }
            }
        }
    }

}