/**
 * Created by martyna.stanczuk on 2018-05-23.
 */

public with sharing class EmailManager_SchoolVisitEmailReminder {

    @TestVisible private static final String WORKFLOW_TRIGGER_NAME = 'VISITATOR_VISIT_REMINDER';
    @TestVisible private static final String VISIT_REMINDER_DEVELOPER_NAME = 'Mail_do_wizytatora_o_terminie';

    public static void sendVisitatorSchoolVisitReminder(Set<Id> eventIdList) {
        List<Event> schoolVisitEventList = [
                SELECT Id, WhatId, WhoId
                FROM Event
                WHERE Id IN :eventIdList
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)
        ];

        EventManager.rewriteVisitatorEmail(schoolVisitEventList);
        try {
            update schoolVisitEventList;

        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

        Set<Id> visitatorIds = new Set<Id>();
        for (Event schoolVisit : schoolVisitEventList) {
            visitatorIds.add(schoolVisit.WhoId);
        }

        List<EmailTemplate> emailTemplates = [
                SELECT Id, Name
                FROM EmailTemplate
                WHERE DeveloperName = :VISIT_REMINDER_DEVELOPER_NAME
        ];

        EmailTemplate emailTemplate = emailTemplates.get(0);

        List<Task> tasksToInsert = new List<Task>();
        List<Event> eventsWithFailure = new List<Event>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();

        for (Event visit : schoolVisitEventList) {
            if (WORKFLOW_TRIGGER_NAME != null) {
                recordIdToWorkflowTriggerName.put(visit.Id,  WORKFLOW_TRIGGER_NAME);

                String emailTemplateId = emailTemplate.Id;

                tasksToInsert.add(EmailManager.createEmailTask(
                        Label.title_EventDocListSentUnr,
                        visit.WhatId,
                        visit.WhoId,
                        EmailManager.prepareFakeEmailBody(emailTemplateId, visit.WhoId),
                        null
                ));
            } else {
                eventsWithFailure.add(visit);
            }
        }

        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }

        if (!eventsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_DocumentListReminderEmail.class.getName()
                    + ' ' + JSON.serialize(eventsWithFailure));
        }
    }
}