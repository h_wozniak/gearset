/**
*   @author         Sebastian Łasisz
*   @description    Utility class, that is used to show additional data on enrollment study page layout
**/

public class EnrollmentSynchronizationController {
    
    public String enrollmentSychStatus { get; set; }
    public String backgroundColor { get; set;}
    
    public Id enrollmentId { get; set; }
    public Enrollment__c enrollment { get; set; }

    public Double tdWidth { get; set; }
    public Double unenrTdWidth { get; set; }
    
    public EnrollmentSynchronizationController (ApexPages.StandardController controller) {
        enrollmentId = controller.getRecord().Id;
        
        enrollment = [SELECT Id, Synchronization_Status__c FROM Enrollment__c WHERE Id = :enrollmentId];
        
        if (enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_NEW || enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_MODIFIED) {
            backgroundColor = '';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_INPROGRESS) {
            backgroundColor = '#D7F26D';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_FINISHED) {
            backgroundColor = '#52A849';
        }
        else {
            backgroundColor = '#D84141';
        }
        
        enrollment = [SELECT Id, ToLabel(Synchronization_Status__c) FROM Enrollment__c WHERE Id = :enrollmentId];
        enrollmentSychStatus = enrollment.Synchronization_Status__c;
    }
}