global class EncryptedDocumentGeneration implements Database.Batchable<sobject>, Database.AllowsCallouts
{
    private Set<Id> enrollmentIds;

    global EncryptedDocumentGeneration(Set<Id> enrollments){
        enrollmentIds = enrollments;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator (
            [SELECT Id, RecordTypeId, CreatedDate, Language_of_Main_Enrollment__c, Reference_Number__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.TECH_Is_Encrypted__c,
             Enrollment_from_Documents__r.Status__c, Enrollment_from_Documents__r.University_Name__c, Document__r.Name, Document__r.Id, Degree__c
             FROM Enrollment__c
             WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
             AND (Document__r.RecordTypeId =: CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT)
             OR Document__r.Name = 'Kwestionariusz osobowy z podaniem'
             OR Document__r.Name = '[PL] Kwestionariusz osobowy z podaniem')
             AND (not Enrollment_from_Documents__r.Status__c IN ('Signed contract', 'Resignation', 'Payments in KS'))
             AND Enrollment_from_Documents__c in : enrollmentIds
            ]);
    }

    global void execute(Database.BatchableContext bc, List<Enrollment__c> scope){
        List<Enrollment__c> basicEnrollments = new List<Enrollment__c>();
        List<Attachment> attachments = updateAttachmentName(scope);

        for (Enrollment__c enrollment: scope) {
            String documentName = getDocumentName(enrollment.Document__r.Name);
            String degree;
            if (documentName.contains('Kwestionariusz') || documentName.contains('lubowani')) {
                degree = null;
            }
            else {
                degree = enrollment.Degree__c;
            }

            DocumentGeneratorManager.generateDocumentSync(
                    enrollment.Language_of_Main_Enrollment__c,
                    documentName,
                    degree,
                    enrollment.Enrollment_from_Documents__r.University_Name__c,
                    enrollment.Reference_Number__c,
                    enrollment.Id,
                    true,
                    true
            );

            Enrollment__c studyEnrollment = new Enrollment__c();
            studyEnrollment.Id = enrollment.Enrollment_from_Documents__c;
            studyEnrollment.TECH_Is_Encrypted__c = true;
            basicEnrollments.add(studyEnrollment);
        }

        update attachments;
        update basicEnrollments;
    }

    global void finish(Database.BatchableContext BC) {
        List<Enrollment__c> docEnr = [
                SELECT Id, RecordTypeId, CreatedDate, Language_of_Main_Enrollment__c, Reference_Number__c, Enrollment_from_documents__c,
                        Enrollment_from_Documents__r.TECH_Is_Encrypted__c,
                        Enrollment_from_Documents__r.Status__c, Enrollment_from_Documents__r.University_Name__c, Document__r.Name, Document__r.Id, Degree__c
                FROM Enrollment__c
                WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
                AND (Document__r.RecordTypeId =: CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT)
                OR Document__r.Name = 'Kwestionariusz osobowy z podaniem'
                OR Document__r.Name = '[PL] Kwestionariusz osobowy z podaniem')
                AND (not Enrollment_from_Documents__r.Status__c IN ('Signed contract', 'Resignation', 'Payments in KS'))
                AND Enrollment_from_Documents__c in :enrollmentIds
        ];

        Set<Id> docEnrIds = new Set<Id>();
        for (Enrollment__c enrollment: docEnr) {
            docEnrIds.add(enrollment.Id);
        }

        List<Attachment> attachmentOnDoc = [
                Select Id, Name, ParentId
                From Attachment
                Where ParentId IN :docEnrIds
                AND Name LIKE '%[nieszyfrowany]%'
                ORDER BY CreatedDate ASC
        ];

        for (Enrollment__c doc : docEnr) {
            for (Attachment att : attachmentOnDoc) {
                if (att.ParentId == doc.Id) {
                    doc.Current_File__c = att.Id;
                    doc.Current_File_Name__c = att.Name;
                    break;
                }
            }
        }

        update docEnr;
    }

    private String getDocumentName(String name) {
        string documentName = name;
        if(documentName.contains('[PL] ')) {
            documentName = documentName.replace('[PL] ', '');
        }
        return documentName;
    }

    private List<Attachment> updateAttachmentName(List<Enrollment__c> scope) {
        Set<Id> documentIds = new Set<Id>();
        for(Enrollment__c enrollment: scope) {
            documentIds.add(enrollment.Id);
        }

        List<Attachment> attachments = [Select Id, Name From Attachment Where ParentId in : documentIds];

        for (Attachment attachment: attachments) {
            attachment.Name = '[' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + ']' + attachment.Name;
        }
        return attachments;
    }
}