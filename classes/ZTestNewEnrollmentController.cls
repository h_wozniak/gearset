@isTest
private class ZTestNewEnrollmentController {
    //----------------------------------------------------------------------------------
    //NewEnrollmentController
    //----------------------------------------------------------------------------------
    @isTest static void testNewEnrollmentController() {
        PageReference pageRef = Page.ContactStageBar;
        Test.setCurrentPageReference(pageRef);
        
        Contact c = ZDataTestUtility.createCandidateContact(new Contact(
            AccountId = ZDataTestUtility.createCompany(new Account(
                NIP__c = '3672620147'
            ), true).Id
        ), true);

        Enrollment__c e = new Enrollment__c(Participant__c = c.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.standardController(e);
        NewEnrollmentController cntrl = new NewEnrollmentController(stdController);
        Test.stopTest();

        System.assertNotEquals(null, cntrl.next());

        cntrl.back();
    }
}