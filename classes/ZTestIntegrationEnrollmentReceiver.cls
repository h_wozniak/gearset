@isTest
global class ZTestIntegrationEnrollmentReceiver {

    @isTest static void test_createEnrollment() {
        Offer__c off = ZDataTestUtility.createCourseSpecialtyOffer(null, true);

        Offer__c product = [SELECT Id, Name, University_Name__c, Course_Offer_from_Specialty__c FROM Offer__c WHERE Id = :off.Id];
        System.assert(product != null);

        String body = '{"updating_system":"ZPI","updating_system_contact_id":null,"first_name":"John","middle_name":"string","last_name":"Smith","family_name":"Smith","father_name":"David","mother_name":"Mom","gender":"Male","personal_id":"91013103211","birthdate":"1991-01-31","place_of_birth":"Poland","foreigner":false,"country_of_origin":"Poland","nationality":"polish","residence_document":{"type":"Polish Card","validity_date":"2017-10-10"},"identity_document":{"type":"Identity Card","identity_document_number":"ACT956616","issue_country":"Poland"},"health_insurance_validity_date":"2030-10-10","teb_graduate":true,"career":[{"company_name":"Enxoo","position":"Software Engineer","date_start":"2015-10-10","date_end":"2020-10-10","address":{"street":"Ruska 3","city":"Wrocław","postal_code":"50-243","country":"Poland"}}],"addresses":[{"type":"Other","country":"Poland","postal_code":"27-200","state":"Świętokrzyskie","city":"Starachowice","street":"Lelewela"}],"phones":[{"type":"Default","phoneNumber":"123456789"}],"email":"sedi1ster@hotmail.com","enrollment_data":{"enrollment_id_zpi":"string","product_name":"'+ product.Name + '","specialization_name":null,"for_next_semester":true,"next_product":true,"tuition_system":"Graded tuition","number_of_installments":2,"discount_code":null,"recommending_person":{"first_name":"Sebastian","last_name":"Łasisz"},"company_for_discount":{"name":"Enxoo","tax_id":"2793226325"},"selected_languages":[{"code":"German","level":"Basic"}],"invoice":{"company_name":"Enxoo","tax_id":"2793226325","street":"Dojazdowa","city":"Kraków","postal_code":"12-123","country":"Poland","remarks":null},"university":{"name":"WSB Wrocław","city":"Wrocław","finished_course":"Archeology","graduation_year":"2017","diploma":{"status":"Before defense","examination_date":"2017-02-08","diploma_number":"12345679","issue_country":"Poland","issue_city":"Wrocław","issue_date":"2017-02-10","title":"Bachelor"}},"secondary_school":{"name":"1st High School","type":null,"city":"Wrocław","a_level_status":"Passed","a_level_id":"asb","a_level_issue_date":"2010-02-02","examination_committee_name":"Regional Examination Commission in Gdańsk","certificate_city":"Wrocłąw","certificate_country":"Poland","graduation_year":"2010-05-05","school_certificate_with_honors":true}}}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationEnrollmentReceiver.receiveEnrollmentInformation();
        }
        catch (Exception e) {
        	System.assertEquals(e.getMessage(), '');
        }
        Test.stopTest();
    }
}