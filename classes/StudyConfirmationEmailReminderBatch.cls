global class StudyConfirmationEmailReminderBatch implements Database.Batchable<sObject> {
    
    Datetime dayOfEnrollmentFrom;
    Datetime dayOfEnrollmentTo;

    global StudyConfirmationEmailReminderBatch(Integer daysAfterEnrollment) {
        Date dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
        dayOfEnrollmentFrom = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 0, 0, 0);
        dayOfEnrollmentTo = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 23, 59, 59);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id 
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY) 
            AND CreatedDate >= :dayOfEnrollmentFrom AND CreatedDate <= :dayOfEnrollmentTo
            AND VerificationDate__c = null AND Enrollment_Source__c = :CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI
            AND Status__c = :CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
            AND VerificationLinkEmailedCount__c < 3
            AND Dont_send_automatic_emails__c = false
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrIds = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrIds.add(enr.Id);
        }

        EmailManager.sendVerificationLink(enrIds);
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}