@isTest
private class ZTestMainCampaignMembersController {

    private static void prepareData() {
        List<Contact> contacts = ZDataTestUtility.createCandidateContacts(null, 20, true);
        Catalog__c con1 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1,
                Consent_ADO_API_Name__c = 'Consent_Marketing_ADO__c',
                Consent_API_Name__c = 'Consent_Marketing__c'
        ), true);
        Catalog__c con2 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_2,
                Consent_ADO_API_Name__c = 'Consent_Electronic_Communication_ADO__c',
                Consent_API_Name__c = 'Consent_Electronic_Communication__c'
        ), true);
        Catalog__c con3 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3,
                Consent_ADO_API_Name__c = 'Consent_PUCA_ADO__c',
                Consent_API_Name__c = 'Consent_PUCA__c'
        ), true);
        Catalog__c con4 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_4,
                Consent_ADO_API_Name__c = 'Consent_Direct_Communications_ADO__c',
                Consent_API_Name__c = 'Consent_Direct_Communications__c'
        ), true);
        Catalog__c con5 = ZDataTestUtility.createBaseConsent(
            new Catalog__c(Name = RecordVals.MARKETING_CONSENT_5,
                Consent_ADO_API_Name__c = 'Consent_Graduates_ADO__c',
                Consent_API_Name__c = 'Consent_Graduates__c'
        ), true);

        for (Contact contact : contacts) {
            contact.Consent_Direct_Communications__c = true;
            contact.Consent_Direct_Communications_ADO__c = CommonUtility.MARKETING_ENTITY_WROCLAW;
            contact.Consent_Electronic_Communication__c = true;
            contact.Consent_Electronic_Communication_ADO__c = CommonUtility.MARKETING_ENTITY_WROCLAW;
            contact.Consent_Graduates__c = true;
            contact.Consent_Graduates_ADO__c = CommonUtility.MARKETING_ENTITY_WROCLAW;
            contact.Consent_Marketing__c = true;
            contact.Consent_Marketing_ADO__c = CommonUtility.MARKETING_ENTITY_WROCLAW;
        }

        update contacts;
    }

    @isTest static void test_MainCampaignMembersController_withoutConsents() {
        Test.startTest();
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        List<Contact> contacts = ZDataTestUtility.createCandidateContacts(null, 20, true);

        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        Test.stopTest();

        System.assertEquals(cntrl.contactsToShow.size(), 0);
    }

    @isTest static void test_MainCampaignMembersController_properData() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        Test.stopTest();

        System.assertEquals(cntrl.contactsToShow.size(), 10);
    }

    @isTest static void test_beggining() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.beginning();
        test.stopTest();
    }

    @isTest static void test_next() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.next();
        test.stopTest();
    }

    @isTest static void test_previous() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        try {
            cntrl.previous();
        }
        catch (Exception e) {}
        test.stopTest();
    }

    @isTest static void test_last() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        try {
            cntrl.last();
        }
        catch (Exception e) {}
        test.stopTest();
    }

    @isTest static void test_addFilterWrapper() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.addFilterWrapper();
        test.stopTest();
    }

    @isTest static void test_deleteLogic() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        try {
            cntrl.deleteLogic();
        }
        catch (Exception e) {}
        test.stopTest();
    }

    @isTest static void test_selectAllContacts() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.selectAllContacts();
        test.stopTest();
    }

    @isTest static void test_prepareOperatorOptionList() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.prepareOperatorOptionList();
        test.stopTest();
    }

    @isTest static void test_prepareOperatorOptionStringList() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.prepareOperatorOptionStringList();
        test.stopTest();
    }

    @isTest static void test_prepareOperatorOptionDateList() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.prepareOperatorOptionDateList();
        test.stopTest();
    }

    @isTest static void test_prepareOperatorOptionBooleanList() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.prepareOperatorOptionBooleanList();
        test.stopTest();
    }
    
    

    @isTest static void test_getAvailableFields() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.getAvailableFields('Contact', true);
        test.stopTest();
    }

    @isTest static void test_sortSelectOptionsByLabel() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        List<SelectOption> operators = cntrl.prepareOperatorOptionBooleanList();
        cntrl.sortSelectOptionsByLabel(operators);
        test.stopTest();
    }

    @isTest static void test_refreshValueField() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        try {
            cntrl.refreshValueField();
        }
        catch (Exception e) {}
        test.stopTest();
    }

    @isTest static void test_stringOperatorToQueryOperator() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.stringOperatorToQueryOperator(CommonUtility.CMC_OPERATOR_LESS_THAN_OR_EQUAL);
        test.stopTest();
    }

    @isTest static void test_refreshAssigmentLogic() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.refreshAssigmentLogic();
        test.stopTest();
    }

    @isTest static void test_prepareFiltersMap() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.prepareFiltersMap();
        test.stopTest();
    }

    @isTest static void test_parseAssigmentContactsLogic() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.parseAssigmentContactsLogic();
        test.stopTest();
    }

    @isTest static void test_applyLimitSize() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.applyLimitSize();
        test.stopTest();
    }

    @isTest static void test_applyAdvancedLogic() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.applyAdvancedLogic();
        test.stopTest();
    }

    @isTest static void test_getTypeOfField() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.getTypeOfField('Email');
        test.stopTest();
    }

    @isTest static void test_save() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.save();
        test.stopTest();
    }

    @isTest static void test_cancel() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.cancel();
        test.stopTest();
    }

    @isTest static void test_getDisableNext() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.getDisableNext();
        test.stopTest();
    }

    @isTest static void test_getDisablePrevious() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.startTest();

        prepareData(); 
        ApexPages.CurrentPage().getparameters().put('campaignId', salesCampaign.Id);
        ApexPages.CurrentPage().getparameters().put('retURL', salesCampaign.Id);

        MainCampaignMembersController cntrl = new MainCampaignMembersController();
        cntrl.getDisablePrevious();
        test.stopTest();
    }
}