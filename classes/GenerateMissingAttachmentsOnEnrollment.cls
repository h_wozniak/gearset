/**
*   @author         Sebastian Łasisz
*   @description    Class used to finalize study enrollment process. Main purpose is to:
*                   - Insert new language records,
*                   - Create documents with dynamically created attachments
*                   - Deduplicate Contacts.
*                   Class used in both creating new study enrollment and annexing existing enrollment.
**/

public without sharing class GenerateMissingAttachmentsOnEnrollment implements Queueable, Database.AllowsCallouts {
    List<Enrollment__c> enrToCreateInitialDocs;
    List<Enrollment__c> agreementDocuments;
    List<Enrollment__c> oathDocuments;
    List<Enrollment__c> questionareDocuments;
    List<Enrollment__c> languages;
    Set<Id> contactIds;
    Set<Id> enrIds;
    Integer queueableDepth;
    Boolean sendEmail = true;

    public GenerateMissingAttachmentsOnEnrollment(List<Enrollment__c> languages, List<Enrollment__c> enrToCreateInitialDocs, List<Enrollment__c> agreementDocuments, List<Enrollment__c> oathDocuments, List<Enrollment__c> questionareDocuments, Integer queueableDepth) {
        this.languages = languages;
        this.agreementDocuments = agreementDocuments;
        this.oathDocuments = oathDocuments;
        this.questionareDocuments = questionareDocuments;
        this.queueableDepth = queueableDepth;

        this.enrIds = new Set<Id>();
        this.contactIds = new Set<Id>();
        for (Enrollment__c enrToCreateInitialDoc : enrToCreateInitialDocs) {
            enrIds.add(enrToCreateInitialDoc.Id);
            contactIds.add(enrToCreateInitialDoc.Candidate_Student__c);
        }

        List<Enrollment__c> enrToCreateInitialDocsWithFields = [
                SELECT Id, Degree__c, University_Name__c, OwnerId, Language_of_Enrollment__c, Candidate_Student__c
                FROM Enrollment__c
                WHERE Id IN :enrIds
        ];

        this.enrToCreateInitialDocs = enrToCreateInitialDocsWithFields;
    }

    public GenerateMissingAttachmentsOnEnrollment(List<Enrollment__c> enrToCreateInitialDocs, List<Enrollment__c> agreementDocuments, List<Enrollment__c> oathDocuments, List<Enrollment__c> questionareDocuments, Integer queueableDepth, Boolean sendEmail) {
        this.sendEmail = sendEmail;
        this.agreementDocuments = agreementDocuments;
        this.oathDocuments = oathDocuments;
        this.questionareDocuments = questionareDocuments;
        this.queueableDepth = queueableDepth;

        this.enrIds = new Set<Id>();
        this.contactIds = new Set<Id>();
        for (Enrollment__c enrToCreateInitialDoc : enrToCreateInitialDocs) {
            enrIds.add(enrToCreateInitialDoc.Id);
            contactIds.add(enrToCreateInitialDoc.Candidate_Student__c);
        }

        List<Enrollment__c> enrToCreateInitialDocsWithFields = [
                SELECT Id, Degree__c, University_Name__c, OwnerId, Language_of_Enrollment__c, Candidate_Student__c
                FROM Enrollment__c
                WHERE Id IN :enrIds
        ];

        this.enrToCreateInitialDocs = enrToCreateInitialDocsWithFields;
    }

    public void execute(QueueableContext qc) {
        CommonUtility.preventSendingEmailWithDocuments = true;

        if (queueableDepth == -1) {
            if (languages != null) {
                insert languages;
            }

            List<Enrollment__c> agreementDocuments = GenerateMissingAttachmentsOnEnrollment.createAgreementForEnrollment(enrToCreateInitialDocs);
            List<Enrollment__c> oathDocuments = GenerateMissingAttachmentsOnEnrollment.createOathForEnrollment(enrToCreateInitialDocs);
            List<Enrollment__c> questionareDocuments = GenerateMissingAttachmentsOnEnrollment.createQuestionnareDocument(enrToCreateInitialDocs);

            queueableDepth++;
            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 0) {
            if (!agreementDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateAgreementForEnrollment(agreementDocuments.get(0), true);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 1) {
            if (!agreementDocuments.isEmpty()) {
                generateAgreementForEnrollment(agreementDocuments.get(0), false);
            }

            queueableDepth++;

            if (agreementDocuments.size() == 1) queueableDepth += 2;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 2) {
            if (!agreementDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateAgreementForEnrollment(agreementDocuments.get(1), true);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 3) {
            if (!agreementDocuments.isEmpty()) {
                generateAgreementForEnrollment(agreementDocuments.get(1), false);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 4) {
            if (!oathDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateOathForEnrollment(oathDocuments.get(0), true);
            }
            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 5) {
            if (!oathDocuments.isEmpty()) {
                generateOathForEnrollment(oathDocuments.get(0), false);
            }
            queueableDepth++;

            if (oathDocuments.size() == 1) queueableDepth += 2;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 6) {
            if (!oathDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateOathForEnrollment(oathDocuments.get(1), true);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 7) {
            if (!oathDocuments.isEmpty()) {
                generateOathForEnrollment(oathDocuments.get(1), false);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 8) {
            if (!questionareDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateQuestionnareDocument(questionareDocuments.get(0), true);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 9) {
            if (!questionareDocuments.isEmpty()) {
                generateQuestionnareDocument(questionareDocuments.get(0), false);
            }

            queueableDepth++;

            if (questionareDocuments.size() == 1) queueableDepth += 2;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 10) {
            if (!questionareDocuments.isEmpty()) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
                generateQuestionnareDocument(questionareDocuments.get(1), true);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 11) {
            if (!questionareDocuments.isEmpty()) {
                generateQuestionnareDocument(questionareDocuments.get(1), false);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languages, enrToCreateInitialDocs, agreementDocuments, oathDocuments, questionareDocuments, queueableDepth));
            }
        }

        else if (queueableDepth == 12 && sendEmail!=false) {
            if (enrIds != null && !enrIds.isEmpty()) {
                sendEmailWithDocuments(enrIds);
            }

            queueableDepth++;

            if (!Test.isRunningTest()) {
                List<Contact> contactsToCheckDuplicates = [
                        SELECT Id, RecordTypeId, FirstName, LastName, Pesel__c, Phone, MobilePhone, Email, Additional_Emails__c, Additional_Phones__c, Name,
                                Is_Potential_Duplicate__c, Potential_Duplicate__c, Foreigner__c, Confirmed_Contact__c, Account.RecordTypeId, AccountId
                        FROM Contact
                        WHERE Id IN :contactIds
                ];

                if (!contactsToCheckDuplicates.isEmpty()) {
                    System.enqueueJob(new CheckForDuplicatesQueueable(contactsToCheckDuplicates));
                }
            }
        }
    }

    public static void generateAttachmentForDocuments(Set<Id> enrIds) {
        List<Enrollment__c> enrToCreateInitialDocs = [
                SELECT Id, Degree__c, University_Name__c, OwnerId, Language_of_Enrollment__c, Candidate_Student__c
                FROM Enrollment__c
                WHERE Id IN :enrIds
        ];

        System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(null, enrToCreateInitialDocs, null, null, null, -1));
    }

    public static void generateAttachmentForDocuments(List<Enrollment__c> enrToCreateInitialDocs) {
        System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(null, enrToCreateInitialDocs, null, null, null, -1));
    }

    public static void sendEmailWithDocuments(Set<Id> studyEnrIds) {
        List<Enrollment__c> studyEnrRelatedDocs = [
                SELECT Id, Enrollment_from_Documents__r.RequiredDocListEmailedCount__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.Degree__c, Document__c,
                        Enrollment_from_Documents__r.Language_of_Enrollment__c,
                (SELECT Id
                FROM Attachments
                WHERE (NOT(Name LIKE : '%' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + '%'))
                ORDER BY CreatedDate LIMIT 1)
                FROM Enrollment__c
                WHERE Enrollment_from_Documents__c IN :studyEnrIds
                AND (
                        Document__c = :CatalogManager.getDocumentAgreementId()
                        OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                        OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                        OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                        OR Document__c = :CatalogManager.getDocumentOathId()
                        OR Document__c = :CatalogManager.getDocumentOathPLId()
                )
        ];

        Set<Id> enrToSendReqDocList = new Set<Id>();
        for (Id studyEnrId : studyEnrIds) {
            Integer generatedCount = 0;
            Boolean mbaORSp = false;
            String language = '';

            for (Enrollment__c doc : studyEnrRelatedDocs) {
                if (studyEnrId == doc.Enrollment_from_Documents__c) {
                    mbaORSp = (doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_PG || doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_MBA);
                    language = doc.Enrollment_from_Documents__r.Language_of_Enrollment__c;
                    if (!doc.Attachments.isEmpty()) {
                        if (!mbaORSp) {
                            generatedCount++;
                        }
                        else if (mbaORSp && doc.Document__c != CatalogManager.getDocumentOathId()) {
                            generatedCount++;
                        }
                    }
                }
            }

            if (language != CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                if (generatedCount == 3 && !mbaORSp) { // Oath and Agreement and Questionnare Documents
                    enrToSendReqDocList.add(studyEnrId);
                }

                if (generatedCount == 2 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnrId);
                }
            }
            else {
                if (generatedCount == 6 && !mbaORSp) { // Oath and Agreement and Questionnare Documents
                    enrToSendReqDocList.add(studyEnrId);
                }

                if (generatedCount == 4 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnrId);
                }
            }
        }

        if (!enrToSendReqDocList.isEmpty()) {
            EmailManager_DocumentListEmail.sendRequiredDocumentList(enrToSendReqDocList);
        }
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- GENERATING DOCUMENTS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */


    /* Sebastian Łasisz */
    // Generate Agreement Document for new Enrollment with Document Generation
    public static List<Enrollment__c> createAgreementForEnrollment(List<Enrollment__c> enrollmentsToAddAgreement) {
        List<Enrollment__c> docEnrToInsert = new List<Enrollment__c>();
        Id agreementDocumentId = CatalogManager.getDocumentAgreementId();
        Id agreementDocumentPlId = CatalogManager.getDocumentAgreementPlId();

        if (agreementDocumentId != null) {
            for (Enrollment__c enrollmentToAddAgreement : enrollmentsToAddAgreement) {
                docEnrToInsert.add(DocumentManager.createDocumentEnrollment(enrollmentToAddAgreement, agreementDocumentId, CommonUtility.ENROLLMENT_FDI_STAGE_AD));

                if (enrollmentToAddAgreement.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                    docEnrToInsert.add(DocumentManager.createDocumentEnrollment_PL(enrollmentToAddAgreement, agreementDocumentPlId, ''));
                }
            }

            try {
                CommonUtility.skipDocumentValidations = true;
                for (Enrollment__c enrollmentToUpdate : docEnrToInsert) {
                    CommonUtility.skipDocumentValidations = true;
                    enrollmentToUpdate.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                    enrollmentToUpdate.Current_File__c = CommonUtility.DURING_GENERATION;
                }
                insert docEnrToInsert;
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }

        return docEnrToInsert;
    }

    /* Wojciech Słodziak */
    // Generate Oath Document for new Enrollment with Document Generation
    public static List<Enrollment__c> createOathForEnrollment(List<Enrollment__c> enrollmentsToAddOath) {
        List<Enrollment__c> docEnrToInsert = new List<Enrollment__c>();
        Id oathDocumentId = CatalogManager.getDocumentOathId();
        Id oathDocumentPLId = CatalogManager.getDocumentOathPlId();

        if (oathDocumentId != null) {
            for (Enrollment__c enrToAddOath : enrollmentsToAddOath) {
                if (enrToAddOath.Degree__c != CommonUtility.OFFER_DEGREE_MBA && enrToAddOath.Degree__c != CommonUtility.OFFER_DEGREE_PG) {
                    docEnrToInsert.add(DocumentManager.createDocumentEnrollment(enrToAddOath, oathDocumentId, CommonUtility.ENROLLMENT_FDI_STAGE_COD));

                    if (enrToAddOath.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                        docEnrToInsert.add(DocumentManager.createDocumentEnrollment_PL(enrToAddOath, oathDocumentPLId, ''));
                    }
                }
            }

            try {
                CommonUtility.skipDocumentValidations = true;
                for (Enrollment__c enrollmentToUpdate : docEnrToInsert) {
                    CommonUtility.skipDocumentValidations = true;
                    enrollmentToUpdate.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                    enrollmentToUpdate.Current_File__c = CommonUtility.DURING_GENERATION;
                }
                insert docEnrToInsert;
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }

        return docEnrToInsert;
    }

    /* Sebastian Łasisz */
    // on Personal Questionnaire add proper attachment to Document
    public static List<Enrollment__c> createQuestionnareDocument(List<Enrollment__c> enrollmentsToAddQuestionnare) {
        Id questionnaireDocumentId = CatalogManager.getDocumentPersonalQuestionnareId();
        Id questionnaireDocumentPLId = CatalogManager.getPLDocumentPersonalQuestionnareId();

        Set<Id> studyEnrIds = new Set<Id>();
        for (Enrollment__c studyEnr : enrollmentsToAddQuestionnare) {
            studyEnrIds.add(studyEnr.Id);
        }

        List<Enrollment__c> docEnrToInsert = new List<Enrollment__c>();
        for (Enrollment__c enrToAddQuestionnare : enrollmentsToAddQuestionnare) {
            if (enrToAddQuestionnare.Language_of_Enrollment__c == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                docEnrToInsert.add(DocumentManager.createDocumentEnrollment_PL(enrToAddQuestionnare, questionnaireDocumentPLId, ''));
            }
        }

        try {
            insert docEnrToInsert;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

        List<Enrollment__c> docEnrollments = [
                SELECT Id, TECH_University_from_Enrollment_Study__c, Reference_Number__c, Language_of_Main_Enrollment__c, Document__r.Name,
                        Enrollment_from_Documents__r.Language_of_Enrollment__c, Enrollment_from_Documents__r.University_Name__c
                FROM Enrollment__c
                WHERE Enrollment_from_Documents__c IN :studyEnrIds
                AND Document__c = :questionnaireDocumentId
        ];

        if (questionnaireDocumentId != null) {
            for (Enrollment__c docEnr : docEnrollments) {
                docEnr.Reference_Number__c = CustomSettingManager.getCurrentDocumentReferenceNumber();
                docEnr.Current_File__c = CommonUtility.DURING_GENERATION;
                docEnr.University_Name__c = docEnr.Enrollment_from_Documents__r.University_Name__c;
            }

            try {
                CommonUtility.skipDocumentValidations = true;
                update docEnrollments;
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }

        List<Enrollment__c> finalQuestionnareList = new List<Enrollment__c>();
        finalQuestionnareList.addAll(docEnrollments);
        finalQuestionnareList.addAll(docEnrToInsert);

        return finalQuestionnareList;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------- GENERATING DOCUMENT ATTACHMENTS ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */


    /* Sebastian Łasisz */
    // Generate Agreement Document for new Enrollment with Document Generation
    public static void generateAgreementForEnrollment(Enrollment__c docEnr, Boolean encodeWithPassword) {
        Id agreementDocumentId = CatalogManager.getDocumentAgreementId();

        if (agreementDocumentId != null && docEnr.Id != null) {
            try {
                DocumentGeneratorManager.generateDocumentSync(
                        docEnr.Language_of_Enrollment__c,
                        RecordVals.CATALOG_DOCUMENT_AGREEMENT,
                        docEnr.Degree__c,
                        docEnr.University_Name__c,
                        docEnr.Reference_Number__c,
                        docEnr.Id,
                        true,
                        encodeWithPassword
                );
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }
    }

    /* Wojciech Słodziak */
    // Generate Oath Document for new Enrollment with Document Generation
    public static void generateOathForEnrollment(Enrollment__c docEnr, Boolean encodeWithPassword) {
        Id oathDocumentId = CatalogManager.getDocumentOathId();

        if (oathDocumentId != null && docEnr.Id != null) {
            try {
                DocumentGeneratorManager.generateDocumentSync(
                        docEnr.Language_of_Enrollment__c,
                        RecordVals.CATALOG_DOCUMENT_OATH,
                        null,
                        docEnr.University_Name__c,
                        docEnr.Reference_Number__c,
                        docEnr.Id,
                        true,
                        encodeWithPassword
                );
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }
    }

    /* Sebastian Łasisz */
    // on Personal Questionnaire add proper attachment to Document
    public static void generateQuestionnareDocument(Enrollment__c docEnr, Boolean encodeWithPassword) {
        Id questionnaireDocumentId = CatalogManager.getDocumentPersonalQuestionnareId();

        if (questionnaireDocumentId != null && docEnr.Id != null) {
            try {
                DocumentGeneratorManager.generateDocumentSync(
                        docEnr.Language_of_Main_Enrollment__c,
                        RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE,
                        null,
                        docEnr.University_Name__c,
                        docEnr.Reference_Number__c,
                        docEnr.Id,
                        true,
                        encodeWithPassword
                );
            }
            catch(Exception e) {
                ErrorLogger.Log(e);
            }
        }
    }
}