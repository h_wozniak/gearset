/**
*   @author         Wojciech Słodziak
*   @description    Utility class, that is used to store globally used variables. Mostly for Record Names that must exist.
*   @note           All record names from this class should be add to page/ConfigChecker to validate System configuration.   
**/

public without sharing class RecordVals {

    /* Tesowy komentarz */

    /* Contact */
    public static final String UPDATING_SYSTEM_CRM = 'CRM';
    public static final String UPDATING_SYSTEM_EXPERIA = 'Experia';
    public static final String UPDATING_SYSTEM_ZPI = 'ZPI';
    public static final String UPDATING_SYSTEM_MA = 'MA';
    public static final String UPDATING_SYSTEM_FCC = 'FCC';
    
    /* WSB Names */
    public static final String WSB_NAME_POZ = 'WSB Poznań';
    public static final String WSB_NAME_WRO = 'WSB Wrocław';
    public static final String WSB_NAME_TOR = 'WSB Toruń';
    public static final String WSB_NAME_GDA = 'WSB Gdańsk';
    public static final String WSB_NAME_GDY = 'WSB Gdynia';
    public static final String WSB_NAME_BYD = 'WSB Bydgoszcz';
    public static final String WSB_NAME_OPO = 'WSB Opole';
    public static final String WSB_NAME_SZC = 'WSB Szczecin';
    public static final String WSB_NAME_CHO = 'WSB Chorzów';

    /* Marketing */
    public static final String MARKETING_CONSENT_ENR_1 = 'Akceptacja regulaminu';
    public static final String MARKETING_CONSENT_ENR_2 = 'Przetwarzanie danych w celach realizacji umowy';
    public static final String MARKETING_CONSENT_1 = 'Marketing';
    public static final String MARKETING_CONSENT_2 = 'Komunikacja kanałami elektronicznymi';
    public static final String MARKETING_CONSENT_3 = 'Prawnie usp. cel administratora UODO 23.1.5';
    public static final String MARKETING_CONSENT_4 = 'Komunikacja bezpośrednia';
    public static final String MARKETING_CONSENT_5 = 'Śledzenie losów absolwentów';
    public static final String MARKETING_CONSENT_PRE_GDA = 'Preambuła - Gdańsk';
    public static final String MARKETING_CONSENT_PRE_POZ = 'Preambuła - Poznań';
    public static final String MARKETING_CONSENT_PRE_TOR = 'Preambuła - Toruń';
    public static final String MARKETING_CONSENT_PRE_WRO = 'Preambuła - Wrocław';

    /* Documents */
    public static final String CATALOG_TERMS_OF_USE = 'Regulamin';
    public static final String CATALOG_TERMS_OF_USE_PAYMENTS = 'Opłat';
    public static final String CATALOG_TERMS_OF_USE_DISCOUNTS = 'Promocji';
    public static final String CATALOG_TERMS_OF_USE_STUDIES = 'Studiów';
    public static final String CATALOG_DOCUMENT_AGREEMENT = 'Umowa';
    public static final String CATALOG_DOCUMENT_AGREEMENT_PL = '[PL] Umowa';
    public static final String CATALOG_DOCUMENT_AGREEMENT_UNENROLLED = 'Umowa wolny słuchacz';
    public static final String CATALOG_DOCUMENT_AGREEMENT_UNENROLLED_PL = '[PL] Umowa wolny słuchacz';
    public static final String CATALOG_DOCUMENT_ACCEPTANCE = 'Decyzja o przyjęciu';
    public static final String CATALOG_DOCUMENT_ACCEPTANCE_PL = '[PL] Decyzja o przyjęciu';
    public static final String CATALOG_DOCUMENT_OATH = 'Ślubowanie';
    public static final String CATALOG_DOCUMENT_OATH_PL = '[PL] Ślubowanie';
    public static final String CATALOG_DOCUMENT_DIPLOMA = 'Kserokopia dyplomu ukończenia studiów wyższych';
    public static final String CATALOG_DOCUMENT_ATTESTATION_DIPLOMA = 'Zaświadczenie o obronie';
    public static final String CATALOG_DOCUMENT_CERTIFICATE_OF_PROFESSIONAL_EXPERIENCE = 'Zaświadczenie o doświadczeniu zawodowym';
    public static final String CATALOG_DOCUMENT_ENTRY_BUSINESS_REGISTER = 'Wpis do ewidencji działalności gospodarczej';
    public static final String CATALOG_DOCUMENT_ENTRYFEE_PAYCONF = 'Potwierdzenie dowodu wpłaty wpisowego';
    public static final String CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE = 'Kwestionariusz osobowy z podaniem';
    public static final String CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE_PL = '[PL] Kwestionariusz osobowy z podaniem';
    public static final String CATALOG_DOCUMENT_COMPANY_DISCOUNT = 'Dokument uprawniający do promocji firmowej';
    public static final String CATALOG_DOCUMENT_ALEVEL_CERTIFICATE = 'Kserokopia świadectwa maturalnego - oryginał lub odpis dokumentu do wglądu';
    public static final String CATALOG_DOCUMENT_ATTESTATION = 'Zaświadczenie';


    /* Additional Charges */
    public static final String CS_AC_NAME_STUDENTCARD = 'Opłata za legitymację';

}