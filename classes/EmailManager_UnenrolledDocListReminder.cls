/**
* @author       Sebastian Lasisz
* @description  Class to send DocumentList for Unenrolled before end of process - based on process builder.
**/

public with sharing class EmailManager_UnenrolledDocListReminder {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */
    @TestVisible
    private static String emailTemplateName = 'X24_StudyUnenrolledEnrDocList';

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE DESIGNATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String designateWorkflowNameForEnrollment(String language, Enrollment__c studyEnr) {
        return language != null? (language + '_' + emailTemplateName) : null;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HELPER METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static String generatePeriodOfRecrutation(Enrollment__c studyEnr) {
        if (studyEnr.Study_Start_Date__c != null) {
            Date dateToCheck = studyEnr.Study_Start_Date__c;
            Boolean isWinterEnrollment = dateToCheck.month() >= 2 && dateToCheck.month() <= 7;
            return isWinterEnrollment ? CommonUtility.CATALOG_PERIOD_WINTER : CommonUtility.CATALOG_PERIOD_SUMMER;
        }
        return null;
    }

    public static Map<Id, Date> generateUnenrolledAgreementTill(List<Enrollment__c> studyEnrollments) {
        Map<Id, Date> unenrnolledTillEnrMap = new Map<Id, Date>();

        List<Catalog__c> offerDateAssigment = [
                SELECT Id, Period_of_Recrutation__c, Unenrolled_Agreement_Till__c, Entity__c
                FROM Catalog__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_OFFER_DATE_ASSIGMENT)
        ];
		System.debug('before for');
        for (Enrollment__c studyEnr : studyEnrollments) {
            System.debug('in for');
            System.debug('study enr start date ' + studyEnr.Study_Start_Date__c);
            String periodOfRecrutation = generatePeriodOfRecrutation(studyEnr);
            System.debug('period of recrutation ' + periodOfRecrutation);
            if (periodOfRecrutation != null) {
                for (Catalog__c offerAssigment : offerDateAssigment) {
                     System.debug('offerAssigment.Entity__c ' + offerAssigment.Entity__c);
                     System.debug('studyEnr.University_Name__c ' + studyEnr.University_Name__c);
                     System.debug('offerAssigment.Period_of_Recrutation__c ' + offerAssigment.Period_of_Recrutation__c);
                     System.debug('periodOfRecrutation ' + periodOfRecrutation);
                    if (offerAssigment.Entity__c == studyEnr.University_Name__c && offerAssigment.Period_of_Recrutation__c == periodOfRecrutation) {
                        List<String> splittedDateFromRecord = offerAssigment.Unenrolled_Agreement_Till__c.split('\\.');
                        Date draftDate = Date.newInstance(System.today().year(), Integer.valueOf(splittedDateFromRecord[1]), Integer.valueOf(splittedDateFromRecord[0]));
                        draftDate = draftDate.addDays(-4);
                        unenrnolledTillEnrMap.put(studyEnr.Id, draftDate);
                    }
                }
            }
        }

        return unenrnolledTillEnrMap;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ INVOKATION METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method for sending required document list reminder for study Enrollment
    public static void sendDocumentListReminderUnenrolled(Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
                SELECT Id, Language_of_Enrollment__c, Candidate_Student__c
                FROM Enrollment__c
                WHERE Id IN :enrollmentIdList
                AND Candidate_Student__r.Email != null
                AND Dont_send_automatic_emails__c = false
        ];

        List<EmailTemplate> emailTemplates = [
                SELECT Id, Name
                FROM EmailTemplate
        ];

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();

        for (Enrollment__c enrollment : enrollmentList) {
            String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
            String workflowTriggerName = EmailManager_UnenrolledDocListReminder.designateWorkflowNameForEnrollment(language, enrollment);

            if (workflowTriggerName != null) {
                recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);

                String templateName = '[' + language + '][24] Przypomnienie o konieczności dostarczenia dokumentów dla WS';

                String emailTemplateId = '';
                for (EmailTemplate et : emailTemplates) {
                    if (et.Name == templateName) {
                        emailTemplateId = et.Id;
                    }
                }

                tasksToInsert.add(EmailManager.createEmailTask(
                        Label.title_EventDocListSentUnr,
                        enrollment.Id,
                        enrollment.Candidate_Student__c,
                        EmailManager.prepareFakeEmailBody(emailTemplateId, enrollment.Id),
                        null
                ));
            } else {
                enrollmentsWithFailure.add(enrollment);
            }
        }

        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }

        if (!enrollmentsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_DocumentListReminderEmail.class.getName()
                    + ' ' + JSON.serialize(enrollmentsWithFailure));
        }
    }
}