@isTest
private class ZTestGenerateAcademicYearUnenrolled {

    @isTest static void testGeneratingAcademicYearFromCourseOffer() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = Date.newInstance(2016, 10, 01)), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        Test.startTest();
        String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '2016/2017');
        Test.stopTest();
    }

    @isTest static void testGeneratingAcademicYearFromCourseOffer2() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = Date.newInstance(2016, 10, 01), Start_Date_Semester_3__c = Date.newInstance(2016, 10, 01)), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = courseOffer.Id, Starting_Semester__c = '3'), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        Test.startTest();
        String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '2017/2018');
        Test.stopTest();
    }

    @isTest static void testGeneratingAcademicYearFromSpecialtyOffer() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = Date.newInstance(2016, 10, 01)), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        Test.startTest();
        String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '2016/2017');
        Test.stopTest();
    }

    @isTest static void testGeneratingAcademicYearFromSpecialtyOffer2() {
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = Date.newInstance(2016, 10, 01)), true);
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = universityOffer.Id), true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = specialtyOffer.Id, Starting_Semester__c = '3'), true);
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = studyEnrollment.Id), true);

        Test.startTest();
        String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(documentEnrollment.Id, null, null);
        System.assertEquals(result, '2017/2018');
        Test.stopTest();
    }
}