/**
* @author       Wojciech Słodziak
* @description  Class to send DocumentList with attachments email (can be done only in apex). For Unenrolled candidate process.
**/

public without sharing class EmailManager_UnenrolledDocumentListEmail {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private enum DocListEmTemplates {
        X25_x_StudyEnrDocList, /* no documents delivered */
        X25_y_StudyEnrDocList /* some documents delivered */
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE DESIGNATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private final static List<EmailTemplate> emailTemplates = [SELECT Id, DeveloperName FROM EmailTemplate];
    private static Map<String, EmailTemplate> devNameToETMap;

    private static EmailTemplate designateEmailTemplateForEnrollment(Enrollment__c studyEnr) {
        if (devNameToETMap == null) {
            devNameToETMap = new Map<String, EmailTemplate>();
            for (EmailTemplate et : emailTemplates) {
                devNameToETMap.put(et.DeveloperName, et);
            }
        }

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_COD) {
            return devNameToETMap.get(language + '_' + DocListEmTemplates.X25_y_StudyEnrDocList.name());
        } else {
            return devNameToETMap.get(language + '_' + DocListEmTemplates.X25_x_StudyEnrDocList.name());
        }

        return null;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ INVOKATION METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // email sending must be transfered to Integration user context, because of problems with Verification Site User nad Visualforce Templates
    public static void sendRequiredDocumentList(Set<Id> enrollmentIdList) {
        if (CommonUtility.isVerificationSiteUser()) {
            TransferJobToDefaultUserEmService.tranfserJob(TransferJobToDefaultUserEmService.Transferable.UNENROLLED_DOC_LIST_EMAIL, enrollmentIdList);
        } else {
            EmailManager_UnenrolledDocumentListEmail.sendRequiredDocumentListFinal(enrollmentIdList);
        }
    }

    // method for sending required document list for study Enrollment with generated documents (or without)
    public static Boolean sendRequiredDocumentListFinal(Set<Id> enrollmentIdList) {
        Boolean result = true;

        Map<Id, Messaging.SingleEmailMessage> mails = new Map<Id, Messaging.SingleEmailMessage>();

        List<Enrollment__c> enrollmentList = [
                SELECT Id, Candidate_Student__c, Candidate_Student__r.Foreigner__c, Degree__c, University_Name__c, Status__c, Language_of_Enrollment__c,
                        RequiredDocListEmailedCount__c, Candidate_Student__r.A_Level__c, Finished_University__r.Status__c, Acceptation_Status__c,
                        Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                        Course_or_Specialty_Offer__c, Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                (SELECT Id, Document__c
                FROM Documents__r
                WHERE (Document__c = :CatalogManager.getDocumentAgreementUnenrolledId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId())
                OR Document__c = :CatalogManager.getDocumentAgreementUnenrolledPLId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathPLId())
                FROM Enrollment__c
                WHERE Id IN :enrollmentIdList
                AND Candidate_Student__r.Email != null
        ];

        Map<Id, Attachment> enrToAgrAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getDocumentAgreementUnenrolledId());
        Map<Id, Attachment> enrToAgrPLAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getDocumentAgreementUnenrolledPLId());
        Map<Id, Attachment> enrToOathAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getDocumentOathId());
        Map<Id, Attachment> enrToOathPLAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getDocumentOathPLId());
        Map<Id, Attachment> enrToQuestionnareAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getDocumentPersonalQuestionnareId());
        Map<Id, Attachment> enrToQuestionnarePLAttachmentMap = getEnrToDocAttachmentMap(enrollmentList, CatalogManager.getPLDocumentPersonalQuestionnareId());
        Map<Id, List<Attachment>> additionalDocumentMap = getAdditionalDocAttachmentMap(enrollmentList);

        Map<Id, List<String>> attachmentMapToDisplay = new Map<Id, List<String>>();

        for (Enrollment__c enrollment : enrollmentList) {
            EmailTemplate et = EmailManager_UnenrolledDocumentListEmail.designateEmailTemplateForEnrollment(enrollment);

            if (et != null) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                mail.setTargetObjectId(enrollment.Candidate_Student__c);
                mail.setSaveAsActivity(false);
                mail.setWhatId(enrollment.Id);
                mail.setTemplateId(et.Id);
                mail.setOrgWideEmailAddressId(OrgWideEmailManager.getOrgWideEmailAddress(
                        enrollment.Candidate_Student__r.Foreigner__c,
                        enrollment.University_Name__c,
                        enrollment.Degree__c
                ).Id);

                List<Messaging.EmailFileAttachment> emailAttachmentList = new List<Messaging.EmailFileAttachment>();
                attachmentMapToDisplay.put(enrollment.Id, EmailManager_UnenrolledDocumentListEmail.addRequiredAttchmentsToList(enrollment, enrToAgrAttachmentMap, enrToOathAttachmentMap, enrToQuestionnareAttachmentMap, enrToAgrPLAttachmentMap, enrToOathPLAttachmentMap, enrToQuestionnarePLAttachmentMap, additionalDocumentMap, emailAttachmentList));
                mail.setFileAttachments(emailAttachmentList);

                mails.put(enrollment.Id, mail);
            } else {
                ErrorLogger.configMsg(CommonUtility.ET_UNDEFINED_ERROR + ' ' + EmailManager_UnenrolledDocumentListEmail.class.getName() + ', enrId: ' + enrollment.Id);
                return false;
            }
        }

        Boolean withError = false;
        try {
            if (!mails.isEmpty()) {
                Messaging.sendEmail(mails.values());
            }
        } catch (Exception ex) {
            withError = true;
            ErrorLogger.log(ex);
            result = false;
        }

        if (!withError) {
            List<Task> tasksToInsert = new List<Task>();
            for (Enrollment__c enrollment : enrollmentList) {

                tasksToInsert.add(EmailManager.createEmailTask(
                        Label.title_EventUnenrolledDocListSent,
                        enrollment.Id,
                        enrollment.Candidate_Student__c,
                        mails.get(enrollment.Id).getHtmlBody(),
                        attachmentMapToDisplay.get(enrollment.Id)
                ));
            }

            try {
                update enrollmentList;
                insert tasksToInsert;
            } catch (Exception ex) {
                ErrorLogger.log(ex);
                result = false;
            }
        }

        return result;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public static List<Offer__c> prepareOfferIdSetFromEnrollment(List<Enrollment__c> enrList) {
        Set<Id> offerIdSet = new Set<Id>();

        for (Enrollment__c enrollment : enrList) {
            offerIdSet.add(enrollment.Course_or_Specialty_Offer__c);
            offerIdSet.add(enrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c);
            offerIdSet.add(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c);
            offerIdSet.add(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c);
        }

        offerIdSet.remove(null);

        List<Offer__c> additionalDocumentsOfferList = [
                SELECT Id, Additional_Document_from_Offer__c, Offer_from_Additional_Document__c
                FROM Offer__c
                WHERE Offer_from_Additional_Document__c IN :offerIdSet
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_ADDITIONAL_DOCUMENT)
        ];

        return additionalDocumentsOfferList;
    }

    public static Map<Id, List<Attachment>> getAdditionalDocAttMap(List<Offer__c> additionalDocumentsOfferList) {
        Set<Id> catalogIds = new Set<Id>();
        for (Offer__c document : additionalDocumentsOfferList) {
            catalogIds.add(document.Additional_Document_from_Offer__c);
        }

        Map<Id, Attachment> attachmentList = new Map<Id, Attachment>([
                SELECT Id, ParentId, Name, Body
                FROM Attachment
                WHERE ParentId IN :catalogIds
                ORDER BY CreatedDate DESC
        ]);

        List<Catalog__c> documentCatalogs = [
                SELECT Id, (SELECT Id, Name FROM Attachments ORDER BY CreatedDate DESC LIMIT 1)
                FROM Catalog__c
                WHERE Id IN :catalogIds
        ];

        Map<Id, List<Attachment>> additionalDocMap = new Map<Id, List<Attachment>>();
        for (Catalog__c catalog : documentCatalogs) {
            for (Offer__c document : additionalDocumentsOfferList) {
                if (document.Additional_Document_from_Offer__c == catalog.Id) {
                    if (!catalog.Attachments.isEmpty()) {
                        if (additionalDocMap.get(document.Offer_from_Additional_Document__c) == null) {
                            additionalDocMap.put(document.Offer_from_Additional_Document__c, new List<Attachment>{
                                    attachmentList.get(catalog.Attachments.get(0).Id)
                            });
                        } else {
                            List<Attachment> attachments = additionalDocMap.get(document.Offer_from_Additional_Document__c);

                            attachments.add(attachmentList.get(catalog.Attachments.get(0).Id));
                            additionalDocMap.put(document.Offer_from_Additional_Document__c, attachments);
                        }
                    }
                }
            }
        }

        return additionalDocMap;
    }

    public static Map<Id, List<Attachment>> getAdditionalDocAttachmentMap(List<Enrollment__c> enrList) {
        Map<Id, List<Attachment>> additionalDocAttachmentMap = new Map<Id, List<Attachment>>();

        List<Offer__c> offerIdSet = prepareOfferIdSetFromEnrollment(enrList);
        Map<Id, List<Attachment>> additionalDocMap = getAdditionalDocAttMap(offerIdSet);

        for (Enrollment__c enrollment : enrList) {
            List<Attachment> additionalDocs = new List<Attachment>();

            if (enrollment.Course_or_Specialty_Offer__c != null
                    && additionalDocMap.get(enrollment.Course_or_Specialty_Offer__c) != null) {
                additionalDocs.addAll(additionalDocMap.get(enrollment.Course_or_Specialty_Offer__c));
            }

            if (enrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c != null
                    && additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c) != null) {
                additionalDocs.addAll(additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c));
            }

            if (enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c != null
                    && additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c) != null) {
                additionalDocs.addAll(additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c));
            }

            if (enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c != null
                    && additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c) != null) {
                additionalDocs.addAll(additionalDocMap.get(enrollment.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c));
            }

            additionalDocAttachmentMap.put(enrollment.Id, additionalDocs);
        }

        return additionalDocAttachmentMap;
    }

    // helper for sendRequiredDocumentListFinal
    public static Map<Id, Attachment> getEnrToDocAttachmentMap(List<Enrollment__c> enrList, Id documentDefinitionId) {
        Map<Id, Attachment> enrToAttachmentMap = new Map<Id, Attachment>();

        Set<Id> attachmentParentIds = new Set<Id>();
        for (Enrollment__c enrollment : enrList) {
            for (Enrollment__c agrDoc : enrollment.Documents__r) {
                if (agrDoc.Document__c == documentDefinitionId) {
                    attachmentParentIds.add(agrDoc.Id);
                }
            }
        }

        List<Attachment> attachmentList = [
                SELECT Id, ParentId, Name, Body
                FROM Attachment
                WHERE ParentId IN :attachmentParentIds
                AND (NOT(Name LIKE : '%' + CommonUtility.DOC_DECRYPTED_FILE_PREFIX + '%'))
                ORDER BY CreatedDate ASC
        ];

        for (Enrollment__c enrollment : enrList) {
            for (Enrollment__c agrDoc : enrollment.Documents__r) {
                for (Attachment agrAtt : attachmentList) {
                    if (agrAtt.ParentId == agrDoc.Id) {
                        enrToAttachmentMap.put(enrollment.Id, agrAtt);
                    }
                }
            }
        }

        return enrToAttachmentMap;
    }

    // helper for sendRequiredDocumentListFinal
    public static List<String> addRequiredAttchmentsToList(Enrollment__c enrollment, Map<Id, Attachment> enrToAgrAttachmentMap,
            Map<Id, Attachment> enrToOathAttachmentMap,
            Map<Id, Attachment> enrToQuestionnareAttachmentMap,
            Map<Id, Attachment> enrToAgrPLAttachmentMap,
            Map<Id, Attachment> enrToOathPLAttachmentMap,
            Map<Id, Attachment> enrToQuestionnarePLAttachmentMap,
            Map<Id, List<Attachment>> additionalDocMap,
            List<Messaging.EmailFileAttachment> emailAttachmentList) {

        List<String> attachmentList = new List<String>();
        // add Agreement
        Attachment agrAtt = enrToAgrAttachmentMap.get(enrollment.Id);
        if (agrAtt != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(agrAtt.Name));
            efa.setBody(agrAtt.Body);

            attachmentList.add(agrAtt.Name);
            emailAttachmentList.add(efa);
        }
        // add Agreement PL
        Attachment agrAttPL = enrToAgrPLAttachmentMap.get(enrollment.Id);
        if (agrAttPL != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(agrAttPL.Name));
            efa.setBody(agrAttPL.Body);

            attachmentList.add(agrAttPL.Name);
            emailAttachmentList.add(efa);
        }

        // add Oath
        Attachment oathAtt = enrToOathAttachmentMap.get(enrollment.Id);
        if (oathAtt != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(oathAtt.Name));
            efa.setBody(oathAtt.Body);

            attachmentList.add(oathAtt.Name);
            emailAttachmentList.add(efa);
        }

        // add Oath PL
        Attachment oathAttPL = enrToOathPLAttachmentMap.get(enrollment.Id);
        if (oathAttPL != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(oathAttPL.Name));
            efa.setBody(oathAttPL.Body);

            attachmentList.add(oathAttPL.Name);
            emailAttachmentList.add(efa);
        }

        // add Questionnare
        Attachment questionnareAtt = enrToQuestionnareAttachmentMap.get(enrollment.Id);
        if (questionnareAtt != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(questionnareAtt.Name));
            efa.setBody(questionnareAtt.Body);

            attachmentList.add(questionnareAtt.Name);
            emailAttachmentList.add(efa);
        }

        // add Questionnare PL
        Attachment questionnareAttPL = enrToQuestionnarePLAttachmentMap.get(enrollment.Id);
        if (questionnareAttPL != null) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(CommonUtility.swapSpecialPolishChars(questionnareAttPL.Name));
            efa.setBody(questionnareAttPL.Body);

            attachmentList.add(questionnareAttPL.Name);
            emailAttachmentList.add(efa);
        }

        // add additional documents
        List<Attachment> additionalDocs = additionalDocMap.get(enrollment.Id);
        if (additionalDocs != null && !additionalDocs.isEmpty()) {
            for (Attachment additionalDocument : additionalDocs) {
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(CommonUtility.swapSpecialPolishChars(additionalDocument.Name));
                efa.setBody(additionalDocument.Body);

                attachmentList.add(additionalDocument.Name);
                emailAttachmentList.add(efa);
            }
        }

        return attachmentList;
    }
}