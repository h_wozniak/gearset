@isTest
global class ZTestIntegrationDidacticsResource {
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(
            Phone = '123456789',
            Firstname = 'Bob',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Pesel__c = '05261508023',
            Country_of_Origin__c = 'Poland',
            The_Same_Mailling_Address__c = true,
            OtherCountry = 'Poland',
            OtherState = 'Lower Silesia',
            OtherCity = 'Wrocław',
            OtherStreet = 'Krzyki'
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = retMap.get(0).Id), true));
        Test.stopTest();
        
        return retMap;
    }

    @isTest static void test_integrationDidacticsResource_properJSON() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":"Economics","degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":"2016-10-10","defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(500, res.statusCode);
    }

    @isTest static void test_integrationDidacticsResource_DateNull() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":"Economics","degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(500, res.statusCode);
    }

    @isTest static void test_integrationDidacticsResource_withoutStatus() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":null,"portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":"Economics","degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":null}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(800, 'didactics_status'), res.responseBody);
    }

    @isTest static void test_integrationDidacticsResource_withoutTradeName() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":null,"degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(800, 'trade_name'), res.responseBody);
    }

    @isTest static void test_integrationDidacticsResource_withoutId() { 
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":null,"degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', null);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(605), res.responseBody);
    }

    @isTest static void test_integrationDidacticsResource_incorrectId() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":null,"degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', 'a038E000003oe5L');

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(1000), res.responseBody);
    }

    @isTest static void test_integrationDidacticsResource_incorrectJSON() {
        Map<Integer, sObject> retMap = prepareData();

        String body = '[{"didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","product_name":null,"trade_name":null,"degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}]';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', 'a038E000003oe5L');

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(1000), res.responseBody);
    }

    @isTest static void test_integrationDidacticsResource_Offer() { 
        Map<Integer, sObject> retMap = prepareData();

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

        Offer__c courseOfferAfterInsert = [
            SELECT Id, Name
            FROM Offer__c
            WHERE Id = :courseOffer.Id
        ];

        String body = '{"product_name":"' + courseOfferAfterInsert.Name + '","didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","trade_name":"some_trade_name","degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        System.debug(body);

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();

        System.assertEquals(200, res.statusCode);
    }

    @isTest static void test_integrationDidacticsResource_incorrectOffer() { 
        Map<Integer, sObject> retMap = prepareData();

        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);

        Offer__c courseOfferAfterInsert = [
            SELECT Id, Name
            FROM Offer__c
            WHERE Id = :courseOffer.Id
        ];

        String body = '{"product_name":"IncorrectOfferName","didactics_status":"finished education","portal_login":"some_login","bank_acc_nbr":"98754321","student_nbr":"123456798","current_semester":2,"current_semester_chg_date":"1991-10-10","trade_name":"trade_name","degree":"I","kind":"Master","mode":"Full Time","number_of_semesters":3,"finished_university":{"obtained_title":"Bachelor","diploma_number":"1231123","country_of_diploma_issue":"Poland","diploma_issue_date":null,"defense_date":"2017-10-10","status":"Before defense"}}';

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(2).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/didactics-update/';
        req.requestBody = Blob.valueof(body);
        req.addParameter('recrutation_id', studyEnrAgr.Id);

        req.httpMethod = 'PUT';
        RestContext.request = req;
        RestContext.response = res;

        IntegrationDidacticsResource.receiveEnrollmentInformation();
        
        System.assertEquals(400, res.statusCode);
        System.assertEquals(IntegrationError.getErrorJSONBlobByCode(1001, 'IncorrectOfferName'), res.responseBody);
    }
}