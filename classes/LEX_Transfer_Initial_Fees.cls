public with sharing class LEX_Transfer_Initial_Fees {

    @AuraEnabled
    public static String checkRecord(String recordId) {
        Enrollment__c enr = [
                SELECT Status__c, Unenrolled_Status__c, Resignation_Reason_List__c
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        if(enr.Unenrolled_Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                || enr.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                || (enr.Status__c == CommonUtility.ENROLLMENT_STATUS_RESIGNATION && enr.Resignation_Reason_List__c == CommonUtility.ENROLLMENT_RESREASON_EXPERIA)) {
            return 'TRUE';
        }

        return 'FALSE';
    }

    @AuraEnabled
    public static String hasEditAccess(String recordId) {
        return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
    }

    @AuraEnabled
    public static String transferInitialFeesToKS(String recordId) {
        return LEXStudyUtilities.transferInitialFeesToKS(new List<String>{recordId});
    }

    @AuraEnabled
    public static String transferEnrollmentsToKS(String recordId) {
        return LEXStudyUtilities.transferEnrollmentsToKS(new List<String>{recordId}, false, true);
    }
}