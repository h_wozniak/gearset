/**
*   @author         Sebastian Łasisz
*   @description    Rest Resource class for receiving acknowledgements for Enrollment transfers to Experia.
**/

@RestResource(urlMapping = '/didactics_ack/*')
global without sharing class IntegrationEnrollmentAckReceiver {


    @HttpPost
    global static void receiveEnrollmentInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        IntegrationManager.AckResponse_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                List<Enrollment__c> eduAgrList = [
                    SELECT Id, Source_Enrollment__c, Source_Enrollment__r.TECH_Integration_Status__c
                    FROM Enrollment__c 
                    WHERE Id = :parsedJson.record_id
                ];

                if (!eduAgrList.isEmpty()) {
                    Boolean updateStatusOnEnr = true;

                    if (parsedJson.success) {
                        Enrollment__c studyEnr = [
                            SELECT Id, Status__c, Unenrolled_Status__c, Unenrolled__c, TECH_Integration_Status__c
                            FROM Enrollment__c
                            WHERE Id = :eduAgrList[0].Source_Enrollment__c
                        ];

                        if (studyEnr.TECH_Integration_Status__c == CommonUtility.ENROLLMENT_INTEGRATION_PAYMENTKS) {
                            IntegrationPaymentScheduleSender.sendPaymentSchedulesSync(new Set<Id> { studyEnr.Id }, false);
                            updateStatusOnEnr = false;
                        }
                        else if (studyEnr.TECH_Integration_Status__c == CommonUtility.ENROLLMENT_INTEGRATION_INITIALFEES) {
                            IntegrationPaymentScheduleSender.sendPaymentSchedulesSync(new Set<Id> { studyEnr.Id }, true);
                            updateStatusOnEnr = false;
                        }
                        else {
                            if (!studyEnr.Unenrolled__c && studyEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT) {
                                studyEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS;
                            } else if (studyEnr.Unenrolled__c && studyEnr.Unenrolled_Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT) {
                                studyEnr.Unenrolled_Status__c = CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS;
                            }

                            update studyEnr;
                        }
                    } 
                    else {
                        ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + '\n' + IntegrationEnrollmentAckReceiver.class.getName() + '\n' + jsonBody);
                        updateStatusOnEnr = true;
                    }

                    if (!parsedJson.success || (updateStatusOnEnr && eduAgrList[0].Source_Enrollment__r.TECH_Integration_Status__c == CommonUtility.ENROLLMENT_INTEGRATION_ENROLLMENTKS)) {
                        IntegrationEnrollmentHelper.updateSyncStatus_onFinish(parsedJson.success, new Set<Id> { eduAgrList[0].Source_Enrollment__c }, parseErrorMsg(jsonBody));
                    }

                } 
                else {
                    ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR_NOT_FOUND + '\n' + IntegrationEnrollmentAckReceiver.class.getName() + '\n' + jsonBody);
                }
                
                res.statusCode = 200;
            } catch (Exception e) {
                ErrorLogger.log(e);
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }
        }
    }
    
    private static IntegrationManager.AckResponse_JSON parse(String jsonBody) {
        return (IntegrationManager.AckResponse_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.AckResponse_JSON.class);
    }

    private static String parseErrorMsg(String jsonBody) {
        if (jsonBody.contains('"status_code":200')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":rekrutacja została zaktualizowana}';
        if (jsonBody.contains('"status_code":201')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":rekrutacja została stworzona}';
        if (jsonBody.contains('"status_code":400')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":niepoprawne dane - komunikat nie może być przetworzony}';
        if (jsonBody.contains('"status_code":405')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":nie można zmienić rekrutacji na podany nowy produkt}';
        if (jsonBody.contains('"status_code":406')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":nie znaleziono produktu o podanym ID}';
        if (jsonBody.contains('"status_code":409')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":zduplikowany produkt/rekrutacja}';
        if (jsonBody.contains('"status_code":500')) jsonBody = jsonBody.left(jsonBody.length() - 1) + ',"status_msg":błąd serwera}';

        return jsonBody;
    }

}