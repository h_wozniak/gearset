global class EnrollmentDocumentEncryption implements Database.Batchable<sobject> 
{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator (
            [ SELECT Id, RecordTypeId, TECH_Is_Encrypted__c, CreatedDate, Status__c
                FROM Enrollment__c
                WHERE RecordTypeId  =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                AND CreatedDate > 2018-03-27T00:00:00.000+0000
                AND CreatedDate < 2018-06-14T00:00:00.000+0000
                AND (not Status__c IN ('Signed contract', 'Resignation', 'Payments in KS'))
            ]);                                                                                       
    }

    global void execute(Database.BatchableContext bc, List<Enrollment__c> scope){        
        List<Enrollment__c> enrollments = new List<Enrollment__c>();
        for (Enrollment__c enrollment: Scope) {
            if(enrollment.TECH_Is_Encrypted__c == true) {
                enrollment.TECH_Is_Encrypted__c = false;
                enrollments.add(enrollment);
            }
        }
        update enrollments ;
    }
                     
    global void finish(Database.BatchableContext BC) {
    } 
}