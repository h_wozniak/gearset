@isTest
private class ZTestOfferManager_Trainings {


	
    @isTest static void testCodeGeneration() {

        Test.startTest();
        Id toId = ZDataTestUtility.createTrainingOffer(new Offer__c(Name='test1'), true).Id;
        Id schId = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(Name='test2', Training_Offer_from_Schedule__c = toId), true).Id;
        Test.stopTest();

        Offer__c trainingOffer = [SELECT Name FROM Offer__c WHERE Id = :toId];
        Offer__c schedule = [SELECT Name FROM Offer__c WHERE Id = :schId];

        System.assertNotEquals(trainingOffer.Name, 'test1'); 
        System.assertNotEquals(schedule.Name, 'test2'); 

    }

    @isTest static void testDisableEditingOf() {
        Offer__c to = ZDataTestUtility.createTrainingOffer(new Offer__c(Name='test1'), true);

        Test.startTest();
        to.University_from_Training__c = ZDataTestUtility.createUniversity(null, true).Id;
        try {
            update to;
            System.assert(false);
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.msg_error_CantEditField)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void testPreventDeletionOfTrainingOffer() {
        Offer__c to = ZDataTestUtility.createTrainingOffer(null, true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        System.debug(sch);

        Test.startTest();
        try {
            delete to;
            System.assert(false);
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.msg_errorCannotDeleteActiveSchedules)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void testEndTimeCopyUpdate() {
        Offer__c to = ZDataTestUtility.createTrainingOffer(null, true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);
        Enrollment__c enr = ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = sch.Id
        ), true);

        Enrollment__c enrBefore = [SELECT Schedule_End_Time_Copy__c FROM Enrollment__c WHERE Id = :enr.Id];

        System.assert(enrBefore.Schedule_End_Time_Copy__c != null);
        Date endDateCopy = enr.Schedule_End_Time_Copy__c;

        Test.startTest();
        Offer__c schToUpdated = new Offer__c(Id = sch.Id);
        schToUpdated.Valid_To__c = System.today().addDays(1);
        update schToUpdated;
        Test.stopTest();

        Enrollment__c enrAfter = [SELECT Schedule_End_Time_Copy__c FROM Enrollment__c WHERE Id = :enr.Id];

        System.assertNotEquals(endDateCopy, enrAfter.Schedule_End_Time_Copy__c);
    }

    @isTest static void testTradeNameUpdateOnSchedule() {
        Offer__c to = ZDataTestUtility.createTrainingOffer(new Offer__c(Trade_Name__c = 'trade1'), true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);


        Test.startTest();
        Offer__c toAfter = [SELECT Id, Trade_Name__c FROM Offer__c WHERE Id = :to.Id];
        toAfter.Trade_Name__c = 'trade2';
        update toAfter;
        Test.stopTest();

        Offer__c schAfter = [SELECT Id, Trade_Name__c FROM Offer__c WHERE Id = :sch.Id];

        System.assertEquals('trade2', schAfter.Trade_Name__c);
    }

    @isTest static void testTaskForScheduleInsert() {

        Test.startTest();
        Offer__c to = ZDataTestUtility.createTrainingOffer(null, true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];

        System.assertEquals(3, taskList.size());
    }

    @isTest static void testTaskForClosedScheduleInsert() {

        Test.startTest();
        Offer__c to = ZDataTestUtility.createTrainingOffer(new Offer__c(Type__c = CommonUtility.OFFER_TYPE_CLOSED), true);
        Offer__c sch = ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Offer_Value__c = 1000,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task];

        System.assertEquals(1, taskList.size());
    }

    @isTest static void testTaskReassignation() {
        Offer__c to = ZDataTestUtility.createTrainingOffer(null, true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        List<Task> taskList = [SELECT Id, OwnerId FROM Task];
        System.assertEquals(3, taskList.size());

        User tAdmin = ZDataTestUtility.createRandomUser();

        Test.startTest();
        Offer__c toAfter = [SELECT Id, Training_Administrator__c FROM Offer__c WHERE Id = :to.Id];
        toAfter.Training_Administrator__c = tAdmin.Id;
        update toAfter;
        Test.stopTest();


        List<Task> taskListAfter = [SELECT Id, OwnerId FROM Task WHERE WhatId = :sch.Id];

        for (Task t : taskListAfter) {
            System.assertEquals(tAdmin.Id, t.OwnerId);
        }
    }

    @isTest static void testTaskDateReassignation() {

        Offer__c to = ZDataTestUtility.createTrainingOffer(null, true);
        Offer__c sch = ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = to.Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today().addDays(10),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true);

        List<Task> taskList = [SELECT Id, ActivityDate FROM Task];
        System.assertEquals(3, taskList.size());

        User tAdmin = ZDataTestUtility.createRandomUser();

        Test.startTest();
        Offer__c schAfter = [SELECT Id, Valid_From__c FROM Offer__c WHERE Id = :sch.Id];
        schAfter.Valid_From__c = schAfter.Valid_From__c.addDays(1);
        update schAfter;
        Test.stopTest();

        List<Task> taskListAfter = [SELECT Id, ActivityDate FROM Task WHERE WhatId = :sch.Id];

        Boolean taskDateChanged = false;
        for (Task tBefore : taskList) {
            for (Task tAfter : taskListAfter) {
                if (tBefore.Id == tAfter.Id && tBefore.ActivityDate != tAfter.ActivityDate) {
                    taskDateChanged = true;
                }
            }
        }

        System.assert(taskDateChanged);
    }


}