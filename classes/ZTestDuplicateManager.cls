@isTest
private class ZTestDuplicateManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ DEDUPLICATION RULE 1 -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testDuplicateCheck_Rule1_differentPESEL() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = null,
            Email = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        List<Contact> contactList = [SELECT Id FROM Contact];
        System.assertEquals(contactList.size(), 1);
    }

    @isTest static void testDuplicateCheck_Rule1_differentPESEL2() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = null,
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = null,
            Email = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        List<Contact> contactList = [SELECT Id FROM Contact];
        System.assertEquals(contactList.size(), 1);
    }

    @isTest static void testDuplicateCheck_Rule1_differentPESEL3() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = null,
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        List<Contact> contactList = [SELECT Id FROM Contact];
        System.assertEquals(contactList.size(), 1);
    }

    @isTest static void testDuplicateCheck_Rule1() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        List<Contact> contactList = [SELECT Id FROM Contact];
        System.assertEquals(contactList.size(), 1);
    }

    @isTest static void testDuplicateCheck_Rule1_differentRT() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createContactPerson(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        List<Contact> contactList = [SELECT Id FROM Contact];
        System.assertEquals(contactList.size(), 2);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ DEDUPLICATION RULE 2 -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testDuplicateCheck_Rule2() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com',
            Pesel__c = '78060612859'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Anthony',
            LastName = 'Ivanov',
            Email = 'antivan@gmail.com',
            Pesel__c = '78060612859'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(c1After.Is_Potential_Duplicate__c);
        System.assertEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(c2After.Is_Potential_Duplicate__c);
        System.assertEquals(c1.Id, c2After.Potential_Duplicate__c);
    }

    @isTest static void testDuplicateCheck_Rule2_otherRT() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com',
            Pesel__c = '78060612859'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createContactPerson(new Contact(
            FirstName = 'Anthony',
            LastName = 'Ivanov',
            Email = 'antivan@gmail.com',
            Pesel__c = '78060612859'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(!c1After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(!c2After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c1.Id, c2After.Potential_Duplicate__c);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ DEDUPLICATION RULE 3 -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testDuplicateCheck_Rule3() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks1234567@mail.com',
            Additional_Emails__c = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(c1After.Is_Potential_Duplicate__c);
        System.assertEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(c2After.Is_Potential_Duplicate__c);
        System.assertEquals(c1.Id, c2After.Potential_Duplicate__c);
    }

    @isTest static void testDuplicateCheck_Rule3_otherRT() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createContactPerson(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks1234567@mail.com',
            Additional_Emails__c = 'tom.hanks@mail.com'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(!c1After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(!c2After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c1.Id, c2After.Potential_Duplicate__c);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ DEDUPLICATION RULE 4 -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void testDuplicateCheck_Rule4() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com',
            Phone = '123123123'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks-Worldeater',
            Email = 'tom.hanks@mail.com',
            Phone = '789789798',
            Additional_Phones__c = '123123123'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(c1After.Is_Potential_Duplicate__c);
        System.assertEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(c2After.Is_Potential_Duplicate__c);
        System.assertEquals(c1.Id, c2After.Potential_Duplicate__c);
    }

    @isTest static void testDuplicateCheck_Rule4_otherRT() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Email = 'tom.hanks@mail.com',
            Phone = '123123123'
        ), true);
            
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Test.startTest();
        Contact c2 = ZDataTestUtility.createContactPerson(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks-Worldeater',
            Email = 'tom.hanks@mail.com',
            Phone = '789789798',
            Additional_Phones__c = '123123123'
        ), true);
        Test.stopTest();

        Contact c1After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c1.Id];
        System.assert(!c1After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c2.Id, c1After.Potential_Duplicate__c);

        Contact c2After = [SELECT Is_Potential_Duplicate__c, Potential_Duplicate__c FROM Contact WHERE Id = :c2.Id];
        System.assert(!c2After.Is_Potential_Duplicate__c);
        System.assertNotEquals(c1.Id, c2After.Potential_Duplicate__c);
    }
}