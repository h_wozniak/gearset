/**
*   @author         Sebastian Łasisz
*   @description    Controller for DeliveredEmailTempalte for visualising sent emails.
**/

public with sharing class DeliveredEmailTemplateController {
	public String htmlEmail { get; set; }
	public Id taskId { get; set; }
	public Task taskWithEmail { get; set; }
	public String pinnedAttachments { get; set; }
	public String pinnedAttachments2 { get; set; }
	public String pinnedTermsOfUse { get; set; }

	public DeliveredEmailTemplateController() {
        taskId = ApexPages.currentPage().getParameters().get('taskId');
        taskWithEmail = [
        	SELECT Id, Description, Pinned_Attachments__c, Pinned_Attachments2__c, Pinned_Terms_of_Use__c
        	FROM Task
        	WHERE Id = :taskId
        ];

        htmlEmail = taskWithEmail.Description;

        pinnedAttachments = CommonUtility.strListToHTMLList(CommonUtility.getOptionsFromMultiSelectToList(taskWithEmail.Pinned_Attachments__c));
        pinnedAttachments2 = CommonUtility.strListToHTMLList(CommonUtility.getOptionsFromMultiSelectToList(taskWithEmail.Pinned_Attachments2__c));
        pinnedTermsOfUse = CommonUtility.strListToHTMLList(CommonUtility.getOptionsFromMultiSelectToList(taskWithEmail.Pinned_Terms_of_Use__c));
	}

	public PageReference back() {
		if (taskId != null) {
			return new PageReference('/' + taskId);
		}

		return new PageReference('/');
	}
}