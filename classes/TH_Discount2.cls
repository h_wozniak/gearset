/**
* @description  This class is a Trigger Handler for Study Discounts - complex logic
**/

public without sharing class TH_Discount2 extends TriggerHandler.DelegateBase {

    Set<Id> studyEnrToUpdatePaymentSchDiscountVal;
    List<Discount__c> companyDiscountsToDisapplyOther;
    List<Discount__c> employeeDiscountToDisapplitOthers;
    List<Discount__c> chancellorDiscountsToDisapplyOther;
    List<Discount__c> visDiscountsToDisapplyOther;
    List<Discount__c> secondCourseDiscountsToDisapplyOther;
    List<Discount__c> recDiscountsToCheckLimit;
    List<Discount__c> offerDCToCheckIfCanInsert;
    List<Discount__c> timeDiscountsToCheckUpdateValidity;
    List<Discount__c> discConnectorsToCheckDuplicates;
    List<Discount__c> discConnToCheckOfferUniversity;
    List<Discount__c> timeDiscountsToCheckIfCanEditFields;
    List<Discount__c> companyDiscountsToCheckIfCanEditFields;
    List<Discount__c> companyDiscountsToSetName;
    List<Discount__c> companyDiscountsToUpdateCompanyDocument;
    List<Discount__c> docEnrIdsToAddCompanyDocument;
    List<Discount__c> companyDiscountsToRemoveDocument;
    List<Discount__c> companyDiscountToCheckIfCanAdd;
    List<Discount__c> offerCodeDiscountConnectorsToValidate;
    Set<Id> discountsToRewriteTradeName;
    Set<Id> enrollmentsToAddSecondCourse;
    List<Discount__c> timeDiscountsToValidate;
    List<Discount__c> manageAppliedDiscounts;
    List<Discount__c> companyDiscountsToDisaplyOthers;

    public override void prepareBefore() {
        recDiscountsToCheckLimit = new List<Discount__c>();
        offerDCToCheckIfCanInsert = new List<Discount__c>();
        timeDiscountsToCheckUpdateValidity = new List<Discount__c>();
        discConnectorsToCheckDuplicates = new List<Discount__c>();
        discConnToCheckOfferUniversity = new List<Discount__c>();
        timeDiscountsToCheckIfCanEditFields = new List<Discount__c>();
        companyDiscountsToCheckIfCanEditFields = new List<Discount__c>();
        companyDiscountsToSetName = new List<Discount__c>();
        companyDiscountToCheckIfCanAdd = new List<Discount__c>();
        offerCodeDiscountConnectorsToValidate = new List<Discount__c>();
        timeDiscountsToValidate = new List<Discount__c>();
    }

    public override void prepareAfter() {
        studyEnrToUpdatePaymentSchDiscountVal = new Set<Id>();
        companyDiscountsToDisapplyOther = new List<Discount__c>();
        employeeDiscountToDisapplitOthers = new List<Discount__c>();
        chancellorDiscountsToDisapplyOther = new List<Discount__c>();
        visDiscountsToDisapplyOther = new List<Discount__c>();
        secondCourseDiscountsToDisapplyOther = new List<Discount__c>();
        discountsToRewriteTradeName = new Set<Id>();
        docEnrIdsToAddCompanyDocument = new List<Discount__c>();
        companyDiscountsToUpdateCompanyDocument = new List<Discount__c>();
        companyDiscountsToRemoveDocument = new List<Discount__c>();
        enrollmentsToAddSecondCourse = new Set<Id>();
        manageAppliedDiscounts = new List<Discount__c>();
        companyDiscountsToDisaplyOthers = new List<Discount__c>();
    }

    public override void beforeInsert(List<sObject> o) {
        List<Discount__c> newDiscountList = (List<Discount__c>)o;
        for(Discount__c newDiscount : newDiscountList) {

            /* Wojciech Słodziak */
            // on Recommended Person Discount insert check if limit of 5 same Discounts on Enrollment wasn't exceeded
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_RECPERSON) {
                recDiscountsToCheckLimit.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // on Offer Time DC insert check if Valid from / to dates are not overlapping
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)) {
                offerDCToCheckIfCanInsert.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // duplicate detection for Offer Discount Connectors
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_COMPANY_DC)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC)) {
                discConnectorsToCheckDuplicates.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to disable adding discounts connectors to offers from different university than main offer
            if (!CommonUtility.preventEditingDiscounts
                    && (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_COMPANY_DC)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC))) {
                discConnToCheckOfferUniversity.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to generate name for newly created company discount connector
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_ACCOUNT_COMPANY_DC)) {
                companyDiscountsToSetName.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to check whether can add company to already defined company discount
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_ACCOUNT_COMPANY_DC)) {
                companyDiscountToCheckIfCanAdd.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Check whether insterted Offer Connector matches Level Details on Discount
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_TIME_DC)) {
                offerCodeDiscountConnectorsToValidate.add(newDiscount);
            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        Discount__c newDiscount;
        for (Id key : newDiscounts.keySet()) {
            newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // trigger to update Enrollment Payment Schedule after Discount insert
            if (
                    !CommonUtility.skipDiscountApplication
                            && (
                            newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    )
                            && newDiscount.Applied__c == true) {
                studyEnrToUpdatePaymentSchDiscountVal.add(newDiscount.Enrollment__c);
            }

            /* Wojciech Słodziak */
            // on chancellor Manual Discount insert with Applied__c = true disapply other discounts of the same Applies_to__c type
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_CHANCELLOR && newDiscount.Applied__c == true) {
                chancellorDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // on VIS Manual Discount insert with Applied__c = true disapply other Tuition fee Discounts
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_VIS && newDiscount.Applied__c == true) {
                visDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // on Second Course Manual Discount insert with Applied__c = true disapply other Tuition fee Discounts
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE && newDiscount.Applied__c == true) {
                secondCourseDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Rewrite Trade Name from Discount Definition
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)) {
                discountsToRewriteTradeName.add(newDiscount.Id);
            }

            /* Wojciech Słodziak */
            // after applying Company Discount disapply other Tuition fee Discounts
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Applied__c == true
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT) {
                companyDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // after applying Employee Discount disapply other Tuition fee Discounts
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Applied__c == true
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_EMPLOYEEDISCOUNT) {
                employeeDiscountToDisapplitOthers.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to generate company discount document after discount is added
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT) {
                docEnrIdsToAddCompanyDocument.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // On second course discount insert change secound course checkbox on enrollment
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE) {
                enrollmentsToAddSecondCourse.add(newDiscount.Enrollment__c);
            }

            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    || (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_CODE)) {
                manageAppliedDiscounts.add(newDiscount);
            }

            if (CommonUtility.allowDisaplyingDiscounts
                    && (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT)) {
                companyDiscountsToDisaplyOthers.add(newDiscount);
            }
        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        for (Id key : newDiscounts.keySet()) {
            Discount__c oldDiscount = oldDiscounts.get(key);
            Discount__c newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // on Time Discount update check if it's Offer Discount Connectors are not overlapping with other Time Discounts
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_TIME_DISCOUNT)
                    && (
                    oldDiscount.Active__c != newDiscount.Active__c
                            || oldDiscount.Discount_Type_Studies__c != newDiscount.Discount_Type_Studies__c
                            || oldDiscount.Valid_From__c != newDiscount.Valid_From__c
                            || oldDiscount.Valid_To__c != newDiscount.Valid_To__c
            )
                    ) {
                timeDiscountsToCheckUpdateValidity.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // if time discount is activedisable editing it except few values
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_TIME_DISCOUNT)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT))
                    && newDiscount.Active__c == true
                    && (newDiscount.Name != oldDiscount.Name
                    || newDiscount.Applies_to__c != oldDiscount.Applies_to__c
                    || newDiscount.Applied_through__c != oldDiscount.Applied_through__c
                    || newDiscount.Discount_Kind__c != oldDiscount.Discount_Kind__c
                    || newDiscount.Discount_Value__c != oldDiscount.Discount_Value__c
                    || newDiscount.University_Names__c != oldDiscount.University_Names__c
                    || newDiscount.English_Trade_Name__c != oldDiscount.English_Trade_Name__c
                    || newDiscount.Russian_Trade_Name__c != oldDiscount.Russian_Trade_Name__c
                    || newDiscount.Trade_Name__c != oldDiscount.Trade_Name__c
                    || newDiscount.Degrees__c != oldDiscount.Degrees__c
                    || newDiscount.Discount_Type_Studies__c != oldDiscount.Discount_Type_Studies__c
                    || newDiscount.Detail_Level__c != oldDiscount.Detail_Level__c
            ))
            {
                newDiscount.addError(' ' + Label.msg_error_CanEditFields3
                        + ' ' + CommonUtility.getFieldLabel('Discount__c', 'Valid_To__c')
                        + ', ' + CommonUtility.getFieldLabel('Discount__c', 'Valid_From__c')
                        + ', ' + CommonUtility.getFieldLabel('Discount__c', 'Active__c')
                        + ', ' + CommonUtility.getFieldLabel('Discount__c', 'Description__c')
                );
            }

            /* Sebastian Łasisz */
            // if time discount is connected to any enrollment disable editing it except few values
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_TIME_DISCOUNT)
                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT))
                    && (newDiscount.Name != oldDiscount.Name
                    || newDiscount.Applies_to__c != oldDiscount.Applies_to__c
                    || newDiscount.Applied_through__c != oldDiscount.Applied_through__c
                    || newDiscount.Discount_Kind__c != oldDiscount.Discount_Kind__c
                    || newDiscount.Discount_Value__c != oldDiscount.Discount_Value__c
                    || newDiscount.University_Names__c != oldDiscount.University_Names__c
                    || newDiscount.English_Trade_Name__c != oldDiscount.English_Trade_Name__c
                    || newDiscount.Russian_Trade_Name__c != oldDiscount.Russian_Trade_Name__c
                    || newDiscount.Trade_Name__c != oldDiscount.Trade_Name__c
                    || newDiscount.Degrees__c != oldDiscount.Degrees__c
                    || newDiscount.Discount_Type_Studies__c != oldDiscount.Discount_Type_Studies__c
                    || newDiscount.Detail_Level__c != oldDiscount.Detail_Level__c
            )) {
                timeDiscountsToValidate.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // if time discount has connectors attached disable editing fields
            if (
                    newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_TIME_DISCOUNT)
                            && (newDiscount.Name != oldDiscount.Name
                            || newDiscount.Applies_to__c != oldDiscount.Applies_to__c
                            || newDiscount.Applied_through__c != oldDiscount.Applied_through__c
                            || newDiscount.Discount_Kind__c != oldDiscount.Discount_Kind__c
                            || newDiscount.Discount_Value__c != oldDiscount.Discount_Value__c
                            || newDiscount.Discount_Type_Studies__c != oldDiscount.Discount_Type_Studies__c
                    )
                    ) {
                timeDiscountsToCheckIfCanEditFields.add(newDiscount);
            }

            /* Sebastian Łasisz */
            //if company discount has connectors attached disable editing fields
            if (
                    newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT)
                            && (newDiscount.Name != oldDiscount.Name
                            || newDiscount.Applies_to__c != oldDiscount.Applies_to__c
                            || newDiscount.Applied_through__c != oldDiscount.Applied_through__c
                            || newDiscount.Discount_Type_Studies__c != oldDiscount.Discount_Type_Studies__c
                            || newDiscount.Company_from_Company_Discount__c != oldDiscount.Company_from_Company_Discount__c
                    )
                    ) {
                companyDiscountsToCheckIfCanEditFields.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // disable editing company after discount is defienied
            if (newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT
                    && oldDiscount.Company_from_Company_Discount__c != newDiscount.Company_from_Company_Discount__c) {
                newDiscount.addError(Label.msg_error_cantEditCompany);
            }

            /* Sebastian Łasisz */
            // Trigger to check whether can add company to already defined company discount
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_ACCOUNT_COMPANY_DC)
                    && oldDiscount.Company_from_Company_Discount__c != newDiscount.Company_from_Company_Discount__c) {
                companyDiscountToCheckIfCanAdd.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Check whether insterted Offer Connector matches Level Details on Discount
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_OFFER_CODE_DC)
                    && newDiscount.Offer_from_Offer_DC__c != oldDiscount.Offer_from_Offer_DC__c) {
                offerCodeDiscountConnectorsToValidate.add(newDiscount);
            }
        }
    }

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        for (Id key : newDiscounts.keySet()) {
            Discount__c oldDiscount = oldDiscounts.get(key);
            Discount__c newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // after applying Company Discount disapply other Tuition fee Discounts
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Applied__c != oldDiscount.Applied__c
                    && newDiscount.Applied__c == true
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT) {
                companyDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // after applying Employee Discount disapply other Tuition fee Discounts
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Applied__c != oldDiscount.Applied__c
                    && newDiscount.Applied__c == true
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_EMPLOYEEDISCOUNT) {
                employeeDiscountToDisapplitOthers.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // trigger to update Enrollment Payment Schedule after Discount update
            if (
                    !CommonUtility.skipDiscountApplication
                            && (
                            newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                                    || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    )
                            && (
                            newDiscount.Applied__c != oldDiscount.Applied__c
                                    || newDiscount.Applied_through__c != oldDiscount.Applied_through__c
                                    || newDiscount.Applies_to__c != oldDiscount.Applies_to__c
                                    || newDiscount.Discount_Value__c != oldDiscount.Discount_Value__c
                                    || newDiscount.Discount_Kind__c != oldDiscount.Discount_Kind__c
                    )
                    ) {
                studyEnrToUpdatePaymentSchDiscountVal.add(newDiscount.Enrollment__c);
            }

            /* Wojciech Słodziak */
            // on chancellor Manual Discount update with Applied__c = true disapply other discounts of the same Applies_to__c type
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_CHANCELLOR
                    && newDiscount.Applied__c == true && oldDiscount.Applied__c != newDiscount.Applied__c) {
                chancellorDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Wojciech Słodziak */
            // on VIS Manual Discount update with Applied__c = true disapply other Tuition fee Discounts
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_VIS
                    && newDiscount.Applied__c == true && oldDiscount.Applied__c != newDiscount.Applied__c) {
                visDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // on Second Course Manual Discount update with Applied__c = true disapply other Tuition fee Discounts
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE && newDiscount.Applied__c == true) {
                secondCourseDiscountsToDisapplyOther.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to change for delivery in stage when discount activity is changed
            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT
                    && newDiscount.Applied__c != oldDiscount.Applied__c) {
                companyDiscountsToUpdateCompanyDocument.add(newDiscount);
            }

            /* Sebastian Łasisz */
            // On second course discount update change secound course checkbox on enrollment
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE
                    && newDiscount.Applied__c != oldDiscount.Applied__c
                    && newDiscount.Applied__c == true) {
                enrollmentsToAddSecondCourse.add(newDiscount.Enrollment__c);
            }

            if ((newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    || (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                    && newDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_CODE))
                    && newDiscount.Applied__c != oldDiscount.Applied__c
                    && newDiscount.Applied__c == true) {
                manageAppliedDiscounts.add(newDiscount);
            }

        }
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Discount__c oldDiscount;
        for (Id key : oldDiscounts.keySet()) {
            oldDiscount = oldDiscounts.get(key);

            /* Wojciech Słodziak */
            // trigger to update Enrollment Payment Schedule after Discount delete
            if (
                    !CommonUtility.skipDiscountApplication
                            && (
                            oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR)
                                    || oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT)
                    )
                            && oldDiscount.Applied__c == true) {
                studyEnrToUpdatePaymentSchDiscountVal.add(oldDiscount.Enrollment__c);
            }

            /* Sebastian Łasisz */
            // Trigger to remove company discount document when Company Discount is deleted.
            if ((oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && oldDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT) {
                companyDiscountsToRemoveDocument.add(oldDiscount);
            }

            /* Sebastian Łasisz */
            // Trigger to change for delivery in stage when discount activity is changed
            if ((oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR) || oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_MANUAL_DISCOUNT))
                    && oldDiscount.Discount_Type_Studies__c == CommonUtility.DISCOUNT_TYPEST_COMPANYDISCOUNT) {
                companyDiscountsToUpdateCompanyDocument.add(oldDiscount);
            }

        }
    }

    public override void finish() {

        /* Sebastian Łasisz */
        // Check whether insterted Offer Connector matches Level Details on Discount (KEEP AT TOP)
        if (offerCodeDiscountConnectorsToValidate != null && !offerCodeDiscountConnectorsToValidate.isEmpty()) {
            DiscountManager_Studies.validateOfferCodeDiscountConnector(offerCodeDiscountConnectorsToValidate);
        }

        /* Sebastian Łasisz */
        // Trigger to check whether can add company to already defined company discount
        if (companyDiscountToCheckIfCanAdd != null && !companyDiscountToCheckIfCanAdd.isEmpty()) {
            DiscountManager_Studies.disableAddingCompanyToDiscount(companyDiscountToCheckIfCanAdd);
        }

        /* Sebastian Łasisz */
        // disable editing time discount if its onnected to enrollments
        if (timeDiscountsToValidate != null && !timeDiscountsToValidate.isEmpty()) {
            DiscountManager_Studies.disableEditingDiscountIfPossible(timeDiscountsToValidate);
        }

        /* Sebastian Łasisz */
        // Trigger to generate name for newly created company discount connector
        if (companyDiscountsToSetName != null && !companyDiscountsToSetName.isEmpty()) {
            DiscountManager_Studies.generateNameForUniversityStudyOffer(companyDiscountsToSetName);
        }

        /* Sebastian Łasisz */
        // Trigger to generate company discount document after discount is added
        if (docEnrIdsToAddCompanyDocument != null && !docEnrIdsToAddCompanyDocument.isEmpty()) {
            DocumentManager.createCompanyDiscountDocument(docEnrIdsToAddCompanyDocument);
        }

        /* Sebastian Łasisz */
        // Trigger to remove company discount document when Company Discount is deleted.
        if (companyDiscountsToRemoveDocument != null && !companyDiscountsToRemoveDocument.isEmpty()) {
            DocumentManager.removeCompanyDocumentDiscount(companyDiscountsToRemoveDocument);
        }

        /* Sebastian Łasisz */
        // Trigger to disable adding discounts connectors to offers from different university than main offer
        if (discConnToCheckOfferUniversity != null && !discConnToCheckOfferUniversity.isEmpty()) {
            DiscountManager_Studies.checkIfConnectorIsAttachedToProperOffer(discConnToCheckOfferUniversity);
        }

        /* Wojciech Słodziak */
        // duplicate detection for Offer Discount Connectors
        if (discConnectorsToCheckDuplicates != null && !discConnectorsToCheckDuplicates.isEmpty()) {
            DiscountManager_Studies.checkOfferDiscountConnectorDuplicates(discConnectorsToCheckDuplicates);
        }

        /* Wojciech Słodziak */
        // on Recommended Person Discount insert check if limit of 5 same Discounts on Enrollment wasn't exceeded
        if (recDiscountsToCheckLimit != null && !recDiscountsToCheckLimit.isEmpty()) {
            DiscountManager_Studies.checkRecPersonDiscountLimit(recDiscountsToCheckLimit);
        }

        /* Sebastian Łasisz */
        // On second course discount insert or update change secound course checkbox on enrollment
        if (enrollmentsToAddSecondCourse != null && !enrollmentsToAddSecondCourse.isEmpty()) {
            EnrollmentManager_Studies.changeSecondCourseCheckboxOnEnrollment(enrollmentsToAddSecondCourse);
        }

        /* Wojciech Słodziak */
        // on Offer Time DC insert check if Valid from / to dates are not overlapping
        if (offerDCToCheckIfCanInsert != null && !offerDCToCheckIfCanInsert.isEmpty()) {
            DiscountManager_Studies.checkInsertedOfferTimeDCDatesOverlapping(offerDCToCheckIfCanInsert);
        }

        /* Wojciech Słodziak */
        // on Time Discount update check if it's Offer Discount Connectors are not overlapping with other Time Discounts
        if (timeDiscountsToCheckUpdateValidity != null && !timeDiscountsToCheckUpdateValidity.isEmpty()) {
            DiscountManager_Studies.checkEditedTimeDiscountsValidity(timeDiscountsToCheckUpdateValidity);
        }

        /* Wojciech Słodziak */
        // after applying Company Discount disapply other Tuition fee Discounts
        if (companyDiscountsToDisapplyOther != null && !companyDiscountsToDisapplyOther.isEmpty()) {
            //DiscountManager_Studies.disapplyOtherDiscountsOfTheSameAppliesToType(companyDiscountsToDisapplyOther);
        }

        /* Sebastian Łasisz */
        // after applying Employee Discount disapply other Tuition fee Discounts
        if (employeeDiscountToDisapplitOthers != null && !employeeDiscountToDisapplitOthers.isEmpty()) {
            //DiscountManager_Studies.disapplyOtherDiscountsOfTheSameAppliesToType(employeeDiscountToDisapplitOthers);
        }

        /* Wojciech Słodziak */
        // on chancellor Manual Discount insert / update with Applied__c = true disapply other discounts of the same Applies_to__c type
        if (chancellorDiscountsToDisapplyOther != null && !chancellorDiscountsToDisapplyOther.isEmpty()) {
            //DiscountManager_Studies.disapplyOtherDiscountsOfTheSameAppliesToType(chancellorDiscountsToDisapplyOther);
        }

        /* Wojciech Słodziak */
        // on VIS Manual Discount insert / update with Applied__c = true disapply other Tuition fee Discounts
        if (visDiscountsToDisapplyOther != null && !visDiscountsToDisapplyOther.isEmpty()) {
            //DiscountManager_Studies.disapplyOtherDiscountsOfTheSameAppliesToType(visDiscountsToDisapplyOther);
        }

        /* Wojciech Słodziak */
        // on Second Course Manual Discount insert / update with Applied__c = true disapply other Tuition fee Discounts
        if (secondCourseDiscountsToDisapplyOther != null && !secondCourseDiscountsToDisapplyOther.isEmpty()) {
            //DiscountManager_Studies.disapplyOtherDiscountsOfTheSameAppliesToType(secondCourseDiscountsToDisapplyOther);
        }

        /* Sebastian Łasisz */
        // on time discount connector check if can edit fields
        if (timeDiscountsToCheckIfCanEditFields != null && !timeDiscountsToCheckIfCanEditFields.isEmpty()) {
            //DiscountManager_Studies.checkIfCanEditFieldsOnTimeConnector(timeDiscountsToCheckIfCanEditFields);
        }

        /* Sebastian Łasisz */
        // on company discount connector check if can edit fields
        if (companyDiscountsToCheckIfCanEditFields != null && !companyDiscountsToCheckIfCanEditFields.isEmpty()) {
            //DiscountManager_Studies.checkIfCanEditFieldsOnCompanyConnector(companyDiscountsToCheckIfCanEditFields);
        }

        /* Sebastian Łasisz */
        // Rewrite Trade Name from Discount Definition
        if (discountsToRewriteTradeName != null && !discountsToRewriteTradeName.isEmpty()) {
            DiscountManager_Studies.rewriteTradeNameOnDiscounts(discountsToRewriteTradeName);
        }

        /* Sebastian Łasisz */
        // Trigger to change for delivery in stage when discount activity is changed
        if (companyDiscountsToUpdateCompanyDocument != null && !companyDiscountsToUpdateCompanyDocument.isEmpty()) {
            DocumentManager.updateCompanyDocumentDiscountFDIS(companyDiscountsToUpdateCompanyDocument);
        }

        /* Wojciech Słodziak */
        // run recalculation of Discount Value on Payment Schedule for listed Enrollment Ids (KEEP AT BOTTOM!)
        if (studyEnrToUpdatePaymentSchDiscountVal != null && !studyEnrToUpdatePaymentSchDiscountVal.isEmpty()) {
            DiscountApplicationManager_Studies.applyDiscountsToPaymentSchedule(studyEnrToUpdatePaymentSchDiscountVal);
        }

        /* Sebastian Łasisz */
        // when newly created discount is added disapply non matching discounts
        if (manageAppliedDiscounts != null && !manageAppliedDiscounts.isEmpty()) {
            DiscountApplicationManager_Studies.manageAppliedDiscounts(manageAppliedDiscounts);
        }

        /* Sebastian Łasisz */
        // when newly created company discount is added disapply non matching discounts (ONLY WORKS FROM BUTTON)
        if (companyDiscountsToDisaplyOthers != null && !companyDiscountsToDisaplyOthers.isEmpty()) {
            DiscountApplicationManager_Studies.dissaplyOtherDiscounts(companyDiscountsToDisaplyOthers);
        }

    }

}