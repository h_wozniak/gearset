@isTest
private class ZTestGenerateStudyModeForUnenrolled {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    @isTest static void testEnrollmentModeFullTime() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);

        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(studyEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();
    }
    
    @isTest static void testEnrollmentModeWeekend() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        

        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(studyEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();
    }
    
    @isTest static void testEnrollmentModeOnline() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_ONLINE,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        

        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(studyEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();
    }
    
    @isTest static void testDocumentEnrollmentModeFullTime() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_FULL_TIME,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);


        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();
    }
    
    @isTest static void testDocumentEnrollmentModeWeekend() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_WEEKEND,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);


        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();     
    }
    
    @isTest static void testDocumentEnrollmentModeOnline() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_ONLINE,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);


        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            System.assertEquals(result, ' <w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stacjonarnej/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> niestacjonarnej weekendowej/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>niestacjonarnej on-line</w:t></w:r>');
        }
        Test.stopTest();
    }
    
    @isTest static void testDocumentEnrollmentModeEvening() {
        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
            Mode__c = CommonUtility.OFFER_MODE_EVENING,
            Number_of_Semesters__c = 5
        ), true);
        
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I, 
            Course_or_Specialty_Offer__c = courseOffer.Id
        ), true);
        
        Enrollment__c documentEnrollment = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = studyEnrollment.Id
        ), true);


        User u = createENLangUser_forStudies();

        System.runAs(u) {
            String result = DocGen_GenerateStudyModeForUnenrolled.executeMethod(documentEnrollment.Id, null, null);
            System.assertEquals(result, '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + CommonUtility.OFFER_MODE_EVENING + '</w:t></w:r>');
        }
        Test.stopTest();
    }
    
}