/**
* @author       Wojciech Słodziak
* @description  Controller for EnrollmentJSONDisplay.
**/

public with sharing class EnrollmentJSONDisplayController {

    public Id enrollmentId { get; set; }
    public Enrollment__c enr { get; set; }

    public String enrJSON { get; set; }

    public EnrollmentJSONDisplayController() {
        enrollmentId = Apexpages.currentPage().getParameters().get('enrId');
        
        enr = [SELECT Enrollment_JSON__c FROM Enrollment__c WHERE Id = :enrollmentId];

        if (enr.Enrollment_JSON__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_NoEnrollmentJSON));
        } else {
            IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON enr = (IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON) JSON.deserialize(enr.Enrollment_JSON__c, IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON.class);

            enrJSON = enr.toJSON();
        }
    }

    public PageReference cancel() {
        if (enrollmentId != null) {
            return new PageReference('/' + enrollmentId);
        }
        return null;
    }
}