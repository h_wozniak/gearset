/**
*   @author         Wojciech Słodziak
*   @description    Batch class for finalizing TrainingSalesTargetComputationsBatch calculations, updates Sales Targets if Realization Target is changed.
**/

global without sharing class TrainingSalesTargetFinalizationBatch implements Database.Batchable<sObject> {
    
    Boolean isRunManually;
    Set<Id> targetRecordTypeIds;
    
    global TrainingSalesTargetFinalizationBatch(Boolean isRunManually) {
        this.isRunManually = isRunManually;

        targetRecordTypeIds = new Set<Id> {
            CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TRAINING_ENR_TARGET)
        };
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Realization_Amount_curr__c, Realization_Amount_Batch_Helper_curr__c
            FROM Target__c
            WHERE RecordTypeId IN :targetRecordTypeIds
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Target__c> targetList = (List<Target__c>) scope;

        for (Target__c target : targetList) {
            target.Realization_Amount_curr__c = target.Realization_Amount_Batch_Helper_curr__c;
            target.Realization_Amount_Batch_Helper_curr__c = 0;
            target.Last_Update_Date__c = System.now();
        }

        try {
            update targetList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (isRunManually) {
            EmailManager.sendNotificationToUser(UserInfo.getUserId(), Label.msg_info_SalesTargetRecalculationFinished);
        }
    }
    
}