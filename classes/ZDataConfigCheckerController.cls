@isTest
private class ZDataConfigCheckerController {

	@isTest static void test_checkDocuments() { 
		Test.startTest();
		ConfigCheckerController.Response_Wrapper result = ConfigCheckerController.checkDocuments();
		Test.stopTest();

		System.assertEquals(result.result, false);
		System.assertEquals(result.missingList.size(), 9);
		System.assert(result.message.contains(Label.msg_error_MissingCatalogDocuments));
	}

	@isTest static void test_checkUniversities() { 
		Test.startTest();
		ConfigCheckerController.Response_Wrapper result = ConfigCheckerController.checkUniversities();
		Test.stopTest();

		System.assertEquals(result.result, false);
		System.assertEquals(result.missingList.size(), 9);
		System.assert(result.message.contains(Label.msg_error_MissingWsbNames));
	}

	@isTest static void test_checkMarketingConsent() { 
		Test.startTest();
		ConfigCheckerController.Response_Wrapper result = ConfigCheckerController.checkMarketingConsent();
		Test.stopTest();

		System.assertEquals(result.result, false);
		System.assertEquals(result.missingList.size(), 11);
		System.assert(result.message.contains(Label.msg_error_MissingMarketingConsents));
	}

	@isTest static void test_checkFooters() {
		Test.startTest();
		ConfigCheckerController.Response_Wrapper result = ConfigCheckerController.checkFooters();
		Test.stopTest(); 

		System.assertEquals(result.result, false);
		System.assertEquals(result.missingList.size(), 5);
		System.assert(result.message.contains(Label.msg_error_MissingFooter));
	}

	@isTest static void test_checkStudentCardCS() { 
		Test.startTest();
		ConfigCheckerController.Response_Wrapper result = ConfigCheckerController.checkStudentCardCS();
		Test.stopTest();

		System.assertEquals(result.result, false);
		System.assertEquals(result.missingList.size(), 1);
		System.assert(result.message.contains(Label.msg_error_MissingCustomSetting));
	}
}