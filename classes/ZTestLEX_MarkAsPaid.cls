@isTest
private class ZTestLEX_MarkAsPaid {

    @isTest static void test_markEnrollmentsAsPaid_Offer_Canceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
                new Enrollment__c(
                        Training_Offer__c = trainingOffer.Id,
                        Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
                ), true);

        Test.startTest();
        String result = LEX_MarkAsPaid.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');

        System.assertEquals(result, 'true');

        List<Enrollment__c> relatedEnrollments = [
                SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c
                WHERE Training_Offer__c = :trainingOffer.Id
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];

        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Last_Active_status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }

        Test.stopTest();
    }

    @isTest static void test_markEnrollmentsAsPaid_Offer_NotCanceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
                new Enrollment__c(
                        Training_Offer__c = trainingOffer.Id,
                        Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
                ), true);

        Test.startTest();
        String result = LEX_MarkAsPaid.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');

        System.assertEquals(result, 'true');

        List<Enrollment__c> relatedEnrollments = [
                SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c
                WHERE Training_Offer__c = :trainingOffer.Id
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];

        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }

        Test.stopTest();
    }

    @isTest static void test_markEnrollmentsAsPaid_Offer_WithoutEnrollments() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);

        Test.startTest();
        String result = LEX_MarkAsPaid.markEnrollmentsAsPaid(trainingOffer.Id, 'Offer__c');

        System.assertEquals(result, 'no_main_enrollment');

        Test.stopTest();
    }

    @isTest static void test_markEnrollmentsAsPaid_Enrollment_Canceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
                new Enrollment__c(
                        Training_Offer__c = trainingOffer.Id,
                        Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_CANCELED
                ), true);

        Test.startTest();
        String result = LEX_MarkAsPaid.markEnrollmentsAsPaid(closedTraining.Id, 'Enrollment__c');

        System.assertEquals(result, 'true');

        List<Enrollment__c> relatedEnrollments = [
                SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c
                WHERE Training_Offer__c = :trainingOffer.Id
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];

        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Last_Active_status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }

        Test.stopTest();
    }

    @isTest static void test_markEnrollmentsAsPaid_Enrollment_NotCanceled() {
        Offer__c trainingOffer = ZDataTestUtility.createClosedTrainingSchedule(null, true);
        Enrollment__c closedTraining = ZDataTestUtility.createClosedTrainingEnrollment(
                new Enrollment__c(
                        Training_Offer__c = trainingOffer.Id,
                        Status__c = CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED
                ), true);

        Test.startTest();
        String result = LEX_MarkAsPaid.markEnrollmentsAsPaid(closedTraining.Id, 'Enrollment__c');

        System.assertEquals(result, 'true');

        List<Enrollment__c> relatedEnrollments = [
                SELECT Id, Status__c, Last_Active_status__c FROM Enrollment__c
                WHERE Training_Offer__c = :trainingOffer.Id
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
        ];

        for (Enrollment__c relatedEnrollment : relatedEnrollments) {
            System.assertEquals(relatedEnrollment.Status__c, CommonUtility.ENROLLMENT_STATUS_PAID);
        }

        Test.stopTest();
    }
}