public without sharing class GenerateDocumentsQueueable implements Queueable, Database.AllowsCallouts {

    public Integer queueableDepth;
    public List<DocumentWrapper> documentWrapperList;
    public Set<Id> enrollmentIds;

    public GenerateDocumentsQueueable(List<DocumentWrapper> documentWrapperList, Set<Id> enrollmentIds, Integer queueableDepth) {
        this.queueableDepth = queueableDepth;
        this.documentWrapperList = documentWrapperList;
        this.enrollmentIds = enrollmentIds;
    }

    public void execute(QueueableContext qc) {
        CommonUtility.preventSendingEmailWithDocuments = true;

        if (queueableDepth < documentWrapperList.size()) {
            if (documentWrapperList.get(queueableDepth).generatePassword) {
                CommonUtility.preventUpdatingCurrentFileOnEnrollment = true;
            }

            DocumentGeneratorManager.generateDocumentSync(
                    documentWrapperList.get(queueableDepth).language,
                    documentWrapperList.get(queueableDepth).docName,
                    documentWrapperList.get(queueableDepth).degree,
                    documentWrapperList.get(queueableDepth).universityName,
                    documentWrapperList.get(queueableDepth).referenceNumber,
                    documentWrapperList.get(queueableDepth).pinAttachmentToId,
                    documentWrapperList.get(queueableDepth).toPdf,
                    documentWrapperList.get(queueableDepth).generatePassword
            );

            queueableDepth++;
            if (!Test.isRunningTest()) {
                System.enqueueJob(new GenerateDocumentsQueueable(documentWrapperList, enrollmentIds, queueableDepth));
            }
        }
        else if (queueableDepth == documentWrapperList.size()) {
            if (enrollmentIds != null && !enrollmentIds.isEmpty()) {
                AttachmentManager.determineEnrollmentsToSendEmail(enrollmentIds);
            }
        }
    }

    public class DocumentWrapper {
        String language;
        String docName;
        String degree;
        String universityName;
        Decimal referenceNumber;
        Id pinAttachmentToId;
        Boolean toPdf;
        Boolean generatePassword;

        public DocumentWrapper(String language, String docName, String degree, String universityName, Decimal referenceNumber, Id pinAttachmentToId, Boolean toPdf, Boolean generatePassword) {
            this.language = language;
            this.docName = docName;
            this.degree = degree;
            this.universityName = universityName;
            this.referenceNumber = referenceNumber;
            this.pinAttachmentToId = pinAttachmentToId;
            this.toPdf = toPdf;
            this.generatePassword = generatePassword;
        }
    }
}