@isTest
private class ZTestClassifierMappingController {

    private static Marketing_Campaign__c prepareData() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);     

        return salesCampaign;
    }
    
    @isTest static void test_ClassifierMappingController() {
        Marketing_Campaign__c camp = prepareData();

        ApexPages.CurrentPage().getParameters().put('retURL', camp.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', camp.Id);

        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        Test.stopTest();
    }
    
    @isTest static void test_acquireExternalClassifiers() { 
        Marketing_Campaign__c camp = prepareData();

        ApexPages.CurrentPage().getParameters().put('retURL', camp.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', camp.Id);

        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        Test.stopTest();
    }
    
    @isTest static void test_save_withoutData() {
        
        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        PageReference save = cntrl.save();
        Test.stopTest();
        
        System.assertNotEquals(null, save);
    }
    
    /* to fill */
    /* test to be changed after DTICRMDEV-376 - Karolina Kowalik
    @isTest static void test_save_withData() {
        Marketing_Campaign__c camp = prepareData();

        ApexPages.CurrentPage().getParameters().put('retURL', camp.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', camp.Id);
        
        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        PageReference save = cntrl.save();
        Test.stopTest();
        
        System.assertNotEquals(null, save);
    }*/
    
    @isTest static void test_cancel_withoutData() {
        prepareData();
        
        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        PageReference cancel = cntrl.cancel();
        Test.stopTest();
        
        System.assertNotEquals(null, cancel);
    }
    /* to fill */
    @isTest static void test_cancel_withData() {
        Marketing_Campaign__c camp = prepareData();

        ApexPages.CurrentPage().getParameters().put('retURL', camp.Id);
        ApexPages.CurrentPage().getParameters().put('campaignId', camp.Id);
        
        Test.startTest();
        ClassifierMappingController cntrl = new ClassifierMappingController();
        PageReference cancel = cntrl.cancel();
        Test.stopTest();
        
        System.assertNotEquals(null, cancel);
    }
}