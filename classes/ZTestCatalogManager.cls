@isTest
private class ZTestCatalogManager {
    
    private static Map<Integer, sObject> prepareData() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createBaseConsent(new Catalog__c(Not_Changeable_Name__c = false), true));
        retMap.put(1, ZDataTestUtility.createBaseConsent(new Catalog__c(Not_Changeable_Name__c = true), true));

        return retMap;
    }

    @isTest static void test_changeConsentName() {
    	Map<Integer, sObject> dataMap = prepareData();
    	Catalog__c consent = (Catalog__c)dataMap.get(0);

    	consent.Name = 'New consent Name';

    	Boolean errorOccured = false;
    	try {
    		update consent;
    	}
    	catch (Exception e) {
    		errorOccured = true;
    	}
		
		System.assert(!errorOccured);

    	Catalog__c consentAfterNameChange = [
    		SELECT Id, Name
    		FROM Catalog__c
    		WHERE Id = :consent.Id
    	];

    	System.assertEquals(consentAfterNameChange.Name, 'New consent Name');
    }

    @isTest static void test_changeConsentName_NotChangeableName() {
    	Map<Integer, sObject> dataMap = prepareData();
    	Catalog__c consent = (Catalog__c)dataMap.get(1);
    	String consentNamePreUpdate = consent.Name;

    	consent.Name = 'New consent Name';

    	Boolean errorOccured = false;
    	try {
    		update consent;
    	}
    	catch (Exception e) {
    		if (e.getMessage().contains(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Catalog__c', 'Name'))) {
    			errorOccured = true;
    		}
    	}

    	System.assert(errorOccured);

    	Catalog__c consentAfterNameChange = [
    		SELECT Id, Name
    		FROM Catalog__c
    		WHERE Id = :consent.Id
    	];

    	System.assertEquals(consentNamePreUpdate, consentAfterNameChange.Name);
    }

    @isTest static void test_detectNameDuplicates_differentConsentNames() {
    	Catalog__c firstConsent = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = 'First Consent'), false);
    	Catalog__c secondConsent = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = 'Second consent'), false);

    	Boolean errorOccured = false;
    	try {
    		insert firstConsent;
    		insert secondConsent;
    	}
    	catch (Exception e) {
    		errorOccured = true;
    	}

    	System.assert(!errorOccured);
    }

    @isTest static void test_detectNameDuplicates_sameConsentNames() {
    	Catalog__c firstConsent = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = 'First Consent'), false);
    	Catalog__c secondConsent = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = 'First Consent'), false);

    	Boolean errorOccured = false;
    	try {
    		insert firstConsent;
    		insert secondConsent;
    	}
    	catch (Exception e) {
    		if (e.getMessage().contains(' ' + Label.msg_error_DupFound + ': <a target="_blank" href="/' + firstConsent.Id + '">' + firstConsent.Name + '</a>')) {
    			errorOccured = true;    			
    		}
    	}

    	System.assert(errorOccured);
    }

}