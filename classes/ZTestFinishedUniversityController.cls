@isTest
private class ZTestFinishedUniversityController {
    
    @isTest static void test_controller() {
        PageReference pageRef = Page.FinishedUniversity;
        Test.setCurrentPageReference(pageRef);
        
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        ApexPages.CurrentPage().getparameters().put('candidateId', candidate.Id);
        
        FinishedUniversityController contr = new FinishedUniversityController();
        
        System.assertEquals(contr.idMatch, candidate.Id);
        System.assertNotEquals(contr.finishedUniversity, null);
    }
    
    @isTest static void test_save() {
        PageReference pageRef = Page.FinishedUniversity;
        Test.setCurrentPageReference(pageRef);
        
        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        ApexPages.CurrentPage().getparameters().put('candidateId', candidate.Id);
        
        FinishedUniversityController contr = new FinishedUniversityController();
        contr.finishedUniversity.ZPI_University_Name__c = 'Politechnika';
        contr.finishedUniversity.ZPI_University_City__c = 'Wrocław';
        contr.save();
        
        List<Qualification__c> finishedUniversities = [SELECT Id FROM Qualification__c];
        System.assert(finishedUniversities.size() > 0);
    }
}