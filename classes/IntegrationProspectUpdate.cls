/**
*   @author         Sebastian Łasisz
*   @description    This class is used to receive data for Propsect Integration
**/

@RestResource(urlMapping = '/prospect_create/*')
global without sharing class IntegrationProspectUpdate {

    @HttpPost
    global static void receiveContactInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String jsonBody = req.requestBody.toString();

        IntegrationManager.Prospect_JSON parsedJson;

        try {
            parsedJson = parse(jsonBody);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(601));
            return ;
        }

        try {
            validateJSON(parsedJson);
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(800, e.getMessage()));
            res.statusCode = 400;
            return ;
        }

        Contact contactToUpdate;
        if (parsedJson != null) {
            Boolean isInserted = false;
            String areasOfInterest;

            if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_EXPERIA) {
                CommonUtility.preventSendingDataToExperia = true;
            } else if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_MA) {
                CommonUtility.preventSendingDataToIPresso = true;
            } else if (parsedJson.updating_system == RecordVals.UPDATING_SYSTEM_ZPI) {
                CommonUtility.preventSendingDataToZPI = true;
            }

            try {
                if (parsedJson.updating_system_contact_id != null) {
                    contactToUpdate = [
                            SELECT Id, FirstName, LastName, Email, Consent_Marketing_ADO__c, Consent_Graduates_ADO__c, Consent_Electronic_Communication_ADO__c,
                                    Consent_Direct_Communications_ADO__c
                            FROM Contact
                            WHERE Id = :parsedJson.updating_system_contact_id
                    ];

                    //contactToUpdate = new Contact(
                    //    Id = parsedJson.updating_system_contact_id,
                    //    RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
                    contactToUpdate.FirstName = parsedJson.first_name;
                    contactToUpdate.LastName = parsedJson.last_name;
                    contactToUpdate.Email = parsedJson.email;
                    //);
                    contactToUpdate = updatePhones(contactToUpdate, parsedJson.phones);
                    contactToUpdate = updateConsents(contactToUpdate, parsedJson.consents);
                    areasOfInterest = setAreasOfInterest(contactToUpdate, parsedJson.campaign_details);

                    CommonUtility.preventTriggeringEmailValidation = true;
                    CommonUtility.preventChangingEmailTwice = true;


                    update contactToUpdate;
                }
                else {
                    contactToUpdate = new Contact(
                            FirstName = parsedJson.first_name,
                            RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CANDIDATE),
                            LastName = parsedJson.last_name,
                            Email = parsedJson.email
                    );
                    contactToUpdate = updatePhones(contactToUpdate, parsedJson.phones);
                    contactToUpdate = updateConsents(contactToUpdate, parsedJson.consents);
                    isInserted = true;

                    areasOfInterest = setAreasOfInterest(contactToUpdate, parsedJson.campaign_details);

                    CommonUtility.preventTriggeringEmailValidation = true;
                    CommonUtility.preventChangingEmailTwice = true;


                    insert contactToUpdate;
                }

                createMarketingCampaigns(jsonBody, res, isInserted, contactToUpdate, areasOfInterest, parsedJson.campaign_id);

                IntegrationError.Success_JSON errObj = new IntegrationError.Success_JSON();
                errObj.status_code = 200;
                errObj.message = 'success';

                res.responseBody = Blob.valueOf(JSON.serialize(errObj));
                res.statusCode = 200;
            }
            catch(Exception e) {
                ErrorLogger.log(e);
                if (e.getMessage().contains('Invalid id')) {
                    res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(604));
                }
                else if (e.getMessage().contains('Duplicate found')) {
                    res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(603));
                }
                else {
                    res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
                }
                res.statusCode = 400;
            }
        }
    }

    public static String setAreasOfInterest(Contact prospectToUpdate, List<IntegrationManager.Campaign_Details_JSON> details) {
        String areasOfInterest = '';
        if (details != null) {
            for (IntegrationManager.Campaign_Details_JSON campaignDetail : details) {
                if (campaignDetail.detail_name != null && campaignDetail.detail_name == 'areas_of_interest') {
                    areasOfInterest = campaignDetail.detail_value;
                }
                else if (campaignDetail.detail_name != null) {
                    prospectToUpdate.put(campaignDetail.detail_name, campaignDetail.detail_value);
                }
            }
        }

        return areasOfInterest;
    }

    public static void createMarketingCampaigns(String jsonBody, RestResponse res, Boolean isInserted, Contact contactToUpdate, String areasOfInterest, String campaign_id) {
        List<Marketing_Campaign__c> marketingCampaings = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE Campaign_Contact__c = :contactToUpdate.Id
                AND Campaign_Member__c = :campaign_id
        ];

        isInserted = marketingCampaings.size() == 0;

        if (isInserted) {
            updateMarketingCampaigns(jsonBody, res, contactToUpdate, areasOfInterest, campaign_id);
        }
        else {
            createMarketingDetailsAndMembers(jsonBody, res, contactToUpdate, areasOfInterest);
        }
    }

    public static void updateMarketingCampaigns(String jsonBody, RestResponse res, Contact contactToUpdate, String areasOfInterest, String campaign_id) {
        try {
            Marketing_Campaign__c campaignDetails = MarketingCampaignSalesManager.createCampaignMemberDetailsForSales(contactToUpdate, areasOfInterest);
            insert campaignDetails;
            MarketingCampaignSalesManager.createMembersOnSalesCampaign(jsonBody, contactToUpdate, campaign_id, campaignDetails.Id);
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            if (e.getMessage().contains('Invalid id')) {
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(604));
            }
            else if (e.getMessage().contains('Duplicate found')) {
                res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(603));
            }
            res.statusCode = 400;
        }
    }

    public static void createMarketingDetailsAndMembers(String jsonBody, RestResponse res, Contact contactToUpdate, String areasOfInterest) {
        List<Marketing_Campaign__c> campaignMembersWithContact = [
                SELECT Id, Campaign_from_Campaign_Details_2__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
                AND Campaign_Member__r.Campaign_Kind__c = :CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT
                AND Campaign_Contact__c = :contactToUpdate.Id
        ];

        Set<Id> marketingDetailsIds = new Set<Id>();
        for (Marketing_Campaign__c campaignMemberWithContact : campaignMembersWithContact) {
            marketingDetailsIds.add(campaignMemberWithContact.Campaign_from_Campaign_Details_2__c);
        }

        List<Marketing_Campaign__c> marketingDetails = [
                SELECT Id, Details_Contact_First_Name__c, Details_Contact_Last_Name__c, Details_Contact_Email__c, Details_Contact_Phone_Numbers__c, Campaign_Areas_of_Interest__c
                FROM Marketing_Campaign__c
                WHERE Id IN :marketingDetailsIds
        ];

        List<Marketing_Campaign__c> detailsToUpdate = new List<Marketing_Campaign__c>();
        for (Marketing_Campaign__c marketingDetail : marketingDetails) {
            Marketing_Campaign__c detailsToAdd = MarketingCampaignSalesManager.createCampaignMemberDetailsForSales(contactToUpdate, areasOfInterest);
            detailsToAdd.Id = marketingDetail.Id;
            //detailsToAdd.Campaign_JSON__c = jsonBody;
            detailsToUpdate.add(detailsToAdd);
        }

        try {
            update detailsToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(600));
            res.statusCode = 400;
        }
    }

    public static Contact updatePhones(Contact contactToUpdate, List<IntegrationManager.Phone_JSON> phones) {
        contactToUpdate.Additional_Phones__c = '';
        for (IntegrationManager.Phone_JSON phone : phones) {
            if (phone.type == CommonUtility.CONTACT_PHONE_MOBILE) {
                contactToUpdate.MobilePhone = phone.phoneNumber;
            }
            else if (phone.type == CommonUtility.CONTACT_PHONE_DEFAULT) {
                contactToUpdate.Phone = phone.phoneNumber;
            }
            else {
                contactToUpdate.Additional_Phones__c += phone.phoneNumber + '; ';
            }
        }
        contactToUpdate.Additional_Phones__c = contactToUpdate.Additional_Phones__c.left(contactToUpdate.Additional_Phones__c.length() - 2);

        return contactToUpdate;
    }

    public static Contact updateConsents(Contact contactToUpdate, List<IntegrationManager.MarketingConsents_JSON> consents) {
        for (IntegrationManager.MarketingConsents_JSON consent : consents) {
            if (consent.value == true) {
                contactToUpdate = MarketingConsentManager.updateConsentOnContact(consent.department, contactToUpdate, CatalogManager.getApiADONameFromConsentName(consent.type));
            }
        }

        return contactToUpdate;
    }

    private static IntegrationManager.Prospect_JSON parse(String jsonBody) {
        return (IntegrationManager.Prospect_JSON) System.JSON.deserialize(jsonBody, IntegrationManager.Prospect_JSON.class);
    }

    private static void validateJSON(IntegrationManager.Prospect_JSON parsedJson) {
        if (parsedJson.updating_system == null) {
            throw new InvalidFieldValueException('updating_system');
        }
        if (parsedJson.first_name == null) {
            throw new InvalidFieldValueException('first_name');
        }
        if (parsedJson.last_name == null) {
            throw new InvalidFieldValueException('last_name');
        }
        if (parsedJson.phones != null) {
            for (IntegrationManager.Phone_JSON phone : parsedJson.phones) {
                if (phone.type == null) {
                    throw new InvalidFieldValueException('type');

                }
                if (phone.phoneNumber == null) {
                    throw new InvalidFieldValueException('phoneNumber');

                }
            }
        }
        if (parsedJson.consents == null) {
            throw new InvalidFieldValueException('consents');
        }
        else {
            for (IntegrationManager.MarketingConsents_JSON consent : parsedJson.consents) {
                if (consent.type == null) {
                    throw new InvalidFieldValueException('type');
                }
                if (consent.department == null) {
                    throw new InvalidFieldValueException('department');
                }
                if (consent.value == null) {
                    throw new InvalidFieldValueException('value');
                }
                if (consent.consent_date == null) {
                    throw new InvalidFieldValueException('consent_date');
                }
                if (consent.source == null) {
                    throw new InvalidFieldValueException('source');
                }
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- EXCEPTION CLASSES ------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class InvalidFieldValueException extends Exception {}
}