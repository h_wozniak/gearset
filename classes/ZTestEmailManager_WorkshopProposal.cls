@isTest
private class ZTestEmailManager_WorkshopProposal {

    private static void prepareTemplates() {
        EmailTemplate defaultEmailTemplate = new EmailTemplate();
        defaultEmailTemplate.isActive = true;
        defaultEmailTemplate.Name = 'x';
        defaultEmailTemplate.DeveloperName = EmailManager_WorkshopProposal.WORKSHOP_PROPOSAL_DEVELOPER_NAME;
        defaultEmailTemplate.TemplateType = 'text';
        defaultEmailTemplate.FolderId = UserInfo.getUserId();
        defaultEmailTemplate.Subject = 'x';

        insert defaultEmailTemplate;
    }

    @isTest static void test_sendWorkshopProposal() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        Account highSchool = ZDataTestUtility.createHighSchool(null, true);

        Test.startTest();
        Set<Id> accountIdSet = new Set<Id>();
        accountIdSet.add(highSchool.Id);


        EmailManager_WorkshopProposal.sendWorkshopProposal(accountIdSet);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(taskList.isEmpty());
        Test.stopTest();
    }

    @isTest static void test_sendWorkshopProposal_2() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            prepareTemplates();
        }

        Contact contactPerson = ZDataTestUtility.createContactPerson(null, true);
        Account highSchool = ZDataTestUtility.createHighSchool(new Account(Contact_Person__c = contactPerson.Id), true);

        Test.startTest();
        Set<Id> accountIdSet = new Set<Id>();
        accountIdSet.add(highSchool.Id);


        EmailManager_WorkshopProposal.sendWorkshopProposal(accountIdSet);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
        Test.stopTest();
    }
}