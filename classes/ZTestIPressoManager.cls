@isTest
private class ZTestIPressoManager {

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- CONTACTS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_removeIPressoContactOnEmailDeletion() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        List<Marketing_Campaign__c> iPressoContactsBeforeEmaiLDeletion = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactsBeforeEmaiLDeletion.size(), 2);

        // First update moves Email to Addtional Emails
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;
        // Second update clears all Email values
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        List<Marketing_Campaign__c> iPressoContactsAfterEmaiLDeletion = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactsAfterEmaiLDeletion.size(), 0);
    }

    @isTest static void test_splitEmails_emptyMain() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        Contact candidateAfterUpdates = [
                SELECT Id, Email, Additional_Emails__c
                FROM Contact
                WHERE Id = :candidate.Id
        ];

        Set<String> splittedEmailsOnContact = IPressoManager.splitEmails(candidateAfterUpdates.Email, candidateAfterUpdates.Additional_Emails__c);
        System.assertEquals(splittedEmailsOnContact.size(), 1);
    }

    @isTest static void test_splitEmails_emptyAddtional() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        Contact candidateAfterUpdates = [
                SELECT Id, Email, Additional_Emails__c
                FROM Contact
                WHERE Id = :candidate.Id
        ];

        Set<String> splittedEmailsOnContact = IPressoManager.splitEmails(candidateAfterUpdates.Email, candidateAfterUpdates.Additional_Emails__c);
        System.assertEquals(splittedEmailsOnContact.size(), 1);
    }

    @isTest static void test_splitEmails_emptyAll() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        // First update moves Email to Addtional Emails
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;
        // Second update clears all Email values
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        Contact candidateAfterUpdates = [
                SELECT Id, Email, Additional_Emails__c
                FROM Contact
                WHERE Id = :candidate.Id
        ];

        Set<String> splittedEmailsOnContact = IPressoManager.splitEmails(candidateAfterUpdates.Email, candidateAfterUpdates.Additional_Emails__c);
        System.assertEquals(splittedEmailsOnContact.size(), 0);
    }

    @isTest static void test_splitEmails_properData() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co', Additional_Emails__c = 'test@test.co2; test@test.co3'), true);

        Contact candidateAfterUpdates = [
                SELECT Id, Email, Additional_Emails__c
                FROM Contact
                WHERE Id = :candidate.Id
        ];

        Set<String> splittedEmailsOnContact = IPressoManager.splitEmails(candidateAfterUpdates.Email, candidateAfterUpdates.Additional_Emails__c);
        System.assertEquals(splittedEmailsOnContact.size(), 3);
    }

    @isTest static void test_disableRemovingContactConnectedToIPresso_mainEmail() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        List<Marketing_Campaign__c> iPressoContactsBeforeEmaiLDeletion = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactsBeforeEmaiLDeletion.size(), 2);

        Boolean errorOccured = false;
        try {
            delete candidate;
        } catch (Exception e) {
            if (e.getMessage().contains(Label.msg_error_cantMerge)) {
                errorOccured = true;
            }
        }
        System.assert(errorOccured);
    }

    @isTest static void test_disableRemovingContactConnectedToIPresso_additionalEmail() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        // First update moves Email to Addtional Emails
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        List<Marketing_Campaign__c> iPressoContactsBeforeEmaiLDeletion = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactsBeforeEmaiLDeletion.size(), 2);

        Boolean errorOccured = false;
        try {
            delete candidate;
        } catch (Exception e) {
            if (e.getMessage().contains(Label.msg_error_cantMerge)) {
                errorOccured = true;
            }
        }
        System.assert(errorOccured);
    }

    @isTest static void test_changeEmailAddressOnContact_afterMainEmailUpdate_manual() {
        CommonUtility.preventTriggeringEmailValidation = false;

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Email = 'test@test.co';

        Boolean errorOccured = false;
        try {
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterMainEmailUpdate_automatic() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Email = 'test@test.co';

        Boolean errorOccured = false;
        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(!errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate2.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterAdditionalEmailUpdate_manual() {
        CommonUtility.preventTriggeringEmailValidation = false;
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        // First update moves Email to Addtional Emails
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Email = 'test@test.co';

        Boolean errorOccured = false;
        try {
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterAdditionalEmailUpdate_automatic() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        // First update moves Email to Addtional Emails
        candidate.Email = '';
        candidate.Additional_Emails__c = '';
        update candidate;

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Email = 'test@test.co';

        Boolean errorOccured = false;
        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(!errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate2.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterAdditionalEmailUpdate_manual2() {
        CommonUtility.preventTriggeringEmailValidation = false;
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Additional_Emails__c = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Additional_Emails__c = 'test@test.co';

        Boolean errorOccured = false;
        try {
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterAdditionalEmailUpdate_automatic2() {
        CommonUtility.preventTriggeringEmailValidation = false;

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Additional_Emails__c = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(null, true);
        candidate2.Additional_Emails__c = 'test@test.co';

        Boolean errorOccured = false;
        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            update candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(!errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterContactInsert_manual() {
        CommonUtility.preventTriggeringEmailValidation = false;

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), false);

        Boolean errorOccured = false;
        try {
            insert candidate2;
        } catch (Exception e) {
            errorOccured = true;
        }

        System.assert(errorOccured);

        List<Marketing_Campaign__c> iPressoContactAfterChanges = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContactAfterChanges.get(0).Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_changeEmailAddressOnContact_afterContactInsert_automatic() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Marketing_Campaign__c iPressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = candidate.Id, Lead_Email__c = 'test@test.co'), true);

        Contact candidate2 = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Jan', LastName = 'Kowalski2', Email = 'test@test.co'), false);

        Boolean errorOccured = false;
        try {
            CommonUtility.preventTriggeringEmailValidation = true;
            insert candidate2;
        } catch (Exception e) {
            System.debug(e);
            errorOccured = true;
        }

        System.assert(!errorOccured);
    }


    @isTest static void test_createCampaignMemberIPresso_afterInsert() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        List<Marketing_Campaign__c> iPressoContacts = [
                SELECT Id, Contact_from_iPresso__c, Lead_Email__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContacts.size(), 1);
        System.assertEquals(iPressoContacts.get(0).Contact_from_iPresso__c, candidate.Id);
        System.assertEquals(iPressoContacts.get(0).Lead_Email__c, candidate.Email);

        Test.stopTest();
    }


    @isTest static void test_createCampaignMemberIPresso_afterUpdate() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        candidate.Email = 'test@testo.co';
        update candidate;

        List<Marketing_Campaign__c> iPressoContacts = [
                SELECT Id
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)
        ];

        System.assertEquals(iPressoContacts.size(), 2);

        Test.stopTest();
    }

    @isTest static void test_createAdditionalGlobalConsets_notGlobalConsent_no() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Catalog__c consentBase = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true);
        Date consentDate = System.today();
        Marketing_Campaign__c consent = ZDataTestUtility.createContactConsent(
                new Marketing_Campaign__c(Base_Consent__c = consentBase.Id,
                        Contact_From_Consent__c = candidate.Id,
                        Consent__c = CommonUtility.MARKETING_CONSENT_NO,
                        Refusal_Date__c = consentDate,
                        Global_Consent__c = false), true);

        Test.stopTest();

        List<Marketing_Campaign__c> consentOnEnrollment = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
        ];

        System.assertEquals(consentOnEnrollment.size(), 1);

        for (Marketing_Campaign__c consentAfterUpdate : consentOnEnrollment) {
            System.assertEquals(consentAfterUpdate.Consent__c, CommonUtility.MARKETING_CONSENT_NO);
            System.assertEquals(consentAfterUpdate.Refusal_Date__c, consentDate);
        }
    }

    @isTest static void test_createAdditionalGlobalConsets_notGlobalConsent_yes() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Catalog__c consentBase = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true);
        Date consentDate = System.today();
        Marketing_Campaign__c consent = ZDataTestUtility.createContactConsent(
                new Marketing_Campaign__c(Base_Consent__c = consentBase.Id,
                        Contact_From_Consent__c = candidate.Id,
                        Consent__c = CommonUtility.MARKETING_CONSENT_YES,
                        Consent_Date__c = consentDate,
                        Global_Consent__c = false), true);

        Test.stopTest();

        List<Marketing_Campaign__c> consentOnEnrollment = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
        ];

        System.assertEquals(consentOnEnrollment.size(), 1);

        for (Marketing_Campaign__c consentAfterUpdate : consentOnEnrollment) {
            System.assertEquals(consentAfterUpdate.Consent__c, CommonUtility.MARKETING_CONSENT_YES);
            System.assertEquals(consentAfterUpdate.Consent_Date__c, consentDate);
        }
    }

    @isTest static void test_updateAdditionalGlobalConsets_notGlobalConsent_no() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Catalog__c consentBase = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true);
        Date consentDate = System.today();
        Marketing_Campaign__c consent = ZDataTestUtility.createContactConsent(
                new Marketing_Campaign__c(Base_Consent__c = consentBase.Id,
                        Contact_From_Consent__c = candidate.Id,
                        Consent__c = CommonUtility.MARKETING_CONSENT_NO,
                        Refusal_Date__c = consentDate,
                        Global_Consent__c = false), true);

        Test.stopTest();

        List<Marketing_Campaign__c> consentOnEnrollment = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
        ];

        System.assertEquals(consentOnEnrollment.size(), 1);

        for (Marketing_Campaign__c consentAfterUpdate : consentOnEnrollment) {
            System.assertEquals(consentAfterUpdate.Consent__c, CommonUtility.MARKETING_CONSENT_NO);
            System.assertEquals(consentAfterUpdate.Refusal_Date__c, consentDate);
        }

        consent.Consent__c = CommonUtility.MARKETING_CONSENT_YES;
        consent.Consent_Date__c = System.today();
        consentDate = consent.Consent_Date__c;
        update consent;

        List<Marketing_Campaign__c> consentsOnEnrollmentAfterUpdate = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
                AND Id != :consent.Id
        ];

        System.assertEquals(consentsOnEnrollmentAfterUpdate.size(), 0);
    }

    @isTest static void test_updateAdditionalGlobalConsets_notGlobalConsent_yes() {
        Test.startTest();
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);
        Catalog__c consentBase = ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true);
        Date consentDate = System.today();
        Marketing_Campaign__c consent = ZDataTestUtility.createContactConsent(
                new Marketing_Campaign__c(Base_Consent__c = consentBase.Id,
                        Contact_From_Consent__c = candidate.Id,
                        Consent__c = CommonUtility.MARKETING_CONSENT_YES,
                        Consent_Date__c = consentDate,
                        Global_Consent__c = false), true);

        Test.stopTest();

        List<Marketing_Campaign__c> consentOnEnrollment = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
        ];

        System.assertEquals(consentOnEnrollment.size(), 1);

        for (Marketing_Campaign__c consentAfterUpdate : consentOnEnrollment) {
            System.assertEquals(consentAfterUpdate.Consent__c, CommonUtility.MARKETING_CONSENT_YES);
            System.assertEquals(consentAfterUpdate.Consent_Date__c, consentDate);
        }

        consent.Consent__c = CommonUtility.MARKETING_CONSENT_NO;
        consent.Refusal_Date__c = System.today();
        consentDate = consent.Refusal_Date__c;
        update consent;

        List<Marketing_Campaign__c> consentsOnEnrollmentAfterUpdate = [
                SELECT Id, Consent__c, Consent_Date__c, Refusal_Date__c
                FROM Marketing_Campaign__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CONSENTS_CONTACT)
                AND Id != :consent.Id
        ];

        System.assertEquals(consentsOnEnrollmentAfterUpdate.size(), 0);
    }

    @isTest static void test_repinIPressoContact() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        Marketing_Campaign__c ipressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = null, Lead_Email__c = 'test2@test.co'), true);

        IPressoManager.repinIPressoContact(candidate, ipressoContact, null);

        iPressoContact = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE Id = :ipressoContact.Id
        ];

        System.assertEquals(ipressoContact.Contact_from_iPresso__c, candidate.Id);
    }

    @isTest static void test_repinIPressoContact2() {
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Email = 'test@test.co'), true);

        Marketing_Campaign__c ipressoContact = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = null, Lead_Email__c = 'test2@test.co'), true);

        Marketing_Campaign__c iPresso2 = ZDataTestUtility.createIPressoContact(new Marketing_Campaign__c(Contact_from_iPresso__c = null, Lead_Email__c = 'test3@test.co'), false);

        IPressoManager.repinIPressoContact(candidate, ipressoContact, new List<Marketing_Campaign__c>{
                iPresso2
        });

        iPressoContact = [
                SELECT Id, Contact_from_iPresso__c
                FROM Marketing_Campaign__c
                WHERE Id = :ipressoContact.Id
        ];

        System.assertEquals(ipressoContact.Contact_from_iPresso__c, candidate.Id);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- PERSON ACCOUNTS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

}