public with sharing class LEX_Enroll_SpecialtyController {
	@AuraEnabled
	public static String checkOffer(String recordId) {

		Offer__c offer = [
				SELECT Degree__c, Configuration_Status_Specialty__c, Enrollment_only_for_Specialties__c, BetweenDates__c
				FROM Offer__c
				WHERE Id = :recordId
		];

		if(offer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG &&
				!LEXStudyUtilities.checkIfCanEnrollToOffer(offer.Id)) {
			return Label.msg_error_specialtyWithoutSpecialization;
		}

		if(!offer.Configuration_Status_Specialty__c) {
			return Label.msg_error_SpecialtyNotActive;
		}

		if(offer.Enrollment_only_for_Specialties__c) {
			return Label.msg_error_EnrollmentOnlyForSpec;
		}

		if(!offer.BetweenDates__c) {
			return Label.msg_info_NotActiveAnymore;
		}

		return 'NO_ERROR';
	}
}