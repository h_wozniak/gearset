/**
* @author       Wojciech Słodziak
* @description  This class is a controller for EnrollmentStudy.page, wchich is a wizard of study enrollment
**/
public with sharing class EnrollmentStudyController {

    public Enrollment_Wrapper ew { get; set; }
    public Boolean completed { get; set; }
    public String enrollmentId { get; set; }

    public Offer__c selectedOffer { get; set; }
    public Id selectedPostGraduateOfferProduct { get; set; }
    public Id selectedSpecialtyOffer { get; set; }
    private String retURL;

    public Id offerId { get; set; }
    public Id candidateId { get; set; }
    public Id previousEnrId { get; set; }
    public Enrollment__c previousEnr { get; set; }

    public List<SelectOption> tuitionSystemList { get; set; }
    public List<SelectOption> tuitionModePaymentList { get; set; }
    public List<SelectOption> finishedUniversitiesList { get; set; }
    public String selectedUniversity { get; set; }
    public Boolean disabledSaveButton { get; set; }
    public String universityNameOnOffer { get; set; }
	public Boolean doNotSetUpStudentCardVisibilityForMBA { get; set; }

    
    List<Qualification__c> finishedUniversities;

    public String degree { get; set; }
    public Boolean showContactLookup { get; set; }

    public Boolean error { get; set; }
    public Boolean guidedGroupOffer { get; set; }

    public String queableJob { get; set; }

    /* for view */ 
    public String ONETIME_PAYMENT { get { return CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME; } }

    public String byMailPV { get { return CommonUtility.ENROLLMENT_INVOICE_DELIVERY_EMAIL; } }


    public EnrollmentStudyController() {
        offerId = ApexPages.currentPage().getParameters().get('offerId');
        candidateId = ApexPages.currentPage().getParameters().get('candidateId');
        previousEnrId = ApexPages.currentPage().getParameters().get('enrollmentId');
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        showContactLookup = false;
		doNotSetUpStudentCardVisibilityForMBA = false;

        selectedOffer = getOffer(offerId);

        if (selectedOffer == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.msg_error_NoOffersSelected));
            return;
        }

        if (selectedOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)
            && !selectedOffer.Course_Offer_from_Specialty__r.Enrollment_only_for_Specialties__c) {
            selectedSpecialtyOffer = selectedOffer.Id;
            selectedOffer = getOffer(selectedOffer.Course_Offer_from_Specialty__c);
            ApexPages.currentPage().getParameters().put('offerId', selectedOffer.Course_Offer_from_Specialty__c);
        }
        
        universityNameOnOffer = CommonUtility.getMarketingEntityByUniversityName(selectedOffer.University_Name__c);
        degree = selectedOffer.Degree__c;
        guidedGroupOffer = selectedOffer.Guided_Group__c;

        ew = new Enrollment_Wrapper();

        ew.enrollment = getFreshEnrollment(selectedOffer.Id);
        ew.enrollment.Guided_Group__c = guidedGroupOffer; 
        ew.languageList = prepareLanguageList(selectedOffer.Id);
        if (ew.languageList == null) {
            ew.languageList = new List<Language_Wrapper>();
        }

        ew.postGraduateOfferProductList = getPostGraduateOfferProductList(selectedOffer.Id);

        ew.consentList = getConsentsForContact(universityNameOnOffer);
        ew.specialtiesList = new List<SelectOption>();

        if (selectedOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
            && !selectedOffer.Enrollment_only_for_Specialties__c) {
            ew.specialtiesList = prepareSpecialtiesList(degree);
        }

        System.debug('candidateId ' + candidateId);
        if (candidateId != null) {
            System.debug('if valid ' + ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, null, candidateId));
            if (ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, null, candidateId)) {
                ew.enrollment.Candidate_Student__c = candidateId;
                ew.enrollment.WSB_Graduate__c = ContactManager.isWSBGraduate(candidateId);
                setTuitionSystemAndMode(selectedOffer.Id, candidateId, null);
                getFinishedUniversities(candidateId);
            } else {
                showContactLookup = true;
            }
        }


        //if candidate wants to change Study Offer
        if (previousEnrId != null) {
            fillEnrWithPreviousEnrData(ew.enrollment, previousEnrId);
            setTuitionSystemAndMode(selectedOffer.Id, ew.enrollment.Candidate_Student__c, previousEnr);
            getFinishedUniversities(ew.enrollment.Candidate_Student__c);
            ew.consentList = getConsentsForContact(universityNameOnOffer, ew.enrollment);

            System.debug('ew.enrollment.Candidate_Student__c ' + ew.enrollment.Candidate_Student__c);
            System.debug('if valid ' + ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, null, ew.enrollment.Candidate_Student__c));
            if (!ContactManager.contactDataIsCompleteOnStudyEnrollment(degree, null, ew.enrollment.Candidate_Student__c)) {
                candidateId = ew.enrollment.Candidate_Student__c;
                ew.enrollment.Candidate_Student__c = null;
                showContactLookup = true;
            }
        }

        error = checkIfErrorOccured();


        if (ApexPages.currentPage().getParameters().get('hasError') == 'true') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_CantChangeWSBEntity));
            return;
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- INIT METHODS -------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private Enrollment__c getFreshEnrollment(Id offerId) {
        Enrollment__c enrollment = new Enrollment__c();
        enrollment.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY);
        enrollment.Course_or_Specialty_Offer__c = offerId;
        enrollment.Starting_Semester__c = CommonUtility.ENROLLMENT_STARTINGSEM_1;
        enrollment.Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED;
        enrollment.Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_CRM;

        if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_MBA || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            enrollment.Do_not_set_up_Student_Card__c = true;
			doNotSetUpStudentCardVisibilityForMBA = true;
        }

        return enrollment;
    }

    private boolean checkIfErrorOccured() {
        return selectedOffer == null;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- DATA LOAD METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    private Offer__c getOffer(String offerId) {
        Offer__c offer;

        List<Offer__c> offerList;
        if (offerId != null) {
            offerList = [
                SELECT Id, Name, Trade_Name__c, Specialty_Trade_Name__c, Active_from__c, Active_to__c, Degree__c, Course_Offer_from_Specialty__c,
                toLabel(RecordType.Name), RecordTypeId, University_Name__c, Enrollment_only_for_Specialties__c, Guided_Group__c,
                Course_Offer_from_Specialty__r.Enrollment_only_for_Specialties__c
                FROM Offer__c 
                WHERE Id = :offerId
            ];
        }

        if (offerList != null && !offerList.isEmpty()) {
            offer = offerList[0];
        }

        return offer;
    }

    private void setTuitionSystemAndMode(Id offerId, Id candidateId, Enrollment__c studyEnr) {
        Contact candidate = [
            SELECT Id, Foreigner__c, Foreigners_PriceBook__c, Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, 
                   Consent_Direct_Communications_ADO__c, Consent_PUCA_ADO__c, Consent_Graduates_ADO__c
            FROM Contact 
            WHERE Id = :candidateId
        ];

        
        Offer__c priceBook = PriceBookManager.getProperPriceBookForOffer(offerId, candidate.Foreigners_PriceBook__c);

        if (priceBook != null) {
            tuitionSystemList = PriceBookManager.getTutitionSystemList(priceBook);
            tuitionModePaymentList = PriceBookManager.getTuitionModePaymentList(priceBook);

            if (!tuitionSystemList.isEmpty()) {
                if (studyEnr != null) {
                    for (SelectOption tuitionSystem : tuitionSystemList) {
                        if (tuitionSystem.getValue() == studyEnr.Tuition_System__c) {
                            ew.enrollment.Tuition_System__c = tuitionSystem.getValue();
                        }
                    }
                }
                if (ew.enrollment.Tuition_System__c == null) ew.enrollment.Tuition_System__c = tuitionSystemList[0].getValue();
            }

            if (tuitionModePaymentList.isEmpty() && ew.enrollment.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME) {
                tuitionModePaymentList.add(new SelectOption('1', '1'));
            }

            if (!tuitionModePaymentList.isEmpty()) {
                if (studyEnr != null) {
                    for (SelectOption tuitionMode : tuitionModePaymentList) {
                        if (tuitionMode.getValue() == studyEnr.Installments_per_Year__c) {
                            ew.enrollment.Installments_per_Year__c = tuitionMode.getValue();
                        }
                    }
                }
                if (ew.enrollment.Installments_per_Year__c == null) ew.enrollment.Installments_per_Year__c = tuitionModePaymentList[0].getValue();
            }
        }
    }

    private void getFinishedUniversities(Id candidateId) {
        Contact candidate = [
            SELECT Id, Foreigner__c, Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, 
                   Consent_Direct_Communications_ADO__c, Consent_PUCA_ADO__c, Consent_Graduates_ADO__c 
           FROM Contact 
           WHERE Id = :candidateId
        ];

        finishedUniversities = [
            SELECT Id, University_from_Finished_University__c, University_from_Finished_University__r.Name, Name, 
                   ToLabel(Status__c), Defense_Date__c, Finished_Course__c, Graduation_Year__c, Diploma_Number__c, ZPI_University_Name__c
            FROM Qualification__c
            WHERE Candidate_from_Finished_University__c =: candidateId
        ];
        
        finishedUniversitiesList = new List<SelectOption>();
        
        if (!finishedUniversities.isEmpty()) {
            for (Qualification__c finishedUniversity : finishedUniversities) {
                String finishedCourse = finishedUniversity.Finished_Course__c != null ? ', ' + Label.title_finishedCourse + ': ' + finishedUniversity.Finished_Course__c : '';
                String graduationYear = finishedUniversity.Graduation_Year__c != null ? ', ' +  Label.title_graduationYear + ': ' + finishedUniversity.Graduation_Year__c : '';
                String diplomaNumber = finishedUniversity.Diploma_Number__c != null ? ', ' + Label.title_diplomaNumber + ': ' + finishedUniversity.Diploma_Number__c : '';
                String status = finishedUniversity.Status__c != null ? ', ' + Label.title_status + ': ' + finishedUniversity.Status__c : '';
                String universityName = finishedUniversity.University_from_Finished_University__c != null ? finishedUniversity.University_from_Finished_University__r.Name : finishedUniversity.ZPI_University_Name__c;
                finishedUniversitiesList.add(
                    new SelectOption(
                        finishedUniversity.Id, 
                        universityName + finishedCourse + graduationYear + diplomaNumber + status
                    )
                );
            }
            selectedUniversity = finishedUniversities[0].Id;
        }
        
    }

    private List<Language_Wrapper> prepareLanguageList(Id offerId) {
        List<Language_Wrapper> enrollmentLanguages = new List<Language_Wrapper>();
        Offer__c offer = [SELECT Course_Offer_from_Specialty__c, RecordTypeId FROM Offer__c WHERE Id =:offerId];

        List<Offer__c> listOfLanguages = new List<Offer__c>();        
        if (offer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)) {
            listOfLanguages = [SELECT Id, Language__c, Mandatory__c
                               FROM Offer__c 
                               WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE) 
                               AND Course_Offer_from_Language__c =: offer.Course_Offer_from_Specialty__c];
        } else {
            listOfLanguages = [SELECT Id, Language__c, Mandatory__c
                               FROM Offer__c 
                               WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_LANGUAGE) 
                               AND Course_Offer_from_Language__c =: offerId];
        }
        
        if (listOfLanguages.size() > 0) {            
            for (Offer__c language : listOfLanguages) {
                Language_Wrapper lw = new Language_Wrapper();
                lw.languageName = language.Language__c;
                lw.languageOffer = language;
                lw.required = language.Mandatory__c;
                
                Enrollment__c langEnr = new Enrollment__c();
                langEnr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE);

                lw.languageEnrollment = langEnr;
                lw.checked = false;
    
                enrollmentLanguages.add(lw);
            }
        }

        return enrollmentLanguages;
    }

    private List<SelectOption> prepareSpecialtiesList(String degree) {
        List<SelectOption> specialitesOffers = new List<SelectOption>();

        List<Offer__c> specialties = [
            SELECT Id, Name, Specialty_Trade_Name__c,
             (SELECT Id FROM Specializations__r)
            FROM Offer__c
            WHERE Course_Offer_from_Specialty__c = :selectedOffer.Id
            AND Configuration_Status_Specialty__c = true AND Active_from__c <= TODAY
            AND Active_to__c >= TODAY ORDER BY Name
        ];

        for (Offer__c specialty : specialties) {
            if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
                if (specialty.Specializations__r.size() > 0) {
                    specialitesOffers.add(
                        new SelectOption(specialty.Id, specialty.Name + ', ' + specialty.Specialty_Trade_Name__c)
                    );                    
                }
            }
            else {
                specialitesOffers.add(
                    new SelectOption(specialty.Id, specialty.Name + ', ' + specialty.Specialty_Trade_Name__c)
                );
            }
        }

        return specialitesOffers;
    }

    private List<Offer__c> preparePostGraduateOfferProductList() {
        List<Offer__c> postGraduateOffers = new List<Offer__c>();
        
        if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            
            List<Offer__c> postGraduateOfferProductsOnMainOffer = [
                SELECT Id, Name, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c,
                       (SELECT Id, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c FROM Specializations__r),
                       (SELECT Id, Name, Trade_Name__c FROM Specialties__r)
                FROM Offer__c 
                WHERE Id = :selectedOffer.Id
            ];
            
            Map<Id, Id> postGraduateProducts = new Map<Id, Id>();
            
            for (Offer__c postGraduateOfferProductOnMainOffer : postGraduateOfferProductsOnMainOffer) {
                if (postGraduateOfferProductOnMainOffer.Specializations__r.size() > 0) {
                    postGraduateProducts.put(postGraduateOfferProductOnMainOffer.Specialization__c, postGraduateOfferProductOnMainOffer.Id);
                }
            }
            
            for (Id postGraduateId : postGraduateProducts.values()) {
                for (Offer__c postGraduateOfferProductOnMainOffer : postGraduateOfferProductsOnMainOffer) {
                    if (postGraduateOfferProductOnMainOffer.Id == postGraduateId) {
                        postGraduateOffers.add(postGraduateOfferProductOnMainOffer);
                    }
                 }
            }
        }

        return postGraduateOffers;
    }

    public List<SelectOption> getPostGraduateOfferProductList(Id offerId) {
        List<SelectOption> selectOptionList = new List<SelectOption>();
        if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_PG || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            for (Offer__c productOnOffer : preparePostGraduateOfferProductList()) {
                for (Offer__c spec : productOnOffer.Specializations__r) {
                    selectOptionList.add(
                        new SelectOption(spec.Specialization__c, spec.Specialization__r.Name + ', ' + spec.Specialization__r.Trade_Name__c)
                    );
                }
            }

            if (selectedOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)
                && selectedSpecialtyOffer != null) {
                selectOptionList.addAll(preparePostGraduateOfferFromSpecialty(selectedSpecialtyOffer));
            }
        }
        
        return selectOptionList;
    }

    public List<SelectOption> preparePostGraduateOfferFromSpecialty(Id specialtyOfferId) {            
        Offer__c postGraduateOfferProductOnMainOffer = [
            SELECT Id, Name, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c,
                   (SELECT Id, Specialization__c, Specialization__r.Name, Specialization__r.Trade_Name__c FROM Specializations__r)
            FROM Offer__c 
            WHERE Id = :specialtyOfferId
        ];

        List<SelectOption> selectOptionList = new List<SelectOption>();
        for (Offer__c spec : postGraduateOfferProductOnMainOffer.Specializations__r) {
            selectOptionList.add(
                new SelectOption(spec.Specialization__c, spec.Specialization__r.Name + ', ' + spec.Specialization__r.Trade_Name__c)
            );
        }

        return selectOptionList;
    }
	
    private List<Consent_Wrapper> getConsentsForContact(String entity) {
        List<Consent_Wrapper> enrollmentConsents = new List<Consent_Wrapper>();

        Set<String> consentNames = new Set<String> {
            RecordVals.MARKETING_CONSENT_1 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_2 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_4 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_5 + ' ' + entity
        };

        for (String consent : consentNames) {
            Consent_Wrapper cw = new Consent_Wrapper();
            cw.name = consent;
            cw.checked = false;

            enrollmentConsents.add(cw);
        }

        return enrollmentConsents;
    }

    private List<Consent_Wrapper> getConsentsForContact(String entity, Enrollment__c newEnr) {
        List<Consent_Wrapper> enrollmentConsents = new List<Consent_Wrapper>();

        Set<String> consentNames = new Set<String> {
            RecordVals.MARKETING_CONSENT_1 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_2 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_4 + ' ' + entity, 
            RecordVals.MARKETING_CONSENT_5 + ' ' + entity
        };

        for (String consent : consentNames) {
            Consent_Wrapper cw = new Consent_Wrapper();
            cw.name = consent;

            if (newEnr.Consent_Direct_Communications__c && consent == (RecordVals.MARKETING_CONSENT_4 + ' ' + entity)) {
                cw.checked = true;
            }
            else if (newEnr.Consent_Graduates__c && consent == (RecordVals.MARKETING_CONSENT_5 + ' ' + entity)) {
                cw.checked = true;                
            }
            else if (newEnr.Consent_Electronic_Communication__c && consent == (RecordVals.MARKETING_CONSENT_2 + ' ' + entity)) {
                cw.checked = true;                
            }
            else if (newEnr.Consent_Marketing__c && consent == (RecordVals.MARKETING_CONSENT_1 + ' ' + entity)) {
                cw.checked = true;                
            }
            else {
                cw.checked = false;
            }

            enrollmentConsents.add(cw);
        }

        return enrollmentConsents;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- USER ACTIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public void setStudent() {
        Id candidateId = ApexPages.currentPage().getParameters().get('contactId');
        ew.enrollment.Candidate_Student__c = candidateId;
        ew.enrollment.WSB_Graduate__c = ContactManager.isWSBGraduate(candidateId);

        setTuitionSystemAndMode(selectedOffer.Id, candidateId, null);
        getFinishedUniversities(candidateId);
        
        if (finishedUniversities != null && finishedUniversities.size() > 0) {
            selectedUniversity = finishedUniversities[0].Id;
        }
    }

    public void setUniversity() {
        Id candidateId = ApexPages.currentPage().getParameters().get('contactId');

        getFinishedUniversities(candidateId);
        
        if (finishedUniversities != null && finishedUniversities.size() > 0) {
            selectedUniversity = finishedUniversities[0].Id;
        }
    }

    public void changeStudent() {
        ew.enrollment.Candidate_Student__c = null;
        ew.enrollment.WSB_Graduate__c = null;
        finishedUniversitiesList = new List<SelectOption>();
    }

    public PageReference specialtyChange() {
        ew.postGraduateOfferProductList = getPostGraduateOfferProductList(selectedOffer.Id);
        return null;
    }

    public PageReference tuitionSystemChange() {
        if (ew.enrollment.Tuition_System__c == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME) {
            ew.enrollment.Installments_per_Year__c = String.valueOf(1);
        }
        
        return null;
    }

    public PageReference cancel() {
        if (retURL != null) {
            return new PageReference(retURL);
        }
        return null;
    }

    public PageReference disableButton() {
        disabledSaveButton = true;
        return null;
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- SAVE ACTION ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public PageReference saveEnrollment() {
        disabledSaveButton = false;
        Savepoint sp = Database.setSavepoint();
        if (previousEnr != null && selectedOffer.University_Name__c != previousEnr.University_Name__c && 
            (/*previousEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
            ||*/ previousEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
            || previousEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS)) {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_CantChangeWSBEntity));
            PageReference pageRef = new PageReference('/apex/EnrollmentStudy');
            pageRef.setRedirect(true);
            pageref.getParameters().put('offerId', selectedOffer.Id);
            pageref.getParameters().put('enrollmentId', previousEnr.Id);
            pageref.getParameters().put('retURL', 'apex/StudySelection?enrollmentId=' + previousEnr.Id +'&retURL=/' + previousEnr.Id);
            pageref.getParameters().put('hasError','true');

            return pageRef;
        }

        if (previousEnr != null && selectedOffer.Id != previousEnr.Course_or_Specialty_Offer__c) {
            ew.enrollment.TECH_Check_Discounts_on_Enrollment__c = true;
        }

        if (previousEnr != null && previousEnr.Status__c != CommonUtility.ENROLLMENT_STATUS_CONFIRMED) {
            CommonUtility.skipEnrOrCampaignUpdate = true;
        }

        if (ew.enrollment.Way_of_Invoice_Delivery__c == byMailPV) {
            if (ew.enrollment.Email_for_Invoice_Delivery__c == null) {
                disabledSaveButton = false;
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info, Label.msg_error_ProvideEmailForInvoice));
                return null;
            }
        } else {
            ew.enrollment.Email_for_Invoice_Delivery__c = null;
        }

        try {
            if (ew.enrollment.Enrollment_Date__c == null) {
                ew.enrollment.Enrollment_Date__c = System.now();
            }

            ew.enrollment.TECH_Ignore_Automatic_Discounts__c = (ew.enrollment.Guided_Group__c && !guidedGroupOffer);

            // specialization list
            if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
                if (ew.postGraduateOfferProductList.size() > 0 && selectedPostGraduateOfferProduct != null) {
                    ew.enrollment.Specialization__c = selectedPostGraduateOfferProduct;
                }
                else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_productOnOfferNotSelected);
                    disabledSaveButton = false;
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }

            // specialty
            if (ew.specialtiesList.size() > 0) {
                if (selectedSpecialtyOffer != null) {
                    ew.enrollment.Initial_Specialty_Declaration__c = selectedSpecialtyOffer;
                }
                else {
                    if (selectedOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_OFFER)) {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_specialtyOnOfferNotSele);
                        disabledSaveButton = false;
                        ApexPages.addMessage(myMsg);
                        return null;    
                    }            
                }
            }

            // finished university list
            if (selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II_PG || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_II 
                || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_PG || selectedOffer.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
                    
                if (selectedUniversity != null) {
                    ew.enrollment.Finished_University__c = selectedUniversity;
                }
                else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_universityNotSelected);
                    disabledSaveButton = false;
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }

            // marketing consents
            String entity = CommonUtility.getMarketingEntityByUniversityName(selectedOffer.University_Name__c);
            ew.enrollment.Consent_Date_processing_for_contract__c = true;

            Contact candidate = [
                SELECT Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Direct_Communications_ADO__c, Consent_PUCA_ADO__c, 
                       Consent_Graduates_ADO__c, University_for_sharing__c
                FROM Contact
                WHERE Id = :ew.enrollment.Candidate_Student__c
            ];

            Boolean changedConsentsOnContact = false;
            List<String> fieldsToValidate = new List<String>();
            for (Consent_Wrapper cw : ew.consentList) {                        
                if (cw.checked) {
                     fieldsToValidate.add(CatalogManager.getApiADONameFromConsentName(cw.name)); 
                     ew.enrollment.put(CatalogManager.getApiNameFromConsentName(cw.name), cw.checked);
                     changedConsentsOnContact = true;
                }
            }
            
            for (String fieldToValidate : fieldsToValidate) {
                candidate = MarketingConsentManager.updateConsentOnContact(entity, candidate, fieldToValidate);
            }

            // main enrollment
            // Queueable in both Annexing and Creating Enrollment should be responsbile for creating documents
            CommonUtility.preventCreatingDocsFromTrigger = true;
            CommonUtility.preventDetailsCreation = true;
            insert ew.enrollment;

            // update contact only when consents were changed
            if (changedConsentsOnContact) {
                CommonUtility.preventDeduplicationOfContacts = true;
                MarketingConsentManager.updateContactWithoutSharing(candidate);
            }

            // languages list
            List<Enrollment__c> languagesToInsert = new List<Enrollment__c>();
            if (ew.languageList.size() > 0) {
                for(Language_Wrapper language : ew.languageList) {
                    if (language.checked && language.languageEnrollment.Language_Level__c != null) {
                        Enrollment__c langEnr = new Enrollment__c();
                        langEnr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE);
                        langEnr.Foreign_Language__c = language.languageOffer.Language__c;
                        langEnr.Language_from_Enrollment_Language__c = language.languageOffer.Id;
                        langEnr.Enrollment_from_Language__c = ew.enrollment.Id;
                        langEnr.Language_Level__c = language.languageEnrollment.Language_Level__c;
                        langEnr.University_Name__c = ew.enrollment.University_Name__c;
                        languagesToInsert.add(langEnr);
                    }
                    else {         
                        if (language.checked) {               
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.error_msg_needToSelectLanguageLevel);
                            disabledSaveButton = false;
                            ApexPages.addMessage(myMsg);
                            Database.rollback(sp);
                            return null;
                        }

                        if (language.languageEnrollment.Language_Level__c != null && !language.checked) {            
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_selectLanguage);
                            disabledSaveButton = false;
                            ApexPages.addMessage(myMsg);
                            Database.rollback(sp);
                            return null;
                        }
                    }
                }
            }

            if (previousEnr != null) {
                // method usable for case when Candidate wants to change Course/Specialty
                // copies Discounts from previous Enrollment and applies them to new Enrollment
                // updates required Documents with values from previousEnrollment
                // selects correct status for Enrollment and setups lookups
                queableJob = System.enqueueJob(new EnrollmentAnnexFinalizationQueueable(ew.enrollment, previousEnr, languagesToInsert));
                return null;
            }
            else {                
                queableJob = System.enqueueJob(new GenerateMissingAttachmentsOnEnrollment(languagesToInsert, new List<Enrollment__c> { ew.enrollment }, null, null, null, -1));
                return null;
            }

        } catch(Exception e) {
            System.debug(e);
            Database.rollback(sp);
            ew.enrollment.Id = null;
            ApexPages.addMessages(e);
            return null;
        }

        return new PageReference('/' + ew.enrollment.Id);
    }

    public PageReference checkDocumentGenerationDocument() {
        if (queableJob != null) {
            List<AsyncApexJob> jobs = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :queableJob];

            if (jobs.get(0).Status == 'Completed') {
                enrollmentId = ew.enrollment.Id;
                completed = true;
            }
        }

        return null;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- OFFER CHANGE VARIANT ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method used in case of Study Offer change - prepares new Enrollment based on previous Enrollment
    @TestVisible
    private void fillEnrWithPreviousEnrData(Enrollment__c newEnr, Id previousEnrId) {
        previousEnr = [
            SELECT Id, Status__c, Candidate_Student__c, Educational_Advisor__c, Source_Studies__c, VerificationDate__c, Starting_Semester__c, 
            Dean_s_Decision_Date__c, Discount_Code__c, Previous_Enrollment__c, Enrollment_Source__c, Enrollment_Date__c, WSB_Graduate__c, Unenrolled__c,
            Resignation_Date__c, Resignation_Reason_List__c, Educational_Agreement__c, First_Name_for_Invoice__c, Last_Name_for_Invoice__c, 
            Street_for_Invoice__c, Postal_Code_for_Invoice__c, City_for_Invoice__c, Company_Name__c, Country_for_Invoice__c, 
            NIP_for_Invoice__c, The_Same_Data_as_on_Company__c, The_Invoice_after_Payment__c, Comments_on_Invoice__c, University_Name__c ,
            Do_not_set_up_Student_Card__c, Tuition_System__c, Installments_per_Year__c, Confirmation_Date__c, Collection_of_Documents_Date__c,
            Signed_Contract_Date__c, Documents_Collected_Date__c, Consent_Date_processing_for_contract__c, Consent_Direct_Communications__c,
            Consent_Electronic_Communication__c, Consent_Graduates__c, Consent_Marketing__c, Dont_send_automatic_emails__c, Course_or_Specialty_Offer__c, Language_of_Enrollment__c 
            FROM Enrollment__c 
            WHERE Id = :previousEnrId
        ];

        newEnr.Candidate_Student__c = previousEnr.Candidate_Student__c;
        newEnr.Unenrolled__c = previousEnr.Unenrolled__c;
        newEnr.WSB_Graduate__c = previousEnr.WSB_Graduate__c;
        newEnr.Educational_Advisor__c = previousEnr.Educational_Advisor__c;
        newEnr.Source_Studies__c = previousEnr.Source_Studies__c;
        newEnr.Enrollment_Source__c = previousEnr.Enrollment_Source__c;
        newEnr.Starting_Semester__c = previousEnr.Starting_Semester__c;
        newEnr.Enrollment_Date__c = previousEnr.Enrollment_Date__c;
        newEnr.Dean_s_Decision_Date__c = previousEnr.Dean_s_Decision_Date__c;
        newEnr.Educational_Agreement__c = previousEnr.Educational_Agreement__c;
        newEnr.Do_not_set_up_Student_Card__c = previousEnr.Do_not_set_up_Student_Card__c;
        newEnr.Confirmation_Date__c = previousEnr.Confirmation_Date__c != null ? previousEnr.Confirmation_Date__c : Datetime.now();
        newEnr.Collection_of_Documents_Date__c = previousEnr.Collection_of_Documents_Date__c;
        newEnr.Consent_Date_processing_for_contract__c = previousEnr.Consent_Date_processing_for_contract__c;
        newEnr.Consent_Direct_Communications__c = previousEnr.Consent_Direct_Communications__c;
        newEnr.Consent_Electronic_Communication__c = previousEnr.Consent_Electronic_Communication__c;
        newEnr.Consent_Graduates__c = previousEnr.Consent_Graduates__c;
        newEnr.Consent_Marketing__c = previousEnr.Consent_Marketing__c;
        newEnr.Dont_send_automatic_emails__c = previousEnr.Dont_send_automatic_emails__c;
        newEnr.Language_Of_Enrollment__c = previousEnr.Language_Of_Enrollment__c;


        // if Enrollment was from ZPI copy VerificationDate__c, which is required to determine Document Delivery Deadlines
        // if Enrollment was from CRM VerificationDate__c will be null, and CreatedDate of previous Enrollment should be treated as date to determine Deadlines 
        newEnr.VerificationDate__c = (previousEnr.VerificationDate__c != null? previousEnr.VerificationDate__c : previousEnr.Enrollment_Date__c);

        newEnr.Previous_Enrollment__c = previousEnrId;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------- WRAPPERS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private class Enrollment_Wrapper {
        public Enrollment__c enrollment { get; set; }
        public List<Language_Wrapper> languageList { get; set; }
        public List<SelectOption> postGraduateOfferProductList { get; set; }
        public List<SelectOption> specialtiesList { get; set; }
        public List<Consent_Wrapper> consentList { get; set; }
        public Marketing_Campaign__c agrRealizationConsent { get; set; }
    }

    @TestVisible
    private class Consent_Wrapper {
        public Marketing_Campaign__c consent { get; set; }
        public String name { get; set; }
        public Boolean checked { get; set; }
    }

    @TestVisible
    private class Language_Wrapper {
        public String languageName { get; set; }
        public Enrollment__c languageEnrollment { get; set; }
        public Offer__c languageOffer { get; set; }
        public Boolean checked { get; set; }
        public Boolean required { get; set;}
    }

}