/**
*   @author         Wojciech Słodziak
*   @description    Batch class summarizing Enrollment Value of finalized Training Enrollments for Sales Targets.
**/

global class TrainingSalesTargetComputationsBatch implements Database.Batchable<sObject> {
    
    Boolean isRunManually;
    Set<String> cummulativeStatusesForOpen;
    Set<String> cummulativeStatusesForClosed;
    Set<String> cummulativeStatusesForSchedule;
    
    global TrainingSalesTargetComputationsBatch(Boolean isRunManually) {
        this.isRunManually = isRunManually;

        cummulativeStatusesForOpen = new Set<String> {
            CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT,
            CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED,
            CommonUtility.ENROLLMENT_STATUS_PAID,
            CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES
        };

        cummulativeStatusesForClosed = new Set<String> {
            CommonUtility.ENROLLMENT_STATUS_NEW,
            CommonUtility.ENROLLMENT_STATUS_WAITING_FOR_PAYMENT,
            CommonUtility.ENROLLMENT_STATUS_GROUP_LAUNCHED,
            CommonUtility.ENROLLMENT_STATUS_PAID,
            CommonUtility.ENROLLMENT_STATUS_SEE_CANDIDATES
        };

        cummulativeStatusesForSchedule = new Set<String> {
            CommonUtility.OFFER_GROUP_STATUS_LAUNCHED,
            CommonUtility.OFFER_GROUP_STATUS_WAITING_FOR_LAUNCH
        };
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, University_Name__c, Value_after_Discount__c, Valid_From_for_email__c, RecordTypeId, Training_Offer__r.Group_Status__c
            FROM Enrollment__c
            WHERE (
                (RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)
                 AND Status__c IN :cummulativeStatusesForOpen
                 AND Training_Offer__r.Group_Status__c IN :cummulativeStatusesForSchedule)
                OR 
                (RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
                 AND Status__c IN :cummulativeStatusesForClosed
                 AND Training_Offer__r.Group_Status__c IN :cummulativeStatusesForSchedule)
            )
        ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Enrollment__c> enrList = (List<Enrollment__c>) scope;

        List<Target__c> targetList = getListOfApplicableTargets(enrList);

        Map<Id, List<Enrollment__c>> targetIdToEnrListMap = mapTargetsWithEnrollments(targetList, enrList);

        for (Target__c trainingTarget : targetList) {
            List<Enrollment__c> enrListForTarget = targetIdToEnrListMap.get(trainingTarget.Id);

            if (enrListForTarget != null) {
                for (Enrollment__c enr : enrListForTarget) {
                    trainingTarget.Realization_Amount_Batch_Helper_curr__c += enr.Value_after_Discount__c;
                }
            }
        }

        try {
            update targetList;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        TrainingSalesTargetFinalizationBatch tstfb = new TrainingSalesTargetFinalizationBatch(isRunManually);
        Database.executeBatch(tstfb);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // methods returns list of Targets that are influenced by queried by batch Enrollments
    private static List<Target__c> getListOfApplicableTargets(List<Enrollment__c> enrList) {
        Set<String> univNames = new Set<String>();
        for (Enrollment__c studyEnr : enrList) {
            univNames.add(studyEnr.University_Name__c);
        }

        List<Target__c> targetList = [
            SELECT Id, Realization_Amount_Batch_Helper_curr__c, University_WSB__r.Name, Applies_from__c, Applies_to__c, Trainings_Type__c
            FROM Target__c
            WHERE University_WSB__r.Name IN :univNames
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TRAINING_ENR_TARGET)
        ];

        return targetList;
    }
    
    // methods returns map of Target Ids to List of Applicable Enrollments 
    private static Map<Id, List<Enrollment__c>> mapTargetsWithEnrollments(List<Target__c> targetList, List<Enrollment__c> enrList) {
        Map<Id, List<Enrollment__c>> resultMap = new Map<Id, List<Enrollment__c>>();

        for (Target__c target : targetList) {
            Datetime appliesFromPrecise = DateTime.newInstance(target.Applies_from__c.year(), target.Applies_from__c.month(), target.Applies_from__c.day(), 
                                                               0, 0, 0);

            Datetime appliesToPrecise = DateTime.newInstance(target.Applies_to__c.year(), target.Applies_to__c.month(), target.Applies_to__c.day(), 
                                                             23, 59, 59);

            for (Enrollment__c enr : enrList) {
                if (target.University_WSB__r.Name.equals(enr.University_Name__c)
                    && enr.Valid_From_for_email__c >= appliesFromPrecise && enr.Valid_From_for_email__c <= appliesToPrecise
                    && ((
                            target.Trainings_Type__c == CommonUtility.TARGET_TRAININGSTYPE_OPENCLOSED
                        ) || (
                            target.Trainings_Type__c == CommonUtility.TARGET_TRAININGSTYPE_OPEN
                            && enr.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING)
                        ) || (
                            target.Trainings_Type__c == CommonUtility.TARGET_TRAININGSTYPE_CLOSED
                            && enr.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
                        )
                    )
                ) {
                    putEnrToMapOfLists(resultMap, enr, target.Id);
                }
            }
        }

        return resultMap;
    }

    // method puts Enrollment to list under specific key or creates new List if one doesn't exist yet
    private static void putEnrToMapOfLists(Map<Id, List<Enrollment__c>> targetKeyToEnrListMap, Enrollment__c enrToPut, Id key) {
        if (targetKeyToEnrListMap.containsKey(key)) {
            List<Enrollment__c> enrListFromMap = targetKeyToEnrListMap.get(key);
            enrListFromMap.add(enrToPut);
        } else {
            List<Enrollment__c> enrList = new List<Enrollment__c> { enrToPut };
            targetKeyToEnrListMap.put(key, enrList);
        }
    }

}