/**
*   @author         Radosław Urbański
*   @description    This class is used to test functionality of IntegrationDocumentUpload which should upload documents for enrollment. 
**/

@isTest
private class ZTestIntegrationDocumentUpload {
	
	@isTest static void test_uploadDocument_success() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);
		Enrollment__c enrollmentDocument = [SELECT Id FROM Enrollment__c WHERE Enrollment_from_Documents__c = :enrollment.Id AND Document__r.Name = :RecordVals.CATALOG_DOCUMENT_AGREEMENT LIMIT 1];

		String requestBody = '{"document_id":"' + enrollmentDocument.Id + '", "uploaded_file_name":"Test upload document", "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		Attachment resultAttachment = [SELECT Name, Body FROM Attachment];

		System.assertEquals(200, res.statusCode);
		System.assertNotEquals(null, res.responseBody);
		System.assertEquals(CommonUtility.UPLOADED_DOCUMENT_PREFIX + 'Test upload document', resultAttachment.Name);
		System.assertNotEquals(null, resultAttachment.Body);
	}
	
	@isTest static void test_uploadDocument_missingEnrollmentId() {
		// given
		String requestBody = '{"document_id":"someId", "uploaded_file_name":"Test upload document", "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(605, 'enrollment_id'), res.responseBody);
	}
	
	@isTest static void test_uploadDocument_missingDocumentId() {
		// given
		String requestBody = '{"document_id":"", "uploaded_file_name":"Test upload document", "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', 'someId2');
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'document_id'), res.responseBody);
	}

	@isTest static void test_uploadDocument_missingFileName() {
		// given
		String requestBody = '{"document_id":"someId", "uploaded_file_name": null , "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', 'someId2');
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_name'), res.responseBody);
	}

	@isTest static void test_uploadDocument_missingFileBody() {
		// given
		String requestBody = '{"document_id":"someId", "uploaded_file_name":"Test upload document", "uploaded_file_content":null}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', 'someId2');
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(602, 'uploaded_file_content'), res.responseBody);
	}

	@isTest static void test_uploadDocument_enrollmentDoesntExist() {
		// given
		String requestBody = '{"document_id":"someId", "uploaded_file_name":"Test upload document", "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', '5003000000D8cuI');
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604), res.responseBody);
	}

	@isTest static void test_uploadDocument_documentDoesntExist() {
		// given
		ZDataTestUtility.prepareCatalogData(true, true);
		Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

		String requestBody = '{"document_id":"5003000000D8cuI", "uploaded_file_name":"Test upload document", "uploaded_file_content":"blob"}';

		RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.params.put('enrollment_id', String.valueOf(enrollment.Id));
        req.requestBody = Blob.valueof(requestBody);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

		// when
		Test.startTest();
		IntegrationDocumentUpload.uploadDocument();
		Test.stopTest();

		// then
		System.assertEquals(400, res.statusCode);
		System.assertEquals(IntegrationError.getErrorJSONBlobByCode(604), res.responseBody);
	}
}