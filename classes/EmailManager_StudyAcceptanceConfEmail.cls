/**
* @author       Wojciech Słodziak
* @description  Class to send Study Acceptance confirmation email - based on workflows.
**/

public without sharing class EmailManager_StudyAcceptanceConfEmail {


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- TEMPLATE LIST ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private enum WorfklowEmailNames { // also names of templates
        X28_I_II_StudyAccConf, /* I II U */
        X28_PG_StudyAccConf, /* PG */
        X28_MBA_StudyAccConf /* MBA */
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ TEMPLATE DESIGNATION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static String designateWorkflowNameForEnrollment(Enrollment__c studyEnr) {
        WorfklowEmailNames workflow;

        String language = CommonUtility.getLanguageAbbreviation(studyEnr.Language_of_Enrollment__c);

        if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_I || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_U
            || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II || studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_II_PG) {
            workflow = WorfklowEmailNames.X28_I_II_StudyAccConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            workflow = WorfklowEmailNames.X28_PG_StudyAccConf;
        }
        else if (studyEnr.Degree__c == CommonUtility.OFFER_DEGREE_MBA) {
            workflow = WorfklowEmailNames.X28_MBA_StudyAccConf;
        }

        return workflow != null? (language + '_' + workflow.name()) : null;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ INVOKATION METHODS ---------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method for sending Study Acceptance confimation email
    public static void sendStudyAcceptanceConfirmationEmail(Set<Id> enrollmentIdList) {
        List<Enrollment__c> enrollmentList = [
            SELECT Id, Degree__c, Candidate_Student__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList 
            AND Candidate_Student__r.Email != null
            AND Dont_send_automatic_emails__c = false
            AND TECH_Send_Acceptance_Email__c = true
        ];

        List<EmailTemplate> emailTemplates = [
            SELECT Id, Name
            FROM EmailTemplate
        ];

        List<Task> tasksToInsert = new List<Task>();
        List<Enrollment__c> enrollmentsWithFailure = new List<Enrollment__c>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();
        for (Enrollment__c enrollment : enrollmentList) {
            String workflowTriggerName = EmailManager_StudyAcceptanceConfEmail.designateWorkflowNameForEnrollment(enrollment);

            if (workflowTriggerName != null) {
                recordIdToWorkflowTriggerName.put(enrollment.Id,  workflowTriggerName);
                
                String emailTemplateId = '';
                String degree = '';
                if (enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II_PG ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_II ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_I ||
                    enrollment.Degree__c == CommonUtility.OFFER_DEGREE_U) {
                    degree = CommonUtility.OFFER_DEGREE_I + '_' + CommonUtility.OFFER_DEGREE_II;
                }
                else {
                    degree = enrollment.Degree__c;
                }

                String language = CommonUtility.getLanguageAbbreviation(enrollment.Language_of_Enrollment__c);
                for (EmailTemplate et : emailTemplates) {
                    if (degree == CommonUtility.OFFER_DEGREE_PG) degree = 'SP';
                    if (et.Name == '[' + language + '][28] Mail informujący kandydata o przyjęciu na studia') {
                        emailTemplateId = et.Id;
                    }
                }

                tasksToInsert.add(EmailManager.createEmailTask(
                    Label.title_StudyAcceptanceConfirmationSent,
                    enrollment.Id,
                    enrollment.Candidate_Student__c,
                    EmailManager.prepareFakeEmailBody(emailTemplateId, enrollment.Id),
                    null
                ));
            } else {
                enrollmentsWithFailure.add(enrollment);
            }
        }
        
        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }


        if (!enrollmentsWithFailure.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_StudyAcceptanceConfEmail.class.getName() 
                + ' ' + JSON.serialize(enrollmentsWithFailure));
        }
    }

}