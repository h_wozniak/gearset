global with sharing class AddCreateTrainerController {


    public Contact trainerToCreate { get; set; }
    public Id scheduleId { get; set; }
    public Boolean saveResult { get; set; }

    public AddCreateTrainerController() {
        scheduleId = Apexpages.currentPage().getParameters().get('scheduleId');

        trainerToCreate = new Contact();
        trainerToCreate.Position__c = CommonUtility.CONTACT_POSITION_TRAINER;
    }


    public void saveAndPin() {
        trainerToCreate.RecordTypeId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_CONTACTPERSON);

        try {
            insert trainerToCreate;
            Offer__c schedule = new Offer__c(Id = scheduleId);
            schedule.Trainer__c = trainerToCreate.Id;
            update schedule;
            saveResult = true;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            saveResult = false;
        }
    }

    public List<Schema.FieldSetMember> getTrainerFields() {
        return SObjectType.Contact.FieldSets.TrainerFields.getFields();
    }

    @RemoteAction
    global static void setTrainer(Id trainerId, Id scheduleId) {
        try {
            Offer__c schedule = new Offer__c(Id = scheduleId);
            schedule.Trainer__c = trainerId;
            update schedule;
        } catch(Exception e) {
            ErrorLogger.log(e);
        }
        
    }

    @RemoteAction
    global static List<Contact> getContacts(String query) {
        query = String.escapeSingleQuotes(query);

        List<String> splitQueries = query.split(' ');
        if (splitQueries.isEmpty()) {
            splitQueries.add(query);
        }

        String soqlQuery = 'SELECT Id, Name, Email, Phone, MobilePhone, AccountId, Account.Name FROM Contact ';
        
        String whereString = 'WHERE';
        for (Integer i = 0; i < splitQueries.size(); i++) {
             whereString += (i != 0? ' AND ': ' ') + '(FirstName LIKE \'%' + splitQueries[i] + '%\' OR LastName LIKE \'%' + splitQueries[i] + '%\')';
        }
        whereString += + ' LIMIT 5';

        soqlQuery += whereString ;

        try {
            return Database.query(soqlQuery);
        }
        catch(Exception e) {
            ErrorLogger.log(e);
            return null;
        }       
    }

}