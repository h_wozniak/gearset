@isTest
private class ZTestVocativeRule {

    @isTest static void testGenerationRules() {
        String result; 

        result = VocativeRule.generateNameInVocative('Wrocław');
        System.assertEquals(result, 'Wrocławiu');

        result = VocativeRule.generateNameInVocative('Gdynia');
        System.assertEquals(result, 'Gdyni');

        result = VocativeRule.generateNameInVocative('Gdańsk');
        System.assertEquals(result, 'Gdańsku');

        result = VocativeRule.generateNameInVocative('Opole');
        System.assertEquals(result, 'Opolu');

        result = VocativeRule.generateNameInVocative('Poznań');
        System.assertEquals(result, 'Poznaniu');

        result = VocativeRule.generateNameInVocative('Toruń');
        System.assertEquals(result, 'Toruniu');

        result = VocativeRule.generateNameInVocative('Szczecin');
        System.assertEquals(result, 'Szczecinie');

        result = VocativeRule.generateNameInVocative('Chorzów');
        System.assertEquals(result, 'Chorzowie');

        result = VocativeRule.generateNameInVocative('Bydgoszcz');
        System.assertEquals(result, 'Bydgoszczy');
    }
}