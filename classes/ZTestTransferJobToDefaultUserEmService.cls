@isTest
private class ZTestTransferJobToDefaultUserEmService {

    private static Map<Integer, sObject> prepareData() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Documents_Collected__c = false,
                Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED,
                Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI
            ), 
            true
        ));
        
        return retMap;
    }


    @isTest static void test_transferJob_sendOverlimitEmailToCourseAdmin() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        TransferJobToDefaultUserEmService.TransferJobDescription jobDesc = new TransferJobToDefaultUserEmService.TransferJobDescription();

        jobDesc.jobName = TransferJobToDefaultUserEmService.Transferable.COURSE_OVERLIMIT_EMAIL.name();
        jobDesc.jobRecordId = null;

        email.plainTextBody = JSON.serialize(jobDesc);

        TransferJobToDefaultUserEmService classObj = new TransferJobToDefaultUserEmService();

        Test.startTest();
        Messaging.InboundEmailResult response = classObj.handleInboundEmail(email, env);
        Test.stopTest();

        System.assert(response.success);
    }

    @isTest static void test_transferJob_sendRequiredDocumentList() {
        Map<Integer, sObject> dataMap = prepareData();

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        TransferJobToDefaultUserEmService.TransferJobDescription jobDesc = new TransferJobToDefaultUserEmService.TransferJobDescription();

        jobDesc.jobName = TransferJobToDefaultUserEmService.Transferable.DOC_LIST_EMAIL.name();
        jobDesc.jobRecordId = dataMap.get(0).Id;

        email.plainTextBody = JSON.serialize(jobDesc);

        TransferJobToDefaultUserEmService classObj = new TransferJobToDefaultUserEmService();

        Test.startTest();
        Messaging.InboundEmailResult response = classObj.handleInboundEmail(email, env);
        Test.stopTest();

        System.assert(response.success);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_transferJob_ErrorJSON() {

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        TransferJobToDefaultUserEmService.TransferJobDescription jobDesc = new TransferJobToDefaultUserEmService.TransferJobDescription();

        jobDesc.jobName = TransferJobToDefaultUserEmService.Transferable.DOC_LIST_EMAIL.name();
        jobDesc.jobRecordId = null;

        email.plainTextBody = 'forErr' + JSON.serialize(jobDesc) + 'forErr';

        TransferJobToDefaultUserEmService classObj = new TransferJobToDefaultUserEmService();

        Test.startTest();
        Messaging.InboundEmailResult response = classObj.handleInboundEmail(email, env);
        Test.stopTest();

        System.assert(!response.success);
    }

    @isTest static void test_transferJob_tranfserJob() {
        Map<Integer, sObject> dataMap = prepareData();

        TransferJobToDefaultUserEmService.tranfserJob(TransferJobToDefaultUserEmService.Transferable.DOC_LIST_EMAIL, new Set<Id> { dataMap.get(0).Id });
    }
}