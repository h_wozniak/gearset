@isTest
private class ZTestEmailManager {
    
    private static Map<Integer, sObject> prepareData1() {
        Date myDate = Date.today();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Valid_To__c = myDate.addDays(2),
            Valid_From__c = myDate.addDays(2)
        ), true));
        retMap.put(2, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Training_Participant__c = retMap.get(2).Id
        ), true));

        return retMap;
    }

    private static Map<Integer, sObject> prepareData2() {
        Date myDate = Date.today();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Valid_To__c = myDate.addDays(2),
            Valid_From__c = myDate.addDays(2)
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Reporting_Person__c = ZDataTestUtility.createContactPerson(null, true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Training_Participant__c = retMap.get(2).Id
        ), true));

        return retMap;
    }

    @future
    private static void prepareTemplates() {
        List<EmailTemplate> templateList = new List<EmailTemplate>();

        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'name';
        validEmailTemplate.DeveloperName = EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_10_3;
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        validEmailTemplate.Subject = 'Your Subject Here';
        templateList.add(validEmailTemplate);

        EmailTemplate validEmailTemplate2 = new EmailTemplate();
        validEmailTemplate2.isActive = true;
        validEmailTemplate2.Name = 'name';
        validEmailTemplate2.DeveloperName = EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_11;
        validEmailTemplate2.TemplateType = 'text';
        validEmailTemplate2.FolderId = UserInfo.getUserId();
        validEmailTemplate2.Subject = 'Your Subject Here';
        templateList.add(validEmailTemplate2);

        EmailTemplate validEmailTemplate3 = new EmailTemplate();
        validEmailTemplate3.isActive = true;
        validEmailTemplate3.Name = 'name';
        validEmailTemplate3.DeveloperName = EmailManager.EMAIL_TEMPLATE_EMAIL_AFTER_TRAINING_ENROLLMENT_2_1;
        validEmailTemplate3.TemplateType = 'text';
        validEmailTemplate3.FolderId = UserInfo.getUserId();
        validEmailTemplate3.Subject = 'Your Subject Here';
        templateList.add(validEmailTemplate3);

        EmailTemplate validEmailTemplate8 = new EmailTemplate();
        validEmailTemplate8.isActive = true;
        validEmailTemplate8.Name = 'name';
        validEmailTemplate8.DeveloperName = EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_COURSE;
        validEmailTemplate8.TemplateType = 'text';
        validEmailTemplate8.FolderId = UserInfo.getUserId();
        validEmailTemplate8.Subject = 'Your Subject Here';
        templateList.add(validEmailTemplate8);

        EmailTemplate validEmailTemplate9 = new EmailTemplate();
        validEmailTemplate9.isActive = true;
        validEmailTemplate9.Name = 'name';
        validEmailTemplate9.DeveloperName = EmailManager.EMAIL_TEMPLATE_LIMIT_EXCEEDED_SPEC;
        validEmailTemplate9.TemplateType = 'text';
        validEmailTemplate9.FolderId = UserInfo.getUserId();
        validEmailTemplate9.Subject = 'Your Subject Here';
        templateList.add(validEmailTemplate9);

        insert templateList;
    }
    
    @isTest static void testEmailManagerSendOverlimitEmailToTrainingAdmin() {
        Map<Integer, sObject> dataMap = prepareData1();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareTemplates();
            Test.stopTest();
        }

        Id scheduleOfferId = dataMap.get(0).Id;
        Set<Id> scheduleIdSet = new Set<Id>();
        scheduleIdSet.add(scheduleOfferId);
        
        EmailManager.sendOverlimitEmailToTrainingAdmin(scheduleIdSet);
    }

    @isTest static void testEmailManagerSendPaymentDataToParticipants() {
        Map<Integer, sObject> dataMap = prepareData1();

        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareTemplates();
            Test.stopTest();
        }

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(dataMap.get(3).Id);
        
        EmailManager.sendPaymentDataToParticipants(enrollmentIdList);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void testsendPaymentDataToReportingPerson() {
        Map<Integer, sObject> dataMap = prepareData1();

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(dataMap.get(3).Id);
        
        EmailManager.sendPaymentDataToReportingPerson(enrollmentIdList);
    }

    @isTest static void testEmailManagerSendVerificationLink() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareTemplates();
            Test.stopTest();
        }

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager.sendVerificationLink(enrollmentIdList);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void testEmailManagerSendVerificationLinkButton() {
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            Test.startTest();
            prepareTemplates();
            Test.stopTest();
        }

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(null, true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager.sendVerificationLinkFinalFromButton(false, enrollmentIdList);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }
    
    @isTest static void testSendOverlimitEmailToCourseAdmin() {
        Offer__c trainingOffer = ZDataTestUtility.createCourseOffer(null, true);

        Test.startTest();
        EmailManager.sendOverlimitEmailToCourseAdmin(new Set<Id> { trainingOffer.Id });
        Test.stopTest();
    }
    
    @isTest static void testSendOverlimitEmailToCourseAdminFinal() {
        Offer__c trainingOffer = ZDataTestUtility.createCourseOffer(null, true);

        Test.startTest();
        EmailManager.sendOverlimitEmailToCourseAdminFinal(new Set<Id> { trainingOffer.Id });
        Test.stopTest();
    }
    
}