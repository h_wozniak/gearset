/**
* @description  This class is a Trigger Handler for Study Enrollments - field edit disabling
**/

public without sharing class TH_EnrollmentDisables2 extends TriggerHandler.DelegateBase {

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        for (Id key : oldEnrollments.keySet()) {
            Enrollment__c oldEnrollment = oldEnrollments.get(key);
            Enrollment__c newEnrollment = newEnrollments.get(key);


            /* ------------------------------------------------------------------------------------------------ */
            /* --------------------------------------- MAIN ENROLLMENT ---------------------------------------- */
            /* ------------------------------------------------------------------------------------------------ */

            /* Sebastian Łasisz */
            // on Enrollment with final statuses disable editing (fields that are visibile on agreement)
            // Michal Bury 13.02.2017 - disabling            
            if (!CommonUtility.checkIfCanEditUneditableEnrollment()
                && !CommonUtility.skipEnrollmentEditDisabling 
                && newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && (newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT
                    || newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS
                    || newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS)
                && (
                    oldEnrollment.Discount_Code__c != newEnrollment.Discount_Code__c
                    || oldEnrollment.Kind__c != newEnrollment.Kind__c
                    || oldEnrollment.Mode__c != newEnrollment.Mode__c
                    || oldEnrollment.Degree__c != newEnrollment.Degree__c
                    || oldEnrollment.Tuition_System__c != newEnrollment.Tuition_System__c
                    || oldEnrollment.Kind__c != newEnrollment.Kind__c
                    || oldEnrollment.University_Name__c != newEnrollment.University_Name__c
                    || oldEnrollment.Trade_Name__c != newEnrollment.Trade_Name__c
                    || oldEnrollment.Trade_Name_Postgraduate_Course__c != newEnrollment.Trade_Name_Postgraduate_Course__c
                    || oldEnrollment.Trade_Name_Specialty__c != newEnrollment.Trade_Name_Specialty__c
                    || oldEnrollment.Price_Book_from_Enrollment__c != newEnrollment.Price_Book_from_Enrollment__c
                    || oldEnrollment.Installments_per_Year__c != newEnrollment.Installments_per_Year__c
                    || oldEnrollment.Initial_Specialty_Declaration__c != newEnrollment.Initial_Specialty_Declaration__c                    
                )
            ) {
                newEnrollment.addError(Label.msg_error_cantEditEnrollment);
            }

            

            /* Sebastian Łasisz */
            // on Enrollment Study disable editing Unerolled field when Status is later than Accepted
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Unenrolled__c != oldEnrollment.Unenrolled__c
                && (
                    EnrollmentManager_Studies.finalizedEnrStatuses.contains(oldEnrollment.Status__c)
                    || oldEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_RESIGNATION
                    || oldEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_ACCEPTED
                )
            ) {
                //newEnrollment.addError(Label.msg_error_cantEditUnenrolled);
            }

            /* Wojciech Słodziak */
            // if process is started for uennrolled candidate it must be sent to ks before it can be finished
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Unenrolled__c != oldEnrollment.Unenrolled__c && newEnrollment.Unenrolled__c == false 
                && UnenrolledManager.finalizedUnenrStatuses.contains(oldEnrollment.Unenrolled_Status__c)
                && newEnrollment.Unenrolled_Status__c != CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS) {
                //newEnrollment.addError(Label.msg_error_UnenrolledMustBeSentToKS);
            }

            /* Wojciech Słodziak */
            // on Enrollment Study disable choosing Unenrolled path again if it was already followed
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
                && newEnrollment.Unenrolled__c != oldEnrollment.Unenrolled__c && newEnrollment.Unenrolled__c == true 
                && UnenrolledManager.finalizedUnenrStatuses.contains(oldEnrollment.Unenrolled_Status__c)) {
                newEnrollment.addError(Label.msg_error_CantSelectUnenrolledAgain);
            }

            /* Wojciech Słodziak */
            // on Enrollment Study update with Discount Code change check if can apply Code Discounts and disapply old Discounts
            if (!CommonUtility.skipCodeDiscountApplication 
                && oldEnrollment.Discount_Code__c != newEnrollment.Discount_Code__c 
                && (newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_RESIGNATION
                || newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT 
                || newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS 
                || newEnrollment.Status__c == CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS)) {
                newEnrollment.addError(Label.msg_error_cantAttachCodeToEnrollment);
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ---------------------------------------- LANGUAGE ENR ------------------------------------------ */
            /* ------------------------------------------------------------------------------------------------ */

            /* Sebastian Łasisz */
            // Disable editing of Language_c on Enrollment
            //if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE) 
            //    && newEnrollment.Foreign_Language__c != oldEnrollment.Foreign_Language__c) {
            //    newEnrollment.addError(Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Enrollment__c', 'Foreign_Language__c'));
            //}

            /* Sebastian Łasisz */
            // Disable editing of Enrollment lookup on Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_FOREIGN_LANGUAGE) 
                && newEnrollment.Enrollment_from_Language__c != oldEnrollment.Enrollment_from_Language__c) {
                newEnrollment.addError(Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Enrollment__c', 'Enrollment_from_Language__c'));
            }



            /* ------------------------------------------------------------------------------------------------ */
            /* ---------------------------------------- DOCUMENT ENR ------------------------------------------ */
            /* ------------------------------------------------------------------------------------------------ */

            /* Wojciech Słodziak */
            // Disable editing of Enrollment_from_Documents__c on Required_Document
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT) 
                && newEnrollment.Enrollment_from_Documents__c != oldEnrollment.Enrollment_from_Documents__c 
                && oldEnrollment.Enrollment_from_Documents__c != null) {
                newEnrollment.addError(Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Enrollment__c', 'Enrollment_from_Documents__c'));
            }

            /* Sebastian Łasisz */
            // Disable editing Document lookup on Enrollment
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT) 
                && newEnrollment.Document__c != oldEnrollment.Document__c) {
                newEnrollment.addError(Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Enrollment__c', 'Document__c'));    
            }

        }
    }

}