@isTest
private class ZTestMergeSetOfContactsBatch {

	@isTest static void test_batch() {
        Contact c1 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com',
            Confirmed_Contact__c = false,
            University_for_sharing__c = 'WSB Wrocław',
            Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO
        ), true);

        Contact c2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'To1m',
            LastName = 'Han1ks',
            Pesel__c = '81031200760',
            Email = 't1om.ha1nks@mail.com',
            Confirmed_Contact__c = false,
            University_for_sharing__c = 'WSB Wrocław',
            Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO
        ), true);      

        Test.startTest();
        Database.executeBatch(new MergeSetOfContactsBatch(new List<Id>{c1.Id}, new List<Id>{c2.Id}), 1);
    	Test.stopTest();

        List<Contact> cAfter = [SELECT Id, FirstName FROM Contact];
        System.assertEquals(cAfter.size(), 1);
        System.assertEquals(cAfter.get(0).FirstName, 'Tom');
	}

    @isTest static void test_batch5contacts() {
        List<Contact> masterContacts = ZDataTestUtility.createCandidateContacts(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com',
            Confirmed_Contact__c = false,
            University_for_sharing__c = 'WSB Wrocław',
            Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO
        ), 5, true);

        List<Contact> mergedContacts = ZDataTestUtility.createCandidateContacts(new Contact(
            FirstName = 'To1m',
            LastName = 'Han1ks',
            Pesel__c = '81031200760',
            Email = 't1om.ha1nks@mail.com',
            Confirmed_Contact__c = false,
            University_for_sharing__c = 'WSB Wrocław',
            Creation_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO
        ), 5, true);      

        List<Contact> cBefore = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'Tom'];
        System.assertEquals(cBefore.size(), 5);

        List<Contact> c2Before = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'To1m'];
        System.assertEquals(cBefore.size(), 5);

        List<Id> masterContactIds = new List<Id>();
        for (Contact masterContact : masterContacts) {
            masterContactIds.add(masterContact.Id);
        }

        List<Id> mergedContactIds = new List<Id>();
        for (Contact mergedContact : mergedContacts) {
            mergedContactIds.add(mergedContact.Id);
        }

        Test.startTest();
        Database.executeBatch(new MergeSetOfContactsBatch(masterContactIds, mergedContactIds), 5);
        Test.stopTest();

        List<Contact> cAfter = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'Tom'];
        System.assertEquals(cAfter.size(), 5);

        List<Contact> c2After = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'To1m'];
        System.assertEquals(c2After.size(), 0);
    }
}