@isTest
private class ZTestWebserviceUtilitiesWS {

    
    @isTest static void testDeletingRecordsWithoutPermission() {        
        ZDataTestUtility.prepareCatalogData(true, true);

        Test.startTest();
        List<Enrollment__c> studyEnrollments = ZDataTestUtility.createEnrollmentStudies(null, 1, true);
        Test.stopTest();
        
        List<Id> studyEnrollmentIds = new List<Id>();
        for (Enrollment__c studyEnrollment : studyEnrollments) {
            studyEnrollmentIds.add(studyEnrollment.Id);
        }
        Boolean result = WebserviceUtilitiesWS.deleteRecords(studyEnrollmentIds, 'Enrollment__c');
        
        System.assert(result);
        
        List<Enrollment__c> enrollments = [SELECT Id FROM Enrollment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)];
        System.assertEquals(enrollments.size(), 0);
    }
    
    @isTest static void testDeletingRecordsWithoutPermissionWithWrongSchema() {        
        ZDataTestUtility.prepareCatalogData(true, true);

        Test.startTest();
        List<Enrollment__c> studyEnrollments = ZDataTestUtility.createEnrollmentStudies(null, 1, true);        
        Test.stopTest();
        
        List<Id> studyEnrollmentIds = new List<Id>();
        for (Enrollment__c studyEnrollment : studyEnrollments) {
            studyEnrollmentIds.add(studyEnrollment.Id);
        }
        try {
            Boolean result = WebserviceUtilitiesWS.deleteRecords(studyEnrollmentIds, 'Offer__c');
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('Invalid id value for this SObject type'));
        }
        
        List<Enrollment__c> enrollments = [SELECT Id FROM Enrollment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)];
        System.assertEquals(enrollments.size(), 1);
    }
    
    @isTest static void testDeletingRecordsWithoutPermissionWithNoSchema() {   
        ZDataTestUtility.prepareCatalogData(true, true);

        Test.startTest();
        List<Enrollment__c> studyEnrollments = ZDataTestUtility.createEnrollmentStudies(null, 1, true);        
        Test.stopTest();
        
        List<Id> studyEnrollmentIds = new List<Id>();
        for (Enrollment__c studyEnrollment : studyEnrollments) {
            studyEnrollmentIds.add(studyEnrollment.Id);
        }
        try {
            Boolean result = WebserviceUtilitiesWS.deleteRecords(studyEnrollmentIds, null);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
        }
        
        List<Enrollment__c> enrollments = [SELECT Id FROM Enrollment__c WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)];
        System.assertEquals(enrollments.size(), 1);
    }
}