@isTest
private class ZTestErrorLogger {

    @isTest static void testErrorLogger() {
        Test.startTest();
        ErrorLogger_Email__c mail = new ErrorLogger_Email__c(Name='Test', Email__c = 'test@test.co', Active__c = true);
        insert mail;
        ErrorLogger.msg('Test message');
        Test.stopTest();
    }

    @isTest static void testErrorLoggerException() {
        Test.startTest();
        ErrorLogger_Email__c mail = new ErrorLogger_Email__c(Name='Test', Email__c = 'test@test.co', Active__c = true);
        insert mail;
        try {
            Contact c = null;
            insert c;
        }
        catch(Exception e) {
            ErrorLogger.log(e);
        }
        Test.stopTest();
    }

}