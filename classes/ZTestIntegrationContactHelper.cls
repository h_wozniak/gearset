@isTest
private class ZTestIntegrationContactHelper {    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(
            Phone = '123456789',
            MobilePhone = '123123',
            Firstname = 'Bob',
            Gender__c = CommonUtility.CONTACT_GENDER_MALE,
            Additional_Emails__c = 'test@test.com',
            Pesel__c = '05261508023',
            Country_of_Origin__c = 'Poland',
            The_Same_Mailling_Address__c = true,
            OtherCountry = 'Poland',
            OtherState = 'Lower Silesia',
            OtherCity = 'Wrocław',
            OtherStreet = 'Krzyki'
        ), true));
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createMarketingCampaign(null, true));
        retMap.put(3, ZDataTestUtility.createCampaignOffer(new Marketing_Campaign__c(Campaign_from_Campaign_Offer__c = retMap.get(2).Id, Campaign_Offer__c = retMap.get(1).Id), true));
        retMap.put(4, ZDataTestUtility.createCourseOffer(new offer__c(University_Study_Offer_from_Course__c = retMap.get(1).Id), true));
        Test.startTest();
        retMap.put(5, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(4).Id, Candidate_Student__c = retMap.get(0).Id), true));
        Test.stopTest();

        Enrollment__c studyEnrAgr = new Enrollment__c();
        studyEnrAgr.RecordTypeId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR);
        studyEnrAgr.Source_Enrollment__c = retMap.get(5).Id;
        studyEnrAgr.Student_from_Educational_Agreement__c = retMap.get(0).Id;
        insert studyEnrAgr;

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_ENR_2,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_ENROLLMENT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_1,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_2,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_3,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_4,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);

        ZDataTestUtility.createBaseConsent(new Catalog__c(
            Name = RecordVals.MARKETING_CONSENT_5,
            Type__c = CommonUtility.CATALOG_TYPE_FOR_CONTACT
        ), true);
        
        return retMap;
    }


	@isTest static void test_prepareResponse() {
        Map<Integer, sObject> retMap = prepareData(); 

        IntegrationContactHelper.ContactToUpdateWrapper wrapper = new IntegrationContactHelper.ContactToUpdateWrapper();
        wrapper.contact_id = retMap.get(0).Id;
        wrapper.ifFCC = true;
        wrapper.ifExperia = true;
        wrapper.ifMA = true;
        wrapper.ifZPI = true;

        Set<Id> candidateIds = new Set<Id> { retMap.get(0).Id };
        List<IntegrationContactHelper.ContactToUpdateWrapper> wrappers = new List<IntegrationContactHelper.ContactToUpdateWrapper>();
        wrappers.add(wrapper);
        
        IntegrationContactHelper.prepareResponse(wrappers);
	}

	@isTest static void test_phonesToList() {
		Test.startTest();
		IntegrationContactHelper.phonesToList('123', '456', '789');
		Test.stopTest();
	}

	@isTest static void test_campaignsToList() { 
        Map<Integer, sObject> retMap = prepareData();

        List<Marketing_Campaign__c> campaigns = new List<Marketing_Campaign__c>();
        campaigns.add((Marketing_Campaign__c)retMap.get(2));

        IntegrationContactHelper.campaignsToList(campaigns);
	}

	@isTest static void test_prepareConsents() { }

	@isTest static void test_prepareCallCenterResponse() { 
        Map<Integer, sObject> retMap = prepareData();

        List<Marketing_Campaign__c> campaigns = new List<Marketing_Campaign__c>();
        campaigns.add((Marketing_Campaign__c)retMap.get(2));

        IntegrationContactHelper.prepareCallCenterResponse((Contact)retMap.get(0), campaigns);
	}

    @isTest static void test_prepareDepartmentForIPresso() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
        Marketing_Campaign__c iPressoContact = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertNotEquals(candidate.University_for_sharing__c, null);

        IntegrationContactHelper.MarketingAutomation_JSON candidate_JSON = new IntegrationContactHelper.MarketingAutomation_JSON();
        IntegrationContactHelper.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'poznan');
    }

    @isTest static void test_prepareDepartmentForIPresso_Enrollment() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Candidate_Student__c = candidate.Id), true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, Status__c, University_Name__c FROM Enrollments_Studies__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań, WSB Wrocław');

        Marketing_Campaign__c iPressoContact = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationContactHelper.MarketingAutomation_JSON candidate_JSON = new IntegrationContactHelper.MarketingAutomation_JSON();
        IntegrationContactHelper.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'wroclaw');
    }

    @isTest static void test_prepareDepartmentForIPresso_Didactic() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Enrollment__c didactic = ZDataTestUtility.createEducationalAgreement(new Enrollment__c(Student_from_Educational_Agreement__c = candidate.Id, University_Name__c = 'WSB Gdańsk'), true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        Marketing_Campaign__c iPressoContact = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationContactHelper.MarketingAutomation_JSON candidate_JSON = new IntegrationContactHelper.MarketingAutomation_JSON();
        IntegrationContactHelper.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'gdansk');
    }

    @isTest static void test_prepareDepartmentForIPresso_Lead() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Marketing_Campaign__c lead = ZDataTestUtility.createReturnedLead(new Marketing_Campaign__c(Lead_UniversityWSB__c = 'WSB Gdańsk', Contact_From_Returning_Leads__c = candidate.Id), true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        Marketing_Campaign__c iPressoContact = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Contact_from_iPresso__c = :candidate.Id                                                                                                     
        ];

        IntegrationContactHelper.MarketingAutomation_JSON candidate_JSON = new IntegrationContactHelper.MarketingAutomation_JSON();
        IntegrationContactHelper.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertEquals(candidate_JSON.department, 'gdansk');
    }

    @isTest static void test_prepareDepartmentForIPresso_training() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

        candidate.University_for_sharing__c = 'WSB Poznań';
        update candidate;

        candidate = [
            SELECT Id, University_for_sharing__c
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        Test.startTest();
        Offer__c training = ZDataTestUtility.createTrainingOffer(null, true);
        Enrollment__c participant = ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(Participant__c = candidate.Id, Training_Offer_for_Participant__c = training.Id) , true);
        Test.stopTest();

        candidate = [
            SELECT Id, University_for_sharing__c,
            (SELECT Id, University_Name__c FROM Enrollments_Studies__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, University_Name__c FROM EducationalAgreements__r WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC LIMIT 1)
            FROM Contact
            WHERE Id = :candidate.Id
        ];

        System.assertEquals(candidate.Participants__r.size(), 1);
        System.assertEquals(candidate.University_for_sharing__c, 'WSB Poznań');

        IntegrationContactHelper.MarketingAutomation_JSON candidate_JSON = new IntegrationContactHelper.MarketingAutomation_JSON();
        IntegrationContactHelper.prepareDepartmentForIPresso(candidate_JSON, candidate);
        System.assertNotEquals(candidate_JSON.department, 'WSB Poznań');
    }
}