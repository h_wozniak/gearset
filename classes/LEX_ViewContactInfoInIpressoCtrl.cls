public with sharing class LEX_ViewContactInfoInIpressoCtrl {
	@AuraEnabled
	public static String retrieveControlPanelLink() {
		return LEXStudyUtilities.retrieveControlPanelLink();
	}

}