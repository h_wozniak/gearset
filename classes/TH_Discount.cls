/**
* @description  This class is a Trigger Handler for Training Discounts - complex logic
**/

public without sharing class TH_Discount extends TriggerHandler.DelegateBase {
    
    public Set<Id> enrollmentsToUpdateDiscountValue;


    //public override void prepareBefore() {}

    public override void prepareAfter() {
        enrollmentsToUpdateDiscountValue = new Set<Id>();
    }

    //public override void beforeInsert(List<sObject> o ) {}

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        Discount__c newDiscount;
        for (Id key : newDiscounts.keySet()) {
            newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // Trigger to update enrollment Discount_Value__c after Discount insert
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR)) {
                enrollmentsToUpdateDiscountValue.add(newDiscount.Enrollment__c);
            }
        }
    }

    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}

    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Map<Id, Discount__c> newDiscounts = (Map<Id, Discount__c>)o;
        for (Id key : newDiscounts.keySet()) {
            Discount__c oldDiscount = oldDiscounts.get(key);
            Discount__c newDiscount = newDiscounts.get(key);

            /* Wojciech Słodziak */
            // Trigger to update enrollment Discount_Value__c after Discount update
            if (newDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR) 
                && newDiscount.Discount_Value__c != oldDiscount.Discount_Value__c) {
                enrollmentsToUpdateDiscountValue.add(oldDiscount.Enrollment__c);
            }

        }       
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Discount__c> oldDiscounts = (Map<Id, Discount__c>)old;
        Discount__c oldDiscount;
        for (Id key : oldDiscounts.keySet()) {
            oldDiscount = oldDiscounts.get(key);

            /* Wojciech Słodziak */
            // Trigger to update enrollment Discount_Value__c after Discount delete
            if (oldDiscount.RecordTypeId == CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_DISCOUNT_CONNECTOR)) {
                enrollmentsToUpdateDiscountValue.add(oldDiscount.Enrollment__c);
            }
        }
    }

    public override void finish() {
        /* Wojciech Słodziak */
        // Run recalculation of Discount Value for listed Enrollment Ids
        if (enrollmentsToUpdateDiscountValue != null && !enrollmentsToUpdateDiscountValue.isEmpty()) {
            EnrollmentManager_Trainings.recalculateDiscountValue(enrollmentsToUpdateDiscountValue);
        }

    }

}