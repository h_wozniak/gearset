/**
*   @author         Sebastian Łasisz
*   @description    Helper class that is used to send new campaign members (contacts) to FCC
**/

public with sharing class IntegrationFCCContactSenderHelper {

    public static Map<String, String> reccomendedProductMap;
    public static Map<String, String> sourceOfAcquisitionMap;


    public static String prepareJSON(Set<Id> campaignIds) {
        List<Marketing_Campaign__c> mainMarketingCampaigns = [
                SELECT Id, External_campaign__r.External_Id__c, (
                        SELECT Id, Campaign_Contact__c, Campaign_Contact__r.FirstName, Campaign_Contact__r.LastName, Campaign_Contact__r.Phone, Campaign_Contact__r.MobilePhone, Campaign_Contact__r.Additional_Phones__c, Campaign_Contact__r.Email, Campaign_Contact__r.Additional_Emails__c,
                                Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__c,
                                Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__r.Classifier_ID__c,
                                Campaign_Details__r.Details_Offer_Name__c, Campaign_Details__r.Details_Offer_Trade_Name__c, Campaign_Details__r.Details_Enrollment_Id__c,
                                Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c, Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c,
                                Campaign_from_Campaign_Details_2__r.Details_Graduated__c, Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c,
                                Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c
                        FROM Campaign_Members__r
                        WHERE External_Id__c = null AND Id IN :campaignIds
                )
                FROM Marketing_Campaign__c
                WHERE Active__c = true
        ];
        sourceOfAcquisitionMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Marketing_Campaign__c.Details_Source_of_Acqiosotion__c);
        reccomendedProductMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Marketing_Campaign__c.Details_Reccomended_Product__c);

        List<FccContact_JSON> fccContactsToSend = new List<FccContact_JSON>();
        for (Marketing_Campaign__c campaign : mainMarketingCampaigns) {
            if (campaign.Campaign_Members__r.size() > 0) {
                FccContact_JSON fccContact = new FccContact_JSON();
                fccContact.fcc_campaign_id = campaign.External_campaign__r.External_Id__c;
                fccContact.contacts = new List<Contact_JSON>();

                for (Marketing_Campaign__c contact : campaign.Campaign_Members__r) {
                    Contact_JSON contact_json = new Contact_JSON();
                    contact_json.person_id = contact.Campaign_Contact__c;
                    contact_json.first_name = contact.Campaign_Contact__r.FirstName;
                    contact_json.last_name = contact.Campaign_Contact__r.LastName;
                    contact_json.offer_name = contact.Campaign_Details__r.Details_Offer_Name__c == null ? '' : contact.Campaign_Details__r.Details_Offer_Name__c;
                    contact_json.offer_trade_name = contact.Campaign_Details__r.Details_Offer_Trade_Name__c == null ? '' : contact.Campaign_Details__r.Details_Offer_Trade_Name__c;
                    contact_json.enrollment_link = contact.Campaign_Details__r.Details_Enrollment_Id__c == null ? '' : URL.getSalesforceBaseUrl().toExternalForm() + '/' + contact.Campaign_Details__r.Details_Enrollment_Id__c;

                    List<String> phonesOnContact = new List<String>();
                    if (contact.Campaign_Contact__r.Additional_Phones__c != null) {
                        String phones = contact.Campaign_Contact__r.Additional_Phones__c.replaceAll(' ', '');
                        phonesOnContact = phones.split(';');
                    }
                    if (contact.Campaign_Contact__r.Phone != null) {
                        phonesOnContact.add(contact.Campaign_Contact__r.Phone);
                    }
                    if (contact.Campaign_Contact__r.MobilePhone != null) {
                        phonesOnContact.add(contact.Campaign_Contact__r.MobilePhone);
                    }
                    contact_json.phones = phonesOnContact;

                    List<String> emailsOnContact = new List<String>();
                    if (contact.Campaign_Contact__r.Additional_Emails__c != null) {
                        String emails = contact.Campaign_Contact__r.Additional_Emails__c.replaceAll(' ', '');
                        emailsOnContact = emails.split(';');
                    }
                    if (contact.Campaign_Contact__r.Email != null) {
                        emailsOnContact.add(contact.Campaign_Contact__r.Email);
                    }

                    contact_json.emails = emailsOnContact;
                    contact_json.classifier_id = contact.Classifier_from_Campaign_Member__r.Classifier_from_External_Classifier__r.Classifier_ID__c;

                    contact_json.crm_link = URL.getSalesforceBaseUrl().toExternalForm() + '/' + contact.Campaign_Contact__c;
                    //contact_json.recommended_product = contact.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c == null ? '' : contact.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c;
                    if (contact.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c == null) {
                        contact_json.recommended_product = '';
                    } else {
                        List<String> reccProd_En = contact.Campaign_from_Campaign_Details_2__r.Details_Reccomended_Product__c.split(';');
                        List<String> reccProd_Pl = new List<String>();
                        for (String reccProd : reccProd_En) {

                            reccProd_Pl.add(reccomendedProductMap.get(reccProd));
                        }
                        contact_json.recommended_product = String.join(reccProd_Pl, '; ');
                    }
                    contact_json.source_of_acquisition = contact.Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c == null ? '' : sourceOfAcquisitionMap.get(contact.Campaign_from_Campaign_Details_2__r.Details_Source_of_Acqiosotion__c);
                    contact_json.date_of_acquisition = contact.Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c == null ? '' : String.valueOf(contact.Campaign_from_Campaign_Details_2__r.Details_Date_of_Acqiosotion__c);
                    contact_json.graduated = contact.Campaign_from_Campaign_Details_2__r.Details_Graduated__c ? 'Tak' : 'Nie';
                    contact_json.graduated_product_name = contact.Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c == null ? '' : contact.Campaign_from_Campaign_Details_2__r.Details_Graduated_Product__c;


                    fccContact.contacts.add(contact_json);
                }
                fccContactsToSend.add(fccContact);
            }
        }

        return JSON.serialize(fccContactsToSend);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class FccContact_JSON {
        public String fcc_campaign_id;
        public List<Contact_JSON> contacts;
    }

    public class Contact_JSON {
        public String person_id;
        public String first_name;
        public String last_name;
        public List<String> phones;
        public List<String> emails;
        public String classifier_id;
        public String enrollment_link;
        public String offer_name;
        public String offer_trade_name;
        public String crm_link;
        public String recommended_product;
        public String source_of_acquisition;
        public String date_of_acquisition;
        public String graduated;
        public String graduated_product_name;
    }
}