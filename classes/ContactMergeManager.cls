/**
* @author       Wojciech Słodziak
* @description  Class supporting merging process for contacts and for performing custom merge.
**/

public without sharing class ContactMergeManager {

    public static final String SEPERATOR_MULTISELECTPICKLIST = ';';
    public static final String SEPARATOR_TEXTFIELD_TAGS = '; ';
    public static final String SEPARATOR_TEXTFIELD_UNIVERSITY = ', ';
    public static final String SEPARATOR_TEXTFIELD_OTHERPHONES = '; ';

    // variable informing if current transaction is caused by a merge
    public static Boolean isMergeTransaction = false;

    // variable preventing multiple duplicate checks caused by merge
    public static Boolean duplicatesCheckedByMerge = false;

    // variable informing if transaction is caused by Contact Email Link Verification, which enables automatic merging
    public static Boolean isVerificationActionTransaction = false;

    // fields that are processed by a merge
    private final static Set<Schema.SObjectField> fieldsToMerge = new Set<Schema.SObjectField> {
        Contact.AccountId, Contact.Birthdate, Contact.Department, Contact.Description, Contact.Email, Contact.HomePhone, Contact.Creation_Source__c,
        Contact.MailingStreet, Contact.MailingCity, Contact.MailingState, Contact.MailingPostalCode, Contact.MailingCountry,
        Contact.OtherStreet, Contact.OtherCity, Contact.OtherState, Contact.OtherPostalCode, Contact.OtherCountry, 
        Contact.MobilePhone, Contact.FirstName, Contact.LastName, Contact.OtherPhone, Contact.Phone, Contact.Lead_Scoring__c,
        Contact.Additional_Emails__c, Contact.Additional_Phones__c, Contact.A_Level__c, Contact.A_Level_Id__c, Contact.A_Level_Type__c, Contact.Attendance__c, 
        Contact.Bank_Account_No__c, Contact.Candidate__c, Contact.Confirmed_Contact__c, Contact.Consent_Storage_Location__c, 
        Contact.Contact_from_Reject__c, Contact.Contact_Person__c, Contact.Country_of_Birth__c, Contact.Country_of_Issue__c, Contact.Country_of_Origin__c, 
        Contact.Date_Electronic_Communication__c, Contact.Date_Marketing__c, Contact.Date_Personal_Data_Affiliated__c, Contact.Date_Personal_Data_Processing__c, 
        Contact.Date_Refusal_on_Electronic_Comm__c, Contact.Date_Refusal_on_Marketing__c, Contact.Date_Refusal_on_Pers_Data_Affiliated__c, 
        Contact.Date_Refusal_on_Personal_Data_Processing__c, Contact.Department_Business_Area__c, Contact.Do_Not_Call_Reason__c, Contact.Electronic_Communication__c, 
        Contact.Electronic_Communication_Consent_Source__c, Contact.Enrollment_Source__c, Contact.Exam_Pass__c, Contact.Expiration_Date_Authorization_Doc__c, 
        Contact.Expiration_Date_Polish_Card_Document__c, Contact.Extranet_User__c, Contact.Family_Name__c, Contact.Father_Name__c, Contact.Foreigner__c, 
        Contact.Function__c, Contact.Gender__c, Contact.Graduation_Status__c, Contact.A_Level_Issue_Date__c, 
        Contact.Group_for_Enroll__c, Contact.Health_Insurance__c, Contact.Health_Insurance_Expiration_Date__c, Contact.Identity_Document__c, Contact.ID_Unique__c, 
        Contact.Industry__c, Contact.IntegrationAddressType__c, Contact.IntegrationPhoneType__c,
        Contact.Lead_Prospect__c, Contact.Locality_Type__c, Contact.Marketing__c, Contact.Marketing_Consent_Source__c, Contact.Middle_Name__c, Contact.Mother_Name__c, 
        Contact.Nationality__c, Contact.Number_and_Series__c, Contact.Contact__c, Contact.Personal_Data_Affiliated__c, Contact.Personal_Data_Affiliated_Source__c, 
        Contact.Personal_Data_Processing__c, Contact.Personal_Data_Processing_Consent_Source__c, Contact.Pesel__c, Contact.Pesel_Not_Acquired__c, Contact.Place_of_Birth__c, 
        Contact.Polish_Card_Document__c, Contact.Position__c, Contact.Rating__c, Contact.Regional_Examination_Commission__c, Contact.Tags__c,
        Contact.School__c, Contact.School_Certificate__c, Contact.School_Certificate_Country__c, Contact.School_Certificate_Date__c, Contact.School_Certificate_ID__c, 
        Contact.School_Certificate_Town__c, Contact.School_Certificate_With_Honors__c, Contact.School_Graduation_Date__c, Contact.Year_of_Graduation__c,
        Contact.School_Name__c, Contact.School_Town__c, Contact.School_Type__c, Contact.Source__c, Contact.Statuses__c, Contact.Student__c, 
        Contact.Study_Authorization_Document__c, Contact.System_Updating_Contact__c, Contact.TEB_Graduate__c, Contact.The_Same_Mailling_Address__c, 
        Contact.Thesis_Date__c, Contact.University_for_sharing__c, Contact.WKU__c, Contact.WKU_Place__c, Contact.Status__c, Contact.Other_Phones__c
    };

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- MERGING METHOD ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    // method for automatically and programmatically merging Contacts
    // map structure: merged contact Id -> master contact Id 
    // Warning: DML in Loop - this case should not happen because trigger is called for single Contact
    public static String mergeContactsFromButton(Map<Id, Id> contactIdMapToMerge) {
        DuplicateManager.skipDeduplicationCheck = true;
        String result;

        Set<Id> contactIds = contactIdMapToMerge.keySet().clone();
        contactIds.addAll(new Set<Id>(contactIdMapToMerge.values()));

        Map<Id, Contact> contactMap = new Map<Id, Contact>([
            SELECT Id,
            AccountId, Birthdate, Department, Description, Email, HomePhone, Lead_Scoring__c, Creation_Source__c,
            MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MobilePhone, Requested_application_for_Stay_Card__c,
            OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, Recognition__c,
            FirstName, LastName, OtherPhone, Phone, Year_of_Graduation__c, Other_Phones__c,
            Additional_Emails__c, Additional_Phones__c, A_Level__c, A_Level_Id__c, A_Level_Type__c, Attendance__c, 
            Bank_Account_No__c, Candidate__c, Confirmed_Contact__c, Consent_Storage_Location__c, 
            Contact_from_Reject__c, Contact_Person__c, Country_of_Birth__c, Country_of_Issue__c, Country_of_Origin__c, 
            Date_Electronic_Communication__c, Date_Marketing__c, Date_Personal_Data_Affiliated__c, Date_Personal_Data_Processing__c, 
            Date_Refusal_on_Electronic_Comm__c, Date_Refusal_on_Marketing__c, Date_Refusal_on_Pers_Data_Affiliated__c, 
            Date_Refusal_on_Personal_Data_Processing__c, Department_Business_Area__c, Do_Not_Call_Reason__c, Electronic_Communication__c, 
            Electronic_Communication_Consent_Source__c, Enrollment_Source__c, Exam_Pass__c, Expiration_Date_Authorization_Doc__c, 
            Expiration_Date_Polish_Card_Document__c, Extranet_User__c, Family_Name__c, Father_Name__c, Foreigner__c, 
            Foreigners_PriceBook__c, Function__c, Gender__c, Graduate__c, Graduation_Status__c, A_Level_Issue_Date__c, 
            Group_for_Enroll__c, Health_Insurance__c, Health_Insurance_Expiration_Date__c, Identity_Document__c, ID_Unique__c, 
            Industry__c, IntegrationAddressType__c, IntegrationPhoneType__c, Tags__c,
            Lead_Prospect__c, Locality_Type__c, Marketing__c, Marketing_Consent_Source__c, Middle_Name__c, Mother_Name__c, 
            Nationality__c, Number_and_Series__c, Contact__c, Personal_Data_Affiliated__c, Personal_Data_Affiliated_Source__c, 
            Personal_Data_Processing__c, Personal_Data_Processing_Consent_Source__c, Pesel__c, Pesel_Not_Acquired__c, Place_of_Birth__c, 
            Polish_Card_Document__c, Position__c, Rating__c, Regional_Examination_Commission__c, 
            School__c, School_Certificate__c, School_Certificate_Country__c, School_Certificate_Date__c, School_Certificate_ID__c, 
            School_Certificate_Town__c, School_Certificate_With_Honors__c, School_Graduation_Date__c, 
            School_Name__c, School_Town__c, School_Type__c, Source__c, Statuses__c, Student__c, 
            Study_Authorization_Document__c, System_Updating_Contact__c, TEB_Graduate__c, The_Same_Mailling_Address__c, 
            Thesis_Date__c, University_for_sharing__c, WKU__c, WKU_Place__c, Status__c,
            Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Direct_Communications_ADO__c, Consent_Graduates_ADO__c, Consent_PUCA_ADO__c, Areas_of_Interest__c
            FROM Contact
            WHERE Id IN :contactIds
        ]);

        try {
            for (Id key : contactIdMapToMerge.keySet()) {
                Id masterId = contactIdMapToMerge.get(key);
                Contact masterContact = ContactMergeManager.getContactWithMergedFields(contactMap.get(masterId), contactMap.get(key));
                CommonUtility.allowDeletingIPressoContact = true;

                masterContact.Is_Potential_Duplicate__c = false;
                masterContact.Potential_Duplicate__c = null;

                /* if deduplication is caused from integration, send update to other contacts anyways */
                CommonUtility.preventSendingDataToExperia = false;
                CommonUtility.preventSendingDataToZPI = false;
                CommonUtility.preventSendingDataToIPresso = false;
                CommonUtility.preventSendingDataToFCC = false;

                merge masterContact contactMap.get(key);
            }
        } catch (Exception e) {
            ErrorLogger.log(e);
            result = CommonUtility.trimErrorMessage(e.getMessage());
        }

        return result;
    }

    // method for automatically and programmatically merging Contacts
    // map structure: merged contact Id -> master contact Id 
    // Warning: DML in Loop - this case should not happen because trigger is called for single Contact
    public static String mergeContacts(Map<Id, Id> contactIdMapToMerge) {
        DuplicateManager.skipDeduplicationCheck = true;
        String result;

        Set<Id> contactIds = contactIdMapToMerge.keySet().clone();
        contactIds.addAll(new Set<Id>(contactIdMapToMerge.values()));

        Map<Id, Contact> contactMap = new Map<Id, Contact>([
            SELECT Id,
            AccountId, Birthdate, Department, Description, Email, HomePhone, Lead_Scoring__c,
            MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MobilePhone, Recognition__c,
            OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, Requested_application_for_Stay_Card__c,
            FirstName, LastName, OtherPhone, Phone, Other_Phones__c, Tags__c,
            Additional_Emails__c, Additional_Phones__c, A_Level__c, A_Level_Id__c, A_Level_Type__c, Attendance__c, 
            Bank_Account_No__c, Candidate__c, Confirmed_Contact__c, Consent_Storage_Location__c, 
            Contact_from_Reject__c, Contact_Person__c, Country_of_Birth__c, Country_of_Issue__c, Country_of_Origin__c, 
            Date_Electronic_Communication__c, Date_Marketing__c, Date_Personal_Data_Affiliated__c, Date_Personal_Data_Processing__c, 
            Date_Refusal_on_Electronic_Comm__c, Date_Refusal_on_Marketing__c, Date_Refusal_on_Pers_Data_Affiliated__c, 
            Date_Refusal_on_Personal_Data_Processing__c, Department_Business_Area__c, Do_Not_Call_Reason__c, Electronic_Communication__c, 
            Electronic_Communication_Consent_Source__c, Enrollment_Source__c, Exam_Pass__c, Expiration_Date_Authorization_Doc__c, 
            Expiration_Date_Polish_Card_Document__c, Extranet_User__c, Family_Name__c, Father_Name__c, Foreigner__c, 
            Foreigners_PriceBook__c, Function__c, Gender__c, Graduate__c, Graduation_Status__c, A_Level_Issue_Date__c, 
            Group_for_Enroll__c, Health_Insurance__c, Health_Insurance_Expiration_Date__c, Identity_Document__c, ID_Unique__c, 
            Industry__c, IntegrationAddressType__c, IntegrationPhoneType__c,  Year_of_Graduation__c,
            Lead_Prospect__c, Locality_Type__c, Marketing__c, Marketing_Consent_Source__c, Middle_Name__c, Mother_Name__c, 
            Nationality__c, Number_and_Series__c, Contact__c, Personal_Data_Affiliated__c, Personal_Data_Affiliated_Source__c, 
            Personal_Data_Processing__c, Personal_Data_Processing_Consent_Source__c, Pesel__c, Pesel_Not_Acquired__c, Place_of_Birth__c, 
            Polish_Card_Document__c, Position__c, Rating__c, Regional_Examination_Commission__c, 
            School__c, School_Certificate__c, School_Certificate_Country__c, School_Certificate_Date__c, School_Certificate_ID__c, 
            School_Certificate_Town__c, School_Certificate_With_Honors__c, School_Graduation_Date__c, Creation_Source__c,
            School_Name__c, School_Town__c, School_Type__c, Source__c, Statuses__c, Student__c, 
            Study_Authorization_Document__c, System_Updating_Contact__c, TEB_Graduate__c, The_Same_Mailling_Address__c, 
            Thesis_Date__c, University_for_sharing__c, WKU__c, WKU_Place__c, Status__c,
            Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Direct_Communications_ADO__c, Consent_Graduates_ADO__c, Consent_PUCA_ADO__c, Areas_of_Interest__c
            FROM Contact
            WHERE Id IN :contactIds
        ]);

        // Needed query to retrieve created date. Cant merge two records when created date is queried.
        Map<Id, Contact> contactMapWithCreatedDate = new Map<Id, Contact>([
            SELECT Id, Creation_Source__c, CreatedDate
            FROM Contact
            WHERE Id IN :contactIds
        ]);

        try {
            for (Id key : contactIdMapToMerge.keySet()) {
                Id masterId = contactIdMapToMerge.get(key);                

                CommonUtility.allowDeletingIPressoContact = true;
                if (determineMasterContact(contactMapWithCreatedDate.get(masterId), contactMapWithCreatedDate.get(key))) {
                    Contact masterContact = ContactMergeManager.getContactWithMergedFields(contactMap.get(key), contactMap.get(masterId));

                    masterContact.Is_Potential_Duplicate__c = false;
                    masterContact.Potential_Duplicate__c = null;

                    /* if deduplication is caused from integration, send update to other contacts anyways */
                    CommonUtility.preventSendingDataToExperia = false;
                    CommonUtility.preventSendingDataToZPI = false;
                    CommonUtility.preventSendingDataToIPresso = false;
                    CommonUtility.preventSendingDataToFCC = false;

                    merge masterContact contactMap.get(masterId);
                }
                else {
                    Contact masterContact = ContactMergeManager.getContactWithMergedFields(contactMap.get(masterId), contactMap.get(key));

                    masterContact.Is_Potential_Duplicate__c = false;
                    masterContact.Potential_Duplicate__c = null;

                    /* if deduplication is caused from integration, send update to other contacts anyways */
                    CommonUtility.preventSendingDataToExperia = false;
                    CommonUtility.preventSendingDataToZPI = false;
                    CommonUtility.preventSendingDataToIPresso = false;
                    CommonUtility.preventSendingDataToFCC = false;

                    merge masterContact contactMap.get(key);
                }
            }
        } catch (Exception e) {
            ErrorLogger.log(e);
            result = CommonUtility.trimErrorMessage(e.getMessage());
        }
        
        return result;
    }



    private static Contact getContactWithMergedFields(Contact masterContact, Contact mergedContact) {
        // custom merge for University_for_sharing__c
        if (mergedContact != null && masterContact != null) {
            mergedContact.University_for_sharing__c = prepareValues(mergedContact.University_for_sharing__c, masterContact.University_for_sharing__c, SEPARATOR_TEXTFIELD_UNIVERSITY);

            /* Other Phones */
            mergedContact.Other_Phones__c = prepareValues(mergedContact.Other_Phones__c, masterContact.Other_Phones__c, SEPARATOR_TEXTFIELD_OTHERPHONES);

            /* Tags */
            mergedContact.Tags__c = prepareValues(mergedContact.Tags__c, masterContact.Tags__c, SEPARATOR_TEXTFIELD_TAGS);

            /* Prepare Consents */
            masterContact.Consent_Marketing_ADO__c = prepareValues(masterContact.Consent_Marketing_ADO__c, mergedContact.Consent_Marketing_ADO__c, SEPERATOR_MULTISELECTPICKLIST);
            masterContact.Consent_Electronic_Communication_ADO__c = prepareValues(masterContact.Consent_Electronic_Communication_ADO__c, mergedContact.Consent_Electronic_Communication_ADO__c, SEPERATOR_MULTISELECTPICKLIST);
            masterContact.Consent_Direct_Communications_ADO__c = prepareValues(masterContact.Consent_Direct_Communications_ADO__c, mergedContact.Consent_Direct_Communications_ADO__c, SEPERATOR_MULTISELECTPICKLIST);
            masterContact.Consent_Graduates_ADO__c = prepareValues(masterContact.Consent_Graduates_ADO__c, mergedContact.Consent_Graduates_ADO__c, SEPERATOR_MULTISELECTPICKLIST);
            masterContact.Consent_PUCA_ADO__c = prepareValues(masterContact.Consent_PUCA_ADO__c, mergedContact.Consent_PUCA_ADO__c, SEPERATOR_MULTISELECTPICKLIST);

            /* Graduate - Triggers for child objects are not fired */
            masterContact.Graduate__c = (mergedContact.Graduate__c || masterContact.Graduate__c);
            masterContact.Foreigners_PriceBook__c = (mergedContact.Foreigners_PriceBook__c || masterContact.Foreigners_PriceBook__c);
            masterContact.Requested_application_for_Stay_Card__c = (mergedContact.Requested_application_for_Stay_Card__c || masterContact.Requested_application_for_Stay_Card__c);
            masterContact.Recognition__c = (mergedContact.Recognition__c || masterContact.Recognition__c);

            /* Areas of Interest */
            masterContact.Areas_of_Interest__c = prepareValues(mergedContact.Areas_of_Interest__c, masterContact.Areas_of_Interest__c, SEPERATOR_MULTISELECTPICKLIST);

            mergedContact.Is_Potential_Duplicate__c = false;
            mergedContact.Potential_Duplicate__c = null;

            // merge all other fields
            for (Schema.SObjectField field : ContactMergeManager.fieldsToMerge) {
                if (mergedContact.get(field) != null) {
                    masterContact.put(field, mergedContact.get(field));
                }
            }
        }

        return masterContact;
    }

    /**
     * Helper method for getContactWithMergedFields responsible for merging field value from both records.
     * @author Sebastian Lasisz
     *
     * @param masterFieldValue Field value from master record.
     * @param mergedFieldValue Field value from merged record.
     * @param separator Separator that's used to seperate values in field.
     *
     * @return Merged field value that's logical disjunction of both field values.
     */
    public static String prepareValues(String masterFieldValue, String mergedFieldValue, String separator) {
        Set<String> splitMasterFieldValue = masterFieldValue != null ? getOptionsAsSet(masterFieldValue, separator) : new Set<String>();
        Set<String> splitMergedFieldValue = mergedFieldValue != null ? getOptionsAsSet(mergedFieldValue, separator) : new Set<String>();

        Set<String> finalSetOfConsents = new Set<String>();
        finalSetOfConsents.addAll(splitMasterFieldValue);
        finalSetOfConsents.addAll(splitMergedFieldValue);

        return getSetAsStringOptions(finalSetOfConsents, separator);
    }

    /**
     * Helper method for prepareValues responsible for building set of strings based on field value and seperator.
     * @author Sebastian Lasisz
     *
     * @param fieldValue Field value taken from record.
     * @param separator String value describing with which value fieldValue should be split.
     *
     * @return Converted set of fieldvalues that's split by seperator.
     */
    public static Set<String> getOptionsAsSet(String fieldValue, String separator) {
        if (fieldValue == null) {
            return new Set<String>();
        }
        return new Set<String>(fieldValue.split(separator));
    }

    /**
     * Helper method for prepareValues responsible for building String field value for given value set and separator
     * @author Sebastian Lasisz
     *
     * @param valueSet Set of field values that needs to be merged into string.
     * @param separator String value describing with which value Set value should be merged.
     *
     * @return String containing all values from valueSet separated by given separator.
     */
    public static String getSetAsStringOptions(Set<String> valueSet, String separator) {
        if (valueSet == null || valueSet.isEmpty()) {
            return null;
        }
        String result = '';

        for (String val : valueSet) {
            result += val + separator;
        }
        result = result.substring(0, result.length() - separator.length());

        return result;
    }

    /* Sebastian Łasisz */
    // Determine master contact for deduplication process based on creation source and created date.
    public static Boolean determineMasterContact(Contact potentialMasterContact, Contact potentialMergedContact) {
        Integer comparedContacts = compareContactSource(potentialMasterContact, potentialMergedContact);

        if (comparedContacts == 0) {
            return potentialMasterContact.CreatedDate <= potentialMergedContact.CreatedDate;
        }
        else {
            return comparedContacts > 0;
        }
    }

    /* Sebastian Łasisz */
    // Compare creation source for two potential duplicates, based on priorities decide whether statuses are (not)equal.
    public static Integer compareContactSource(Contact potentialMasterContact, Contact potentialMergedContact) {
        if (potentialMasterContact.Creation_Source__c == potentialMergedContact.Creation_Source__c
            || (potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_CRM && potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI)
            || (potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI && potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_CRM)
            || (potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO && potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD)
            || (potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD && potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO)
        ) {
            return 0;
        }
        else {
            if (((potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_CRM || potentialMasterContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI) 
                && (potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO || potentialMergedContact.Creation_Source__c == CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD))
                || (potentialMasterContact.Creation_Source__c != null && potentialMergedContact.Creation_Source__c == null)
            ) {
                return 1;
            }

            return -1;
        }
    }
}