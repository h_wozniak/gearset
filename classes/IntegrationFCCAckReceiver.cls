/**
*   @author         Sebastian Łasisz
*   @description    Rest Resource class for receiving acknowledgements for Marketing Campaign transfers from FCC.
**/

@RestResource(urlMapping = '/fcc-services/update-records')
global without sharing class IntegrationFCCAckReceiver {

    @HttpPut
    global static void receiveFCCInformation() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonBody = req.requestBody.toString();
        
        List<FCCAckReceiver_JSON> parsedJson;

        try {
            parsedJson = parse(jsonBody);
        } catch(Exception ex) {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (parsedJson != null) {
            try {
                Set<Id> contactIds = new Set<Id>();
                Set<String> externalCampaignIds = new Set<String>();
                for (FCCAckReceiver_JSON fccAck : parsedJson) {
                    for (RecordIds_JSON record_id : fccAck.contacts) {
                        contactIds.add(record_id.crm_contact_id);
                    }             
                    externalCampaignIds.add(fccAck.ext_campaign_id);       
                }

                System.debug('externalCampaignIds ' + externalCampaignIds);
                System.debug('contactIds ' + contactIds);

                List<Marketing_Campaign__c> campaignsToUpdate = [
                    SELECT Id, External_Id__c, Campaign_Member__r.External_campaign__r.External_Id__c, Campaign_Contact__c
                    FROM Marketing_Campaign__c
                    WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
                    AND Campaign_Member__r.External_campaign__r.External_Id__c  IN :externalCampaignIds
                    AND External_Id__c = null
                    AND Campaign_Contact__c IN :contactIds
                ];

                System.debug(campaignsToUpdate);

                for (Marketing_Campaign__c campaignToUpdate : campaignsToUpdate) {
                    for (FCCAckReceiver_JSON fccAck : parsedJson) {
                        if (fccAck.ext_campaign_id == campaignToUpdate.Campaign_Member__r.External_campaign__r.External_Id__c) {
                            for (RecordIds_JSON record_id : fccAck.contacts) {
                                if (record_id.crm_contact_id == campaignToUpdate.Campaign_Contact__c) {
                                    campaignToUpdate.External_Id__c = record_id.ext_contact_id;
                                }
                            }
                        }
                    }
                }

                update campaignsToUpdate;

                res.statusCode = 200;
            } catch (Exception e) {
                ErrorLogger.log(e);
                res.statusCode = 500;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
            }
        }
    }
    
    private static List<FCCAckReceiver_JSON> parse(String jsonBody) {
        return (List<FCCAckReceiver_JSON>) System.JSON.deserialize(jsonBody, List<FCCAckReceiver_JSON>.class);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class FCCAckReceiver_JSON {
        public List<RecordIds_JSON> contacts;
        public String campaign_id;
        public String ext_campaign_id;
    }

    public class RecordIds_JSON {
        public String ext_contact_id;
        public String crm_contact_id;
    }
}