/**
* @author       Przemysław Tustanowski, editor Wojciech Słodziak
* @description  This class is a controller for NewEnrollment.page, wchich is a page which decide with which wizard connect
**/
public with sharing class NewEnrollmentController {
    
    public List<SelectOption> rtOptions { get; set; }
    public String chosenRT { get; set; }
    
    public Id candidateId { get; set; }
    public Id companyId { get; set; }

    private String thisUrl;

    public NewEnrollmentController(ApexPages.StandardController controller) {
        thisUrl = ApexPages.currentPage().getUrl();

        candidateId = ApexPages.currentPage().getParameters().get('candidate');
        companyId = ApexPages.currentPage().getParameters().get('company');

        rtOptions = getAvailableRecordTypeOptions();

        if (chosenRT == null && !rtOptions.isEmpty()) {
            chosenRT = rtOptions[0].getValue();
        }
    }

    private List<SelectOption> getAvailableRecordTypeOptions() {
        List<SelectOption> rtToReturn = new List<SelectOption>();
        Set<Id> availableForUserRTIds = CommonUtility.getAvailableRecordTypeIdsForSObject(Schema.SObjectType.Enrollment__c);

        Id openTrainingEnrRTId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING);
        if (availableForUserRTIds.contains(openTrainingEnrRTId)) {
            rtToReturn.add(new SelectOption(openTrainingEnrRTId, Label.title_OpenTraining));
        }


        if (candidateId == null) {
            Id closedTrainingRTId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING);
            if (availableForUserRTIds.contains(closedTrainingRTId)) {
                rtToReturn.add(new SelectOption(closedTrainingRTId, Label.title_CloseTraining));
            }
        }
        
        if (companyId == null) {
            Id studyEnrRTId = CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY);
            if (availableForUserRTIds.contains(studyEnrRTId)) {
                rtToReturn.add(new SelectOption(studyEnrRTId, Label.title_Study));
            }
        }

        return rtToReturn;
    }

    public PageReference next() {
        if (chosenRT == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)) {
            return new PageReference('/apex/StudySelection?retURL=' + thisUrl + (candidateId != null? '&candidateId=' + candidateId : '') );
        } else {
            if (candidateId != null) {
                return new PageReference('/apex/ScheduleSelection?enrollmentRT=' + chosenRT + (candidateId != null? '&candidateId=' + candidateId : '') + '&retURL=' + thisUrl);
            }
            else{
                return new PageReference('/apex/ScheduleSelection?enrollmentRT=' + chosenRT + (companyId!= null? '&companyId=' + companyId: '') + '&retURL=' + thisUrl);
            }
        }
    }

    public PageReference back() {
        if (candidateId != null) {
            return new PageReference('/' + candidateId);
        }
        else if (companyId!= null) {
            return new PageReference('/' + companyId);
        }
        else{
            return new PageReference('/' + CommonUtility.getPrefix('Enrollment__c'));
        }
    }
}