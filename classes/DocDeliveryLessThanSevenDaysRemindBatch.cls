/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Study Enrollment confirmation reminder
**/

global class DocDeliveryLessThanSevenDaysRemindBatch implements Database.Batchable<sObject> {
    
    //Datetime dayOfEnrollmentFrom;
    //Datetime dayOfEnrollmentTo;
    Date dayOfDeadline;
    Integer docListCounter;

    global DocDeliveryLessThanSevenDaysRemindBatch(Integer daysAfterEnrollment, Integer docCounter) {
        //Date dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
        /*
        Changing newInstanceGMT to newInstance for performing operations on the same time zone locale. For instance if there was enrollment on 02.01 00:30 SF would interprate it as
        01.01 22:30, which would screw sending emails (they would be send too early). So instead of looking for enrollments between 02.01 00:00 - 02.01 23:59 we look for
        01.01 22:00 to 02.01 21:59. In theory it should filter this records that are supposed to go (as going in -2h in SF own locale). 
         */
        //dayOfEnrollmentFrom = Datetime.newInstance(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 0, 0, 0);
        //dayOfEnrollmentTo = Datetime.newInstance(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 23, 59, 59);
        dayOfDeadline = System.today().addDays(daysAfterEnrollment);
        docListCounter = docCounter;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id
            FROM Enrollment__c 
            WHERE Documents_Delivered__c = false
            AND Remaining_Days_For_Document_Delivery__c <= 7
            //AND (
            //    (VerificationDate__c = null AND Enrollment_Date__c >= :dayOfEnrollmentFrom AND Enrollment_Date__c <= :dayOfEnrollmentTo)
            //    OR (VerificationDate__c != null AND VerificationDate__c >= :dayOfEnrollmentFrom AND VerificationDate__c <= :dayOfEnrollmentTo)
            //)
            AND Delivery_Deadline__c = :dayOfDeadline
            AND Unenrolled__c = false
            AND RequiredDocListEmailedCount__c < 3
            AND (Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
            OR Status__c = :CommonUtility.ENROLLMENT_STATUS_COD)
            AND Dont_send_automatic_emails__c = false
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrIds = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrIds.add(enr.Id);
        }

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrIds);
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}