@isTest
private class ZTestIntegrationFCCContactSender {

	@isTest static void test_sendNewFCCRecordsAsync_fail() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Failed());

		Test.startTest();
		IntegrationFCCContactSender.sendNewFCCRecordsAsync(null);
		Test.stopTest();
	}

	@isTest static void test_sendNewFCCRecordsAsync_null() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

		Test.startTest();
		IntegrationFCCContactSender.sendNewFCCRecordsAsync(null);
		Test.stopTest();
	}

	@isTest static void test_sendNewFCCRecordsAsync_emptySet() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

		Test.startTest();
		IntegrationFCCContactSender.sendNewFCCRecordsAsync(new Set<Id>());
		Test.stopTest();
	}

	@isTest static void test_sendNewFCCRecordsAsync() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(new Marketing_Campaign__c(Active__c = true), true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
        	new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
        ), true);

		Test.startTest();
		IntegrationFCCContactSender.sendNewFCCRecordsAsync(new Set<Id> {member.Id});
		Test.stopTest();
	}

	@isTest static void test_sendNewFCCRecordsSync() {
		CustomSettingDefault.initEsbConfig();

		Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.RemoveContactsCalloutMock_Success());

        Marketing_Campaign__c marketing = ZDataTestUtility.createMarketingCampaign(new Marketing_Campaign__c(Active__c = true), true);
        Marketing_Campaign__c classifier = ZDataTestUtility.createClassifier(new Marketing_Campaign__c(Campaign_from_Classifier__c = marketing.Id), true);

        Marketing_Campaign__c member = ZDataTestUtility.createCampaignMember(
        	new Marketing_Campaign__c(Campaign_Member__c = marketing.Id, Classifier_from_Campaign_Member__c = classifier.Id
        ), true);

		Test.startTest();
		Boolean result = IntegrationFCCContactSender.sendNewFCCRecordsSync(new Set<Id> {member.Id});
		Test.stopTest();

		System.assertEquals(result, true);
	}
}