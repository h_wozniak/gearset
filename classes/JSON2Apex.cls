public with sharing class JSON2Apex {

//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

    public String trainingCode;
    public String type;
    public Boolean attendeeType;
    public List<Attendees> attendees;
    public Boolean personalDataProcessingAgreement;
    public Boolean commercialInfoAgreement;
    public Boolean contactAgreement;
    public String source;
    public String companyName;
    public String companyStreet;
    public String companyPostalCode;
    public String companyCity;
    public String companySize;
    public String voucher;
    public String supportId;
    public String nip;
    public String businessArea;
    public String contactPersonFirstName;
    public String contactPersonLastName;
    public String contactPersonPosition;
    public String contactPersonDepartment;
    public String contactPersonPhone;
    public String contactPersonEmail;
    public Boolean sameAsContact;
    public Boolean invoiceAfterPayment;
    public String invoiceDeliveryChannel;
    public Boolean invoiceProforma;
    public String invoiceNip;
    public String invoiceCompanyName;
    public String invoiceDescription;
    public String invoiceFirstName;
    public String invoiceLastName;
    public String invoiceCity;
    public String invoicePostalCode;
    public String invoiceStreet;
    public String invoiceCountry;
    public String cc;
    public String futureTrainingInfo;

    public class Attendees {
        public String firstName;
        public String lastName;
        public Boolean attendeeType;
        public String phone;
        public String email;
        public String position;
        public String department;
        public String pesel;
        public String mailingStreet;
        public String mailingCity;
        public String mailingState;
        public String mailingPostalCode;
        public String mailingCountry;
    }

    
    public static JSON2Apex parse(String json) {
        return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
    }

     public String createDuplicateStringInfoForRecruitment(){
        return  'Kod szkolenia= ' + trainingCode + '\n' + '\n' +
                //
                'Rodzaj kontaktu: ' + '\n' +
                'Typ kandydata= ' + type + '\n' + '\n' +
                //
                'Dane firmy/instytucji:' + '\n' + 
                'Nazwa firmy= ' + companyName + '\n' +
                'Adres firmy, ulica= ' + companyStreet + '\n' +
                'Adres firmy, kod pocztowy= ' + companyPostalCode + '\n' +
                'Adres firmy, miasto= ' + companyCity + '\n' +
                'Wielkość firmy= ' + companySize + '\n' +
                'NIP= ' + nip + '\n' +
                'Obszar działalności firmy= ' + businessArea + '\n' +
                'Imię osoby kontaktowej= ' + contactPersonFirstName + '\n' +
                'Nazwisko osoby kontaktowej= ' + contactPersonLastName + '\n' +
                'Stanowisko osoby kontaktowej= ' + contactPersonPosition + '\n' +
                'Departament osoby kontaktowej= ' + contactPersonDepartment + '\n' +
                'Telefon osoby kontaktowej= ' + contactPersonPhone + '\n' +
                'Adres email osoby kontaktowej= ' + contactPersonEmail + '\n' +
                'supportId= ' + supportId + '\n' + '\n' +
                
                'Dane do faktury:' + '\n' +
                'Proszę o wystawienie faktury proforma: invoiceProforma= ' + invoiceProforma + '\n' +
                'Dane do faktury zgodne z wyżej podanymi danymi adresowym: sameAsContact= ' + sameAsContact + '\n' +
                'Proszę o wystawienie faktury po dokonaniu płatności: invoiceAfterPayment= ' + invoiceAfterPayment + '\n' +
                'Forma dostarczenia faktury: invoiceDeliveryChannel= ' + invoiceDeliveryChannel + '\n' +
                'NIP: invoiceNip= ' + invoiceNip + '\n' +
                'Nazwa firmy: invoiceCompanyName= ' + invoiceCompanyName + '\n' +
                'Uwagi do faktury: invoiceDescription= ' + invoiceDescription + '\n' +
                'Imię: invoiceFirstName= ' + invoiceFirstName + '\n' +
                'Nazwisko: invoiceLastName= ' + invoiceLastName + '\n' +
                'Miasto: invoiceCity= ' + invoiceCity + '\n' +
                'Kod pocztowy: invoicePostalCode= ' + invoicePostalCode + '\n' +
                'Ulica/nr: invoiceStreet= ' + invoiceStreet + '\n' +
                'Kraj: invoiceCountry= ' + invoiceCountry + '\n' + '\n' +
                //'cc=' + cc + '\n' +
                'Informacje:' + '\n' +
                'W przyszłości jestem zainteresowany szkoleniami z obszarów: futureTrainingInfo= ' + futureTrainingInfo + '\n' +
                'Źródło=' + source + '\n' +
                'Zgoda na przetwarzanie danych osobowych: personalDataProcessingAgreement= ' + personalDataProcessingAgreement + '\n' +
                'Zgoda na materiały promocyjne: commercialInfoAgreement= ' + commercialInfoAgreement + '\n' +
                'Zgoda na kontakt: contactAgreement= ' + contactAgreement;
    }

}