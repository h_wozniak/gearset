@isTest
global class ZTestIntegrationProductsHelper {

    @isTest static void test_prepareTuitionSystemAndInstallmentsOnOffer() {
    	Map<Integer, sObject> mapData = ZDataTestUtilityMethods.IPH_prepareDataForTest_Statistics(true);

    	PriceBookManager.activatePriceBooks(new List<Id> { mapData.get(5).Id, mapData.get(6).Id });

    	IntegrationProductsHelper.CourseOffer_JSON courseOfferToParse = new IntegrationProductsHelper.CourseOffer_JSON();

    	List<Offer__c> offerWithPriceBook = [
            SELECT Installment_Variants__c, Price_Book_Type__c, Graded_Tuition__c, Onetime_Price__c
            FROM Offer__c 
            WHERE Currently_in_use__c = true AND Price_Book_Type__c != :CommonUtility.OFFER_PBTYPE_SPECIAL
            AND Offer_from_Price_Book__c = :mapData.get(1).Id
        ];

        System.assertEquals(offerWithPriceBook.size(), 2);

    	Test.startTest();
    	IntegrationProductsHelper.CourseOffer_JSON newcourseOfferToParse = IntegrationProductsHelper.prepareTuitionSystemAndInstallmentsOnOffer(courseOfferToParse, offerWithPriceBook);
    	Test.stopTest();

    	System.assertEquals(newcourseOfferToParse.tuition_system_default, 
    						new List<String> { 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED), 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED) 
                            });
        
    	System.assertEquals(newcourseOfferToParse.tuition_system_foreigner, 
    						new List<String> { 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED), 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED) 
                            });

    	System.assertEquals(newcourseOfferToParse.installments, new List<Integer> { 1, 2, 10, 12 });
    	System.assertEquals(newcourseOfferToParse.installments_foreigner, new List<Integer> { 1, 2, 10, 12 });
    }

    @isTest static void test_prepareTuitionSystemAndInstallmentsOnSpecialtyOffer() {
    	Map<Integer, sObject> mapData = ZDataTestUtilityMethods.IPH_prepareDataForTest_Statistics(true);

    	PriceBookManager.activatePriceBooks(new List<Id> { mapData.get(8).Id, mapData.get(9).Id  });

    	IntegrationProductsHelper.SpecialtyOffer_JSON specialtyOffer = new IntegrationProductsHelper.SpecialtyOffer_JSON();

        List<Offer__c> offerWithPriceBook = [
            SELECT Installment_Variants__c, Price_Book_Type__c, Graded_Tuition__c, Onetime_Price__c
            FROM Offer__c 
            WHERE Currently_in_use__c = true AND Price_Book_Type__c != :CommonUtility.OFFER_PBTYPE_SPECIAL
            AND Offer_from_Price_Book__c = :mapData.get(2).Id
        ];

        System.assertEquals(offerWithPriceBook.size(), 2);

    	Test.startTest();
    	IntegrationProductsHelper.SpecialtyOffer_JSON newSpecialtyOffer = IntegrationProductsHelper.prepareTuitionSystemAndInstallmentsOnSpecialtyOffer(specialtyOffer, offerWithPriceBook);
    	Test.stopTest();

    	System.assertEquals(newSpecialtyOffer.tuition_system_default, 
    						new List<String> { 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED), 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED) });

    	System.assertEquals(newSpecialtyOffer.tuition_system_foreigner, 
    						new List<String> { 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED), 
                                IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED) });

    	System.assertEquals(newSpecialtyOffer.installments, new List<Integer> { 1, 2, 10, 12 });
    	System.assertEquals(newSpecialtyOffer.installments_foreigner, new List<Integer> { 1, 2, 10, 12 });
    }

    @isTest static void test_prepareSpecializationOnoffer() {
    	Map<Integer, sObject> mapData = ZDataTestUtilityMethods.IPH_prepareDataForTest_Statistics(true);

    	Test.startTest();
    	IntegrationProductsHelper.SpecializationOffer_JSON specializationsOnoffer = IntegrationProductsHelper.prepareSpecializationOnoffer(
    										(Offer__c)mapData.get(1)
    	);

    	Test.stopTest();

    	System.assert(specializationsOnoffer != null);
    }

    @isTest static void test_prepareProductsToSend() {
    	Map<Integer, sObject> mapData = ZDataTestUtilityMethods.IPH_prepareDataForTest_Statistics(true);

    	Test.startTest();
    	String result = IntegrationProductsHelper.prepareProductsToSend();
    	Test.stopTest();

    	System.assert(result != null);
    	System.assert(result != '');
    }

}