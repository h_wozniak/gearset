@isTest
global class ZTestIntegrationGoogleCalendarEvent {

    @isTest static void test_getEventsList() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Position__c = 'Visitator', Email = 'tamara.rubin@enxoo.com'), true);

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.Calendar_SendCalloutMock());

        List<WSB_Calendar_Settings__mdt> wsbSettings = [
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                        Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ];

        System.assert(wsbSettings.size() > 0);

        Test.startTest();
        Boolean status = IntegrationGoogleCalendarEvent.getEventsList(wsbSettings.get(0), 'primary');
        System.assert(status);
        Test.stopTest();

        List<Event> events = [
                SELECT Id, WhoId
                FROM Event
        ];

        System.assertEquals(events.size(), 0);
    }

    @isTest static void test_getEventsList_ESBUnresponding() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Position__c = 'Visitator', Email = 'tamara.rubin@enxoo.com'), true);

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.Calendar_SendFalseCalloutMock());

        List<WSB_Calendar_Settings__mdt> wsbSettings = [
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                        Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ];

        System.assert(wsbSettings.size() > 0);

        Test.startTest();
        Boolean status = IntegrationGoogleCalendarEvent.getEventsList(wsbSettings.get(0), 'primary');
        System.assert(!status);
        Test.stopTest();

        List<Event> events = [
                SELECT Id, WhoId
                FROM Event
        ];

        System.assertEquals(events.size(), 0);
    }

    @isTest static void test_getEventsList_emptyCalendar() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Position__c = 'Visitator', Email = 'tamara.rubin@enxoo.com'), true);

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.Calendar_SendCalloutMock());

        List<WSB_Calendar_Settings__mdt> wsbSettings = [
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                        Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ];

        System.assert(wsbSettings.size() > 0);

        Test.startTest();
        Boolean status = IntegrationGoogleCalendarEvent.getEventsList(wsbSettings.get(0), null);
        System.assert(!status);
        Test.stopTest();

        List<Event> events = [
                SELECT Id, WhoId
                FROM Event
        ];

        System.assertEquals(events.size(), 0);
    }
    
    @isTest static void test_getEventsList_fromBatch() {
        CustomSettingDefault.initEsbConfig();

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Position__c = 'Visitator', Email = 'tamara.rubin@enxoo.com'), true);

        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.Calendar_SendCalloutMock());

        List<WSB_Calendar_Settings__mdt> wsbSettings = [
                SELECT Auth_Provider__c, Auth_URI__c, Client_Cert__c, Client_Email__c, Client_id__c, Type__c,
                        Private_Key__c, Private_Key_Id__c, Project_id__c, SFDC_Username__c, Token_URI__c
                FROM WSB_Calendar_Settings__mdt
        ];

        System.assert(wsbSettings.size() > 0);

        Test.startTest();
        RetrieveHighSchoolVisitsBatch highSchoolEventsRetriever = new RetrieveHighSchoolVisitsBatch();
        Database.executebatch(highSchoolEventsRetriever, 200);
        //Boolean status = IntegrationGoogleCalendarEvent.getEventsList(wsbSettings.get(0), 'primary');
        //System.assert(status);
        Test.stopTest();

        List<Event> events = [
                SELECT Id, WhoId
                FROM Event
        ];

        System.assertEquals(events.size(), 0);
    }
}