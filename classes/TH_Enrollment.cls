/**
* @description  This class is a Trigger Handler for Training Enrollments - complex logic
**/

public without sharing class TH_Enrollment extends TriggerHandler.DelegateBase {

    Set<Id> offersToUpdateStatistics;
    Set<Id> trainingEnrollmentsToUpdateValue;
    Set<Id> enrollmentsToUpdateDiscountValue;
    List<Enrollment__c> potentialDuplicateParticipants;
    Set<Id> potentialDuplicateMainSchedulesIds;
    List<Enrollment__c> participantsToCheckReserveList;
    Set<Id> enrollmentsToSendEmail;
    Set<Id> enrollmentsToSendEmailToRP;
    List<Enrollment__c> prEnrollmentsToUpdateContacts;
    List<Enrollment__c> prEnrollmentsToUpdateContacts3;
    Set<Id> contactsIdsToDesignateStatuses;


    public override void prepareBefore() {
        potentialDuplicateParticipants = new List<Enrollment__c>();
        potentialDuplicateMainSchedulesIds = new Set<Id>();
        participantsToCheckReserveList = new List<Enrollment__c>();
    }

    public override void prepareAfter() {
        trainingEnrollmentsToUpdateValue = new Set<Id>();
        enrollmentsToUpdateDiscountValue = new Set<Id>();
        offersToUpdateStatistics = new Set<Id>();
        potentialDuplicateParticipants = new List<Enrollment__c>();
        enrollmentsToSendEmail = new Set<Id>();
        enrollmentsToSendEmailToRP = new Set<Id>();
        prEnrollmentsToUpdateContacts = new List<Enrollment__c>();
        prEnrollmentsToUpdateContacts3 = new List<Enrollment__c>();
        contactsIdsToDesignateStatuses = new Set<Id>();
    }

    public override void beforeInsert(List<sObject> o) {
        List<Enrollment__c> newEnrollmentList = (List<Enrollment__c>)o;
        for(Enrollment__c newEnrollment : newEnrollmentList) {

            /* Beniamin Cholewa */
            // checking for potential duplicates in participants
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)) {
                potentialDuplicateParticipants.add(newEnrollment);
                potentialDuplicateMainSchedulesIds.add(newEnrollment.Training_Offer_for_Participant__c);
            }

            /* Beniamin Cholewa */
            // Check if available seats number is exceeded. If so, update new participants statuses to 'Reserve list'
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Training_Offer_for_Participant__c != null) {
                participantsToCheckReserveList.add(newEnrollment);  
            }

        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;
        Enrollment__c newEnrollment;

        for(Id key : newEnrollments.keySet()) {
            newEnrollment = newEnrollments.get(key);

            /* Beniamin Cholewa */
            // calculate potential and confirmed candidates on enrollment insert
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
                && newEnrollment.Training_Offer_for_Participant__c != null) {
                offersToUpdateStatistics.add(newEnrollment.Training_Offer_for_Participant__c);
            }

            /* Beniamin Cholewa */
            // send email to the participant while enrolling 
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Training_Offer_for_Participant__c != null) {
                enrollmentsToSendEmail.add(newEnrollment.Id); 
            }

            /* Wojciech Słodziak */
            // send email to Reporting Person 
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING) 
                && newEnrollment.Reporting_Person__c != null
                && newEnrollment.Company__c != null) {
                enrollmentsToSendEmailToRP.add(newEnrollment.Id); 
            }

            /* Wojciech Słodziak */
            // update Enrollment_Value__c on Training enrollment when new Participant_Enrollment_Result is inserted
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)) {
                trainingEnrollmentsToUpdateValue.add(newEnrollment.Enrollment_Training_Participant__c);
            }

            /* Wojciech Słodziak */
            // Update participant recordType on participant result enrollment insert
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)) {
                prEnrollmentsToUpdateContacts.add(newEnrollment);  
            }

            /* Wojciech Słodziak */
            // Update participant University_for_sharing after enrolling
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.University_Name__c != null) {
                prEnrollmentsToUpdateContacts3.add(newEnrollment);  
            }     

            /* Sebastian Łasisz */
            // on Enrollment Training insert designate new Statuses on Contact
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Training_Offer_for_Participant__c != null) {
                contactsIdsToDesignateStatuses.add(newEnrollment.Participant__c);
            }
        }
    }

    //public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {}
 
    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Map<Id, Enrollment__c> newEnrollments = (Map<Id, Enrollment__c>)o;

        for (Id key : oldEnrollments.keySet()) {
            Enrollment__c oldEnrollment = oldEnrollments.get(key);
            Enrollment__c newEnrollment = newEnrollments.get(key);

            /* Beniamin Cholewa */
            // update potential and confirmed candidates on enrollment change 
            if (oldEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c != oldEnrollment.Status__c 
                && oldEnrollment.Training_Offer_for_Participant__c != null) {
                offersToUpdateStatistics.add(newEnrollment.Training_Offer_for_Participant__c);
            }

            /* Wojciech Słodziak */
            // update Enrollment_Value__c on Training enrollment when Participant_Enrollment_Result's Enrollment_Value__c or Status__c changed
            if (newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && 
                    (newEnrollment.Enrollment_Value__c != oldEnrollment.Enrollment_Value__c 
                    || newEnrollment.Status__c != oldEnrollment.Status__c
                )) {
                trainingEnrollmentsToUpdateValue.add(newEnrollment.Enrollment_Training_Participant__c);
            }

            /* Wojciech Słodziak */
            // Update Discount Value for Open and Closed trainings on EnrollmentValue change
            if (
                (
                    newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_OPEN_TRAINING) 
                    || newEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_CLOSED_TRAINING)
                ) && newEnrollment.Enrollment_Value__c != oldEnrollment.Enrollment_Value__c) {
                enrollmentsToUpdateDiscountValue.add(newEnrollment.Id);
            }  

            /* Sebastian Łasisz */
            // on Enrollment Training insert designate new Statuses on Contact
            if (oldEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT) 
                && newEnrollment.Status__c != oldEnrollment.Status__c 
                && oldEnrollment.Training_Offer_for_Participant__c != null) {
                contactsIdsToDesignateStatuses.add(newEnrollment.Participant__c);
            }
        }
        
    }

    public override void beforeDelete(Map<Id, sObject> old) {}

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Enrollment__c> oldEnrollments = (Map<Id, Enrollment__c>)old;
        Enrollment__c oldEnrollment;
        for(Id key : oldEnrollments.keySet()) {
            oldEnrollment = oldEnrollments.get(key);

            /* Beniamin Cholewa */
            // update potential and confirmed candidates on enrollment delete
            if (oldEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)
                && oldEnrollment.Training_Offer_for_Participant__c != null) {
                offersToUpdateStatistics.add(oldEnrollment.Training_Offer_for_Participant__c);
            }

            /* Wojciech Słodziak */
            // update Enrollment_Value__c on Training enrollment when new Participant_Enrollment_Result is deleted
            if (oldEnrollment.RecordTypeId == CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_PARTICIPANT_RESULT)) {
                trainingEnrollmentsToUpdateValue.add(oldEnrollment.Enrollment_Training_Participant__c);
            }
        }
    }

    public override void finish() {
        /* Beniamin Cholewa */
        // Potential duplicate participants detection
        if (potentialDuplicateMainSchedulesIds != null && !potentialDuplicateMainSchedulesIds.isEmpty()) {
            EnrollmentManager_Trainings.participantDuplicateDetection(potentialDuplicateParticipants, potentialDuplicateMainSchedulesIds);
        }

        /* Wojciech Słodziak */
        // update Enrollment_Value__c on Training Enrollment (ONLY OPEN ENROLLMENTS) when new Participant_Enrollment_Result is inserted/deleted
        if (trainingEnrollmentsToUpdateValue != null && !trainingEnrollmentsToUpdateValue.isEmpty()) {
            EnrollmentManager_Trainings.updateTrainingEnrollmentsValue(trainingEnrollmentsToUpdateValue);
        }

        /* Wojciech Słodziak */
        // Run recalculation of Discount Value for listed Enrollment Ids
        if (enrollmentsToUpdateDiscountValue != null && !enrollmentsToUpdateDiscountValue.isEmpty()) {
            EnrollmentManager_Trainings.recalculateDiscountValue(enrollmentsToUpdateDiscountValue);
        }

        /* Beniamin Cholewa */
        // formula for assigning 'Reserve list' status when necessary
        if (participantsToCheckReserveList != null && !participantsToCheckReserveList.isEmpty()) {
            EnrollmentManager_Trainings.updateParticipantStatusToReserveList(participantsToCheckReserveList);
        }

        /* Beniamin Cholewa */
        // update confirmed and potential candidates
        if (offersToUpdateStatistics != null && !offersToUpdateStatistics.isEmpty()) {
            OfferManager_Trainings.updateCandidateStatisticsOnOffer(offersToUpdateStatistics);
        }

        /* Wojciech Słodziak */
        // if Enrollment or EducationalAgreement statuses change Contact change designate Statuses
        if (contactsIdsToDesignateStatuses != null && !contactsIdsToDesignateStatuses.isEmpty()) {
            ContactManager.designateContactStatuses(contactsIdsToDesignateStatuses);
        }

        /* Wojciech Słodziak */
        // Update participant recordType on participant result enrollment insert
        if (prEnrollmentsToUpdateContacts != null && !prEnrollmentsToUpdateContacts.isEmpty()) {
            ContactManager.updateRecordTypeOnParticipant(prEnrollmentsToUpdateContacts);
        }

        /* Wojciech Słodziak */
        // Update participant University_for_sharing after enrolling
        if (prEnrollmentsToUpdateContacts3 != null && !prEnrollmentsToUpdateContacts3.isEmpty()) {
            ContactManager.updateUniversityForSharingOnContact(prEnrollmentsToUpdateContacts3);
        }

        /* Beniamin Cholewa, editor Wojciech Słodziak */
        // sending mails to participants (KEEP AT BOTTOM)
        if (enrollmentsToSendEmail != null && !enrollmentsToSendEmail.isEmpty()) {
            EmailManager.sendPaymentDataToParticipants(enrollmentsToSendEmail);
        }

        /* Wojciech Słodziak */
        // sending mails to reporting person (KEEP AT BOTTOM)
        if (enrollmentsToSendEmailToRP != null && !enrollmentsToSendEmailToRP.isEmpty()) {
            EmailManager.sendPaymentDataToReportingPerson(enrollmentsToSendEmailToRP);
        }
    }

}