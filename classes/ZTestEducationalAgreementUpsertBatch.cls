@isTest
private class ZTestEducationalAgreementUpsertBatch {

    private static void prepareCatalogData() {
        List<Catalog__c> catalogsToInsert = new List<Catalog__c> {
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1,
                    Consent_ADO_API_Name__c = 'Consent_Marketing_ADO__c',
                    Consent_API_Name__c = 'Consent_Marketing__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_2,
                    Consent_ADO_API_Name__c = 'Consent_Electronic_Communication_ADO__c',
                    Consent_API_Name__c = 'Consent_Electronic_Communication__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3,
                    Consent_ADO_API_Name__c = 'Consent_PUCA_ADO__c',
                    Consent_API_Name__c = 'Consent_PUCA__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_4,
                    Consent_ADO_API_Name__c = 'Consent_Direct_Communications_ADO__c',
                    Consent_API_Name__c = 'Consent_Direct_Communications__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_5,
                    Consent_ADO_API_Name__c = 'Consent_Graduates_ADO__c',
                    Consent_API_Name__c = 'Consent_Graduates__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_1,
                    Consent_API_Name__c = 'Consent_Terms_of_Service_Acceptation__c'
            ), false),
            ZDataTestUtility.createBaseConsent(
                new Catalog__c(Name = RecordVals.MARKETING_CONSENT_ENR_2,
                    Consent_API_Name__c = 'Consent_Date_processing_for_contract__c'
            ), false),
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_AGREEMENT), false),    
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_OATH), false),
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ACCEPTANCE), false),    
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_DIPLOMA), false),    
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ATTESTATION_DIPLOMA), false),    
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_CERTIFICATE_OF_PROFESSIONAL_EXPERIENCE), false),   
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ENTRY_BUSINESS_REGISTER), false), 
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ENTRYFEE_PAYCONF), false),  
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_PERSONAL_QUESTIONNARE), false),  
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_COMPANY_DISCOUNT), false),  
            ZDataTestUtility.createDocument(new Catalog__c(Name = RecordVals.CATALOG_DOCUMENT_ALEVEL_CERTIFICATE), false)
        };

        insert catalogsToInsert;
    }
    
    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true));
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(1).Id), true));
        Test.startTest();
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id, Candidate_Student__c = retMap.get(0).Id), true));
        
        return retMap;
    }

	@isTest static void test_EducationalAgreementUpsertBatch() {
        prepareCatalogData();
        Map<Integer, sObject> dataMap = prepareData();

        Enrollment__c studyEnr = (Enrollment__c)dataMap.get(3);

        Test.setMock(HttpCalloutMock.class, new SendCalloutMock());

        
        Database.executeBatch(new EducationalAgreementUpsertBatch(new Set<Id> { studyEnr.Id }, false, false), 1);
        Test.stopTest();

        List<Enrollment__c> eduAgr = [
        	SELECT Id
        	FROM Enrollment__c
        	WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_EDU_AGR)
        ];

        System.assertEquals(eduAgr.size(), 1);
	}



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public class SendCalloutMock implements HttpCalloutMock {
        
        public HttpResponse respond(HTTPRequest req) {            
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            return res;
        }
    }
}