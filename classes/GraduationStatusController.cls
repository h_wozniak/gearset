/**
*   @author         Sebastian Łasisz
*   @description    Class used to inform used that new agreement must be generated.
**/

public class GraduationStatusController {
	public String enrollmentId;
	public Enrollment__c enrollment;

    public String fontColor { get; set; }

	public String graduateErrorMsg { get; set; }
	public String generateAgreementMsg { get; set; }
    public String generateAllDocumentsMsg { get; set; }

    public String backgroundColor { get; set;}
    public String backgroundAgreementColor { get; set; }
    public String backgroudnAllDocumentsColor { get; set; }

    public String checkDiscounts { get; set; }
    public String checkDiscountsBackgroundColor { get; set; }


	public GraduationStatusController (ApexPages.StandardController controller) {
    	enrollmentId = controller.getRecord().Id;

    	enrollment = [
    		SELECT Id, Graduate_CRM__c, WSB_Graduate__c, Graduation_Checked__c, Graduation_Comparison__c, Verify_Agreement__c,
                   Verify_All_Documents__c, Changed_Fields_on_Documents__c, TECH_Check_Discounts_on_Enrollment__c
    		FROM Enrollment__c
    		WHERE Id = :enrollmentId
    	];

        if (enrollment.TECH_Check_Discounts_on_Enrollment__c) {
            checkDiscounts = '- ' + Label.msg_error_checkDiscountsOnEnrollment;
            checkDiscountsBackgroundColor = '#f4f433';
        }

    	/* Graduation Status */
    	if (!enrollment.Graduation_Comparison__c) {
    		graduateErrorMsg = '';
    		backgroundColor = '';
    	}
    	else {
    		if (!enrollment.Graduation_Checked__c) {
    			graduateErrorMsg = '- ' + Label.msg_differenceInGraduationStatus;
            	backgroundColor = '#f4f433';
                fontColor = '#000000';
    		}
    		else {
    			graduateErrorMsg = '- ' + Label.msg_differenceInGraduationStatusAccepted;
            	backgroundColor = '#52A849';
                fontColor = '#FFFFFF';
    		}
    	}

    	/* Generate Agreement Status */
    	if (enrollment.Verify_Agreement__c) {
    		generateAgreementMsg = '- ' + Label.msg_verifyAgreement;
    		backgroundAgreementColor = '#f4f433';
    	}
    	else {
    		generateAgreementMsg = '';
    		backgroundAgreementColor = '';    		
    	}

        /* Generate All Documents */
        Map<String, Schema.SObjectField> contactFieldMap = Schema.SObjectType.Contact.fields.getMap();
        Map<String, Schema.SObjectField> enrollmentFieldMap = Schema.SObjectType.Enrollment__c.fields.getMap();

        if (enrollment.Changed_Fields_on_Documents__c != null) {
            List<String> fieldsToUpdateField = enrollment.Changed_Fields_on_Documents__c.split(' ');

            String finalFieldsList = '';
            for (String field : fieldsToUpdateField) {
                if (field == 'Initial_Specialty_Declaration__c') {
                    finalFieldsList += enrollmentFieldMap.get(field).getDescribe().getLabel() + ', ';
                }
                else {
                    for(String key : contactFieldMap.KeySet()) {
                        if (field == contactFieldMap.get(key).getDescribe().getName()) {
                            finalFieldsList += contactFieldMap.get(key).getDescribe().getLabel() + ', ';                                
                        }
                    }
                }
            }

            finalFieldsList = finalFieldsList.substring(0, finalFieldsList.lastIndexOf(', '));

            if (enrollment.Verify_All_Documents__c) {
                generateAllDocumentsMsg = '- ' + Label.msg_error_validateAllDocuments1 
                                        + ' ' + finalFieldsList 
                                        + '. ' + Label.msg_error_validateAllDocuments2;
                backgroudnAllDocumentsColor = '#f4f433';
            }
            else {
                generateAllDocumentsMsg = '';
                backgroudnAllDocumentsColor = '';
            }
        }
	}
}