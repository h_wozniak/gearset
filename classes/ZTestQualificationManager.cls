@isTest
private class ZTestQualificationManager {

    private static User createTestUser() {
        User testUser = new User();
        testUser.UserName = 'testUser1@company12345612345xx.com';
        testUser.Email = 'testuser1@company.com';
        testUser.LastName = 'user';
        testUser.FirstName = 'test';
        testUser.Alias = 'alias';
        testUser.EmailEncodingKey = 'UTF-8';
        testUser.LanguageLocaleKey = 'pl';
        testUser.WSB_Department__c = RecordVals.WSB_NAME_WRO;
        testUser.LocaleSidKey = 'pl';
        testUser.TimeZoneSidKey = 'Europe/Berlin';
        testUser.ProfileId = ZDataTestUtility.getProfile(CommonUtility.PROFILE_SALES_BR).Id;
        testUser.UserRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = :CommonUtility.WSB_ROLE_WRO].Id;

        insert testUser;

        return testUser;
    }
    
    @isTest static void test_rewriteUniversityForSharingFromContact() {
        User u = createTestUser();

        Contact c;
        Qualification__c finishedUniv;

        Test.startTest();
        System.runAs(u) {
            c = ZDataTestUtility.createCandidateContact(null, true);

            finishedUniv = ZDataTestUtility.createFinishedUniversity(new Qualification__c(
                Candidate_from_Finished_University__c = c.Id
            ), true);
        }
        Test.stopTest();

        Qualification__c finishedUnivAfter = [SELECT University_for_sharing__c FROM Qualification__c WHERE Id = : finishedUniv.Id];

        System.assertEquals(RecordVals.WSB_NAME_WRO, finishedUnivAfter.University_for_sharing__c);
    }
    
    @isTest static void test_updateUniversityForSharingOnRelatedDidactics() {
        User u = createTestUser();

        Contact c;
        Qualification__c finishedUniv;

        Test.startTest();
        System.runAs(u) {
            c = ZDataTestUtility.createCandidateContact(null, true);

            finishedUniv = ZDataTestUtility.createFinishedUniversity(new Qualification__c(
                Candidate_from_Finished_University__c = c.Id
            ), true);

            c = [SELECT Id, University_for_sharing__c FROM Contact WHERE Id = :c.Id];
            c.University_for_sharing__c += ', ' + RecordVals.WSB_NAME_CHO;
            update c;
        }
        Test.stopTest();


        Qualification__c finishedUnivAfter = [SELECT University_for_sharing__c FROM Qualification__c WHERE Id = : finishedUniv.Id];

        System.assertNotEquals(null, finishedUnivAfter.University_for_sharing__c);
        System.assert(finishedUnivAfter.University_for_sharing__c.contains(RecordVals.WSB_NAME_WRO));
        System.assert(finishedUnivAfter.University_for_sharing__c.contains(RecordVals.WSB_NAME_CHO));
    }

}