/**
*   @author         Grzegorz Długosz
*   @description    This class is used to test if functionality used to make dictionaries available for external services works properly
**/
@isTest
private class ZTestIntegrationDictionaryResource {

    @isTest static void getDictionary_Countries() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'countries');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('language_code'));
        System.assert(response.contains('dictionary_values'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_Universities_All_Params() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities');
        req.addParameter('date_from', '1990-01-01');
        req.addParameter('wsb', 'true');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_Universities_No_Date_noWSB() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities');
        req.addParameter('wsb', 'false');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_Universities_No_Date() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities');
        req.addParameter('wsb', 'true');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_Universities_No_Wsb() {

        ZdataTestUtility.createUniversityExternal(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities');
        req.addParameter('wsb', 'false');
        req.addParameter('date_from', '1990-01-01');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();


        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_HighSchools_All_Params() {

        ZdataTestUtility.createHighSchool(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'schools');
        req.addParameter('date_from', '1990-01-01');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('school_name'));
        System.assert(response.contains('school_city'));
        System.assert(response.contains('school_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_HighSchools_No_Date() {

        ZdataTestUtility.createHighSchool(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'schools');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('school_name'));
        System.assert(response.contains('school_city'));
        System.assert(response.contains('school_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_Wrong_Id() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'noDictionaryWithThisId');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assertEquals(400, res.statusCode);

    }

    @isTest static void getDictionary_No_Id() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assertEquals(400, res.statusCode);

    }

    @isTest static void getDictionary_universities_ipresso_All_Params() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities_ipresso');
        req.addParameter('date_from', '1990-01-01');
        req.addParameter('wsb', 'true');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_universities_ipresso_No_Date_noWSB() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities_ipresso');
        req.addParameter('wsb', 'false');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_universities_ipresso_No_Date() {

        ZdataTestUtility.createUniversity(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities_ipresso');
        req.addParameter('wsb', 'true');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();

        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    @isTest static void getDictionary_universities_ipresso_No_Wsb() {

        ZdataTestUtility.createUniversityExternal(null, true);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/dictionary/';
        req.addParameter('dictionary_id', 'universities_ipresso');
        req.addParameter('wsb', 'false');
        req.addParameter('date_from', '1990-01-01');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();        
            IntegrationDictionaryResource.getDictionary();
        Test.stopTest();

        String response = res.responseBody.toString();


        System.assert(String.isNotEmpty(response));
        System.assert(response.contains('university_name'));
        System.assert(response.contains('university_city'));
        System.assert(response.contains('university_type'));
        System.assertEquals(200, res.statusCode);

    }

    
}