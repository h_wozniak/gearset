/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to send emails with Extranet Information
**/

/**
*   To initiate the EnrollmentDocumentDeliveryExtranetEmail execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('EnrollmentDocumentDeliveryExtranetEmail', '0 0 8 * * ? *', new EnrollmentDocumentDeliveryExtranetEmail());
**/

global class WorkshopReminderSchoolSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        WorkshopReminderSchoolBatch workshopReminderSchool = new WorkshopReminderSchoolBatch(1);
        Database.executebatch(workshopReminderSchool, 2);
    }
}