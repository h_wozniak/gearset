@isTest
private class ZTestDeliveredEmailTemplateController {

	@isTest static void test_DeliveredEmailTemplateController_empty() {
		Test.startTest();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
		Task t = EmailManager.createEmailTask('Test subject', studyEnr.Id, studyEnr.Candidate_Student__c, null, null);
		insert t;
		Test.stopTest();

        ApexPages.CurrentPage().getparameters().put('taskId', t.Id);
        DeliveredEmailTemplateController cntrl = new DeliveredEmailTemplateController();

        System.assertEquals(cntrl.htmlEmail, null);
        System.assertEquals(cntrl.pinnedAttachments, null);
	}

	@isTest static void test_DeliveredEmailTemplateController() {
		Test.startTest();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
		Task t = EmailManager.createEmailTask('Test subject', studyEnr.Id, studyEnr.Candidate_Student__c, 'Test msg', new List<String> {'Test attachment'});
		insert t;
		Test.stopTest();

        ApexPages.CurrentPage().getparameters().put('taskId', t.Id);
        DeliveredEmailTemplateController cntrl = new DeliveredEmailTemplateController();

        System.assertNotEquals(cntrl.htmlEmail, null);
        System.assertNotEquals(cntrl.pinnedAttachments, null);
	}

	@isTest static void test_DeliveredEmailTemplateController_back() {
		Test.startTest();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);
		Task t = EmailManager.createEmailTask('Test subject', studyEnr.Id, studyEnr.Candidate_Student__c, 'Test msg', new List<String> {'Test attachment'});
		insert t;
		Test.stopTest();

        ApexPages.CurrentPage().getparameters().put('taskId', t.Id);
        DeliveredEmailTemplateController cntrl = new DeliveredEmailTemplateController();

        PageReference pf = cntrl.back();
        System.assertNotEquals(pf, null);
	}
}