global class CreateCampaignLackOfDocumentsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    Date dayOfEnrollment;
    Set<Id> campaignIds = new Set<Id>();
    Map<String, String> contactsToUpdateInOtherSystem = new Map<String, String>();

    global CreateCampaignLackOfDocumentsBatch(Integer daysAfterEnrollment) {
        dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Candidate_Student__c
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY) 
            AND RequiredDocumentsEmailedDate__c = :dayOfEnrollment
            AND RequiredDocListEmailedCount__c  >= 3
            AND Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
            AND Source_Studies__c != :CommonUtility.ENROLLMENT_SOURCE_ST_EA
            AND Guided_group__c = false
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrollmentIDs = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrollmentIDs.add(enr.Id);
            contactsToUpdateInOtherSystem.put(enr.Candidate_Student__c + ';' + contactsToUpdateInOtherSystem.size(), RecordVals.UPDATING_SYSTEM_FCC);
        }


        CommonUtility.preventsSendingCalloutAsAtFuture = true;
        CommonUtility.preventSendingWelcomeCallToFcc = true;
        campaignIds.addAll(MarketingCampaignManager.createContactCampaing(enrollmentIDs, CommonUtility.MARKETING_CAMPAIGN_LACKOFDOCUMENTS));
        
    }
    
    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            IntegrationFCCContactSender.sendNewFCCRecordsSync(campaignIds);
            
            CommonUtility.preventSendingContactUpdateCalloutsAsAtFuture = true;
            IntegrationContactUpdateOther.sendUpdatedContactToOtherSystemsSync(contactsToUpdateInOtherSystem);
        }
    }    
}