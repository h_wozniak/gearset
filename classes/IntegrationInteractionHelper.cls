/**
*   @author         Sebastian Łasisz
*   @description    This class is used to parse data for Integration Interaction
**/

public with sharing class IntegrationInteractionHelper {

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- PREPARING DATA ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class Interaction_JSON {
    	public String crm_contact_id;
    	public String external_contact_id;
    	public String interaction_subject;
    	public String interaction_date;
    	public String interaction_type;
    	public String interaction_classifier;
    	public String interaction_description;
    	public String agent_id;
    	public CallDetails_JSON call_details;
    	public EmailDetails_JSON email_details;
    }

    public class CallDetails_JSON {
    	public String call_type;
    	public Boolean call_answered;
    	public String phone_number;
    }

    public class EmailDetails_JSON {
    	public String email;
    }
}