/**
* @description  This class is a Trigger Handler for Studies - field edit disabling
**/

public without sharing class TH_OfferDisables extends TriggerHandler.DelegateBase {


    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Offer__c> oldOffers = (Map<Id, Offer__c>)old;
        Map<Id, Offer__c> newOffers = (Map<Id, Offer__c>)o;
        Offer__c oldOffer;
        Offer__c newOffer;
        for (Id key : newOffers.keySet()) {
            oldOffer = oldOffers.get(key);
            newOffer = newOffers.get(key);

            /* Wojciech Słodziak */
            // disable editing of Name on all RTs
            if (!CommonUtility.skipNameEditDisabling && newOffer.Name != oldOffer.Name) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Name'));
            }

            /* Wojciech Słodziak */
            // disable editing of University_from_Training__c on Training Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                && newOffer.University_from_Training__c != oldOffer.University_from_Training__c && oldOffer.University_from_Training__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'University_from_Training__c'));
            }

            /* Wojciech Słodziak */
            // disable editing of Type__c on Training Offer
            if (newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_TRAINING_OFFER) 
                && newOffer.Type__c != oldOffer.Type__c && oldOffer.Type__c != null) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Type__c'));
            }

            /* Wojciech Słodziak */
            // disable editing of Training_Offer_from_Schedule__c on Schedule
            if ((
                    newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_OPEN_TRAINING_SCHEDULE) 
                    || newOffer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_CLOSED_TRAINING_SCHEDULE)
                ) 
                && newOffer.Training_Offer_from_Schedule__c != oldOffer.Training_Offer_from_Schedule__c
            ) {
                newOffer.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Offer__c', 'Training_Offer_from_Schedule__c'));
            }

        }
    }

}