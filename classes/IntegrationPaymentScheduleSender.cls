/**
*   @author         Wojciech Słodziak
*   @description    This class is used to send Payment Schedules to Experia
**/

global without sharing class IntegrationPaymentScheduleSender {

   public static void sendPaymentSchedulesAsync(Set<Id> enrIds, Boolean sendingInitialFees) {
        PaymentManager.updatePaymentsWithExpiredDeadline(enrIds);
        IntegrationEnrollmentSender.sendEnrollmentListAtFuture(enrIds, true, sendingInitialFees);
        IntegrationPaymentScheduleHelper.updateSyncStatus_inProgress(true, enrIds, '', sendingInitialFees);
    }

    @future(callout=true)
    public static void sendPaymentSchedulesAsyncWithoutEnrollment(Set<Id> enrIds, Boolean sendingInitialFees) {
        IntegrationPaymentScheduleSender.sendPaymentScheduleFinal(enrIds, sendingInitialFees);
    }

    public static Boolean sendPaymentSchedulesSync(Set<Id> enrIds, Boolean sendingInitialFees) {
        return IntegrationPaymentScheduleSender.sendPaymentScheduleFinal(enrIds, sendingInitialFees);
        //IntegrationPaymentScheduleHelper.updateSyncStatus_inProgress(true, enrIds);
        //sendPaymentSchedulesAsyncWithoutEnrollment(enrIds);
        //return true;
    }

    // final method for sending callout
    @TestVisible
    private static Boolean sendPaymentScheduleFinal(Set<Id> enrIds, Boolean sendingInitialFees) {
        HttpRequest httpReq = new HttpRequest();
        
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createPaymentEndPoint = esb.Create_Payment_Schedules_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createPaymentEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationPaymentScheduleHelper.buildPaymentSchedulesJSON(enrIds, sendingInitialFees);
        httpReq.setBody(jsonToSend);

        
        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        try {
            Error_JSON errors = parseErrors(httpRes.getBody());

            Set<Id> potentailInvalidRecords = new Set<Id>();
            for (Record_JSON record : errors.invalid_records) {
                potentailInvalidRecords.add(record.recrutation_id);
            }

            Map<Id, Enrollment__c> educationalAgreements = new Map<Id, Enrollment__c>([
                SELECT Id, Source_Enrollment__c
                FROM Enrollment__c
                WHERE Id IN :potentailInvalidRecords
            ]);

            Set<Id> invalidRecords = new Set<Id>();
            for (Record_JSON record : errors.invalid_records) {
                invalidRecords.add(educationalAgreements.get(record.recrutation_id).Source_Enrollment__c);
                enrIds.remove(educationalAgreements.get(record.recrutation_id).Source_Enrollment__c);
            }
            
            IntegrationPaymentScheduleHelper.updateSyncStatus_inProgress(false, invalidRecords, httpRes.getBody(), sendingInitialFees);
        }
        catch (Exception e) {
            //no error occured
            System.debug(e);
        }

        Boolean success = (httpRes.getStatusCode() == 200 || httpRes.getStatusCode() == 202);
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationPaymentScheduleSender.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + enrIds);
        }

        IntegrationPaymentScheduleHelper.updateSyncStatus_inProgress(success, enrIds, '', sendingInitialFees);
        return success;
    }
    
    private static Error_JSON parseErrors(String jsonBody) {
        return (Error_JSON) System.JSON.deserialize(jsonBody, Error_JSON.class);
    }

    public class Error_JSON {
        public String error_code;
        public List<Record_JSON> invalid_records;
    }

    public class Record_JSON {
        public String error_message;
        public String recrutation_id;
    }

}