@isTest
global class ZTestIntegrationProspectUpdate {

    @isTest static void test_insertProspect() {    
        CustomSettingDefault.initDidacticsCampaign();
        String body = '{"status":"Prospect","updating_system":"CRM","first_name":"Roberto12453","last_name":"Barszcz12453","phones":[{"type":"Default","phoneNumber":"987654321"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"a048E0000046dmH","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        IntegrationProspectUpdate.receiveContactInformation();
        Test.stopTest();
    }

    @isTest static void test_updateProspect() {        
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(null, true);

        String body = '{"updating_system_contact_id":"' + prospect.Id + '","status":"Prospect","updating_system":"CRM","first_name":"Roberto12453","last_name":"Barszcz12453","phones":[{"type":"Default","phoneNumber":"987654321"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"a048E0000046dmH","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        IntegrationProspectUpdate.receiveContactInformation();
        Test.stopTest();
    }

    @isTest static void test_updateProspect_invalidJSON() { 
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(null, true);

        String body = '{}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationProspectUpdate.receiveContactInformation();
        }
        catch (Exception e) {
        	System.assertEquals(e.getMessage(), 'updating_system');
        }
        Test.stopTest();
    }

    @isTest static void test_updateProspect_invalidJSON2() { 
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(null, true);
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationProspectUpdate.receiveContactInformation();
        }
        catch (Exception e) {
        	System.assert(e.getMessage().contains('Argument cannot be null'));
        }
        Test.stopTest();
    }

    @isTest static void test_updateProspect_invalidJSON3() { 
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(null, true);
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof('body');
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationProspectUpdate.receiveContactInformation();
        }
        catch (Exception e) {
        	System.assert(e.getMessage().contains('Argument cannot be null'));
        }
        Test.stopTest();
    }

    @isTest static void test_updateProspect_invalidID() {
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(null, true);

        String body = '{"updating_system_contact_id":"TestID123","status":"Prospect","updating_system":"CRM","first_name":"Roberto12453","last_name":"Barszcz12453","phones":[{"type":"Default","phoneNumber":"987654321"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"a048E0000046dmH","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationProspectUpdate.receiveContactInformation();
        }
        catch (Exception e) {
        	System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
        }
        Test.stopTest();
    }  	

    @isTest static void test_insertProspect_duplicate() {
        CustomSettingDefault.initDidacticsCampaign();
    	Contact prospect = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Roberto12345', LastName = 'Barteez', Phone = '123123123'), true);

        String body = '{"status":"Prospect","updating_system":"CRM","first_name":"' + prospect.FirstName + '","last_name":"' + prospect.LastName + '","phones":[{"type":"Default","phoneNumber":"'+ prospect.Phone + '"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"a048E0000046dmH","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        try {
        	IntegrationProspectUpdate.receiveContactInformation();
        }
        catch (Exception e) {
        	System.assert(e.getMessage().contains('Duplicate'));
        }
        Test.stopTest();
    }

    @isTest static void test_insertProspect_withMarketingCampaign() {
        CustomSettingDefault.initDidacticsCampaign();
    	Marketing_Campaign__c marketingCampaign = new Marketing_Campaign__c();
    	marketingCampaign.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES);
    	marketingCampaign.Campaign_Type__c = 'Incoming';
    	marketingCampaign.Campaign_Communication_Form__c = 'WWW';
    	marketingCampaign.Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT;
    	marketingCampaign.Date_Start__c = System.today();
    	marketingCampaign.Date_End__c = System.today();

    	insert marketingCampaign; 

        String body = '{"status":"Prospect","updating_system":"CRM","first_name":"Roberto12453","last_name":"Barszcz12453","phones":[{"type":"Default","phoneNumber":"987654321"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"a048E0000046dmH","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        IntegrationProspectUpdate.receiveContactInformation();
        Test.stopTest();
    }

    @isTest static void test_insertProspect_withMarketingCampaign_properId() {
        CustomSettingDefault.initDidacticsCampaign();
    	Marketing_Campaign__c marketingCampaign = new Marketing_Campaign__c();
    	marketingCampaign.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES);
    	marketingCampaign.Campaign_Type__c = 'Incoming';
    	marketingCampaign.Campaign_Communication_Form__c = 'WWW';
    	marketingCampaign.Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT;
    	marketingCampaign.Date_Start__c = System.today();
    	marketingCampaign.Date_End__c = System.today();

    	insert marketingCampaign; 

        String body = '{"status":"Prospect","updating_system":"CRM","first_name":"Roberto12453","last_name":"Barszcz12453","phones":[{"type":"Default","phoneNumber":"987654321"},{"type":"Other","phoneNumber":"987654321"},{"type":"Home","phoneNumber":"123456789"},{"type":"Mobile","phoneNumber":"508421915"}],"emails":[{"type":"Default","email":"randomemail@email.com"},{"type":"Other","email":"randomemail2@email2.com"}],"consents":[{"type":"Marketing","department":"WSB Chorzów","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"campaign_id":"' + marketingCampaign.Id + '","campaign_details":[{"detail_name":"areas_of_interest","detail_value":"Logistics and Transport; Sales and Customer Service"}]}';
        
        RestRequest req = new RestRequest();
    	req.httpMethod = 'POST';
    	req.requestBody = Blob.valueof(body);
    	
    	RestResponse res = new RestResponse();
    	RestContext.request = req;
    	RestContext.response = res;
        
        Test.startTest();
        IntegrationProspectUpdate.receiveContactInformation();
        Test.stopTest();
    }
}