@isTest
private class ZTestMarketingCampaignSalesManager {

    @isTest static void test_createClassifiersForMarketingCampaignSales() {
        Test.startTest();
        Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaignSales(null, true);
        Test.stopTest();

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :campaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());
    }

    @isTest static void test_createClassifiersForMarketingCampaignSales_LeaveContact() {
        Test.startTest();
        Marketing_Campaign__c campaign = ZDataTestUtility.createMarketingCampaignSales(new Marketing_Campaign__c(Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT), true);
        Test.stopTest();

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :campaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());
    }

    @isTest static void test_createMembersOnSalesCampaign_rejectOfConsent() {
        ZDataTestUtility.prepareCatalogData(true, false);

        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT,
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());

        Marketing_Campaign__c reject = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_CONSENT_REJECT
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];
        
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);

        Test.startTest();

        MarketingCampaignSalesManager.createMembersOnSalesCampaign('', candidate, salesCampaign.Id, null);

        Test.stopTest();

        candidate.Consent_Electronic_Communication__c = false;
        update candidate;

        Marketing_Campaign__c contact = [
            SELECT Id, Classifier_from_Campaign_Member__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
            AND Campaign_Contact__c = :candidate.Id
        ];

        System.assertEquals(contact.Classifier_from_Campaign_Member__c, reject.Id);
    }

    @isTest static void test_createMembersOnSalesCampaign_duplicatePhone() {
        ZDataTestUtility.prepareCatalogData(true, false);

        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT,
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());

        Marketing_Campaign__c newClassifier = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_NEW
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];

        Marketing_Campaign__c duplicateClassifier = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_DUPLICATIOM
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];
        
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Phone = '1234', Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);
        Contact candidate2 = ZDataTestUtility.createCandidateContact(new Contact(Phone = '1234', Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);

        Test.startTest();

        MarketingCampaignSalesManager.createMembersOnSalesCampaign('', candidate, salesCampaign.Id, null);
        MarketingCampaignSalesManager.createMembersOnSalesCampaign('', candidate2, salesCampaign.Id, null);

        Test.stopTest();

        Marketing_Campaign__c contact = [
            SELECT Id, Classifier_from_Campaign_Member__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
            AND Campaign_Contact__c = :candidate.Id
        ];

        System.assertEquals(contact.Classifier_from_Campaign_Member__c, newClassifier.Id);

        Marketing_Campaign__c contact2 = [
            SELECT Id, Classifier_from_Campaign_Member__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
            AND Campaign_Contact__c = :candidate2.Id
        ];

        System.assertEquals(contact2.Classifier_from_Campaign_Member__c, duplicateClassifier.Id);
    }

    @isTest static void test_createMembersOnSalesCampaign_ZPIEnrollment() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT,
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());

        Marketing_Campaign__c zpi_enrollment = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];
        
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Consent_Electronic_Communication_ADO__c = 'WSB Wrocław'), true);

        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Candidate_Student__c = candidate.Id, 
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
        ), true);

        MarketingCampaignSalesManager.createMembersOnSalesCampaign('', candidate, salesCampaign.Id, null);
        Test.stopTest();

        Marketing_Campaign__c contact = [
            SELECT Id, Classifier_from_Campaign_Member__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
            AND Campaign_Contact__c = :candidate.Id
        ];

        System.assertEquals(contact.Classifier_from_Campaign_Member__c, zpi_enrollment.Id);
    }

    @isTest static void test_createMembersOnSalesCampaign_ZPIEnrollment2() {
        ZDataTestUtility.prepareCatalogData(true, true);

        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Campaign_Kind__c = CommonUtility.MARKETING_CAMPAIGN_LEAVECONTACT,
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);

        List<Marketing_Campaign__c> classifiersOnCampaign = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_CLASSIFIER)
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];

        System.assertEquals(19, classifiersOnCampaign.size());

        Marketing_Campaign__c zpi_enrollment = [
            SELECT Id, Classifier_Name__c
            FROM Marketing_Campaign__c
            WHERE Classifier_Name__c = :CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI
            AND Campaign_from_Classifier__c = :salesCampaign.Id
        ];
        
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com',
            Phone = '1235',
            University_for_sharing__c = 'WSB Wrocław',
            Consent_Electronic_Communication_ADO__c = 'WSB Wrocław',
            Confirmed_Contact__c = true
        ), true);

        MarketingCampaignSalesManager.createMembersOnSalesCampaign('', candidate, salesCampaign.Id, null);

        CommonUtility.preventDeduplicationOfContacts = true;
        CommonUtility.preventTriggeringEmailValidation = true;
        CommonUtility.preventChangingEmailTwice = true;

        Contact candidate2 = ZDataTestUtility.createCandidateContact(new Contact(
            FirstName = 'Tom',
            LastName = 'Hanks',
            Pesel__c = '81031200760',
            Email = 'tom.hanks@mail.com',
            University_for_sharing__c = 'WSB Wrocław',
            Phone = '1234',
            Consent_Electronic_Communication_ADO__c = 'WSB Wrocław',
            Confirmed_Contact__c = true
        ), true);

        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Candidate_Student__c = candidate2.Id, 
            Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
            Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED
        ), true);
        
        candidate2.TECH_Force_Deduplication__c = true;
        CommonUtility.preventDeduplicationOfContacts = false;
        update candidate2;

        Test.stopTest();

        // contact should be deleted after merge
        List<Contact> cAfter = [SELECT Id FROM Contact];
        System.assertEquals(cAfter.size(), 1);

        Marketing_Campaign__c contact = [
            SELECT Id, Classifier_from_Campaign_Member__c, Classifier_from_Campaign_Member__r.Classifier_Name__c
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
            AND Campaign_Member__r.RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_SALES)
            AND Campaign_Contact__c = :candidate2.Id
        ];

        System.assertEquals(contact.Classifier_from_Campaign_Member__r.Classifier_Name__c, CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_SALES_ENROLLED_ZPI);
        System.assertEquals(contact.Classifier_from_Campaign_Member__c, zpi_enrollment.Id);
    }

    @isTest static void test_createCampaignMemberDetailsForSales() {
        Marketing_Campaign__c salesCampaign = ZDataTestUtility.createMarketingCampaignSales(
            new Marketing_Campaign__c(
                Date_Start__c = System.today(),
                Date_End__c = System.today(),
                Active__c = true
        ), true);

        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(FirstName = 'Jan'), true);

        Test.startTest();
        Marketing_Campaign__c newlyCreatedDetails = MarketingCampaignSalesManager.createCampaignMemberDetailsForSales(candidate, '');
        Test.stopTest();

        System.assertEquals(newlyCreatedDetails.Details_Contact_First_Name__c, candidate.FirstName);
        System.assertEquals(newlyCreatedDetails.Id, null);
    }

}