@isTest
private class ZTestAccountManager {
    


    private static Map<Integer, sObject> prepareData1() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Account company = ZDataTestUtility.createCompany(null, true);

        retMap.put(0, ZDataTestUtility.createTrainingOffer(null, true));
        retMap.put(1, ZDataTestUtility.createOpenTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Price_per_Person__c = 1000,
            Price_per_Person_Student_Graduate__c = 900,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createOpenTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Company__c = company.Id
        ), true));
        retMap.put(3, company);
        retMap.put(4, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        
        return retMap;
    }



    @isTest static void testDuplicateAccs() {
        ZDataTestUtility.createCompany(new Account(
            NIP__c = '1877716335'
        ), true);

        Test.startTest();
        try {
            ZDataTestUtility.createCompany(new Account(
                NIP__c = '1877716335'
            ), true);
            System.assert(false);
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.msg_error_DupFound)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void testUpdateOfVocativeRule() {
        Map<Integer, sObject> dataMap = prepareData1();

        Id univerId = ((Offer__c)dataMap.get(0)).University_from_Training__c;

        Test.startTest();
        Account university = [SELECT BillingCity FROM Account WHERE Id = :univerId];
        university.BillingCity = 'Gdynia';
        update university;
        Test.stopTest();

        Offer__c sch = [SELECT University_City_in_Vocative__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];

        System.assertEquals('Gdyni', sch.University_City_in_Vocative__c);
    }
    
}