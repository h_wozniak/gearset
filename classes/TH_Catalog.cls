/**
* @description  This class is a Trigger Handler for Catalogs - complex logic
**/

public without sharing class TH_Catalog extends TriggerHandler.DelegateBase {
    
    List<Catalog__c> catalogsToDisableEditingFields;
    List<Catalog__c> catalogsToCheckDuplicates;
    
    public override void prepareBefore() {
        catalogsToDisableEditingFields = new List<Catalog__c>();
        catalogsToCheckDuplicates = new List<Catalog__c>();
    }
    
    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Catalog__c newCatalog = (Catalog__c)obj;

            /* Wojciech Słodziak */
            // duplicate detection
            if (newCatalog.RecordTypeId != CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_TERMSOFUSE)) {
                catalogsToCheckDuplicates.add(newCatalog);
            }

            /* Sebastian Łasisz */
            // Disable adding catalog for RT other than terms of use unless its administrator user
            if (newCatalog.RecordTypeId != CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_TERMSOFUSE)) {
                if (!CommonUtility.isAdminUser() && !CommonUtility.allowCreatingDocuments) {
                    newCatalog.addError(Label.msg_error_cantCreateCatalog);
                }
            }
        }
    }


    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Catalog__c> oldCatalogs = (Map<Id, Catalog__c>)old;
        Map<Id, Catalog__c> newCatalogs = (Map<Id, Catalog__c>)o;
        for (Id key : oldCatalogs.keySet()) {
            Catalog__c oldCatalog = oldCatalogs.get(key);
            Catalog__c newCatalog = newCatalogs.get(key);
            
            /* Sebastian Łasisz */
            // disable editing of Name field on Document and Consents Record Type when Not Changeable Name is checked
            if ((
                    newCatalog.RecordTypeId == CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT)
                    || newCatalog.RecordTypeId == CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
                    || newCatalog.RecordTypeId == CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT_FORDELIVERY)
                )
                && oldCatalog.Name != newCatalog.Name
                && newCatalog.Not_Changeable_Name__c) {
                newCatalog.addError(' ' + Label.msg_error_CantEditField + ' ' + CommonUtility.getFieldLabel('Catalog__c', 'Name'));
            }

            /* Wojciech Słodziak */
            // duplicate detection
            if (oldCatalog.Name != newCatalog.Name
                && newCatalog.RecordTypeId != CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_TERMSOFUSE)) {
                catalogsToCheckDuplicates.add(newCatalog);
            }

            /* Sebastian Łasisz */
            // Disable editing catalog for RT other than terms of use unless its administrator user
            if (newCatalog.RecordTypeId != CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_TERMSOFUSE)) {
                if (!CommonUtility.isAdminUser()) {
                    newCatalog.addError(Label.msg_error_cantEditCatalog);
                }
            }
        }
    }

    public override void beforeDelete(Map<Id, sObject> old) {
        Map<Id, Catalog__c> oldCatalogs = (Map<Id, Catalog__c>)old;
        for (Id key : oldCatalogs.keySet()) {
            Catalog__c oldCatalog = oldCatalogs.get(key);

            /* Sebastian Łasisz */
            // Disable editing catalog unless its admin user
            if (!CommonUtility.isAdminUser()) {
                oldCatalog.addError(Label.msg_error_cantDeleteCatalog);
            }
        }

    }



    public override void finish() {
        /* Wojciech Słodziak */
        // duplicate Detection (KEEP AT BOTTOM)
        if (catalogsToCheckDuplicates != null && !catalogsToCheckDuplicates.isEmpty()) {
            CatalogManager.detectNameDuplicates(catalogsToCheckDuplicates);
        }

    }

}