@isTest
private class ZTestTargetNewButtonOverride {
    
    @isTest static void testTargetNewButtonOverride() {
        PageReference pageRef = Page.TargetNewButtonOverride;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController(new Target__c());

        TargetNewButtonOverride dnbo = new TargetNewButtonOverride(stdController);

        System.assertNotEquals(null, dnbo.recordTypeList);

        dnbo.continueStep();
    }
    
}