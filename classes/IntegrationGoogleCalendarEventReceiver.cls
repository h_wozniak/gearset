/**
*   @author         Michal Bujak      
*   @description    This class is used for retrieve Calendar Events from Google Calendar.
**/

@RestResource(urlMapping = '/add_calendar_events/*')
global without sharing class IntegrationGoogleCalendarEventReceiver {
    
    @HttpPost
    global static void receiveCalendarEvents() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String jsonBody;
        try {
            jsonBody = req.requestBody.toString().replaceAll('"dateTime"', '"dateTimeJSON"').replaceAll('"end"', '"endtime"');
        } catch (Exception e) {
        //Usually throws when received message is not valid UTF-8 json
            System.debug(e.getMessage());
        ErrorLogger.msg(jsonBody);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
        }
        
        System.debug('jsonBody ' + jsonBody);

        GoogleCalendarEvent googleCEvent;

        try {
            googleCEvent = parseMessage(jsonBody);
        }
        catch(Exception e) {
            System.debug(e.getMessage());
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
        }

        if (googleCEvent != null) {
            try {
                List<WSB_Calendar_Settings__mdt> settingRecord = [
                    SELECT Client_id__c, SFDC_Username__c
                    FROM WSB_Calendar_Settings__mdt
                    WHERE Client_id__c = :googleCEvent.client_id
                ];
                
                Id userId = null;
                if (!settingRecord.isEmpty()) {
                    String userName = settingRecord.get(0).SFDC_Username__c;
                    
                    List<User> users = [
                        SELECT Id, Name
                        FROM User
                        WHERE Name = :userName
                    ];
                    
                    if (!users.isEmpty()) {
                        userId = users.get(0).Id;
                    }
                }
                
                
                Set<String> emailAddresses = new Set<String>();
                for (GoogleEvent googleEvent : googleCEvent.events) {
                    emailAddresses.add(googleEvent.calendarId);
                }

                System.debug('emailAddresses ' + emailAddresses);
                List<Contact> visitators = [
                        SELECT Id, Email
                        FROM Contact
                        WHERE Email IN :emailAddresses
                        AND Position__c = :CommonUtility.CONTACT_POSITION_VISITATOR
                ];

                List<Event> calendarEventsToInsert = getEventsToInsert(googleCEvent, userId);

                if(calendarEventsToInsert.isEmpty()) {
                    res.statusCode = 400;
                    res.responseBody = IntegrationError.getErrorJSONBlobByCode(600);
                    return;
                }

                try {
                    upsert calendarEventsToInsert Activity.Calendar_Id__c;
                }
                catch (Exception e) {
                    ErrorLogger.log(e);
                }

                List<EventRelation> relations = getEventsRelations(visitators, calendarEventsToInsert, googleCEvent);
                Database.insert(relations, false);
            }
            catch(Exception e) {

                res.statusCode = 400;
                res.responseBody = IntegrationError.getErrorJSONBlobByCode(601, e.getMessage());
                return ;
            }
        }
    }
    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public static List<EventRelation> getEventsRelations(List<Contact> visitators, List<Event> calendarEventsToInsert, GoogleCalendarEvent googleCEvent) {
        List<EventRelation> relations = new List<EventRelation>();
        for (Event calendarEvent : calendarEventsToInsert) {
            EventRelation eventCalendarRelation = new EventRelation();
            eventCalendarRelation.EventId = calendarEvent.Id;

            for (GoogleEvent googleEvent : googleCEvent.events) {
                if (calendarEvent.Calendar_Id__c == (googleEvent.calendarId + googleEvent.event.Id)) {
                    for (Contact visitator : visitators) {
                        if (visitator.Email == googleEvent.calendarId) {
                            eventCalendarRelation.RelationId = visitator.Id;
                        }
                    }
                }
            }

            eventCalendarRelation.isInvitee  = true;
            eventCalendarRelation.IsParent = true;
            eventCalendarRelation.RespondedDate = System.now();
            eventCalendarRelation.Status = 'Accepted';

            relations.add(eventCalendarRelation);
        }
        return relations;
    }


    public static List<Event> getEventsToInsert(GoogleCalendarEvent googleCEvent, Id ownerId) {
        List<Event> calendarEventsToInsert = new List<Event>();
        for (GoogleEvent googleEvent : googleCEvent.events) {
            try {
                Event calendarEvent = new Event();
                calendarEvent.Calendar_Id__c = googleEvent.calendarId + googleEvent.event.Id;
                calendarEvent.Description = googleEvent.event.description;
                calendarEvent.EndDateTime = DateTime.newInstance(Long.valueOf(googleEvent.event.endTime.dateTimeJSON.value));
                calendarEvent.Location = googleEvent.event.location;
                calendarEvent.StartDateTime = DateTime.newInstance(Long.valueOf(googleEvent.event.start.dateTimeJSON.value));
                calendarEvent.Subject = googleEvent.event.summary;
                calendarEvent.OwnerId = ownerId;
                calendarEventsToInsert.add(calendarEvent);
            } catch(Exception e) {
                ErrorLogger.log(e);
            } finally {
                continue;
            }
        }
        return calendarEventsToInsert;
    }

    public static GoogleCalendarEvent parseMessage(String jsonBody) {
        return (GoogleCalendarEvent) System.JSON.deserialize(jsonBody, GoogleCalendarEvent.class);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class GoogleCalendarEvent {
        public Integer status;
        public String client_id;
        public List<GoogleEvent> events;
    }

    public class GoogleEvent {
        public EventJSON event;
        public String calendarId;
    }

    public class EventJSON {
        public String description;
        public String summary;
        public String location;
        public String id;
        public StartEndTime start;
        public StartEndTime endtime;
    }

    public class DateTimeJSON {
        public String value;
        public Boolean dateOnly;
        public Integer timeZoneShift;
    }

    public class StartEndTime {
        public DateTimeJSON dateTimeJSON;
    }
}