/**
* @author       Sebastian Łasisz
* @description  Class to send Workshop Reminder to High Schools (triggered from schedule)
**/
public class EmailManager_WorkshopReminderSchool {

    @TestVisible private static final String WORKFLOW_TRIGGER_NAME = 'WORKSHOP_PROPOSAL_SCHOOL';
    @TestVisible private static final String WORKSHOP_REMINDER_DEVELOPER_NAME = 'Mail_do_szk_o_terminie';

    public static Boolean sendWorkshopProposal(Set<Id> eventIds) {
        List<Event> highSchoolEventList = [
                SELECT Id, University__c, WhatId
                FROM Event
                WHERE Id IN :eventIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)
        ];

        EventManager.rewriteHighSchoolsEmail(highSchoolEventList);

        try {
            update highSchoolEventList;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

        Set<Id> highSchoolIds = new Set<Id>();
        for (Event highSchoolEvent : highSchoolEventList) {
            highSchoolIds.add(highSchoolEvent.WhatId);
        }

        Map<Id, Account> highSchoolsMap = new Map<Id, Account>([
                SELECT Id, Contact_Person__c
                FROM Account
                WHERE Id IN :highSchoolIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Account', CommonUtility.ACCOUNT_RT_HIGHSCHOOL)
        ]);

        List<EmailTemplate> emailTemplates = [
                SELECT Id, Name
                FROM EmailTemplate
                WHERE DeveloperName = :WORKSHOP_REMINDER_DEVELOPER_NAME
        ];

        EmailTemplate emailTemplate = emailTemplates.get(0);

        List<Task> tasksToInsert = new List<Task>();
        List<Event> eventWithFailues = new List<Event>();
        Map<Id, String> recordIdToWorkflowTriggerName = new Map<Id, String>();

        for (Event highSchoolEvent : highSchoolEventList) {
            if (WORKFLOW_TRIGGER_NAME != null) {
                recordIdToWorkflowTriggerName.put(highSchoolEvent.Id,  WORKFLOW_TRIGGER_NAME);

                String emailTemplateId = emailTemplate.Id;

                tasksToInsert.add(EmailManager.createEmailTask(
                        Label.msg_sentWorkshopSchoolReminder,
                        highSchoolEvent.WhatId,
                        highSchoolsMap.get(highSchoolEvent.WhatId).Contact_Person__c,
                        EmailManager.prepareFakeEmailBody(emailTemplateId, highSchoolEvent.WhatId),
                        null
                ));
            } else {
                eventWithFailues.add(highSchoolEvent);
            }
        }

        try {
            insert tasksToInsert;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }

        if (!recordIdToWorkflowTriggerName.isEmpty()) {
            EmailHelper.invokeWorkflowEmail(recordIdToWorkflowTriggerName);
        }


        if (!eventWithFailues.isEmpty()) {
            ErrorLogger.msg(CommonUtility.WORKFLOW_DESIGNATION_FAILED + ' ' + EmailManager_WorkshopProposal.class.getName()
                    + ' ' + JSON.serialize(eventWithFailues));
            return false;
        }

        return true;
    }

}