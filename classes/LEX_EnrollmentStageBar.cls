/**
 * Created by Sebastian.Lasisz on 17.04.2018.
 */

public with sharing class LEX_EnrollmentStageBar {

    @AuraEnabled
    public static String getBackgroundColor(Id recordId) {
        String backgroundColor;

        Enrollment__c enrollment = [
                SELECT Id, Synchronization_Status__c
                FROM Enrollment__c
                WHERE Id = :recordId
        ];

        if (enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_NEW || enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_MODIFIED) {
            backgroundColor = 'border-bottom: 2px solid #FFFFFF';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_INPROGRESS) {
            backgroundColor = 'border-bottom: 2px solid #D7F26D';
        }
        else if(enrollment.Synchronization_Status__c == CommonUtility.SYNCSTATUS_FINISHED) {
            backgroundColor = 'border-bottom: 2px solid #52A849';
        }
        else {
            backgroundColor = 'border-bottom: 2px solid #D84141';
        }

        return backgroundColor;
    }

    @AuraEnabled
    public static Enrollment__c getEnrollment(Id recordId) {
        return [
                SELECT Id, Status__c, Unenrolled__c, Unenrolled_Status__c, WasIs_Unenrolled__c, ToLabel(Synchronization_Status__c)
                FROM Enrollment__c
                WHERE Id = :recordId
        ];
    }

    @AuraEnabled
    public static List<StageWrapper> getList() {
        Map<String, String> statusMap = CommonUtility.getPicklistLabelMap(Enrollment__c.Status__c);

        List<StageWrapper>  retList = new List<StageWrapper> ();
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED), 'icons/stage1.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_CONFIRMED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_CONFIRMED), 'icons/like.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_COD, statusMap.get(CommonUtility.ENROLLMENT_STATUS_COD), 'icons/file.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_DC, statusMap.get(CommonUtility.ENROLLMENT_STATUS_DC), 'icons/tasks.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_ACCEPTED, statusMap.get(CommonUtility.ENROLLMENT_STATUS_ACCEPTED), 'icons/potwierdzony.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, statusMap.get(CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT), 'icons/handshake.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS, statusMap.get(CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS), 'icons/book.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS, statusMap.get(CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS), 'icons/coin.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_STATUS_RESIGNATION, statusMap.get(CommonUtility.ENROLLMENT_STATUS_RESIGNATION), 'icons/stage5.png'));

        return retList;
    }

    @AuraEnabled
    public static  List<StageWrapper> getUnenrList() {
        Map<String, String> statusMap = CommonUtility.getPicklistLabelMap(Enrollment__c.Status__c);

        List<StageWrapper>  retList = new List<StageWrapper> ();
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_COD, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_COD), 'icons/file.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_SIGNED_CONTRACT), 'icons/handshake.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_DIDACTICS_KS), 'icons/book.png'));
        retList.add(new StageWrapper(CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS, statusMap.get(CommonUtility.ENROLLMENT_UNENR_STATUS_PAYMENT_KS), 'icons/coin.png'));

        return retList;
    }


    private class StageWrapper {
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String url { get; set; }

        public StageWrapper(String value, String label, String url) {
            this.value = value;
            this.label = label;
            this.url = url;
        }
    }

}