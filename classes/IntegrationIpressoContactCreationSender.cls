/**
*   @author         Sebastian Łasisz
*   @description    This class is used to create new Contact in iPresso
**/

global without sharing class IntegrationIpressoContactCreationSender {
    public static Map<String, String> countryMap;

    @future(callout=true)
    public static void sendIPressoContactListAtFuture(Set<Id> contactsToSendIds) {
        IntegrationIpressoContactCreationSender.sendIPressoContactList(contactsToSendIds);
    }

    public static Boolean sendIPressoContactList(Set<Id> contactsToSendIds) {       
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

        String jsonToSend = prepareIPressoContactList(contactsToSendIds);

        if (jsonToSend != null) {

            String endPoint = esb.Create_iPresso_Contact_End_Point__c;
                    
            HttpRequest httpReq = new HttpRequest();
            String esbIp = esb.Service_Url__c;

            httpReq.setEndpoint(esbIp + endPoint);
            httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
            httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
            httpReq.setBody(jsonToSend);

            Http http = new Http();
            HTTPResponse httpRes = http.send(httpReq);

            Boolean success = httpRes.getStatusCode() == 200;

            List<Contact_AckReceiver_JSON> iPressoContacts = new List<Contact_AckReceiver_JSON>();
            try {
                iPressoContacts = parse(httpRes.getBody());  
            }
            catch (Exception e) {
                //ErrorLogger.log(e);
            }
            
            List<Marketing_Campaign__c> contactsToUpdateWithIPressoId = new List<Marketing_Campaign__c>();
            for (Contact_AckReceiver_JSON iPressoContact : iPressoContacts) {
                contactsToUpdateWithIPressoId.add(
                    new Marketing_Campaign__c(
                        Id = iPressoContact.crm_contact_id,
                        iPressoId__c = iPressoContact.ipresso_contact_id
                    )
                );
            }

            System.debug(iPressoContacts);

            try {
                update contactsToUpdateWithIPressoId;
            }
            catch (Exception e) {
                //ErrorLogger.log(e);
            }

            return success; 
        }

        return true;
    }

    public static String prepareIPressoContactList(Set<Id> contactsToSendIds) {        
        countryMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Country_of_Origin__c);

        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name, Active_To__c, Active_From__c, iPressoId__c, ADO__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
            AND Send_To_iPresso__c = true
            AND IPressoId__c != null
        ];

        List<Contact> contactsToSend = [
            SELECT Id, FirstName, LastName, Email, MobilePhone, MailingCity, MailingPostalCode, MailingStreet, MailingCountry, Account.Name, MailingState,
                   Nationality__c, University_for_sharing__c, Areas_of_Interest__c, Year_of_Graduation__c, A_Level_Issue_Date__c, Position__c, AccountId,
                   School__c, School_Type__c, School_Name__c, School__r.Name, School__r.School_Type__c, Statuses__c,
                   Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c,
                   Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c,Consent_Marketing_ADO__c,
            (SELECT Id, Lead_Email__c FROM iPressoContacts__r WHERE iPressoId__c = null),
            (SELECT Id, Degree__c, Mode__c, Course_or_Specialty_Offer__r.Trade_Name__c, Specialization__c, Specialization__r.Trade_Name__c, WSB_graduate__c FROM Enrollments_Studies__r ORDER BY CreatedDate DESC LIMIT 1),
            (SELECT Id, Date_of_Graduation__c, Thesis_Defense_Date__c FROM EducationalAgreements__r ORDER BY CreatedDate ASC LIMIT 1)
            FROM Contact
            WHERE Id IN :contactsToSendIds
        ];

        /* Needed to retrieve additional data for university name */
        Map<Id, Contact> contactsWithUniversityMap = new Map<Id, Contact>([
            SELECT Id, University_for_sharing__c,
            (SELECT Id, Degree__c, University_Name__c, Status__c FROM Enrollments_Studies__r ORDER BY CreatedDate DESC),
            (SELECT Id, University_Name__c, Status__c FROM EducationalAgreements__r ORDER BY CreatedDate DESC),
            (SELECT Id, Training_Offer_for_Participant__r.University_Name__c FROM Participants__r ORDER BY CreatedDate DESC),
            (SELECT Id, Lead_UniversityWSB__c FROM ReturnedLeads__r ORDER BY CreatedDate DESC)
            FROM Contact
            WHERE Id IN :contactsToSendIds
        ]);

        List<ContactIPresso_JSON> contactsToSendToIPresso = new List<ContactIPresso_JSON>();
        for (Contact contactToSend : contactsToSend) {
            for (Marketing_Campaign__c iPressoContact : contactToSend.iPressoContacts__r) {
                ContactIPresso_JSON iPressoContactToSend = new ContactIPresso_JSON();
                iPressoContactToSend.crm_contact_id = iPressoContact.Id;
                iPressoContactToSend.first_name = contactToSend.FirstName;
                iPressoContactToSend.last_name = contactToSend.LastName;
                iPressoContactToSend.email = iPressoContact.Lead_Email__c;
                iPressoContactToSend.crm_contact_status = contactToSend.Statuses__c;
                iPressoContactToSend.mobile = contactToSend.MobilePhone;
                if (contactToSend.AccountId != null && !contactToSend.Account.Name.contains('Migracja') && !contactToSend.Account.Name.contains(CommonUtility.INDIVIDUAL_CONTACT_ACCOUNT)) {
                    iPressoContactToSend.company = contactToSend.Account.Name;
                }
                iPressoContactToSend.city = contactToSend.MailingCity;
                iPressoContactToSend.postal_code = contactToSend.MailingPostalCode;
                iPressoContactToSend.street = contactToSend.MailingStreet;
                iPressoContactToSend.country = countryMap.get(contactToSend.MailingCountry);
                iPressoContactToSend.state = contactToSend.MailingState;

                iPressoContactToSend.consents = prepareConsentsForContact(contactToSend, catalogConsentsToValidate);  
                iPressoContactToSend.nationality = splitAdditionalInfoField(contactToSend.Nationality__c);       

                Enrollment__c studyEnrWithResignation = prepareDepartmentForIPresso(iPressoContactToSend, contactsWithUniversityMap.get(contactToSend.Id));

                iPressoContactToSend.area_of_interest = splitAdditionalInfoField(contactToSend.Areas_of_Interest__c);
                iPressoContactToSend.a_level_exam_year = contactToSend.Year_of_Graduation__c;
                iPressoContactToSend.school_name = contactToSend.School__c == null ? contactToSend.School_Name__c : contactToSend.School__c;
                iPressoContactToSend.school_type = contactToSend.School__c == null ? IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.School_Type__c) : IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.School__r.School_Type__c);
                iPressoContactToSend.position = IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.Position__c);

                if (!contactToSend.Enrollments_Studies__r.isEmpty()) {
                    if (studyEnrWithResignation == null) {
                        iPressoContactToSend.degree = prepareDegree(prepareDegreeFromEnrollment(contactsWithUniversityMap.get(contactToSend.Id)));
                    }
                    else {
                        iPressoContactToSend.interested_degree = studyEnrWithResignation.Degree__c;
                    }

                    iPressoContactToSend.mode = IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(contactToSend.Enrollments_Studies__r.get(0).Mode__c);
                    iPressoContactToSend.product_name = contactToSend.Enrollments_Studies__r.get(0).Course_or_Specialty_Offer__r.Trade_Name__c;
                    if (contactToSend.Enrollments_Studies__r.get(0).Specialization__c != null) {
                        iPressoContactToSend.postgraduate_product_name = contactToSend.Enrollments_Studies__r.get(0).Specialization__r.Trade_Name__c;
                    }
                    iPressoContactToSend.WSB_graduate = contactToSend.Enrollments_Studies__r.get(0).WSB_graduate__c;
                }

                if (!contactToSend.EducationalAgreements__r.isEmpty()) {
                    if (contactToSend.EducationalAgreements__r.get(0).Date_of_Graduation__c != null) {
                        iPressoContactToSend.study_graduation_year = String.valueOf(contactToSend.EducationalAgreements__r.get(0).Date_of_Graduation__c.year());
                    }

                    iPressoContactToSend.defence_date = String.valueOf(contactToSend.EducationalAgreements__r.get(0).Thesis_Defense_Date__c);
                }

                contactsToSendToIPresso.add(iPressoContactToSend);
            }
        }

        if (contactsToSendToIPresso.size() > 0) {
           return JSON.serialize(contactsToSendToIPresso);
        }
        else {
            return null;
        }
    }

    public static String prepareDegree(String degree) {
        if (degree == CommonUtility.OFFER_DEGREE_I) {
            return 'studia-i-stopnia';
        }
        else if (degree == CommonUtility.OFFER_DEGREE_II) {
            return 'studia-ii-stopnia';
        }
        else if (degree == CommonUtility.OFFER_DEGREE_MBA) {
            return 'studia-mba';
        }
        else if (degree == CommonUtility.OFFER_DEGREE_PG) {
            return 'studia-podyplomowe';
        }
        else if (degree == CommonUtility.OFFER_DEGREE_U) {
            return 'studia-jednolitego-magisterskie';
        }
        else if (degree == CommonUtility.OFFER_DEGREE_II_PG) {
            return 'studia-ii-stopnia';
        }

        return null;
    }

    public static String parseDepartmentForIpresso(String department) {
        Map<String, String> entityList = new Map<String, String> {
           RecordVals.WSB_NAME_GDA => 'gdansk',
           RecordVals.WSB_NAME_GDY => 'gdynia',
           RecordVals.WSB_NAME_POZ => 'poznan',
           RecordVals.WSB_NAME_TOR => 'torun',
           RecordVals.WSB_NAME_WRO => 'wroclaw',
           RecordVals.WSB_NAME_BYD => 'bydgoszcz',
           RecordVals.WSB_NAME_OPO => 'opole',
           RecordVals.WSB_NAME_SZC => 'szczecin',
           RecordVals.WSB_NAME_CHO => 'chorzow'
        };

        return entityList.get(department);
    }

    public static String prepareDegreeFromEnrollment(Contact candidate) {
        if (!candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    return studyEnr.Degree__c;
                }
            }
        }

        return null;
    }

    public static Enrollment__c prepareDepartmentForIPresso(ContactIPresso_JSON iPressoContactToSend, Contact candidate) {
        Boolean departmentFound = false;
        Enrollment__c enrollmentWithResignation = null;

        if (!candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c != CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    iPressoContactToSend.department = parseDepartmentForIpresso(studyEnr.University_Name__c);
                    departmentFound = true;
                    break;
                }
            }
        }

        if (!departmentFound && !candidate.EducationalAgreements__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.EducationalAgreements__r.get(0).University_Name__c);
            departmentFound = true;
        }

        if (!departmentFound && !candidate.Enrollments_Studies__r.isEmpty()) {
            for (Enrollment__c studyEnr : candidate.Enrollments_Studies__r) {
                if (studyEnr.Status__c == CommonUtility.ENROLLMENT_STATUS_RESIGNATION) {
                    iPressoContactToSend.department = parseDepartmentForIpresso(studyEnr.University_Name__c);
                    departmentFound = true;
                    enrollmentWithResignation = studyEnr;
                    break;
                }
            }
        }

        if (!departmentFound && !candidate.Participants__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.Participants__r.get(0).Training_Offer_for_Participant__r.University_Name__c);
            departmentFound = true;
        }

        if (!departmentFound && !candidate.ReturnedLeads__r.isEmpty()) {
            iPressoContactToSend.department = parseDepartmentForIpresso(candidate.ReturnedLeads__r.get(0).Lead_UniversityWSB__c);
            departmentFound = true;
        }

        if (!departmentFound && candidate.University_for_sharing__c != null) {
            List<String> university = candidate.University_for_sharing__c.split(', ');
            iPressoContactToSend.department = parseDepartmentForIpresso(university.get(university.size() - 1));
            departmentFound = true;
        }

        return enrollmentWithResignation;
    }

    public static List<MarketingConsents_JSON> prepareConsentsForContact(Contact contactWithConsent, List<Catalog__c> catalogConsentsToValidate) {
        List<MarketingConsents_JSON> consentsForContact = new List<MarketingConsents_JSON>();

        for (Catalog__c catalogConsent : catalogConsentsToValidate) {
            if (contactWithConsent != null && CatalogManager.getApiADONameFromConsentName(catalogConsent.Name) != null 
                && contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)) != null) {
                
                Set<String> contactAcceptedADO = CommonUtility.getOptionsFromMultiSelect((String)contactWithConsent.get(CatalogManager.getApiADONameFromConsentName(catalogConsent.Name)));
                Set<String> catalogSelectedADO = CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c);
                consentsForContact.add(new MarketingConsents_JSON(catalogConsent.IPressoId__c, contactAcceptedADO.containsAll(catalogSelectedADO))); 
            }   
            else {
                consentsForContact.add(new MarketingConsents_JSON(catalogConsent.IPressoId__c, false)); 
            }  
        }

        return consentsForContact;
    }

    private static List<String> splitAdditionalInfoField(String fieldValue) {
        if (fieldValue == null) {
            return new List<String>();
        }

        List<String> splitVals = new List<String>();
        for (String splitVal : fieldValue.split(';')) {
            splitVals.add(IntegrationPicklistDictionaryHelper.removeSpecialCharactersFromString(splitVal));
        }
        
        return splitVals;
    }
    
    private static List<Contact_AckReceiver_JSON> parse(String jsonBody) {
        return (List<Contact_AckReceiver_JSON>) System.JSON.deserialize(jsonBody, List<Contact_AckReceiver_JSON>.class);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    global class ContactIPresso_JSON {
        public String crm_contact_id;
        public String first_name;
        public String last_name;
        public String email;
        public String mobile;
        public String company;
        public String position;
        public String city;
        public String crm_contact_status;
        public String postal_code;
        public String street;
        public String country;
        public String state;
        public List<String> nationality;
        public String department;
        public List<String> area_of_interest;
        public String a_level_exam_year;
        public String degree;
        public String interested_degree;
        public String mode;
        public String product_name;
        public String postgraduate_product_name;
        public Boolean WSB_graduate;
        public String school_type;
        public String school_name;
        public String study_graduation_year;
        public String defence_date;
        public List<MarketingConsents_JSON> consents;
    }
    
    public class MarketingConsents_JSON {
        public String ipresso_consent_id;
        public Boolean consent_value;

        public MarketingConsents_JSON(String ipresso_consent_id, Boolean consent_value) {
            this.ipresso_consent_id = ipresso_consent_id;
            this.consent_value = consent_value;
        }
    }

    public class Contact_AckReceiver_JSON {
        public String crm_contact_id;
        public String ipresso_contact_id;
        public String status_code;
    }
}