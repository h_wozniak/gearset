global class DocGen_GenerateDiscountsOnEnrollment implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateDiscountsOnEnrollment() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateDiscountsOnEnrollment.generateDiscountsOnEnrollment(objectId, templateId); 
    }

    global static String generateDiscountsOnEnrollment(String objectId, String templateId) {
        List<Discount__c> discountsOnEnrollment = [
            SELECT Trade_Name__c, Discount_Kind__c, Discount_Value__c, Enrollment__r.Unenrolled__c, Discount_Type_Studies__c,
                   English_Trade_Name__c, Russian_Trade_Name__c
            FROM Discount__c 
            WHERE Enrollment__c = :objectId
        ];

        String enrollmentId = objectId;
        String language;

        if (discountsOnEnrollment.size() == 0) {
            Enrollment__c enr = [SELECT Enrollment_from_Documents__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id =: objectId];
            language = enr.Language_of_Main_Enrollment__c;

            discountsOnEnrollment = [
                SELECT Trade_Name__c, Discount_Kind__c, Discount_Value__c, Enrollment__r.Unenrolled__c, Discount_Type_Studies__c,
                       English_Trade_Name__c, Russian_Trade_Name__c
                FROM Discount__c 
                WHERE Enrollment__c = :enr.Enrollment_from_Documents__c 
                AND Applied__c = true
                AND Applies_to__c = :CommonUtility.DISCOUNT_APPLIESTO_TUITION
            ];
            enrollmentId = enr.Enrollment_from_Documents__c;
        }

        
        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(language);
        String result = '';
        Integer ilvl_val;
        Integer numId_val;

        enxoodocgen__Document_Template__c template = [
            SELECT Name
            FROM enxoodocgen__Document_Template__c 
            WHERE Id = :templateId 
        ];
        
        if (template.Name.contains('[PG]')) {
            ilvl_val = 3;
            numId_val = 9;
        }
        if (template.Name.contains('[Wyższe]')) {
            ilvl_val = 0;
            numId_val = 19;
        }
        if (template.Name.contains('[MBA]')) {
            ilvl_val = 3;
            numId_val = 4;
        }
        if (template.Name.contains('[Umowa wolny słuchacz]')) {
            ilvl_val = 3;
            numId_val = 4;
        }

        for (Discount__c discountOnEnrollment : discountsOnEnrollment) {
            String discountTradeName;
            if (language == CommonUtility.ENROLLMENT_LANGUAGE_POLISH) {
                discountTradeName = discountOnEnrollment.Trade_Name__c;
            }
            else if (language == CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                discountTradeName = discountOnEnrollment.English_Trade_Name__c;
            }
            else if (language == CommonUtility.ENROLLMENT_LANGUAGE_RUSSIAN) {
                discountTradeName = discountOnEnrollment.Russian_Trade_Name__c;
            }


            if (discountOnEnrollment.Discount_Kind__c == CommonUtility.DISCOUNT_KIND_AMOUNT) {
                result += '<w:p w:rsidR="00965AEB" w:rsidRPr="008B5D5A" w:rsidRDefault="008B5D5A" w:rsidP="000D077D"><w:pPr><w:pStyle w:val="Standard"/><w:numPr><w:ilvl w:val="'+ilvl_val+'"/><w:numId w:val="'+numId_val+'"/></w:numPr><w:spacing w:before="0" w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00D0293A"><w:rPr><w:i/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>nazwa: ' + discountTradeName + '; ' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_InTheAmountOf') + ' ' + Integer.valueOf(discountOnEnrollment.Discount_Value__c) + ' zł</w:t></w:r></w:p>';
            }
            else {
                result += '<w:p w:rsidR="00965AEB" w:rsidRPr="008B5D5A" w:rsidRDefault="008B5D5A" w:rsidP="000D077D"><w:pPr><w:pStyle w:val="Standard"/><w:numPr><w:ilvl w:val="'+ilvl_val+'"/><w:numId w:val="'+numId_val+'"/></w:numPr><w:spacing w:before="0" w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00D0293A"><w:rPr><w:i/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>nazwa: ' + discountTradeName + '; ' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_InTheAmountOf') + ' ' + Integer.valueOf(discountOnEnrollment.Discount_Value__c) + '%</w:t></w:r></w:p>';
            }
        }

        return result != '' ? result : '<w:p><w:pPr><w:pStyle w:val="Standard"/><w:autoSpaceDE w:val="0"/><w:spacing w:before="0" w:after="0" w:line="240" w:lineRule="auto"/><w:ind w:left="280" w:firstLine="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_NotApplicable') + '</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p>';
    }
}