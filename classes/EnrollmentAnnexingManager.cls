/**
* @author       Wojciech Słodziak, Sebastian Łasisz
* @description  Class for storing methods previous to Enrollment__c object for Study Annexing processes
*               Code is this class could be easily compared to Mom's spaghetti so be careful while making changes
**/

public without sharing class EnrollmentAnnexingManager {



    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------- ENROLLMENT ANNEXING PROCESS ----------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /* Wojciech Słodziak */
    // method for creating new Enrollment (Annex) from existing Enrollment
    // returns id of newly created Enrollment
    public static Id createAnnex(Id previousEnrId) {
        Enrollment__c previousEnr = [
            SELECT Id, Status__c, Candidate_Student__c, Educational_Advisor__c, Source_Studies__c, VerificationDate__c, 
            Starting_Semester__c, Dean_s_Decision_Date__c, Discount_Code__c, Previous_Enrollment__c, Enrollment_Source__c, Enrollment_Date__c,
            WSB_Graduate__c, Educational_Agreement__c, Finished_University__c, Price_Book_from_Enrollment__c, Tuition_System__c, Unenrolled_Status__c,
            Installments_per_Year__c, Unenrolled__c, Second_Course__c, Specialization__c, Guided_group__c, First_Name_for_Invoice__c, Dont_send_automatic_emails__c,
            Last_Name_for_Invoice__c, Street_for_Invoice__c, Postal_Code_for_Invoice__c, City_for_Invoice__c, Company_Name__c, 
            Country_for_Invoice__c, NIP_for_Invoice__c, The_Same_Data_as_on_Company__c, The_Invoice_after_Payment__c, Comments_on_Invoice__c, 
            Course_or_Specialty_Offer__c, Do_not_set_up_Student_Card__c, RecordTypeId, Confirmation_Date__c, Collection_of_Documents_Date__c,
            Resignation_Comments__c, Resignation_Reason_List__c, Documents_Collected_Date__c, Initial_Specialty_Declaration__c, Enrollment_from_Language__c,
            Consent_Date_processing_for_contract__c, Consent_Direct_Communications__c, Consent_Electronic_Communication__c, Consent_Graduates__c,
            Consent_Marketing__c, Consent_Terms_of_Service_Acceptation__c
            FROM Enrollment__c 
            WHERE Id = :previousEnrId
        ];

        Enrollment__c newEnr = previousEnr.clone(false, true, false, false);

        // required to trigger initial logic
        newEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED;

        // if Enrollment was from ZPI copy VerificationDate__c, which is required to determine Document Delivery Deadlines
        // if Enrollment was from CRM VerificationDate__c will be null, and CreatedDate of previous Enrollment should be treated as date to determine Deadlines 
        newEnr.VerificationDate__c = (previousEnr.VerificationDate__c != null? previousEnr.VerificationDate__c : previousEnr.Enrollment_Date__c);

        newEnr.Previous_Enrollment__c = previousEnrId;

        // without try catch and ErrorLogger because this method is encapsulated elsewhere
        //CommonUtlity.skipCreatingCampaignMember = true;
        insert newEnr;
        //CommonUtlity.skipCreatingCampaignMember = false;

        //EnrollmentAnnexingManager.createAnnex_CopypreviousRecords(newEnr, previousEnr);

        //EnrollmentAnnexingManager.createAnnex_FinalizationHelper(newEnr, previousEnr);

        //PaymentScheduleManager.generatePaymentSchedules(new Set<Id> { newEnr.Id });


        return newEnr.Id;
    }

    public static Id finalizeAnnex(Id previousEnrId, Id newEnrId) {
        Enrollment__c previousEnr = [
            SELECT Id, Status__c, Candidate_Student__c, Educational_Advisor__c, Source_Studies__c, VerificationDate__c, Unenrolled_Status__c,
            Starting_Semester__c, Dean_s_Decision_Date__c, Discount_Code__c, Previous_Enrollment__c, Enrollment_Source__c, Enrollment_Date__c,
            WSB_Graduate__c, Educational_Agreement__c, Finished_University__c, Price_Book_from_Enrollment__c, Tuition_System__c, 
            Installments_per_Year__c, Unenrolled__c, Second_Course__c, Specialization__c, Guided_group__c, First_Name_for_Invoice__c, 
            Last_Name_for_Invoice__c, Street_for_Invoice__c, Postal_Code_for_Invoice__c, City_for_Invoice__c, Company_Name__c, 
            Country_for_Invoice__c, NIP_for_Invoice__c, The_Same_Data_as_on_Company__c, The_Invoice_after_Payment__c, Comments_on_Invoice__c, 
            Course_or_Specialty_Offer__c, Do_not_set_up_Student_Card__c, RecordTypeId, Confirmation_Date__c, Collection_of_Documents_Date__c,
            Resignation_Comments__c, Resignation_Reason_List__c, Documents_Collected_Date__c, Initial_Specialty_Declaration__c, Enrollment_from_Language__c,
            Consent_Date_processing_for_contract__c, Consent_Direct_Communications__c, Consent_Electronic_Communication__c, Consent_Graduates__c,
            Consent_Marketing__c, Consent_Terms_of_Service_Acceptation__c
            FROM Enrollment__c 
            WHERE Id = :previousEnrId
        ];

        Enrollment__c newEnr = [
            SELECT Id, Status__c, Candidate_Student__c, Educational_Advisor__c, Source_Studies__c, VerificationDate__c, Degree__c, Unenrolled_Status__c,
            Starting_Semester__c, Dean_s_Decision_Date__c, Discount_Code__c, Previous_Enrollment__c, Enrollment_Source__c, Enrollment_Date__c,
            WSB_Graduate__c, Educational_Agreement__c, Finished_University__c, Price_Book_from_Enrollment__c, Tuition_System__c, 
            Installments_per_Year__c, Unenrolled__c, Second_Course__c, Specialization__c, Guided_group__c, First_Name_for_Invoice__c, 
            Last_Name_for_Invoice__c, Street_for_Invoice__c, Postal_Code_for_Invoice__c, City_for_Invoice__c, Company_Name__c, 
            Country_for_Invoice__c, NIP_for_Invoice__c, The_Same_Data_as_on_Company__c, The_Invoice_after_Payment__c, Comments_on_Invoice__c, 
            Course_or_Specialty_Offer__c, Do_not_set_up_Student_Card__c, RecordTypeId, Confirmation_Date__c, Collection_of_Documents_Date__c,
            Resignation_Comments__c, Resignation_Reason_List__c, Documents_Collected_Date__c, Initial_Specialty_Declaration__c, Enrollment_from_Language__c,
            Consent_Date_processing_for_contract__c, Consent_Direct_Communications__c, Consent_Electronic_Communication__c, Consent_Graduates__c,
            Consent_Marketing__c, Consent_Terms_of_Service_Acceptation__c
            FROM Enrollment__c 
            WHERE Id = :newEnrId
        ];

        EnrollmentAnnexingManager.createAnnex_CopypreviousRecords(newEnr, previousEnr);

        EnrollmentAnnexingManager.createAnnex_FinalizationHelper(newEnr, previousEnr, null);

        PaymentScheduleManager.generatePaymentSchedules(new Set<Id> { newEnr.Id });

        return newEnr.Id;
    }

    /* Wojciech Słodziak */
    // helper for createAnnex method
    // method for copying previous records from previous Enrollment if creating Annex (NOT Change Offer process)
    private static void createAnnex_CopypreviousRecords(Enrollment__c newEnr, Enrollment__c previousEnr) {
        // languages
        List<Enrollment__c> langList = [
            SELECT Id, RecordTypeId, Foreign_Language__c, Language_from_Enrollment_Language__c, Enrollment_from_Language__c
            FROM Enrollment__c
            WHERE Enrollment_from_Language__c = :previousEnr.Id
        ];

        List<Enrollment__c> langListToInsert = new List<Enrollment__c>();
        for (Enrollment__c lang : langList) {
            Enrollment__c clone = lang.clone(false, true, false);
            clone.Enrollment_from_Language__c = newEnr.Id;

            langListToInsert.add(clone);
        }

        if (!langListToInsert.isEmpty()) {
            insert langListToInsert;
        }
    }

    /* Wojciech Słodziak */
    // helper for createAnnex method and EnrollmentStudyController
    // method for executing logic that is required for both standard Annex and Offer Change Annex
    public static void createAnnex_FinalizationHelper(Enrollment__c newEnr, Enrollment__c previousEnr, List<Enrollment__c> languages) {
        // languages
        if (languages != null) {
            insert languages;
        }

        // discounts
        List<Discount__c> discountList = [
            SELECT Id, Enrollment__c, Discount_from_Discount_Connector__c, Applied__c, Discount_Value__c, Discount_Kind__c, 
            Applies_to__c, Applied_through__c, RecordTypeId, Discount_Type_Studies__c, Discount_Type__c, Name, Trade_Name__c, Enrollment__r.Language_of_Enrollment__c
            FROM Discount__c
            WHERE Enrollment__c = :previousEnr.Id
        ];

        List<Discount__c> discountListToInsert = new List<Discount__c>();
        for (Discount__c discount : discountList) {
            Discount__c clone = discount.clone(false, true, false);
            clone.Enrollment__c = newEnr.Id;

            discountListToInsert.add(clone);
        }

        if (!discountListToInsert.isEmpty()) {
            insert discountListToInsert;
        } else {
            DiscountManager_Studies.determineAutomaticDiscountsOnEnrollment(new Set<Id> { newEnr.Id });
        }
        
        // documents         
        Id acceptanceDocumentPLId = CatalogManager.getDocumentAcceptancePLId();
        Id acceptanceDocumentId = CatalogManager.getDocumentAcceptanceId();
        Id unenrolledAgreementId = CatalogManager.getDocumentAgreementUnenrolledId();
        Id unenrolledAgreementPLId = CatalogManager.getDocumentAgreementUnenrolledPLId();
         
        Map<Id, Enrollment__c> docListToUpdate = new Map<Id, Enrollment__c>();

        List<Enrollment__c> previousEnrDocList;

        Enrollment__c studyEnrWithDegree = [SELECT Id, Degree__c, Course_or_Specialty_Offer__r.Degree__c FROM Enrollment__c WHERE Id = :newEnr.Id];

        if (studyEnrWithDegree.Degree__c == CommonUtility.OFFER_DEGREE_PG || studyEnrWithDegree.Course_or_Specialty_Offer__r.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            previousEnrDocList = [
                SELECT Id, Document__c, Document_Accepted__c, Acceptance_Date__c, Language_of_Enrollment__c
                FROM Enrollment__c 
                WHERE Enrollment_from_Documents__c = :previousEnr.Id 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
                AND Document_Accepted__c = true 
                AND (
                    For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_COD
                    OR For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_COD_CON
                    OR For_delivery_in_Stage__c = null
                )
                AND Document__c != :acceptanceDocumentPLId
                AND Document__c != :acceptanceDocumentId
                AND Document__c != :unenrolledAgreementId
                AND Document__c != :unenrolledAgreementPLId
            ];
        }
        else {
            previousEnrDocList = [
                SELECT Id, Document__c, Document_Accepted__c, Acceptance_Date__c, Language_of_Enrollment__c
                FROM Enrollment__c 
                WHERE Enrollment_from_Documents__c = :previousEnr.Id 
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
                AND Document_Accepted__c = true 
                AND (
                    For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_COD
                    OR For_delivery_in_Stage__c = :CommonUtility.ENROLLMENT_FDI_STAGE_COD_CON
                    OR For_delivery_in_Stage__c = null
                )
                AND Document__c != :unenrolledAgreementId
                AND Document__c != :unenrolledAgreementPLId
            ];
        }

        List<Enrollment__c> newEnrDocList = [
            SELECT Id, Document__c, Document_Accepted__c, Acceptance_Date__c, For_delivery_in_Stage__c, Language_of_Enrollment__c
            FROM Enrollment__c 
            WHERE Enrollment_from_Documents__c = :newEnr.Id 
            AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
        ];

        Boolean anyDocDelivered = false;
        Boolean allDocDelivered = true;
        for (Enrollment__c newEnrDoc : newEnrDocList) {
            for (Enrollment__c previousEnrDoc : previousEnrDocList) {
                if (previousEnrDoc.Document__c == newEnrDoc.Document__c && previousEnrDoc.Document_Accepted__c) {
                    newEnrDoc.Document_Accepted__c = previousEnrDoc.Document_Accepted__c;
                    newEnrDoc.Acceptance_Date__c = previousEnrDoc.Acceptance_Date__c;
                    docListToUpdate.put(newEnrDoc.Id, newEnrDoc);

                    if (newEnrDoc.For_delivery_in_Stage__c == CommonUtility.ENROLLMENT_FDI_STAGE_COD) {
                       anyDocDelivered = true;
                    }
                }
            }
            if (!newEnrDoc.Document_Accepted__c && newEnrDoc.For_delivery_in_Stage__c == CommonUtility.ENROLLMENT_FDI_STAGE_COD) {
                allDocDelivered = false;
            }
        }
        
        newEnr.Documents_Collected_Date__c = allDocDelivered ? previousEnr.Documents_Collected_Date__c : null;

        update docListToUpdate.values();

        // update previous Enrollment with lookup to new Enrollment
        previousEnr.Annex_or_Related_Enr__c = newEnr.Id;

        // if reason of Annexing is Study Offer Change or Specialty/Specialization and Agreement wasn't signed yet then
        if (!previousEnr.Unenrolled__c) {
            if (!EnrollmentManager_Studies.finalizedEnrStatusesForContact.contains(previousEnr.Status__c)) {
                previousEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
                previousEnr.Resignation_Date__c = System.today();
                previousEnr.Resignation_Reason_List__c = CommonUtility.ENROLLMENT_RESREASON_CCHANGE;
            } else { // perform standard annexing process
                previousEnr.Annexing__c = true;
            }
        }
        else {
            if (!EnrollmentManager_Studies.finalizedEnrStatusesForContact.contains(previousEnr.Unenrolled_Status__c)) {
                previousEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
                previousEnr.Resignation_Date__c = System.today();
                previousEnr.Resignation_Reason_List__c = CommonUtility.ENROLLMENT_RESREASON_CCHANGE;
            }
            else {
                previousEnr.Annexing__c = true;
            }
        }

        update previousEnr;

        // lock previous Enrollment from editing
        try {
            Approval.lock(previousEnr);
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        // select status on newEnr
        if (allDocDelivered) {
            newEnr.Status__c = (previousEnr.Dean_s_Decision_Date__c != null? CommonUtility.ENROLLMENT_STATUS_ACCEPTED : CommonUtility.ENROLLMENT_STATUS_DC);
        } else {
            if (anyDocDelivered) {
                newEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_COD;
            }
        }

        newEnr.Last_Active_Status__c = newEnr.Status__c;
        newEnr.Discount_Code__c = previousEnr.Discount_Code__c;  
              
        if (studyEnrWithDegree.Degree__c == CommonUtility.OFFER_DEGREE_PG || studyEnrWithDegree.Course_or_Specialty_Offer__r.Degree__c == CommonUtility.OFFER_DEGREE_PG) {
            newEnr.Dean_s_Decision_Date__c = null;
        }

        CommonUtility.skipCodeDiscountApplication = true;
        update newEnr;
        CommonUtility.skipCodeDiscountApplication = false;

        // generate agreement and oath manually (trigger is disabled for previous Enrollments)
        Enrollment__c studyEnrToCreateDocuments = [
            SELECT Id, Degree__c, University_Name__c, OwnerId, Language_of_Enrollment__c, Candidate_Student__c
            FROM Enrollment__c
            WHERE Id = :newEnr.Id
        ];
        
        if (!Test.isRunningTest()) {
            GenerateMissingAttachmentsOnEnrollment.generateAttachmentForDocuments(new List<Enrollment__c> { studyEnrToCreateDocuments });
        }
        // generate acceptance decision if previousEnrollment was on Accepted stage
        if (previousEnr.Dean_s_Decision_Date__c != null && studyEnrWithDegree.Degree__c != CommonUtility.OFFER_DEGREE_PG 
            && studyEnrWithDegree.Course_or_Specialty_Offer__r.Degree__c != CommonUtility.OFFER_DEGREE_PG) {
            DocumentManager.createAcceptanceDecisionForEnrollment(new List<Enrollment__c> { studyEnrToCreateDocuments });
        }
    }

    /* Wojciech Słodziak */
    // method for finalizing Annexing process
    // sets Resignation on previous Enrollment after Annex is signed
    // updates Didactics record with modified parameters
    public static void finalizeAnnexingProcess(Set<Id> finAnnexIds) {
        List<Enrollment__c> finalizedAnnexList = [
            SELECT Id, Previous_Enrollment__c, Educational_Agreement__c
            FROM Enrollment__c
            WHERE Id IN :finAnnexIds
        ];

        Set<Id> previousEnrIds = new Set<Id>();
        Set<Id> eduAgrIds = new Set<Id>();
        for (Enrollment__c finalizedAnnex : finalizedAnnexList) {
            previousEnrIds.add(finalizedAnnex.Previous_Enrollment__c);
            eduAgrIds.add(finalizedAnnex.Educational_Agreement__c);
        }

        List<Enrollment__c> previousEnrList = [
            SELECT Id, Status__c, Annexing__c
            FROM Enrollment__c
            WHERE Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION AND Annexing__c = true
            AND Id IN :previousEnrIds
        ];

        for (Enrollment__c previousEnr : previousEnrList) {
            previousEnr.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
            previousEnr.Resignation_Date__c = System.today();
            previousEnr.Resignation_Reason_List__c = CommonUtility.ENROLLMENT_RESREASON_ANNEXING;
            previousEnr.Annexing__c = false;
        }

        try {
            CommonUtility.skipEnrollmentEditDisabling = true;
            update previousEnrList;
            CommonUtility.skipEnrollmentEditDisabling = false;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        Set<Id> annexesAlreadyInKS = new Set<Id>();

        // update Educational Agreement Record with annexed data
        Map<Id, Enrollment__c> eduAgrMap = new Map<Id, Enrollment__c>([
            SELECT Id, Source_Enrollment__c
            FROM Enrollment__c 
            WHERE Id IN :eduAgrIds
        ]);

        for (Enrollment__c finalizedAnnex : finalizedAnnexList) {
            Enrollment__c eduAgr = eduAgrMap.get(finalizedAnnex.Educational_Agreement__c);

            if (eduAgr != null) {
                eduAgr.Source_Enrollment__c = finalizedAnnex.Id;
                annexesAlreadyInKS.add(finalizedAnnex.Id);
            }
        }

        try {
            update eduAgrMap.values();
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        if (!annexesAlreadyInKS.isEmpty()) {
            // call Batch to update Educational Agreements
            Database.executeBatch(new EducationalAgreementUpsertBatch(annexesAlreadyInKS, false, false), 1);

            // call Experia synchronization - will be called with above batch
            IntegrationEnrollmentHelper.updateSyncStatus_inProgress(true, annexesAlreadyInKS, '', false, false);
        }
    }

    /* Wojciech Słodziak */
    // method for resigning from Annexing process
    // method reverts changes to original Enrollment process 
    public static void resignFromAnnexingProcess(Set<Id> resignedAnnexes) {
        List<Enrollment__c> previousEnrList = [
            SELECT Id, Annexing__c, Annex_or_Related_Enr__c
            FROM Enrollment__c
            WHERE Annex_or_Related_Enr__c IN :resignedAnnexes
            AND Status__c != :CommonUtility.ENROLLMENT_STATUS_RESIGNATION
        ];

        for (Enrollment__c previousEnr : previousEnrList) {
            previousEnr.Annexing__c = false;
            previousEnr.Annex_or_Related_Enr__c = null;
        }

        try {
            update previousEnrList;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        // ulock previous Enrollments for editing
        try {
            Approval.unlock(previousEnrList, false);
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }
}