/**
*   @author        Sebastian Łasisz
*   @description   Controller for CampaignMember page thats responsbile for filtering list of contacts that are being added to given campaign.
*                  Responsible for preparing iPresso campaign sales.
**/

public with sharing class CampaignMembersController {

    public List<Contact> contacts = new List<Contact>();
    public List<ContactWrapper> contactsToShow { get; set; }
    public List<FilterWrapper> filters { get; set; }
    public List<Selectoption> fieldsOnObject { get; set; }
    public List<Selectoption> possibleOperators { get; set; }
    public List<SelectOption> possibleOperatorsString { get; set; }
    public List<Selectoption> possibleOperatorsDate { get; set; }
    public List<SelectOption> possibleOperatorsBoolean { get; set; }
    
    public Boolean assignFilters { get; set; }
    public Boolean assignAdvancedLogic { get; set; }
    public String assigmentContactsLogic { get; set; }
    public Boolean selectAll { get; set; }
    public String query { get; set; }
    public Id recordId { get; set; }
    public Id campaignId { get; set; }

    public Marketing_Campaign__c marketingCampaignToAddContacts;
    public String retURL;
    public Integer filterCounter { get; set; }
    public String filterCurrentCounter { get; set; }

    public Integer counter = 0;
    public Integer limitSize { get; set; }
    public Integer totalSize = 0;
    
    /** 
     *  @author Sebastian Lasisz
     *
     *  Constructor of controller. Method is responsible for setting basic parameters so that page can display proper data.
     *  First of all it resets visibily on all field columns except first one. 
     *  Then it queries iPresso Contacts that are already added to that campaign (to remove the need of checking duplicates in either system).
     *  After that operators are being prepared (static list of data) based on field type.
     *  In the end first part of contacts are queried and then trimmed to proper records. 
     *  After that only small batch of contacts is selected to be displayed on first page.
    **/
    public CampaignMembersController () {    
        limitSize = 10;   
        filterCurrentCounter = '0';
        assignAdvancedLogic = false;
        assigmentContactsLogic = '';
        
        String campaignId = ApexPages.currentPage().getParameters().get('campaignId');
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        if (campaignId == null || campaignId == '') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing campaign id'));
            return;
        }
        else {
            marketingCampaignToAddContacts = [
                SELECT Id, Name, Campaign_Kind__c
                FROM Marketing_Campaign__c 
                WHERE Id = :campaignId
            ];
        }

        filterCounter = 0;
        filters = new List<FilterWrapper>();

        possibleOperators = prepareOperatorOptionList();
        possibleOperatorsString = prepareOperatorOptionStringList();
        possibleOperatorsDate = prepareOperatorOptionDateList();
        possibleOperatorsBoolean = prepareOperatorOptionBooleanList();

        fieldsOnObject = getAvailableFields('Contact', true);
        contactsToShow = new List<ContactWrapper>();

        contacts = reapplyContactList([
            SELECT Id, Name, Account.Name, Phone, Statuses__c ,
            (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
            FROM Contact
            WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
            AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :campaignId) 
            AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
            LIMIT :limitSize
            OFFSET 0
        ]);

        recordId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS);
        totalSize = getContactsForMaxLimist(query, marketingCampaignToAddContacts.Id, recordId).size();

        if ((counter + limitSize) <= totalSize) {
            for(Integer i = 0; i < limitSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        } else {
            for(Integer i = 0; i < totalSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        }
    }   

    @RemoteAction @ReadOnly
    public static List<Contact> getContactsForMaxLimist(String query, Id campaignId, Id recordId) {
        List<Contact> contacts = new List<Contact>();
        if (query == null) {
            contacts = [
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :campaignId) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT 10000
            ];

            List<Contact> contactsToShowTmp = new List<Contact>();
            for (Contact c : contacts) {
                for (Marketing_Campaign__c contactIPresso : c.iPressoContacts__r) {
                    if (contactIPresso.iPressoId__c != null) {
                        contactsToShowTmp.add(c);
                        break;
                    }
                }
            }

            return contactsToShowTmp;
        }
        else {
            contacts = Database.query(query);

            List<Contact> contactsToShowTmp = new List<Contact>();
            for (Contact c : contacts) {
                for (Marketing_Campaign__c contactIPresso : c.iPressoContacts__r) {
                    if (contactIPresso.iPressoId__c != null) {
                        contactsToShowTmp.add(c);
                        break;
                    }
                }
            }

            return contactsToShowTmp;
        } 

        return contacts;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used resets applied advanced logic and shows default list of contacts based on limit size.
    **/
    public void beginning() {     
        List<Contact> contacts = new List<Contact>();
        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET 0
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET 0'));
        } 

        contactsToShow.clear();
        counter = 0;
        if ((counter + limitSize) <= totalSize) {
            for (Integer i = 0; i < limitSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }   
           
        } else {       
            for(Integer i = 0; i < totalSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }  
        }
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used gets contact list from next offset based on limit size and total size of queried records.
    **/   
    public void next() {
        contactsToShow.clear();
        counter = counter + limitSize;   

        List<Contact> contacts = new List<Contact>();
        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET :counter
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET :counter'));
        } 

        if (limitSize <= contacts.size()) {
            for(Integer i = 0; i < limitSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        } else {
            for(Integer i = 0; i < contacts.size(); i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        }
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used gets contact list from previous offset based on limit size and total size of queried records.
    **/
    public void previous() {   
        contactsToShow.clear();
        counter = counter - limitSize;      
        if (counter < 0) counter = 0;

        List<Contact> contacts = new List<Contact>();
        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET :counter
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET :counter'));
        } 
       
        for (Integer i = 0; i < contacts.size(); i++) {
            String emails = '';
            for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                emails += contactIPresso.Lead_Email__c + '; ';
            }
            contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
        }
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used gets contact list from last offset based on limit size and total size of queried records.
    **/
    public void last() {
        contactsToShow.clear();

        List<Contact> contacts = new List<Contact>();
        counter = totalSize - limitSize;

        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET :counter
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET :counter'));
        } 
       
        for (Integer i = 0; i < contacts.size(); i++) {
            String emails = '';
            for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                emails += contactIPresso.Lead_Email__c + '; ';
            }
            contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
        }       
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used creates additional filter wrapper for additional filter logic. Filter wrapper on creation is filled with default values:
     *  - Index number (based on quantity of elements already added)
     *  - Field - first field taken from object (sorted alphabetically)
     *  - Operator - first operator based on predefiened list
     *  - Value - empty field that's type is based on Field value
    **/
    public void addFilterWrapper () {
        filters.add(new FilterWrapper(++filterCounter, fieldsOnObject.get(0).getValue(), getTypeOfField(fieldsOnObject.get(0).getValue())));
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used removes Filter Wrapper based on index number. During operation all field levels are recalculated to show only neccessary columns. 
    **/
    public PageReference deleteLogic() {
        filters.remove(Integer.valueOf(filterCurrentCounter) - 1);

        filterCounter = 0;
        for(FilterWrapper fw : filters){
            fw.counter = ++filterCounter;
        }      

        return null;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Action launched from the button on visual force layout. 
     *  When used selects all listed contacts and updates ContactWrappers to store which contacts are selected.
    **/
    public void selectAllContacts() {
        for (ContactWrapper contactToShow : contactsToShow) {
            contactToShow.selected = selectAll;
        }
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method prepares general list of SelectOption. 
     *  List contains as value English Values (ComonUtiltiy), where as Label contains translated values based on Labels.
     *
     *  @return list of available general operators.
    **/
    public List<SelectOption> prepareOperatorOptionList() {
        List<SelectOption> operatorList = new List<Selectoption>();
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_EQUALS, Label.field_label_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_EQUALS, Label.field_label_not_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_STARTS_WITH, Label.field_label_starts_with));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_CONTAINS, Label.field_label_contains));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_CONTAINS, Label.field_label_not_contains));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_GREATER_THAN, Label.field_label_greater_than));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_LESS_THAN, Label.field_label_less_than));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_GREATER_THAN_OR_EQUAL, Label.field_label_greater_than_or_equal));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_LESS_THAN_OR_EQUAL, Label.field_label_less_than_or_equal));

        return operatorList;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method prepares list of SelectOption available for String field.
     *  List contains as value English Values (ComonUtiltiy), where as Label contains translated values based on Labels.
     *
     *  @return list of available operators for String field.
    **/
    public List<SelectOption> prepareOperatorOptionStringList() {
        List<SelectOption> operatorList = new List<Selectoption>();
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_EQUALS, Label.field_label_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_EQUALS, Label.field_label_not_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_STARTS_WITH, Label.field_label_starts_with));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_CONTAINS, Label.field_label_contains));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_CONTAINS, Label.field_label_not_contains));

        return operatorList;        
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method prepares list of SelectOption available for Date field.
     *  List contains as value English Values (ComonUtiltiy), where as Label contains translated values based on Labels.
     *
     *  @return list of available operators for Date field.
    **/
    public List<SelectOption> prepareOperatorOptionDateList() {
        List<SelectOption> operatorList = new List<Selectoption>();
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_EQUALS, Label.field_label_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_EQUALS, Label.field_label_not_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_GREATER_THAN, Label.field_label_greater_than));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_LESS_THAN, Label.field_label_less_than));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_GREATER_THAN_OR_EQUAL, Label.field_label_greater_than_or_equal));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_LESS_THAN_OR_EQUAL, Label.field_label_less_than_or_equal));

        return operatorList;        
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method prepares list of SelectOption available for Boolean field.
     *  List contains as value English Values (ComonUtiltiy), where as Label contains translated values based on Labels.
     *
     *  @return list of available operators for Boolean field.
    **/
    public List<SelectOption> prepareOperatorOptionBooleanList() {
        List<SelectOption> operatorList = new List<Selectoption>();
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_EQUALS, Label.field_label_equals));
        operatorList.add(new SelectOption(CommonUtility.CMC_OPERATOR_NOT_EQUALS, Label.field_label_not_equals));

        return operatorList;        
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method prepares list of SelectOption for given object name and ignoreReference variable. Based on ignoreReference list is properly parsed.
     *
     *  @param objectName       Name of the object that will be used to retrieve all fields.
     *  @param ignoreReferences Boolean value whether references should be ignored. 
     *                          If its set to false, field names having reference type will have '      >' added to their name.
     *  @return list of available fields for given object name.
    **/
    public List<SelectOption> getAvailableFields (String objectName, Boolean ignoreReferences){
        Map<String, String> mapFieldNames = new Map<String, String>();  
        List<SelectOption> result = new List<SelectOption>();
        if(objectName != null && objectName != '' && objectName != '-'){
            Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
            for (String fieldName : fieldsMap.keySet()){
                Schema.DescribeFieldResult fieldDescribe = fieldsMap.get(fieldName).getDescribe();
                
                if(fieldDescribe.isUpdateable()) {
                    result.add(new SelectOption(fieldName, fieldDescribe.getLabel()));
                }
            
                mapFieldNames.put(objectName + '___' + fieldName, fieldDescribe.getLabel());
            }
        }
        sortSelectOptionsByLabel(result);
        return result;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method sorts List of SelectOption based on labels.
     *
     *  @param opts List of SelectOption that needs to be sorted.
    **/
    public void sortSelectOptionsByLabel (List<SelectOption> opts) {
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        Integer suffix = 1;
        
        for (Selectoption opt : opts) {
            mapping.put((opt.getLabel() + suffix++), opt);
        }

        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        opts.clear();

        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }

    /**
     * @author Sebastian Lasisz
     *  
     * Method prepares dynamic columns for each wrapper for each level of depth (childs).
     * 
     * @return PageReference to the page user is currently on.
     */
    public PageReference refreshValueField() {
        FilterWrapper current = filters.get(Integer.valueOf(filterCurrentCounter) - 1);
        //current.contactWithFieldTypes = new Contact();
        current.contactWithFieldTypes.put(current.field, null);
        current.fieldType = getTypeOfField(current.field);

        return null;
    }

    /**
     * @author Sebastian Lasisz
     * 
     * Method that replace word written operators in mathematical values. 
     * 
     * @param  operator String value of operator from SelectOption list.
     * @return          Proper boolean operator that is responsible for parsing query.
     */    
    public String stringOperatorToQueryOperator(String operator) {
        if (operator == CommonUtility.CMC_OPERATOR_EQUALS) {
            return '=';
        }
        else if (operator == CommonUtility.CMC_OPERATOR_NOT_EQUALS) {
            return '!=';
        }
        else if (operator == CommonUtility.CMC_OPERATOR_GREATER_THAN) {
            return '>';
        }
        else if (operator == CommonUtility.CMC_OPERATOR_LESS_THAN) {
            return '<';
        }
        else if (operator == CommonUtility.CMC_OPERATOR_GREATER_THAN_OR_EQUAL) {
            return '>=';
        }
        else if (operator == CommonUtility.CMC_OPERATOR_LESS_THAN_OR_EQUAL) {
            return '<=';
        }
        return '=';
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method that takes all filter wrappers and prepares Map<Integer, String> of values that are used in building query to filter contacts.
     *  
     * @return Map<Integer, String> containing index number of filter logic and it's field name with operator and value. 
     *         Used to define proper query on object.
     */
    public Map<Integer, String> prepareFiltersMap() {
        Map<Integer, String> filtersToMap = new Map<Integer, String>();
        for (FilterWrapper filter : filters) {           

            if (filter.fieldType == CommonUtility.CMC_DATE) {
                try {
                    Date myDate = (Date)filter.contactWithFieldTypes.get(filter.field);
                    myDate = Date.newinstance(myDate.year(), myDate.month(), myDate.day());
                    filter.value = String.valueOf(myDate);
                }
                catch (Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_DateCantBeEmpty));
                }
            }
            else {
                filter.value = String.valueOf(filter.contactWithFieldTypes.get(filter.field));
                if ((filter.fieldType == CommonUtility.CMC_STRING || filter.fieldType == CommonUtility.CMC_PICKLIST || filter.fieldType == CommonUtility.CMC_MULTIPICKLIST || filter.fieldType == CommonUtility.CMC_REFERENCE) 
                    && filter.value == null) {
                    filter.value = '';
                }
            }

            if (filter.operator == CommonUtility.CMC_OPERATOR_CONTAINS) {
                if (filter.fieldType == CommonUtility.CMC_MULTIPICKLIST) {
                    filtersToMap.put(filter.counter, filter.field + ' includes (\'' + filter.value + '\')');
                }
                else {
                    filtersToMap.put(filter.counter, filter.field + ' LIKE \'%' + filter.value + '%\'');
                }
            }
            else if(filter.operator == CommonUtility.CMC_OPERATOR_NOT_CONTAINS) {
                if (filter.fieldType == CommonUtility.CMC_MULTIPICKLIST) {
                    filtersToMap.put(filter.counter, filter.field + ' excludes (\'' + filter.value + '\')');
                }
                else {
                    filtersToMap.put(filter.counter, '\\(NOT ' + filter.field + ' LIKE \'%' + filter.value + '%\'' + '\\)');
                }
            }
            else if (filter.operator == CommonUtility.CMC_OPERATOR_STARTS_WITH) {
                filtersToMap.put(filter.counter, filter.field + ' LIKE \'' + filter.value + '%\'');
            }
            else {
                if (filter.fieldType == CommonUtility.CMC_STRING || filter.fieldType == CommonUtility.CMC_PICKLIST || filter.fieldType == CommonUtility.CMC_MULTIPICKLIST || filter.fieldType == CommonUtility.CMC_REFERENCE) {
                    filtersToMap.put(filter.counter, filter.field + ' ' + stringOperatorToQueryOperator(filter.operator) + ' ' + '\'' + filter.value + '\'');
                }
                else {
                    filtersToMap.put(filter.counter, filter.field + ' ' + stringOperatorToQueryOperator(filter.operator) + ' ' + filter.value);
                }
            }
        }

        return filtersToMap;
    }

    /**
     * @author Sebastian Lasisz
     *
     * Method responsible for creating advanced filter logic (if there wasnt one defenied by user) or preparing proper boolean expression that could be parsed.
     * 
     * @return Proper boolean expresion to be parsed before selecting new set of contacts.
     */
    public String parseAssigmentContactsLogic() {
        Map<Integer, String> filters = prepareFiltersMap();

        String newAssigmentContactsLogic = assigmentContactsLogic;
        if (newAssigmentContactsLogic == null || newAssigmentContactsLogic == '') {
            newAssigmentContactsLogic += '(';
            for (Integer fw : filters.keySet()) {
                newAssigmentContactsLogic += String.valueOf(fw) + ' AND ';
            }
            newAssigmentContactsLogic = newAssigmentContactsLogic.left(newAssigmentContactsLogic.length() - 5) + ')';
        }

        newAssigmentContactsLogic = newAssigmentContactsLogic.replaceAll('\\(', ' \\( ');
        newAssigmentContactsLogic = newAssigmentContactsLogic.replaceAll('\\)', ' \\) ');

        List<String> filterLogicList = newAssigmentContactsLogic.split(' ');
        String changedAssigmentContactLogic = '';
        for (String filterLogic : filterLogicList) {
            if (filterLogic.isNumeric()) {
                changedAssigmentContactLogic += filters.get(Integer.valueOf(filterLogic)) + ' ';
            }
            else {
                changedAssigmentContactLogic += filterLogic + ' ';
            }
        }

        return changedAssigmentContactLogic;
    }

    /**
     * @author Sebastian Lasisz
     * 
     * Method that responds to action on visual force page. For given limit size resize list of visible contacts on page at once.
     */
    public void applyLimitSize() {
        contactsToShow = new List<ContactWrapper>();
        List<Contact> contacts = new List<Contact>();
        counter = 0;

        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET :counter
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET :counter'));
        }


        if (limitSize <= contacts.size()) {
            for(Integer i = 0; i < limitSize; i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        } else {
            for(Integer i = 0; i < contacts.size(); i++) {
                String emails = '';
                for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                    emails += contactIPresso.Lead_Email__c + '; ';
                }
                contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
            }
        }
    }

    /**
     * @author Sebastian Lasisz
     *  
     * Method parse list of contacts and removes from list ones that doesnt have attached iPresso Contacts with iPresso Id.
     * 
     * @param  listOfContacts list of queries contacts that needs to be parsed
     * @return                New list of contacts that match given criteria
     */
    public List<Contact> reapplyContactList(List<Contact> listOfContacts) {
        List<Contact> contactsToShowTmp = new List<Contact>();
        for (Contact c : listOfContacts) {
            for (Marketing_Campaign__c contactIPresso : c.iPressoContacts__r) {
                if (contactIPresso.iPressoId__c != null) {
                    contactsToShowTmp.add(c);
                    break;
                }
            }
        }

        return contactsToShowTmp;
    }
    
    /**
     * @author Sebastian Lasisz
     * 
     * Method that parse filter logic with defined advanced logic and reselects list of contacts thats avaiable for user to choose from.
     * 
     * @return PageReference containing either error or redirect to page on which user is currently on.
     */
    public PageReference applyAdvancedLogic() {
        String changedAssigmentContactLogic = parseAssigmentContactsLogic();

        Id contactRecordId = CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS);
        campaignId = marketingCampaignToAddContacts.Id;
        query = 'SELECT Id, Name, Account.Name, Phone, Statuses__c, (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r WHERE iPressoId__c != null) ' +
        		'FROM Contact ' +
        		'WHERE RecordTypeId != :contactRecordId' +
        			'AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :campaignId) ' +
        			'AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null) ' +
        			'AND ' + changedAssigmentContactLogic;

        contactsToShow = new List<ContactWrapper>();
        try {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET 0'));
            totalSize = getContactsForMaxLimist(query, marketingCampaignToAddContacts.Id, recordId).size();
    
            if ((counter + limitSize) <= totalSize) {
                for(Integer i = 0; i < limitSize; i++) {
                    String emails = '';
                    for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                        emails += contactIPresso.Lead_Email__c + '; ';
                    }
                    contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
                }
            } else {
                for(Integer i = 0; i < totalSize; i++) {
                    String emails = '';
                    for (Marketing_Campaign__c contactIPresso : contacts.get(i).iPressoContacts__r) {
                        emails += contactIPresso.Lead_Email__c + '; ';
                    }
                    contactsToShow.add(new ContactWrapper(false, contacts.get(i), emails));
                }
            }
        }
        catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_InvalidQuery));
        }
        
        return null;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method that goes over object for given field name and gives field type based on Schema.
     *
     *  @param fieldName    
     *  @return list of available fields for given object name.
    **/
    public String getTypeOfField(String fieldName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();     
        Schema.DisplayType fieldType = objectFields.get(fieldName).getDescribe().getType();

        return String.valueOf(fieldType);
    }

    public void refreshAssigmentLogic() {
        assigmentContactsLogic = '';
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method that goes over list of ContactWrapper and creates Marketing_Campaign__c records if records were selected.
     *  After creating list of Marketing_Campaign__c DML to insert records is performed.
     *  If everything went as expected user is redirected to campaign record.
     *
     *  @return PageReference to campaign page.
    **/
    public PageReference save() {
        List<Marketing_Campaign__c> campaignsToInsert = new List<Marketing_Campaign__c>();
        for (ContactWrapper contactWithWrapper : contactsToShow) {
            if (contactWithWrapper.selected) {            
                campaignsToInsert.add(IPressoManager.createCampaignMemberIPresso(marketingCampaignToAddContacts.Id, contactWithWrapper.contact.Id));
            }
        }

        try {
            insert campaignsToInsert;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }

//        if (retURL != null) {
//            return new PageReference(retURL);
//        }
        return null;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method that cancels adding contacts to campaign and redirects to previous page.
     *
     * @return PageReference to campaign (if campaign id was specified).
    **/
    public PageReference cancel() {
        if (retURL != null) {
            return new PageReference(retURL);
        }
        return null;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method that checks whether next button should be enabled.
    **/   
    public Boolean getDisableNext() {   
        Integer tmp_counter = counter + limitSize;
        List<Contact> contacts = new List<Contact>();

        if (query == null) {
            contacts = reapplyContactList([
                SELECT Id, Name, Account.Name, Phone, Statuses__c ,
                (SELECT Id, iPressoId__c, Lead_Email__c FROM iPressoContacts__r)
                FROM Contact
                WHERE RecordTypeId != :CommonUtility.getRecordTypeId('Contact', CommonUtility.CONTACT_RT_ANONYMOUS)
                AND Id NOT IN (SELECT Campaign_Contact__c FROM Marketing_Campaign__c WHERE Campaign_Member__c = :marketingCampaignToAddContacts.Id) 
                AND Id IN (SELECT Contact_from_iPresso__c FROM Marketing_Campaign__c WHERE iPressoId__c != null)
                LIMIT :limitSize
                OFFSET :tmp_counter
            ]);
        }
        else {
            contacts = reapplyContactList(Database.query(query + ' LIMIT :limitSize OFFSET :tmp_counter'));
        }

        System.debug('contacts ' + contacts.size());
        return contacts.isEmpty();
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  Method that checkes whether previous button should be enabled.
    **/
    public Boolean getDisablePrevious() {   
        return counter == 0;
    }

    /** 
     *  @author Sebastian Lasisz
     *
     *  ContactWrapper stores required information about contacts that must be added to campaign. 
    **/
    public class ContactWrapper {
        public Boolean selected { get; set; }
        public Contact contact { get; set; }
        public String email { get; set; }

        public ContactWrapper(Boolean itemSelected, Contact candidateToAdd, String iPressoEmail) {
            selected = itemSelected;
            contact = candidateToAdd;
            email = iPressoEmail;
        }
    } 

    /** 
     *  @author Sebastian Lasisz
     *
     *  FilterWrapper is inner class that stores all required information about every row for filtering table.
    **/
    public class FilterWrapper {
        public Integer counter { get; set; }
        public String field { get; set; }
        public String operator { get; set; }
        public String value { get; set; }
        public String fieldType { get; set; }
        public Contact contactWithFieldTypes { get; set; }

        public FilterWrapper (Integer counter, String field, String fieldType) {
            this.counter = counter;
            this.field = field;
            this.fieldType = fieldType;
            this.contactWithFieldTypes = new Contact();
            this.contactWithFieldTypes.put(field, null);
        }
    }
}