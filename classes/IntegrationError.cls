/**
* @author       Wojciech Słodziak
* @description  Class for maintaining/generating Error Codes related to Integration. 
**/

public without sharing class IntegrationError {



    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------- ERROR CODE DEFINITION -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private final static Map<Integer, String> errorCodeToMsgMap = new Map<Integer, String> {

        /* ---------------------------------- COMMON ERRORS (6xx) ------------------------------------- */

        600 => 'Unexpected error.',
        601 => 'JSON serialization error.',
        602 => 'JSON required field missing.',
        603 => 'Duplicate found.',
        604 => 'Invalid ID for object.',
        605 => 'Missing parameters.',
        606 => 'Business logic validation error.',


        /* ---------------------------------- ZPI FORM ERROS (7xx) ------------------------------------ */

        700 => 'JSON field value is invalid or missing.',
        701 => 'Product with given Name was not found.',
        702 => 'University Account with given ID was not found.',
        703 => 'Specialization with given Name was not found.',
        704 => 'Candidate for given ID was not found.',
        705 => 'High School for given Name was not found.',


        /* ------------------------------- PRODUCT PRICE ERROS (8xx) ---------------------------------- */

        800 => 'JSON field value is invalid or missing.',
        801 => 'Product for given Name was not found.',
        802 => 'PriceBook for given product and parameters was not found.',
        803 => 'Tuition system is not available for designated PriceBook.',
        804 => 'Installment variant is not defined for designated PriceBook.',
        805 => 'Code discount not found for given code.',
        806 => 'Company not found for given NIP number',


        /* ------------------------------- DICTIONARY ERROS (9xx) ---------------------------------- */

        900 => 'Dictionary with given ID was not found.',


        /* ------------------------------- DIDACTICS ERROS (10xx) ---------------------------------- */

        1000 => 'Didactics with given ID was not found.',
        1001 => 'Product with given Name was not found',


        /* ---------------------------- MARKETING CAMPAIGN ERROS (11xx) ------------------------------ */

        1100 => 'Campaign with given external ID was not found.',
        1101 => 'Invalid consent defitinition.',

        /* --------------------------------- CONTACT ERROS (12xx) ------------------------------------ */
        1200 => 'Contact with given parameters was not found.',

        /* --------------------------------- DOCUMENT ERROS (12xx) ------------------------------------ */
        1300 => 'Accepted documents cannot be delete.'
    };


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- INVOKE METHOD ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    public static String getErrorJSONByCode(Integer errorCode) {
        Error_JSON errObj = new Error_JSON();

        errObj.error_code = errorCode;
        errObj.message = errorCodeToMsgMap.get(errorCode);

        return JSON.serialize(errObj);
    }

    public static Blob getErrorJSONBlobByCode(Integer errorCode) {
        return Blob.valueOf(getErrorJSONByCode(errorCode));
    }

    public static String getErrorJSONByCode(Integer errorCode, String additonalData) {
        Error_JSON errObj = new Error_JSON();

        errObj.error_code = errorCode;
        errObj.message = errorCodeToMsgMap.get(errorCode) + ' [' + additonalData + ']';

        return JSON.serialize(errObj);
    }

    public static Blob getErrorJSONBlobByCode(Integer errorCode, String additonalData) {
        return Blob.valueOf(getErrorJSONByCode(errorCode, additonalData));
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    public class Error_JSON {
        public Integer error_code;
        public String message;
    }

    @TestVisible
    public class Success_JSON {
        public Integer status_code;
        public String message;
    }

}