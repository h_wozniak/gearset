public with sharing class LEX_Sync_Payment_Templates_Controller {
	
	@AuraEnabled
	public static Boolean hasEditAccess(String offerId){
		return LEXServiceUtilities.hasEditAccess(offerId);
	}

	@AuraEnabled
	public static Offer__c getObjectRecord(String offerId){
		return [SELECT Price_Book_Type__c, Synchronization_Status__c FROM Offer__c WHERE Id = :offerId LIMIT 1];
	}

	@AuraEnabled
	public static Boolean transferPaymentTemplatesToExperia(String offerId){
		return LEXServiceUtilities.transferPaymentTemplatesToExperia(offerId);
	}

}