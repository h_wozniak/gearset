@isTest
private class ZTestLEX_AddManualDiscount {



    @isTest static void testLEX_AddManualDiscount() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.LEX_AMD_prepareDataForTest(true, 100);

        List<Payment__c> payments = [SELECT Id FROM Payment__c];
        System.assert(payments.size() > 0);

        Test.stopTest();
        Discount__c newDiscount = LEX_AddManualDiscount.getNewDiscount(dataMap.get(3).Id);

        System.assertEquals(newDiscount.Enrollment__c, dataMap.get(3).Id);

        try {
            LEX_AddManualDiscount.saveRecord(newDiscount);
        }
        catch(Exception e) {
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            Boolean messageFound = false;
            Integer counter = 0;
            for(ApexPages.Message message : pageMessages) {
                if(message.getSummary() != ' ') {
                    messageFound = true;
                }
                counter ++;
            }
            System.assert(messageFound);
        }
    }

    @isTest static void testVISDiscountApplication() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.LEX_AMD_prepareDataForTest(true, 100);

        Discount__c newDiscount = LEX_AddManualDiscount.getNewDiscount(dataMap.get(3).Id);
        Test.stopTest();

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        newDiscount.Name = 'DiscountX';
        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_VIS;
        newDiscount.Discount_Value__c = 25;
        newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
        newDiscount.Active__c = true;
        LEX_AddManualDiscount.saveRecord(newDiscount);

        Enrollment__c studyEnr = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Discount__c discount = [
                SELECT Id, Discount_Value__c
                FROM Discount__c
                WHERE Id =: newDiscount.Id
        ];

        System.assert(valBefore * ((100 - discount.Discount_Value__c)/100) < studyEnr.Value_after_Discount__c);
    }

    @isTest static void testPararelDiscount() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.LEX_AMD_prepareDataForTest(true, 100);

        Discount__c newDiscount = LEX_AddManualDiscount.getNewDiscount(dataMap.get(3).Id);

        Test.stopTest();

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Enrollment__c studyEnrollment2 = new Enrollment__c();
        studyEnrollment2.Candidate_Student__c = ZDataTestUtility.createCandidateContact(null, true).Id;
        insert studyEnrollment2;

        newDiscount.Name = 'DiscountX';
        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
        newDiscount.Discount_Value__c = 25;
        newDiscount.Recommended_by__c = studyEnrollment2.Id;
        newDiscount.Discount_Kind__c = CommonUtility.DISCOUNT_KIND_PERCENTAGE;
        newDiscount.Active__c = true;

        LEX_AddManualDiscount.saveRecord(newDiscount);
    }

    @isTest static void testSecondCourseDiscountApplication() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.LEX_AMD_prepareDataForTest(true, 100);

        Test.stopTest();

        Discount__c newDiscount = LEX_AddManualDiscount.getNewDiscount(dataMap.get(3).Id);

        Enrollment__c studyEnrBefore = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        Decimal valBefore = studyEnrBefore.Value_after_Discount__c;

        newDiscount.Name = 'DiscountX';
        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_SECONDCOURSE;

        LEX_AddManualDiscount.fillDiscountInfo(newDiscount);

        LEX_AddManualDiscount.saveRecord(newDiscount);

        Enrollment__c studyEnr = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        System.assert(valBefore > studyEnr.Value_after_Discount__c);
    }


    @isTest static void testRecommendationDiscountApplication_same() {
        Map<Integer, sObject> dataMap = ZDataTestUtilityMethods.LEX_AMD_prepareDataForTest(true, 100);

        Test.stopTest();

        Discount__c newDiscount = LEX_AddManualDiscount.getNewDiscount(dataMap.get(3).Id);

        Enrollment__c studyEnr1Before = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        newDiscount.Name = 'DiscountX';
        newDiscount.Discount_Type_Studies__c = CommonUtility.DISCOUNT_TYPEST_RECPERSON;
        newDiscount.Recommended_by__c = dataMap.get(3).Id;

        LEX_AddManualDiscount.fillDiscountInfo(newDiscount);

        LEX_AddManualDiscount.saveRecord(newDiscount);

        Enrollment__c studyEnr1 = [
                SELECT Id, Value_after_Discount__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(3).Id
        ];

        System.assert(studyEnr1Before.Value_after_Discount__c == studyEnr1.Value_after_Discount__c);
    }
}