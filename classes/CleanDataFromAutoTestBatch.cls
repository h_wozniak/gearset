/**
* @author       Karolina Bolinger
* @description  Batch class for clean data created in automatic tests.  
**/

global class CleanDataFromAutoTestBatch implements Database.Batchable<sObject> {
	public static Map<String, TestContactToDelete__c> mapContactCustomSetting = TestContactToDelete__c.getAll();
	public static List<String> listOfContactsEmails = new List<String>();
	public static List<Enrollment__c> enrollmentsToDelete = new List<Enrollment__c>();
	public static List<Marketing_Campaign__c> iPressoConsToDelete = new List<Marketing_Campaign__c>();
	public static List<Contact> contactsToDelete = new List<Contact>();
	public static List<Id> contactsIDs = new List<Id>();
	
    global CleanDataFromAutoTestBatch(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    	for(TestContactToDelete__c a : mapContactCustomSetting.values()){
			listOfContactsEmails.add(a.Email__c);
		}
		
        return Database.getQueryLocator([
            SELECT Id, Email
            FROM Contact
            WHERE Email IN :listOfContactsEmails
         ]);
    }

    global void execute(Database.BatchableContext BC, List<Contact> contacts) {
        for (Contact con : contacts) {
        	contactsIDs.add(con.Id);
        } 
        //contacts scope
        contactsToDelete = [SELECT id FROM Contact
        					WHERE id IN :contactsIDs];
        					
        //iPresso scope
        iPressoConsToDelete = [SELECT id FROM Marketing_Campaign__c 
        					WHERE  Contact_from_iPresso__c IN :contactsIDs
        					AND RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_IPRESSO_CONTACT)];
        
        //enrollments - study
        enrollmentsToDelete = [SELECT id FROM Enrollment__c 
        					WHERE Candidate_Student__c = :contactsIDs
        					AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)];
        
    	try {
	    	if (iPressoConsToDelete != null || !iPressoConsToDelete.isEmpty()) {
	    		delete iPressoConsToDelete;
	    	}
	        if (enrollmentsToDelete != null || !enrollmentsToDelete.isEmpty()) {
	        	delete enrollmentsToDelete;
	        }
	        if (contactsToDelete != null || !contactsToDelete.isEmpty()) {
	        	delete contactsToDelete;
	        }
	    }
	     
    	catch (Exception e){
            ErrorLogger.log(e);
        } 
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}