public without sharing class TH_AccountRewrites extends TriggerHandler.DelegateBase {

    public override void beforeInsert(List<sObject> o) {
        for (sObject obj : o) {
            Account aNew = (Account)obj;

        }
    }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        for (Id key : old.keySet()) {
            Account aOld = (Account)old.get(key);
            Account aNew = (Account)o.get(key);

        }
    }
}