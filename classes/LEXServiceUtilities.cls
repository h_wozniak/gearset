/**
*   @author         Michał Bujak
*   @description    Utility class, that is used to store methods to be called from lightning components
**/

public with sharing class LEXServiceUtilities {

    public static Boolean hasEditAccess(Id recordId) {
        UserRecordAccess ura = [
            SELECT RecordId, HasEditAccess
            FROM UserRecordAccess 
            WHERE UserId = :UserInfo.getUserId() AND RecordId = :recordId
        ];
        return ura.HasEditAccess;
    }

    public static Boolean hasEditAccessList(List<Id> recordIdList) {
        Boolean hasAccess = true;

        List<UserRecordAccess> uraList = [
            SELECT RecordId, HasEditAccess 
            FROM UserRecordAccess 
            WHERE UserId = :UserInfo.getUserId() AND RecordId IN :recordIdList
        ];

        for (UserRecordAccess ura : uraList) {
            if (!ura.HasEditAccess) {
                hasAccess = false;
            }
        }

        return hasAccess;
    }
    
    public static Boolean hasCreateAccessToPriceBooks() {
        List<PermissionSetAssignment> psa = [
            SELECT Id  
            FROM PermissionSetAssignment 
            WHERE PermissionSet.Name = :CommonUtility.PERMISSION_SET_PRICEBOOK
            AND AssigneeId = :UserInfo.getUserId()
        ];

        return psa.size() > 0;        
    }

    public static Boolean updateFieldValues(String objId, String sObjectName, String[] fieldNames, String[] fieldTypes, String[] fieldValues) {
        Schema.SObjectType objSchema = Schema.getGlobalDescribe().get(sObjectName);
        sObject obj = objSchema.newSObject(objId);
        for (Integer i = 0; i < fieldNames.size(); i++) {
            if (fieldTypes[i].toLowerCase() == 'string') {
                obj.put(fieldNames[i], fieldValues[i]);
            } else if (fieldTypes[i].toLowerCase() == 'boolean') {
                obj.put(fieldNames[i], Boolean.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'integer') {
                obj.put(fieldNames[i], Integer.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'decimal') {
                obj.put(fieldNames[i], Decimal.valueOf(fieldValues[i]));
            } else if (fieldTypes[i].toLowerCase() == 'date') {
                if(fieldValues[i] == ''){
                    Date today = null;
                    obj.put(fieldNames[i], today);
                }
                else{
                    String todaysDate = fieldValues[i];
                    String[] dateComponents = todaysDate.split('\\.');
                    Date today = Date.newInstance(Integer.valueOf(dateComponents[2]), Integer.valueOf(dateComponents[1]), Integer.valueOf(dateComponents[0]));
                    obj.put(fieldNames[i], today);
                }
            }
        }
        try {
            update obj;
            return true;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
            return false;
        }
    }

    public static Boolean transferPaymentTemplatesToExperia(Id pbId) {
        Offer__c pb = [
            SELECT Active__c
            FROM Offer__c
            WHERE Id = :pbId
        ];

        if (pb.Active__c) {
            return IntegrationPaymentTemplateSender.sendPaymentTemplatesSync(new Set<Id> { pbId });
        }

        return false;
    }
}