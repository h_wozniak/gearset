global class DocGen_GenerateTuitionSystemUnenrolled implements enxoodocgen.DocGen_StringTokenInterface {

    public DocGen_GenerateTuitionSystemUnenrolled() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        return DocGen_GenerateTuitionSystemUnenrolled.generateStudyTuitionSystem(objectId);  
    }

    global static String generateStudyTuitionSystem(String objectId) {
        Enrollment__c enr = [SELECT ToLabel(Tuition_System__c), ToLabel(Enrollment_from_Documents__r.Tuition_System__c) FROM Enrollment__c WHERE Id = :objectId LIMIT 1];
        String tuition = '';

        if (enr.Tuition_System__c != null) {
            tuition = enr.Tuition_System__c;
        }
        else {
            tuition = enr.Enrollment_from_Documents__r.Tuition_System__c;
        }
        
        if (tuition == CommonUtility.ENROLLMENT_TUITION_SYSTEM_FIXED) {
            return '<w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>';
        }
        if (tuition == CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED) {
            return '<w:r><w:rPr><w:strike/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stałego/ </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> stopniowanego </w:t></w:r>';
        }
        return '';
    }
}