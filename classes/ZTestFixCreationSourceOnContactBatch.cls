@isTest
private class ZTestFixCreationSourceOnContactBatch {

	@isTest static void test_batch_crm() {
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

		// Force null value - ignore default value
		candidate.Creation_Source__c = null;
		update candidate;

        Test.startTest();
		Database.executeBatch(new FixCreationSourceOnContactBatch(CommonUtility.ENROLLMENT_ENR_SOURCE_CRM), 100);
        Test.stopTest();

        Contact contactAfterChanges = [
        	SELECT Id, Creation_Source__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(contactAfterChanges.Creation_Source__c, CommonUtility.ENROLLMENT_ENR_SOURCE_CRM);		
	}

	@isTest static void test_batch_zpi() {
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(
			new Enrollment__c(
				Candidate_Student__c = candidate.Id, 
				Enrollment_Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI,
				VerificationDate__c = System.today()
		), true);

		// Force null value - ignore default value
		candidate.Creation_Source__c = null;
		update candidate;

        Test.startTest();
		Database.executeBatch(new FixCreationSourceOnContactBatch(CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI), 100);
        Test.stopTest();

        Contact contactAfterChanges = [
        	SELECT Id, Creation_Source__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(contactAfterChanges.Creation_Source__c, CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI);		
	}

	@isTest static void test_batch_zpi2() {
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);

		// Force null value - ignore default value
		candidate.Creation_Source__c = null;
		update candidate;

        Test.startTest();
		Database.executeBatch(new FixCreationSourceOnContactBatch(CommonUtility.ENROLLMENT_ENR_SOURCE_ZPI), 100);
        Test.stopTest();

        Contact contactAfterChanges = [
        	SELECT Id, Creation_Source__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(contactAfterChanges.Creation_Source__c, null);		
	}

	@isTest static void test_batch_ipresso() {
		Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Source__c = CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO), true);

		// Force null value - ignore default value
		candidate.Creation_Source__c = null;
		update candidate;

        Test.startTest();
		Database.executeBatch(new FixCreationSourceOnContactBatch(CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO), 100);
        Test.stopTest();

        Contact contactAfterChanges = [
        	SELECT Id, Creation_Source__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(contactAfterChanges.Creation_Source__c, CommonUtility.ENROLLMENT_ENR_SOURCE_IPRESSO);		
	}

	@isTest static void test_batch_returnedLead() {
		Contact candidate = ZDataTestUtility.createCandidateContact(null, true);
		ZDataTestUtility.createReturnedLead(new Marketing_Campaign__c(Contact_From_Returning_Leads__c = candidate.Id), true);

		// Force null value - ignore default value
		candidate.Creation_Source__c = null;
		update candidate;

        Test.startTest();
		Database.executeBatch(new FixCreationSourceOnContactBatch(CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD), 100);
        Test.stopTest();

        Contact contactAfterChanges = [
        	SELECT Id, Creation_Source__c
        	FROM Contact
        	WHERE Id = :candidate.Id
        ];

        System.assertEquals(contactAfterChanges.Creation_Source__c, CommonUtility.ENROLLMENT_ENR_SOURCE_RETURNED_LEAD);		
	}
}