/**
*   @author         Grzegorz Długosz
*   @description    This class is used to request history of ineractions (calls) from FCC.
**/

global without sharing class IntegrationFCCInteractionReceiver {
       
    @future(callout=true)
    public static void sendInteractionRequestAsync() {
        IntegrationFCCInteractionReceiver.sendInteractionRequestFinal();
    }

    public static Boolean sendInteractionRequestSync() {
        return IntegrationFCCInteractionReceiver.sendInteractionRequestFinal();
    }

    // final method for sending callout
    private static Boolean sendInteractionRequestFinal() {
        HttpRequest httpReq = new HttpRequest();
        
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createImportCallsEndPoint = esb.Create_Import_Calls_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createImportCallsEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = IntegrationFCCInteractionHelper.buildInteractionImportJson();
        httpReq.setBody(jsonToSend);
        
        Boolean success = false;
        
        if (!Test.isRunningTest()) {
            Http http = new Http();
            HTTPResponse httpRes = http.send(httpReq);
    
            success = (httpRes.getStatusCode() == 200);
    
            if (!success) {
                ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationFCCInteractionHelper.class.getName() 
                    + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n');
            }
            else {
                String jsonBody =  httpRes.getBody();
                //IntegrationFCCInteractionHelper.receiveAndUpdateInteractions(jsonBody);
            }
        }

        return success;
    }

}