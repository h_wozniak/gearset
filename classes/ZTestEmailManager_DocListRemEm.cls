@isTest
private class ZTestEmailManager_DocListRemEm {

    @isTest static void test_sendRequiredDocumentListReminder_I() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);

        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);

        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                Enrollment_from_Documents__c = enrollment.Id,
                For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Test.stopTest();

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(2, enrollmentList[0].RequiredDocListEmailedCount__c);


        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentListSecondTime = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(3, enrollmentListSecondTime[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_II() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_II,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);

        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 1
        ), true);
        Test.stopTest();

        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = enrollment.Id,
            For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);
        
        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(2, enrollmentList[0].RequiredDocListEmailedCount__c);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentListSecondTime = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(3, enrollmentListSecondTime[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_PG() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_PG,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);

        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = courseOffer.Id,
                RequiredDocListEmailedCount__c = 1,
                Do_not_set_up_Student_Card__c = true
        ), true);
        Test.stopTest();

        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = enrollment.Id,
            For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(2, enrollmentList[0].RequiredDocListEmailedCount__c);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentListSecondTime = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(3, enrollmentListSecondTime[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_MBA() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_MBA,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);

        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = courseOffer.Id,
                RequiredDocListEmailedCount__c = 1,
                Do_not_set_up_Student_Card__c = true
        ), true);

        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
                Enrollment_from_Documents__c = enrollment.Id,
                For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Test.stopTest();

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentList = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(2, enrollmentList[0].RequiredDocListEmailedCount__c);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Enrollment__c> enrollmentListSecondTime = [
            SELECT Id, RequiredDocListEmailedCount__c 
            FROM Enrollment__c 
            WHERE Id IN :enrollmentIdList
        ];
        System.assertEquals(3, enrollmentListSecondTime[0].RequiredDocListEmailedCount__c);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(!taskList.isEmpty());
    }

    @isTest static void test_sendRequiredDocumentListReminder_Fail() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Offer__c universityOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I,
            University__c = ZDataTestUtility.createUniversity(new Account(
                Name = RecordVals.WSB_NAME_WRO
            ), true).Id
        ), true);

        Test.startTest();
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = universityOffer.Id
        ), true);

        Enrollment__c enrollment = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
            Course_or_Specialty_Offer__c = courseOffer.Id,
            RequiredDocListEmailedCount__c = 3
        ), true);
        Test.stopTest();

        Enrollment__c document = ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(
            Enrollment_from_Documents__c = enrollment.Id,
            For_delivery_in_Stage__c = CommonUtility.ENROLLMENT_FDI_STAGE_COD
        ), true);

        Set<Id> enrollmentIdList = new Set<Id>();
        enrollmentIdList.add(enrollment.Id);

        EmailManager_DocumentListReminderEmail.sendRequiredDocumentListReminder(enrollmentIdList);

        List<Task> taskList = [SELECT Id FROM Task];
        System.assert(taskList.isEmpty());
    }
}