/**
*   @author         Sebastian Łasisz112345
*   @description    schedulable class that runs batches daily to create send Lack Of Documents Activity to iPresso
**/
    
global class ActivityLackOfDocumentsBatach implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    Datetime dayOfEnrollmentFrom;
    Datetime dayOfEnrollmentTo;

    global ActivityLackOfDocumentsBatach(Integer daysAfterEnrollment) {
        Date dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
        dayOfEnrollmentFrom = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 0, 0, 0);
        dayOfEnrollmentTo = Datetime.newInstanceGMT(dayOfEnrollment.year(), dayOfEnrollment.month(), dayOfEnrollment.day(), 23, 59, 59);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Candidate_Student__c, Status__c
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            AND Confirmation_Date__c >= :dayOfEnrollmentFrom AND Confirmation_Date__c <= :dayOfEnrollmentTo
            AND (((Degree__c = :CommonUtility.OFFER_DEGREE_I OR Degree__c = :CommonUtility.OFFER_DEGREE_U) AND Candidate_Student__r.A_Level__c != :CommonUtility.CONTACT_ALEVEL_WAITING_FOR_RESULT)
            OR (Degree__c = :CommonUtility.OFFER_DEGREE_MBA OR Degree__c = :CommonUtility.OFFER_DEGREE_II OR Degree__c = :CommonUtility.OFFER_DEGREE_II_PG OR Degree__c = :CommonUtility.OFFER_DEGREE_PG))
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        ActivityManager.lackOfDocumentsActivity(enrList);    
    }
    
    global void finish(Database.BatchableContext BC) {
    }    
}