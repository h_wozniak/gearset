/**
*   @author         Sebastian Łasisz
*   @description    Integration class that retrieves arrears for educational agreements, used from batch that is scheduled daily.
**/
global without sharing class IntegrationArrearsRetrieval {

    public static void retrieveArrearsForLogins(List<Enrollment__c> eduAgreements) {
        HttpRequest httpReq = new HttpRequest();

        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();

        String createDidactics = esb.Retrieve_Arrears_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createDidactics);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        Set<String> portalLogins = preparePortalLogins(eduAgreements);

        if (portalLogins != null && !portalLogins.isEmpty()) {
            String jsonToSend = JSON.serialize(portalLogins);
            httpReq.setBody(jsonToSend);

            Http http = new Http();
            HTTPResponse httpRes = http.send(httpReq);

            Boolean success = (httpRes.getStatusCode() == 200);
            if (!success) {
                ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationArrearsRetrieval.class.getName()
                        + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + portalLogins);
            }

            System.debug(httpRes.getBody());
            List<AckResponse_JSON> response = parseHTTPResponse(httpRes.getBody());
            List<Enrollment__c> eduAgrToUpdate = parseAckResponse(response, eduAgreements);

            try {
                if (eduAgrToUpdate != null && !eduAgrToUpdate.isEmpty()) {
                    update eduAgrToUpdate;
                }
            } catch (Exception e) {
                ErrorLogger.log(e);
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    @TestVisible
    private static Set<String> preparePortalLogins(List<Enrollment__c> eduAgreements) {
        Set<String> portalLogins = new Set<String>();
        for (Enrollment__c eduAgr : eduAgreements) {
            if (eduAgr.Portal_Login__c != null) {
                portalLogins.add(eduAgr.Portal_Login__c);
            }
        }

        return portalLogins;
    }

    @TestVisible
    private static List<Enrollment__c> parseAckResponse(List<AckResponse_JSON> response, List<Enrollment__c> eduAgreements) {
        List<Enrollment__c> eduAgrToUpdate = new List<Enrollment__c>();

        for (Enrollment__c eduAgr : eduAgreements) {
            Decimal balance = 0;
            Boolean eduAgrExists = false;

            for (AckResponse_JSON portalLoginResponse : response) {
                if (portalLoginResponse.portal_login == eduAgr.Portal_Login__c) {
                    eduAgrExists = true;

                    if (portalLoginResponse.to_pay_total > 0) {
                        balance = portalLoginResponse.to_pay_total;
                        break;
                    }
                }
            }

            if (eduAgrExists) {
                eduAgr.Balance__c = (balance * -1);
                eduAgrToUpdate.add(eduAgr);
            }
        }

        return eduAgrToUpdate;
    }

    private static List<AckResponse_JSON> parseHTTPResponse(String jsonBody) {
        return (List<AckResponse_JSON>) System.JSON.deserialize(jsonBody, List<AckResponse_JSON>.class);
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public class AckResponse_JSON {
        public String portal_login;
        public Decimal to_pay_total;
        public List<Balance_JSON> balance;
    }

    public class Balance_JSON {
        public String title;
        public Decimal amount_to_pay;
    }
}