/**
* @author       Sebastian Łasisz
* @description  Class for storing Company Discount methods for Studies
**/

public without sharing class CompanyDiscountManager {

    /**
     * Method for determining company discounts for given list of study enrollments.
     */
    public static Map<Id, Set<String>> determineCompanyDiscountOnEnrollment(List<Enrollment__c> enrList) {
        Map<Id, Discount__c> companyDiscounts = getCompanyDiscounts();
        Map<Id, Set<Id>> attachedAccountsToDiscountMap = getAttachedAccountsToDiscountMap(companyDiscounts);

        Map<Id, Set<String>> existingOfferCompanyDiscounts = CompanyDiscountManager.determineCompanyDiscountFromOffer(enrList, companyDiscounts, attachedAccountsToDiscountMap);
        return CompanyDiscountManager.determineCompanyDiscountOnEnrollment(enrList, companyDiscounts, existingOfferCompanyDiscounts);
    }

    /**
     * Method for connecting Company Discounts to Enrollment based on attached offer connectors.
     */
    private static Map<Id, Set<String>> determineCompanyDiscountFromOffer(List<Enrollment__c> enrList, Map<Id, Discount__c> companyDiscounts, Map<Id, Set<Id>> attachedAccountsToDiscountMap) {
        Map<Id, Set<String>> enrWithAttachedCompDiscounts = new Map<Id, Set<String>>();
        Map<Enrollment__c, Set<Id>> enrToStudyOfferMap = new Map<Enrollment__c, Set<Id>>();

        Set<Id> allPotentialOfferIds = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            Set<Id> potentialOfferIds = new Set<Id>{
                    enr.Course_or_Specialty_Offer__c,
                    enr.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__c,
                    enr.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__c,
                    enr.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c
            };

            potentialOfferIds.remove(null);
            allPotentialOfferIds.addAll(potentialOfferIds);

            enrToStudyOfferMap.put(enr, potentialOfferIds);
        }

        Map<Id, Offer__c> offersWithDiscountsList = CompanyDiscountManager.getOffersWithValidCompanyDiscountsMap(allPotentialOfferIds);

        List<Discount__c> enrDiscConnToInsert = new List<Discount__c>();


        for (Enrollment__c enr : enrList) {
            Set<String> appliesToSet = new Set<String>();
            Set<String> foribbdenDiscounts = CompanyDiscountManager.determineForbiddenAppliesToValues(enr);

            for (Id offerId : enrToStudyOfferMap.get(enr)) {
                Offer__c offerWithDiscounts = offersWithDiscountsList.get(offerId);

                for (Discount__c offerDC : offerWithDiscounts.Discounts__r) {
                    Discount__c companyDiscount = companyDiscounts.get(offerDC.Discount_from_Offer_DC__c);
                    Set<Id> relatedAccountIds = attachedAccountsToDiscountMap.get(companyDiscount.Id);

                    if (relatedAccountIds.contains(enr.Candidate_Student__r.AccountId)
                            && !appliesToSet.contains(offerDc.Discount_from_Offer_DC__r.Applies_to__c)
                            && !foribbdenDiscounts.contains(offerDc.Discount_from_Offer_DC__r.Applies_to__c)) {

                        Discount__c enrDiscConn = prepareCompanyDiscountFromCompany(enr.Id, offerDC.Discount_from_Offer_DC__r, offerDC.Id);
                        CompanyDiscountManager.updateAppliesToRestrictions(enrWithAttachedCompDiscounts, enr.Id, appliesToSet, enrDiscConn.Applies_to__c);

                        enrDiscConnToInsert.add(enrDiscConn);
                    }
                }

                /* if there is no company discount from accountId, look for company discount from parentId */
                for (Discount__c offerDC : offerWithDiscounts.Discounts__r) {
                    Discount__c companyDiscount = companyDiscounts.get(offerDC.Discount_from_Offer_DC__c);
                    Set<Id> relatedAccountIds = attachedAccountsToDiscountMap.get(companyDiscount.Id);

                    if (relatedAccountIds.contains(enr.Candidate_Student__r.Account.ParentId)
                            && !appliesToSet.contains(offerDc.Discount_from_Offer_DC__r.Applies_to__c)
                            && !foribbdenDiscounts.contains(offerDc.Discount_from_Offer_DC__r.Applies_to__c)) {

                        Discount__c enrDiscConn = prepareCompanyDiscountFromCompany(enr.Id, offerDC.Discount_from_Offer_DC__r, offerDC.Id);
                        CompanyDiscountManager.updateAppliesToRestrictions(enrWithAttachedCompDiscounts, enr.Id, appliesToSet, enrDiscConn.Applies_to__c);

                        enrDiscConnToInsert.add(enrDiscConn);
                    }
                }
            }
        }

        try {
            insert enrDiscConnToInsert;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        return enrWithAttachedCompDiscounts;
    }

    /**
     * Method for connecting Company Discounts to Enrollment based on University, Degree and Company Name
     */
    private static Map<Id, Set<String>> determineCompanyDiscountOnEnrollment(List<Enrollment__c> enrList, Map<Id, Discount__c> companyDiscounts, Map<Id, Set<String>> enrWithAttachedCompDiscounts) {
        List<Discount__c> enrDiscConnToInsert = new List<Discount__c>();

        for (Enrollment__c enr : enrList) {
            Set<String> appliesToSet;
            if (enrWithAttachedCompDiscounts.get(enr.Id) != null) {
                appliesToSet = enrWithAttachedCompDiscounts.get(enr.Id);
            } else {
                appliesToSet = new Set<String>();
            }

            Set<String> forbiddenAppliesTo = CompanyDiscountManager.determineForbiddenAppliesToValues(enr);

            for (Id companyDiscountId : companyDiscounts.keySet()) {
                Discount__c companyDiscount = companyDiscounts.get(companyDiscountId);

                if (!forbiddenAppliesTo.contains(companyDiscount.Applies_to__c) && !appliesToSet.contains(companyDiscount.Applies_to__c)) {
                    for (Discount__c companyDiscountConnector : companyDiscount.CompanyDiscounts__r) {
                        List<String> degreesFromDiscountLst = new List<String>();
                        if (companyDiscount.Degrees__c != null) {
                            degreesFromDiscountLst = companyDiscount.Degrees__c.split(';');
                        }

                        Set<String> degreesFromDiscountSet = new Set<String>();
                        degreesFromDiscountSet.addAll(degreesFromDiscountLst);

                        if (companyDiscountConnector.Company_from_Company_Discount__c == enr.Candidate_Student__r.AccountId
                                && (companyDiscount.University_Names__c != null && companyDiscount.University_Names__c.contains(enr.Course_or_Specialty_Offer__r.University_Name__c))
                                && degreesFromDiscountSet.contains(enr.Degree__c)) {

                            Discount__c enrDiscConn = prepareCompanyDiscountFromCompany(enr.Id, companyDiscount, companyDiscountConnector.Id);
                            CompanyDiscountManager.updateAppliesToRestrictions(enrWithAttachedCompDiscounts, enr.Id, appliesToSet, enrDiscConn.Applies_to__c);

                            enrDiscConnToInsert.add(enrDiscConn);
                        }
                    }
                }
            }

            /* if there is no company discount from accountId, look for company discount from parentId */
            for (Id companyDiscountId : companyDiscounts.keySet()) {
                Discount__c companyDiscount = companyDiscounts.get(companyDiscountId);

                if (!appliesToSet.contains(companyDiscount.Applies_to__c) && !forbiddenAppliesTo.contains(companyDiscount.Applies_to__c)) {
                    for (Discount__c companyDiscountConnector : companyDiscount.CompanyDiscounts__r) {
                        List<String> degreesFromDiscountLst = new List<String>();
                        if (companyDiscount.Degrees__c != null) {
                            degreesFromDiscountLst = companyDiscount.Degrees__c.split(';');
                        }

                        Set<String> degreesFromDiscountSet = new Set<String>();
                        degreesFromDiscountSet.addAll(degreesFromDiscountLst);

                        if (companyDiscountConnector.Company_from_Company_Discount__c == enr.Candidate_Student__r.Account.ParentId
                                && companyDiscount.University_Names__c.contains(enr.Course_or_Specialty_Offer__r.University_Name__c)
                                && degreesFromDiscountSet.contains(enr.Degree__c)) {

                            Discount__c enrDiscConn = prepareCompanyDiscountFromCompany(enr.Id, companyDiscount, companyDiscountConnector.Id);
                            CompanyDiscountManager.updateAppliesToRestrictions(enrWithAttachedCompDiscounts, enr.Id, appliesToSet, enrDiscConn.Applies_to__c);

                            enrDiscConnToInsert.add(enrDiscConn);
                        }
                    }
                }
            }
        }

        try {
            insert enrDiscConnToInsert;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        return enrWithAttachedCompDiscounts;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ PREPARE DATA METHODS -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * Helper for determineCompanyDiscountsOnEnrollment and IntegrationProductPriceHelper.class, returns map of Offers with it's valid discounts
     */
    public static Map<Id, Offer__c> getOffersWithValidCompanyDiscountsMap(Set<Id> offerIds) {
        return new Map<Id, Offer__c>([
                SELECT Id, Ignore_Discounts__c, Course_Offer_from_Specialty__r.Ignore_Discounts__c, University_Study_Offer_from_Course__c,
                        Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__c, Course_Offer_from_Specialty__c,
                        Applies_to__c,
                (
                        SELECT Id, Discount_from_Offer_DC__r.Trade_Name__c, Offer_from_Offer_DC__c, Discount_from_Offer_DC__r.Discount_Value__c,
                                Discount_from_Offer_DC__r.Discount_Kind__c, Discount_from_Offer_DC__r.Discount_Type_Studies__c,
                                Discount_from_Offer_DC__r.Applies_to__c, Discount_from_Offer_DC__r.Applied_through__c, Discount_from_Offer_DC__r.Detail_Level__c
                        FROM Discounts__r
                        WHERE Discount_from_Offer_DC__r.Active__c = true
                        AND Discount_from_Offer_DC__r.RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT)
                        ORDER BY Discount_from_Offer_DC__r.Discount_Type_Studies__c DESC
                )
                FROM Offer__c
                WHERE Id IN :offerIds
        ]);
    }


    /**
     * Helper to retrieve company discount with it's account and offer connectors.
     */
    public static Map<Id, Discount__c> getCompanyDiscounts() {
        return new Map<Id, Discount__c>([
                SELECT Id, University_Names__c, Trade_Name__c, Degrees__c, Discount_Kind__c, Discount_Value__c, Applies_to__c, Applied_through__c, Discount_Type_Studies__c, (
                        SELECT Id, Company_from_Company_Discount__c
                        FROM CompanyDiscounts__r
                        WHERE (Valid_From__c = null OR Valid_From__c <= :System.today())
                        AND (Valid_To__c = null OR Valid_To__c >= :System.today())
                ), (
                        SELECT Id, Discount_from_Offer_DC__c, Offer_from_Offer_DC__c
                        FROM OfferDiscountConnectors__r
                )
                FROM Discount__c
                WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_COMPANY_DISCOUNT)
                AND Active__c = true
        ]);
    }

    /**
     * Method responsible for creating Map containing set of account ids pinned to given discount.
     */
    public static Map<Id, Set<Id>> getAttachedAccountsToDiscountMap(Map<Id, Discount__c> companyDiscounts) {
        Map<Id, Set<Id>> attachedAccountsToDiscountMap = new Map<Id, Set<Id>>();
        for (Id companyDiscountId : companyDiscounts.keySet()) {
            Discount__c companyDiscount = companyDiscounts.get(companyDiscountId);

            for (Discount__c companyDiscountConnector : companyDiscount.CompanyDiscounts__r) {
                if (attachedAccountsToDiscountMap.get(companyDiscountId) == null) {
                    attachedAccountsToDiscountMap.put(companyDiscountId, new Set<Id>{
                            companyDiscountConnector.Company_from_Company_Discount__c
                    });
                } else {
                    attachedAccountsToDiscountMap.get(companyDiscountId).add(companyDiscountConnector.Company_from_Company_Discount__c);
                }
            }
        }

        return attachedAccountsToDiscountMap;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------- HELPER  METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    /**
     * Method responsible for creating new study discount connector.
     *
     * @param enrollmentId Id of study enrollment ot attach discount to.
     * @param companyDiscount Company discount to rewrite data to connector.
     * @param parentId Id of discount connector to which study connector should be attached.
     *
     * @return Newly creeated but not inserted study connector for company discount.
     */
    private static Discount__c prepareCompanyDiscountFromCompany(Id enrollmentId, Discount__c companyDiscount, Id parentId) {
        Discount__c enrDiscConn = new Discount__c();
        enrDiscConn.RecordTypeId = CommonUtility.getRecordTypeId('Discount__c', CommonUtility.DISCOUNT_RT_STUDY_DISCOUNT_CONNECTOR);
        enrDiscConn.Enrollment__c = enrollmentId;
        enrDiscConn.Discount_from_Discount_Connector__c = parentId;
        enrDiscConn.Applied__c = true;
        enrDiscConn.Discount_Kind__c = companyDiscount.Discount_Kind__c;
        enrDiscConn.Discount_Value__c = companyDiscount.Discount_Value__c;
        enrDiscConn.Applies_to__c = companyDiscount.Applies_to__c;
        enrDiscConn.Applied_through__c = companyDiscount.Applied_through__c;
        enrDiscConn.Discount_Type_Studies__c = companyDiscount.Discount_Type_Studies__c;

        return enrDiscConn;
    }

    /**
     * Method responsible for updating existing applies to for newly created discounts (used to not-overlap discounts for the same applies to).
     */
    private static void updateAppliesToRestrictions(Map<Id, Set<String>> enrWithAttachedCompDiscounts, Id enrId, Set<String> appliesToSet, String appliesToValue) {
        if (enrWithAttachedCompDiscounts.get(enrId) == null) {
            enrWithAttachedCompDiscounts.put(enrId, new Set<String>{appliesToValue});
            appliesToSet.add(appliesToValue);
        } else {
            enrWithAttachedCompDiscounts.get(enrId).add(appliesToValue);
            appliesToSet.add(appliesToValue);
        }
    }

    /**
     * Method responsible for preparing Set of values of forbidden Applies to statuses.
     *
     * @param enr Study enrollment containing information about ignoring discounts.
     *
     * @return Set of values containing forbidden Applies to statuses.
     */
    private static Set<String> determineForbiddenAppliesToValues(Enrollment__c enr) {
        Set<String> forbiddenAppliesTo = new Set<String>();

        if (enr.Starting_Semester__c != CommonUtility.ENROLLMENT_STARTINGSEM_1) {
            forbiddenAppliesTo.add(CommonUtility.DISCOUNT_APPLIESTO_TUITION);
        } else {
            forbiddenAppliesTo = enr.TECH_Ignore_Automatic_Discounts__c ? CommonUtility.getOptionsFromMultiSelect(enr.Course_or_Specialty_Offer__r.Applies_to__c) : new Set<String>();
        }

        return forbiddenAppliesTo;
    }
}