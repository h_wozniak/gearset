@isTest
private class ZTestTargetManager {
    

    /* ------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------- VALIDATIONS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_checkIfTargetContainsSubtargets_Fail() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500
        ), true);


        Test.startTest();
        try {
            ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
                Study_Target_from_Subtarget__c = studyEnrTarget.Id,
                Target_Realization__c = 501
            ), true);
        } catch(Exception e) {
            Boolean expectedExceptionThrown = e.getMessage().contains(Label.msg_error_SubtargetRealizationOverMax)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }
    
    @isTest static void test_checkIfTargetContainsSubtargets_Pass() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500
        ), true);


        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500
        ), true);

        System.assert(true);
    }

    @isTest static void test_checkIfSubtargetDatesOverlap_Fail1() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500,
            Applies_from__c = Date.newInstance(2016, 3, 24),
            Applies_to__c = Date.newInstance(2016, 3, 26)
        ), true);


        Test.startTest();
        try {
            ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
                Study_Target_from_Subtarget__c = studyEnrTarget.Id,
                Target_Realization__c = 500,
                Applies_from__c = Date.newInstance(2016, 3, 23),
                Applies_to__c = Date.newInstance(2016, 3, 25)
            ), true);
        } catch(Exception e) {
            Boolean expectedExceptionThrown = e.getMessage().contains(Label.msg_error_SubtargetDatesOverlap)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void test_checkIfSubtargetDatesOverlap_Fail2() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500,
            Applies_from__c = Date.newInstance(2016, 3, 24),
            Applies_to__c = Date.newInstance(2016, 3, 26)
        ), true);


        Test.startTest();
        try {
            ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
                Study_Target_from_Subtarget__c = studyEnrTarget.Id,
                Target_Realization__c = 500,
                Applies_from__c = Date.newInstance(2016, 3, 25),
                Applies_to__c = Date.newInstance(2016, 3, 26)
            ), true);
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.msg_error_SubtargetDatesOverlap)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void test_checkIfSubtargetDatesOverlap_Fail3() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
            Study_Target_from_Subtarget__c = studyEnrTarget.Id,
            Target_Realization__c = 500,
            Applies_from__c = Date.newInstance(2016, 3, 24),
            Applies_to__c = Date.newInstance(2016, 3, 26)
        ), true);


        Test.startTest();
        try {
            ZDataTestUtility.createStudyEnrollmentSubtarget(new Target__c(
                Study_Target_from_Subtarget__c = studyEnrTarget.Id,
                Target_Realization__c = 500,
                Applies_from__c = Date.newInstance(2016, 3, 21),
                Applies_to__c = Date.newInstance(2016, 3, 27)
            ), true);
        } catch(Exception e) {
            Boolean expectedExceptionThrown = e.getMessage().contains(Label.msg_error_SubtargetDatesOverlap)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }

    @isTest static void test_checkIfTeamTargetAlreadyExists() {
        Target__c studyEnrTarget = ZDataTestUtility.createStudyEnrollmentTarget(new Target__c(
            Target_Realization__c = 1000
        ), true);

        ZDataTestUtility.createTeamTarget(new Target__c(
            Target_from_Team_Target__c = studyEnrTarget.Id,
            Target_Realization__c = 500,
            Team__c = CommonUtility.TARGET_TEAM_CC
        ), true);


        Test.startTest();
        try {
        ZDataTestUtility.createTeamTarget(new Target__c(
            Target_from_Team_Target__c = studyEnrTarget.Id,
            Target_Realization__c = 500,
            Team__c = CommonUtility.TARGET_TEAM_CC
        ), true);
        } catch(Exception e) {
            Boolean expectedExceptionThrown = e.getMessage().contains(Label.msg_error_TeamTargetAlreadyExists)? true : false;
            System.assert(expectedExceptionThrown);
        }
        Test.stopTest();
    }
    
}