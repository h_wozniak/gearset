@isTest
private class ZTestEnrollmentJSONDisplayController {

	@isTest static void test_EnrollmentJSONDisplayController() {
		CustomSettingDefault.initDidacticsCampaign();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Enrollment_JSON__c = '{"hello world" : "yello"}'), true);

		PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getParameters().put('enrId', studyEnr.Id);

		Test.startTest();
		EnrollmentJSONDisplayController cntrl = new EnrollmentJSONDisplayController();
		System.assert(cntrl.enrJSON != null);
		Test.stopTest();
	}

	@isTest static void test_EnrollmentJSONDisplayController_noData() {
		CustomSettingDefault.initDidacticsCampaign();
		Enrollment__c studyEnr = ZDataTestUtility.createEnrollmentStudy(null, true);

		PageReference pageRef = Page.EnrollmentStudy;
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getParameters().put('enrId', studyEnr.Id);

		Test.startTest();
		try {
			EnrollmentJSONDisplayController cntrl = new EnrollmentJSONDisplayController();
		}
		catch (Exception e) {
			System.assertEquals(e.getMessage(), '');
		}
		Test.stopTest();
	}
}