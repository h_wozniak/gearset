/**
*   @author         Sebastian Łasisz
*   @description    This class is used to update Candidate's Consents from Experia when requested.
**/

@RestResource(urlMapping = '/consents/update/*')
global without sharing class IntegrationContactConsentsUpdate {
    
    @HttpPOST
    global static void receiveConsents() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

		String contactId = req.params.get('person_id');

		Contact candidate;
        try {
        	candidate = [
        		SELECT Id, Consent_Direct_Communications__c, Consent_Electronic_Communication__c, Consent_Graduates__c, Consent_Marketing__c, Change_History__c,
        		Consent_Direct_Communications_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Graduates_ADO__c, Consent_Marketing_ADO__c, CreatedDate
        		FROM Contact
        		WHERE Id = :contactId
        	];
        }
        catch (Exception ex) {
            ErrorLogger.log(ex);
            res.responseBody = Blob.valueOf(IntegrationError.getErrorJSONByCode(604));
            res.statusCode = 400;    
            return ;
        }

        String jsonBody = req.requestBody.toString();     
        List<IntegrationContactConsentsUpdate.Consent_JSON> consentsToValidate;

        try {
            consentsToValidate = parse(jsonBody);
        } catch(Exception ex) {
            ErrorLogger.log(ex);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
            return ;
        }

        /* Create Marketing Consents for Contact */                                                          
        List<Catalog__c> catalogConsentsToValidate = [
            SELECT Id, Name, Active_To__c, Active_From__c, ADO__c, IPressoId__c, Consent_ADO_API_Name__c
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
        ];
        
        /* Prepare non-global consents first */
        for (IntegrationContactConsentsUpdate.Consent_JSON consent : consentsToValidate) {
            for (Catalog__c catalogConsent : catalogConsentsToValidate) {
                if (consent.consent_name == catalogConsent.Name) {
                    if (consent.value) {
                        candidate = MarketingConsentManager.updateConsentOnContact(CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c), candidate, catalogConsent.Consent_ADO_API_Name__c);
                    }
                    else {
                        candidate = MarketingConsentManager.removeConsentOnContact(CommonUtility.getOptionsFromMultiSelect(catalogConsent.ADO__c), candidate, catalogConsent.Consent_ADO_API_Name__c);
                    }
                }
            }
        }

        try {
        	update candidate;
            res.statusCode = 200;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(601);
            return ;        	
        }
    }
    
    private static List<IntegrationContactConsentsUpdate.Consent_JSON> parse(String jsonBody) {
        return (List<IntegrationContactConsentsUpdate.Consent_JSON>) System.JSON.deserialize(jsonBody, List<IntegrationContactConsentsUpdate.Consent_JSON>.class);
    }

    public class Consent_JSON {
    	public String consent_name;
    	public String consent_date;
    	public Boolean value;
    }
}