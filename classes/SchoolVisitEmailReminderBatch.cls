/**
 * Created by martyna.stanczuk on 2018-05-23.
 */

global class SchoolVisitEmailReminderBatch implements Database.Batchable<sObject>{

    Datetime dayOfVisitFrom;
    Datetime dayOfVisitTo;

    global SchoolVisitEmailReminderBatch(Integer daysBeforeVisit) {
        Date dayOfVisit = System.today().addDays(daysBeforeVisit);
        dayOfVisitFrom = Datetime.newInstanceGmt(dayOfVisit.year(), dayOfVisit.month(), dayOfVisit.day(), 0, 0, 0);
        dayOfVisitTo = Datetime.newInstanceGmt(dayOfVisit.year(), dayOfVisit.month(), dayOfVisit.day(), 23, 59, 59);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, University__c, WhatId, WhoId
                FROM Event
                WHERE StartDateTime >= :dayOfVisitFrom AND StartDateTime <= :dayOfVisitTo
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Event> eventList) {
        Set<Id> eventIds = new Set<Id>();
        for (Event event : eventList) {
            eventIds.add(event.Id);
        }

        EmailManager_SchoolVisitEmailReminder.sendVisitatorSchoolVisitReminder(eventIds);
    }

    global void finish(Database.BatchableContext BC) {}

}