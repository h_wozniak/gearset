@isTest
global class ZTestIntegrationCandidateUpdater {

    private static User createENLangUser_forStudies() {
        Profile usrProfile = [SELECT Id FROM Profile WHERE Name = :CommonUtility.PROFILE_SYSTEM_ADMIN];
        User usr = new User(
                Alias = 'tebaST',
                Email = 'tebaST@tebaAn.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestingSt',
                LanguageLocaleKey = 'en_us',
                LocaleSidKey = 'en_us',
                ProfileId = usrProfile.Id,
                Entity__c = 'WRO',
                TimezoneSidKey = 'America/Los_Angeles',
                WSB_Department__c = 'Integration',
                Username = 'tebasalestraining@testorg.com'
        );

        insert usr;

        return usr;
    }

    private static Map<Integer, sObject> prepareData() {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(null, true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id, Candidate_Student__c = retMap.get(0).Id), true));

        return retMap;
    }

    //add asserts
    // add method with more data
    @isTest static void test_updateCandidate() {
        Map<Integer, sObject> dataMap = prepareData();

        Contact c = (Contact)dataMap.get(0);

        String body = '{"updating_system":"CRM","updating_system_contact_id":"' + c.Id + '","first_name":"Sebastian","second_name":"","last_name":"Lasisz","family_name":"Laisz","addresses":[{"type":"Other","country":"Poland","postal_code":"00-000","city":"Wrocław","street":"Skłodowska"}],"phones":[{"type":"Other","number":"508421915"}],"primary_email":"sebastian.lasisz@enxoo.com","additional_emails":[],"consents":[{"type":"Marketing","department":"WSB Poznań","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"marketing_automation":{"position":"Director","department":["WSB Wrocław","WSB Poznań"],"area_of_interest":["Business"],"a_level_exam_year":"2020","school_type":"High School","school_name":"Woot"}}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        IntegrationCandidateUpdate.receiveContactInformation();
        Test.stopTest();
    }

    //add asserts
    // add method with more data
    @isTest static void test_updateCandidate2() {
        Map<Integer, sObject> dataMap = prepareData();

        Contact c = (Contact)dataMap.get(0);

        String body = '{"updating_system":"CRM","updating_system_contact_id":"' + c.Id + '","first_name":"Sebastian","second_name":"","last_name":"Lasisz","family_name":"Laisz","addresses":[{"type":"Other","country":"Polska","postal_code":"00-000","city":"Wrocław","street":"Skłodowska"}],"phones":[{"type":"Other","number":"508421915"}],"primary_email":"sebastian.lasisz@enxoo.com","additional_emails":[],"consents":[{"type":"Marketing","department":"WSB Poznań","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"marketing_automation":{"position":"Director","department":["WSB Wrocław","WSB Poznań"],"area_of_interest":["Business"],"a_level_exam_year":"2020","school_type":"High School","school_name":"Woot"}}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        IntegrationCandidateUpdate.receiveContactInformation();
        Test.stopTest();
    }

    @isTest static void test_updateCandidate_withoutRequest() {
        Map<Integer, sObject> dataMap = prepareData();

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        try {
            IntegrationCandidateUpdate.receiveContactInformation();
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }
        Test.stopTest();
    }

    @isTest static void test_updateCandidate_incompleteJSON() {
        Map<Integer, sObject> dataMap = prepareData();

        Contact c = (Contact)dataMap.get(0);

        String body = '{updating_system_contact_id":"' + c.Id + '","first_name":"Sebastian","second_name":"","last_name":"Lasisz","family_name":"Laisz","addresses":[{"type":"Other","country":"Poland","postal_code":"00-000","city":"Wrocław","street":"Skłodowska"}],"phones":[{"type":"Other","number":"508421915"}],"primary_email":"sebastian.lasisz@enxoo.com","additional_emails":[],"consents":[{"type":"Marketing","department":"WSB Poznań","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"marketing_automation":{"position":"Director","department":["WSB Wrocław","WSB Poznań"],"area_of_interest":["Business"],"a_level_exam_year":"2020","school_type":"High School","school_name":"Woot"}}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        try {
            IntegrationCandidateUpdate.receiveContactInformation();
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }
        Test.stopTest();
    }

    @isTest static void test_updateCandidate_incompleteDataInJSON() {
        Map<Integer, sObject> dataMap = prepareData();

        Contact c = (Contact)dataMap.get(0);
        String body = '{updating_system_contact_id":"' + c.Id + '","first_name":"Sebastian","second_name":"","last_name":null,"family_name":"Laisz","addresses":[{"type":"Other","country":"Poland","postal_code":"00-000","city":"Wrocław","street":"Skłodowska"}],"phones":[{"type":"Other","number":"508421915"}],"primary_email":"sebastian.lasisz@enxoo.com","additional_emails":[],"consents":[{"type":"Marketing","department":"WSB Poznań","value":true,"consent_date":"1991-11-11","refusal_date":"1991-11-11","source":"CRM","storage_location":""}],"marketing_automation":{"position":"Director","department":["WSB Wrocław","WSB Poznań"],"area_of_interest":["Business"],"a_level_exam_year":"2020","school_type":"High School","school_name":"Woot"}}';

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        try {
            IntegrationCandidateUpdate.receiveContactInformation();
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }
        Test.stopTest();
    }

    @isTest static void test_listToString_oneElement() {
        List<String> listToConvertToString = new List<String> { 'first element' };
        Test.startTest();
        String result = IntegrationCandidateUpdate.listToString(listToConvertToString);
        Test.stopTest();

        System.assertEquals(result, 'first element');
    }

    @isTest static void test_listToString_manyElements() {
        List<String> listToConvertToString = new List<String> { 'first element', 'second element', 'third element' };
        Test.startTest();
        String result = IntegrationCandidateUpdate.listToString(listToConvertToString);
        Test.stopTest();

        System.assertEquals(result, 'first element; second element; third element');
    }

    @isTest static void test_listToString_nullList() {
        Test.startTest();
        try {
            String result = IntegrationCandidateUpdate.listToString(null);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }
        Test.stopTest();
    }

    @isTest static void test_listToString_emptyList() {
        List<String> listToConvertToString = new List<String>();
        Test.startTest();
        String result = IntegrationCandidateUpdate.listToString(listToConvertToString);
        Test.stopTest();

        System.assertEquals(result, '');
    }

    @isTest static void test_updatePhones_homePhone() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        IntegrationManager.Phone_JSON mobilePhone = new IntegrationManager.Phone_JSON();
        mobilePhone.type = 'Home';
        mobilePhone.phoneNumber = '508421915';
        contactPhones.add(mobilePhone);
        sObject contactWithPhoneNumber = (Contact)dataMap.get(0);

        Contact preUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone, Additional_Phones__c
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        Test.startTest();

        sObject result = IntegrationCandidateUpdate.updatePhones(contactWithPhoneNumber, contactPhones);
        update result;

        Test.stopTest();

        Contact postUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone, Additional_Phones__c
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        System.assertEquals(postUpdate.Additional_Phones__c, '508421915');
    }

    @isTest static void test_updatePhones_otherPhone() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        IntegrationManager.Phone_JSON otherPhone = new IntegrationManager.Phone_JSON();
        otherPhone.type = 'Other';
        otherPhone.phoneNumber = '508421915';
        contactPhones.add(otherPhone);
        sObject contactWithPhoneNumber = (Contact)dataMap.get(0);

        Contact preUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone, Additional_Phones__c
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        Test.startTest();

        sObject result = IntegrationCandidateUpdate.updatePhones(contactWithPhoneNumber, contactPhones);
        update result;

        Test.stopTest();

        Contact postUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone, Additional_Phones__c
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        System.assertEquals(preUpdate.OtherPhone, postUpdate.OtherPhone);
        System.assertEquals(postUpdate.Additional_Phones__c, '508421915');
    }

    @isTest static void test_updatePhones_mobilePhone() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        IntegrationManager.Phone_JSON mobilePhone = new IntegrationManager.Phone_JSON();
        mobilePhone.type = 'Mobile';
        mobilePhone.phoneNumber = '508421915';
        contactPhones.add(mobilePhone);
        Contact contactWithPhoneNumber = (Contact)dataMap.get(0);

        Contact preUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        Test.startTest();

        CommonUtility.skipContactUpdateInOtherSystems = true;
        sObject result = IntegrationCandidateUpdate.updatePhones(contactWithPhoneNumber, contactPhones);
        update result;
        CommonUtility.skipContactUpdateInOtherSystems = false;

        Test.stopTest();

        Contact postUpdate = [
                SELECT Id, HomePhone, MobilePhone, OtherPhone
                FROM Contact
                WHERE Id =: contactWithPhoneNumber.Id
        ];

        System.assertNotEquals(preUpdate.MobilePhone, postUpdate.MobilePhone);
        System.assertEquals(postUpdate.MobilePhone, '508421915');
    }

    @isTest static void test_updatePhone_withEmptyPhoneList() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        sObject contactWithPhoneNumber = (Contact)dataMap.get(0);

        Test.startTest();
        sObject result = IntegrationCandidateUpdate.updatePhones(contactWithPhoneNumber, contactPhones);
        Test.stopTest();

        System.assertEquals(((Contact)result).MobilePhone, ((Contact)contactWithPhoneNumber).MobilePhone);
        System.assertEquals(((Contact)result).OtherPhone, ((Contact)contactWithPhoneNumber).OtherPhone);
        System.assertEquals(((Contact)result).HomePhone, ((Contact)contactWithPhoneNumber).HomePhone);
    }

    @isTest static void test_updatePhone_nullPhoneList() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        sObject contactWithPhoneNumber = (Contact)dataMap.get(0);

        Test.startTest();
        try {
            sObject result = IntegrationCandidateUpdate.updatePhones(contactWithPhoneNumber, null);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }
        Test.stopTest();
    }

    @isTest static void test_updatePhone_nullContact() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Phone_JSON> contactPhones = new List<IntegrationManager.Phone_JSON>();
        IntegrationManager.Phone_JSON mobilePhone = new IntegrationManager.Phone_JSON();
        mobilePhone.type = 'Mobile';
        mobilePhone.phoneNumber = '508421915';
        contactPhones.add(mobilePhone);

        Test.startTest();
        try {
            sObject result = IntegrationCandidateUpdate.updatePhones(null, contactPhones);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }

        Test.stopTest();
    }




    @isTest static void test_updateAddresses_MailingAddress() {
        Map<Integer, sObject> dataMap = prepareData();
        Contact contactWithAddress = (Contact)dataMap.get(0);
        List<IntegrationManager.Address_JSON> contactAddresses = new List<IntegrationManager.Address_JSON>();
        IntegrationManager.Address_JSON mailingAddress = new IntegrationManager.Address_JSON();
        mailingAddress.type = 'Mailing';
        mailingAddress.city = 'Wrocław';
        mailingAddress.country = 'Germany';
        contactAddresses.add(mailingAddress);

        Contact preUpdate = [
                SELECT Id, MailingCity, MailingCountry
                FROM Contact
                WHERE Id = :contactWithAddress.Id
        ];

        Test.startTest();
        sObject result = IntegrationCandidateUpdate.updateAddresses(contactWithAddress, contactAddresses);
        update result;
        Test.stopTest();

        Contact postUpdate = [
                SELECT Id, MailingCity, MailingCountry
                FROM Contact
                WHERE Id = :contactWithAddress.Id
        ];

        System.assertNotEquals(preUpdate.MailingCity, postUpdate.MailingCity);
        System.assertNotEquals(preUpdate.MailingCountry, postUpdate.MailingCountry);

        System.assertEquals(postUpdate.MailingCity, 'Wrocław');
        System.assertEquals(postUpdate.MailingCountry, 'Germany');

    }

    @isTest static void test_updateAddresses_OtherAddress() {
        Map<Integer, sObject> dataMap = prepareData();
        Contact contactWithAddress = (Contact)dataMap.get(0);
        List<IntegrationManager.Address_JSON> contactAddresses = new List<IntegrationManager.Address_JSON>();
        IntegrationManager.Address_JSON otherAddress = new IntegrationManager.Address_JSON();
        otherAddress.type = 'Other';
        otherAddress.city = 'Wrocław';
        otherAddress.country = 'Germany';
        contactAddresses.add(otherAddress);

        Contact preUpdate = [
                SELECT Id, OtherCity, OtherCountry
                FROM Contact
                WHERE Id = :contactWithAddress.Id
        ];

        Test.startTest();
        sObject result = IntegrationCandidateUpdate.updateAddresses(contactWithAddress, contactAddresses);
        update result;
        Test.stopTest();

        Contact postUpdate = [
                SELECT Id, OtherCity, OtherCountry
                FROM Contact
                WHERE Id = :contactWithAddress.Id
        ];

        System.assertNotEquals(preUpdate.OtherCity, postUpdate.OtherCity);
        System.assertNotEquals(preUpdate.OtherCountry, postUpdate.OtherCountry);

        System.assertEquals(postUpdate.OtherCity, 'Wrocław');
        System.assertEquals(postUpdate.OtherCountry, 'Germany');
    }

    @isTest static void test_updateAddresses_WithoutAddress() {
        Map<Integer, sObject> dataMap = prepareData();
        Contact contactWithAddress = (Contact)dataMap.get(0);
        List<IntegrationManager.Address_JSON> contactAddresses = new List<IntegrationManager.Address_JSON>();

        Contact preUpdate = [
                SELECT Id, MailingStreet, OtherStreet
                FROM Contact
                WHERE Id = :contactWithAddress.Id
        ];

        Test.startTest();
        sObject result = IntegrationCandidateUpdate.updateAddresses(contactWithAddress, contactAddresses);

        User u = createENLangUser_forStudies();

        System.runAs(u) {
            try {
                update result;
            } catch (Exception e) {
                System.assert(e.getMessage().contains('Other address must be provided in order to copy it to Mailing address fields.'));
            }

            Test.stopTest();

            Contact postUpdate = [
                    SELECT Id, MailingStreet, OtherStreet
                    FROM Contact
                    WHERE Id = :contactWithAddress.Id
            ];

            System.assertEquals(preUpdate.MailingStreet, postUpdate.MailingStreet);
            System.assertEquals(preUpdate.OtherStreet, postUpdate.OtherStreet);
        }
    }

    @isTest static void test_updateAddresses_NullAddress() {
        Map<Integer, sObject> dataMap = prepareData();
        Contact contactWithAddress = (Contact)dataMap.get(0);

        Test.startTest();
        try {
            sObject result = IntegrationCandidateUpdate.updateAddresses(contactWithAddress, null);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }

        Test.stopTest();
    }

    @isTest static void test_updateAddresses_NullContact() {
        Map<Integer, sObject> dataMap = prepareData();
        List<IntegrationManager.Address_JSON> contactAddresses = new List<IntegrationManager.Address_JSON>();
        IntegrationManager.Address_JSON otherAddress = new IntegrationManager.Address_JSON();
        otherAddress.type = 'Other';
        otherAddress.city = 'Wrocław';
        otherAddress.country = 'Poland';
        contactAddresses.add(otherAddress);

        Test.startTest();
        try {
            sObject result = IntegrationCandidateUpdate.updateAddresses(null, contactAddresses);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(' '));
        }

        Test.stopTest();
    }
}