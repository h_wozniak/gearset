/**
*   @author         Sebastian Łasisz
*   @description    schedulable class that runs batches daily to sync consents with iPresso
**/

/**
*   To initiate the IntegrationIPressoConsentSyncSchedule execute below code in Developer Console (execute Anonymous Code)
*
*   --- runs once a day at 8:00 ---
*   System.schedule('IntegrationIPressoConsentSyncSchedule', '0 0 8 * * ? *', new IntegrationIPressoConsentSyncSchedule());
**/


global class IntegrationIPressoConsentSyncSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
    	List<Catalog__c> consentsToSync = [
    		SELECT Id, IPressoId__c, Name, Content__c,
    		(SELECT Id, IPressoId__c, Name, Language_Code__c, Content__c FROM Consent_Languages__r)
    		FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
            AND Active_From__c <= :System.today()
            AND (Active_To__c = null OR Active_To__c >= : System.today())
            AND Send_To_iPresso__c = true
    	];

    	for (Catalog__c consent : consentsToSync) {
			System.debug('Record ' + consent.Id + ' ' + consent.Name);
    		IntegrationIPressoConsentSync.syncConsentsWithIPressoAsync(consent.Id, consent.Name);
    		for (Catalog__c languageConsent : consent.Consent_Languages__r) {
    			System.debug('Language Record ' + languageConsent.Id + ' ' + consent.Name + '_' + languageConsent.Language_Code__c);
    			IntegrationIPressoConsentSync.syncConsentsWithIPressoAsync(languageConsent.Id, consent.Name + '_' + languageConsent.Language_Code__c);
    		}
    	}
    }
}