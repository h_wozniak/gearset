/**
* @author       Sebastian Łasisz
* @description  Utility class for Language methods (on Study)
**/

public without sharing class LanguageManager {

    
    public static void validateAndUpdateLanguages(List<Enrollment__c> langListToValidateAndUpdate) {
        Set<Id> mainEnrollmentIds = new Set<Id>();
        Map<Enrollment__c, Id> enrLangToOfferMap = new Map<Enrollment__c, Id>();
        Set<Id> offersWithIds = new Set<Id>();

        for (Enrollment__c enollmentToUpdate : langListToValidateAndUpdate) {
            mainEnrollmentIds.add(enollmentToUpdate.Enrollment_from_Language__c);
        }

        Map<Id, Enrollment__c> mainEnrollmentsWithLanguages = new Map<Id, Enrollment__c>([
            SELECT Id, 
                (SELECT Id, Foreign_Language__c, Course_or_Specialty_Universal__c, Enrollment_from_Language__c 
                 FROM Languages__r) 
            FROM Enrollment__c 
            WHERE Id IN :mainEnrollmentIds
        ]);

        //Duplicate detection
        for (Enrollment__c langEnr : langListToValidateAndUpdate) {
            Enrollment__c mainEnrollment = mainEnrollmentsWithLanguages.get(langEnr.Enrollment_from_Language__c);

            for (Enrollment__c enrollmentLanguage: mainEnrollment.Languages__r) {
                if (langEnr.Foreign_Language__c == enrollmentLanguage.Foreign_Language__c) {
                        if(!CommonUtility.isLightningEnabled()) {
                            langEnr.addError(' '+ Label.msg_error_duplicateLanguageOnEnrollment + ': <a target="_blank" href="/' 
                                + enrollmentLanguage.Enrollment_from_Language__c + '">' + 'Link' + '</a>', false);
                        } else {
                            langEnr.addError(' '+ Label.msg_error_duplicateLanguageOnEnrollment + ': ' 
                                + enrollmentLanguage.Enrollment_from_Language__c, false);
                        }
                }
            }
        }

        //Language offer detection
        List<Enrollment__c> mainEnrollments = [SELECT Id, RecordTypeId, Course_or_Specialty_Universal__c FROM Enrollment__c WHERE Id IN :mainEnrollmentIds];

        for (Enrollment__c langEnr : langListToValidateAndUpdate) {
            for (Enrollment__c mainEnr : mainEnrollments) {
                if (langEnr.Enrollment_from_Language__c == mainEnr.Id) {
                    enrLangToOfferMap.put(langEnr, mainEnr.Course_or_Specialty_Universal__c);
                }
            }
        }
        
        for (Enrollment__c mainEnrollment : mainEnrollments) {
            offersWithIds.add(mainEnrollment.Course_or_Specialty_Universal__c);
        }
        
        List<Offer__c> offerList = [
            SELECT Id, Course_Offer_from_Specialty__c, 
                (SELECT Id, Language__c, Course_Offer_from_Language__c 
                 FROM Languages__r) 
            FROM Offer__c 
            WHERE Id IN :offersWithIds
        ];

        Map<Id, List<Offer__c>> offerToLanguagesList = new Map<Id, List<Offer__c>>();
        Set<Id> coursesWithLanguages = new Set<Id>();
        List<Offer__c> offersWithoutLanguagesList = new List<Offer__c>();
        for (Offer__c  offer : offerList) {
            if (!offer.Languages__r.isEmpty()) {
                offerToLanguagesList.put(offer.Id, offer.Languages__r);
            }
            else {
                offersWithoutLanguagesList.add(offer);
                coursesWithLanguages.add(offer.Course_Offer_from_Specialty__c);
            }
        }

        List<Offer__c> courseList = [
            SELECT Id, Course_Offer_from_Specialty__c, 
                (SELECT Id, Language__c, Course_Offer_from_Language__c 
                 FROM Languages__r) 
            FROM Offer__c 
            WHERE Id IN :coursesWithLanguages
        ];

        for (Offer__c  offer : offersWithoutLanguagesList) {
            for (Offer__c course : courseList ) {
                if (course.Id == offer.Course_Offer_from_Specialty__c && !course.Languages__r.isEmpty()) {
                    offerToLanguagesList.put(offer.Id, course.Languages__r);
                }
            }
        }


        for (Enrollment__c langEnr : langListToValidateAndUpdate) {

            Boolean langOfferExists = false;
            List<Offer__c> languageList = offerToLanguagesList.get(enrLangToOfferMap.get(langEnr));
            if (languageList != null) {
                for (Offer__c langOffer : languageList) {
                    if (langOffer.Language__c == langEnr.Foreign_Language__c) {
                        langOfferExists = true;
                        langEnr.Language_from_Enrollment_Language__c = langOffer.Id;
                    }
                }
            }

            if (!langOfferExists) {
                if(!CommonUtility.isLightningEnabled()) {
                    langEnr.addError(' '+ Label.msg_error_notExistingLanguageOnEnrollment + ': <a target="_blank" href="/' + enrLangToOfferMap.get(langEnr) 
                        + '">' + 'Link' + '</a>', false);
                } else {
                    langEnr.addError(' '+ Label.msg_error_notExistingLanguageOnEnrollment, false);
                }
            }

        }        
    }

    public static ApexPages.Message testError(String error) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
        return myMsg;
    }
}