/**
*   @author         Wojciech Słodziak
*   @description    This class is used to send Payment Templates to Experia
**/

global without sharing class IntegrationPaymentTemplateSender {

    @future(callout=true)
    public static void sendPaymentTemplatesAsync(Set<Id> pbIds) {
        IntegrationPaymentTemplateSender.sendPaymentTemplateFinal(pbIds);
    }

    public static Boolean sendPaymentTemplatesSync(Set<Id> pbIds) {
        return IntegrationPaymentTemplateSender.sendPaymentTemplateFinal(pbIds);
    }

    // final method for sending callout
    private static Boolean sendPaymentTemplateFinal(Set<Id> pbIds) {
        HttpRequest httpReq = new HttpRequest();
        
        ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
        String createPaymentEndPoint = esb.Create_Payment_Templates_End_Point__c;
        String esbIp = esb.Service_Url__c;

        httpReq.setEndpoint(esbIp + createPaymentEndPoint);
        httpReq.setMethod(IntegrationManager.HTTP_METHOD_POST);
        httpReq.setHeader(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);

        String jsonToSend = '';
        try {
            jsonToSend = IntegrationPaymentTemplateHelper.buildPriceBookPaymentTemplatesJSON(pbIds);
        }
        catch (IntegrationPaymentTemplateHelper.InadequatePriceBookConfigException e) {
            IntegrationPaymentTemplateHelper.updateSyncStatus_inProgress(false, pbIds);
            return false;
        }

        httpReq.setBody(jsonToSend);

        
        Http http = new Http();
        HTTPResponse httpRes = http.send(httpReq);

        Boolean success = (httpRes.getStatusCode() == 200);
        if (!success) {
            ErrorLogger.msg(CommonUtility.INTEGRATION_ERROR + ' ' + IntegrationPaymentTemplateSender.class.getName() 
                + '\n' + httpRes.toString() + '\n' + httpRes.getBody() + '\n' + pbIds);
        }

        IntegrationPaymentTemplateHelper.updateSyncStatus_inProgress(success, pbIds);

        return success;
    }

}