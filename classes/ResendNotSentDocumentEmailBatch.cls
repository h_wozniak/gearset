/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Study Enrollment documents list
*	Błąd #1729 - CRM PRODUKCJA - R-00362588 - brak automatycznej wysyłki dokumentów rekrutacyjnych do kandydata
**/

global class ResendNotSentDocumentEmailBatch implements Database.Batchable<sObject> {

	global ResendNotSentDocumentEmailBatch() { }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id
            FROM Enrollment__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY) 
            AND RequiredDocListEmailedCount__c = 0
            AND Status__c = :CommonUtility.ENROLLMENT_STATUS_CONFIRMED
            AND LastModifiedDate >= :System.now().addDays(-2)
        ]);
    }

	global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> studyEnrIds = new Set<Id>();
        for (Enrollment__c studyEnr : enrList) {
            studyEnrIds.add(studyEnr.Id);
        }

        List<Enrollment__c> docEnr = [
            SELECT Id, Enrollment_from_Documents__c, Enrollment_from_Documents__r.Degree__c, Document__c, Enrollment_from_Documents__r.Language_of_Enrollment__c,
                (SELECT Id
                FROM Attachments
                ORDER BY CreatedDate LIMIT 1)
            FROM Enrollment__c
            WHERE Enrollment_from_Documents__c IN :studyEnrIds
            AND (
                Document__c = :CatalogManager.getDocumentAgreementId()
                OR Document__c = :CatalogManager.getDocumentAgreementPLId()
                OR Document__c = :CatalogManager.getDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getPLDocumentPersonalQuestionnareId()
                OR Document__c = :CatalogManager.getDocumentOathId()
                OR Document__c = :CatalogManager.getDocumentOathPLId()
            )
        ];

        Set<Id> enrToSendReqDocList = new Set<Id>();

        for (Enrollment__c studyEnr : enrList) {
            Integer generatedCount = 0;
            Boolean mbaORSp = false;
            String language = '';

            for (Enrollment__c doc : docEnr) {
                if (studyEnr.Id == doc.Enrollment_from_Documents__c) {
                    mbaORSp = (doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_PG || doc.Enrollment_from_Documents__r.Degree__c == CommonUtility.OFFER_DEGREE_MBA);
                    language = doc.Enrollment_from_Documents__r.Language_of_Enrollment__c;
                    if (!doc.Attachments.isEmpty()) {
                        if (!mbaORSp) {
                            generatedCount++;
                        }
                        
                        if (mbaORSp && doc.Document__c != CatalogManager.getDocumentOathId()) {
                            generatedCount++;                            
                        }
                    }
                }
            }
            
            if (language != CommonUtility.ENROLLMENT_LANGUAGE_ENGLISH) {
                if (generatedCount == 3 && !mbaORSp) { // Oath and Agreement and Questionnare Documents 
                    enrToSendReqDocList.add(studyEnr.Id);
                }
                
                if (generatedCount == 2 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnr.Id);
                }
            }
            else {
                if (generatedCount == 6 && !mbaORSp) { // Oath and Agreement and Questionnare Documents 
                    enrToSendReqDocList.add(studyEnr.Id);
                }
                
                if (generatedCount == 4 && mbaORSp) { // Oath and Agreement Documents
                    enrToSendReqDocList.add(studyEnr.Id);
                }                
            }
        }
        
        EmailManager_DocumentListEmail.sendRequiredDocumentList(enrToSendReqDocList);	
	}

	global void finish(Database.BatchableContext BC) { }
}