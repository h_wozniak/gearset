@isTest 
private class ZTestFixEncryptedDocumentGeneration {
    
    private static Map<Integer, sObject> prepareData(Id docId, Id contactId) {
        CustomSettingDefault.initEsbConfig();
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        
        retMap.put(1, ZDataTestUtility.createUniversityOfferStudy(null, true));
        retMap.put(2, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id, Candidate_Student__c = contactId), true));
        retMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(3).Id, Document__c = docId), true));
        
        ZDataTestUtility.createAttachment(new Attachment(ParentId = retMap.get(4).Id, Name = '[nieszyfrowany]att1.docx'), true);
        
        Integer millisec = 3600;
        if(millisec == null || millisec < 0) {
            millisec = 0;
        }
          
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < millisec) {
            finishTime = DateTime.now().getTime();
        }
        
        ZDataTestUtility.createAttachment(new Attachment(ParentId = retMap.get(4).Id, Name = '[nieszyfrowany]att2.docx'), true); 
        
        return retMap;
    }

     @isTest static void test_batch() {
        ZDataTestUtility.prepareCatalogData(true, true);
        
        Contact candidate = ZDataTestUtility.createCandidateContact(new Contact(Phone = '123456789'), true);
        
        Id docId = [SELECT Id FROM Catalog__c WHERE Name = :RecordVals.CATALOG_DOCUMENT_AGREEMENT].Id;
        
        Catalog__c doc = new Catalog__c();
        doc.Id = docId;
        doc.RecordTypeId = CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT);
        update doc;
        
        Test.startTest();
        Map<Integer, sObject> dataMap = prepareData(docId, candidate.Id);        

        Database.executeBatch(new FixEncryptedDocumentGeneration(Datetime.newInstance(2018, 03, 27), Datetime.newInstance(2019, 06, 13, 19, 20, 0)), 200);
        Test.stopTest();
        
        List<Enrollment__c> docEnrs = [
            SELECT Id, RecordTypeId, CreatedDate, Language_of_Main_Enrollment__c, Reference_Number__c, Enrollment_from_Documents__c, Enrollment_from_Documents__r.TECH_Is_Encrypted__c,
             Enrollment_from_Documents__r.Status__c, Enrollment_from_Documents__r.University_Name__c, Document__r.Name, Document__r.Id, Degree__c, Current_File__c, Current_File_Name__c
             FROM Enrollment__c
             WHERE RecordTypeId =: CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
             AND (Document__r.RecordTypeId =: CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_DOCUMENT)
             OR Document__r.Name = 'Kwestionariusz osobowy z podaniem'
             OR Document__r.Name = '[PL] Kwestionariusz osobowy z podaniem')
             AND Enrollment_from_Documents__c = :dataMap.get(3).Id
        ];
        
        System.assertEquals(docEnrs.size(), 2);
        
        List<Attachment> attachmentOnDoc = [
            Select Id, Name, ParentId
            From Attachment
            Where ParentId = :docEnrs.get(1).Id
            AND Name LIKE '%[nieszyfrowany]%'
            ORDER BY CreatedDate DESC
        ];
        
        System.assertEquals(attachmentOnDoc.get(0).Name, '[nieszyfrowany]att2.docx');
        
        System.assertEquals(docEnrs.get(1).Current_File_Name__c, '[nieszyfrowany]att2.docx');
     }
}