/**
* @description  This class is a Trigger Handler for Event - complex logic
**/

public without sharing class TH_Event extends TriggerHandler.DelegateBase {
    List<Event> eventsToRewriteSchoolEmail;

    public override void prepareBefore() {
        eventsToRewriteSchoolEmail = new List<Event>();
    }

//    public override void prepareAfter() { }

    public override void beforeInsert(List<sObject> o) {
        List<Event> newEventList = (List<Event>) o;
        for (Event newEvent : newEventList) {

            /* Sebastian Lasisz */
            // For newly created high school event, rewrite high school contact person's email
            if (newEvent.WhatId != null && newEvent.RecordTypeId == CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)) {
                eventsToRewriteSchoolEmail.add(newEvent);
            }
        }

    }

//    public override void afterInsert(Map<Id, sObject> o) { }

    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Event> oldEvents = (Map<Id, Event>) old;
        Map<Id, Event> newEvents = (Map<Id, Event>) o;
        for (Id key : oldEvents.keySet()) {
            Event oldEvent = oldEvents.get(key);
            Event newEvent = newEvents.get(key);

            /* Sebastian Lasisz */
            // For updated high school event, rewrite high school contact person's email
            if (newEvent.WhatId != oldEvent.WhatId
                && newEvent.WhatId != null && newEvent.RecordTypeId == CommonUtility.getRecordTypeId('Event', CommonUtility.EVENT_RT_HIGH_SCHOOL_VISIT)) {
                eventsToRewriteSchoolEmail.add(newEvent);
            }
        }
    }

//    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) { }

//    public override void beforeDelete(Map<Id, sObject> old) { }

//    public override void afterDelete(Map<Id, sObject> old) { }

    public override void finish() {

        /* Sebastian Lasisz */
        // For newly created or updated high school event, rewrite high school contact person's email
        if (eventsToRewriteSchoolEmail != null && !eventsToRewriteSchoolEmail.isEmpty()) {
            EventManager.rewriteHighSchoolsEmail(eventsToRewriteSchoolEmail);
        }
    }
}