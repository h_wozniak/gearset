/**
*   @author         Sebastian Łasisz
*   @description    batch class that sends emails with Extranet Informations
**/

global class DocDeliveryExtranetBatch implements Database.Batchable<sObject> {    
    Date dayOfEnrollment;
    Set<String> acceptableStatuses = new Set<String> { CommonUtility.ENROLLMENT_STATUS_DIDACTICS_KS, CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS };

    global DocDeliveryExtranetBatch(Integer daysAfterEnrollment) {
        dayOfEnrollment = System.today().addDays(-daysAfterEnrollment);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id
            FROM Enrollment__c 
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY)
            AND (Status__c IN :acceptableStatuses
            OR Unenrolled_Status__c IN :acceptableStatuses)
            AND Educational_Agreement__c != null 
            AND Educational_Agreement__r.TECH_Extranet_Data_Update_Date__c = :dayOfEnrollment
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Enrollment__c> enrList) {
        Set<Id> enrIds = new Set<Id>();
        for (Enrollment__c enr : enrList) {
            enrIds.add(enr.Id);
        }

        EmailManager_StudyExtranetEmail.sendExtranetInformationnEmail(enrIds);
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}