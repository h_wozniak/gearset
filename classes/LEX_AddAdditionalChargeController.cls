/**
 * Created by Sebastian on 22.01.2018.
 */

public with sharing class LEX_AddAdditionalChargeController {

    @AuraEnabled
    public static String getEnrollmentStatus(Id studyEnrId) {
        Enrollment__c studyEnrollment = [
                SELECT Id, Status__c
                FROM Enrollment__c
                WHERE Id = :studyEnrId
        ];

        return studyEnrollment.Status__c;
    }

    @AuraEnabled
    public static List<AdditionalChargeController> getAdditionalChargesAsPicklist(Id studyEnrId) {
        Map<String, AdditionalCharges__c> customSettingMap = AdditionalCharges__c.getAll().clone();

        List<Payment__c> exisitingCharges = [
                SELECT Id, Name, Description__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :studyEnrId
                AND Type__c = :CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE
        ];

        for (Payment__c charge : exisitingCharges) {
            customSettingMap.remove(charge.Description__c);
        }

        if (customSettingMap.isEmpty()) {
            return null;
        }

        List<AdditionalChargeController> values = new List<AdditionalChargeController>();
        for (AdditionalCharges__c charge : customSettingMap.values())
        {
            values.add(
                new AdditionalChargeController(
                    charge.Name + ' (' + charge.Price__c.format() + ' ' + getCurrencySymbolFromIso(CommonUtility.CURRENCY_CODE_PLN) + ')',
                    charge.Name
                )
            );
        }

        return values;
    }

    public static String getCurrencySymbolFromIso(String iso) {
        Map<String, String> currencySymbolMap = new Map<String, String> {
                'USD' => '$',
                'EUR' => '&euro;',
                'GBP' => '&pound;',
                'PLN' => 'zł'
        };
        return currencySymbolMap.get(iso);
    }

    @AuraEnabled
    public static String addAdditionalCharge(Id studyEnrId, String chargeName) {
        WebserviceUtilities.WS_Response response;

        AdditionalCharges__c charge = AdditionalCharges__c.getInstance(chargeName);

        List<Payment__c> tuitionPaymentList = [
                SELECT Payment_Deadline__c, Installment_Year_Calendar__c, Installment_Semester__c, OwnerId
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :studyEnrId
                AND Type__c = :CommonUtility.PAYMENT_TYPE_TUITION_FEE
                ORDER BY Payment_Deadline__c ASC
                LIMIT 1
        ];

        if (charge != null) {
            Payment__c newCharge = new Payment__c(
                    Description__c = chargeName,
                    RecordTypeId = CommonUtility.getRecordTypeId('Payment__c', CommonUtility.PAYMENT_RT_SCHEDULE_INST),
                    Type__c = CommonUtility.PAYMENT_TYPE_ADDITIONAL_CHARGE,
                    Enrollment_from_Schedule_Installment__c = studyEnrId,
                    PriceBook_Value__c = charge.Price__c,
                    Is_added_manually__c = true
            );

            if (!tuitionPaymentList.isEmpty()) {
                newCharge.Payment_Deadline__c = tuitionPaymentList[0].Payment_Deadline__c;
                newCharge.Installment_Year_Calendar__c = tuitionPaymentList[0].Installment_Year_Calendar__c;
                newCharge.Installment_Semester__c = tuitionPaymentList[0].Installment_Semester__c;
                newCharge.OwnerId = tuitionPaymentList[0].OwnerId;
            }

            try {
                insert newCharge;

                response = new WebserviceUtilities.WS_Response(true, null, null);
            } catch(Exception e) {
                String errorMsg = CommonUtility.trimErrorMessage(e.getMessage());
                if (errorMsg == null) {
                    ErrorLogger.log(e);
                    errorMsg = Label.msg_error_UnexpectedError;
                }
                response = new WebserviceUtilities.WS_Response(false, errorMsg, null);
            }
        } else {
            response = new WebserviceUtilities.WS_Response(false, Label.msg_error_UnexpectedError, null);
        }

        return JSON.serialize(response);
    }

    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class AdditionalChargeController {
        @AuraEnabled  public String label {get; set;}
        @AuraEnabled  public String value {get; set;}

        public AdditionalChargeController(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}