public with sharing class LEX_Send_InformatorController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}

	@AuraEnabled
	public static String sendContactsToIPresso(String recordId) {
		return LEXStudyUtilities.sendInformatorToIpresso(recordId) ? 'SUCCESS' : 'FAILURE';
	}
}