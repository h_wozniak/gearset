@isTest
private class ZTestDocGen_GenerateDatesForUnenrolled {


    private static Map<Integer, sObject> prepareDataForTest() {
    	Boolean isWinterEnrollment = Date.today().month() >= 2 && Date.today().month() <= 7;
      	String periodOfRecrutation = isWinterEnrollment ? CommonUtility.CATALOG_PERIOD_WINTER : CommonUtility.CATALOG_PERIOD_SUMMER;

        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(new Catalog__c(Period_of_Recrutation__c = periodOfRecrutation), true));

        return retMap;
	}

	@isTest static void test_generateUnenrolledAgreementTill() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.generateUnenrolledAgreementTill(dataMap.get(3).Id);
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '20.04.' + (studyOffer.Study_Start_Date__c.year()));
	}

	@isTest static void test_generateDateOfWidthrawal() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.generateDateOfWidthrawal(dataMap.get(3).Id);
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '30.04.' + (studyOffer.Study_Start_Date__c.year()));
	}

	@isTest static void test_generateResignationTill() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.generateResignationTill(dataMap.get(3).Id);
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '25.04.' + (studyOffer.Study_Start_Date__c.year()));
	}

	@isTest static void test_generatePeriodOfRecrutation() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

      	Enrollment__c docEnr = [
    	    SELECT Id, Enrollment_From_documents__r.University_Name__c,
	        Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, 
	            Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
	        FROM Enrollment__c
	        WHERE Id = :dataMap.get(3).Id
    	];

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.generatePeriodOfRecrutation(docEnr);
		Test.stopTest();

      	if (Date.today().month() >= 2 && Date.today().month() <= 7) {
			System.assertEquals(result, CommonUtility.CATALOG_PERIOD_WINTER);
		}
		else {
			System.assertEquals(result, CommonUtility.CATALOG_PERIOD_SUMMER);			
		}
	}

	@isTest static void test_determineStudyStartDate() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

      	Enrollment__c docEnr = [
    	    SELECT Id, Enrollment_From_documents__r.University_Name__c,
	        Enrollment_From_documents__r.Course_or_Specialty_Offer__r.University_Study_Offer_from_Course__r.Study_Start_Date__c, 
	            Enrollment_From_documents__r.Course_or_Specialty_Offer__r.Course_Offer_from_Specialty__r.University_Study_Offer_from_Course__r.Study_Start_Date__c
	        FROM Enrollment__c
	        WHERE Id = :dataMap.get(3).Id
    	];

		Test.startTest();
		Date result = DocGen_GenerateDatesForUnenrolled.determineStudyStartDate(docEnr);
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, studyOffer.Study_Start_Date__c);
	}

	@isTest static void test_executeMethod_generateUnenrolledAgreementTill() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.executeMethod(dataMap.get(3).Id, null, 'generateUnenrolledAgreementTill');
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '20.04.' + (studyOffer.Study_Start_Date__c.year()));
	}

	@isTest static void test_executeMethod_generateDateOfWidthrawal() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.executeMethod(dataMap.get(3).Id, null, 'generateDateOfWidthrawal');
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '30.04.' + (studyOffer.Study_Start_Date__c.year()));		
	}

	@isTest static void test_executeMethod_generateResignationTill() {
		Map<Integer, sObject> dataMap = prepareDataForTest();

		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.executeMethod(dataMap.get(3).Id, null, 'generateResignationTill');
		Test.stopTest();

		Offer__c studyOffer = (Offer__c)dataMap.get(0);

		System.assertEquals(result, '25.04.' + (studyOffer.Study_Start_Date__c.year()));		
	}

	@isTest static void test_executeMethod_emptyName() {
		Test.startTest();
		String result = DocGen_GenerateDatesForUnenrolled.executeMethod(null, null, '');
		Test.stopTest();		

		System.assertEquals(result, '');
	}
}