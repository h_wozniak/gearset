/**
*   @author         Grzegorz Długosz
*   @description    This class is used to make dictionaries available for external external systems.
**/

@RestResource(urlMapping = '/dictionary/*')
global without sharing class IntegrationDictionaryResource {


    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------------- HTTP METHODS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */


    @HttpGet
    global static void getDictionary() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String dictionaryId = RestContext.request.params.get('dictionary_id');

        if (dictionaryId == null || dictionaryId == '') {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(605);
            return ;
        }

        dictionaryId = dictionaryId.toLowerCase();

        if (IntegrationPicklistDictionaryHelper.dictKeyToSObjFieldMap.containsKey(dictionaryId)) {
            res.responseBody = Blob.valueOf(IntegrationPicklistDictionaryHelper.getDictionaryPicklist(dictionaryId));
            res.statusCode = 200;
            return ;
        }

        else if (dictionaryId.equals('universities')) {

            String dateFrom = RestContext.request.params.get('date_from');
            String stringWsb = RestContext.request.params.get('wsb');
            Boolean wsb;

            if (stringWsb == null || stringWsb == '') {
                wsb = false;
            }
            else {
                wsb = Boolean.valueOf(RestContext.request.params.get('wsb'));
            }

            if (dateFrom == null || dateFrom == '') {
                res.responseBody = Blob.valueOf(IntegrationUniversityDictionaryHelper.getDictionaryUniversitiesNoDate(wsb));
                res.statusCode = 200;
                return ;
            }

            else {
                res.responseBody = Blob.valueOf(IntegrationUniversityDictionaryHelper.getDictionaryUniversities(dateFrom, wsb));
                res.statusCode = 200;
                return ;
            }
                
        }

        else if (dictionaryId.equals('schools')) {

            String dateFrom = RestContext.request.params.get('date_from');

            if (dateFrom == null || dateFrom == '') {
                res.responseBody = Blob.valueOf(IntegrationHighSchoolDictionaryHelper.getDictionaryHighSchoolNoDate());
                res.statusCode = 200;
                return ;
            }

            res.responseBody = Blob.valueOf(IntegrationHighSchoolDictionaryHelper.getDictionaryHighSchool(dateFrom));
            res.statusCode = 200;
            return ;
        }

        else if (dictionaryId.equals('universities_ipresso')) {

            String dateFrom = RestContext.request.params.get('date_from');
            String stringWsb = RestContext.request.params.get('wsb');
            Boolean wsb;

            if (stringWsb == null || stringWsb == '') {
                wsb = false;
            }
            else {
                wsb = Boolean.valueOf(RestContext.request.params.get('wsb'));
            }

            if (dateFrom == null || dateFrom == '') {
                res.responseBody = Blob.valueOf(IntegrationUniversityDictionaryHelper.getDictionaryUniversitiesNoDate_Ipresso(wsb));
                res.statusCode = 200;
                return ;
            }

            else {
                res.responseBody = Blob.valueOf(IntegrationUniversityDictionaryHelper.getDictionaryUniversities_iPresso(dateFrom, wsb));
                res.statusCode = 200;
                return ;
            }
                
        }

        else if (dictionaryId.equals('consents')) {
            res.responseBody = Blob.valueOf(IntegrationConsentDictionaryHelper.getDictionaryConsents());
            res.statusCode = 200;
            return ;
        }

        else {
            res.statusCode = 400;
            res.responseBody = IntegrationError.getErrorJSONBlobByCode(900);
            return ;
        }
    }

}