@isTest
private class ZTestMarketingNewButtonOverride {

	@isTest static void test_MarketingNewButtonOverride() {
        PageReference pageRef = Page.MarketingNewButtonOverride;
        Test.setCurrentPageReference(pageRef);

		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.standardController(new Marketing_Campaign__c());
		MarketingNewButtonOverride cntrl = new MarketingNewButtonOverride(stdController);
		Test.stopTest();

		System.assert(!cntrl.recordTypeList.isEmpty());

        System.assertNotEquals(null, cntrl.continueStep());
	}
}