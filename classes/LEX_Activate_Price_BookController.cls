public with sharing class LEX_Activate_Price_BookController {
	
	@AuraEnabled
	public static Boolean hasEditAccess(String offerId) {
		return LEXServiceUtilities.hasEditAccess(offerId);
	}

	@AuraEnabled
	public static Boolean isActive(String offerId) {
		return [SELECT Active__c FROM Offer__c WHERE Id = :offerId LIMIT 1].Active__c;
	}

	@AuraEnabled
	public static Boolean activatePriceBooks(String offerId) {
		return PriceBookManager.activatePriceBooks(new List<String>{offerId});
	}
}