global class CountryDataFixBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global CountryDataFixBatch() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, Country_of_Origin__c, Country_of_Birth__c, Country_of_Issue__c
            FROM Contact
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Contact> contactsToUpdate) {
        Map<String, String> picklistValuesMap = DictionaryTranslator.getTranslatedPicklistValues('PL', Contact.Country_of_Birth__c);

        //for (Contact c : contactsToUpdate) {
        //    for (String value : picklistValuesMap.keySet()) {
        //        if (c.Country_of_Issue2__c != null && c.Country_of_Issue2__c.toLowerCase() == picklistValuesMap.get(value)) {
        //             c.Country_of_Issue__c = value;
        //        }
        //        else if (c.Country_of_Issue2__c == 'Poland') {
        //            c.Country_of_Issue__c = c.Country_of_Issue2__c;
        //        }
                
        //        if (c.Country_of_Birth2__c != null && c.Country_of_Birth2__c.toLowerCase() == picklistValuesMap.get(value)) {
        //            c.Country_of_Birth__c = value;
        //        }
        //        else if (c.Country_of_Birth2__c == 'Poland') {
        //            c.Country_of_Birth__c = c.Country_of_Birth2__c;
        //        }
                
        //        if (c.Country_of_Origin2__c != null && c.Country_of_Origin2__c.toLowerCase() == picklistValuesMap.get(value)) {
        //            c.Country_of_Origin__c = value;
        //        }
        //        else if (c.Country_of_Origin2__c == 'Poland') {
        //            c.Country_of_Origin__c = c.Country_of_Origin2__c;
        //        }
        //    }
        //}

        update contactsToUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}