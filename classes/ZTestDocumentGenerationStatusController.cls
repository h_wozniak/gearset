@isTest
private class ZTestDocumentGenerationStatusController {

    private static Map<Integer, sObject> prepareDataForTest() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        ZDataTestUtility.prepareCatalogData(true, true);

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = CommonUtility.OFFER_DEGREE_II, Study_Start_Date__c = System.today()), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c( Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, [
            SELECT Id
            FROM Enrollment__c
            WHERE Enrollment_from_Documents__c = :retMap.get(2).Id
            AND Document__c = :CatalogManager.getDocumentAcceptanceId()
        ]);

        return retMap;
	}

    @isTest static void test_DocumentGenerationStatusController_study() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(2));

        Enrollment__c docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = CommonUtility.DURING_GENERATION;
        update docEnr;

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        System.assert(!cntrl.enrollmentDocuments.isEmpty());
        System.assertEquals(cntrl.numberOfDocuments, 1);
        System.assertEquals(cntrl.enrollmentType, CommonUtility.ENROLLMENT_RT_ENROLLMENT_STUDY);
    }

    @isTest static void test_DocumentGenerationStatusController_document() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(3));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        System.assertEquals(cntrl.enrollmentDocuments, null);
        System.assertEquals(cntrl.numberOfDocuments, null);
        System.assertEquals(cntrl.enrollmentType, CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT);
    }

    @isTest static void test_checkDocumentGenerationStudy_withoutDocChange() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(2));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        cntrl.checkDocumentGenerationStudy();
        System.assertEquals(cntrl.enrDocSize, 0);
        System.assertEquals(cntrl.generationStatus, false);
    }

    @isTest static void test_checkDocumentGenerationStudy_withDocChange() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(2));

        Enrollment__c docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = CommonUtility.DURING_GENERATION;
        update docEnr;

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);   
        Test.stopTest();

        System.assertEquals(cntrl.enrollmentDocuments.size(), 1);

        docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = 'New file';
        update docEnr;

        cntrl.checkDocumentGenerationStudy();
        System.assertEquals(cntrl.enrDocSize, 1);
        System.assertEquals(cntrl.generationStatus, true);
    }

    @isTest static void test_checkDocumentGenerationDocument_withoutChange() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(3));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        cntrl.checkDocumentGenerationDocument();
        System.assertEquals(cntrl.generationStatus, false);
    }

    @isTest static void test_checkDocumentGenerationDocument_withChange() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(3));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        Enrollment__c docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = 'New file';
        update docEnr;

        cntrl.checkDocumentGenerationDocument();
        System.assertEquals(cntrl.generationStatus, true);    	
    }

    @isTest static void test_checkDocumentGenerationDocument() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(3));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        Enrollment__c docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = 'New file';
        update docEnr;

        cntrl.checkDocumentGenerationDocument();
        System.assertEquals(cntrl.getGenerationStatus(), true);    	

    }

    @isTest static void test_disablePopup() {
    	Map<Integer, sObject> dataMap = prepareDataForTest();

        PageReference pageRef = Page.DocumentGenerationStatus;
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.standardController((Enrollment__c)dataMap.get(3));

        Test.startTest();
        DocumentGenerationStatusController cntrl = new DocumentGenerationStatusController(stdController);
        Test.stopTest();

        Enrollment__c docEnr = [SELECT Id, Current_file__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];

        docEnr.Current_File__c = 'New file';
        update docEnr;

        cntrl.disablePopup();
        System.assertEquals(cntrl.showedPopup, true);    	

    }

}