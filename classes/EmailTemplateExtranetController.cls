/**
* @author       Sebastian Łasisz
* @description  Class used to retrieve information about Extranet login data. 
**/

public without sharing class EmailTemplateExtranetController {
    
    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------------------------- INIT ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public String enrollmentId { get; set; }

	public EmailTemplateExtranetController() { }

    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- HELPER METHODS ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public String getExtranetLogin() {
        List<Enrollment__c> studyEnr = [
            SELECT Id, Educational_Agreement__r.Portal_Login__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

    	return studyEnr.size() > 0 ? studyEnr.get(0).Educational_Agreement__r.Portal_Login__c : null;
    }

    public String getExtranetIndexNumber() {
        List<Enrollment__c> studyEnr = [
            SELECT Id, Educational_Agreement__r.Student_no__c
            FROM Enrollment__c
            WHERE Id = :enrollmentId
        ];

        return studyEnr.size() > 0 ? studyEnr.get(0).Educational_Agreement__r.Student_no__c : '';
    }
}