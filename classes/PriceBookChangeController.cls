global with sharing class PriceBookChangeController {

    /**
    * @author       Sebasian Łasisz
    * @description  This class is a controller for PriceBookChange.page, which allows price book changing
    **/

    public Boolean detailMode { get; set; }
    
    public List<PriceBook_Wrapper> priceBookWrapperList { get; set; }
    public List<SelectOption> installmentsPerYearList { get; set; }
    public List<SelectOption> tuitionSystemList { get; set; }
    
    public PriceBook_Wrapper selectedPriceBook { get; set;}
    public String selectedInstallmentsPerYear { get; set; }
    public String selectedTuitionSystem { get; set; }
    public String selectedPBOption { get; set; }

    public Contact candidateStudent { get; set; }
    public Offer__c offer { get; set; }
    public Enrollment__c enrollment { get; set; }

    /* for view */
    public String ONETIME_PAYMENT { get { return CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME; } }
    


    public PriceBookChangeController() {
        String enrollmentId = Apexpages.currentPage().getParameters().get('enrollmentId');

        getRequiredData(enrollmentId);

        createPriceBookWrapperList();
        
        setSelectedPriceBook();
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- INIT METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private void getRequiredData(Id enrollmentId) {
        enrollment = [
            SELECT Id, Candidate_Student__c, Price_Book_from_Enrollment__c, Course_or_Specialty_Offer__c, Tuition_System__c, 
            Installments_per_Year__c 
            FROM Enrollment__c 
            WHERE Id = :enrollmentId
        ];

        candidateStudent = [
            SELECT Id, Foreigner__c 
            FROM Contact 
            WHERE Id = :enrollment.Candidate_Student__c
        ];

        offer = [
            SELECT Id, Name, RecordTypeId, Course_Offer_from_Specialty__c 
            FROM Offer__c 
            WHERE Id = :enrollment.Course_or_Specialty_Offer__c
        ];
    }

    private List<Offer__c> getPriceBooksForOffer(Id offerId) {
        if (candidateStudent.Foreigner__c) {
            return [
                SELECT Id, Name, Installment_Variants__c, Graded_tuition__c, Onetime_Price__c, ToLabel(Price_Book_Type__c),
                Number_of_Semesters_formula__c, Entry_fee__c, Enrollment_fee__c
                FROM Offer__c 
                WHERE Offer_from_Price_Book__c = :offerId 
                AND (
                    (
                        ((Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD AND Currently_in_use__c = true) 
                        OR (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_SPECIAL AND Active__c = true) 
                        OR (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_FOREIGNERS AND Currently_in_use__c = true))
                    )
                    OR Id = :enrollment.Price_Book_from_Enrollment__c
                )
            ];
        }
        else {
            return [
                SELECT Id, Name, Installment_Variants__c, Graded_tuition__c, Onetime_Price__c, ToLabel(Price_Book_Type__c),
                Number_of_Semesters_formula__c, Entry_fee__c, Enrollment_fee__c 
                FROM Offer__c 
                WHERE Offer_from_Price_Book__c = :offerId 
                AND (
                    (
                        ((Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_STANDARD AND Currently_in_use__c = true) 
                        OR (Price_Book_Type__c = :CommonUtility.OFFER_PBTYPE_SPECIAL AND Active__c = true))
                    )
                    OR Id = :enrollment.Price_Book_from_Enrollment__c
                )
            ];
        }
    }

    private void createPriceBookWrapperList() {
        priceBookWrapperList = new List<PriceBook_Wrapper>();
        for (Offer__c priceBook : getPriceBooksForEnrollment()) {
            selectedPriceBook = new PriceBook_Wrapper();
            selectedPriceBook.name = priceBook.Name;
            selectedPriceBook.priceBookOffer = priceBook;
            selectedPriceBook.tuitionSystem = PriceBookManager.getTutitionSystemList(priceBook);
            selectedPriceBook.installmentsPerYear = PriceBookManager.getTuitionModePaymentList(priceBook);
            selectedPriceBook.hasGradedPrices = priceBook.Number_of_Semesters_formula__c > 2 && priceBook.Graded_tuition__c;
            selectedPriceBook.installmentConfigList = PriceBookManager.getInstallmentConfigList(priceBook.Id);

            priceBookWrapperList.add(selectedPriceBook);
        }
    }
    
    private List<Offer__c> getPriceBooksForEnrollment() {
        List<Offer__c> priceBookList = new List<Offer__c>();

        priceBookList.addAll(getPriceBooksForOffer(offer.Id));
        if (offer.RecordTypeId == CommonUtility.getRecordTypeId('Offer__c', CommonUtility.OFFER_RT_COURSE_SPECIALTY_OFFER)) {
            priceBookList.addAll(getPriceBooksForOffer(offer.Course_Offer_from_Specialty__c));
        }

        return priceBookList;
    }

    public List<SelectOption> getPriceBookItems() {
        List<SelectOption> selectOptionList = new List<SelectOption>();
        for (PriceBook_Wrapper priceBook : priceBookWrapperList) {
            selectOptionList.add(new SelectOption(priceBook.priceBookOffer.Id, priceBook.name + ', ' + priceBook.priceBookOffer.Price_Book_Type__c));
        }
        return selectOptionList;
    }

    private void setSelectedPriceBook() {
        if (enrollment.Price_Book_from_Enrollment__c != null) {
            Offer__c currentPriceBook = [
                SELECT Id, Name, Installment_Variants__c, Graded_tuition__c, Onetime_Price__c, Price_Book_Type__c, Number_of_Semesters_formula__c 
                FROM Offer__c 
                WHERE Id = :enrollment.Price_Book_from_Enrollment__c
            ];
            
            selectedPriceBook = new PriceBook_Wrapper();
            selectedPriceBook.name = currentPriceBook.Name;
            selectedPriceBook.priceBookOffer = currentPriceBook;
            selectedPriceBook.hasGradedPrices = currentPriceBook.Number_of_Semesters_formula__c > 2 && currentPriceBook.Graded_tuition__c;
            selectedPriceBook.installmentConfigList = PriceBookManager.getInstallmentConfigList(currentPriceBook.Id);

            installmentsPerYearList =  PriceBookManager.getTuitionModePaymentList(currentPriceBook);
            tuitionSystemList = PriceBookManager.getTutitionSystemList(currentPriceBook);
            selectedPBOption = currentPriceBook.Id;
            updateSelectedFields();
        }
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------------- USER ACTIONS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    public PageReference tuitionSystemChange() {
        if (selectedTuitionSystem == CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME) {
            selectedInstallmentsPerYear = String.valueOf(1);
        }
        
        return null;
    }

    public PageReference cancel() {
        return new PageReference('/' + enrollment.Id);
    }

    public void updateSelectedFields() {
        for (PriceBook_Wrapper pbw : priceBookWrapperList) {
            if (pbw.priceBookOffer.Id == selectedPBOption) {
                selectedPriceBook = pbw;
                installmentsPerYearList = pbw.installmentsPerYear;
                tuitionSystemList = pbw.tuitionSystem;
            }
        } 
        for (SelectOption installmentOpt : installmentsPerYearList) {
            if (installmentOpt.getValue() == enrollment.Installments_per_Year__c) {
                selectedInstallmentsPerYear = enrollment.Installments_per_Year__c;
            }
        }
        
        for (SelectOption tuitionOpt : tuitionSystemList) {
            if (tuitionOpt.getValue() == enrollment.Tuition_System__c) {
                selectedTuitionSystem = enrollment.Tuition_System__c;
            }
        }           
    }

    public PageReference save() {
        try {
            enrollment.Price_Book_from_Enrollment__c = selectedPriceBook.priceBookOffer.Id;
            enrollment.Tuition_System__c = selectedTuitionSystem;
            enrollment.Installments_per_Year__c = selectedInstallmentsPerYear;
            update enrollment;
        }
        catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }

        return new PageReference('/' + enrollment.Id);
    }







    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @TestVisible
    private class PriceBook_Wrapper {
        public String name { get; set; }
        public Offer__c priceBookOffer { get; set; }
        public List<Offer__c> installmentConfigList { get; set; }
        public List<SelectOption> installmentsPerYear { get; set; }
        public List<SelectOption> tuitionSystem { get; set; }
        public Boolean selected { get; set; }
        public Boolean hasGradedPrices { get; set; }
    }
}