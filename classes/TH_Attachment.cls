public without sharing class TH_Attachment extends TriggerHandler.DelegateBase {

    List<Attachment> attachmentsToCheckIfCanEditDescription;
    Set<Id> enrollmentsToUpdateCurrentFile;
    Set<Id> docsToCheckIfGeneratingComplete;
    Set<Id> docsToCheckUnenrolledGeneration;
    Set<Id> agreementsToCheckIfGenerated;
    Set<Id> firstVersionAgreement;
    Set<Id> returningLeadsWithAttachment;

    public override void prepareBefore() {
        attachmentsToCheckIfCanEditDescription = new List<Attachment>();
    }
    
    public override void prepareAfter() {
        enrollmentsToUpdateCurrentFile = new Set<Id>();
        docsToCheckIfGeneratingComplete = new Set<Id>();
        docsToCheckUnenrolledGeneration = new Set<Id>();
        agreementsToCheckIfGenerated = new Set<Id>();
        firstVersionAgreement = new Set<Id>();
        returningLeadsWithAttachment = new Set<Id>();
    }
    
    //public override void beforeInsert(List<sObject> o) {}

    public override void afterInsert(Map<Id, sObject> o) {
        Map<Id, Attachment> newAttachments = (Map<Id, Attachment>)o;
        Attachment newAttachment;

        for(Id key : newAttachments.keySet()) {
            newAttachment = newAttachments.get(key);

            /* Sebastian Łasisz */
            // update current file on Enrollment Document
            if (!CommonUtility.preventUpdatingCurrentFileOnEnrollment) {
                enrollmentsToUpdateCurrentFile.add(newAttachment.ParentId);
            }

            /* Sebastian Łasisz */
            // check and send Document List after Oath and Agreement Documents finished generating
            if (!CommonUtility.preventSendingEmailWithDocuments) {
                docsToCheckIfGeneratingComplete.add(newAttachment.ParentId);
            }

            /* Wojciech Słodziak */
            // check and send Document List after Agreement Document for Unenrolled Candidate finished generating
            docsToCheckUnenrolledGeneration.add(newAttachment.ParentId);

            /* Sebastian Łasisz */
            // update enrollment study Verify Agreement checkbox if Agreement has been generated
            agreementsToCheckIfGenerated.add(newAttachment.ParentId);

            /* Sebastian Łasisz */
            // update first version of agreement on enrollment study
            firstVersionAgreement.add(newAttachment.ParentId);
            
            /* Karolina Kowalik */
            // confirm when Returning Lead has attachment
            returningLeadsWithAttachment.add(newAttachment.ParentId);
        }
    }
    
    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        for (Id key : old.keySet()) {
            Attachment aOld = (Attachment)old.get(key);
            Attachment aNew = (Attachment)o.get(key);

            /* Sebastian Łasisz */
            // Disable editing description Field on Attachment
            if (aNew.Description != aOld.Description && aOld.ParentId != null) {
                attachmentsToCheckIfCanEditDescription.add(aNew);
            } 
        }
    }
 
    public override void afterUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {
        Map<Id, Attachment> oldAttachments = (Map<Id, Attachment>)old;
        Map<Id, Attachment> newAttachments = (Map<Id, Attachment>)o;
        for(Id key : oldAttachments.keySet()) {
            Attachment oldAttachment = oldAttachments.get(key);
            Attachment newAttachment = newAttachments.get(key);

            /* Sebastian Łasisz */
            // update current file on Enrollment Document with previous attachment
            if (!CommonUtility.preventUpdatingCurrentFileOnEnrollment) {
                enrollmentsToUpdateCurrentFile.add(oldAttachment.ParentId);
            }
            
            /* Karolina Kowalik */
            // confirm when Returning Lead has attachment
            returningLeadsWithAttachment.add(newAttachment.ParentId);
        }
    }

    public override void afterDelete(Map<Id, sObject> old) {
        Map<Id, Attachment> oldAttachments = (Map<Id, Attachment>)old;
        Attachment oldAttachment;
        for(Id key : oldAttachments.keySet()) {
            oldAttachment = oldAttachments.get(key);

            /* Sebastian Łasisz */
            // update current file on Enrollment Document with previous attachment
            if (!CommonUtility.preventUpdatingCurrentFileOnEnrollment) {
                enrollmentsToUpdateCurrentFile.add(oldAttachment.ParentId);
            }
        }
    }
    
    public override void finish() {

        /* Sebastian Łasisz */
        // update current file on Enrollment Document
        if (enrollmentsToUpdateCurrentFile != null && !enrollmentsToUpdateCurrentFile.isEmpty()) {
            AttachmentManager.updateCurrentFileOnEnrollment(enrollmentsToUpdateCurrentFile);
        }

        /* Sebastian Łasisz */
        // check and send Document List after Oath and Agreement Documents finished generating
        if (docsToCheckIfGeneratingComplete != null && !docsToCheckIfGeneratingComplete.isEmpty()) {
            AttachmentManager.checkSendReqDocList(docsToCheckIfGeneratingComplete);
        }

        /* Wojciech Słodziak */
        // check and send Document List after Agreement Document for Unenrolled Candidate finished generating
        if (docsToCheckUnenrolledGeneration != null && !docsToCheckUnenrolledGeneration.isEmpty()) {
            //AttachmentManager.checkSendReqDocList_Unenrolled(docsToCheckUnenrolledGeneration);
        }

        /* Sebastian Łasisz */
        // update enrollment study Verify Agreement checkbox if Agreement has been generated
        if (agreementsToCheckIfGenerated != null && !agreementsToCheckIfGenerated.isEmpty()) {
            EnrollmentManager_Studies.updateStudyEnrollmentOnAgreementGenerated(agreementsToCheckIfGenerated);
        }

        /* Sebastian Łasisz */
        // update first version of agreement on enrollment study
        if (firstVersionAgreement != null && !firstVersionAgreement.isEmpty()) {
            //EnrollmentManager_Studies.updateGeneratedFirstVersionAgreement(firstVersionAgreement);
        }
        
        /* Karolina Kowalik */
        // confirm when Returning Lead has attachment
        if (returningLeadsWithAttachment != null && !returningLeadsWithAttachment.isEmpty()) {
            AttachmentManager.confirmReturningLeadsWithAttachment(returningLeadsWithAttachment);
        }
    }
}