public with sharing class LEX_RelatedListDataGridController {
    public static Map<Schema.DisplayType, String> typesMap = new Map<Schema.DisplayType, String>();

    static {
        typesMap.put(Schema.DisplayType.TextArea, 'TextArea');
        typesMap.put(Schema.DisplayType.STRING, 'String');
        typesMap.put(Schema.DisplayType.BOOLEAN, 'Boolean');
        typesMap.put(Schema.DisplayType.COMBOBOX, 'Combobox');
        typesMap.put(Schema.DisplayType.CURRENCY, 'Currency');
        typesMap.put(Schema.DisplayType.DATE, 'Date');
        typesMap.put(Schema.DisplayType.DATETIME, 'Datetime');
        typesMap.put(Schema.DisplayType.DOUBLE, 'Double');
        typesMap.put(Schema.DisplayType.ADDRESS, 'Adress');
        typesMap.put(Schema.DisplayType.EMAIL, 'Email');
        typesMap.put(Schema.DisplayType.INTEGER, 'Integer');
        typesMap.put(Schema.DisplayType.PERCENT, 'Percent');
        typesMap.put(Schema.DisplayType.MULTIPICKLIST, 'Multi PickList');
        typesMap.put(Schema.DisplayType.PICKLIST, 'PickList');
        typesMap.put(Schema.DisplayType.PHONE, 'Phone');
        typesMap.put(Schema.DisplayType.REFERENCE, 'Reference');
        typesMap.put(Schema.DisplayType.URL, 'Url');
    }

    /**
    * Clean the related list metadata
	* -If the field is AutoNumber or Calculated set the type to Formula. Otherwise set the type from the map
	* -If the column is not an inner field set the type to Reference
	* -If the field is not accessible remove the field from columns list
	* */
    private static void cleanRelatedListMetadata(Map<String, Object> mRelatedListMetadata) {
        Object[] columns = (Object[]) mRelatedListMetadata.get('columns');
        String relatedObjectName = (String) mRelatedListMetadata.get('sobject');

        Schema.SObjectType relatedObjType = Schema.getGlobalDescribe().get(relatedObjectName);
        Schema.DescribeSObjectResult relatedObjDesc = relatedObjType.getDescribe();

        for (Integer i = 0; i < columns.size(); i++) {
            Map<String, Object> mColumn = (Map<String, Object>) columns.get(i);
            String columnKey = (String) mColumn.get('name');

            //If it's a Picklist field
            if (columnKey.startsWith('toLabel(')) {
                columnKey = columnKey.substringBetween('toLabel(', ')');
                mColumn.put('name', columnKey);
            }
            //If it's a Reference field
            if (columnKey.endsWith('.Name')) {
                mColumn.put('refObjName', ((String) mColumn.get('field')).replace('.Name', ''));
                if (((String) mColumn.get('lookupId')) != null) {
                    columnKey = ((String) mColumn.get('lookupId')).replace('.', '');
                }
                else {
                    columnKey = ((String) mColumn.get('lookupId'));
                }
                mColumn.put('name', columnKey);
            }

            Schema.SObjectField sfield = relatedObjDesc.fields.getMap().get(columnKey);
            if (sfield != null) {
                DescribeFieldResult fieldDescResult = sfield.getDescribe();

                if (fieldDescResult.isAccessible()) {
                    //Set requied flag
                    mColumn.put('required', JSON.serialize(!fieldDescResult.isNillable()));

                    //Set precision
                    mColumn.put('precision', JSON.serialize(fieldDescResult.getPrecision()));

                    //Set picklistValues
                    mColumn.put('options', JSON.serialize(fieldDescResult.getPicklistValues()));

                    //Set digits
                    mColumn.put('digits', JSON.serialize(fieldDescResult.getDigits()));

                    //Set length
                    mColumn.put('length', JSON.serialize(fieldDescResult.getLength()));

                    //Set type
                    mColumn.put('type', typesMap.get(fieldDescResult.getType()));

                    //Set type for AutoNumber and Formula Fields
                    if (fieldDescResult.isAutoNumber() || fieldDescResult.isCalculated()) {
                        mColumn.put('calculatedType', mColumn.get('type'));
                        mColumn.put('type', 'Formula');
                    }
                } else {
                    columns.remove(i);
                }
            } else {
                mColumn.put('type', 'Reference');
            }
        }
    }

    /**
    * Return the list of related metadata information
    * */
    @AuraEnabled
    public static Object[] getReleatedListsMetadata(Id objectId) {
        String recordTypeName = LEX_RestAPIUtils.getObjectRecordTypeName(objectId);
        if (recordTypeName == 'Master' || recordTypeName == 'Principal') {
            recordTypeName = null;
        }
        Object[] relatedListsMetatdata = LEX_RestAPIUtils.getRelatedListsMetadata(objectId, recordTypeName);

        for (Object relatedListMetadata : relatedListsMetatdata) {
            cleanRelatedListMetadata((Map<String, Object>) relatedListMetadata);
        }

        return relatedListsMetatdata;
    }

    /**
    * Return the metadata of the related list
    * */
    @AuraEnabled
    public static Object getReleatedListMetadata(Id objectId, String relatedListName) {
        String recordTypeName = LEX_RestAPIUtils.getObjectRecordTypeName(objectId);
        if (recordTypeName == 'Master' || recordTypeName == 'Principal') {
            recordTypeName = null;
        }
        Object[] relatedListsMetatdata = LEX_RestAPIUtils.getRelatedListsMetadata(objectId, recordTypeName);

        for (Object relatedListMetadata : relatedListsMetatdata) {
            Map<String, Object> mRelatedListMetadata = (Map<String, Object>) relatedListMetadata;
            if (mRelatedListMetadata.get('name') == relatedListName) {
                cleanRelatedListMetadata(mRelatedListMetadata);
                System.debug(JSON.serialize(mRelatedListMetadata.get('columns')));
                return mRelatedListMetadata;
            }
        }

        return null;
    }

    /**
    * Return the related list items
    * */
    @AuraEnabled
    public static List<ObjectWithCheckboxWrapper> getRelatedListItems(Id objectId, String relatedlistName, String columns, String lookupFieldName, String objectName, String orderByFields) {
        Object[] items;

        if (columns == null) {
            items = LEX_RestAPIUtils.getRelatedListItems(objectId, relatedlistName);
        }
        else {
            List<String> fields = new List<String>();
            for (ColumnWrapper result : (List<ColumnWrapper>) JSON.deserialize(columns, List<ColumnWrapper>.class)) {
                if (result.type != null && result.type.equalsIgnoreCase('Picklist')) {
                    fields.add('ToLabel(' + result.fieldApiName + ')');
                }
                else {
                    fields.add(result.fieldApiName);
                }
            }

            String query = 'SELECT Id, ' + string.join(fields, ', ') + ' FROM ' + objectName + ' WHERE ' + lookupFieldName + ' =:objectId';
            if (!String.isEmpty(orderByFields)) {
                query += ' ORDER BY ' + orderByFields;
            }

            items = Database.query(query);

            List<ObjectWithCheckboxWrapper> finalList = new List<ObjectWithCheckboxWrapper>();
            for (Object item : items) {
                finalList.add(new ObjectWithCheckboxWrapper(false, item));
            }
            return finalList;
        }

        if (items.size() > 0) {
            Id firstChildId = (Id) ((Map<String, Object>) items.get(0)).get('Id');
            DescribeSObjectResult objectDescResult = firstChildId.getSobjectType().getDescribe();

            Map<String, DescribeFieldResult> lookupFields = new Map<String, DescribeFieldResult>();
            for (Schema.SObjectField sfield : objectDescResult.fields.getMap().Values()) {
                DescribeFieldResult fieldDescResult = sfield.getDescribe();
                if (fieldDescResult.getType() == Schema.DisplayType.REFERENCE) {
                    lookupFields.put(fieldDescResult.getName(), fieldDescResult);
                }
            }

            //Set the lookup labels based on the field type
            for (String fieldKey : lookupFields.keySet()) {
                Set<Id> lookupIds = new Set<Id>();
                Map<Id, List<Object>> lookupMap = new Map<Id, List<Object>>();

                //Build the list of IDs
                for (Object item : items) {
                    Map<String, Object> mItem = (Map<String, Object>) item;
                    if (mItem.containsKey(fieldKey)) {
                        Id lookupId = (Id) mItem.get(fieldKey);
                        if (lookupId != null) {
                            lookupIds.add(lookupId);
                            if (!lookupMap.containsKey(lookupId)) {
                                lookupMap.put(lookupId, new List<Object>());
                            }
                            lookupMap.get(lookupId).add(mItem);
                        }
                    }
                }

                //Do SOQL query based on the ID list
                if (!lookupIds.isEmpty()) {
                    SObject[] lookupObjs = getLookupObjects(lookupIds);
                    for (SObject lookupObj : lookupObjs) {
                        for (Object item : lookupMap.get(lookupObj.Id)) {
                            Map<String, Object> mItem = (Map<String, Object>) item;
                            try {
                                mItem.put(fieldKey + '__Name', lookupObj.get('Name'));
                            } catch (Exception e) {
                                System.debug(e.getMessage());
                                mItem.put(fieldKey + '__Name', 'Undefined');
                            }
                        }
                    }
                }
            }

            List<ObjectWithCheckboxWrapper> finalList = new List<ObjectWithCheckboxWrapper>();
            for (Object item : items) {
                finalList.add(new ObjectWithCheckboxWrapper(false, item));
            }
            return finalList;
        }

        return null;
    }

    /**
     * Return the list of objects based on the lookup ids
     * */
    private static SObject[] getLookupObjects(Set<Id> lookupIds) {
        Schema.SObjectType sobjectType = (new list<Id>(lookupIds))[0].getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();

        if (isSafeObject(sobjectName) && isSafeField('Name', sobjectName)) {
            return Database.query('SELECT Id, Name FROM ' + sobjectName + ' WHERE Id IN :lookupIds');
        }

        return null;
    }

    /**
     * Search Candiate for Lookup Field
     * */
    @AuraEnabled
    public static Object[] getLookupCandidates(String refObjName, String searchTerm) {
        String qname = '%' + searchTerm + '%';

        if (isSafeObject(refObjName) && isSafeField('Name', refObjName)) {
            return Database.query('SELECT Id, Name FROM ' + refObjName + ' WHERE Name LIKE :qname LIMIT 5');
        }

        return null;
    }

    private static boolean isSafeObject(String objName) {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        SObjectType myObj = schemaMap.get(objName);

        return (myObj != null && myObj.getDescribe().isAccessible());
    }

    private static boolean isSafeField(String fieldName, String objName) {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        SObjectType myObj = schemaMap.get(objName);

        SObjectField myField = myObj.getDescribe().fields.getMap().get(fieldName);
        return (myField != null && myField.getDescribe().isAccessible());
    }

    /**
    * Return the object details
    * */
    @AuraEnabled
    public static Object getObject(Id objectId) {
        Object item = LEX_RestAPIUtils.getObject(objectId);
        if (item != null) {
            DescribeSObjectResult objectDescResult = objectId.getSobjectType().getDescribe();

            Map<String, DescribeFieldResult> lookupFields = new Map<String, DescribeFieldResult>();
            for (Schema.SObjectField sfield : objectDescResult.fields.getMap().Values()) {
                DescribeFieldResult fieldDescResult = sfield.getDescribe();
                if (fieldDescResult.getType() == Schema.DisplayType.REFERENCE) {
                    lookupFields.put(fieldDescResult.getName(), fieldDescResult);
                }
            }

            //Set the lookup labels based on the field type
            for (String fieldKey : lookupFields.keySet()) {
                Set<Id> lookupIds = new Set<Id>();

                //Build the list of IDs
                Map<String, Object> mItem = (Map<String, Object>) item;
                if (mItem.containsKey(fieldKey)) {
                    Id lookupId = (Id) mItem.get(fieldKey);
                    if (lookupId != null) {
                        lookupIds.add(lookupId);
                    }
                }


                //Do SOQL query based on the ID list
                if (!lookupIds.isEmpty()) {
                    SObject[] lookupObjs = getLookupObjects(lookupIds);
                    for (SObject lookupObj : lookupObjs) {
                        try {
                            mItem.put(fieldKey + '__Name', lookupObj.get('Name'));
                        } catch (Exception e) {
                            System.debug(e.getMessage());
                            mItem.put(fieldKey + '__Name', 'Undefined');
                        }
                    }
                }
            }

            return item;
        }

        return null;
    }


    @AuraEnabled
    public static Boolean deleteRelatedRecord(Id objectId) {
        Schema.SObjectType sobjectType = objectId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();

        if (isSafeObject(sobjectName)) {
            Database.DeleteResult result = Database.delete(objectId);
            return result.isSuccess();
        }
        return False;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- MODEL DEFINITION ----------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    public class ObjectWithCheckboxWrapper {
        @AuraEnabled  public Boolean checked {get; set;}
        @AuraEnabled  public Object objectRecord {get; set;}

        public ObjectWithCheckboxWrapper(Boolean checked, Object objectRecord) {
            this.checked = checked;
            this.objectRecord = objectRecord;
        }
    }

    public class ColumnWrapper {
        public String fieldApiName { get; set; }
        public String type { get; set; }
    }
}