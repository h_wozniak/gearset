@isTest
private class ZTestPriceBookChangeController {

    //----------------------------------------------------------------------------------
    //PriceBookChangeController
    //----------------------------------------------------------------------------------
    
    private static void configPriceBook(Id pbId, Decimal amount) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];
        
        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }
        
        update pb.InstallmentConfigs__r;
    }
    
    @isTest static void testPriceBookChange() {
        PageReference pageRef = Page.PriceBookChange;
        Test.setCurrentPageReference(pageRef);
        
        Offer__c offerFromPriceBook = ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 5
        ), true);
        
        Offer__c priceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        configPriceBook(priceBook.Id, 10.0);
        
        Test.startTest();
        Enrollment__c studyEnrollment = ZDataTestUtility.createEnrollmentStudy(
            new Enrollment__c(
                Course_or_Specialty_Offer__c = priceBook.Offer_from_Price_Book__c, 
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'),
            true);

        Test.stopTest();        
        
        ApexPages.currentPage().getParameters().put('enrollmentId', studyEnrollment.Id);
        PriceBookChangeController pbc = new PriceBookChangeController();
        
        System.assertEquals(pbc.selectedPriceBook.priceBookOffer.Id, priceBook.Id);
        
        System.assertNotEquals(null, pbc.installmentsPerYearList);
        System.assertNotEquals(null, pbc.tuitionSystemList);

        pbc.cancel();

        Id pretest = pbc.enrollment.Price_Book_from_Enrollment__c;
        
        pbc.save();
        
        Id posttest = pbc.enrollment.Price_Book_from_Enrollment__c;
        System.assertEquals(pretest, posttest);
        
        PriceBookChangeController.PriceBook_Wrapper newSelectedPriceBook = new PriceBookChangeController.PriceBook_Wrapper();
        
        Offer__c newPriceBook = ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Offer_from_Price_Book__c = offerFromPriceBook.Id,
            Active__c = true
        ), true);
        
        newSelectedPriceBook.name = 'currentPriceBook';
        newSelectedPriceBook.priceBookOffer = newPriceBook;
        newSelectedPriceBook.hasGradedPrices = newPriceBook.Number_of_Semesters_formula__c > 2 && newPriceBook.Graded_tuition__c;
        pbc.selectedPriceBook = newSelectedPriceBook;
        
        System.assertEquals(pbc.selectedPriceBook, newSelectedPriceBook);
        
        pbc.save();
        System.assertNotEquals(pretest, pbc.enrollment.Price_Book_from_Enrollment__c);
        
        pbc.selectedPBOption = newPriceBook.Id;
        pbc.updateSelectedFields();
        System.assertEquals(pbc.selectedPriceBook.priceBookOffer.Id, newPriceBook.Id);

        System.assertNotEquals(null, pbc.getPriceBookItems());
    }
}