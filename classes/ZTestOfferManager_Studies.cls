@isTest
private class ZTestOfferManager_Studies {




    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- TEST STATISTICS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_Statistics(Boolean withSpecialties) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));

        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
            University_Study_Offer_from_Course__c = retMap.get(0).Id
        ), true));

        if (withSpecialties) {
            retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = retMap.get(1).Id
            ), true));
            retMap.put(3, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = retMap.get(1).Id
            ), true));
        }

        return retMap;
    }

    @isTest static void test_Statistics_SpecEnr() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(true);


        Test.startTest();
        ZDataTestUtility.createEnrollmentStudies(new Enrollment__c(
            Course_or_Specialty_Offer__c = dataMap.get(2).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED
        ), 1, true);
        Test.stopTest();

        ZDataTestUtility.createEnrollmentStudies(new Enrollment__c(
            Course_or_Specialty_Offer__c = dataMap.get(3).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED
        ), 2, true);

        Offer__c univOfferStudy = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(0).Id];
        Offer__c courseOffer = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        Offer__c specOffer1 = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        Offer__c specOffer2 = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(3).Id];

        System.assertEquals(3, univOfferStudy.Candidates__c);
        System.assertEquals(3, courseOffer.Candidates__c);
        System.assertEquals(1, specOffer1.Candidates__c);
        System.assertEquals(2, specOffer2.Candidates__c);

    }

    @isTest static void test_Statistics_CourseEnr() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Test.startTest();
        ZDataTestUtility.createEnrollmentStudies(new Enrollment__c(
            Course_or_Specialty_Offer__c = dataMap.get(1).Id,
            Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED
        ), 4, true);

        Offer__c univOfferStudy = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(0).Id];
        Offer__c courseOffer = [SELECT Id, Candidates__c, Accepted_Candidates__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        Test.stopTest();

        System.assertEquals(4, univOfferStudy.Candidates__c);
        System.assertEquals(4, courseOffer.Candidates__c);
    }

    @isTest static void test_createAdditionalDocument() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c additionalOfferDocument = ZDataTestUtility.createAdditionalOfferDocument(new Offer__c(
                Offer_from_Additional_Document__c = dataMap.get(1).Id
        ), true);

        Offer__c additionalDocument = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :additionalOfferDocument.Id];
        System.assertNotEquals(additionalDocument.Name, null);
    }

    @isTest static void test_createSpecializationDocument() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_Statistics(false);

        Offer__c createdSpecialization = ZDataTestUtility.createSpecializationOffer(new Offer__c(
                Offer_from_Specialization__c = dataMap.get(1).Id
        ), true);

        Offer__c specialization = [SELECT Id, Name, Code_Helper_Num__c FROM Offer__c WHERE Id = :createdSpecialization.Id];
        System.assertNotEquals(specialization.Name, null);
    }

    @isTest static void test_createDuplicateLanguage() {
        Offer__c courseOffer = ZDataTestUtility.createCourseOffer(null, true);
        Offer__c specialtyOffer = ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = courseOffer.Id), true);
        ZDataTestUtility.createForeignLanguages(new Offer__c(Course_Offer_from_language__c = specialtyOffer.Id), 1, true);

        Boolean errorOccured = false;
        try {
            ZDataTestUtility.createForeignLanguages(new Offer__c(Course_Offer_from_language__c = specialtyOffer.Id), 1, true);
        }
        catch (Exception e) {
            errorOccured = true;
            System.assert(e.getMessage().contains(Label.msg_error_LanguageDuplicate));
        }

        System.assertEquals(errorOccured, true);

    }

    @isTest static void test_generateOfferNumber_Summer() {
        List<Offer__c> studyOffers = new List<Offer__c>();
        Id studyOfferId = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Study_Start_Date__c=Date.newInstance(2017, 8, 9)), true).Id;
        Offer__c studyCourseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = studyOfferId, Semester_Type__c=CommonUtility.CATALOG_PERIOD_SUMMER), true);
        studyOffers.add(studyCourseOffer);
        OfferManager_Studies.generateOfferNumber(studyOffers);

        System.assertEquals(studyCourseOffer.Offer_Number__c, 8);
    }

    @isTest static void test_generateOfferNumber_Winter() {
        List<Offer__c> studyOffers = new List<Offer__c>();
        Id studyOfferId = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Study_Start_Date__c=Date.newInstance(2017, 3, 3)), true).Id;
        Offer__c studyCourseOffer = ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = studyOfferId, Semester_Type__c=CommonUtility.CATALOG_PERIOD_WINTER), true);
        studyOffers.add(studyCourseOffer);
        OfferManager_Studies.generateOfferNumber(studyOffers);

        System.assertEquals(studyCourseOffer.Offer_Number__c, 7);
    }

    @isTest static void areDatesEmpty_notAllFilled(){
        List<Date> startDates = new List<Date>();
        startDates.add(Date.newInstance(2017, 8, 9));
        startDates.add(null);

        Boolean areDatesEmpty = OfferManager_Studies.areDatesEmpty(startDates);
        System.assertEquals(areDatesEmpty, true);
    }

    @isTest static void areDatesEmpty_allNotFilled(){
        List<Date> startDates = new List<Date>();
        startDates.add(null);
        startDates.add(null);

        Boolean areDatesEmpty = OfferManager_Studies.areDatesEmpty(startDates);
        System.assertEquals(areDatesEmpty, true);
    }

    @isTest static void areDatesEmpty_false(){
        List<Date> startDates = new List<Date>();
        startDates.add(Date.newInstance(2017, 8, 9));
        startDates.add(Date.newInstance(2017, 2, 9));

        Boolean areDatesEmpty = OfferManager_Studies.areDatesEmpty(startDates);
        System.assertEquals(areDatesEmpty, false);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------ GUIDED GROUP COURSE --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_generatingEnrollmentIdLink_course() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));

        Offer__c courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, false);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_generatingEnrollmentIdLink_course_OnInsert() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id,
                Guided_group__c = true
        ), true));

        Offer__c courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, true);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_generatingEnrollmentIdLink_course_clear() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id,
                Guided_group__c = true
        ), true));


        Offer__c courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, true);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);

        courseOffer.Guided_group__c = false;
        update courseOffer;

        courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, false);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_clearingEnrollmentIdLink_course_OnUpdate() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));

        Offer__c courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, false);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);

        courseOffer.Guided_group__c = true;
        update courseOffer;

        courseOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(1).Id];
        System.assertEquals(courseOffer.Guided_Group__c, true);
        System.assertEquals(courseOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(courseOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* --------------------------------- GUIDED GROUP SPECIALTY --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_generatingEnrollmentIdLink_specialty() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = dataMap.get(1).Id
        ), true));

        Offer__c specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, false);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_generatingEnrollmentIdLink_specialty_OnInsert() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = dataMap.get(1).Id,
                Guided_group__c = true
        ), true));

        Offer__c specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, true);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_generatingEnrollmentIdLink_specialty_clear() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = dataMap.get(1).Id,
                Guided_group__c = true
        ), true));

        Offer__c specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, true);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);

        specialtyOffer.Guided_group__c = false;
        update specialtyOffer;

        specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, false);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }

    @isTest static void test_clearingEnrollmentIdLink_specialty_OnUpdate() {
        ZDataTestUtility.prepareCatalogData(true, true);
        CustomSettingDefault.initEsbConfig();

        Map<Integer, sObject> dataMap = new Map<Integer, sObject>();
        dataMap.put(0, ZDataTestUtility.createUniversityOfferStudy(null, true));
        dataMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(
                University_Study_Offer_from_Course__c = dataMap.get(0).Id
        ), true));
        dataMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
                Course_Offer_from_Specialty__c = dataMap.get(1).Id
        ), true));

        Offer__c specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, false);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);

        specialtyOffer.Guided_group__c = true;
        update specialtyOffer;

        specialtyOffer = [SELECT Id, Guided_Group__c, Enrollment_Link__c, Dont_show_in_ZPI__c FROM Offer__c WHERE Id = :dataMap.get(2).Id];
        System.assertEquals(specialtyOffer.Guided_Group__c, true);
        System.assertEquals(specialtyOffer.Dont_show_in_ZPI__c, false);
        System.assertNotEquals(specialtyOffer.Enrollment_Link__c, Label.msg_NotApplicable);
    }
}