public with sharing class TargetNewButtonOverride {

    public List<SelectOption> recordTypeList { get; set; }
    public String selectedRecordType { get; set; }
    public Map<Integer, WrappedRecordTypes> availableRecordTypesTable { get; set; }

    public TargetNewButtonOverride(ApexPages.StandardController controller) {
        recordTypeList = getRecordTypes();
    }

    public List<SelectOption> getRecordTypes() {
        List<SelectOption> result = new List<SelectOption>();
        List<Id> recordTypesIdList = new List<Id>();

        recordTypesIdList.add(CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_STUDY_ENR_TARGET));
        recordTypesIdList.add(CommonUtility.getRecordTypeId('Target__c', CommonUtility.TARGET_RT_TRAINING_ENR_TARGET));

        Set<Id> availableForUserRTIds = CommonUtility.getAvailableRecordTypeIdsForSObject(Schema.SObjectType.Target__c);
        List<RecordType> possibleRecordTypes = [SELECT Id, toLabel(Name), Description FROM RecordType WHERE Id IN :recordTypesIdList ORDER BY Id];

        availableRecordTypesTable = new Map<Integer, WrappedRecordTypes>();
        Integer index = 0;
        for(RecordType rt : possibleRecordTypes) {
            if (availableForUserRTIds.contains(rt.Id)) {
                result.add(new SelectOption(rt.Id, rt.Name));
                WrappedRecordTypes wrt = new WrappedRecordTypes();
                wrt.name = rt.Name;
                wrt.description = rt.description;
                availableRecordTypesTable.put(index, wrt);
                index++;
            }
        }   

        return result;
    }

    public class WrappedRecordTypes {
        public String name { get; set; }
        public String description { get; set; }
    }

    public PageReference continueStep() {
        String prefix = CommonUtility.getPrefix('Target__c');
        String url = '/' + prefix + '/e?RecordType=' + selectedRecordType + '&retURL=/' + prefix + '/o&nooverride=1';
        
        return new PageReference(url);
    }

}