@isTest
global class ZTestIntegrationPaymentTemplate {
    

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ TEST DATA ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_OfferWithPB_I_II_U(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_I
        ), true);
        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 7,
            University_Study_Offer_from_Course__c = univOfferStudy.Id
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createPriceBook(new Offer__c(
            Graded_tuition__c = true, 
            Entry_fee__c = 500,
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Active__c = false
        ), true));

        //config PriceBook
        configPriceBook(retMap.get(2).Id, instAmount, true);

        return retMap;
    }

    private static Map<Integer, sObject> prepareDataForTest_OfferWithPB_PG_MBA(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Offer__c univOfferStudy = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
            Degree__c = CommonUtility.OFFER_DEGREE_PG
        ), true);
        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
            Number_of_Semesters__c = 7,
            University_Study_Offer_from_Course__c = univOfferStudy.Id
        ), true));
        retMap.put(1, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(
            Course_Offer_from_Specialty__c = retMap.get(0).Id
        ), true));
        retMap.put(2, ZDataTestUtility.createPriceBookPG(new Offer__c(
            Offer_from_Price_Book__c = retMap.get(0).Id,
            Entry_fee__c = 500,
            Active__c = false,
            Installment_Variants__c = '1;2;8;10;12'
        ), true));

        //config PriceBook
        configPriceBook(retMap.get(2).Id, instAmount, false);

        return retMap;
    }

    private static void configPriceBook(Id pbId, Decimal amount, Boolean graded) {
        Offer__c pb = [
            SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, 
                Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c 
                FROM InstallmentConfigs__r)
            FROM Offer__c
            WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            if (graded) {
                for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                    instConfig.put(field, amount);
                }
            }
        }

        update pb.InstallmentConfigs__r;
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- TEST METHODS - SENDING -------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_buildPriceBookPaymentTemplatesJSON_I_II_U() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);
        PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });

        Offer__c pb = [
            SELECT Id, Currently_in_use__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(true, pb.Currently_in_use__c);


        Test.startTest();
        String generatedJSON = IntegrationPaymentTemplateHelper.buildPriceBookPaymentTemplatesJSON(new Set<Id> { pb.Id });
        Test.stopTest();

        System.assertNotEquals(null, generatedJSON);
        System.assert(generatedJSON.contains('"title":2'));
        System.assert(generatedJSON.contains('"title":3'));
    }

    @isTest static void test_buildPriceBookPaymentTemplatesJSON_PG_MBA() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_PG_MBA(100);
        PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });

        Offer__c pb = [
            SELECT Id, Currently_in_use__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(true, pb.Currently_in_use__c);


        Test.startTest();
        String generatedJSON = IntegrationPaymentTemplateHelper.buildPriceBookPaymentTemplatesJSON(new Set<Id> { pb.Id });
        Test.stopTest();

        System.assertNotEquals(null, generatedJSON);
        System.assert(generatedJSON.contains('"title":2'));
    }
    
    @isTest static void test_sendPaymentTemplatesAsync() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock());

        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);
        PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });

        Test.startTest();
        IntegrationPaymentTemplateSender.sendPaymentTemplatesAsync(new Set<Id> { dataMap.get(2).Id });
        Test.stopTest();

        Offer__c pb = [
            SELECT Id, Synchronization_Status__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, pb.Synchronization_Status__c);
    }
    
    @isTest static void test_sendPaymentTemplatesSync_Manual() {
        CustomSettingDefault.initEsbConfig();
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock());

        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);
        PriceBookManager.activatePriceBooks(new List<Id> { dataMap.get(2).Id });

        Test.startTest();
        Boolean result = StudyWebservices.transferPaymentTemplatesToExperia(dataMap.get(2).Id);
        Test.stopTest();

        System.assert(result);

        Offer__c pb = [
            SELECT Id, Synchronization_Status__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(CommonUtility.SYNCSTATUS_INPROGRESS, pb.Synchronization_Status__c);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ---------------------------------- TEST METHODS - RECEIVING ------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    @isTest static void test_receivePaymentTemplateAcks_validJSON_success() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(2).Id;
        respBodyJSON.success = true;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(respBodyJSON));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentTemplateAckReceiver.receivePaymentTemplateAcks();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        Offer__c pb = [
            SELECT Id, Synchronization_Status__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(CommonUtility.SYNCSTATUS_FINISHED, pb.Synchronization_Status__c);
    }

    @isTest static void test_receivePaymentTemplateAcks_validJSON_failure() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(2).Id;
        respBodyJSON.success = false;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(respBodyJSON));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentTemplateAckReceiver.receivePaymentTemplateAcks();
        Test.stopTest();

        System.assertEquals(200, RestContext.response.statusCode);

        Offer__c pb = [
            SELECT Id, Synchronization_Status__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(CommonUtility.SYNCSTATUS_FAILED, pb.Synchronization_Status__c);
    }

    @isTest static void test_receivePaymentTemplateAcks_invalidJSON() {
        Map<Integer, sObject> dataMap = prepareDataForTest_OfferWithPB_I_II_U(100);

        IntegrationManager.AckResponse_JSON respBodyJSON = new IntegrationManager.AckResponse_JSON();
        respBodyJSON.record_id = dataMap.get(2).Id;
        respBodyJSON.success = true;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize('stringPartToDamageJSON' + respBodyJSON + 'stringPartToDamageJSON'));
        req.headers.put(IntegrationManager.HEADER_CONTENT_TYPE, IntegrationManager.CONTENT_TYPE_JSON);
        req.httpMethod = IntegrationManager.HTTP_METHOD_POST;
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        IntegrationPaymentTemplateAckReceiver.receivePaymentTemplateAcks();
        Test.stopTest();

        System.assertEquals(400, RestContext.response.statusCode);

        System.assert(RestContext.response.responseBody.toString().contains(IntegrationError.errorCodeToMsgMap.get(601)));

        Offer__c pb = [
            SELECT Id, Synchronization_Status__c
            FROM Offer__c
            WHERE Id = :dataMap.get(2).Id
        ];

        System.assertEquals(CommonUtility.SYNCSTATUS_NEW, pb.Synchronization_Status__c);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class IntegrationPaymentTemplateMock implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
            String createPaymentEndPoint = esb.Create_Payment_Templates_End_Point__c;
            String esbIp = esb.Service_Url__c;

            System.assertEquals(esbIp + createPaymentEndPoint, req.getEndpoint());
            System.assertEquals(IntegrationManager.HTTP_METHOD_POST, req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);

            return res;
        }
    }

}