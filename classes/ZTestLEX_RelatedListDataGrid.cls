@isTest
global class ZTestLEX_RelatedListDataGrid {
    static testMethod void testGetReleatedListsMetadata() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAPIMockHttpResponseGenerator());
        Object[] relatedListsMatadata = LEX_RelatedListDataGridController.getReleatedListsMetadata('0010Y000003iFovQAE');
        System.assertEquals(true, relatedListsMatadata.size() > 0);

        Test.stopTest();
    }


    static testMethod void testGetReleatedListMetadata() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAPIMockHttpResponseGenerator());

        Object relatedListsMatadata = LEX_RelatedListDataGridController.getReleatedListMetadata('0010Y000003iFovQAE',
                'Contacts');
        System.assertEquals(true, relatedListsMatadata != null);

        Test.stopTest();
    }

    static testMethod void testGetObject() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAPIMockHttpResponseGenerator());
        Object jsonObj = LEX_RelatedListDataGridController.getObject('0019E000006nVr3QAE');
        System.assertEquals(true, jsonObj != null);

        Test.stopTest();
    }

    static testMethod void testDelectObject() {
        Test.startTest();

        Contact ct = ZDataTestUtility.createCandidateContact(null, true);

        boolean result = LEX_RelatedListDataGridController.deleteRelatedRecord(ct.Id);
        System.assertEquals(true, result);

        Test.stopTest();
    }

    global class RestAPIMockHttpResponseGenerator  implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setHeader('Content-Type', 'application/json');
            mock.setStatusCode(200);

            String endPoint = req.getEndpoint();
            System.debug(endPoint);

            //Get Layout Response
            if(endPoint.contains('sobjects/Account/describe/layouts')){
                mock.setStaticResource('TestAccountLayoutResponse');
            }

            //Get Describe Response
            else if(endPoint.contains('sobjects/Account/describe')){
                mock.setStaticResource('TestAccountDescribeResponse');
            }

            //Get Contacts Response
            else if(endPoint.contains('/Contacts')){
                mock.setStaticResource('TestAccountContactsResponse');
            }

            else if(endPoint.contains('sobjects/Account')){
                mock.setStaticResource('TestAccountDetailsResponse');
            }

            else if (endPoint.contains('/tooling/query')){
                mock.setStaticResource('TestAccountDetailsResponse');
            }

            return mock.respond(req);
        }
    }
}