@isTest
global class ZTestJobScheduleManager {

    @isTest static void test_schedule_EnrollmentConfirmationEmailReminder() {
        Test.startTest();

        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_EnrollmentConfirmationEmailReminder();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_EnrollmentDocumentDeliveryEmailReminder() {
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_EnrollmentDocumentDeliveryEmailReminder();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_SalesTargetComputations() {
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_SalesTargetComputations();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_IntegrationFCCInteractionReceiver() {
        Test.setMock(HttpCalloutMock.class, new IntegrationFCCInteractionWebserviceMockEmpty());
        ZDataTestUtilityMethods.prepareContacts();
        ZDataTestUtilityMethods.prepareCampaigns();
        Test.startTest();
        
        FCC_Last_Interaction_Retrival_Date__c fccLastInteraction = new FCC_Last_Interaction_Retrival_Date__c();
        fccLastInteraction.Date_From__c = DateTime.valueOf('2014-10-10 15:21:00');
        insert fccLastInteraction;
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_IntegrationFCCInteractionReceiver();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_ResendNotSentDocumentEmail() {
        Test.startTest();

        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_ResendNotSentDocumentEmail();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_CreateCampaignContact() {
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_CreateCampaignContact();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_CampaignLackOfDocuments() {
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_CampaignLackOfDocuments();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_ExtranetEmail() {
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_ExtranetEmail();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_UnenrolledDocReminder() {
        Test.startTest();

        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_UnenrolledDocReminder();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

        List<CronTrigger> ctList = [
                SELECT Id
                FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_PaymentTemplates() {        
        Test.setMock(HttpCalloutMock.class, new IntegrationPaymentTemplateMock());
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_PaymentTemplates();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_syncConsentsWithIPress() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_3 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW, Accepted_by_default__c = true), true));
        retMap.put(1, ZDataTestUtility.createBaseConsent(new Catalog__c(Name = RecordVals.MARKETING_CONSENT_1 + ' ' + CommonUtility.MARKETING_ENTITY_WROCLAW), true));
        
        Test.setMock(HttpCalloutMock.class, new ConsentsSendCalloutMock());
        Test.startTest();
        
        Boolean errorOccured = false;
        try {
            JobScheduleManager.schedule_syncConsentsWithIPresso();
        }
        catch (Exception e) {
            errorOccured = true;
        }
        Test.stopTest();

        System.assert(!errorOccured);

         List<CronTrigger> ctList = [
            SELECT Id
            FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_removeIpressoCampaigns_Tags1() {
        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_TAGS, TECH_Synchronized_All_Contacts__c = true), true);
        Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

        Test.startTest();
        JobScheduleManager.schedule_removeIpressoCampaigns();
        Test.stopTest();
    }

    @isTest static void test_schedule_removeIpressoCampaigns_Segments1() {
        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS, Segment_Expiration_Date__c = System.now().addDays(-20)), true);
        Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

        Test.startTest();
        JobScheduleManager.schedule_removeIpressoCampaigns();
        Test.stopTest();
    }

    @isTest static void test_schedule_removeIpressoCampaigns_Tags() {
        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_TAGS, TECH_Synchronized_All_Contacts__c = true), true);
        Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

        Test.startTest();
        IntegrationIpressoDeleteTagsBatch campaignsBatch = new IntegrationIpressoDeleteTagsBatch(new Set<Id> { iPressoCampaign.Id });
        Database.executebatch(campaignsBatch, 200);
        Test.stopTest();

        List<Marketing_Campaign__c> members = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
        ];

        System.assertEquals(members.size(), 0);

        System.assertEquals(Approval.isLocked(iPressoCampaign.Id), true);

        List<Marketing_Campaign__c> campaigns = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_IPRESSO_SALES)
        ];

        System.assertEquals(campaigns.size(), 1);
    }

    @isTest static void test_schedule_removeIpressoCampaigns_Segments() {
        CustomSettingDefault.initDidacticsCampaign();
        Marketing_Campaign__c iPressoCampaign = ZDataTestUtility.createMarketingCampaigniPresso(new Marketing_Campaign__c(iPresso_Criteria_Type__c = CommonUtility.MARKETING_CRITERIA_TYPE_SEGMENTS, Segment_Expiration_Date__c = System.now().addDays(-20)), true);
        Marketing_Campaign__c campaignMember = ZDataTestUtility.createCampaignMember(new Marketing_Campaign__c(Campaign_Member__c = iPressoCampaign.Id), true);

        Test.startTest();
        IntegrationIpressoDeleteTagsBatch campaignsBatch = new IntegrationIpressoDeleteTagsBatch(new Set<Id> { iPressoCampaign.Id });
        Database.executebatch(campaignsBatch, 200);
        Test.stopTest();

        List<Marketing_Campaign__c> members = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER)
        ];

        System.assertEquals(members.size(), 0);

        System.assertEquals(Approval.isLocked(iPressoCampaign.Id), false);

        List<Marketing_Campaign__c> campaigns = [
            SELECT Id
            FROM Marketing_Campaign__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_IPRESSO_SALES)
        ];

        System.assertEquals(campaigns.size(), 0);
    }

    @isTest static void test_schedule_retrieveArrears() {
        CustomSettingDefault.initEsbConfig();
        Enrollment__c eduAgr = ZDataTestUtility.createEducationalAgreement(null, true);

        Enrollment__c qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        qEduAgr.Student_from_Educational_Agreement__c = qEduAgr.Source_Enrollment__r.Candidate_Student__c;
        qEduAgr.Didactics_Status__c = CommonUtility.ENROLLMENT_DIDACTICS_STATUS_OK;
        update qEduAgr;

        Test.setMock(HttpCalloutMock.class, new ArrearsRetrieveCalloutMock());

        Test.startTest();
        JobScheduleManager.schedule_retrieveArrears();
        Test.stopTest();

        qEduAgr = [
                SELECT Id, Source_Enrollment__c, Student_from_Educational_Agreement__c, Source_Enrollment__r.Candidate_Student__c, Balance__c
                FROM Enrollment__c
                WHERE Id = :eduAgr.Id
        ];

        System.assertEquals(qEduAgr.Balance__c, null);
    }

    @isTest static void test_schedule_workshopReminderSchool() {
        DateTime dayOfEventFrom = System.now().addDays(1);
        DateTime dayOfEventTo = System.now().addDays(1).addMinutes(45);

        ZDataTestUtility.createHighSchoolVisit(new Event(StartDateTime = dayOfEventFrom, EndDateTime = dayOfEventTo), true);

        Test.startTest();
        JobScheduleManager.schedule_workshopReminderSchool();
        Test.stopTest();

        List<CronTrigger> ctList = [
                SELECT Id
                FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }

    @isTest static void test_schedule_visitatorSchoolVisitEmailReminder() {
        DateTime dayOfEventFrom = System.now().addDays(1);
        DateTime dayOfEventTo = System.now().addDays(1).addMinutes(45);

        ZDataTestUtility.createHighSchoolVisit(new Event(StartDateTime = dayOfEventFrom, EndDateTime = dayOfEventTo), true);

        Test.startTest();
        JobScheduleManager.schedule_workshopReminderSchool();
        Test.stopTest();

        List<CronTrigger> ctList = [
                SELECT Id
                FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }
    
     @isTest static void test_schedule_retrieveVisitatorEvents() {
        Test.setMock(HttpCalloutMock.class, new ZDataTestUtilityMethods.Calendar_SendCalloutMock());
         
        Test.startTest();
        JobScheduleManager.schedule_retrieveVisitatorEvents();
        Test.stopTest();

        List<CronTrigger> ctList = [
                SELECT Id
                FROM CronTrigger
        ];

        System.assert(ctList.size() >= 1);
    }



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------------ MOCK CLASS ------------------------------------------ */
    /* ------------------------------------------------------------------------------------------------ */

    global class ArrearsRetrieveCalloutMock implements HttpCalloutMock {

        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);

            List<IntegrationArrearsRetrieval.AckResponse_JSON> responseJSON = new List<IntegrationArrearsRetrieval.AckResponse_JSON>();

            IntegrationArrearsRetrieval.AckResponse_JSON response = new IntegrationArrearsRetrieval.AckResponse_JSON();
            response.portal_login = '1234';
            response.to_pay_total = 0;
            response.balance = new List<IntegrationArrearsRetrieval.Balance_JSON>();
            responseJSON.add(response);

            String preparedJson = JSON.serializePretty(responseJSON);
            res.setBody(preparedJson);

            return res;
        }
    }

    global class ConsentsSendCalloutMock implements HttpCalloutMock {
        
        global HttpResponse respond(HTTPRequest req) {            
            HttpResponse res = new HttpResponse();
            res.setStatus('Created');
            res.setStatusCode(200);
            return res;
        }
    }
    
    global class ConsentsSendFailedCalloutMock implements HttpCalloutMock {
        
        global HttpResponse respond(HTTPRequest req) {            
            HttpResponse res = new HttpResponse();
            res.setStatus('Not Found');
            res.setStatusCode(404);
            return res;
        }
    }

    global class IntegrationPaymentTemplateMock implements HttpCalloutMock {

        global HTTPResponse respond(HTTPRequest req) {

            ESB_Config__c esb = ESB_Config__c.getOrgDefaults();
        
            String createPaymentEndPoint = esb.Create_Payment_Templates_End_Point__c;
            String esbIp = esb.Service_Url__c;

            System.assertEquals(esbIp + createPaymentEndPoint, req.getEndpoint());
            System.assertEquals(IntegrationManager.HTTP_METHOD_POST, req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);

            return res;
        }
    }

    public class IntegrationFCCInteractionWebserviceMockEmpty implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');

            List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON> requestInteractionsJson = new List<IntegrationFCCInteractionHelper.InteractionReceiver_JSON>();

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson1 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-10 15:00:00', 
                'testAgentId1', 
                'nameOfTestAgent1',
                'Outcoming', 
                'ANSWER', 
                '123456789',
                '123456789');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson2 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId2', 
                '2015-10-11 15:00:00', 
                'testAgentId2', 
                'nameOfTestAgent2', 
                'Incoming', 
                'NO ANSWER', 
                '987654321',
                '12345679');

            IntegrationFCCInteractionHelper.InteractionReceiver_JSON requestInteractionJson3 = new IntegrationFCCInteractionHelper.InteractionReceiver_JSON(
                'testExternalId1', 
                '2015-10-12 15:00:00', 
                'testAgentId1', 
                'nameOfTestAgent1', 
                'Incoming', 
                'BUSY', 
                '123456789',
                '12356789');

            requestInteractionsJson.add(requestInteractionJson1);
            requestInteractionsJson.add(requestInteractionJson2);
            requestInteractionsJson.add(requestInteractionJson3);

            String preparedJson = JSON.serializePretty(requestInteractionsJson);

            res.setBody(preparedJson);

            return res;
        }

    }

}