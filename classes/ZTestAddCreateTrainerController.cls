@isTest
private class ZTestAddCreateTrainerController {

    //----------------------------------------------------------------------------------
    //AddCreateTrainerController 
    //----------------------------------------------------------------------------------
    @isTest static void testAddCreateTrainerController() {
        PageReference pageRef = Page.EnrollmentDiscount;
        Test.setCurrentPageReference(pageRef);
        Offer__c schedule = ZDataTestUtility.createOpenTrainingSchedule(null, true);
        
        ApexPages.CurrentPage().getparameters().put('scheduleId', schedule.id);
        
        AddCreateTrainerController actc = new AddCreateTrainerController();
        System.assertEquals(actc.scheduleId, schedule.id);
        
        Contact trainerToCreate = actc.trainerToCreate;
        actc.saveAndPin();
        System.assertEquals(trainerToCreate, actc.trainerToCreate);
        
        AddCreateTrainerController.setTrainer(actc.trainerToCreate.Id, schedule.id);
        AddCreateTrainerController.getContacts('');
        
        System.assertNotEquals(null, actc.getTrainerFields());
    }
}