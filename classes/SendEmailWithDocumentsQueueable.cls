public without sharing class SendEmailWithDocumentsQueueable implements Queueable, Database.AllowsCallouts {

    Set<Id> docsToCheckIfGeneratingComplete;

    public SendEmailWithDocumentsQueueable(Set<Id> docsToCheckIfGeneratingComplete) {
        this.docsToCheckIfGeneratingComplete = docsToCheckIfGeneratingComplete;
    }

    public void execute(QueueableContext qc) {
        EmailManager_DocumentListEmail.sendRequiredDocumentList(docsToCheckIfGeneratingComplete);
    }
}