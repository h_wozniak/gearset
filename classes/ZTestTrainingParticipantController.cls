@isTest
private class ZTestTrainingParticipantController {
    //----------------------------------------------------------------------------------
    //TrainingParticipantController
    //----------------------------------------------------------------------------------
    
    private static Map<Integer, sObject> prepareData2() {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        retMap.put(0, ZDataTestUtility.createTrainingOffer(new Offer__c(
            Type__c = CommonUtility.OFFER_TYPE_CLOSED
        ), true));
        retMap.put(1, ZDataTestUtility.createClosedTrainingSchedule(new Offer__c(
            Trade_Name__c = 'Obsługa komputera',
            Training_Offer_from_Schedule__c = retMap.get(0).Id,
            Active__c = true,
            Bank_Account_No__c = '123',
            Number_of_Seats__c = 5,
            Offer_Value__c = 1000,
            Valid_From__c = System.today(),
            Valid_To__c = System.today(),
            Start_Time__c = '10:50',
            Trainer__c = ZDataTestUtility.createCandidateContact(null, true).Id,
            Training_Place__c = 'Hotel WSB',
            City__c = 'Wrocław',
            Street__c = 'Warszawska 10',
            Payment_Deadline__c = System.today()
        ), true));
        retMap.put(2, ZDataTestUtility.createClosedTrainingEnrollment(new Enrollment__c(
            Training_Offer__c = retMap.get(1).Id,
            Company__c = ZDataTestUtility.createCompany(new Account(
                NIP__c = '3672620147'
            ), true).Id
        ), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentParticipant(new Enrollment__c(
            Enrollment_Training_Participant__c = retMap.get(2).Id,
            Training_Offer_for_Participant__c = retMap.get(1).Id,
            Enrollment_Value__c = ((Offer__c)retMap.get(1)).Price_per_Person__c
        ), true));
        
        return retMap;
    }
    
    @isTest static void testTrainingParticipantController() {
        Map<Integer, sObject> dataMap = prepareData2();

        PageReference pageRef = Page.TrainingParticipant;
        Test.setCurrentPageReference(pageRef);

        Apexpages.currentPage().getParameters().put('candidate', dataMap.get(3).Id);

        ApexPages.StandardController stdController = new ApexPages.standardController(dataMap.get(3));
        TrainingParticipantController cntrl = new TrainingParticipantController(stdController);

        Test.startTest();
        cntrl.participant.Exam_Pass__c = true;
        cntrl.save();
        Test.stopTest();

        Enrollment__c eAfter = [SELECT Exam_Pass__c FROM Enrollment__c WHERE Id = :dataMap.get(3).Id];
        System.assertEquals(true, eAfter.Exam_Pass__c);

        System.assertNotEquals(null, cntrl.back());

        System.assertNotEquals(null, cntrl.save());
        cntrl.participant = null;
        System.assertEquals(null, cntrl.save());
    }
}