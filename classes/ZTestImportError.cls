@isTest
private class ZTestImportError {
    
    @isTest static void testImportError() {
        Test.startTest();
        ApexPages.Message errorValue = ImportError.getError('fileNum', 0, 0, 'errorLabel', 'line');
        Test.stopTest();
        
        String result = '[' + Label.title_File + '_' + 'fileNum' + '] ' + '[' + Label.title_ImportRow + ' ' + '0' + ', ' + Label.title_ImportColumn + ' ' + '0' + '] ' + 'errorLabel' + ' ' + '<b>' + 'line' + '</b>';
        ApexPages.Message newValue = new ApexPages.Message(ApexPages.severity.ERROR, result);
        
        System.assertEquals(errorValue, newValue);
    }
    
    @isTest static void testImportErrorWithoutFileName() {
        Test.startTest();
        ApexPages.Message errorValue = ImportError.getError(null, 0, 0, 'errorLabel', 'line');
        Test.stopTest();
        
        String result = '[' + Label.title_ImportRow + ' ' + '0' + ', ' + Label.title_ImportColumn + ' ' + '0' + '] ' + 'errorLabel' + ' ' + '<b>' + 'line' + '</b>';
        ApexPages.Message newValue = new ApexPages.Message(ApexPages.severity.ERROR, result);
        
        System.assertEquals(errorValue, newValue);
    }

}