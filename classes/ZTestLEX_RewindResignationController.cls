/**
 * Created by martyna.stanczuk on 2018-06-28.
 */
@isTest
public with sharing class ZTestLEX_RewindResignationController {

    @isTest static void test_hasEditAccess() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(null, true);
        String result = LEX_Rewind_Resignation_Controller.hasEditAccess(participant.Id);
        System.assertEquals(result, 'SUCCESS');
    }

    @isTest static void test_updateFieldValues() {
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(null, true);
        String result = LEX_Rewind_Resignation_Controller.updateFieldValues(participant.Id, 'Enrollment__c',
                new List<String> {'Status__c', 'Resignation_Reason_List__c', 'Resignation_Date__c', 'Resignation_Comments__c'},
                new List<String> {'string', 'string', 'date', 'string'},
                new List<String> {CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED, '', '', ''});

        System.assertEquals(result, 'SUCCESS');
    }

    /*@isTest static void test_getLastActiveStatus(){
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION,
                Last_Active_Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, Resignation_Reason_List__c = 'Wrong contact', Resignation_Date__c = system.today()), true);
        Test.startTest();
        String result = LEX_Rewind_Resignation_Controller.getLastActiveStatus(participant.Id);

        System.assertEquals(result, CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT);
        Test.stopTest();
    }*/

    /*@isTest static void test_checkRecord_true(){
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION,
                Last_Active_Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT, Resignation_Reason_List__c = 'Wrong contact', Resignation_Date__c = system.today()), true);
        Test.startTest();
        String canBeRewind = LEX_Rewind_Resignation_Controller.checkRecord(participant.Id);

        system.assertEquals(canBeRewind, 'TRUE');
        Test.stopTest();
    }*/

    @isTest static void test_checkRecord_false(){
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION,
                Last_Active_Status__c = CommonUtility.ENROLLMENT_STATUS_PAYMENT_KS, Resignation_Reason_List__c = 'Wrong contact', Resignation_Date__c = system.today()), true);
        Test.startTest();
        String canBeRewind = LEX_Rewind_Resignation_Controller.checkRecord(participant.Id);

        system.assertEquals(canBeRewind, 'FALSE');
        Test.stopTest();
    }

    @isTest static void test_checkRecord_status_not_resignation(){
        Enrollment__c participant = ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Status__c = CommonUtility.ENROLLMENT_STATUS_SIGNED_CONTRACT,
                Last_Active_Status__c = CommonUtility.ENROLLMENT_STATUS_UNCONFIRMED), true);
        String canBeRewind = LEX_Rewind_Resignation_Controller.checkRecord(participant.Id);

        system.assertEquals(canBeRewind, 'FALSE');
    }

}