public with sharing class LEX_Map_ClassifiersController {
	@AuraEnabled
	public static String hasEditAccess(String recordId) {
		return LEXServiceUtilities.hasEditAccess(recordId) ? 'SUCCESS' : 'NO_EDIT_ACCESS';
	}
	@AuraEnabled
	public static String hasExternalCampaign(String recordId) {
		Marketing_Campaign__c camp = [SELECT External_campaign__c FROM Marketing_Campaign__c WHERE Id = :recordId];
		return camp.External_campaign__c != null ? 'TRUE' : 'FALSE';
	}
}