/**
 * Created by Sebastian on 22.01.2018.
 */

public with sharing class LEX_DocumentAcceptationController {

    @AuraEnabled
    public static String getEnrollmentStatus(Id studyEnrId) {
        Enrollment__c studyEnrollment = [
                SELECT Id, Status__c
                FROM Enrollment__c
                WHERE Id = :studyEnrId
        ];

        return studyEnrollment.Status__c;
    }

    @AuraEnabled
    public static String markDocumentsAsDelivered(List<Id> recordIds, String dateString) {
        List<Enrollment__c> docList = [
                SELECT Id, Document_Delivered__c, Delivery_Date__c
                FROM Enrollment__c
                WHERE Id IN :recordIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
                AND Document_Delivered__c = false
        ];

        Date deliveryDate;
        if (dateString != null && dateString != '') {
            try {
                List<String> dateParts = dateString.split('-');
                deliveryDate = Date.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
            } catch(Exception ex) {
                // wrong date input
                return Label.msg_error_WrongDate;
            }
        } else {
            deliveryDate = System.today();
        }

        for (Enrollment__c doc : docList) {
            doc.Document_Delivered__c = true;
            doc.Delivery_Date__c = deliveryDate;
        }


        try {
            update docList;
            return String.valueOf(true);
        } catch (Exception ex) {
            // other error, i.e. trigger validation
            return ex.getMessage();
        }
    }

    @AuraEnabled
    public static String markDocumentsAsAccepted(List<Id> recordIds, String dateString) {
        List<Enrollment__c> docList = [
                SELECT Id, Document_Accepted__c, Acceptance_Date__c
                FROM Enrollment__c
                WHERE Id IN :recordIds
                AND RecordTypeId = :CommonUtility.getRecordTypeId('Enrollment__c', CommonUtility.ENROLLMENT_RT_ENROLLMENT_DOCUMENT)
                AND Document_Accepted__c = false
        ];

        Date deliveryDate;
        if (dateString != null && dateString != '') {
            try {
                List<String> dateParts = dateString.split('-');
                deliveryDate = Date.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
            } catch(Exception ex) {
                // wrong date input
                return Label.msg_error_WrongDate;
            }
        } else {
            deliveryDate = System.today();
        }

        for (Enrollment__c doc : docList) {
            doc.Document_Accepted__c = true;
            doc.Acceptance_Date__c = deliveryDate;
        }


        try {
            update docList;
            return String.valueOf(true);
        } catch (Exception ex) {
            // other error, i.e. trigger validation
            return ex.getMessage();
        }
    }

    @AuraEnabled
    public static Boolean hasEditAccess(Id studyEnrId) {
        UserRecordAccess ura = [
                SELECT RecordId, HasEditAccess
                FROM UserRecordAccess
                WHERE UserId = :UserInfo.getUserId()
                AND RecordId = :studyEnrId
        ];

        return ura.HasEditAccess;
    }
}