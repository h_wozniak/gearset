global class DocGen_GeneratePriceBookOnEnrollment implements enxoodocgen.DocGen_StringTokenInterface {
    public static String GUARANTED_FIXED_TUITION;
    public static String INSTALLMENTS_VALUE;
    public static String SUM_PAYMENTS_PER_YEAR;
    public static String GRADED_TUITION;
    public static String INSTALLMENTS_PER_YEAR;
    public static String INSTALLMENTS;
    public static String TUITION_SYSTEM;
    public static String ONE_TIME_PAYMENT;

    public DocGen_GeneratePriceBookOnEnrollment() {}

    global static String executeMethod(String objectId, String templateId, String methodName) {
        Enrollment__c enr = [SELECT Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId];
        String languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

        GUARANTED_FIXED_TUITION = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_GUARANTED_FIXED_TUITION');
        INSTALLMENTS_VALUE = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS_VALUE');
        SUM_PAYMENTS_PER_YEAR = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_SUM_PAYMENTS_PER_YEAR');
        GRADED_TUITION = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_GRADED_TUITION');
        INSTALLMENTS_PER_YEAR = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS_PER_YEAR');
        INSTALLMENTS = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_INSTALLMENTS');
        TUITION_SYSTEM = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_TUITION_SYSTEM');
        ONE_TIME_PAYMENT = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_ONE_TIME_PAYMENT');

        if (methodName == 'PriceBookSP') {
            return DocGen_GeneratePriceBookOnEnrollment.generateDynamicPriceBookSP(objectId, templateId);
        }
        else {
            return DocGen_GeneratePriceBookOnEnrollment.generateDynamicPriceBook(objectId);  
        }
    }

    global static String generateDynamicPriceBook(String objectId) {        
        Enrollment__c enrollmentWithPriceBook = [
            SELECT Id, Price_Book_from_Enrollment__c, Number_of_Semesters__c, Language_of_Main_Enrollment__c 
            FROM Enrollment__c 
            WHERE Id =: objectId
        ];

        String languageCode;
        if (enrollmentWithPriceBook.Price_Book_from_Enrollment__c == null) {
            Enrollment__c enr = [SELECT Enrollment_from_Documents__c, Number_of_Semesters__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId];
            languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

            enrollmentWithPriceBook = [SELECT Id, Price_Book_from_Enrollment__c, Number_of_Semesters__c FROM Enrollment__c WHERE Id =: enr.Enrollment_from_Documents__c];
        }

        final List<String> academicYear = new List<String> {
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_1Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_2Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_3Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_4Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_5Year')
        };

        Offer__c priceBookOnEnrollment = [SELECT Id FROM Offer__c WHERE Id = :enrollmentWithPriceBook.Price_Book_from_Enrollment__c];

        List<Offer__c> installmentsOnPriceBook = [
            SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c = :enrollmentWithPriceBook.Price_Book_from_Enrollment__c
            ORDER BY Installment_Variant__c ASC NULLS LAST
        ];

        Map<Integer, Offer__c> priceBook = PriceBookManager.getInstallmentConfigsMapByInstallmentVariant(installmentsOnPriceBook);

        List<Integer> listOfInstallmentVariants = getListOfInstallmentsVariants(priceBook);
        Set<String> listOfTuitionSystem = getListOfTuitionSystem(listOfInstallmentVariants, priceBook);

        String result = '';

        result += '<w:tbl><w:tblPr><w:tblW w:w="10773" w:type="dxa"/><w:tblInd w:w="60" w:type="dxa"/><w:tblBorders><w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1516"/></w:tblGrid>';

        result += createTableHeaderRow(listOfInstallmentVariants.size()) + createTableSecondHeaderRow(6061, listOfInstallmentVariants);

        Boolean first = true;
        Integer counter = 0;
        String lastRow = String.valueOf(enrollmentWithPriceBook.Number_of_Semesters__c);
        
        for (String tuitionSystem : listOfTuitionSystem) {
            if (tuitionSystem == 'Fixed_Price__c') result += generateGuarantedFixedTuitionRow(6061, listOfInstallmentVariants, priceBook);
            else {
                result += generateGradedTuitionRow(first, 6061, academicYear.get(counter), tuitionSystem, listOfInstallmentVariants, priceBook, lastRow, counter);
                first = false;
                counter++;
            }
        }

        result += '</w:tbl>';
        
        return result;
    }

    global static String generateDynamicPriceBookSP(String objectId, String templateId) {
        enxoodocgen__Document_Template__c template = [
            SELECT Name
            FROM enxoodocgen__Document_Template__c 
            WHERE Id = :templateId 
        ];
        Decimal numberOfSemesters = 0;
        Enrollment__c enrollmentWithPriceBook = [SELECT Id, Number_of_Semesters__c, Price_Book_from_Enrollment__c FROM Enrollment__c WHERE Id =: objectId];
        numberOfSemesters = enrollmentWithPriceBook.Number_of_Semesters__c;

        String languageCode;
        if (enrollmentWithPriceBook.Price_Book_from_Enrollment__c == null) {
            Enrollment__c enr = [SELECT Enrollment_from_Documents__c, Number_of_Semesters__c, Language_of_Main_Enrollment__c FROM Enrollment__c WHERE Id = :objectId];
            languageCode = CommonUtility.getLanguageAbbreviationDocGen(enr.Language_of_Main_Enrollment__c);

            enrollmentWithPriceBook = [SELECT Id, Price_Book_from_Enrollment__c, Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c, Number_of_Semesters__c FROM Enrollment__c WHERE Id =: enr.Enrollment_from_Documents__c];

            if (enrollmentWithPriceBook.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c != null) {
                numberOfSemesters = enrollmentWithPriceBook.Price_Book_from_Enrollment__r.Number_of_Payment_Semesters__c;
            }
            else {
                numberOfSemesters = enrollmentWithPriceBook.Number_of_Semesters__c;                
            }
        }

        final List<String> academicYear = new List<String> {
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_1Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_2Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_3Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_4Year'),
            DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_5Year')
        };

        Offer__c priceBookOnEnrollment = [SELECT Id, Onetime_Price__c FROM Offer__c WHERE Id = :enrollmentWithPriceBook.Price_Book_from_Enrollment__c];

        List<Offer__c> installmentsOnPriceBook = [
            SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c, Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
            FROM Offer__c 
            WHERE Price_Book_from_Installment_Config__c = :enrollmentWithPriceBook.Price_Book_from_Enrollment__c
            ORDER BY Installment_Variant__c ASC NULLS LAST
        ];

        Map<Integer, Offer__c> priceBook = PriceBookManager.getInstallmentConfigsMapByInstallmentVariant(installmentsOnPriceBook);

        List<Integer> listOfInstallmentVariants = getListOfInstallmentsVariants(priceBook);
        Set<String> listOfTuitionSystem = getListOfTuitionSystem(listOfInstallmentVariants, priceBook);

        String result = '';

        result += '<w:tbl><w:tblPr><w:tblW w:w="10773" w:type="dxa"/><w:tblInd w:w="60" w:type="dxa"/><w:tblBorders><w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="10" w:type="dxa"/><w:right w:w="10" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1515"/><w:gridCol w:w="1516"/></w:tblGrid>';


        result += createRow(priceBookOnEnrollment.Onetime_Price__c, listOfInstallmentVariants);
        Integer year = 0;
        Boolean first = true;
        Decimal academicYearValue = math.mod(Integer.valueOf(numberOfSemesters), 2) == 0 ? (numberOfSemesters / 2) : (numberOfSemesters + 1) / 2 ;        

        for (Integer i = 0; i < academicYearValue; i++) {
            Boolean ifOddSemester = (math.mod(Integer.valueOf(numberOfSemesters), 2) != 0) && ((i+1) == academicYearValue);
            result += createGuarantedFixedTuitionRowSP(ifOddSemester, first, academicYear.get(i), priceBookOnEnrollment.Onetime_Price__c, 6061, listOfInstallmentVariants, priceBook);
            first = false;
        }

        if (template.Name.contains(CommonUtility.OFFER_DEGREE_MBA) && (template.Name.contains(RecordVals.WSB_NAME_WRO))) {
            Integer spanWidth = 2 + listOfInstallmentVariants.size();
            if (priceBookOnEnrollment.Onetime_Price__c != null) spanWidth = spanWidth + 1;

            String docGen_WS_WRO_1 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_WRO_1');
            String docGen_WS_WRO_2 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_WRO_2');
            String docGen_WS_WRO_3 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_WRO_3');
            String docGen_WS_WRO_4 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_WRO_4');

            result += '<w:tr><w:tc><w:jc w:val="left"/><w:tcPr><w:tcW w:w="4545" w:type="dxa"/><w:gridSpan w:val="' + spanWidth + '"/><w:vMerge w:val="restart"/><w:vAlign w:val="left"/></w:tcPr><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + docGen_WS_WRO_1 + '</w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>  ' + docGen_WS_WRO_2 + '</w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>  ' + docGen_WS_WRO_3 + '</w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + docGen_WS_WRO_4 + '</w:t></w:r></w:p></w:tc></w:tr>';
        }

        if (template.Name.contains(CommonUtility.OFFER_DEGREE_MBA) && (template.Name.contains(RecordVals.WSB_NAME_OPO))) {
            Integer spanWidth = 2 + listOfInstallmentVariants.size();
            if (priceBookOnEnrollment.Onetime_Price__c != null) spanWidth = spanWidth + 1;

            String docGen_WS_OPO_1 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_OPO_1');
            String docGen_WS_OPO_2 = DictionaryTranslator.getTranslatedPicklistValues(languageCode, 'DocGen_WS_OPO_2');

            result += '<w:tr><w:tc><w:jc w:val="left"/><w:tcPr><w:tcW w:w="4545" w:type="dxa"/><w:gridSpan w:val="' + spanWidth + '"/><w:vMerge w:val="restart"/><w:vAlign w:val="left"/></w:tcPr><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + docGen_WS_OPO_1 + '</w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:ind w:left="108" w:right="108" /><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + docGen_WS_OPO_2 + '</w:t></w:r></w:p></w:tc></w:tr>';
        }

        result += '</w:tbl>';
        
        return result;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- HELPER MEHTODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */
    global static List<Integer> getListOfInstallmentsVariants(Map<Integer, Offer__c> priceBook) {
        List<Integer> installmentVariantsList = new List<Integer>();
        for (Integer priceBookInstallmentVariant : priceBook.keySet()) {
            installmentVariantsList.add(Integer.valueOf(priceBookInstallmentVariant));
        }

        return installmentVariantsList;
    }

    global static Set<String> getListOfTuitionSystem(List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        Set<String> tuitionSystemList = new Set<String>();
        for (Integer installmentVariant : installmentVariants) {
            if (priceBook.get(installmentVariant).Fixed_Price__c != null) tuitionSystemList.add('Fixed_Price__c');
            if (priceBook.get(installmentVariant).Graded_Price_1_Year__c != null) tuitionSystemList.add('Graded_Price_1_Year__c');
            if (priceBook.get(installmentVariant).Graded_Price_2_Year__c != null) tuitionSystemList.add('Graded_Price_2_Year__c');
            if (priceBook.get(installmentVariant).Graded_Price_3_Year__c != null) tuitionSystemList.add('Graded_Price_3_Year__c');
            if (priceBook.get(installmentVariant).Graded_Price_4_Year__c != null) tuitionSystemList.add('Graded_Price_4_Year__c');
            if (priceBook.get(installmentVariant).Graded_Price_5_Year__c != null) tuitionSystemList.add('Graded_Price_5_Year__c');
        }

        return tuitionSystemList;
    }

    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- ROW MEHTODS ---------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global static String createTableHeaderRow(Integer headerInstallmentsSpan) {
        return '<w:tr><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:gridSpan w:val="3"/><w:vMerge w:val="restart"/><w:tcW w:w="4545" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + TUITION_SYSTEM + '</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="6061" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:gridSpan w:val="' + headerInstallmentsSpan + '"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + INSTALLMENTS_PER_YEAR + '</w:t></w:r></w:p></w:tc></w:tr>';
    }


    global static String createRow(Decimal oneTimePayment, List<Integer> values) {
        String result = '<w:tr><w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/><w:jc w:val="center"/><w:tcW w:w="4545" w:type="dxa"/><w:gridSpan w:val="2"/><w:vMerge w:val="restart"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + INSTALLMENTS + '</w:t></w:r></w:p></w:tc>';

        if (oneTimePayment != null) result += createColumnWithBackgroudnColorString(1515, ONE_TIME_PAYMENT);


        for (Integer value : values) {
            result += '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/><w:tcW w:w="1768" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
        }

        result += '</w:tr>';

        return result;
    }

    global static String createTableSecondHeaderRow(Integer headerWidth, List<Integer> values) {
        String result = '<w:tr><w:jc w:val="center"/>' + createColoredMergedColumnsWithSpan(4545, 3);

        Integer width = headerWidth;
        Integer valuesSize = values.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer value : values) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;

            result += createColumnWithBackgroudnColorString(columnSize, String.valueOf(value));
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }

    global static String createGuarantedFixedTuitionRowSP(Boolean ifOdd, Boolean first, String year, Decimal oneTimePayment, Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        return generateGuarantedFixedTuitionRowMergableSP(first, year, oneTimePayment, totalCoulumnWidth, installmentVariants, priceBook) + generateGuarantedFixedTuitionRowMergedSP(ifOdd, oneTimePayment, totalCoulumnWidth, installmentVariants, priceBook);
    }

    global static String generateGuarantedFixedTuitionRowMergableSP(Boolean first, String year, Decimal oneTimePayment, Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        String result = '<w:tr><w:jc w:val="center"/>' + createMergableColumn(1515, year) + generateTableColumn(1515, INSTALLMENTS_VALUE);

        if (oneTimePayment != null) { 
            if (first) {
                result += createMergableColumn(1515, String.valueOf(oneTimePayment.format()) + ' zł');
            }
            else {
                result += createMergedColumnsWithSpan(1515, 1);                
            }
        }

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;

            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).Fixed_Price__c == null ? ' ' :  String.valueOf((priceBook.get(installmentVariant).Fixed_Price__c).format()) + ' zł'));
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }

    global static String generateGuarantedFixedTuitionRowMergedSP(Boolean ifOdd, Decimal oneTimePayment, Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        String result = '<w:tr><w:jc w:val="center"/>' + createMergedColumnsWithSpan(1515, 1) + generateTableColumn(1515, SUM_PAYMENTS_PER_YEAR);
        if (oneTimePayment != null) result += createMergedColumnsWithSpan(1515, 1);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;
            if (ifOdd) {
                Decimal inst = installmentVariant == 1 ? 0.5 : installmentVariant / 2;

                result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).Fixed_Price__c == null ? ' ' :  String.valueOf((   priceBook.get(installmentVariant).Fixed_Price__c * (inst)).format()) + ' zł' ));
            }
            else {
                result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).Fixed_Price__c == null ? ' ' :  String.valueOf((priceBook.get(installmentVariant).Fixed_Price__c * installmentVariant).format()) + ' zł' ));
            }
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }







    global static String generateGuarantedFixedTuitionRow(Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        return generateGuarantedFixedTuitionRowMergable(totalCoulumnWidth, installmentVariants, priceBook) + generateGuarantedFixedTuitionRowMerged(totalCoulumnWidth, installmentVariants, priceBook);
    }

    global static String generateGuarantedFixedTuitionRowMergable(Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        String result = '<w:tr>' + createMergableColumnWithSpan(3030, 2, GUARANTED_FIXED_TUITION) + generateTableColumn(1515, INSTALLMENTS_VALUE);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;

            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).Fixed_Price__c == null ? ' ' :  String.valueOf((priceBook.get(installmentVariant).Fixed_Price__c).format()) + ' zł'));
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }

    global static String generateGuarantedFixedTuitionRowMerged(Integer totalCoulumnWidth, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        String result = '<w:tr>' + createMergedColumnsWithSpan(3030, 2) + generateTableColumn(1515, SUM_PAYMENTS_PER_YEAR);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;

            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).Fixed_Price__c == null ? ' ' :  String.valueOf((priceBook.get(installmentVariant).Fixed_Price__c * installmentVariant).format()) + ' zł'));
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }

    global static String generateGradedTuitionRow(Boolean first, Integer totalCoulumnWidth, String academicYear, String gradedPrice, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook, String lastRow, Integer counter) {
        String result =  first 
                         ? generateGradedTuitionMergableRowWithLabel(totalCoulumnWidth, academicYear, gradedPrice, installmentVariants, priceBook)
                         : generateGradedTuitionMergableRowWithoutLabel(totalCoulumnWidth, academicYear, gradedPrice, installmentVariants, priceBook, lastRow, counter);

        return (result + generateGradedTuitionMergedRow(totalCoulumnWidth, gradedPrice, installmentVariants, priceBook, lastRow, counter));
    }

    global static String generateGradedTuitionMergableRowWithLabel(Integer totalCoulumnWidth, String academicYear, String gradedPrice, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook) {
        String result = '<w:tr>' + createMergableColumn(1515, GRADED_TUITION) + createMergableColumn(1515, academicYear) + generateTableColumn(1515, INSTALLMENTS_VALUE);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;
            
            Integer value = Integer.valueOf(priceBook.get(installmentVariant).get(gradedPrice));
            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).get(gradedPrice) == null ? ' ' :  String.valueOf(value.format()) + ' zł'));
            currentWidth -= columnSize; 
        }
        result += '</w:tr>';

        return result;
    }

    global static String generateGradedTuitionMergableRowWithoutLabel(Integer totalCoulumnWidth, String academicYear, String gradedPrice, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook, String lastRow, Integer counter) {
        String result = '<w:tr>' + generateMergedColumn(1515) + createMergableColumn(1515, academicYear) + generateTableColumn(1515, INSTALLMENTS_VALUE);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;
            
            Decimal value = priceBook.get(installmentVariant).get(gradedPrice) != null ? Integer.valueOf(priceBook.get(installmentVariant).get(gradedPrice)) : null;  

            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).get(gradedPrice) == null ? ' ' :  String.valueOf(value.format()) + ' zł'));

            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }

    global static String generateGradedTuitionMergedRow(Integer totalCoulumnWidth, String gradedPrice, List<Integer> installmentVariants, Map<Integer, Offer__c> priceBook, String lastRow, Integer counter) {
        String result = '<w:tr>' + generateMergedColumn(1515) + generateMergedColumn(1515) + generateTableColumn(1515, SUM_PAYMENTS_PER_YEAR);

        Integer width = totalCoulumnWidth;
        Integer valuesSize = installmentVariants.size();
        Integer currentWidth = width;
        Integer columnCounter = valuesSize;

        for (Integer installmentVariant : installmentVariants) {
            columnCounter--;
            Integer columnSize = columnCounter > 0 ? width / valuesSize : currentWidth;
            
            Decimal value = priceBook.get(installmentVariant).get(gradedPrice) != null ? Integer.valueOf(priceBook.get(installmentVariant).get(gradedPrice)) * installmentVariant : null;

            if (Integer.valueOf(lastRow) == ((2 * counter) + 1) && math.mod(Integer.valueOf(lastRow), 2) != 0) {   
                value = value == null ? null : value * 0.5;
            }   

            result += generateTableColumn(columnSize, (priceBook.get(installmentVariant).get(gradedPrice) == null ? ' ' :  String.valueOf(value.format()) + ' zł'));
            currentWidth -= columnSize; 
        }

        result += '</w:tr>';

        return result;
    }


    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------------- COLUMN METHODS ------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    global static String generateTableColumn(Integer width, String value) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value +'</w:t></w:r></w:p></w:tc>';
    }

    global static String generateMergedColumn(Integer width) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:vMerge/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p></w:tc>';
    }
    
    global static String createMergableColumn(Integer width, String value) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:vMerge w:val="restart"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createMergedColumnsWithSpan(Integer width, Integer gridSpanSize) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:gridSpan w:val="' + gridSpanSize + '"/><w:vMerge/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p></w:tc>';
    }

    global static String createColoredMergedColumnsWithSpan(Integer width, Integer gridSpanSize) {
        return '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="' + width + '" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/><w:gridSpan w:val="' + gridSpanSize + '"/><w:vMerge/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p></w:tc>';
    }

    global static String createMergableColumnWithSpan(Integer width, Integer gridSpanSize, String value) {
        return '<w:tc><w:tcPr><w:tcW w:w="' + width + '" w:type="dxa"/><w:gridSpan w:val="' + gridSpanSize + '"/><w:vMerge w:val="restart"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }

    global static String createColumnWithBackgroudnColorString(Integer width, String value) {
        return '<w:tc><w:tcPr><w:cnfStyle w:val="001000000000" w:firstRow="0" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:oddVBand="0" w:evenVBand="0" w:oddHBand="0" w:evenHBand="0" w:firstRowFirstColumn="0" w:firstRowLastColumn="0" w:lastRowFirstColumn="0" w:lastRowLastColumn="0"/><w:tcW w:w="' + width + '" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p><w:pPr><w:widowControl/><w:autoSpaceDE w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>' + value + '</w:t></w:r></w:p></w:tc>';
    }
}