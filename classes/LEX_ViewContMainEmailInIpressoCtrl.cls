public with sharing class LEX_ViewContMainEmailInIpressoCtrl {
	@AuraEnabled
	public static String retrieveMainEmailFromIPressoContact(String recordId) {
		Contact con = [SELECT Id, Email FROM Contact WHERE Id = :recordId];
		return LEXStudyUtilities.retrieveMainEmailFromIPressoContact(con.Id, con.Email);
	}

	@AuraEnabled
	public static String retrieveControlPanelLink() {
		return LEXStudyUtilities.retrieveControlPanelLink();
	}
}