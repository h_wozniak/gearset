/**
*   @author         Mateusz Pruszyński, Wojciech Słodziak
*   @description    Utility class required for creating URLS and redirects in Javascript buttons 
**/

global without sharing class URLFactory {

    public static final String FAULT = 'fault';
    public static final String NAME_AUTOGENERATED = Label.placeholder_AutoGenerated;


    /* this method returns the whole url (apart from retURL parameter) based on JSON string passed as parameter in JSON_Wrapper structure */
    webservice static String generateUrl(String jsonString, Boolean isOverriden) {
        String url = '';
        String noOverride = isOverriden == true ? '&nooverride=1' : '';

        try {
            JSONParser parser = JSON.createParser(jsonString);

            JSON_Wrapper parsedValues = (JSON_Wrapper)parser.readValueAs(JSON_Wrapper.class);
            
            url += '/' + getPrefix(parsedValues.sObjectToName) + '/e?RecordType=' 
                + CommonUtility.getRecordTypeId(parsedValues.sObjectToName, parsedValues.recordTypeDevName) + noOverride;

            Integer initFieldListSize = parsedValues.fieldToApiNames.size();

            if (parsedValues.placeholderForName != null && parsedValues.placeholderForName == true) {
                parsedValues.fieldToApiNames.add('Name');
            }

            List<String> labelFields = getLabelForField(parsedValues.sObjectToName, parsedValues.fieldToApiNames);

            List<String> fieldIds = getFieldIds(labelFields, url);

            String queryFields = '';
            for (String fieldToken : parsedValues.fieldFromApiNames) {
                queryFields += fieldToken + ', ';
            }
            queryFields = queryFields.substring(0, queryFields.length() - 2);
            
            String query = 'SELECT ' + queryFields + ' FROM ' + parsedValues.sObjectFromName + ' WHERE Id = ' + '\'' + parsedValues.sObjectFromId + '\'';

            SObject queryResult = Database.Query(query);

            System.debug(queryResult);

            url += URLFactory.buildUrlBasedOnFields(parsedValues, fieldIds, queryResult, initFieldListSize);
            System.debug(url);

            return url;
        } catch(Exception e) {
            ErrorLogger.log(e);
            return FAULT;
        }           
    }

    private static String buildUrlBasedOnFields(JSON_Wrapper parsedValues, List<String> fieldIds, SObject queryResult, Integer initFieldListSize) {
        String urlPart = '';

        for (Integer i = 0; i < initFieldListSize; i++) {
            String fieldToken = parsedValues.fieldFromApiNames[i];

            String fieldValue;
            String idForLookup;

            if (fieldToken.contains('.')) {
                List<String> splitFields = fieldToken.split('\\.');

                SObject lookupObjPrev;
                SObject lookupObj = queryResult;

                for (Integer j = 0; j < splitFields.size() - 1; j++) {
                    lookupObjPrev = lookupObj;
                    lookupObj = lookupObjPrev.getSObject(splitFields[j]);

                    if (lookupObj != null && j == splitFields.size() - 2) {
                        fieldValue = String.valueOf(lookupObj.get(splitFields[j + 1]));
                        if (splitFields[j + 1] == 'Name') {
                            String lookupName = splitFields[j].replace('__r', '__c');
                            idForLookup = String.valueOf(lookupObjPrev.get(lookupName));
                        }
                    }
                }
            } else {
                fieldValue = String.valueOf(queryResult.get(parsedValues.fieldFromApiNames[i]));
                if (fieldToken == 'Name') {
                    idForLookup = parsedValues.sObjectFromId;
                }
            }

            if (!String.isBlank(fieldValue)) {
                if (idForLookup == null) {
                    urlPart += '&' + fieldIds[i] + '=' + EncodingUtil.urlEncode(URLFactory.conditionalConversion(fieldValue), 'UTF-8');
                } else {
                    urlPart += '&' + fieldIds[i] + '=' + EncodingUtil.urlEncode(fieldValue, 'UTF-8') + '&' + fieldIds[i] + '_lkid=' + idForLookup;
                }
            }
        }

        if (parsedValues.placeholderForName != null && parsedValues.placeholderForName == true) {
            urlPart += '&' + fieldIds[initFieldListSize] + '=' + URLFactory.NAME_AUTOGENERATED;
        }

        return urlPart;
    }


    /* method that returns list of field Ids placed on html document */
    public static List<String> getFieldIds(List<String> fieldLabels, String url) {
        List<String> fieldIds = new List<String>();
        PageReference p = new PageReference(url);
        String html;
        if (Test.IsRunningTest()) {
            html = Blob.valueOf('UNIT.TEST').toString();
        }
        else {
            html = p.getContent().toString();
        }

        for (String field_label : fieldLabels) {
            Map<String, String> labelToId = new Map<String, String>();
            Matcher m = Pattern.compile('<label for="(.*?)">(<span class="assistiveText">\\*</span>)?(.*?)</label>').matcher(html);
            while (m.find()) {
                String label = m.group(3);
                String id = m.group(1);
                if (label.equalsIgnoreCase(field_label)) {
                    fieldIds.add(id);
                }
            }
        }
        return fieldIds;
    } 

    /* method that returns list of field labels in user locale */
    public static String[] getLabelForField(String objectName, String[] fields) {
        for (integer i = 0; i < fields.size(); i++) {
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType leadSchema = schemaMap.get(objectName);
            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

            fields[i] = fieldMap.get(fields[i]).getDescribe().getLabel();
        }
        return fields;
    } 


    /* methods to get url prefixes of sObjects */
    @TestVisible
    private static String getPrefix(String sObjectApiName) {
        Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
        return globalDescribe.get(sObjectApiName).getDescribe().getkeyprefix();
    }   


    /* method that returns field value in correct format for URL */
    @TestVisible
    private static String conditionalConversion(String val) {
        if (val == null) return '';

        //Boolean
        if (val == 'true') return '1';
        if (val == 'false') return '0';

        //Numbers
        Pattern numPattern = Pattern.compile('-?[0-9]*\\.[0-9]*');
        Matcher m1 = numPattern.matcher(val);
        if (m1.matches()) {
            return val.replace('.', ',');
        }

        //Date
        Pattern datePattern = Pattern.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}');
        Matcher m2 = datePattern.matcher(val);
        if (m2.find()) {
            return Date.valueOf(val).format();
        }

        return val;
    }


    private class JSON_Wrapper {
        String sObjectFromId;
        String sObjectToName;
        String sObjectFromName;
        String recordTypeDevName;
        List<String> fieldToApiNames;
        List<String> fieldFromApiNames;
        Boolean placeholderForName;
    }

}