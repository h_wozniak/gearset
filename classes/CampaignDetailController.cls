public with sharing class CampaignDetailController {
    
    public Marketing_Campaign__c campaignDetail { get; set; }
    public Enrollment__c enrollmentToUpdate { get; set; }
    public List<Schema.FieldSetMember> campaignMembersFields { get; set; }
    public List<Schema.FieldSetMember> campaignDetailsFields { get; set; }
    public List<Schema.FieldSetMember> enrollmentResignationFields { get; set; }
    public List<SelectOption> classifiersOnCampaign { get; set; }
    public String selectedClassifier { get; set; }
    public List<String> campaignMemberLookUps;
    public Id marketingCampaign;
    public Id contactId { get; set; }
    public String retUrl;
    public Marketing_Campaign__c classifiers { get; set; }
    
    public CampaignDetailController() {
        campaignMemberLookUps = campaignDetailsMapToString();
        contactId = Apexpages.currentPage().getParameters().get('contactId');
        marketingCampaign = Apexpages.currentPage().getParameters().get('marketingCampaign');
        retUrl = Apexpages.currentPage().getParameters().get('retURL');
        Id campaignMemberId = Apexpages.currentPage().getParameters().get('campaignId');
        if (campaignMemberId != null) {
            String query = 'SELECT Comments__c, Campaign_Resignation_Reason__c, Campaign_Resignation_Comments__c, Classifier_from_Campaign_Member__c, ';

            campaignMembersFields = campaignMemberFields();
            for (Schema.FieldSetMember campaignMemberFields : campaignMembersFields) {
                query += campaignMemberFields.getFieldPath() + ', ';
            }

            campaignDetailsFields = campaignDetailsFields();
            for (Schema.FieldSetMember campaignDetailFields : campaignDetailsFields) {
                query += campaignDetailFields.getFieldPath() + ', ';
            }
            query = query.left(query.length() - 2);
            query += ' FROM Marketing_Campaign__c WHERE Id = :campaignMemberId';
            
            enrollmentResignationFields = enrollmentResignationFields();

            campaignDetail = Database.query(query);  

            enrollmentToUpdate = new Enrollment__c();
            enrollmentToUpdate.Resignation_Reason_List__c = campaignDetail.Campaign_Resignation_Reason__c;
            enrollmentToUpdate.Resignation_Comments__c = campaignDetail.Campaign_Resignation_Comments__c;

            classifiersOnCampaign = classifiersOnMarketingCampaignFromMember(campaignMemberId);

            selectedClassifier = campaignDetail.Classifier_from_Campaign_Member__c;
        }
        else {    
            campaignMembersFields = campaignMemberFields();
            campaignDetailsFields = campaignDetailsFields();
            enrollmentResignationFields = enrollmentResignationFields();
            campaignDetail = new Marketing_Campaign__c();
            campaignDetail.Campaign_Member__c = marketingCampaign;
            campaignDetail.Campaign_Contact__c = contactId;
            campaignDetail.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_MEMBER);
            
            enrollmentToUpdate = new Enrollment__c();
            enrollmentToUpdate.Resignation_Reason_List__c = campaignDetail.Campaign_Resignation_Reason__c;
            enrollmentToUpdate.Resignation_Comments__c = campaignDetail.Campaign_Resignation_Comments__c;
            
            classifiersOnCampaign = classifiersOnMarketingCampaignFromMember(marketingCampaign);
        }
    }

    public PageReference loadClassifiers() {
        if (campaignDetail.Campaign_Member__c != null) {
            classifiersOnCampaign = classifiersOnMarketingCampaign(campaignDetail.Campaign_Member__c);
            selectedClassifier = campaignDetail.Classifier_from_Campaign_Member__c;
        }
        return null;
    }
    
    public PageReference save() {
        CommonUtility.preventDetailsCreation = true;

        Integer counter = 0;
        for (String lookUpName : campaignMemberLookUps) {
            if (campaignDetail.get(lookUpName) != null) {
                counter++;
            }
        }
        
        if (counter > 1) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.msg_error_canFillOnlyOneLookupForCampaignDetail));
            return null;
        }
        else {
            try {
                if (contactId != null) {
                    Contact candidate = [
                        SELECT Id, FirstName, LastName, Email, Phone, MobilePhone, Additional_Phones__c
                        FROM Contact
                        WHERE Id = :contactId
                    ];

                    Marketing_Campaign__c details = createCampaignMemberDetails(candidate);
                    insert details;
                    campaignDetail.Campaign_from_Campaign_Details_2__c = details.Id;
                }
                
                campaignDetail.Campaign_Resignation_Reason__c = enrollmentToUpdate.Resignation_Reason_List__c;
                campaignDetail.Campaign_Resignation_Comments__c = enrollmentToUpdate.Resignation_Comments__c;
                campaignDetail.Classifier_from_Campaign_Member__c = selectedClassifier;
                upsert campaignDetail;
            }
            catch (Exception e) {
                ApexPages.addMessages(e);
                
                return null;
            }
            for (String lookUpName : campaignMemberLookUps) {
                if (campaignDetail.get(lookUpName) != null && campaignDetail.Classifier__c == CommonUtility.MARKETING_CAMPAIGN_CLASIFFIER_RESIGNATION) {
                    Marketing_Campaign__c details = [
                        SELECT Id, Details_Enrollment_Id__c
                        FROM Marketing_Campaign__c
                        WHERE Id = :String.valueOf(campaignDetail.get(lookUpName))
                    ];
                    
                    enrollmentToUpdate.Id = details.Details_Enrollment_Id__c;
                    enrollmentToUpdate.Status__c = CommonUtility.ENROLLMENT_STATUS_RESIGNATION;
                    enrollmentToUpdate.Resignation_Date__c = Date.today();
                    
                    try {
                        update enrollmentToUpdate;
                    }
                    catch (Exception e) {
                        ErrorLogger.log(e);
                    }
                }
            }
        }
        return new PageReference('/' + campaignDetail.Id);
    } 

    public static Marketing_Campaign__c createCampaignMemberDetails(Contact candidate) {
        Marketing_Campaign__c newCampaignDetails = new Marketing_Campaign__c();
        newCampaignDetails.RecordTypeId = CommonUtility.getRecordTypeId('Marketing_Campaign__c', CommonUtility.MARKETING_RT_CAMPAIGN_DETAILS2);
        newCampaignDetails.Details_Contact_First_Name__c = candidate.FirstName;
        newCampaignDetails.Details_Contact_Last_Name__c = candidate.LastName;
        newCampaignDetails.Details_Contact_Email__c = candidate.Email;

        newCampaignDetails.Details_Contact_Phone_Numbers__c = MarketingCampaignManager.fillPhoneFieldForCampaign(candidate.Phone, 
                                                                                        candidate.MobilePhone, 
                                                                                        candidate.Additional_Phones__c);

        return newCampaignDetails;
    }

    public void pageRender() {
        for (Marketing_Campaign__c classifier : classifiers.Marketing2__r) {
            if (classifier.Id == selectedClassifier) {
                campaignDetail.Classifier__c = classifier.Classifier_Name__c;
            }
        }
    } 
    
    public PageReference cancel() {
        return new PageReference(retUrl);
    }
    
    public List<Schema.FieldSetMember> campaignMemberFields() {
        return SObjectType.Marketing_Campaign__c.FieldSets.Campaign_Member.getFields();
    }
    
    public List<Schema.FieldSetMember> campaignDetailsFields() {
        return SObjectType.Marketing_Campaign__c.FieldSets.Marketing_Campaign_Details.getFields();
    }   
    
    public List<Schema.FieldSetMember> enrollmentResignationFields() {
        return SObjectType.Enrollment__c.FieldSets.Resignation_Field_Set.getFields();
    }   

    public List<SelectOption> classifiersOnMarketingCampaign(Id campaignMemberId) {
        classifiers = [
            SELECT Id, (SELECT Id, Classifier_Name__c FROM Marketing2__r)
            FROM Marketing_Campaign__c
            WHERE Id = :campaignMemberId
        ];

        List<SelectOption> selectedClassifiers = new List<SelectOption>();
        selectedClassifiers.add(new SelectOption('', '--- ' + Label.title_SelectClassifier + ' ---'));
        for (Marketing_Campaign__c classifier : classifiers.Marketing2__r) {
            selectedClassifiers.add(new SelectOption(classifier.Id, classifier.Classifier_Name__c));
        }

        return selectedClassifiers;
    }

    public List<SelectOption> classifiersOnMarketingCampaignFromMember(Id campaignMemberId) {
        Marketing_Campaign__c mainCampaign = [SELECT Id, Campaign_Member__r.Id FROM Marketing_Campaign__c WHERE Id = :campaignMemberId];

        Set<String> namesOfClassifiersToAdd = new Set<String> {
            'vmd (system)', 'odebrany (system)', 'nieodebrany (system)', 'błędny (system)', 'zajęty (system)', 'zamknięcie (system)',
            'abandon (system)', 'zamknięcie (system, predictive)', 'nieobsłużony (system)', 'recall (system, edycja klasyfikatora)',
            'recall (system, usunięcie klasyfikatora)', 'zamknięcie (system, edycja klasyfikatora)',
            'zamknięcie (system, usunięcie klasyfikatora)', 'pominięty (system)', 'transfer (system)',
            'transfer na zewnątrz (system)', 'konsultacja (system)', 'konsultacja na zewnątrz (system)',
            'zamknięcie (system, limit połączeń)', 'zamknięcie (system, czarna lista)', 'zamknięcie (system, hlr)',
            'pds odmowa (system)', 'przekierowanie (system)', 'recall ticket (system)', 'oczekujące nieodebrane (system)'
        };

        classifiers = [
            SELECT Id, (SELECT Id, Classifier_Name__c FROM Marketing2__r WHERE Classifier_Name__c NOT IN :namesOfClassifiersToAdd)
            FROM Marketing_Campaign__c
            WHERE Id = :mainCampaign.Id
        ];

        List<SelectOption> selectedClassifiers = new List<SelectOption>();
        selectedClassifiers.add(new SelectOption('', '--- ' + Label.title_SelectClassifier + ' ---'));
        for (Marketing_Campaign__c classifier : classifiers.Marketing2__r) {
            selectedClassifiers.add(new SelectOption(classifier.Id, classifier.Classifier_Name__c));
        }

        return selectedClassifiers;
    }

    public static List<String> campaignDetailsMapToString() {        
        Map<String, Campaign_Details__c> campaignDetailsMap = Campaign_Details__c.getAll();
        List<String> fieldsToCheck = new List<String>();
        for (String campaignDetail : campaignDetailsMap.keySet()) {
            fieldsToCheck.add(campaignDetailsMap.get(campaignDetail).API_Name__c);
        }

        return fieldsToCheck;
    }
}