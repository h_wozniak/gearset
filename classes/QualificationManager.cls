/**
* @author       Sebastian Łasisz
* @description  Class for storing methods related to Didactics object
**/


public without sharing class QualificationManager {

    /* Wojciech Słodziak */
    // rewriete  University for sharing on Didactics based on assigned Contact before insert
    public static void rewriteUniversityForSharingFromContact(List<Qualification__c> didacticsToUpdate) {
        Set<Id> contactIds = new Set<Id>();
        for (Qualification__c didactics : didacticsToUpdate) {
            contactIds.add(didactics.Candidate_from_Finished_University__c);
        }

        Map<Id, Contact> contactMap = new Map<Id, Contact>([
            SELECT Id, University_for_sharing__c
            FROM Contact 
            WHERE Id IN :contactIds
        ]);

        for (Qualification__c didactics : didacticsToUpdate) {
            Contact c = contactMap.get(didactics.Candidate_from_Finished_University__c);

            if (c != null) {
                didactics.University_for_sharing__c = c.University_for_sharing__c;
            }
        }
    }

    /* Wojciech Słodziak */
    // update University for sharing on Didactics based on assigned contact
    public static void updateUniversityForSharingOnRelatedDidactics(Set<Id> contactIds) {
        List<Qualification__c> didacticsToUpdate = [
            SELECT Id, University_for_sharing__c, Candidate_from_Finished_University__r.University_for_sharing__c
            FROM Qualification__c
            WHERE Candidate_from_Finished_University__c IN : contactIds 
        ];

        for (Qualification__c didactics : didacticsToUpdate) {
            didactics.University_for_sharing__c = didactics.Candidate_from_Finished_University__r.University_for_sharing__c;
        }

        try {
            update didacticsToUpdate;
        } catch (Exception ex) {
            ErrorLogger.log(ex);
        }
    }

}