/**
* @author       Wojciech Słodziak
* @description  Class for storing methods related to Event
**/

public without sharing class EventManager {

    public static void rewriteHighSchoolsEmail(List<Event> eventsToRewriteSchoolEmail) {
        Set<Id> accountIds = new Set<Id>();
        for (Event visit : eventsToRewriteSchoolEmail) {
            accountIds.add(visit.WhatId);
        }

        List<Account> highSchools = [
                SELECT Id, Contact_Person__r.Email, WSB_Area__c
                FROM Account
                WHERE Id IN :accountIds
        ];

        for (Event visit : eventsToRewriteSchoolEmail) {
            for (Account highSchool : highSchools) {
                if (visit.WhatId == highSchool.id) {
                    visit.School_Email__c = highSchool.Contact_Person__r.Email;
                    visit.TECH_WSB_Area__c = highSchool.WSB_Area__c;
                }
            }
        }
    }

    public static void rewriteVisitatorEmail(List<Event> eventsToRewriteVisitatorEmail) {
        Set<Id> contactIds = new Set<Id>();
        for (Event visit : eventsToRewriteVisitatorEmail) {
            contactIds.add(visit.WhoId);
        }

        List<Contact> visitators = [
                SELECT Id, Email
                FROM Contact
                WHERE Id IN :contactIds
        ];

        for (Event visit : eventsToRewriteVisitatorEmail) {
            for (Contact visitator : visitators) {
                if (visit.WhoId == visitator.id) {
                    visit.Visitator_Email__c = visitator.Email;
                }
            }
        }
    }
}