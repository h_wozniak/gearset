@isTest
private class ZTestDocGen_GenerateAcademicYearUnr {
    private static Map<Integer, sObject> prepareDataForTest(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = Date.newInstance(2017, 4, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}
    private static Map<Integer, sObject> prepareDataForTest_Winter(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = Date.newInstance(2017, 10, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

    private static Map<Integer, sObject> prepareDataForTest_specialty(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = Date.newInstance(2017, 4, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(3).Id), true));
        retMap.put(5, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

    private static Map<Integer, sObject> prepareDataForTest_specialty_Winter(String degree) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();
        retMap.put(0, ZDataTestUtility.createUniversityOfferStudy(new Offer__c(Degree__c = degree, Study_Start_Date__c = Date.newInstance(2017, 10, 13)), true));
        retMap.put(1, ZDataTestUtility.createCourseOffer(new Offer__c(University_Study_Offer_from_Course__c = retMap.get(0).Id), true));
        retMap.put(2, ZDataTestUtility.createCourseSpecialtyOffer(new Offer__c(Course_Offer_from_Specialty__c = retMap.get(1).Id), true));
        retMap.put(3, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(Course_or_Specialty_Offer__c = retMap.get(2).Id), true));
        retMap.put(4, ZDataTestUtility.createEnrollmentDocument(new Enrollment__c(Enrollment_from_Documents__c = retMap.get(3).Id), true));
        retMap.put(5, ZDataTestUtility.createOfferDateAssigment(null, true));

        return retMap;
	}

	@isTest static void test_startingAcademicYear() {
		Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_I);

		Test.startTest();
		String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(dataMap.get(3).Id, null, 'startingAcademicYear');
		Test.stopTest();

		System.assertEquals(result, '1');
	}

	@isTest static void test_generateAcademicYear() {
		Map<Integer, sObject> dataMap = prepareDataForTest(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(dataMap.get(3).Id, null, 'generateAcademicYear');
		Test.stopTest();

		System.assertEquals(result, '2016/2017');		
	}

	@isTest static void test_generateAcademicYear_Winter() {
		Map<Integer, sObject> dataMap = prepareDataForTest_Winter(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(dataMap.get(3).Id, null, 'generateAcademicYear');
		Test.stopTest();

		System.assertEquals(result, '2017/2018');		
	}

	@isTest static void test_generateAcademicYear_specialty() {
		Map<Integer, sObject> dataMap = prepareDataForTest_specialty(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(dataMap.get(4).Id, null, 'generateAcademicYear');
		Test.stopTest();

		System.assertEquals(result, '2016/2017');		
	}

	@isTest static void test_generateAcademicYear_specialty_Winter() {
		Map<Integer, sObject> dataMap = prepareDataForTest_specialty_Winter(CommonUtility.OFFER_DEGREE_II);

		Test.startTest();
		String result = DocGen_GenerateAcademicYearUnenrolled.executeMethod(dataMap.get(4).Id, null, 'generateAcademicYear');
		Test.stopTest();

		System.assertEquals(result, '2017/2018');		
	}
}