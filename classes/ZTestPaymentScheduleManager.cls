@isTest
private class ZTestPaymentScheduleManager {



    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------- I II U PAYMENT SCHEDULE GENERATION  ---------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_I_II_U_ScheduleGeneration(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();


        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true,
                Entry_fee__c = 500
        ), true));

        //config PriceBook
        configPriceBook_I_II_U(retMap.get(1).Id, instAmount);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        return retMap;
    }

    private static void configPriceBook_I_II_U(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void testPaymentScheduleGeneration_I_II_U() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_I_II_U_ScheduleGeneration(100);

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        Test.stopTest();

        System.assert(!instList.isEmpty());

        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_Value__c,
                (SELECT Id, PriceBook_Value__c
                FROM ScheduleInstallments__r
                WHERE Type__c = :CommonUtility.PAYMENT_TYPE_ENTRY_FEE)
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(studyEnr.Enrollment_Value__c > 0);

        System.assert(!studyEnr.ScheduleInstallments__r.isEmpty());
        System.assertEquals(500, studyEnr.ScheduleInstallments__r[0].PriceBook_Value__c);
    }




    /* ------------------------------------------------------------------------------------------------ */
    /* -------------------------- II Unenrolled PAYMENT SCHEDULE GENERATION  -------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_II_Unenrolled_ScheduleGeneration(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Offer__c univOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                Degree__c = CommonUtility.OFFER_DEGREE_II
        ), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5,
                University_Study_Offer_from_Course__c = univOffer.Id
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBook(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true,
                Entry_fee__c = 500
        ), true));

        //config PriceBook
        configPriceBook_II_Unenrolled(retMap.get(1).Id, instAmount);

        //create enrollment
        Test.startTest();
        retMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = retMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '2'
        ), true));

        return retMap;
    }

    private static void configPriceBook_II_Unenrolled(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void testPaymentScheduleGeneration_II_Unenrolled() {
        ZDataTestUtility.prepareCatalogData(true, true);
        Map<Integer, sObject> dataMap = prepareDataForTest_II_Unenrolled_ScheduleGeneration(100);

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assert(!instList.isEmpty());

        Test.stopTest();

        // check
        Enrollment__c studyEnrToUpdate = new Enrollment__c(Id = dataMap.get(2).Id);
        studyEnrToUpdate.Unenrolled__c = true;
        update studyEnrToUpdate;


        List<Payment__c> instListAfter1 = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assertEquals(26, instListAfter1.size());

        //Test.stopTest();

    }





    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------- PG PAYMENT SCHEDULE GENERATION  -------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_PG_ScheduleGeneration(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Offer__c univOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                Degree__c = CommonUtility.OFFER_DEGREE_PG
        ), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5,
                University_Study_Offer_from_Course__c = univOffer.Id
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBookPG(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true,
                Onetime_Price__c = 2000
        ), true));

        //config PriceBook
        configPriceBook_PG(retMap.get(1).Id, instAmount);

        return retMap;
    }

    private static void configPriceBook_PG(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void testPaymentScheduleGeneration_PG_Graded() {
        Map<Integer, sObject> dataMap = prepareDataForTest_PG_ScheduleGeneration(100);

        //create enrollment
        Test.startTest();
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = dataMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '1',
                Do_not_set_up_Student_Card__c = true
        ), true));
        Test.stopTest();

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assert(!instList.isEmpty());

        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_Value__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(studyEnr.Enrollment_Value__c > 0);
    }

    @isTest static void testPaymentScheduleGeneration_PG_Onetime() {
        Map<Integer, sObject> dataMap = prepareDataForTest_PG_ScheduleGeneration(100);

        //create enrollment
        Test.startTest();
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = dataMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME,
                Installments_per_Year__c = '1',
                Do_not_set_up_Student_Card__c = true
        ), true));
        Test.stopTest();

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assert(!instList.isEmpty());

        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_Value__c,
                (SELECT Id, PriceBook_Value__c
                FROM ScheduleInstallments__r)
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(studyEnr.Enrollment_Value__c > 0);
        System.assertEquals(1, studyEnr.ScheduleInstallments__r.size());
        System.assertEquals(2000, studyEnr.ScheduleInstallments__r[0].PriceBook_Value__c);
    }






    /* ------------------------------------------------------------------------------------------------ */
    /* ------------------------------- MBA PAYMENT SCHEDULE GENERATION  -------------------------------- */
    /* ------------------------------------------------------------------------------------------------ */

    private static Map<Integer, sObject> prepareDataForTest_MBA_ScheduleGeneration(Decimal instAmount) {
        Map<Integer, sObject> retMap = new Map<Integer, sObject>();

        Offer__c univOffer = ZDataTestUtility.createUniversityOfferStudy(new Offer__c(
                Degree__c = CommonUtility.OFFER_DEGREE_MBA
        ), true);

        retMap.put(0, ZDataTestUtility.createCourseOffer(new Offer__c(
                Number_of_Semesters__c = 5,
                University_Study_Offer_from_Course__c = univOffer.Id
        ), true));
        retMap.put(1, ZDataTestUtility.createPriceBookMBA(new Offer__c(
                Graded_tuition__c = true,
                Offer_from_Price_Book__c = retMap.get(0).Id,
                Active__c = true,
                Onetime_Price__c = 1700
        ), true));

        //config PriceBook
        configPriceBook_MBA(retMap.get(1).Id, instAmount);

        return retMap;
    }

    private static void configPriceBook_MBA(Id pbId, Decimal amount) {
        Offer__c pb = [
                SELECT Id,
                (SELECT Id, Price_Book_from_Installment_Config__c, Installment_Variant__c, Fixed_Price__c, Graded_Price_1_Year__c,
                        Graded_Price_2_Year__c, Graded_Price_3_Year__c, Graded_Price_4_Year__c, Graded_Price_5_Year__c
                FROM InstallmentConfigs__r)
                FROM Offer__c
                WHERE Id = :pbId
        ];

        for (Offer__c instConfig : pb.InstallmentConfigs__r) {
            instConfig.Fixed_Price__c = amount;
            for (Schema.SObjectField field : PriceBookManager.gradedPriceFields) {
                instConfig.put(field, amount);
            }
        }

        update pb.InstallmentConfigs__r;
    }

    @isTest static void testPaymentScheduleGeneration_MBA_Graded() {
        Map<Integer, sObject> dataMap = prepareDataForTest_MBA_ScheduleGeneration(100);

        //create enrollment
        Test.startTest();
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = dataMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_GRADED,
                Installments_per_Year__c = '1',
                Do_not_set_up_Student_Card__c = true
        ), true));
        Test.stopTest();

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assert(!instList.isEmpty());

        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_Value__c
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(studyEnr.Enrollment_Value__c > 0);
    }

    @isTest static void testPaymentScheduleGeneration_MBA_Onetime() {
        Map<Integer, sObject> dataMap = prepareDataForTest_MBA_ScheduleGeneration(100);

        //create enrollment
        Test.startTest();
        dataMap.put(2, ZDataTestUtility.createEnrollmentStudy(new Enrollment__c(
                Course_or_Specialty_Offer__c = dataMap.get(0).Id,
                Tuition_System__c = CommonUtility.ENROLLMENT_TUITION_SYSTEM_ONE_TIME,
                Installments_per_Year__c = '1',
                Do_not_set_up_Student_Card__c = true
        ), true));
        Test.stopTest();

        List<Payment__c> instList = [
                SELECT Id, PriceBook_Value__c
                FROM Payment__c
                WHERE Enrollment_from_Schedule_Installment__c = :dataMap.get(2).Id
        ];

        System.assert(!instList.isEmpty());

        Enrollment__c studyEnr = [
                SELECT Id, Enrollment_Value__c,
                (SELECT Id, PriceBook_Value__c
                FROM ScheduleInstallments__r)
                FROM Enrollment__c
                WHERE Id = :dataMap.get(2).Id
        ];

        System.assert(studyEnr.Enrollment_Value__c > 0);
        System.assertEquals(1, studyEnr.ScheduleInstallments__r.size());
        System.assertEquals(1700, studyEnr.ScheduleInstallments__r[0].PriceBook_Value__c);
    }

}