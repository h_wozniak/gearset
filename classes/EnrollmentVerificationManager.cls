/**
* @author       Wojciech Słodziak
* @description  Class for handling actions related to enrollment verification
**/

public without sharing class EnrollmentVerificationManager {

    /* Wojciech Słodziak */
    // method called after Enrollment is verified by Candidate (by clicking verification link in email)
    public static void performStudyVerificationActions(List<Enrollment__c> studyEnrToPerformVerifiedActions) {
        Set<Id> enrIds = new Set<Id>();
        Map<Id, Enrollment__c> contactIdToEnrMap = new Map<Id, Enrollment__c>();
        List<Contact> contactsToUpdate = new List<Contact>();

        for (Enrollment__c verifiedEnr : studyEnrToPerformVerifiedActions) {
            enrIds.add(verifiedEnr.Id);
            contactIdToEnrMap.put(verifiedEnr.Candidate_Student__c, verifiedEnr);

            contactsToUpdate.add(
                new Contact(
                    Id = verifiedEnr.Candidate_Student__c,
                    Confirmed_Contact__c = true
                )
            );
        }

        Set<Id> contactIds = new Set<Id>();
        for (Contact c : contactsToUpdate) {
            contactIds.add(c.Id);
        }

        Map<Id, Contact> contactsAfterDeduplication = new Map<Id, Contact>([
            SELECT Id, Confirmed_Contact__c,
            Consent_Marketing__c, Consent_Electronic_Communication__c, Consent_Direct_Communications__c, Consent_Graduates__c, Consent_PUCA__c,
            Consent_Marketing_ADO__c, Consent_Electronic_Communication_ADO__c, Consent_Direct_Communications_ADO__c, Consent_Graduates_ADO__c, Consent_PUCA_ADO__c
            FROM Contact
            WHERE Id IN :contactIds
        ]);
                                
        for (Id contactId : contactIdToEnrMap.keySet()) {
            Boolean exists = false;   
            for (Id conId : contactsAfterDeduplication.keySet()) {
                if (conId == contactId) {
                    exists = true;
                }
            }

            if (!exists) contactIdToEnrMap.remove(contactId);
        }

        // create marketing consents from JSON stored on Enrollment
        try {
            EnrollmentVerificationManager.createMarketingConsentsForVerifiedContact(contactIdToEnrMap, contactsAfterDeduplication);
        } catch (Exception e) { 
            ErrorLogger.log(e);
        }
        
        System.enqueueJob(new PerformStudyVerificationActionsQueueable(enrIds));

        // verify contacts
        try {
            // will cause duplication check with possibility for automatic merge
            ContactMergeManager.isVerificationActionTransaction = true;
            CommonUtility.preventTriggeringEmailValidation = true;
            CommonUtility.preventChangingEmailTwice = true;
            CommonUtility.preventDeduplicationOfContacts = true;
            update contactsToUpdate;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }
    }


    /* Wojciech Słodziak */
    // method for calling most of the performed logic at future to prevent long study verification transaction time
    public static void performStudyVerificationActionsAtFuture(Set<Id> enrIds) {
        List<Enrollment__c> enrList = [
            SELECT Id, Candidate_Student__c, University_Name__c, Enrollment_JSON__c, Enrollment_Date__c
            FROM Enrollment__c
            WHERE Id IN :enrIds
        ];

        List<Contact> contactsToUpdate = new List<Contact>();
        List<Enrollment__c> enrollmentsToUpdate = new List<Enrollment__c>();

        // prepare for verification of enrollments and contacts
        for (Enrollment__c verifiedEnr : enrList) {
            enrollmentsToUpdate.add(
                new Enrollment__c(
                    Id = verifiedEnr.Id,
                    Status__c = CommonUtility.ENROLLMENT_STATUS_CONFIRMED,
                    Confirmation_Date__c = System.now()
                )
            );
        }

        // verify enrollments
        try {
            update enrollmentsToUpdate;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        // create event and invoke document generation
        Set<Id> enrollmentIdList = new Set<Id>();
        List<Task> tasksToInsert = new List<Task>();
        for(Enrollment__c verifiedEnr : enrList) {
            Task e = new Task();
            e.Subject = Label.title_EventCandidateVerifiedEnrollment;
            e.WhatId = verifiedEnr.Id;
            e.ActivityDate = System.today();
            e.RecordTypeId = CommonUtility.getRecordTypeId('Task', CommonUtility.TASK_RT_EMAIL);
            e.WhoId = verifiedEnr.Candidate_Student__c;
            e.Status = 'Completed';
            e.OwnerId = CommonUtility.getIntegrationUser().Id;

            tasksToInsert.add(e);
        }
        
        try {
            insert tasksToInsert;
        } catch (Exception e) {
            ErrorLogger.log(e);
        }

        TransferJobToDefaultUserEmService.tranfserJob(TransferJobToDefaultUserEmService.Transferable.DOCUMENTS, enrIds);
    }


    /* Wojciech Słodziak */
    // method for adding Marketing Consents for a newly created verified contact
    // try catch on whole method above
    private static void createMarketingConsentsForVerifiedContact(Map<Id, Enrollment__c> contactIdToEnrMap, Map<Id, Contact> contactsAfterDeduplication) {
        List<Contact> contactsToUpdate = new List<Contact>();

        List<Catalog__c> consents = [
            SELECT Id, Name, ADO__c 
            FROM Catalog__c
            WHERE RecordTypeId = :CommonUtility.getRecordTypeId('Catalog__c', CommonUtility.CATALOG_RT_CONSENTS)
        ];

        Map<String, String> consentAdoNames = new Map<String, String>();
        for (Catalog__c consent : consents) {
            consentAdoNames.put(consent.Name, consent.ADO__c);
        }

        for (Id contactId : contactIdToEnrMap.keySet()) {
            Enrollment__c studyEnr = contactIdToEnrMap.get(contactId);
            String entity = CommonUtility.getMarketingEntityByUniversityName(studyEnr.University_Name__c);
            Contact contactWithConsent = contactsAfterDeduplication.get(contactId);

            IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON parsedJSON = 
                (IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON) 
                JSON.deserialize(studyEnr.Enrollment_JSON__c, IntegrationEnrollmentReceiverHelper.Enrollment_toInsert_JSON.class);

            Set<String> consentNames = new Set<String>();
            Boolean createUODO = true;

            for (IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON consent : parsedJson.consents) {
                consentNames.add(consent.Name);

                if (consent.value && (consent.name.contains(RecordVals.MARKETING_CONSENT_1)
                    || consent.name.contains(RecordVals.MARKETING_CONSENT_2)
                    || consent.name.contains(RecordVals.MARKETING_CONSENT_3)
                    || consent.name.contains(RecordVals.MARKETING_CONSENT_4)
                    || consent.name.contains(RecordVals.MARKETING_CONSENT_5))) {
                    createUODO = false;
                }
            }


            for (IntegrationEnrollmentReceiverHelper.MarketingConsents_JSON consent : parsedJson.consents) {
                String consentADOName = CatalogManager.getApiADONameFromConsentName(consent.name);
                if (consent.value == true && consentADOName != null) {  
                    Set<String> entities = CommonUtility.getOptionsFromMultiSelect(consentAdoNames.get(consent.Name));
                    contactWithConsent = MarketingConsentManager.updateConsentOnContact(entities, contactWithConsent, consentADOName);
                }
            }

            if (createUODO) {
                contactWithConsent.Consent_PUCA__c = true;
                if (contactWithConsent.Consent_PUCA_ADO__c == null) {
                    contactWithConsent.Consent_PUCA_ADO__c = entity;
                }
                else if (!contactWithConsent.Consent_PUCA_ADO__c.contains(entity)) {
                    contactWithConsent.Consent_PUCA_ADO__c += ';' + entity;
                }
            }

            contactsToUpdate.add(contactWithConsent);
        }

        try {
            update contactsToUpdate;
        }
        catch (Exception e) {
            ErrorLogger.log(e);
        }
    }

}